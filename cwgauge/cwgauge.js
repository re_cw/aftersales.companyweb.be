﻿
function showGauge(score,gaugecontainer) {
    var gauge = new Gauge({
        renderTo: gaugecontainer,
        width       : 180,
        height      : 180,
        glow        : true,
        units       : '',
        title       : '',
        minValue    : -5,
        maxValue    : 5,
        majorTicks: ['-5', '-4', '-3', '-2', '-1', '0', '1', '2', '3', '4', '5'],
        minorTicks: 0,
        strokeTicks : false,
        highlights: [
		{ from: -5, to: -4, color: 'rgba(255, 0, 0, .8)' },
        { from: -4, to: -3, color: 'rgba(255, 64, 0, .8)' },
        { from: -3, to: -2, color: 'rgba(255, 96, 0, .8)' },
        { from: -2, to: -1, color: 'rgba(255, 200, 0, .8)' },
        { from: -1, to: -0, color: 'rgba(255, 255, 0, .8)' },
        { from: 0, to: 1, color: 'rgba(255, 255, 5, .8)' },
        { from: 1, to: 2, color: 'rgba(235, 245, 40, .8)' },
        { from: 2, to: 3, color: 'rgba(100, 250, 100, .8)' },
        { from: 3, to: 4, color: 'rgba(50, 200, 50, .8)' },
        { from: 4, to: 5, color: 'rgba(0, 150, 0, .8)' }
        ],
        animation: {
            delay : 30,
            duration: 1000,
            fn: 'linear'
        },
        valueFormat: {
            int: 1,
            dec: 1
        },
        colors      : {
            plate: '#c2cddf',
            majorTicks: '#deedff',
            minorTicks: '#deedff',
            title: '#808080',
            units: '#808080',
            numbers: '#808080',
            needle     : {
                start : 'rgba(240, 128, 128, 1)',
                end   : 'rgba(255, 160, 122, .9)',
                circle : {
                    outerStart : '#f0f0f0',
                    outerEnd   : '#ccc',
                    innerStart : '#e8e8e8',
                    innerEnd   : '#f5f5f5'
                },
                shadowUp   : 'rgba(2, 255, 255, 0.2)',
                shadowDown : 'rgba(188, 143, 143, 0.45)'
            },
            valueBox : {
                rectStart  : '#888',
                rectEnd    : '#666',
                background : '#e4f2fe',
                shadow     : 'rgba(0, 0, 0, 1)'
            },
            valueText : {
                foreground : '#444',
                shadow     : 'rgba(0, 0, 0, 0.3)'
            },
            circle : {
                shadow      : 'rgba(0, 0, 0, 0.5)',
                outerStart  : '#ddd',
                outerEnd    : '#aaa',
                middleStart : '#eee',
                middleEnd   : '#f0f0f0',
                innerStart  : '#fafafa',
                innerEnd    : '#ccc'
            }
        },
        circles : {
            outerVisible : true,
            middleVisible: true,
            innerVisible: true
        },
        valueBox : {
            visible : true
        },
        valueText : {
            visible: true
        }
    });
    gauge.onready = function() {
        gauge.setValue(score);
	    
    };
    gauge.draw();
};


