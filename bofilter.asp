﻿<!-- #include virtual ="/classes/common/variables.inc" -->
<%
    'if  request.Cookies("TELESALESTOOLV2")("usrId") = "" then response.redirect ("error.asp")
Response.CodePage = 65001
Response.CharSet = "utf-8"

    Dim LocaleJsFile, localeJsSchedFile, lang, userPicture, userFirstName
    userFirstName = request.Cookies ("TELESALESTOOLV2")("firstname")
    lang= request.Cookies ("TELESALESTOOLV2")("lang_radical")
    if len(trim(lang))= 0 then lang=C_SYS_DEFAULT_LANG
    LocaleJsFile="<script src=""locale/locale_" & lang & ".cw.js""></script>"
%>

<!DOCTYPE html>
<html>
<head>
    <title>TeleSales Tools V02.00 Back Office Quick Search</title>
    <%=LocaleJsFile%>
    <meta charset="utf-8" />
    <script src="scripts/searchmanager.js"></script>
    <link href="css/main.css" rel="stylesheet" />
	<script src="scripts/jquery-3.1.1.js"></script>
    <script type="text/javascript" charset="utf-8">

        function anyDigit(val) {
            return /^\d+$/.test(val)
        }
        function formatEuroDate(d) {
            if (NullUndefinedToNewVal(d, 0) == 0) { return ""; }
            var result = d;

            var dd = d.substr(6, 2);
            var mm = d.substr(4, 2);
            var yyyy = d.substr(0, 4);

            result = dd + "/" + mm + "/" + yyyy;
            return result;
        }
        function NullUndefinedToNewVal(value, NewVal) {
            return (value == null) ? NewVal : value;
        }
        function cleanUpCallNumber(o) {
            var n = o;
            n = n.replace(/ /g, '');
            n = n.replace(/-/g, '');
            n = n.replace(/\+/g, '00');
            n = n.replace(/\//g, '');
            return n;
        }

        function ASP_GetTsCompanyByBackOfficeID() {

            if (anyDigit(document.getElementById('criterion').value) != true) { document.getElementById("formatResult").innerHTML = '&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + locale.popup.msgusedigitsonly + '</span>'; return false };

            var fieldname = $('#cbxFieldName').val();
            var id = $('#criterion').val();
            var result;
			
            switch (fieldname) {
                case 'bo_id':
                    AjaxGetTsCompanyByBackOfficeIDPromise(id)
                    .done(function (data) {
                        if (data.length > 0)
						{
							$("#formatResult").html(ASP_FormatResult(data));
						}else{
							$("#formatResult").html('&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + fieldname + ' [ ' + id + ' ] : ' + locale.main_page.lblnoresult + '</span>');
						}
                    })

                    break;
                case 'event_id':
					AjaxGetTsCompanyByEventIDPromise(id)
					.done(function(data){
					    if (data.length > 0)
						{
							$("#formatResult").html(ASP_FormatResult(data));
						}else{
							$("#formatResult").html('&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + fieldname + ' [ ' + id + ' ] : ' + locale.main_page.lblnoresult + '</span>');
						}
					});
                    break;
                case 'company_id':
                    AjaxGetTsCompanyByCompanyIDPromise(id)
                    .done(function (data) {
                        if (data.length > 0)
						{
							$("#formatResult").html(ASP_FormatResult(data));
						}else{
							$("#formatResult").html('&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + fieldname + ' [ ' + id + ' ] : ' + locale.main_page.lblnoresult + '</span>');
						}
                    });
                    break;
                case 'company_vat':
                    AjaxGetTsCompanyByCompanyVatPromise(id)
                    .done(function (data) {
                        if (data.length > 0)
						{
							$("#formatResult").html(ASP_FormatResult(data));
						}else{
							$("#formatResult").html('&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + fieldname + ' [ ' + id + ' ] : ' + locale.main_page.lblnoresult + '</span>');
						}
                    })

                    break;
                default:
                    break;
            }
        }

        function ASP_FormatResult(result) {
            var s = ''; var contactmobile = ''; var contactphone = ''; var contactmail = ''; var userFullName = ''; var vatnav = '';
			for (var i = 0; i<result.length; i++)
			{
				var eventID = result[i].id;
				var addrfull = result[i].company_address_street + ' ' + result[i].company_address_number + ' ' + result[i].company_address_postcode + ' ' + result[i].company_address_localite
				var phon = result[i].company_tel;
				var mob = result[i].company_mobile;
				var mail = result[i].company_mail;
				vatnav = result[i].company_vat;
				userFullName = result[i].firstname + ' ' + result[i].lastname + ' (' + result[i].extension + ')'
				/*
				if (phon.length != 0) { contactphone = '<img class=onmouseoverphone src="/graphics/common/win_16x16/Phone-icon16.png" onClick="localJS_EventCall(' + result[0].company_tel + ');"> ' + result[0].company_tel; }
				if (mob.length != 0) { contactmobile = '<img class=onmouseovermobile src="/graphics/common/win_16x16/mobile16.png" onClick="localJS_EventCall(' + result[0].company_mobile + ');"> ' + result[0].company_mobile; }
				var contactfull = contactphone + ' ' + contactmobile + ' ' + mail;
				*/
				var contactfull = phon + ' ' + mob + ' ' + mail;
				var cwurl = '<a href="http://www.companyweb.be/page_companydetail.asp?vat=' + vatnav + '" target=_blank>' + vatnav + '</a>';

				s += '<table width=100%>';
				s += '<tr><td class="commontitle">' + locale.main_page.lblVatCompany + '</td><td class="testPageText"><b>' + cwurl + '</b></td>';
				s += '<td class="commontitle">COMPANY ID</td><td class="testPageText"><b>' + result[i].company_id + '</b></td>';
				s += '<td class="commontitle">BO ID</td><td class="testPageText"><b>' + result[i].company_hbc_id + '</b></td>';
				s += '<td class="commontitle">TT ID</td><td class="testPageText"><b>' + result[i].tt_id + '</b></td></tr>';

				s += '<tr><td class="commontitle" width=20%>' + locale.main_page.lblEntreprise + '</td><td colspan=7 class="testPageText"><b>' + result[i].company_name + ' ' + result[i].jfc_s_desc + '</b></td></tr>';
				s += '<tr><td colspan=8 class="testPageText">' + result[i].nbc_desc + '</td></tr>';
				s += '<tr><td colspan=8 class="testPageText">' + addrfull + '</td></tr>';
				s += '<tr><td colspan=8 class="testPageText">' + contactfull + '</td></tr>';
				s += '<tr><td colspan=8 ><hr></td></tr>';
				s += '</table>';

				if (result[i].company_hbc_id != '') {
					//GetCompanyBackOfficeInfoByCompany_hbc_id(result[0].company_hbc_id);
					//AjaxCompanyBackOfficeHistory(result[0].company_vat, result[0].company_hbc_id, "resBOHistory");
				};

				//if valid event id then if event user is logged user then display command to go to scheduler for updates
				if (eventID != '') {
					var urltoevent = '';
					var startdate = result[i].start_date;
					startdate = startdate.split(' ');
					startdate = startdate[i];
					var d = result[i].us_start_date;
					var euroStartDate = formatEuroDate(result[i].us_start_date);
					var euroEndDate = formatEuroDate(result[i].us_end_date);

					/*
					if (result[0].usr_id == getValuefromCookie("TELESALESTOOLV2", "usrId")) {
						urltoevent = '<img src =/graphics/common/win_16x16/Calendar.png class=onmouseoverlookup onClick=loadEvent(' + eventID + ',"' + d + '");>';
					}
					*/

					//TELESALES EVENT INFO TABLE
					s += '<table width=100%>'
					s += '<tr><td class="commontitle"  width=22%>Event ID</td><td class="schedulerEventType"><b>' + result[i].id + '</b>  ' + urltoevent + '</td></tr>';
					s += '<tr><td class="commontitle" width=22%>Event Owner</td><td class="schedulerEventType">' + userFullName + '</td></tr>';
					s += '<tr><td class="commontitle"  width=22%>' + locale.main_page.lblEventType + '</td><td class="schedulerEventType" style="color:' + result[i].event_type_textColor + '; background-color:' + result[i].event_type_color + '";><b>' + result[i].event_type_name + '</b></td></tr>';
					s += '<tr><td class="commontitle" width=22%>' + locale.main_page.lblStartDate + '</td><td class="schedulerDate">' + euroStartDate + '</td></tr>';
					s += '<tr><td class="commontitle" width=22%>' + locale.main_page.lblEndDate + '</td><td class="schedulerDate">' + euroEndDate + '</td></tr>';
					s += '<tr><td colspan=2 class="testPageText">' + result[i].text + '</td></tr>';
					s += '<tr><td colspan=8 ><hr></td></tr>';
					s += '</table>';
					s += ASP_formatCommentTable(eventID);
				}
			}

            return s;

        }

        //TELESALES EVENT COMMENT TABLE
        function ASP_formatCommentTable(event_id) {
            var tc = ''; var j;
            var oc = AjaxGetTsCompanyHistoryByEventID(event_id);
            tc += '<table width=100%>';
            for (j = 0; j < oc.length; j++) {
                tc += '<tr><td valign=top class="nakedcellheader" width=115px>' + formatEuroDate(oc[j].comment_date) + ' ' + oc[j].comment_time + '</td><td class="testPageText"><b>' + oc[j].event_type_name + '</b></td></tr>'
                tc += '<tr><td valign=top colspan=2 class="testPageText">' + oc[j].comment + '</td></tr>'
            };

            tc += '</table>';
            return tc;
        }

        function setQueryContainer() {
            t = '';
            t += '<table width=50% class="LeadDetails">';
            t += '<tr><td class="commontitle">field</td><td class="commontitle">value</td></tr>';
            t += '<tr><td>';
            t += '<select id="cbxFieldName" name="cbxFieldName" class="maindocmgtcontainer">';
            t += '<option value="bo_id">BO ID</option>';
            t += '<option value="event_id">Event ID</option>';
            t += '<option value="company_id">Company ID</option>';
            t += '<option value="company_vat">Vat Number</option>';
            t += '</select>';
            t += '</td><td>';
            t += '<input id="criterion" name="criterion" type="text" maxlength=50 style="width: 99%;" onkeydown="if (event.keyCode == 13){ASP_GetTsCompanyByBackOfficeID();}" />';
            t += '</td></tr><tr><td></td><td><input id="btnBoID" type="button" value=' + locale.main_page.lblsearch + ' onclick="ASP_GetTsCompanyByBackOfficeID();" /></td></tr></table><hr />';

            document.getElementById('querycontainer').innerHTML = t;
        }

        function InitializeBoFilterPage() {
            setQueryContainer();
            var boid = '';
            boid = getParameterByName("boid");
            if (boid.length != 0) {
                document.getElementById('cbxFieldName').value = 'bo_id';
                document.getElementById('criterion').value = boid;

            }
            var eventid = '';
            eventid = getParameterByName("eventid");
            if (eventid.length != 0) {
                document.getElementById('cbxFieldName').value = 'event_id';
                document.getElementById('criterion').value = eventid;

            }

            ASP_GetTsCompanyByBackOfficeID();
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.href);
            if (results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>
</head>
<body onload="InitializeBoFilterPage();">
    <!-- friendly logon info initialization -->
    <script src="classes/dal/clsPerson/AjaxPerson.js"></script>
    <div class="UserFriendlyInfo" id="UserFriendlyInfo">guest access mode</div>
    <div id="cwHeaderContainer" class="cwHeaderContainer"></div>
    <div class="cwAppLogoContainer">
        <span class="cwappTele">TELE</span><span class="cwappSales">SALES</span><span class="cwappTool">TOOL</span>
    </div>
    <div class="cwappVersion">V02.00.00</div>

    <div id="my_header" class="my_hdr">
        <div class="text"></div>
    </div>
    <div id="querycontainer"></div>
    <div id="formatResult"></div>
    <div class="bottomFooter">
        <div id="my_footer" class="my_ftr">
            <div class="text"><%=C_SYS_APP_FOOTER %><iframe id="callarea" name="callarea" src="blank.htm" width="1" height="1" style="visibility: hidden;"></iframe>
            </div>
        </div>
    </div>
</body>
</html>