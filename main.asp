﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
option explicit
Session.CodePage = 65001
Response.charset ="utf-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/common/variables.inc" -->
<!-- #include virtual ="classes/dal/clsMenu/clsMenu.asp" -->
<!-- #include virtual ="classes/utilities/json.asp" -->
<%
    if  request.Cookies("TELESALESTOOLV2")("usrId") = "" then response.redirect ("error.asp")
Dim LocaleJsFile, localeJsSchedFile, lang, userPicture, userFirstName
    userFirstName = request.Cookies ("TELESALESTOOLV2")("firstname")
    lang= request.Cookies ("TELESALESTOOLV2")("lang_radical")
    if len(trim(lang))= 0 then lang=C_SYS_DEFAULT_LANG
    LocaleJsFile="<script src=""locale/locale_" & lang & ".cw.js""></script>"
    localeJsSchedFile="<script src=""dhtmlx/dhtmlxScheduler/codebase/locale/locale_" & lang & ".js?v=2.0""></script>"
    userPicture = "<img class =cwprofilepicture src=""graphics/people/" & request.Cookies ("TELESALESTOOLV2")("picture") & """ />"
%>
<!DOCTYPE html>
<html>
<head>
    <title><%=C_SYS_APP_NAME %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="odometer/themes/odometer-theme-train-station.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <script src="scripts/jquery-3.1.1.js"></script>
    <link href="dhtmlx/dhtmlxSuite_v50_std/skins/skyblue/dhtmlx.css" rel="stylesheet" />
    <!-- <link href="dhtmlx/dhtmlxSuite_v50_std/codebase/dhtmlx.css" rel="stylesheet" /> -->
    <script src="dhtmlx/dhtmlxSuite_v50_std/codebase/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_agenda_view.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_collision.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_readonly.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_active_links.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_tooltip.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_minical.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_limit.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_editors.js"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_multiselect.js"></script>
    <link href="dhtmlx/dhtmlxScheduler/codebase/dhtmlxscheduler.css" rel="stylesheet" />
    <script src="scripts/main.asp.js?v=2.70" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.min.js"></script>
    <script src="odometer/odometer.js"></script>
    <script src="classes/dal/clsCounter/AjaxCounter.js?v=1"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/seedrandom/2.4.0/seedrandom.min.js">
    </script>
    <link rel="stylesheet" href="css/main.css?v=2.0" />
    <link rel="stylesheet" href="css/mail.css" />
    <link href="css/myScheduler.css" rel="stylesheet" />
    <%=LocaleJsFile%>
    <%=localeJsSchedFile %>
    <style>
        .odometer {
            font-size: 20px;
            background-image: url('graphics/odometer/preview_Grey-Pattern.jpg');
        }

        .dhx_cal_today_button, .dhx_cal_prev_button, .dhx_cal_next_button {
            display: block;
        }
        /* Important !!! */
        .dhx_scale_hour {
            line-height: normal;
        }

        .dhx_scale_holder {
            background-image: url('/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_62.png');
        }

        .dhx_scale_holder_now {
            background-image: url('/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_now_62.png');
        }

        div#objEmail {
            position: relative;
            width: 100%;
            height: 100%;
            overflow: auto;
        }

        .dhtmlx-salestoolmessage {
            font-weight: bold !important;
            color: white !important;
            background-color: #0094ff !important;
        }
    </style>
</head>
<body>
    <!-- friendly logon info initialization -->
    <script src="classes/dal/clsPerson/AjaxPerson.js?v=2"></script>
    <!-- User CallDetails info initialization -->
    <script src="classes/dal/clsCalls/AjaxCall.js?v=2"></script>
    <!-- HBC Company Details info -->
    <script src="classes/dal/clsBackOffice/AjaxBackOffice.js?v=2.2"></script>
    <!-- CW Company Details info -->
    <script src="classes/dal/clsCompanyWeb/AjaxCompanyWeb.js?v=2"></script>
    <!-- Leads Details info -->
    <script src="classes/dal/clsLeads/AjaxLeads.js?v=2"></script>
    <!-- Leads To Event Promo BusinessLayer -->
    <script src="classes/dal/clsEvtPromo/AjaxEvtPromo.js?v=2"></script>
    <!-- Company BusinessLayer -->
    <script src="classes/dal/clsCompany/AjaxCompany.js?v=2.2"></script>
    <!-- AJAXMailer BusinessLayer -->
    <script src="classes/dal/clsMailer/AjaxMailer.js?v=2.5"></script>
    <!-- AJAX Contact BusinessLayer -->
    <script src="classes/dal/clsContact/AjaxContact.js?v=2"></script>
    <!-- AJAX Order BusinessLayer -->
    <script src="classes/dal/clsOrders/AjaxOrder.js?v=2"></script>
    <!-- AJAX Competitors BusinessLayer -->
    <script src="classes/dal/clsCompetitors/AjaxCompetitor.js?v=2"></script>
    <!-- AJAX Software BusinessLayer -->
    <script src="classes/dal/clsSoftwares/AjaxSoftware.js?v=2"></script>
    <!-- AJAX Federations BusinessLayer -->
    <script src="classes/dal/clsFederations/AjaxFederation.js?v=2"></script>
    <!-- AJAX Software houses BusinessLayer -->
    <script src="classes/dal/clsSoftwareHouses/AjaxSoftwareHouse.js?v=2"></script>
    <script src="classes/dal/clsExamples/AjaxExamples.js"></script>
    <script src="classes/dal/clsCex/clsCex.js"></script>
    
    <!-- AJAX Mail Manager BusinessLayer -->
    <script src="scripts/mailmanager.js?v=2.20"></script>
    <!-- AJAX Document Manager BusinessLayer -->
    <script src="scripts/documentmanager.js?v=2.25"></script>
    <!-- AJAX Trial Manager BusinessLayer -->
    <script src="scripts/trialmanager.js?v=2.12"></script>
    <!-- AJAX Dirty Leads Manager BusinessLayer -->
    <script src="scripts/dirtyleadsmanager.js?v=2.25"></script>
	<!-- AJAX Meeting Room Manager BusinessLayer -->
	<script src="scripts/meetingroommanager.js?v=1"></script>
    <!-- AJAX Data Manager BusinessLayer -->
    <script src="scripts/datamanager.js?v=2"></script>
    <!-- AJAX Data Manager BusinessLayer -->
    <script src="scripts/mytelesales.js?v=2.1"></script>
    <!-- AJAX Search Manager BusinessLayer -->
    <script src="scripts/searchmanager.js?v=2.1"></script>
    <!-- AJAX Administration Manager BusinessLayer -->
    <script src="scripts/adminmanager.js?v=2"></script>
    <script src="scripts/examplesmanager.js"></script>
	<script src="scripts/lowusagemanager.js?v=1"></script>
    <script src="scripts/cancelledsubscriptionmanager.js?v=1"></script>
    <script src="scripts/soldsubscriptionmanager.js?v=1"></script>
    <!-- CW Company Details gauge score -->
    <script src="js/gauge.min.js?v=2"></script>
    <script src="cwgauge/cwgauge.js?v=2"></script>
    <div class="cwsalesPosition"></div>
    <div class="cwprofile" id="cwprofile"><%=userPicture%></div>
    <div class="salescounters" id="salescounters">
        Companyweb&nbsp;&nbsp;<div id="odglobal" class="odometer">9999</div>
        &nbsp; <%=userFirstName %>&nbsp;&nbsp;<div id="odindividual" class="odometer">9999</div>
    </div>
    <div class="UserDailyCallTime" id="UserDailyCallTime"></div>
	<div class="UserMeetingStatus" id="UserMeetingStatus"></div>
	<div class="UserFriendlyInfo" id="UserFriendlyInfo">hello logon</div>
    <div id="cwHeaderContainer" class="cwHeaderContainer"></div>
    <div class="cwAppLogoContainer">
        <span class="cwappTele">TELE</span><span class="cwappSales">SALES</span><span class="cwappTool">TOOL</span>
    </div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js" integrity="sha256-+q+dGCSrVbejd3MDuzJHKsk2eXd4sF5XYEMfPZsOnYE=" crossorigin="anonymous"></script>
    <div class="cwappVersion">V02.00.00</div>
    <div id="my_header" class="my_hdr">
        <div class="text"></div>
    </div>
    <div id="layoutObj"></div>

    <div id="my_footer" class="my_ftr">
        <div class="text">
            <%=C_SYS_APP_FOOTER %><iframe id="callarea" name="callarea" src="blank.htm" width="1" height="1" style="visibility: hidden;"></iframe>
        </div>
    </div>

    <script>
    $(window).unload(function(){
        doOnUnload();
    });

    $(function(){
		var viewParameter = GetURLParameter("view");
		
        loadView('1C', viewParameter);
    });
    $.ajaxSetup({
        crossDomain: true
    });

        //window position

        var myLayout;
        var clickedMenu = 0;
        var lastSelected;
        var viewsList;
		
		function GetURLParameter(sParam)
		{
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');

			for (var i = 0; i < sURLVariables.length; i++)
			{
				var sParameterName = sURLVariables[i].split('=');
				
				if (sParameterName[0] == sParam)
				{
					return sParameterName[1];
				}
			}
		}
		
        function loadView(view, id, listLeads) {
            if (lastSelected == view) return;

            if (myLayout != null) myLayout.unload();
            myLayout = new dhtmlXLayoutObject({parent: document.body, pattern: view});
            myLayout.attachHeader("my_header");
            myLayout.attachFooter("my_footer");
            myLayout.cells("a").hideHeader();
            lastSelected = view;
            myToolbar = myLayout.attachToolbar({
                icons_path: "/graphics/common/win_16x16/",

            });
            myToolbar.setAlign("right");
            myToolbar.loadStruct("../classes/dal/clsMenu/clsMenu.asp?a=DYNXML_GetToolBarParentMenu", function(){
							if (id != null) {
					view = myToolbar.getUserData(id, "pattern");
					JS_createinsidelayout(id, myLayout, view);
				}
				else {
					var lout = myLayout.cells("a").attachLayout({ pattern: "4T" });
					myTS_Initialize(lout);
					getTodayPie(lout.cells("b"));
					getTomorrowPie(lout.cells("c"));
					getThisWeekPie(lout.cells("d"));
				}

				myToolbar.attachEvent("onClick", function (id) {
					view = myToolbar.getUserData(id, "pattern");
					var menuurl= myToolbar.getUserData(id, "url");
					clickedMenu = id;

					if (view == null && menuurl == null)
					{
						var parentId = myToolbar.getParentId(id);
						view = myToolbar.getListOptionUserData(parentId, id, "pattern");

						menuurl = myToolbar.getListOptionUserData(parentId, id, "url");
					}

					//listLeads=myToolbar.getItemText(id);
					JS_createinsidelayout(id, myLayout, view);
				});
			});

            JSON_WS_GetCallDetails();
            SetPositionAndCounter();

            setInterval(function(){
                SetPositionAndCounter();
                JSON_WS_GetCallDetails();
				JSON_GetUserFriendlyInfo();
            }, 60000);

			JSON_GetUserFriendlyInfo();
        }

    function doOnUnload() {
        if (dhxWins != null && dhxWins.unload != null) {
            dhxWins.unload();
            dhxWins = null;
        }
    }
    </script>
</body>
</html>