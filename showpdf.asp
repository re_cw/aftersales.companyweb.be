<%

	filetype = Request.QueryString("type")
	file = Request.QueryString("file")

Const adTypeBinary = 1
Dim strFilePath

if lcase(filetype) = "f" then strFilePath = "Y:\Companyweb\Facturen\" & Replace(file,"/","\")
if lcase(filetype) = "c" then strFilePath = "Y:\Companyweb\CreditNotes\" & Replace(file,"/","\")
if lcase(filetype) = "bx" then strFilePath = server.mappath(Replace(file,"/","\"))

Set objFS = Server.CreateObject("scripting.Filesystemobject")

if objFS.FileExists(strFilePath) then

	Set objStream = Server.CreateObject("ADODB.Stream")
	objStream.Open
	objStream.Type = adTypeBinary
	objStream.LoadFromFile strFilePath
	if not objStream.eos then

	'On Error Resume Next
		If Not Response.isClientConnected Then
		    Response.end
		End If
		Response.buffer = true

		'Specify a MIME type such as "text/html", "image/gif" or "application/pdf"

		'Set the content type to the specific type that you are sending.
		Response.ContentType = "application/pdf"

		Response.BinaryWrite objStream.Read

	else
		RespStr = "File '" & File & "' Not available (" & FileType & ")"
		Response.Write RespStr
	end if
	objStream.Close
	Set objStream = Nothing

else

	RespStr = "File '" & File & "' Not available (" & FileType & ")"
	Response.Write RespStr

end if

Set objBinFile = Nothing
Response.End

%>