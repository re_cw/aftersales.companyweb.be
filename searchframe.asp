<%
	dim vConnStr
	vConnStr = "Provider=sqloledb;Data Source=10.81.15.3,1433;Network Library=DBMSSOCN;Initial Catalog=odmSearch_sub;User ID=odmTst ;Password=CMt3J8gy;"

    Set conn = server.CreateObject ("adodb.connection")
	conn.Open(vConnStr)

    intLang = 0

    dim strSearch(2)
    dim strBedrijf(2)

    strSearch(0) = "Zoek"
    strSearch(1) = "Rechercher"
    strBedrijf(0) = "Bedrijf"
    strBedrijf(1) = "Soci�t�"

    dim PSID
    dim check
    dim fname
    PSID= request.QueryString("PSID")
    check= request.QueryString("check")
    fname = request.QueryString("fname")
%>
<html>
<head>
    <title>Outcome Data Management</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/assets/build/yahoo-dom-event/yahoo-dom-event.js"></script>
    <script type="text/javascript" src="/assets/build/connection/connection-min.js"></script>
    <script type="text/javascript" src="/assets/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="/assets/build/datasource/datasource-min.js"></script>
    <script type="text/javascript" src="/assets/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript">
<!--
    var handleSuccess = function (o) {
        YAHOO.log("The success handler was called.  tId: " + o.tId + ".", "info", "example");
        if (o.responseText !== undefined) {

            if (!isNaN(o.responseText)) {
                setTimeout("makeRequest(2," + o.responseText + ");", 2000);
            }
            else {
                if (o.responseText == 'Fin') {
                    makeResultRequest(o.argument);
                }
                else {

                    document.getElementById("loading").innerHTML = o.responseText;

                }

            }
        }
    };

    var handleResultSuccess = function (o) {
        YAHOO.log("The success handler was called.  tId: " + o.tId + ".", "info", "example");
        if (o.responseText !== undefined) {

            document.getElementById("mc-results").setAttribute("class", "clearboth");
            document.getElementById("loading").innerHTML = o.responseText;

        }
    };
    var handleFailure = function (o) {
        YAHOO.log("The failure handler was called.  tId: " + o.tId + ".", "info", "example");

        if (o.responseText !== undefined) {

            document.getElementById("loading").innerHTML = '<div id="mssg" style="display: none;">Transaction id: ' + o.tId + '<br />';
            document.getElementById("loading").innerHTML += 'HTTP status: ' + o.status + '<br />';
            document.getElementById("loading").innerHTML += 'Status code message: ' + o.statusText + '<br />';
            document.getElementById("loading").innerHTML += 'Line: ' + o.responseText + '</div>';
        }
    };

    function makeRequest(Tpe, sid) {
        var callback =
	{
	    success: handleSuccess,
	    failure: handleFailure,
	    argument: [sid]
	};
        var sUrl = 'searchLoading.asp?ret=' + Tpe + '&sid=' + sid;
        var postData = '';
        if (Tpe == 1) {
            document.getElementById("mc-results").setAttribute("class", "leftalign smaller nodottedbottomline");
            document.getElementById("loading").innerHTML = '<img src="/assets/images/ajaxLoad.gif" align="center" alt="loading" />Loading Please Wait...';
        }
        YAHOO.util.Connect.setForm(document.dummy);
        var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);
    }

    function makeResultRequest(sid) {
        var callback =
	{
	    success: handleResultSuccess,
	    failure: handleFailure,
	    argument: [sid]
	};

        var sUrl = 'searchResult.asp?check=<%=check %>&PSID=<%=PSID %>&intLang=<%=intLang%>&seqid=' + sid;
        var request = YAHOO.util.Connect.asyncRequest('GET', sUrl, callback);

    }
    function setfName(fName) {
    }

    function clearfields() {
        document.getElementById("txtCompany").value = '';

    }

    function testEnterKey(evt) {
        if (evt.keyCode == 13) {
            evt.cancelBubble = true;
            evt.returnValue = false;
            makeRequest(1, 0);
        }
    }

    // -->
    </script>
</head>
<body>
    <table border="0" cellpadding="0" cellspacing="5" align="left" width="100%">
        <tr valign="top">
            <td class="nodottedbottomline">
                <form name="dummy" id="dummy" action="" class="nopadding nospacing">
                    <table border="0" class="leftalign nopadding nospacing" width="100%">
                        <tr>
                            <td class="nodottedbottomline "><%=strBedrijf(intlang) %>&nbsp;&nbsp;</td>
                            <td class="nodottedbottomline">
                                <input type="text" id="txtCompany" name="txtCompany" onkeypress="testEnterKey(event);" value="<%=fname %>" />
                            </td>
                            <td class="nodottedbottomline">&nbsp;&nbsp;</td>
                            <td class="nodottedbottomline">
                                <input type="button" id="btnSubmit" name="btnSubmit" value="<%=strSearch(intlang) %>" class="buttonbig buttonred" onclick="makeRequest(1, 0);" /></td>
                            <td class="error nodottedbottomline" width="40%"><%=vError %></td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
        <tr valign="top">
            <td class="nodottedbottomline">
                <table cellpadding="0" cellspacing="0" align="left" border="0" width="100%">
                    <tr valign="top">
                        <td class="text nodottedbottomline" id="mc-results">
                            <div id="loading"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
<% conn.close %>