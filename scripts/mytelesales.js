﻿function myTS_Initialize(myinsidelayout) {
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');

    var greeting = '<div class=myts_UserFriendlyInfo><br>' + getGreeting() + ' ' + salesfirstname + ', ' + locale.mytelesales.howareyou + ' ?</div>'
	
	myinsidelayout.cells("a").setText("Message");
    myinsidelayout.cells("a").attachHTMLString(greeting);
}

function getGreeting() {
    var x = parseInt(getCurrentHour());
    var gret = '';
    switch (true) {
        case (x < 12):
            gret = locale.mytelesales.goodmorning;
            break;
        case (x >= 12 && x <= 16):
            gret = locale.mytelesales.goodafternoon;
            break;
        case (x > 16):
            gret = locale.mytelesales.goodevening;
            break;
        default:
            gret = locale.mytelesales.goodmorning;
            break;
    }
    return gret;
}

function getCurrentHour() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                        + (currentdate.getMonth() + 1) + "/"
                        + currentdate.getFullYear() + " @ "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();
    return currentdate.getHours();
}

function getCurrenttimeStamp() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                        + (currentdate.getMonth() + 1) + "/"
                        + currentdate.getFullYear() + "  "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();
    return datetime;
}