﻿<%@ language="vbscript" %><%option explicit %>
<!-- #include virtual ="/classes/common/variables.inc" -->
<!-- #include virtual ="classes/dal/clsMenu/clsMenu.asp" -->
<% 
    if  request.Cookies("TELESALESTOOLV2")("usrId") = "" then response.redirect ("error.asp")
Response.CodePage = 65001    
Response.CharSet = "utf-8"  
Dim LocaleJsFile, localeJsSchedFile, lang, userPicture, userFirstName
    userFirstName = request.Cookies ("TELESALESTOOLV2")("firstname")
    lang= request.Cookies ("TELESALESTOOLV2")("lang_radical")
    if len(trim(lang))= 0 then lang=C_SYS_DEFAULT_LANG
    LocaleJsFile="<script src=""locale/locale_" & lang & ".cw.js?v=2.0""></script>"
    localeJsSchedFile="<script src=""dhtmlx/dhtmlxScheduler/codebase/locale/locale_" & lang & ".js?v=1.5""></script>"  
    userPicture = "<img class =cwprofilepicture src=""graphics/people/" & request.Cookies ("TELESALESTOOLV2")("picture") & """/>"
%>
<!DOCTYPE html>
<html>
<head><title><%=C_SYS_APP_NAME %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="odometer/themes/odometer-theme-train-station.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="dhtmlx/dhtmlxSuite_v46_std/codebase/dhtmlx.css" rel="stylesheet" />
    <script src="dhtmlx/dhtmlxSuite_v46_std/codebase/dhtmlx.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_week_agenda.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_agenda_view.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_collision.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_readonly.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_active_links.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_tooltip.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_minical.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_limit.js" type="text/javascript" charset="utf-8"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_editors.js"></script>
    <script src="dhtmlx/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_multiselect.js"></script>
    <link href="dhtmlx/dhtmlxScheduler/codebase/dhtmlxscheduler.css" rel="stylesheet" />
    <script src="scripts/main.asp.js?v=1.35" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.min.js"></script><script src="odometer/odometer.js"></script><script src="classes/dal/clsCounter/AjaxCounter.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/seedrandom/2.4.0/seedrandom.min.js">
	</script>
	<link rel="stylesheet" href="css/main.css?v=1.0" />
    <link rel="stylesheet" href="css/mail.css" />
    <link href="css/myScheduler.css" rel="stylesheet" />
    <%=LocaleJsFile%>
    <%=localeJsSchedFile %>
    <style>
        .odometer {font-size: 20px;background-image:url('graphics/odometer/preview_Grey-Pattern.jpg')}        
        .dhx_cal_today_button, .dhx_cal_prev_button, .dhx_cal_next_button {
            display: block;
        }
        /* Important !!! */
	    .dhx_scale_hour{ 
		line-height:normal;
	    }
        .dhx_scale_holder {
        background-image: url('/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_62.png');
        }
        .dhx_scale_holder_now {
        background-image: url('/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_now_62.png');
        }
        div#objEmail {
    position: relative;
    width: 100%;
    height: 100%;
    overflow: auto;
}
        
.dhtmlx-salestoolmessage{
    font-weight:bold !important;
    color:white !important;
    background-color:#0094ff !important;
}

    </style>
    <script type="text/javascript" charset="utf-8">
        
        //window position
        
        var myLayout;
        var clickedMenu = 0;
        var lastSelected;
        var viewsList;
        
       
        function loadView(view, id, listLeads) {


            if (lastSelected == view) return;
			
            if (myLayout != null) myLayout.unload();
            myLayout = new dhtmlXLayoutObject({parent: document.body, pattern: view});
            myLayout.attachHeader("my_header");
            myLayout.attachFooter("my_footer"); 
            myLayout.cells("a").hideHeader();
            lastSelected = view;
            myToolbar = myLayout.attachToolbar({
                icons_path: "/graphics/common/win_16x16/",
                
            });
            myToolbar.setAlign("right");
            myToolbar.loadStruct("../classes/dal/clsMenu/clsMenu.asp?a=DYNXML_GetToolBarParentMenu");
            //myToolbar.loadStruct("../classes/dal/clsMenu/testMenu.xml");
            //myToolbar.loadStruct("../classes/dal/clsMenu/buttons_select.json");

            if (id != null) {
                myLayout.cells("a").setText(listLeads);
                myLayout.cells("b").setText(locale.main_page.lblmyAgenda);
            }
            else {
                 
                myTS_Initialize(myLayout);
            }

            myToolbar.attachEvent("onClick", function (id) {       
                view = myToolbar.getUserData(id, "pattern");
                var menuurl= myToolbar.getUserData(id, "url");
                clickedMenu = id;
				
				if (view == null && menuurl == null)
				{
					var parentId = myToolbar.getParentId(id);
					view = myToolbar.getListOptionUserData(parentId, id, "pattern");
					
					menuurl = myToolbar.getListOptionUserData(parentId, id, "url");
				}
               
                //listLeads=myToolbar.getItemText(id);
                JS_createinsidelayout(id, myLayout, view);
            });         
        }

        
    function doOnUnload() {
        if (dhxWins != null && dhxWins.unload != null) {
            dhxWins.unload();
            dhxWins = null;
        }
    }
    </script>


</head>
<body onload="loadView('1C');"  onunload="doOnUnload();">
<!-- friendly logon info initialization -->
<script src="classes/dal/clsPerson/AjaxPerson.js?v=1"></script>
<!-- User CallDetails info initialization -->
<script src="classes/dal/clsCalls/AjaxCall.js?v=1"></script>
<!-- HBC Company Details info -->
<script src="classes/dal/clsBackOffice/AjaxBackOffice.js?v=1.65"></script>
<!-- CW Company Details info -->
<script src="classes/dal/clsCompanyWeb/AjaxCompanyWeb.js?v=1"></script>
<!-- Leads Details info -->
<script src="classes/dal/clsLeads/AjaxLeads.js?v=1"></script>
<!-- Leads To Event Promo BusinessLayer -->
<script src="classes/dal/clsEvtPromo/AjaxEvtPromo.js?v=1"></script>
<!-- Company BusinessLayer -->
<script src="classes/dal/clsCompany/AjaxCompany.js?v=1"></script>
<!-- AJAXMailer BusinessLayer -->
<script src="classes/dal/clsMailer/AjaxMailer.js?v=1"></script>
<!-- AJAX Contact BusinessLayer -->
<script src="classes/dal/clsContact/AjaxContact.js?v=1"></script>
<!-- AJAX Order BusinessLayer -->
<script src="classes/dal/clsOrders/AjaxOrder.js?v=1"></script>
<!-- AJAX Competitors BusinessLayer -->
<script src="classes/dal/clsCompetitors/AjaxCompetitor.js?v=1"></script>
<!-- AJAX Software BusinessLayer -->
<script src="classes/dal/clsSoftwares/AjaxSoftware.js?v=1"></script>
<!-- AJAX Federations BusinessLayer -->
<script src="classes/dal/clsFederations/AjaxFederation.js?v=1"></script>
<!-- AJAX Software houses BusinessLayer -->
<script src="classes/dal/clsSoftwareHouses/AjaxSoftwareHouse.js?v=1"></script>
<script src="classes/dal/clsExamples/AjaxExamples.js"></script>
<!-- AJAX Mail Manager BusinessLayer -->
<script src="scripts/mailmanager.js?v=1"></script>
<!-- AJAX Document Manager BusinessLayer -->
<script src="scripts/documentmanager.js?v=1.02"></script>
<!-- AJAX Trial Manager BusinessLayer -->
<script src="scripts/trialmanager.js?v=1.05"></script>
<!-- AJAX Dirty Leads Manager BusinessLayer -->
<script src="scripts/dirtyleadsmanager.js?v=1.25"></script>
<!-- AJAX Meeting Room Manager BusinessLayer -->
<script src="scripts/meetingroommanager.js?v=1"></script>
<!-- AJAX Data Manager BusinessLayer -->
<script src="scripts/datamanager.js?v=1"></script>
<!-- AJAX Data Manager BusinessLayer -->
<script src="scripts/mytelesales.js?v=1"></script>
<!-- AJAX Search Manager BusinessLayer -->
<script src="scripts/searchmanager.js?v=1"></script>
<!-- AJAX Administration Manager BusinessLayer -->
<script src="scripts/adminmanager.js?v=1"></script>
<script src="scripts/examplesmanager.js?v=0"></script>
<!-- CW Company Details gauge score -->
<script src="js/gauge.min.js?v=1"></script><script src="cwgauge/cwgauge.js?v=1"></script>

    <div class="cwsalesPosition"></div>
    <div class="cwprofile" id="cwprofile"><%=userPicture%></div>
    <div class ="salescounters" id="salescounters">Companyweb&nbsp;&nbsp;<div id="odglobal" class="odometer">9999</div> &nbsp; <%=userFirstName %>&nbsp;&nbsp;<div id="odindividual" class="odometer">9999</div></div>
    <div class="UserDailyCallTime" id="UserDailyCallTime"></div>
    <div class="UserFriendlyInfo" id="UserFriendlyInfo">hello logon</div>
    <div id="cwHeaderContainer" class="cwHeaderContainer"></div>
    <div class="cwAppLogoContainer">
            <span class="cwappTele">TELE</span><span class="cwappSales">SALES</span><span class="cwappTool">TOOL</span></div>
	        <div class="cwappVersion">V02.00.00</div>
    <div id="my_header" class="my_hdr"><div class="text"></div></div>
    <div id="layoutObj"></div>
    
    <div id="my_footer" class="my_ftr"><div class="text"><%=C_SYS_APP_FOOTER %><iframe id=callarea name=callarea src=blank.htm width=1 height=1  style=visibility:hidden;></iframe></div></div>
    

<script> 


    JSON_GetUserFriendlyInfo();
    setInterval (function(){
      JSON_WS_GetCallDetails()
      
        }, 62000);

    setTimeout(function(){
    setSalesPosition('cwsalesPosition');  
    SetCounter(1, 'odglobal');
      SetCounter(2, 'odindividual');
  }, 60000);
   
    setInterval (function(){
      SetCounter(1, 'odglobal');
      SetCounter(2, 'odindividual'); 
    setSalesPosition('cwsalesPosition'); 
        }, 61000);
             
</script>
  
</body>
</html>
