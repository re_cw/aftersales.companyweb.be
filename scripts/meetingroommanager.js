var insideLayout = '';
var trialCal = '';
var curDateObj = '';
var trialGrid = '';

function MR_InitializeMeetingRoomEngine(myinsidelayout) {
	EditMeetingRoomSettings();
}

function EditMeetingRoomSettings() {
	// var fullName = '';
	 var windowName = "Meeting Room";
	// var category = locale.main_page.editcurrent;
	var user_mr_name = "";
	var user_mr_sessionID = "";
	var user_mr_isNew = 0;

	 var objWindows;
	 var thisWindow;
	 var x = 20;
	 var y = 20;
	 var width = 640;
	 var height = 480;

    $.when(JSON_GetMeetingRoomSettingsForUser())
		.done(function (data) {
			if (data) {
				user_mr_name = data.name;
				user_mr_sessionID = data.sessionID;
				user_mr_isNew = data.isNew;
			} else {
				user_mr_name = "empty";
			}

			var headerText = 'Meeting room';
			var formStructure = '';

			// headerText = category + ' ' + fullName;
			objWindows.window(windowName).setText(headerText);

			formStructure = [
				{ type: "settings", position: "label-top", inputWidth: "auto", offsetLeft: 10 },
				{ type: "input", name: "user_mr_name", label: locale.main_page.lblmeetingname, value: user_mr_name, required: true, validate: "[A-Za-z ]+", inputWidth: 300 },
				{ type: "input", name: "user_mr_sessionID", label: locale.main_page.lblmeetingsession, value: user_mr_sessionID, required: true, validate: "[A-Za-z0-9 ]+", inputWidth: 300 },

				{ type: "hidden", name: "user_mr_isNew", value: user_mr_isNew, label: "user_mr_isNew", required: true },

				{ type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 100, inputTop: 390 },
				{ type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 200, inputTop: 390 }
			];

			var editForm = objWindows.window(windowName).attachForm(formStructure);

			editForm.attachEvent("onButtonClick", function (buttonid) {	
				
				user_mr_sessionID = cleanUpCallNumber(editForm.getItemValue("user_mr_sessionID"));
				if (user_mr_sessionID.length > 0) {
					user_mr_sessionID = checkSessionId(user_mr_sessionID);
					if (anyDigitAndLength(user_mr_sessionID) !== true) {
						dhtmlx.message({
							text: locale.popup.msgusedigitsonly,
							expire: -1,
							type: "errormessage"
						});
						dhtmlx.alert(anino + locale.popup.msgusedigitsonly);
						return false;
					}else{
						editForm.setItemValue("user_mr_sessionID", user_mr_sessionID);
					}
				}
				
				user_mr_name = editForm.getItemValue("user_mr_name");
				if (user_mr_name.length < 3) {
					dhtmlx.message({
						text: locale.popup.msgstringerror,
						expire: -1,
						type: "errormessage"
					});
					dhtmlx.alert(anino + locale.popup.msgstringerror);
					return false;
				}else{
					editForm.setItemValue("user_mr_name", user_mr_name);
				}

				var objMeetingRoom = {
					user_mr_name: editForm.getItemValue("user_mr_name"),
					user_mr_sessionID: editForm.getItemValue("user_mr_sessionID"),
					user_mr_isNew: editForm.getItemValue("user_mr_isNew"),
				};
				
				switch (buttonid) {
					case "btnsave":
						if (editForm.validate()) {
							if (user_mr_isNew == "0") {
								AjaxMR_EditMeetingRoom(objMeetingRoom);
							}
							else {
								AjaxMR_AddMeetingRoom(objMeetingRoom);					
							}
						}
						JSON_GetUserFriendlyInfo();
						thisWindow.close();	
						break;
					case "btncancel":
						thisWindow.close();
						break;
					default:
				}
			});
			 objWindows.window(windowName).progressOff();
		});

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(windowName, x, y, width, height);
	objWindows.attachViewportTo(document.body);
	objWindows.window(windowName).center();
	objWindows.window(windowName).progressOn();
}

function anyDigitAndLength(val) {
	return /^\d+$/.test(val) && (val.length > 8);
}

function checkSessionId(val){
	var n = val;
	n = n.replace(/ /g, '');
	n = n.replace(/-/g, '');
	n = n.replace(/\//g, '');
	n = n.replace(/\./g, '');	
	n = n.replace(/\-/g, '');
	return n;
}