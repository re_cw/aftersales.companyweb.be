var insideLayout = '';
var trialCal = '';
var curDateObj = '';
var trialGrid = '';

function TM_InitializeTrialEngine(myinsidelayout) {
    insidelayout = myinsidelayout;
    myinsidelayout.cells("a").setHeight(63);
    myinsidelayout.cells("a").fixSize(false, true);
    myinsidelayout.cells("a").hideHeader();
    myinsidelayout.cells("b").setText(locale.labels.icon_details);
    myinsidelayout.cells("c").setText(locale.main_page.lbltrialList);
    myinsidelayout.cells("a").showInnerScroll();
    myinsidelayout.cells("c").showInnerScroll();
    myinsidelayout.cells("a").attachHTMLString(InitializeQueryControls(myinsidelayout));
    trialGrid = myinsidelayout.cells("c").attachGrid();
    ConfigTrialGrid(trialGrid);
    GetTrials(trialGrid);
    InitializeTrialCalendar(myinsidelayout);
    //setComboTrialCompanies(trialGrid);
    //setComboTrialContacts(trialGrid);
}

function InitializeQueryControls(myinsidelayout) {
    var s;
    s = '';

    s += '<table width=100% style="background-color:#ffffd2;"><tr>'
    s += '<td>'
    s += '<p class="commontitle">' + locale.main_page.lblEntreprise + '</p><input type="text" name="companyFilter" value="" id="companyFilter" size=20>';
    s += '&nbsp;<input class=btnSendMail type=button id=btncompanyFilter, value="' + locale.main_page.lblFilter + '" onclick="FilterGridByCompany();">';
    s += '</td>'
    s += '<td>'
    s += '<p class="commontitle">' + locale.main_page.lblEventContact + '</p><input type="text" name="contactFilter" value="" id="contactFilter" size=20>';
    s += '&nbsp;<input class=btnSendMail type=button id=btncontactFilter, value="' + locale.main_page.lblFilter + '" onclick="FilterGridByContact();">';
    s += '</td>'
    s += '<td>'
    s += '<p class="commontitle">' + locale.main_page.lblDueDate + '</p><input type="text" name="endDateFilter" value="" id="endDateFilter" size=10>';
    s += '&nbsp;<input class=btnSendMail type=button id=btncontactFilter, value="' + locale.main_page.lblFilter + '" onclick="FilterGridByEndDate();">';
    s += '</td>'
    s += '</tr></table>'

    return s;
}
function FilterGridByCompany() {
    trialGrid.filterBy(1, document.getElementById('companyFilter').value);
}
function FilterGridByContact() {
    trialGrid.filterBy(2, document.getElementById('contactFilter').value);
}
function FilterGridByEndDate() {
    trialGrid.filterBy(4, document.getElementById('endDateFilter').value);
}

function resetGrid() {
    GetTrials(trialGrid);
}

function InitializeTrialCalendar(myinsidelayout) {
    trialCal = new dhtmlXCalendarObject("trialCalDiv");
    trialCal.setDateFormat("%Y%m%d");
    trialCal.show();
    trialCal.showToday();
    trialCal.showWeekNumbers();
    trialCal.hideTime();
    curDateObj = trialCal.getDate();

    trialCal.attachEvent("onClick", function (date) {
        dhtmlx.message.position = "bottom";
        //dhtmlx.message(GetSalesToolDateFormat(date));
        dhtmlx.message({
            type: "errormessage",
            text: GetSalesToolDateFormat(date),
            expire: 1000
        })
    });
}

function ConfigTrialGrid(trialGrid) {
    var tbHeader = 'id,' + locale.main_page.lblEntreprise + ',' + locale.main_page.lblEventContact + ',' + locale.main_page.lblhits + ',' + locale.main_page.lblDueDate
    trialGrid.setColumnHidden(0, true);
    trialGrid.setHeader(tbHeader);
    trialGrid.setInitWidths("30,220, 150,50,80");
    trialGrid.setColAlign("left,left,left,left,left");
    trialGrid.setColTypes("ro,ro,ro,ro,ro");
    trialGrid.setColSorting("str,str,str,str,str");
    trialGrid.init();
    trialGrid.attachEvent("onRowSelect", doOnRowSelect);
}
function displayTrialCompanyBusinessData(hbc_id, container) {
	
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		$("#" + container).empty();
		
		var company_id = $('#company_id').val();
		var company_name = $('#company_name').text();
		
		var imgEdit = '<img src ="/graphics/common/win_16x16/edit.png" class="onmouseoveredit" onClick="submitItem(category);">';
		var imgItem = '<img src ="/graphics/common/win_16x16/add.png" class="onmouseoveradd" onClick="editItem('+company_id+',\''+company_name +'\',category);">';
	
		var it1 = '<table class="innertblbusinessdata">';
		it1 += '<tr><td class="header">' + locale.main_page.lblCompetitor + '</td><td align=imgItem>' + imgEdit.replace("category", "'competitor'") + '</td></tr>';
		it1 += '<tr><td><div id="combo_competitors" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'competitor'") + '</td></tr>';
		it1 += '<tr><td colspan=2><div id="itCompetition" name="competitors">competitor list</div></td><td align=imgItem></td></tr>';
		it1 += '</table>';
	
		var it2 = '<table class="innertblbusinessdata">';
		it2 += '<tr><td class="header">' + locale.main_page.lblSoftware + '</td><td align=left>' + imgEdit.replace("category", "'software'") + '</td></tr>';
		it2 += '<tr><td ><div id="combo_softwares" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'software'") + '</td></tr>';
		it2 += '<tr><td colspan=2><div id="itsoftware" name="softwares">software list</div></td></tr>';
		it2 += '</table>';
	
		var it3 = '<table class="innertblbusinessdata">';
		it3 += '<tr><td class="header">' + locale.main_page.lblfederation + '</td><td align=left>' + imgEdit.replace("category", "'federation'") + '</td></tr>';
		it3 += '<tr><td><div id="combo_federations" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'federation'") + '</td></tr>';
		it3 += '<tr><td colspan=2><div id="itfederation" name="federation">federation list</div></td></tr>';
		it3 += '</table>';
	
		var it4 = '<table class="innertblbusinessdata">';
		it4 += '<tr><td class="header">' + locale.main_page.lblsoftwarehouse + '</td><td align=left>' + imgEdit.replace("category", "'softwarehouse'") + '</td></tr>';
		it4 += '<tr><td><div id="combo_softwarehouse" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'softwarehouse'") + '</td></tr>';
		it4 += '<tr><td colspan=2><div id="itsoftwarehouse" name="softwarehouse"></div></td></tr>';
		it4 += '</table>';
	
		var t = '<table class="tblbusinessdata" border=0>';
		t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEventBusinessData + '</td></tr><tr>';
		t += '<td class=tdtop>' + it1 + '</td>';
		t += '<td class=tdtop>' + it2 + '</td></tr>';
		t += '<tr><td class=tdtop>' + it3 + '</td>';
		t += '<td class=tdtop>' + it4 + '</td>';
		t += '</tr></table>';
	
		$("#" + container).html(t);
	
		
		$.when(
			AjaxCompetitor_GetCompetitorsListFromHbcId("itCompetition", hbc_id),
			AjaxSoftware_GetSoftwaresListFromHbcId("itsoftware", hbc_id),
			AjaxFederation_GetFederationsListFromHbcId("itfederation", hbc_id),
			AjaxSoftwareHouse_GetSoftwareHousesListFromHbcId("itsoftwarehouse", hbc_id)
		).always(function(){
			$("#" + container).data('requestRunning', false);
		});
	
		setComboCompetitors();
		setComboSoftwares();
		setComboFederations();
		setComboSoftwareHouses();
			
	}
	return $.when(null);
}

function displayEmailAddressesPromise(hbc_id, container) {
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		$("#" + container).empty();
		
		var url = "/classes/dal/clsContact/clsContact.asp";
		return $.get(url, { a: "JSONGetStContactDetailsFromHbcId", hbc_id: hbc_id })
		.done(function (data) {
			$("#" + container).html(formatTrialContactDetails(data));
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Display EmailAddresses",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning', false);
		});
		
	}
	return $.when(null);
}

function formatTrialContactDetails(obj) {
    var spacer, remove, contact, mailsend, phoneurl, phonenr, mobileurl, mobilenr, purchaseorder, abo, bestelbon;

    var t = '';
    t = "<table cellpadding=1 width=100% border=0>";
    t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEventContact + '</td></tr>';

    if (!$.isEmptyObject(obj)) { 
        for (var i = 0; i < obj.length; i++) {
            spacer = '&nbsp;&nbsp;'
            remove = '<img class =onmouseoverdel src="/graphics/common/win_16x16/male-user-remove.png" onClick="DeleteContact(' + obj[i].contact_id + ');">';
            contact = '<img class =onmouseoveredit src="/graphics/common/win_16x16/male-user-edit.png" onClick="EditContact(' + obj[i].contact_id + ');">';
            mailsend = '<img class =onmouseovermail src="/graphics/common/win_16x16/email_16.png" onClick="MailContact(\'' + obj[i].contact_id + '\', \'' + obj[i].company_id + '\');">';
            phonenr = "'" + obj[i].contact_tel + "'";
            mobilenr = "'" + obj[i].contact_mobile + "'";
            phoneurl = '<img class =onmouseoverphone src="/graphics/common/win_16x16/Phone-icon16.png" onClick="JS_EventCall(' + phonenr + ');">';
            mobileurl = '<img class =onmouseovermobile src="/graphics/common/win_16x16/mobile16.png" onClick="JS_EventCall(' + mobilenr + ');">';
            abo = '<img id="eurotoken" class =onmouseovereuro src="/graphics/common/win_16x16/Activation16.png" onclick="displayPurchaseOrderWindow(' + i + ',' + controlsource.button.activation + ');">';;
            bestelbon = '<img id="bestelbon" class =onmouseovermail src="/graphics/common/win_16x16/purchase_order_16.png" onclick="displayPurchaseOrderWindow(' + i + ',' + controlsource.button.offre + ');">';;

            t += "<tr><td width=85% class=eventContactHeader>" + obj[i].contact_title + " " + obj[i].contact_firstname + " " + obj[i].contact_lastname + ", " + obj[i].contact_type + " (" + obj[i].lang_name + ")</td>";
            t += "<tr><td><table width=100% border=0>"
            t += "<tr class=CwCyInfo><td class=CwCylblInfo width=50px>" + phoneurl + locale.main_page.lblTel + "</td><td class=STInfo width=100px>" + obj[i].contact_tel + "</td>";
            t += "<td class=CwCylblInfo width= width=50px>" + mobileurl + locale.main_page.lblGsm + "</td><td class=STInfo width=100px>" + obj[i].contact_mobile + "</td>";
            t += "<td class=CwCylblInfo width= width=50px>" + mailsend + locale.main_page.lblEmail + "</td><td class=STInfo width=200px>" + obj[i].contact_email + "</td></tr>";
            t += "</table>"
        }
        
    }
    t += "</table>";
    return t;
}
function displayTrialCompanyDetailsPromise(hbc_id, container) {
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		$("#" + container).empty();
		
		var url = "/classes/dal/clsCompany/clsCompany.asp";
		return $.get(url, { a: "JSONGetCompanyInfoHbc", company_hbc_id: hbc_id })
		.done(function (data) {
			$("#" + container).html(formatTrialCompanyDetails(data));
		}).fail(function(resp){
			dhtmlx.message({
				text: "Error in TrialCompany Details",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning', false);
		});
	}
	return $.when(null);
}

function formatTrialCompanyDetails(objCy)
{
    var iframecall = '<iframe id="callarea" name="callarea" src="blank.htm" width="1" height="1" scrolling="no" frameborder="0" style="visibility:hidden;"></iframe>'
    var postcodenr = "'" + objCy.company_address_postcode + "'";
    var postcode = '<img class=onmouseoverlookup src="/graphics/common/win_32x32/bpost32.png" onClick="displayCompaniesbyPostalCode(' + postcodenr + ');">';
    var phonenr = "'" + objCy.company_tel + "'";
    var phoneurl = '<img class=onmouseoverphone src="/graphics/common/win_32x32/Phone-icon.png" onClick="JS_EventCall(' + phonenr + ');">';
    var mobilenr = "'" + objCy.company_mobile + "'";
    var mobileurl = '<img class=onmouseovermobile src="/graphics/common/win_32x32/mobile32.png" onClick="JS_EventCall(' + mobilenr + ');">';
    var company_id = "'" + objCy.company_id + "'";
    var company_vat = "'" + objCy.company_vat + "'";
    var cwnfo = '<img id="cwnfo" class=onmouseovercwnfo src="/graphics/common/win_32x32/cw32.png" onclick="cwnavigate(' + company_vat + ');">';
    var company_name = objCy.company_name;
    company_name = company_name.replace(/ /g, '+');
    var webnfo = '<img id=cwnfo class=onmouseoverlookup src=/graphics/common/win_32x32/searchglobe.png onclick=webnavigate("' + company_name + '");>';

    var stToolBox = webnfo + '&nbsp;&nbsp;' + cwnfo + '&nbsp;&nbsp;' + postcode + '&nbsp;&nbsp;' + mobileurl + '&nbsp;&nbsp;' + phoneurl;

    var t = ''; //'<div id=objCytoolBox class=objCytoolBox>' + companyedit + '&nbsp;&nbsp;' + webnfo + '&nbsp;&nbsp;' + cwnfo + '&nbsp;&nbsp;' + postcode + '&nbsp;&nbsp;' + mobileurl + '&nbsp;&nbsp;' + phoneurl + '&nbsp;&nbsp;' + abo + '&nbsp;&nbsp;' + contact + '</div>';
	t += "<input type='hidden' name='company_id' id='company_id' value='" + objCy.company_id + "' />";
    t += "<input type='hidden' name='company_vat' id='company_vat' value='" + objCy.company_vat + "' />";
    t += "<table cellpadding=2 width=100% border=0>";
    t += '<tr><td colspan=2 align=right>' + stToolBox + '</td></tr>';
    t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEntreprise + '</td></tr>';
    t += "<tr><td colspan=2><span class=eventCompanyName id=eventTitle><span id='company_name' name='company_name'>" + objCy.company_name + "</span> " + objCy.jfc_s_desc + "(" + objCy.lang_name + ")<span style='float:right'><input type='button' value='...' onClick='UpdateWithCompanyInfo(" + objCy.company_vat + ")' /></span></td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" + locale.main_page.lblCompanyStatus + "</td><td class=STInfo><span id='state'></span></td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" + locale.main_page.lblNoEntreprise + "</td><td class=STInfo>" + objCy.company_vat + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblAdress + "</td><td class=STInfo>" + objCy.company_address_street + ", " + objCy.company_address_number + " " + objCy.company_address_boxnumber + " - " + objCy.company_address_postcode + " " + objCy.company_address_localite + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblActivity + "</td><td class=STInfo>" + objCy.nbc_desc + "</td></tr>"
    t += "<tr><td colspan=2>"
    t += "<table width=100%><tr class=CwCyInfo><td class=CwCylblInfo width=16%>" + locale.main_page.lblTel + "</td><td class=STInfo width=16%>" + objCy.company_tel + "</td>";
    t += "<td class=CwCylblInfo width=16%>" + locale.main_page.lblGsm + "</td><td class=STInfo width=16%>" + objCy.company_mobile + "</td>";
    t += "<td class=CwCylblInfo width=16%>" + locale.main_page.lblEmail + "</td><td class=STInfo width=16%>" + objCy.company_mail + "</td></tr></table>";
    t += "</td></tr>";
    t += "</table>";
    t += iframecall;
    return t;
}
function doOnRowSelect(id, ind) {
    //dhtmlx.alert(imgalertsaveok3 + hbc_id);
    evtAcc.cells("stCompany").progressOn();

    

    $.when(
		displayTrialCompanyDetailsPromise(id, "stCompany"),
		displayEmailAddressesPromise(id, "stContact"),
		displayTrialEventDetailsPromise(id, "eventHistory")
	)
    .done(function () {
		displayTrialCompanyBusinessData(id, "stBusinessData")
		.done(function(){
			evtAcc.cells("stCompany").progressOff();
		});
        
    });

    
    //BackOffice accordion pane
    evtAcc.cells("stBo").progressOn();
    $.when(displayTrialCompanyBackOfficeDetails(id, "stBackOffice", "stBackOfficeHistory"))
    .done(function (data) {
        evtAcc.cells("stBo").progressOff();
    });

    //st info
    displayTrialCompanyCWDetails(id, "stCwInfo");
	
    evtAcc.progressOff();
	
	evtAcc.cells("stCex").progressOn();
		GetCexHistoryPromise(id)
        .done(function (objHi) {
            evtAcc.cells("stCex").attachHTMLString(FormatCexHistory(objHi));
            GetCexChartPromise(id)
            .done(function (data) {
				if (!$.isEmptyObject(data)){
					GetChart("cexChart", formatJSONChart(data));
				}else{
					$("#cexChart").text("NO DATA AVAILABLE");
				}
            });
			GetCexExperiencePromise(id)
			.done(function(data)
			{
				if (!$.isEmptyObject(data)){
					GetCexAbsoluteScore("cexAbsolute", formatAbsoluteNumbers(data));
					GetExperienceScore("cexExperience", formatJSONExperience(data));
				}else{
					$("#cexAbsolute").text("NO DATA AVAILABLE");
					$("#cexExperience").text("NO DATA AVAILABLE");
				}
				
			});
			
        }).always(function () {
            evtAcc.cells("stCex").progressOff();
        });
    return true;
}
function displayTrialEventDetailsPromise(hbc_id, container) {
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		$("#" + container).empty();
		var url = "/classes/dal/clsScheduler/events.asp";
	
		return $.get(url, { a: "GetEventHistoryFromHbcId", hbc_id: hbc_id })
		.done(function (data) {
			$("#" + container).html(formatEventDetails(data));
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Trial Event Details",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning', false);
		});
	}
	return $.when(null);
}
function displayTrialCompanyBackOfficeDetails(hbc_id, container, container2) {		
	if (hbc_id !== undefined){
		$.when(
			JSONGetCompanyVatFromBackOfficeId(hbc_id)
		)
		.done(function (data) {
			if (!$("#" + container).data('requestRunning')){
				$("#" + container).data('requestRunning', true);
				
				
				$("#stBackOffice").empty();
				JSON_displayStCompanyBackOfficeDetails(data.vat, hbc_id, container)
				.always(function(data)
				{
				    
					$("#" + container).data('requestRunning', false)
				});
			}
			
				
			$("#stBackOfficeUserInfo").empty();
				JSONGetCompanyBackOfficeUserInfoPromise(data.vat, hbc_id, "stBackOfficeUserInfo")
				.always(function () {
				    
					$('#stBackOfficeUserInfo').data('requestRunning', true);
				});
			
							
				$("#stBackOfficeUserBillingInfo").empty();
				JSONGetCompanyBackOfficeUserBillingInfoPromise(data.vat, hbc_id, "stBackOfficeUserBillingInfo")
				.always(function()
				{
				   
				});
				
				$("#stBackOfficeHistory").empty();
				
				JSON_displayStCompanyBackOfficeHistory(data.vat, hbc_id, container2);
				$("#" + container2).data('requestRunning', false);
			
		});
	}
}
function displayTrialCompanyCWDetails(hbc_id, container) {
	if (!$('#state').data('requestRunning')){
		$('#state').data('requestRunning', true);
		
		JSON_GetCompanyCompanyWebStateFromHbcId(hbc_id, "state")
		.always(function(){
			$('#state').data('requestRunning', false);
		});
	}

	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		
		$("#" + container).empty();
		var strCWCompanyDetails = "<div id=CWCompanyDetails" + hbc_id + ">" + hbc_id + "</div>"
		strCWCompanyDetails = strCWCompanyDetails + "<div class=CWCyBarometer><canvas id=g" + hbc_id + "></canvas></div><div id=gv" + hbc_id + " style='visibility:hidden;position: absolute;'></div>"
		strCWCompanyDetails = strCWCompanyDetails + "<div id=CWCompanyMandates" + hbc_id + "></div>"

		$("#" + container).html(strCWCompanyDetails);

		$.when(
			JSON_GetCWCompanyDetailsFromHbcIdPromise(hbc_id),
			JSON_GetCWCompanyMandatesFromHbcIdPromise(hbc_id)
		).done(function () {
			showGauge($("#gv" + hbc_id).html(), "g" + hbc_id);
		}).always(function()
		{
			$("#" + container).data('requestRunning', false);
		});
	}
}

function GetTrials(trialGrid) {
    trialGrid.clearAll();
    Ajax_GetTrials();
}

function Ajax_GetExpiredTrials() {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    return $.get(url, { q : 95})
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error while fetching expired trials:<br/>" + resp.responseText,
            expire: -1,
            type: "errormessage"
        });
    });
}
function Ajax_GetTrials() {
    dhtmlx.message({
        text: "Fetching Trials",
        expire: 500,
        type: "noticemessage"
    });

    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    $.get(url, { q: 15 })
    .done(function (data) {
        if (data.rows.length > 0) {
            trialGrid.parse(data, "json");
            Ajax_GetExpiredTrials()
            .done(function (expiredTrials) {
                if (!$.isEmptyObject(expiredTrials))
                {
                    for (var i = 0; i < expiredTrials.length; i++) {
                        trialGrid.setRowTextStyle(expiredTrials[i].ID, "opacity: 0.5");
                    }
                }
            });

        }
    }).fail(function (resp) {
        dhtmlx.message({
            text: "Error while fetching trials:<br/>" + resp.responseText,
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetTrialsFiltered() {
    dhtmlx.message({
        text: "Fetching Filtered Trials",
        expire: 500,
        type: "noticemessage"
    });

    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    $.get(url, { q: 15 })
    .done(function (data) {
        if (data.rows.length > 0) {
            trialGrid.parse(data, "json");
        }
    }).fail(function (resp) {
        dhtmlx.message({
            text: "Error while Fetching Filtered Trials<br/>" + resp.responseText,
            expire: -1,
            type: "errormessage"
        });
    });
}