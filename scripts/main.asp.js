/*
var varGlobalMailTo = 'companyweb.daniel@gmail.com'
var varGlobalMailCC = 'companyweb.daniel@gmail.com';
var varGlobalMailBcc = 'companyweb.daniel@gmail.com';
*/

var varGlobalMailTo = 'sales@companyweb.be';
var varGlobalMailCC = 'sales@companyweb.be';
var varGlobalMailBcc = 'ccuser@companyweb.be;hdemeulder@xtradev.com;ilse@companyweb.be;ranu@companyweb.be';
var confirmationbox = false;

var urlCORS = "http://cors.io/?u=http://aftersales.companyweb.be";

var imgalertok = '<img src=/graphics/common/win_32x32/Gnome-Face-Smile-Big-32.png>';
var imgalertnok = '<img src=/graphics/common/win_32x32/Gnome-Face-Crying-32.png>';

var imgalertsaveok = '<img src=/graphics/common/win_32x32/Gnome-Face-Cool-32.png>';
var imgalertsavenok = '<img src=/graphics/common/win_32x32/Gnome-Face-Sad-32.png>';

var imgalertsaveok2 = '<img src=/graphics/common/win_32x32/Gnome-Face-Kiss-32.png>';
var imgalertsavenok2 = '<img src=/graphics/common/win_32x32/Gnome-Face-Surprise-32.png>';

var imgalertsaveok3 = '<img src=/graphics/common/win_32x32/Gnome-Face-Devilish-32.png>';
var imgalertsavenok3 = '<img src=/graphics/common/win_32x32/Gnome-Face-Angel-32.png>';

var imgconfirm1 = '<img src=/graphics/common/win_32x32/Gnome-Face-Monkey-32.png>';

var anino = '<img src=/graphics/animated/no.gif>';

var tCustomObjectsLevel = 4;
var maintoolbar = {
	menu: { leads: "1", events: "2", agenda: "3", search: "4", mailsearch: "18", documentmanagement: "7", disconnect: "8", contact: "11", history: "12", searchcw: "13", searchinternet: "14", mytrials: "20", dirtyleads: "21", linkBackOffice: "23", admin: "6", mailtemplates: "19", examples: "26", lowusage: "28", cancelledSubscription: "30", soldsubscription: "31", settings : "32", MRstatus : "33" }
};
var wintoolbar = {
	menu: { leadcall: "15", leadtransfer: "16", leadcomment: "17", noaction: "24", quickedit: "25" }
};

var controlsource = {
	button: { activation: 1, offre: 2 }
};

var x = 150;
var y = 80;
var dhxWins;

var input_from;
var input_till;
var myCalendar;
var myScheduler;
var load_url;
var cbxEvtTyp;

var objEvent;
var evtAcc;

var myinsidelayout;

function JS_createinsidelayout(id, myLayout, view) {
	
	if(id != maintoolbar.menu.MRstatus && id != maintoolbar.menu.settings)
	{
		var myDataView, mydataset;
		myinsidelayout = myLayout.cells("a").attachLayout({ pattern: view });

		QuarterTimeStep();

		JS_InitializeAgenda();
	}

	switch (id) {
		case maintoolbar.menu.leads:
			// Lead Object modal window
			dhxWins = new dhtmlXWindows();
			dhxWins.attachViewportTo(document.body);

			myinsidelayout.cells("a").setText(myToolbar.getItemText(id));
			myinsidelayout.cells("b").setText(locale.main_page.lblmyAgenda);
			myinsidelayout.cells("a").setWidth(300);
			myinsidelayout.cells("b").setWidth(970);

			JS_InitializeLeadToolbar(myinsidelayout);
			JS_createJSONLeads_dataview(myinsidelayout, "a");
			JS_createAgenda(myinsidelayout, "b");
			break;

		case maintoolbar.menu.events:
			scheduler.destroyCalendar();
			myinsidelayout.cells("a").setText(myToolbar.getItemText(id));
			myinsidelayout.cells("b").setText(locale.main_page.lblEventDetail);
			myinsidelayout.cells("a").setWidth(500);
			myinsidelayout.cells("b").setWidth(770);
			JS_DisplayOpenTasks(myinsidelayout, "a");

			JS_createTaskDetailsLayout(myinsidelayout, "b");

			break;

		case maintoolbar.menu.agenda:
			scheduler.destroyCalendar();
			myinsidelayout.cells("a").setText(myToolbar.getItemText(id));
			myinsidelayout.cells("b").collapse();
			myinsidelayout.cells("b").setText(locale.main_page.lblEventDetail);
			JS_createAgenda(myinsidelayout, "a");
			JS_GetAgendaTask(myinsidelayout, "b");
			JS_createTaskDetailsLayout(myinsidelayout, "b");

			break;

		case maintoolbar.menu.search:
			SE_InitializeSearchEngine(myinsidelayout, id);
			break;

		case maintoolbar.menu.documentmanagement:
			myinsidelayout.cells("a").setText(locale.main_page.lblEmailOptions);
			myinsidelayout.cells("a").attachHTMLString(MM_DocumentEngineSetLayout(-1));
			myinsidelayout.cells("a").showInnerScroll();
			MM_DocumentEngineInitialize(myinsidelayout);
			break;

		case maintoolbar.menu.mailsearch:
			MM_InitializeMailSearchEngine(myinsidelayout);

			break;

		case maintoolbar.menu.mytrials:
			TM_InitializeTrialEngine(myinsidelayout);
			//JS_GetTrialTask(myinsidelayout, "b");
			JS_createTaskDetailsLayout(myinsidelayout, "b");
			break;

		case maintoolbar.menu.dirtyleads:
			QM_InitializeDirtyLeadsEngine(myinsidelayout);
			break;
		case maintoolbar.menu.admin:
			ADM_InitializeAdminEngine(myinsidelayout);
			break;

		case maintoolbar.menu.linkBackOffice:
			myinsidelayout = myLayout.cells("a").attachLayout({ pattern: '2U' });
			myinsidelayout.cells("a").showInnerScroll();
			myinsidelayout.cells("b").showInnerScroll();
			myinsidelayout.cells("b").setWidth(200);
			DataM_Initialize(myinsidelayout);
			break;

		case maintoolbar.menu.disconnect:
			document.cookie = 'TELESALESTOOLV2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			window.location = '/login.asp?a=-1';
			break;
		case maintoolbar.menu.mailtemplates:
			myinsidelayout.cells("a").setText(locale.main_page.lblEmailOptions);
			myinsidelayout.cells("a").attachHTMLString(MM_DocumentTemplateEngineSetLayout());
			myinsidelayout.cells("a").showInnerScroll();
			MM_DocumentTemplateEngineInitialize(myinsidelayout);
			break;
		case maintoolbar.menu.examples:
			MM_ExamplesEngineInitialize(myinsidelayout);

			break;
		case maintoolbar.menu.lowusage:
			MM_LowUsageEngineInitialize(myinsidelayout);

			break;
	    case maintoolbar.menu.cancelledSubscription:
	        MM_CancelledEngineInitialize(myinsidelayout);
	        break;
		case maintoolbar.menu.soldsubscription:
			MM_SoldSubscriptionEngineInitialize(myinsidelayout);
			break;
		case maintoolbar.menu.settings:
			MR_InitializeMeetingRoomEngine(myinsidelayout);
			break;
		case maintoolbar.menu.MRstatus:
			AjaxMR_ChangeMeetingRoomStatus();
			JSON_GetUserFriendlyInfo();
			break;
		default:
			break;
	}
}

// GLOBAL SETTINGS FOR SCHEDULER HOUR SCALE
function switchView() {
	// add this into the div section of calendar
	//'<input type="button" name="btnswitchView" id="btnswitchView" value="30 minutes" onclick="switchView()" class="dhx_cal_tab"  style = "position:absolute; left:295px; top:15px; padding:1px;width:100px">';
	if (document.getElementById("btnswitchView").value === locale.main_page.btnswitchview30) {
		HalfHourTimeStep();
		document.getElementById("btnswitchView").value = locale.main_page.btnswitchview15;
	}
	else {
		QuarterTimeStep();
		document.getElementById("btnswitchView").value = locale.main_page.btnswitchview30;
	}
	scheduler.updateView();
}

function UpdateAgendaBackground(step) {
	if (step === 15) {
		var d = document.getElementsByClassName("dhx_scale_holder");
		for (var index = 0; index < d.length; index++) {
			d[index].style.backgroundImage = "url(/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_31.png)";
		}

		var e = document.getElementsByClassName("dhx_scale_holder_now");
		for (var index = 0; index < e.length; index++) {
			e[index].style.backgroundImage = "url(/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_31.png)";
		}
	}
	else {
		var d = document.getElementsByClassName("dhx_scale_holder");
		for (var index = 0; index < d.length; index++) {
			d[index].style.backgroundImage = "url(/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_62.png)";
		}

		var e = document.getElementsByClassName("dhx_scale_holder_now");
		for (var index = 0; index < e.length; index++) {
			e[index].style.backgroundImage = "url(/dhtmlx/dhtmlxScheduler/codebase/imgs_glossy/databg_62.png)";
		}
	}
}

function QuarterTimeStep() {
	var step = 15;
	var format = scheduler.date.date_to_str("%H:%i");

	//scheduler.config.hour_size_px = (60 / step) * 22;
	scheduler.xy.min_event_height = step;
	scheduler.templates.hour_scale = function (date) {
		html = "";
		for (var i = 0; i < 60 / step; i++) {
			html += "<div style='height:16px;line-height:15px;'>" + format(date) + "</div>";
			date = scheduler.date.add(date, step, "minute");
		}
		UpdateAgendaBackground(step);
		return html;
	};
}

function HalfHourTimeStep() {
	//this customize the display for hours bigger height
	var format = scheduler.date.date_to_str("%H:%i");
	var step = 30;

	scheduler.xy.min_event_height = 30;// 30 minutes is the shortest duration to be displayed as is
	scheduler.templates.hour_scale = function (date) {
		html = "";
		for (var i = 0; i < 60 / step; i++) {
			html += "<div style='height:32px;line-height:21px;'>" + format(date) + "</div>";
			date = scheduler.date.add(date, step, "minute");
		}
		UpdateAgendaBackground(step);
		return html;
	};
}

function JS_InitializeAgenda() {
	scheduler.config.multi_day = true;
	scheduler.config.xml_date = "%Y-%m-%d %H:%i";
	scheduler.config.resize_month_events = true;
	scheduler.config.prevent_cache = true;
	scheduler.config.start_on_monday = true;
	scheduler.config.first_hour = 8;
	scheduler.config.last_hour = 19;
	scheduler.config.limit_time_select = true;
	scheduler.config.details_on_dblclick = true; //opens the lightbox on dbl click
	scheduler.config.details_on_create = false; //uses extended form while creating new events
	scheduler.config.select = true; //disable the small left select bar in the event's box
	scheduler.config.dblclick_create = false;   //disable event creation on dbl click
	scheduler.config.active_link_view = "day"; //dbl click on month' day opens the clicked day
	scheduler.config.max_month_events = 8; //view more link in month view
	scheduler.xy.bar_height = 12; //set spaces between events in month view
	scheduler.config.mark_now = true; // set the current time red line in week/day view
	scheduler.config.event_duration = 1;
	scheduler.init.auto_end_date = true;

	scheduler.config.collision_limit = 2; // so others can get there as well

	scheduler.config.separate_short_events = true;
	scheduler.config.show_loading = true;
	scheduler.config.hour_size_px = 50;
	scheduler.locale.labels.agenda_tab = locale.scheduler.weekAbutton;
	scheduler.locale.labels.week_agenda_tab = locale.scheduler.weekAbutton;
	//scheduler.config.ajax_error = false; // do not show popup error message

	//**** EXTENDED CONFIG ****
	//hides Saturdays and Sundays
	scheduler.ignore_week = function (date) {
		if (date.getDay() === 6 || date.getDay() === 0)
			return true;
	};
	// disable toolbar event on click
	scheduler.xy.menu_width = 0;
	scheduler.attachEvent("onClick", function () { return false; });

	//implement tooltip on mouse over : ENRICH DATA HERE
	scheduler.templates.tooltip_text = function (start, end, ev) {
		return "<b>event ID:</b> " + ev.id + " - <b>Company ID:</b> " + ev.company_id + "<br/>" +
		 "<b>" + ev.event_type_name + ":</b> " + ev.company_name + "<br/>" +
			"<b>" + locale.main_page.lblEventComment + ":</b> " + ev.text +
			"<br/><b>" + locale.main_page.lblStartDate + ":</b> " + scheduler.templates.tooltip_date_format(start) +
		"<br/><b>" + locale.main_page.lblEndDate + ":</b> " + scheduler.templates.tooltip_date_format(end);
	};

	//implement lightbox code
	JS_SetEventCustomLightBox(scheduler);

	//implement readonly for events disabled(end of contract, blacklist, lifecycle finished)
	function block_readonly(id) {
		if (!id) return true;
		return !this.getEvent(id).readonly;
	}

	//scheduler.attachEvent("onBeforeDrag", block_readonly)
	//scheduler.attachEvent("onClick", block_readonly)

	//implement refresh on event comments
	scheduler.attachEvent("onBeforeLightbox", function (id) {
		return JS_GetEventHistory(scheduler, id, "eventhistory");
	});
	scheduler.attachEvent("onEventChanged", function (id) {
		return JS_GetEventHistory(scheduler, id, "eventhistory");
	});

	scheduler.templates.event_class = function (start, end, ev) {
		if (ev.usr_id !== getValuefromCookie("TELESALESTOOLV2", "usrId")) {
			if (ev['!nativeeditor_status'] === "updated") {
				return "reassigned_ev";
			}
			return "";
		}
	};

	//text to display in event box
	scheduler.templates.event_text = function (start, end, event) { return ''; };

	//event header in DAY view
	scheduler.templates.event_header = function (start, end, ev) {
		return scheduler.templates.event_date(start) + " - " +
		scheduler.templates.event_date(end) + " : " + ev.company_name + " (" + ev.event_type_name + ")";
	};

	//event header in WEEK A view
	scheduler.templates.week_agenda_event_text = function (start, end, ev, date, position) {
		var defaultCyInfo = ev.company_name + " (" + ev.event_type_name + ")";
		switch (position) {
			case "middle":
				return "-- " + defaultCyInfo;
			case "end":
				return "End: " + scheduler.templates.event_date(end) + " " + defaultCyInfo;
			case "start":
				return "Start: " + scheduler.templates.event_date(start) + " " + defaultCyInfo;
			default:
				return scheduler.templates.event_date(start) + " " + defaultCyInfo;
		}
	};
	//remove onclick event showing nasty tooltip has been made into dhtmlx.css with the display:none; property
	//Validate on save
	scheduler.attachEvent("onEventSave", function (id, ev) {
		if (!ev.text) {
			dhtmlx.message({
				text: locale.main_page.msgemptycomment,
				expire: -1,
				type: "errormessage"
			});
			dhtmlx.alert({ type: "confirm-error", title: locale.popup.titlealidationerror, text: locale.main_page.msgemptycomment });
			//alert(locale.main_page.msgemptycomment);
			return false;
		}
		if (ev.text.length < 2) {
			dhtmlx.message({
				text: anino + locale.main_page.msgshortcomment,
				expire: -1,
				type: "errormessage"
			});
			dhtmlx.alert({ type: "confirm-error", title: locale.popup.titlealidationerror, text: locale.main_page.msgshortcomment });
			//dhtmlx.alert(locale.main_page.msgshortcomment);
			return false;
		}
		ev.text = ev.text.replace(/(?:\r\n|\r|\n)/g, ' ');
		return true;
	});
	return true;
}

var save_url = "classes/dal/clsScheduler/events.asp?a=XML_SaveEvents"; //webservice
var dp = new dataProcessor(save_url);

dp.init(scheduler);
dp.setTransactionMode("POST", false);

dp.attachEvent("onValidationError", function (id, details) {
	dhtmlx.message({ type: "errormessage", expire: -1, text: "/!\\\\ Validation Error" });
});

dp.attachEvent("onFullSync", function () {
	dhtmlx.message({ type: "successmessage", expire: 500, text: "Data synced." });
	return true;
});

function JS_createAgenda(myinsidelayout, myCell) {
	//customize views buttons in main agenda
	var sTabs = '<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>' +
	'<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>' +
	'<div class="dhx_cal_tab" name="month_tab" style="right:280px;"></div>' +
	'<div class="dhx_cal_tab" name="week_agenda_tab" style="right:76px;"></div>' +
	'<div style="left:300px;"><input class="dhx_cal_tab dhx_cal_tab_standalone" style="width:80px; height:33px;" type=button name=btnswitchView id="btnswitchView" value="' + locale.main_page.btnswitchview30 + '" onClick="switchView();"></div>';

	myinsidelayout.cells(myCell).attachScheduler(new Date(), "day", sTabs);
	load_url = "classes/dal/clsScheduler/events.asp?a=XML_GetEventsNF&b=1&c=0"; //webservice
	scheduler.load(load_url);
}

function JS_GetCompanyFutureHistoryPromise(company_id) {
	var url = "/classes/dal/clsScheduler/events.asp";
	return $.get(url, { a: "GetFutureEvents", company_id: company_id })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Future History",
			expire: -1,
			type: "errormessage"
		});
	});
}
	function getEventsForNextWeek(){
		var url = "/classes/dal/clsScheduler/events.asp";
		return $.get(url, { a: "JSON_GetEventsCountForThisWeek"})
		.done(function (data) {
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Object",
				expire: -1,
				type: "errormessage"
			});
		});
	}
	function getEventsForTomorrow(){
		var url = "/classes/dal/clsScheduler/events.asp";
		
		return $.get(url, { a: "JSON_GetEventsCountForTomorrow"})
		.done(function (data) {
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Object",
				expire: -1,
				type: "errormessage"
			});
		});
	}
	
	function getTomorrowPie(myinsidelayout){
		myinsidelayout.setText("Tomorrow");
		getEventsForTomorrow()
		.done(function(data){
			var pieChart = myinsidelayout.attachChart({
				view:"pie3D",
				value:"#events#",
				label:"#event_type_name#",
				tooltip:"#events#"
			});
			pieChart.parse(data, "json");
		});
	}
	function getThisWeekPie(myinsidelayout){
		myinsidelayout.setText("Week");
		getEventsForThisWeek()
		.done(function(data){
			var pieChart = myinsidelayout.attachChart({
				view:"pie3D",
				value:"#events#",
				label:"#event_type_name#",
				tooltip:"#events#"
			});
			pieChart.parse(data, "json");
		});
	}
	
	function getTodayPie(myinsidelayout)
{
	myinsidelayout.setText("Today");
	getEventsForToday()
	.done(function(data){
		var pieChart = myinsidelayout.attachChart({
			view:"pie3D",
			value:"#events#",
			label:"#event_type_name#",
			tooltip:"#events#"
		});
		pieChart.parse(data, "json");
	});
};
	function getEventsForTomorrow(){
		var url = "/classes/dal/clsScheduler/events.asp";
		
		return $.get(url, { a: "JSON_GetEventsCountForTomorrow"})
		.done(function (data) {
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Object",
				expire: -1,
				type: "errormessage"
			});
		});
	}
		function getEventsForThisWeek(){
		var url = "/classes/dal/clsScheduler/events.asp";
		
		return $.get(url, { a: "JSON_GetEventsCountForThisWeek"})
		.done(function (data) {
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Object",
				expire: -1,
				type: "errormessage"
			});
		});
	}
	function getEventsForToday(){
		var url = "/classes/dal/clsScheduler/events.asp";
		
		return $.get(url, { a: "JSON_GetEventsCountForToday"})
		.done(function (data) {
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Object",
				expire: -1,
				type: "errormessage"
			});
		});
	}
function JS_GetAgendaTask(myinsidelayout, myCell) {
	scheduler.attachEvent("onClick", function (id, ev) {
		
		$('[customColor]').css('background-color', '');
		$('[event_id=' + id + ']').css('background-color', 'pink');
		$('[event_id=' + id + ']').attr('customColor', true);
		objEvent = {
			event_id: id,
			company_id: scheduler.getEvent(id).company_id,
			company_vat: scheduler.getEvent(id).company_vat,
			company_hbc_id: scheduler.getEvent(id).company_hbc_id,
			company_name: scheduler.getEvent(id).company_name,
			event_type_id: scheduler.getEvent(id).event_type_id,
			event_type_name: scheduler.getEvent(id).event_type_name,
			event_start_date: scheduler.getEvent(id).start_date,
			event_end_date: scheduler.getEvent(id).end_date,
			event_text: scheduler.getEvent(id).text,
			icon: scheduler.getEvent(id).icon
		};

		var event = scheduler.getEvent(id);
		var sdte = new Date(event.start_date);
		var edte = new Date(event.end_date);

		sdte = sdte.toLocaleString();
		edte = edte.toLocaleString();

		//var s1 = sdte.getDay() + '-' + sdte.getMonth() + '-' + sdte.getFullYear(); + ' ' + sdte.getHours() + ':' + sdte.getMinutes();

		//var barText = objEvent.event_type_name + ' : ' + objEvent.company_name + ' ' + sdte + ' - ' + edte;
		//myinsidelayout.cells("b").setText(barText);
		//myinsidelayout.cells("b").undock();
		JS_GetTaskDetails(objEvent, myinsidelayout);

		return false;
	});
}

function JS_DisplayOpenTasks(myinsidelayout, myCell) {
	var today = GetDateNowScheduler(0, 0);
	var endDate = GetDateNowScheduler(today, 1);
	var beginOftime = "1970-01-01";

	scheduler.config.agenda_start = new Date(beginOftime);
	scheduler.config.agenda_end = new Date(endDate);

	scheduler.locale.labels.agenda_tab = locale.scheduler.agendatab;

	scheduler.templates.agenda_text = function (start, end, ev) {
		return '<img src="../graphics/common/win_16x16/' + ev.icon + '" />' + ev.company_name;
	};
	var divCal = '<div style="position:absolute;left:50px; top:15px;"><img id="start_date" src="/../graphics/common/win_32x32/calenda.png"></div>';
	var sTabs = divCal + '<div id="combo_eventypes" style="width:280px;left:187px;top:10px"></div>';

	myinsidelayout.cells(myCell).attachScheduler(null, "agenda", sTabs);

	load_url = "../classes/dal/clsScheduler/events.asp?a=XML_GetCurrentOpenTasks"; //webservice

	scheduler.clearAll();

	//myinsidelayout.cells(myCell).attachScheduler(null, "agenda");
	scheduler.load(load_url);

	var d = '';
	var arrDiv = ['dhx_cal_today_button', 'dhx_cal_prev_button', 'dhx_cal_next_button'];
	for (var a = 0; a < arrDiv.length; a++) {
		d = document.getElementsByClassName(arrDiv[a]);
		for (var index = 0; index < d.length; index++) {
			d[index].style.display = 'none';
		}
	}

	setComboEvtTypesForOpenTasks();
	setCalendar("start_date");

	//myinsidelayout.cells("b").progressOn();
	scheduler.attachEvent("onClick", function (id, ev) {
		$('[customColor]').css('background-color', '');
		$('[event_id=' + id + ']').css('background-color', 'pink');
		$('[event_id=' + id + ']').attr('customColor', true);
		objEvent = {
			event_id: id,
			company_id: scheduler.getEvent(id).company_id,
			company_vat: scheduler.getEvent(id).company_vat,
			company_hbc_id: scheduler.getEvent(id).company_hbc_id,
			company_name: scheduler.getEvent(id).company_name,
			event_type_id: scheduler.getEvent(id).event_type_id,
			event_type_name: scheduler.getEvent(id).event_type_name,
			event_start_date: scheduler.getEvent(id).start_date,
			event_end_date: scheduler.getEvent(id).end_date,
			event_text: scheduler.getEvent(id).text,
			icon: scheduler.getEvent(id).icon
		};

		JS_GetTaskDetails(objEvent, myinsidelayout);

		return true;
	});
}

function JS_createTasks(myinsidelayout, myCell) {
	var beginOftime = new Date(0);
	var endDate = new Date().getTime() + 6 * 28 * 24 * 60 * 60;
	scheduler.config.agenda_start = new Date(beginOftime);
	scheduler.config.agenda_end = new Date(endDate);
	//scheduler.config.agenda_start = new Date(GetYear(), 0, 1);

	scheduler.locale.labels.agenda_tab = locale.scheduler.agendatab;

	var divCal = '<div style="position:absolute;left:50px; top:15px;"><img id="start_date" src="/../graphics/common/win_32x32/calenda.png"></div>';
	var sTabs = '<div id="combo_eventypes" style="width:280px;left:187px;top:10px"></div>';
	sTabs += divCal;

	scheduler.templates.agenda_text = function (start, end, ev) {
		return '<img src="../graphics/common/win_16x16/' + ev.icon + '" />' + ev.company_name;
	};

	myinsidelayout.cells(myCell).attachScheduler(new Date(), "agenda", sTabs);
	load_url = "../classes/dal/clsScheduler/events.asp?a=XML_GetEvents&b=0&c=1"; //webservice

	scheduler.load(load_url);

	var d = '';
	var arrDiv = ['dhx_cal_today_button', 'dhx_cal_prev_button', 'dhx_cal_next_button'];
	for (var a = 0; a < arrDiv.length; a++) {
		d = document.getElementsByClassName(arrDiv[a]);
		for (var index = 0; index < d.length; index++) {
			d[index].style.display = 'none';
		}
	}

	setComboEvtTypes();
	setCalendar("start_date");

	//myinsidelayout.cells("b").progressOn();
	scheduler.attachEvent("onClick", function (id) {
		$('[customColor]').css('background-color', '');
		$('[event_id=' + id + ']').css('background-color', 'pink');
		$('[event_id=' + id + ']').attr('customColor', true);
		
		objEvent = {
			event_id: id,
			company_id: scheduler.getEvent(id).company_id,
			company_vat: scheduler.getEvent(id).company_vat,
			company_hbc_id: scheduler.getEvent(id).company_hbc_id,
			company_name: scheduler.getEvent(id).company_name,
			event_type_id: scheduler.getEvent(id).event_type_id,
			event_type_name: scheduler.getEvent(id).event_type_name,
			event_start_date: scheduler.getEvent(id).start_date,
			event_end_date: scheduler.getEvent(id).end_date,
			event_text: scheduler.getEvent(id).text,
			icon: scheduler.getEvent(id).icon
		};

		JS_GetTaskDetails(objEvent, myinsidelayout);
	});
}

function SwitchProgress(myinsidelayout, OnOff) {
    if (OnOff === true) {
        myinsidelayout.cells("b").progressOn();
    } else {
        myinsidelayout.cells("b").progressOff();
    }
}

//LAYOUT:right event detail layout of clicked event in the left layout
function JS_createTaskDetailsLayout(myinsidelayout, myCell) {
	evtAcc = myinsidelayout.cells(myCell).attachAccordion({
		dnd: true,
		icons_path: "/graphics/common/win_16x16/",
		items: [
				{ id: "stCompany", text: locale.main_page.lblEntreprise, open: true },
				{ id: "stBo", text: locale.main_page.winleadlayout_B, open: false },
				{ id: "stCw", text: locale.main_page.winleadlayout_C, open: false },
				{ id: "stCex", text: "CEX", open: false}
		]
	});

	var divStCompany = '<div id="stCompany" class="STInfo"></div>';
	var divContact = '<div id="stContact" class="STInfo"></div>';
	var divStCyBusinessData = '<div id="stBusinessData" class="STInfo"></div>';
	var divHistory = '<div id="eventHistory" class="eventhistorymini"></div>';

	var divBackOffice = '<p class="commontitle">' + locale.main_page.lblEventBackOffice + '</p><div id="stBackOffice" class="STInfo"></div>';
	var divBackOfficeUserInfo = '<div id="stBackOfficeUserInfo" class="STInfo"></div>';
	divBackOfficeUserInfo += '<div id="stBackOfficeUserBillingInfo" class="STInfo"></div>';
	var divBackOfficehistory = '<div id="stBackOfficeHistory" class="eventhistorymini"></div>';

	var divCwInfo = '<div id="stCwInfo" class="STInfo"></div>';

	var attachStCompanyInfos = divStCompany + divContact + divStCyBusinessData + divHistory;
	var attachStbackOfficeInfos = divBackOffice + divBackOfficehistory + divBackOfficeUserInfo;
	var attachStCwInfos = divCwInfo;

	evtAcc.cells("stCompany").attachHTMLString(attachStCompanyInfos);
	evtAcc.cells("stBo").attachHTMLString(attachStbackOfficeInfos);
	evtAcc.cells("stCw").attachHTMLString(attachStCwInfos);

	evtAcc.cells("stCompany").showInnerScroll();
	evtAcc.cells("stBo").showInnerScroll();
	evtAcc.cells("stCw").showInnerScroll();
	evtAcc.cells("stCex").showInnerScroll();

	return true;
}

function JS_GetTaskDetails(objEvent, myinsidelayout) {
	if (evtAcc.cells === null) {
		return;
	}

	if (objEvent.event_id !== $("#event_id").val()) {
		if (NullUndefinedToNewVal(objEvent.icon, false) === false) {
			objEvent.icon = 'Calendar.png';
		}

		evtAcc.cells("stCompany").setIcon(objEvent.icon);

		evtAcc.cells("stCompany").setText('EVENT ID: ' + objEvent.event_id + ' ' + objEvent.company_name + ' - company ID:' + objEvent.company_id);
		//evtAcc.cells("stBo").showInnerScroll();

		//SalesTool accordion pane
		evtAcc.cells("stCompany").progressOn();

		$.when(
			displayStCompanyDetailsPromise(objEvent.company_id, "stCompany"),
			displayStCompanycontactsPromise(objEvent.company_id, "stContact"),
			displayStCompanyBusinessData(objEvent.company_id, objEvent.company_name, "stBusinessData"),
			displayEventDetailsPromise(objEvent.company_id, "eventHistory")
		).done(function (companyDetails, companyContacts, companyBusiness, eventDetails) {
			evtAcc.cells("stCompany").progressOff();
		});

		//BackOffice accordion pane
		evtAcc.cells("stBo").progressOn();
		$.when(
			displayStCompanyBackOfficeDetails(objEvent.company_vat, objEvent.company_hbc_id, "stBackOffice", "stBackOfficeHistory")
		).done(function () {
			evtAcc.cells("stBo").progressOff();
		});
		
		
		evtAcc.cells("stCex").progressOn();

		GetCexHistoryPromise(objEvent.company_hbc_id)
        .done(function (objHi) {
            evtAcc.cells("stCex").attachHTMLString(FormatCexHistory(objHi));
            GetCexChartPromise(objEvent.company_hbc_id)
            .done(function (data) {
				if (!$.isEmptyObject(data)){
					GetChart("cexChart", formatJSONChart(data));
				}else{
					$("#cexChart").text("NO DATA AVAILABLE");
				}
            });
			GetCexExperiencePromise(objEvent.company_hbc_id)
			.done(function(data)
			{
				if (!$.isEmptyObject(data)){
					GetCexAbsoluteScore("cexAbsolute", formatAbsoluteNumbers(data));
					GetExperienceScore("cexExperience", formatJSONExperience(data));
				}else{
					$("#cexAbsolute").text("NO DATA AVAILABLE");
					$("#cexExperience").text("NO DATA AVAILABLE");
				}
			});
			
        }).always(function () {
            evtAcc.cells("stCex").progressOff();
        });
		
		//CompanyWeb accordion pane
		displayStCompanyCWDetails(objEvent, "stCwInfo");
	}



	return true;
}

/*SalesTool COMPANY DETAILS*/

function displayStCompanyDetailsPromise(company_id, container) {
	if (!$("#" + container).data('requestRunning')) {
		$("#" + container).data('requestRunning', true);

		var url = "/classes/dal/clsCompany/clsCompany.asp";
		return $.get(url, { a: "JSONGetCompanyInfo", company_id: company_id })
		.done(function (data) {
			$("#" + container).empty().html(formatStCompanyDetails(data));
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Details",
				expire: -1,
				type: "errormessage"
			});
		}).always(function () {
			$("#" + container).data('requestRunning', false);
		});
	}
	return $.when(null);
}

function formatStCompanyDetails(objCy) {
	var iframecall = '<iframe id="callarea" name="callarea" src="blank.htm" width="1" height="1" scrolling="no" frameborder="0" style="visibility:hidden;"></iframe>';
	var postcodenr = "'" + objCy.company_address_postcode + "'";
	var postcode = '<img class=onmouseoverlookup src="/graphics/common/win_32x32/bpost32.png" onClick="displayCompaniesbyPostalCode(' + postcodenr + ');">';
	var contact = '<img class=onmouseovercontact src="/graphics/common/win_32x32/male-user-add.png" onClick="EditContact(-1,' + objCy.company_id + ', ' + objCy.company_address_id + ');">';
	var mailsend = '<img class=onmouseovermail src="/graphics/common/win_32x32/email_32.png">';
	var phonenr = "'" + objCy.company_tel + "'";
	var phoneurl = '<img class=onmouseoverphone src="/graphics/common/win_32x32/Phone-icon.png" onClick="JS_EventCall(' + phonenr + ');">';
	var mobilenr = "'" + objCy.company_mobile + "'";
	var mobileurl = '<img class=onmouseovermobile src="/graphics/common/win_32x32/mobile32.png" onClick="JS_EventCall(' + mobilenr + ');">';
	var company_id = "'" + objCy.company_id + "'";

	var companyedit = '<img id="editcompanybutton" class=onmouseoveredit src="/graphics/common/win_32x32/company_edit.png" onClick="EditCompany(' + company_id + ');">';

	var company_vat = "'" + objCy.company_vat + "'";
	var cwnfo = '<img id="cwnfo" class=onmouseovercwnfo src="/graphics/common/win_32x32/cw32.png" onclick="cwnavigate(' + company_vat + ');">';
	var company_name = objCy.company_name;
	company_name = company_name.replace(/ /g, '+');
	var webnfo = '<img id=cwnfo class=onmouseoverlookup src=/graphics/common/win_32x32/searchglobe.png onclick=webnavigate("' + company_name + '");>';

	var stToolBox = companyedit + '&nbsp;&nbsp;' + webnfo + '&nbsp;&nbsp;' + cwnfo + '&nbsp;&nbsp;' + postcode + '&nbsp;&nbsp;' + mobileurl + '&nbsp;&nbsp;' + phoneurl + '&nbsp;&nbsp;' + contact;

	var t = ''; //'<div id=objCytoolBox class=objCytoolBox>' + companyedit + '&nbsp;&nbsp;' + webnfo + '&nbsp;&nbsp;' + cwnfo + '&nbsp;&nbsp;' + postcode + '&nbsp;&nbsp;' + mobileurl + '&nbsp;&nbsp;' + phoneurl + '&nbsp;&nbsp;' + abo + '&nbsp;&nbsp;' + contact + '</div>';
	t += "<input type='hidden' name='company_id' id='company_id' value='" + objCy.company_id + "' />";
	t += "<input type='hidden' name='company_vat' id='company_vat' value='" + objCy.company_vat + "' />";
	t += "<table cellpadding=2 width=100% border=0>";
	t += '<tr><td><button onclick="scheduler.showLightbox('+ $('#event_id').val()+ ')">*fiche*</button></td><td align=right>' + stToolBox + '</td></tr>';
	t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEntreprise + '</td></tr>';
	t += "<tr><td colspan=2><span class=eventCompanyName id=eventTitle><span id='company_name'>" + objCy.company_name + "</span> " + objCy.jfc_s_desc + "(" + objCy.lang_name + ")<span style='float:right'><input type='button' value='...' onClick='UpdateWithCompanyInfo(" + objCy.company_vat + ")' /></span></td></tr>";
	t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" + locale.main_page.lblCompanyStatus + "</td><td class=STInfo><span id='state'></span></td></tr>";
	t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" + locale.main_page.lblNoEntreprise + "</td><td class=STInfo>" + objCy.company_vat + "</td></tr>";
	t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblAdress + "</td><td class=STInfo id='company_address'>" + objCy.company_address_street + ", " + objCy.company_address_number + " " + objCy.company_address_boxnumber + " - " + objCy.company_address_postcode + " " + objCy.company_address_localite + "</td></tr>";
	t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblActivity + "</td><td class=STInfo>" + objCy.nbc_desc + "</td></tr>";
	t += "<tr><td colspan=2>";
	t += "<table width=100%><tr class=CwCyInfo><td class=CwCylblInfo width=16%>" + locale.main_page.lblTel + "</td><td class=STInfo width=16%>" + objCy.company_tel + "</td>";
	t += "<td class=CwCylblInfo width=16%>" + locale.main_page.lblGsm + "</td><td class=STInfo width=16%>" + objCy.company_mobile + "</td>";
	t += "<td class=CwCylblInfo width=16%>" + locale.main_page.lblEmail + "</td><td class=STInfo width=16%>" + objCy.company_mail + "</td></tr></table>";
	t += "</td></tr>";
	t += "</table>";
	t += iframecall;
	return t;
}

function cwnavigate(vatnumber) {
	window.open('http://www.companyweb.be/page_companydetail.asp?vat=' + vatnumber + '', '_blank');
}
function webnavigate(thevalue) {
	//window.open('https://www.google.be/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=' + thevalue + '', '_blank');
	window.open('https://www.google.be/#q=' + thevalue + '', '_blank');
}

function UpdateWithCompanyInfo(Vat) {
	if (Vat) {
		$.when(JSONGetCompanyFromVat(Vat), JSONGetCompanyInfoFromCompanywebPromise(Vat))
		.done(function (stCompany, cwCompany) {
			var objCy = stCompany[0];
			var data = cwCompany[0];

			objCy.company_name = data.fu_Name;
			objCy.company_address_street = data.fu_StraatNaam;
			objCy.company_address_number = data.fu_HuisNr;
			objCy.company_address_boxnumber = data.fu_BusNr;
			objCy.company_address_localite = data.fu_Gemeente;
			objCy.company_address_postcode = data.fu_Postcode;

			AjaxCompany_UpdateNameAndStreet(objCy)
			.done(function () {
				var addressFormat = data.fu_StraatNaam + ", " + data.fu_HuisNr + " " + data.fu_BusNr + " - " + data.fu_Postcode + " " + data.fu_Gemeente;
				$("#company_address").text();
				$("#company_name").text(data.fu_Name);
			});
		});
	}
}

function displayCompaniesbyPostalCode(postcode) {
	var headerText = locale.main_page.lblCompaniesbyPostCode;
	var formName = "cp";
	var objWindows;
	var thisWindow;
	var formStructure = '';
	var x = 20;
	var y = 20;
	var width = 780;
	var height = 480;

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(formName, x, y, width, height);
	objWindows.attachViewportTo(document.body);
	objWindows.window(formName).setText(headerText);
	objWindows.window(formName).center();

	var mydataset = "/classes/dal/clsBackOffice/clsBackOffice.asp?q=4&postCode=" + postcode;
	var dhxDataView = objWindows.window(formName).attachDataView({
		type: {
			template: "<table width=100%><tr><td width=10%>#HBC_Firmvat#</td><td width=60%><b>#HBC_FirmName#&nbsp; #jfc_s_desc#</b></td><td width=20%>#HBC_Postcode#&nbsp;#HBC_gemeente#</td></tr><tr><td colspan=2>#nbc_desc#</td></tr></table>",
			padding: 2,
			height: 40,
			width: width
		},
		drag: false,
		select: false
	});
	dhxDataView.load(mydataset, "json");
}

function formatToSticker(objCy, objContact) {
	var fn = objContact.contact_title + " " + objContact.contact_firstname + " " + objContact.contact_lastname;
	var t = '';
	t += "<table cellpadding=0 width=400px border=0>";
	t += "<tr><td colspan=2><b>" + objCy.company_name + "</b></td></tr>";
	t += "<tr><td>" + locale.main_page.lblMandateName + "</td><td><b>" + fn + "</b></td></tr>";
	t += "<tr><td width=105px>" + locale.main_page.lblNoEntreprise + "</td><td>" + objCy.company_vat + "</td></tr>";
	t += "<tr><td >" + locale.main_page.lblAdress + "</td><td>" + objCy.company_address_street + ", " + objCy.company_address_number + " " + objCy.company_address_boxnumber + " - " + objCy.company_address_postcode + " " + objCy.company_address_localite + "</td></tr>";

	t += "<tr><td>" + locale.main_page.lblTel + "</td><td>" + objCy.company_tel + "</td></tr>";
	t += "<tr><td>" + locale.main_page.lblGsm + "</td><td>" + objCy.company_mobile + "</td></tr>";
	t += "<tr><td>" + locale.main_page.lblEmail + "</td><td>" + objContact.contact_email + "</td></tr>";
	t += "<tr><td>" + locale.main_page.lblDueDate + ":</td><td>" + getContractEndDate() + "</td></tr>";

	t += "</table>";
	return t;
}

function displayPurchaseOrderWindow(contactId, ctlsource) {
	WindowPurchaseOrder($("#company_id").val(), contactId, ctlsource);
}

var chkcustompackageState;
var objCellAHeight;

var isBestelBon = false;
function WindowPurchaseOrder(id, contactId, ctlsource) {
	var headerText = '';
	switch (ctlsource) {
		case controlsource.button.activation:
			headerText = locale.main_page.lblactivation;
			isBestelBon = false;
			break;
		case controlsource.button.offre:
			headerText = locale.main_page.lblBid;
			isBestelBon = true;
			break;
	}

	$.when(
		GetStCompanyObjectPromise(id), 
		AjaxContact_GetContactFromCompanyPromise(contactId, id)
	).done(function (comp, cont) {
		var objCy = comp[0];
		var objContact = cont[0];

		headerText += ' ' + objCy.company_name;
		var formName = "po";
		var objWindows;
		var thisWindow;
		var formStructure = '';
		var x = 20;
		var y = 20;
		var width = 900;
		var height = 840;
		objCellAHeight = 164;
		objWindows = new dhtmlXWindows();
		thisWindow = objWindows.createWindow(formName, x, y, width, height);
		objWindows.attachViewportTo(document.body);

		objWindows.window(formName).setText(headerText);
		objWindows.window(formName).center();
		objWindows = thisWindow.attachLayout("2E");
		objWindows.cells("a").setHeight(objCellAHeight);

		objWindows.cells("a").setText(locale.main_page.lblProducts);
		objWindows.cells("b").setText(locale.main_page.lblpurchaseorder);
		objWindows.cells("a").showInnerScroll();
		objWindows.cells("b").showInnerScroll();

		objWindows.cells("a").attachHTMLString(displayItemsSelector(objEvent, objWindows));

		$.when(setComboPackagesPromise(objContact.lang_radical), setComboAlertsPromise(),
			setComboMobileAppsPromise(), setComboIntReportsPromise())
		.done(function (packages, alerts, mobile, international) {
		});

		var CyManagementId = 0;
		var cysticker = ''; var contractdate = '';

		//var customerSticker = formatToSticker(objCy, objContact, arrContactIndex);

		var customerlang = objContact.lang_radical;
		objCy.lang_radical = customerlang;
		setMailTemplatePromise(objCy).done(function (data) {
			var mailstring = data;

			var contactName = objContact.contact_title + " " + objContact.contact_lastname;
			var mailTo = varGlobalMailTo;
			var subject = headerText;
			var Attachments = '';
			var mailType = locale.main_page.lblACTIVATION;
			cysticker = formatToSticker(objCy, objContact);
			contractdate = getContractDate();

			var sendmailText = locale.main_page.lblSendMail + ' ' + locale.main_page.lblACTIVATION + ' ' + locale.main_page.wintoolbarToText + ' ' + locale.main_page.lblbackOffice;

			if (isBestelBon === true) {
				var bestelBonTemplate = 'bestelbon_int_' + customerlang + '.pdf';
				mailType = locale.main_page.lblCustomOffer;
				mailTo = objContact.contact_email;
				Attachments = ''; //'/xlib/xtemplates/' + bestelBonTemplate;
				cysticker = '';
				sendmailText = locale.main_page.lblSendMail + ' ' + locale.main_page.lblCustomOffer + ' ' + locale.main_page.wintoolbarToText + contactName;
			}

			mailstring = mailstring.replace('@mailTitle', mailType);
			mailstring = mailstring.replace("@cysticker", cysticker);
			mailstring = mailstring.replace("@contractdate", contractdate);
			mailstring = mailstring.replace("§lastname", contactName + ",");

			if (objContact.contact_gender === 'F') {
				mailstring = mailstring.replace("Cher", "Chère");
			}

			mailstring = mailstring.replace("@politeuser", contactName + ",");

			objWindows.cells("b").attachHTMLString('<div id="mailbody" class="mailbox">' + mailstring + '</div>');

			CyManagementId = 0;
			amtCyManagement = 0;

			$("#transperiod").on('click', function () {
				if (this.checked) {
					setTransitionalArrangementPromise(objCy)
					.done(function (data) {
						$("#§transarr").empty().html(data);
					});
				} else {
					$("#§transarr").empty();
				}
			});

			$("#resignationletter").on('click', function () {
				if (this.checked) {
					setResignationLetterPromise(objCy).done(function (data) {
						$("#§rletter").empty().html(data);
					});
				} else {
					$("#§rletter").empty();
				}
			});

			$("#chkcustompackage").on('click', function () {
				chkcustompackageState = this.checked;
				if (this.checked) {
					document.getElementById("customordercontrols").className = "animated slideInRight";
					showcustom('customordercontrols');
				} else {
					document.getElementById("customordercontrols").className = "animated slideOutRight";
					ResetCustomOrderValues();
					hidecustom('customordercontrols');
				}
			});

			$("#imgAddMemo").on('click', function () {
				document.getElementById('§additionalMemo').innerHTML = document.getElementById('txtAddMemo').value;
			});

			var mailToolbar = objWindows.attachToolbar({
				icons_path: "../graphics/common/win_16x16/"
			});
			var imgDisabled;

			mailToolbar.addButton("1", "1", sendmailText, "email.png", imgDisabled);

			mailToolbar.attachEvent("onClick", function (id) {
				var objOrder = {
					order_list_token_id: GetTokenId(),
					item_package: cbxPackage.getSelectedValue(),
					item_alert: cbxAlert.getSelectedValue(),
					item_mobileApp: cbxMobileApp.getSelectedValue(),
					item_intRep: cbxIntReport.getSelectedValue(),
					item_Cymanagement: CyManagementId,
					order_company_id: objCy.company_id,
					order_company_vat: objCy.company_vat,
					order_contact_id: objContact.contact_id,
					order_event_id: $("#event_id").val(),
					order_user_id: getValuefromCookie("TELESALESTOOLV2", "usrId"),
					order_item_alert_price: Number($("#custom_alerts_price").val()),
					order_item_alert_qty: Number($("#custom_alerts_number").val()),
					order_item_mobileApp_price: Number($("#custom_mobileapps_price").val()),
					order_item_mobileApp_qty: Number($("#custom_mobileapps_number").val()),
					order_item_intRep_price: Number($("#custom_intreports_price").val()),
					order_item_intRep_qty: Number($("#custom_intreports_number").val()),
					order_IsValid: false,
					isbestelbon: isBestelBon,
					order_saved_with_sucess: false
				};
				validateOrder(objOrder);
				if (objOrder.order_IsValid === true) {
					saveOrderPromise(objOrder)
					.done(function () {
						var mailbody = $("#mailbody").html();
						sendMail(mailTo, subject, mailbody, customerlang, objOrder, 1, Attachments)
						.done(function () {
							$("#mailbody").empty();
							thisWindow.close();
						});
					}).fail(function (data) {
						dhtmlx.message({
							text: "Error Saving.",
							expire: -1,
							type: "errormessage"
						});
					});
				}
			});
		});
	});
}

function ResetCustomOrderValues() {
	cbxAlert.setComboValue(-1);
	cbxMobileApp.setComboValue(-1);
	cbxIntReport.setComboValue(-1);
	removeCustomItem('@alerts');
	removeCustomItem('@mobileapps');
	removeCustomItem('@intreports');
}

function validateOrder(objOrder) {
	var i;
	var mailkeywords = ['@package', '@nationalreport', '@alerts', '@mobileapps', '@intreports', '@amount', 'custom'];
	var errkeywords = [];
	var sErr = '';
	var searchStr = $("#mailbody").html();

	for (i = 0; i < mailkeywords.length; i++) {
		if (searchStr.indexOf(mailkeywords[i]) > 0) {
			errkeywords.push(mailkeywords[i]);
		}
	}
	if (errkeywords.length > 0) {
		objOrder.order_IsValid = false;
		for (i = 0; i < errkeywords.length; i++) {
			sErr += errkeywords[i] + '<br>';
		}
		dhtmlx.message({
			text: sErr,
			expire: -1,
			type: "errormessage"
		});
		dhtmlx.alert({ type: "confirm-error", title: locale.popup.titlealidationerror, text: sErr });
	} else {
		objOrder.order_IsValid = true;
	}
}

function saveOrderPromise(objOrder) {
	return AjaxOrders_SaveOrderPromise(objOrder);
}

function sendMail(mailTo, subject, mailbody, customerlang, objOrder, mail_template_id, Attachments) {
	var objMail = {
		Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
		MailTo: mailTo,
		MailCC: varGlobalMailCC,
		MailBCC: varGlobalMailBcc,
		Subject: subject,
		Body: mailbody,
		IsHTML: 1,
		Attachment: Attachments,
		customerlang: customerlang,
		mail_template_id: mail_template_id,
		trusted: 1,
		order_list_token_id: objOrder.order_list_token_id,
		event_id: objOrder.order_event_id,
		company_id: objOrder.order_company_id,
		contact_id: objOrder.order_contact_id,
		user_id: objOrder.order_user_id
	};

	return AjaxMailer_SendMailPromise(objMail);
}

var amtpackage = 0;
var amtAlerts = 0;
var amtMobileApps = 0;
var amtIntreps = 0;
var amtCyManagement = 0;

var cbxPackage, cbxAlert, cbxMobileApp, cbxIntReport;
function displayItemsSelector(objEvent, objWindows) {
	var hrlevel = getValuefromCookie("TELESALESTOOLV2", "hrfunctionid");
	var imgCustomAlert = "<img src =/graphics/common/win_16x16/Download.png class=onmouseoveradd onClick=addCustomItem('@alerts');>";
	var imgCustomMobileApp = "<img src =/graphics/common/win_16x16/Download.png class=onmouseoveradd onClick=addCustomItem('@mobileapps');>";
	var imgCustomIntReports = "<img src =/graphics/common/win_16x16/Download.png class=onmouseoveradd onClick=addCustomItem('@intreports');>";
	var imgEdit = '';

	var t1 = '<table class=innerProductSelector>';
	t1 += '<tr><td class="itemheader">' + locale.main_page.lblPackage + '</td><td><div id="combo_packages" style="width:130px;"></div></td></tr>';
	t1 += '<tr><td colspan=2><input type="checkbox" id="transperiod" value=37>' + locale.main_page.lblTransPeriod + '</td></tr>';
	t1 += '<tr><td colspan=2><input type="checkbox" id="resignationletter" value=37>Nothing</td></tr>';
	t1 += '<tr><td class="itemheader">' + locale.main_page.lblCustomPackage + '</td><td><input type="checkbox"  name="chkcustompackage" id="chkcustompackage"></td></tr>';
	t1 += '</table>';

	var t2 = '<table class=innertblbusinessdata border=0><tr>';
	t2 += '<td class="itemheader">' + locale.main_page.lblAlerts + '</td><td><div id="combo_alerts" style="width:120px;"></div></td>';
	t2 += '<td class="itemheader">' + locale.main_page.lblMobileApps + '</td><td><div id="combo_mobileapps" style="width:120px;"></div></td>';
	t2 += '<td class="itemheader">' + locale.main_page.lblIntReports + '</td><td><div id="combo_intreports" style="width:120px;"></div></td></tr>';
	t2 += '<tr>';
	t2 += '<td colspan=2 align=right><div id=custom_alerts style=display:none;>' + imgCustomAlert + '&nbsp;#&nbsp;<input type=text id=custom_alerts_number size=2>&nbsp;€&nbsp;<input type=text id=custom_alerts_price size=2></div></td>';
	t2 += '<td colspan=2 align=right><div id=custom_mobileapps style=display:none;>' + imgCustomMobileApp + '&nbsp;#&nbsp;<input type=text id=custom_mobileapps_number size=2>&nbsp;€&nbsp;<input type=text id=custom_mobileapps_price size=2></div></td>';
	t2 += '<td colspan=2 align=right><div id=custom_intreports style=display:none;>' + imgCustomIntReports + '&nbsp;#&nbsp;<input type=text id=custom_intreports_number size=2>&nbsp;€&nbsp;<input type=text id=custom_intreports_price size=2></div></td>';
	t2 += '</tr><tr><td colspan=6>&nbsp;</td></tr></table>';

	var t0 = '<table border=0><tr><td width=220px>' + t1 + '</td><td valign=top><div id=customordercontrols style=display:none;position:absolute;>' + t2 + '</div></td></tr><tr><td class="itemheader">' + locale.main_page.lblmemo + '<td valign=top><input type=text id=txtAddMemo size=80>&nbsp;&nbsp;<img src="/graphics/common/win_16x16/add.png" class="onmouseoveradd" id="imgAddMemo";></td></tr></table>';

	var tcustom = '<tr><td colspan=2><input type="checkbox" id="transperiod" value=37>' + locale.main_page.lblTransPeriod + '</td></td><td colspan=2 align=right><div id=custom_alerts style=display:none;>' + imgCustomAlert + '&nbsp;#&nbsp;<input type=text id=custom_alerts_number size=2>&nbsp;€&nbsp;<input type=text id=custom_alerts_price size=2></div></td>';
	tcustom += '<td colspan=2 align=right><div id=custom_mobileapps style=display:none;>' + imgCustomMobileApp + '&nbsp;#&nbsp;<input type=text id=custom_mobileapps_number size=2>&nbsp;€&nbsp;<input type=text id=custom_mobileapps_price size=2></div></td>';
	tcustom += '<td colspan=2 align=right><div id=custom_intreports style=display:none;>' + imgCustomIntReports + '&nbsp;#&nbsp;<input type=text id=custom_intreports_number size=2>&nbsp;€&nbsp;<input type=text id=custom_intreports_price size=2></div></td>';
	tcustom += '</tr>';

	var t = '<table class=innertblbusinessdata border=0>';
	t += '<tr><td class="itemheader">' + locale.main_page.lblPackage + '</td><td><div id="combo_packages" style="width:130px;"></div></td>';
	t += '<td class="itemheader">' + locale.main_page.lblAlerts + '</td><td><div id="combo_alerts" style="width:120px;"></div></td>';
	t += '<td class="itemheader">' + locale.main_page.lblMobileApps + '</td><td><div id="combo_mobileapps" style="width:120px;"></div></td>';
	t += '<td class="itemheader">' + locale.main_page.lblIntReports + '</td><td><div id="combo_intreports" style="width:120px;"></div></td></tr>';
	if (hrlevel < tCustomObjectsLevel) {
		t += tcustom;
		objWindows.cells("a").setHeight(objCellAHeight);
	}
	//'<td colspan=4><input type="checkbox"  id="cymanagement" value=14>' + locale.main_page.lblCyManagement + '</td>'
	t += '<tr><td class="itemheader">' + locale.main_page.lblCustomPackage + '</td><td><input type="checkbox"  id="custompackage" value=-1></td><td colspan=6></td></tr>';
	t += '</table>';
	return t0;
}

function showcustom(container) {
	$("#" + container).show();
}
function hidecustom(container) {
	$("#" + container).hide();
}
function removeCustomItem(type) {
	var price = 0;
	switch (type) {
		case '@alerts':
			var content = document.getElementById("§alerts").innerHTML;
			if (content.indexOf('@') > -1) {
				document.getElementById("§alerts").innerHTML = '';
			}
			amtAlerts = price;
			break;
		case '@mobileapps':
			var content = document.getElementById("§mobileapps").innerHTML;
			if (content.indexOf('@') > -1) {
				document.getElementById("§mobileapps").innerHTML = '';
			}

			amtMobileApps = price;
			break;

		case '@intreports':
			var content = document.getElementById("§intreports").innerHTML;
			if (content.indexOf('@') > -1) {
				document.getElementById("§intreports").innerHTML = '';
			}

			amtIntreps = price;
			break;
	}
	setTotalAmount();
}

function addCustomItem(type) {
	var qty = 0; price = 0;
	switch (type) {
		case '@alerts':
			qty = Number(document.getElementById("custom_alerts_number").value);
			price = Number(document.getElementById("custom_alerts_price").value);
			document.getElementById("§alerts").innerHTML = '<li>' + qty + ' ' + locale.main_page.lblAlerts + '</li>';
			amtAlerts = price;
			break;
		case '@mobileapps':
			qty = Number(document.getElementById("custom_mobileapps_number").value);
			price = Number(document.getElementById("custom_mobileapps_price").value);
			document.getElementById("§mobileapps").innerHTML = '<li>' + qty + ' ' + locale.main_page.lblMobileApps + '</li>';
			amtMobileApps = price;
			break;

		case '@intreports':
			qty = Number(document.getElementById("custom_intreports_number").value);
			price = Number(document.getElementById("custom_intreports_price").value);
			document.getElementById("§intreports").innerHTML = '<li>' + qty + ' ' + locale.main_page.lblmailIntReports + '</li>';
			amtIntreps = price;
			break;
	}
	setTotalAmount();
}

// not used?
function setComboPackagesMultiPromise() {
	var url = "/classes/dal/clsItems/clsItems.asp";
	return $.get(url, { a: "GetComboPackages" })
		.fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Combo Packages Multi",
				expire: -1,
				type: "errormessage"
			});
		});
}

var comboPackageValue = 0;

function setComboPackagesPromise(lang_radical) {
	var url = "/classes/dal/clsItems/clsItems.asp";

	return $.get(url, { a: "GetComboPackages" })
	.done(function (data) {
		$("#combo_packages").empty();

		cbxPackage = new dhtmlXCombo("combo_packages", null, null);
		//cbxPackage.enableFilteringMode(true);
		cbxPackage.allowFreeText(false);
		cbxPackage.load(data, function () {
			cbxPackage.selectOption(0);
		});

		cbxPackage.attachEvent("onChange", function (value, text) {
			comboPackageValue = value;
			var res = text;
			var re = res.split(/\t/);
			res = re[0];

			if (value === -1) {
				amtpackage = 0;
				document.getElementById("§alerts").innerHTML = '@alerts';
				document.getElementById("§package").innerHTML = '@package';
				document.getElementById("§nationalreport").innerHTML = '@nationalreport';
			}
			else {
				amtpackage = re[1];
				amtpackage = amtpackage.replace("(", "");
				amtpackage = amtpackage.replace(")", "");
				amtpackage = amtpackage.replace("€", "");
				document.getElementById("§package").innerHTML = res + " " + locale.main_page.lblPackage;
				setNationalReport(value, lang_radical)
				.done(function (data) {
					$("#§nationalreport").html(data);
				});
			}
			//SetPackageDefaultOptions(value);

			switch (value) {
				case "-1":
					document.getElementById("§alerts").innerHTML = '@alerts';
					document.getElementById("§package").innerHTML = '@package';
					document.getElementById("§nationalreport").innerHTML = '@nationalreport';
					document.getElementById("§mobileapps").innerHTML = '@mobileapps';
					document.getElementById("§intreports").innerHTML = '@intreports';
					document.getElementById('§management').innerHTML = '';
					CyManagementId = 0;

					break;
				case "1":
					document.getElementById("§alerts").innerHTML = "<li>50 " + locale.main_page.lblAlerts + '</li>';
					document.getElementById("§mobileapps").innerHTML = '';
					document.getElementById("§intreports").innerHTML = '';
					document.getElementById("§management").innerHTML = '';
					CyManagementId = 0;
					break;
				case "2":
					document.getElementById("§alerts").innerHTML = "<li>50 " + locale.main_page.lblAlerts + '</li>';
					document.getElementById("§mobileapps").innerHTML = '';
					document.getElementById("§intreports").innerHTML = '';
					document.getElementById("§management").innerHTML = '<li>' + locale.main_page.lblCyManagement + '</li>';
					CyManagementId = 14;
					break;
				case "3":
					document.getElementById("§alerts").innerHTML = "<li>250 " + locale.main_page.lblAlerts + '</li>';
					document.getElementById("§mobileapps").innerHTML = '<li>1 ' + locale.main_page.lblMobileApps + '</li>';
					document.getElementById("§intreports").innerHTML = '';
					document.getElementById("§management").innerHTML = '<li>' + locale.main_page.lblCyManagement + '</li>';
					CyManagementId = 14;
					break;
				case "42":
					document.getElementById("§alerts").innerHTML = "<li>500 " + locale.main_page.lblAlerts; + '</li>';
					document.getElementById("§mobileapps").innerHTML = '<li>1 ' + locale.main_page.lblMobileApps + '</li>';
					document.getElementById("§intreports").innerHTML = '';
					document.getElementById("§management").innerHTML = '<li>' + locale.main_page.lblCyManagement + '</li>';
					CyManagementId = 14;
					break;
				case "43":
					document.getElementById("§alerts").innerHTML = "<li>250 " + locale.main_page.lblAlerts + '</li>';
					document.getElementById("§mobileapps").innerHTML = '<li>1 ' + locale.main_page.lblMobileApps + '</li>';
					document.getElementById("§intreports").innerHTML = '<li>25 ' + locale.main_page.lblmailIntReports + '</li>';
					document.getElementById("§management").innerHTML = '<li>' + locale.main_page.lblCyManagement + '</li>';
					CyManagementId = 14;
					break;
				case "46": // premium 1000
					document.getElementById("§alerts").innerHTML = "<li>1000 " + locale.main_page.lblAlerts; + '</li>';
					document.getElementById("§mobileapps").innerHTML = '<li>1 ' + locale.main_page.lblMobileApps + '</li>';
					document.getElementById("§intreports").innerHTML = '';
					document.getElementById("§management").innerHTML = '<li>' + locale.main_page.lblCyManagement + '</li>';
					CyManagementId = 14;
					break;
				case "47": // premium 2000
					document.getElementById("§alerts").innerHTML = "<li>2000 " + locale.main_page.lblAlerts; + '</li>';
					document.getElementById("§mobileapps").innerHTML = '<li>1 ' + locale.main_page.lblMobileApps + '</li>';
					document.getElementById("§intreports").innerHTML = '';
					document.getElementById("§management").innerHTML = '<li>' + locale.main_page.lblCyManagement + '</li>';
					CyManagementId = 14;
					break;
			}
			cbxAlert.setComboValue(-1);
			cbxMobileApp.setComboValue(-1);
			cbxIntReport.setComboValue(-1);
			setTotalAmount();
		});
	})
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Packages",
			expire: -1,
			type: "errormessage"
		});
	});
}

function setComboAlertsPromise() {
	var url = "/classes/dal/clsItems/clsItems.asp";

	return $.get(url, { a: "GetComboAlerts" })
	.done(function (data) {
		$("#combo_alerts").empty();
		cbxAlert = new dhtmlXCombo("combo_alerts", null, null);
		//cbxAlert.enableFilteringMode(true);
		cbxAlert.allowFreeText(false);
		cbxAlert.load(data, function () {
			cbxAlert.selectOption(0);
		});

		cbxAlert.attachEvent("onChange", function (value, text) {
			var res = text;

			var re = res.split(/\t/);
			res = re[0];

			if (value === -1) {
				amtAlerts = 0;
				document.getElementById("§alerts").innerHTML = '@alerts';
				document.getElementById("custom_alerts_number").value = 0;
				document.getElementById("custom_alerts_price").value = 0;
			}
			else {
				amtAlerts = re[1];
				amtAlerts = amtAlerts.replace("(", "");
				amtAlerts = amtAlerts.replace(")", "");
				amtAlerts = amtAlerts.replace("€", "");
				document.getElementById("§alerts").innerHTML = '<li>' + res + ' ' + locale.main_page.lblAlerts + '</li>';
				document.getElementById("custom_alerts_number").value = res;
				document.getElementById("custom_alerts_price").value = amtAlerts;
			}
			if (chkcustompackageState === true) {
				switch (comboPackageValue) {
					case "3": //premium 250
					case "43": //premium int
						if (res < 251) { amtAlerts = 0; }
						break;
					case "42": //premium 500
						if (res < 501) { amtAlerts = 0; }
						break;
					case "46": //premium 1000
						if (res < 1001) { amtAlerts = 0; }
						break;
					case "47": //premium 2000
						if (res < 2001) { amtAlerts = 0; }
						break;
				}
			}

			if (res === 'custom') {
				showcustom("custom_alerts");
				document.getElementById("custom_alerts_number").value = 0;
				document.getElementById("custom_alerts_price").value = 0;
			} else {
				hidecustom("custom_alerts");
			}

			setTotalAmount();
		});
	})
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Alerts",
			expire: -1,
			type: "errormessage"
		});
	});
}
function setComboMobileAppsPromise() {
	var url = "/classes/dal/clsItems/clsItems.asp";
	return $.get(url, { a: "GetComboMobileApps" })
	.done(function (data) {
		$("#combo_mobileapps").empty();

		cbxMobileApp = new dhtmlXCombo("combo_mobileapps", null, null);
		//cbxMobileApp.enableFilteringMode(true);
		cbxMobileApp.allowFreeText(false);
		cbxMobileApp.load(data, function () {
			cbxMobileApp.selectOption(0);
		});
		cbxMobileApp.attachEvent("onChange", function (value, text) {
			var res = text;
			var re = res.split(/\t/);
			res = re[0];

			if (value === -1) {
				amtMobileApps = 0;
				document.getElementById("§mobileapps").innerHTML = '@mobileapps';
				document.getElementById("custom_mobileapps_number").value = 0;
				document.getElementById("custom_mobileapps_price").value = 0;
			}
			else {
				amtMobileApps = re[1];
				amtMobileApps = amtMobileApps.replace("(", "");
				amtMobileApps = amtMobileApps.replace(")", "");
				amtMobileApps = amtMobileApps.replace("€", "");
				document.getElementById("§mobileapps").innerHTML = '<li>' + res + " " + locale.main_page.lblMobileApps + '</li>';
				document.getElementById("custom_mobileapps_number").value = res;
				document.getElementById("custom_mobileapps_price").value = amtMobileApps;
			}
			if (res === 0) {
				document.getElementById("§mobileapps").innerHTML = '';
			}
			if (chkcustompackageState === true) {
				switch (comboPackageValue) {
					case "1":
						amtMobileApps = 0;
						document.getElementById("§mobileapps").innerHTML = '';
						dhtmlx.message({
							text: locale.popup.msgnomobileappforbasic,
							expire: -1,
							type: "errormessage"
						});
						dhtmlx.alert({ type: "confirm-error", title: locale.main_page.lblMobileApps, text: locale.popup.msgnomobileappforbasic });
						break;
					case "2":
						if (amtMobileApps === 0 && value !== -1) {
							document.getElementById("§mobileapps").innerHTML = '@mobileapps';
							dhtmlx.message({
								text: locale.popup.msgnofreemobileapp,
								expire: -1,
								type: "errormessage"
							});
							dhtmlx.alert({ type: "confirm-error", title: locale.main_page.lblMobileApps, text: locale.popup.msgnofreemobileapp });
						}
						break;
					case "3": //premium 250
						if (res < 2) { amtMobileApps = 0; }
						break;
					case "42": //premium 500
						if (res < 2) { amtMobileApps = 0; }
						break;
					case "43", "46": //premium int
						if (res < 2) { amtMobileApps = 0; }
						break;
					case "46": // premium 1000
						if (res < 2) { amtMobileApps = 0; }
						break;
					case "47": // premium 2000
						if (res < 2) { amtMobileApps = 0; }
						break;
				}
			}

			if (res === 'custom') {
				showcustom("custom_mobileapps");
				document.getElementById("custom_mobileapps_number").value = 0;
				document.getElementById("custom_mobileapps_price").value = 0;
			} else {
				hidecustom("custom_mobileapps");
			}

			setTotalAmount();
		});
	})
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo MobileApps",
			expire: -1,
			type: "errormessage"
		});
	});
}

function setComboIntReportsPromise() {
	var url = "/classes/dal/clsItems/clsItems.asp";

	return $.get(url, { a: "GetComboIntReports" })
	.done(function (data) {
		$("#combo_intreports").empty();

		cbxIntReport = new dhtmlXCombo("combo_intreports", null, null);
		//cbxIntReport.enableFilteringMode(true);
		cbxIntReport.allowFreeText(false);
		cbxIntReport.load(data, function () {
			cbxIntReport.selectOption(0);
		});
		cbxIntReport.attachEvent("onChange", function (value, text) {
			var res = text;
			var re = res.split(/\t/);
			res = re[0];

			if (value === -1 || res === '0') {
				amtIntreps = 0;
				document.getElementById("§intreports").innerHTML = '';
				document.getElementById("custom_intreports_number").value = 0;
				document.getElementById("custom_intreports_price").value = 0;
			}
			else {
				amtIntreps = re[1];
				amtIntreps = amtIntreps.replace("(", "");
				amtIntreps = amtIntreps.replace(")", "");
				amtIntreps = amtIntreps.replace("€", "");
				document.getElementById("§intreports").innerHTML = '<li>' + res + " " + locale.main_page.lblmailIntReports + '</li>';
				document.getElementById("custom_intreports_number").value = res;
				document.getElementById("custom_intreports_price").value = amtIntreps;
			}
			if (chkcustompackageState === true) {
				switch (comboPackageValue) {
					case "43": //premium int
						if (res < 26) { amtIntreps = 0; }
						break;
				}
			}
			if (res === 'custom') {
				showcustom("custom_intreports");
				document.getElementById("custom_intreports_number").value = 0;
				document.getElementById("custom_intreports_price").value = 0;
			} else {
				hidecustom("custom_intreports");
			}

			setTotalAmount();
		});
	})
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Internation Reports",
			expire: -1,
			type: "errormessage"
		});
	});
}
function setMailTemplatePromise(objCy) {
	var customerLang = objCy.lang_radical;
	var url = "/classes/dal/clsItems/clsItems.asp";
	return $.get(url, { a: "GetMailTemplate", mail_template_id: 1, CustomerLang: customerLang })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting MailTemplate",
			expire: -1,
			type: "errormessage"
		});
	});
}
function setResignationLetterPromise(objCy) {
	var customerLang = objCy.lang_radical;
	var url = "/classes/dal/clsItems/clsItems.asp";

	return $.get(url, { a: "GetResignationLetter", CustomerLang: customerLang })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Resignation Letter",
			expire: -1,
			type: "errormessage"
		});
	});
}

function setTransitionalArrangementPromise(objCy) {
	var customerLang = objCy.lang_radical;
	var url = "/classes/dal/clsItems/clsItems.asp";

	return $.get(url, { a: "GetTransitionalArrangement", CustomerLang: customerLang })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Transitional Arrangement",
			expire: -1,
			type: "errormessage"
		});
	});
}
function GetPriceByItemIDPromise(itemID) {
	var url = "/classes/dal/clsItems/clsItems.asp";

	return $.get(url, { a: "GetPriceByItemID", item_ID: itemID })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Price For Item",
			expire: -1,
			type: "errormessage"
		});
	});
}
function setNationalReport(packageID, lang_radical) {
	var url = "/classes/dal/clsItems/clsItems.asp";
	return $.get(url, { a: "GetNationalReport", CustomerLang: lang_radical, packageID: packageID })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting National Report",
			expire: -1,
			type: "errormessage"
		});
	});
}

function setTotalAmount() {
	var pricestring = $("#§amount").text();
	var totalAmount = 0;
	if (chkcustompackageState !== true)
	{ totalAmount = parseInt(amtpackage); }
	else {
		totalAmount = parseInt(amtpackage) + parseInt(amtAlerts) + parseInt(amtMobileApps) + parseInt(amtIntreps) + parseInt(amtCyManagement);
	}

	if (totalAmount === 0) {
		$("#§amount").text('@price');
	}
	else {
		$("#§amount").text(locale.main_page.lbltotalamountprice + ' ' + totalAmount + locale.main_page.lbltotalamounteuroyearexvat);
	}
}

function EditCompany(company_id) {
	headerText = objEvent.company_name;
	formStructure = [

				{ type: "input", name: "company_name", value: objEvent.company_name, required: true, readonly: true, inputWidth: 300 },
				{ type: "settings", position: "label-left" },
				{ type: "label", label: "Settings" },
				{ type: "input", name: "company_id", required: true, label: 'Company ID ', value: company_id, readonly: true },
				{ type: "input", name: "company_hbc_id", required: true, label: 'BackOffice ID', value: "0" },

				{ type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 370, inputTop: 160 },
				{ type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 270, inputTop: 160 }
	];

	var objWindows;
	var thisWindow;
	var category = "Link Back Office ID > Company ID";
	var x = 20;
	var y = 20;
	var width = 480;
	var height = 240;

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(category, x, y, width, height);
	objWindows.attachViewportTo(document.body);
	objWindows.window(category).setText(headerText);
	objWindows.window(category).center();

	var editForm = objWindows.window(category).attachForm(formStructure);
	editForm.attachEvent("onButtonClick", function (buttonid) {
		var objCompany = {
			company_id: editForm.getItemValue("company_id"),
			company_hbc_id: editForm.getItemValue("company_hbc_id"),
			trusted: 1
		};

		switch (buttonid) {
			case 'btnsave':
				AjaxCompany_UpdateBackOfficeID(objCompany);
				objEvent.company_hbc_id = objCompany.company_hbc_id;
				displayStCompanyBackOfficeDetails(objEvent.company_vat, objEvent.company_hbc_id, "stBackOffice", "stBackOfficeHistory");
				thisWindow.close();
				break;
			case "btncancel":
				thisWindow.close();
				break;
			default:
		}
	});
}

/*SalesTool COMPANY CONTACT DETAILS*/
function displayStCompanycontactsPromise(company_id, container) {
	if (!$("#" + container).data('requestRunning')) {
		$("#" + container).data('requestRunning', true);
		var url = "/classes/dal/clsContact/clsContact.asp";
		$("#" + container).empty();

		return $.get(url, { a: "JSONGetStContactDetails", company_id: company_id })
		.done(function (data) {
			$("#" + container).html(formatStContactDetails(data));
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Contacts",
				expire: -1,
				type: "errormessage"
			});
		}).always(function () {
			$("#" + container).data('requestRunning', false);
		});
	}
	return $.when(null);
}

function formatStContactDetails(obj) {
	var spacer, remove, contact, mailsend, phoneurl, phonenr, mobileurl, mobilenr, purchaseorder, abo, bestelbon;

    
	var t = "<table cellpadding=1 width=100% border=0>";
	t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEventContact + '</td></tr>';
	if (!$.isEmptyObject(obj)) {
	    for (var i = 0; i < obj.length; i++) {
			if (!$.isEmptyObject(obj[i])){
	        spacer = '&nbsp;&nbsp;';
	        remove = '<img class =onmouseoverdel src="/graphics/common/win_16x16/male-user-remove.png" onClick="DeleteContact(' + obj[i].contact_id + ');">';
	        contact = '<img class =onmouseoveredit src="/graphics/common/win_16x16/male-user-edit.png" onClick="EditContact(' + obj[i].contact_id + ', ' + obj[i].company_id + ', ' + obj[i].company_address_id + ');">';
	        mailsend = '<img class =onmouseovermail src="/graphics/common/win_16x16/email_16.png" onClick="MailContact(\'' + obj[i].contact_id + '\');">';
	        phonenr = "'" + obj[i].contact_tel + "'";
	        mobilenr = "'" + obj[i].contact_mobile + "'";
	        phoneurl = '<img class =onmouseoverphone src="/graphics/common/win_16x16/Phone-icon16.png" onClick="JS_EventCall(' + phonenr + ');">';
	        mobileurl = '<img class =onmouseovermobile src="/graphics/common/win_16x16/mobile16.png" onClick="JS_EventCall(' + mobilenr + ');">';
	        abo = '<img id="eurotoken" class =onmouseovereuro src="/graphics/common/win_16x16/Activation16.png" onclick="displayPurchaseOrderWindow(' + obj[i].contact_id + ',' + controlsource.button.activation + ');">';
	        bestelbon = '<img id="bestelbon" class =onmouseovermail src="/graphics/common/win_16x16/purchase_order_16.png" onclick="displayPurchaseOrderWindow(' + obj[i].contact_id + ',' + controlsource.button.offre + ');">';

	        t += "<tr id='contact_id" + obj[i].contact_id + "'><td width=85% class=eventContactHeader>ContactId:" + obj[i].contact_id + ' ' + obj[i].contact_title + " " + obj[i].contact_firstname + " " + obj[i].contact_lastname + ", " + obj[i].contact_type + " (" + obj[i].lang_name + ")</td>";
	        t += "<td align=right>" + bestelbon + spacer + abo + spacer + remove + spacer + contact + "</td></tr>";
	        t += "<tr><td><table width=100% border=0>";
	        t += "<tr class=CwCyInfo><td class=CwCylblInfo width=50px>" + phoneurl + locale.main_page.lblTel + "</td><td class=STInfo width=100px>" + obj[i].contact_tel + "</td>";
	        t += "<td class=CwCylblInfo width= width=50px>" + mobileurl + locale.main_page.lblGsm + "</td><td class=STInfo width=100px>" + obj[i].contact_mobile + "</td>";
	        t += "<td class=CwCylblInfo width= width=50px>" + mailsend + locale.main_page.lblEmail + "</td><td class=STInfo width=200px>" + obj[i].contact_email + "</td></tr>";
	        t += "</table>";
			}
	    }
	}
	t += "</table>";

	return t;
}

var objTypes = '';
function GetComboContactTypesPromise() {
	var url = "/classes/dal/clsContact/clsContact.asp";

	return $.get(url, { a: "GetComboContactTypes" })
	.done(function (data) {
		objTypes = data;
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Contact Types",
			expire: -1,
			type: "errormessage"
		});
	});
}

function GetComboContactTitlesPromise() {
	var url = "/classes/dal/clsContact/clsContact.asp";

	return $.get(url, { a: "GetComboContactTitles" })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Contact Titles",
			expire: -1,
			type: "errormessage"
		});
	});
}

function MailContact(contactId) {
	var formName = "frm_mailcontact";
	var objWindows;
	var thisWindow;
	var formStructure = '';
	var x = 20;
	var y = 20;
	var width = 1200;
	var height = 900;

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(formName, x, y, width, height);

	objWindows.attachViewportTo(document.body);

	thisWindow.center();
	thisWindow.maximize();
	objWindows = thisWindow.attachLayout("1C");
	thisWindow.attachEvent("onClose", function (win) {
		//return JS_RefreshEventHistory(objEvent.event_id, "eventhistory")
		return true;
	});
	//objWindows.cells("a").setHeight(120);
	var cyId = $("#company_id").val();
	
    AjaxContact_GetContactFromCompanyPromise(contactId, cyId)
	.done(function (data) {
		var headerText = locale.main_page.lblEmail + ' ' + data.contact_firstname + ' ' + data.contact_lastname + '(' + data.contact_email + '; ' + data.lang_name + ')';
		thisWindow.setText(headerText);

		objWindows.cells("a").setText(locale.main_page.lblEmail + data.contact_email);
		objWindows.cells("a").showInnerScroll();

		objWindows.cells("a").attachHTMLString(MM_DocumentEngineSetLayout(data.contact_id));
		document.getElementsByName('MailTo')[0].value = data.contact_email;

		var event_id = 0;
		var company_id = data.company_id;

		event_id = $("#event_id").val();

		var objMail = {
			mail_template_id: $("#mail_template_id").val(),
			Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
			MailTo: data.contact_email,
			MailCC: varGlobalMailCC,
			MailBCC: varGlobalMailBcc,
			Subject: '',
			Body: '',
			IsHTML: 1,
			Attachment: '',
			customerlang: data.lang_radical,
			trusted: 1,
			order_list_token_id: 0,
			event_id: event_id,
			company_id: company_id,
			contact_id: data.contact_id,
			user_id: getValuefromCookie("TELESALESTOOLV2", "usrId")
		};

		MM_DocumentEngineInitialize(myinsidelayout, i, objWindows, objMail, data);
	});

	//var mailCC = document.getElementsByName('MailCC')[0].value
	//document.getElementsByName('MailTo')[0].disabled = 'disabled';

	//
}

function DeleteContact(i) {
	var cyId = $("#company_id").val();
	
	$.when($.when(AjaxContact_GetContactFromCompanyPromise(i, cyId)))
	.done(function (objContact) {
		var fullName = objContact.contact_firstname + " " + objContact.contact_lastname;
		var id = objContact.contact_id;
		dhtmlx.confirm({
			type: locale.main_page.lblMessage,
			ok: locale.main_page.ok, cancel: locale.main_page.cancel,
			text: imgconfirm1 + locale.main_page.confirmDelete + fullName,
			callback: function (result) {
				if (result === true) {
					AjaxContact_DeleteContact(id);
					displayStCompanycontactsPromise(objEvent.company_id, "stContact");
					//dhtmlx.alert({ type: "confirm-error", title: "TOO LATE", text: "deleted" });
				}
			}
		});
	});
}

function EditContact(i, company_id, address_id) {
	var fullName = '';
	var windowName = "EditContact" + i;
	var category = locale.main_page.editcurrent;
	var contact_title_id = "0";
	var contact_tel = '';
	var contact_mobile = '';
	var contact_gender = "1";
	var contact_firstname = '';
	var contact_lastname = '';
	var contact_email = '';
	var contact_lang_id = "1";
	var contact_id = "0";
	var company_address_link_id = "0";
	var contact_type_id = "1";
	var contact_company_id = company_id;
	var company_address_id = address_id;

	var objWindows;
	var thisWindow;
	var x = 20;
	var y = 20;
	var width = 640;
	var height = 480;

    $.when(AjaxContact_GetContactFromCompanyPromise(i, company_id))
		.done(function (objContact) {
			if (!$.isEmptyObject(objContact)) {
				fullName = objContact.contact_firstname + " " + objContact.contact_lastname;
				contact_title_id = objContact.contact_title_id;
				contact_tel = cleanUpCallNumber(objContact.contact_tel);
				contact_gender = objContact.contact_gender;
				contact_firstname = objContact.contact_firstname;
				contact_lastname = objContact.contact_lastname;
				contact_mobile = cleanUpCallNumber(objContact.contact_mobile);
				contact_email = objContact.contact_email;
				contact_lang_id = objContact.contact_lang_id;
				contact_id = objContact.contact_id;
				contact_type_id = objContact.contact_type_id;
				company_address_link_id = objContact.company_address_link_id;

				contact_tel = contact_tel.replace(/ /g, '');
				contact_mobile = contact_mobile.replace(/ /g, '');
				contact_email = contact_email.replace(/ /g, '');
			} else {
				category = locale.main_page.addnew;
				contact_id = "0";
			}

			var headerText = '';
			var formStructure = '';

			headerText = category + ' ' + fullName;
			objWindows.window(windowName).setText(headerText);

			var genderOpts = [{ value: "M", text: "M" }, { value: "F", text: "F" }];
			var langOpts = [{ value: "1", text: "FR" }, { value: "2", text: "NL" }, { value: "3", text: "EN" }];

			$.when(GetComboContactTypesPromise(), GetComboContactTitlesPromise())
			.done(function (contacttypes, titles) {
				var typeOpts = [];
				var titleOpts = [];
				var data = contacttypes[0];
				for (var j = 0; j < data.type.length; j++) {
					typeOpts.push({ value: data.type[j].value, text: data.type[j].text });
				}

				data = titles[0];
				for (var j = 0; j < data.title.length; j++) {
					titleOpts.push({ value: data.title[j].value, text: data.title[j].text });
				}

				formStructure = [
					{ type: "settings", position: "label-top", inputWidth: "auto", offsetLeft: 10 },
					{ type: "select", name: "contact_title", label: locale.main_page.lblTitle, options: titleOpts, value: contact_title_id, required: true, validate: "[0-9]+", inputWidth: 300 },
					{ type: "input", name: "contact_firstname", label: locale.main_page.lblFirstName, value: contact_firstname, required: true, validate: "[A-Za-z ]+", inputWidth: 300 },
					{ type: "input", name: "contact_lastname", label: locale.main_page.lblLastName, value: contact_lastname, required: true, validate: "[A-Za-z ]+", inputWidth: 300 },

					{ type: "input", name: "contact_tel", label: locale.main_page.lblTel, value: contact_tel, required: true, inputWidth: 300, validate: "ValidNumeric" },
					{ type: "input", name: "contact_mobile", label: locale.main_page.lblGsm, value: contact_mobile, required: false, inputWidth: 300, validate: "ValidNumeric" },
					{ type: "input", name: "contact_email", label: locale.main_page.lblEmail, value: contact_email, required: false, inputWidth: 300 },

					{ type: "newcolumn" },
					{ type: "select", name: "contact_type", label: locale.main_page.lblContactType, options: typeOpts, value: contact_type_id, required: true },
					{ type: "select", name: "contact_lang_radical", label: locale.main_page.lblLang, options: langOpts, value: contact_lang_id, required: true },
					{ type: "select", name: "contact_gender", label: locale.main_page.lblGender, options: genderOpts, value: contact_gender, required: true },

					{ type: "hidden", name: "contact_id", value: contact_id, label: "contact_id", required: true },

					{ type: "hidden", name: "contact_company_id", value: contact_company_id, label: "contact_company_id", required: true },
					{ type: "hidden", name: "company_address_id", value: company_address_id, label: "company_address_id", required: true },
					{ type: "hidden", name: "company_address_link_id", value: company_address_link_id, label: "company_address_link_id", required: true },
					{ type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 100, inputTop: 390 },
					{ type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 200, inputTop: 390 }
				];

				var editForm = objWindows.window(windowName).attachForm(formStructure);

				editForm.attachEvent("onButtonClick", function (buttonid) {
					var contact_tel = cleanUpCallNumber(editForm.getItemValue("contact_tel"));
					if (contact_tel.length > 0) {
						if (anyDigit(contact_tel) !== true) {
							dhtmlx.message({
								text: locale.popup.msgusedigitsonly,
								expire: -1,
								type: "errormessage"
							});
							dhtmlx.alert(anino + locale.popup.msgusedigitsonly);
							return false;
						}else{
							editForm.setItemValue("contact_tel", contact_tel);
						}
					}
					var contact_mobile = cleanUpCallNumber(editForm.getItemValue("contact_mobile"));
					if (contact_mobile.length > 0) {
						if (anyDigit(contact_mobile) !== true) {
							dhtmlx.message({
								text: locale.popup.msgusedigitsonly,
								expire: -1,
								type: "errormessage"
							});
							dhtmlx.alert(anino + locale.popup.msgusedigitsonly);
							return false;
						}else{
							editForm.setItemValue("contact_mobile", contact_mobile);
						}
					}
					var contact_email = editForm.getItemValue("contact_email");
					contact_email = contact_email.replace(/ /g, '');

					var objContactSave = {
						contact_id: editForm.getItemValue("contact_id"),
						contact_gender: editForm.getItemValue("contact_gender"),
						contact_type_id: editForm.getItemValue("contact_type"),
						contact_title_id: editForm.getItemValue("contact_title"),
						contact_lang_id: editForm.getItemValue("contact_lang_radical"),
						contact_firstname: editForm.getItemValue("contact_firstname"),
						contact_lastname: editForm.getItemValue("contact_lastname"),
						contact_tel: contact_tel, //editForm.getItemValue("contact_tel"),
						contact_mobile: contact_mobile, //editForm.getItemValue("contact_mobile"),
						contact_email: contact_email, //editForm.getItemValue("contact_email"),
						contact_company_id: editForm.getItemValue("contact_company_id"),
						company_address_link_id: editForm.getItemValue("company_address_link_id"),
						company_address_id: editForm.getItemValue("company_address_id")
					};

					switch (buttonid) {
						case "btnsave":
							if (editForm.validate()) {
								if (i !== -1) {
									if (validateFormData(objContactSave) === true) {
										AjaxContact_EditContact(objContactSave);
										thisWindow.close();
										displayStCompanycontactsPromise(objEvent.company_id, "stContact");
									}
								}
								else {
									if (validateFormData(objContactSave) === true) {
										AjaxContact_AddContact(objContactSave);
										thisWindow.close();
										displayStCompanycontactsPromise(objEvent.company_id, "stContact");
									}
								}
							}
							break;
						case "btncancel":
							thisWindow.close();
							break;
						default:
					}
				});
			});
			objWindows.window(windowName).progressOff();
		});

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(windowName, x, y, width, height);
	objWindows.attachViewportTo(document.body);
	objWindows.window(windowName).center();
	objWindows.window(windowName).progressOn();
}

/*SalesTool COMPANY CONTACT Validation Rules*/

function validateFormData(obj) {
	var errmsg = ''; var blnvalid = true;
	//if (anyChar(obj.contact_firstname) != true) {errmsg += locale.main_page.lblFirstName + ':' + locale.popup.msgstringerror +'<br>'; blnvalid= false;}
	if (anyChar(obj.contact_lastname) !== true) { errmsg += locale.main_page.lblLastName + ':' + locale.popup.msgstringerror + '<br>'; blnvalid = false; }
	if (anyChar(obj.contact_tel) !== true) { errmsg += locale.main_page.lblTel + ':' + locale.popup.msgnumericerror + '<br>'; blnvalid = false; }
	//if (anyChar(obj.contact_mobile) != true) { errmsg += locale.main_page.lblGsm + ':' + locale.popup.msgnumericerror + '<br>'; blnvalid = false; }
	//if (IsvalidEmail(obj.contact_email) != true) { errmsg += locale.main_page.lblEmail + ':' + locale.popup.msgemailerror + '<br>'; blnvalid = false; }
	if (blnvalid === false) {
		dhtmlx.message({
			text: errmsg,
			expire: -1,
			type: "errormessage"
		});
		dhtmlx.alert({
			title: locale.popup.titlealidationerror,
			type: "alert-error",
			text: errmsg
		});
	}
	else {
		return true;
	}
}

/*ST COMPANY BUSINESS DATA*/

/* submit new Competitor, Software, Federation, Software house to backoffice*/
function submitItem(category) {
	var message = "submit new " + category + " for BackOffice Approval";
	dhtmlx.message({
		text: message,
		expire: -1,
		type: "warningmessage"
	});
	dhtmlx.alert(message);
}

function delItem(id, category) {
	switch (category) {
		case 'competitor':
			AjaxCompetitor_DeleteEntry(id);
			AjaxCompetitor_GetCompetitorsList("itCompetition", objEvent.company_id);
			break;
		case 'software':
			AjaxSoftware_DeleteEntry(id);
			AjaxSoftware_GetSoftwaresList("itsoftware", objEvent.company_id);
			break;
		case 'federation':
			AjaxFederation_DeleteEntry(id);
			AjaxFederation_GetFederationsList("itfederation", objEvent.company_id);
			break;
		case 'softwarehouse':
			AjaxSoftwareHouse_DeleteEntry(id);
			AjaxSoftwareHouse_GetSoftwareHousesList("itsoftwarehouse", objEvent.company_id);
			break;
	}
}

/* map Competitor, Software, Federation, softwarehouse to customer*/
function editItem(company_id, company_name, category) {
	var headerText = '';
	var formStructure = '';
	var item_id = '';
	var labelItemName = '';
	var blnDateHidden = true;
	switch (category) {
		case 'competitor':
			headerText = locale.main_page.addnew + ' ' + locale.main_page.lblCompetitor + ' : ' + company_name;
			item_id = cbxCompetitor.getSelectedValue();
			item_name = cbxCompetitor.getSelectedText();
			labelItemName = locale.main_page.lblCompetitor;
			blnDateHidden = false;
			break;
		case 'software':
			headerText = locale.main_page.addnew + ' ' + locale.main_page.lblSoftware + ' : ' + company_name;
			item_id = cbxSoftware.getSelectedValue();
			item_name = cbxSoftware.getSelectedText();
			labelItemName = locale.main_page.lblSoftware;
			blnDateHidden = true;
			break;
		case 'federation':
			headerText = locale.main_page.addnew + ' ' + locale.main_page.lblfederation + ' : ' + company_name;
			item_id = cbxFederation.getSelectedValue();
			item_name = cbxFederation.getSelectedText();
			labelItemName = locale.main_page.lblfederation;
			blnDateHidden = true;
			break;
		case 'softwarehouse':
			headerText = locale.main_page.addnew + ' ' + locale.main_page.lblsoftwarehouse + ' : ' + company_name;
			item_id = cbxSoftwarehouse.getSelectedValue();
			item_name = cbxSoftwarehouse.getSelectedText();
			labelItemName = locale.main_page.lblsoftwarehouse;
			blnDateHidden = true;
			break;
	}

	formStructure = [
				{ type: "settings", position: "label-top" },
				{ type: "hidden", name: "category", value: category },
				{ type: "hidden", name: "company_id", value: company_id },
				{ type: "hidden", name: "item_id", value: item_id },
				{ type: "input", name: "company_name", label: locale.main_page.lblEntreprise, value: company_name, required: true, readonly: true, inputWidth: 300 },
				{ type: "input", name: "item_name", label: labelItemName, value: item_name, required: true, readonly: true, inputWidth: 300 },
				{ type: "calendar", name: "end_date", label: locale.main_page.lblContractEndDate, dateFormat: "%Y-%m-%d", required: true, hidden: blnDateHidden },
				{ type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 520, inputTop: 390 },
				{ type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 420, inputTop: 390 }
	];

	var objWindows;
	var thisWindow;

	var x = 20;
	var y = 20;
	var width = 640;
	var height = 480;

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(category, x, y, width, height);
	objWindows.attachViewportTo(document.body);
	objWindows.window(category).setText(headerText);
	objWindows.window(category).center();

	var editForm = objWindows.window(category).attachForm(formStructure);
	editForm.attachEvent("onButtonClick", function (buttonid) {
		var objCompany = {
			category: editForm.getItemValue("category"),
			company_id: editForm.getItemValue("company_id"),
			item_id: editForm.getItemValue("item_id"),
			end_date: editForm.getItemValue("end_date", true),
			trusted: 1
		};
		switch (buttonid) {
			case 'btnsave':
				ASPSaveCompanyBusinessData(objCompany);

				switch (objCompany.category) {
					case 'competitor':
						AjaxCompetitor_GetCompetitorsList("itCompetition", company_id);
						break;
					case 'software':
						AjaxSoftware_GetSoftwaresList("itsoftware", company_id);
						break;
					case 'federation':
						AjaxFederation_GetFederationsList("itfederation", company_id);
						break;
					case 'softwarehouse':
						AjaxSoftwareHouse_GetSoftwareHousesList("itsoftwarehouse", company_id);
						break;
				}

				thisWindow.close();
				break;
			case "btncancel":
				thisWindow.close();
				break;
			default:
		}
	});
}

function displayStCompanyBusinessData(company_id, company_name, container) {
	if (!$("#" + container).data('requestRunning')) {
		$("#" + container).data('requestRunning', true);

		var imgEdit = '<img src ="/graphics/common/win_16x16/edit.png" class="onmouseoveredit" onClick="submitItem(category);">';
		var imgItem = '<img src ="/graphics/common/win_16x16/add.png" class="onmouseoveradd" onClick="editItem(\'' + company_id + '\',\'' + company_name + '\',category);">';

		var it1 = '<table class="innertblbusinessdata">';
		it1 += '<tr><td class="header">' + locale.main_page.lblCompetitor + '</td><td align=imgItem>' + imgEdit.replace("category", "'competitor'") + '</td></tr>';
		it1 += '<tr><td><div id="combo_competitors" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'competitor'") + '</td></tr>';
		it1 += '<tr><td colspan=2><div id="itCompetition" name="competitors">competitor list</div></td><td align=imgItem></td></tr>';
		it1 += '</table>';

		var it2 = '<table class="innertblbusinessdata">';
		it2 += '<tr><td class="header">' + locale.main_page.lblSoftware + '</td><td align=left>' + imgEdit.replace("category", "'software'") + '</td></tr>';
		it2 += '<tr><td ><div id="combo_softwares" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'software'") + '</td></tr>';
		it2 += '<tr><td colspan=2><div id="itsoftware" name="softwares">software list</div></td></tr>';
		it2 += '</table>';

		var it3 = '<table class="innertblbusinessdata">';
		it3 += '<tr><td class="header">' + locale.main_page.lblfederation + '</td><td align=left>' + imgEdit.replace("category", "'federation'") + '</td></tr>';
		it3 += '<tr><td><div id="combo_federations" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'federation'") + '</td></tr>';
		it3 += '<tr><td colspan=2><div id="itfederation" name="federation">federation list</div></td></tr>';
		it3 += '</table>';

		var it4 = '<table class="innertblbusinessdata">';
		it4 += '<tr><td class="header">' + locale.main_page.lblsoftwarehouse + '</td><td align=left>' + imgEdit.replace("category", "'softwarehouse'") + '</td></tr>';
		it4 += '<tr><td><div id="combo_softwarehouse" style="width:100%;"></div></td><td>' + imgItem.replace("category", "'softwarehouse'") + '</td></tr>';
		it4 += '<tr><td colspan=2><div id="itsoftwarehouse" name="softwarehouse"></div></td></tr>';
		it4 += '</table>';

		var t = '<table class="tblbusinessdata" border=0>';
		t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEventBusinessData + '</td></tr><tr>';
		t += '<td class=tdtop>' + it1 + '</td>';
		t += '<td class=tdtop>' + it2 + '</td></tr>';
		t += '<tr><td class=tdtop>' + it3 + '</td>';
		t += '<td class=tdtop>' + it4 + '</td>';
		t += '</tr></table>';

		$("#" + container).empty().html(t);

		$.when(
			AjaxCompetitor_GetCompetitorsList("itCompetition", company_id),
			AjaxSoftware_GetSoftwaresList("itsoftware", company_id),
			AjaxFederation_GetFederationsList("itfederation", company_id),
			AjaxSoftwareHouse_GetSoftwareHousesList("itsoftwarehouse", company_id)
		).always(function () {
			$("#" + container).data('requestRunning', false);
		});

		setComboCompetitors();
		setComboSoftwares();
		setComboFederations();
		setComboSoftwareHouses();
	}
	return $.when(null);
}

var cbxCompetitor, cbxSoftware, cbxFederation, cbxSoftwarehouse;

function setComboCompetitors() {
	if (!$("#combo_competitors").data('requestRunning')) {
		$("#combo_competitors").data('requestRunning', true);

		AjaxCompetitor_GetCompetitorsPromis()
		.done(function (data) {
			$("#combo_competitors").empty();

			cbxCompetitor = new dhtmlXCombo("combo_competitors", null, null, "image");
			cbxCompetitor.setImagePath("/graphics/common/imgcombo/competitors/");
			cbxCompetitor.enableFilteringMode(true);
			cbxCompetitor.load(data, function () {
				cbxCompetitor.selectOption(0);
			});
		}).always(function () {
			$("#combo_competitors").data('requestRunning', false);
		});
	}
	return $.when(null);
}

function setComboSoftwares() {
	if (!$("#combo_softwares").data('requestRunning')) {
		$("#combo_softwares").data('requestRunning', true);

		AjaxSoftware_GetSoftwaresPromise()
		.done(function (data) {
			$("#combo_softwares").empty();

			cbxSoftware = new dhtmlXCombo("combo_softwares", null, null, "image");
			cbxSoftware.setImagePath("/graphics/common/imgcombo/softwares/");
			cbxSoftware.enableFilteringMode(true);
			cbxSoftware.load(data, function () {
				cbxSoftware.selectOption(0);
			});
		}).always(function () {
			$("#combo_softwares").data('requestRunning', false);
		});
	}
	return $.when(null);
}

function setComboFederations() {
	if (!$("#combo_federations").data('requestRunning')) {
		$("#combo_federations").data('requestRunning', true);

		AjaxFederation_GetFederationsPromise()
		.done(function (data) {
			$("#combo_federations").empty();

			cbxFederation = new dhtmlXCombo("combo_federations", null, null, "image");
			cbxFederation.setImagePath("/graphics/common/imgcombo/federations/");
			cbxFederation.enableFilteringMode(true);
			cbxFederation.load(data, function () {
				cbxFederation.selectOption(0);
			});
		}).always(function () {
			$("#combo_federations").data('requestRunning', false);
		});
	}
	return $.when(null);
}

function setComboSoftwareHouses() {
	if (!$("#combo_softwarehouse").data('requestRunning')) {
		$("#combo_softwarehouse").data('requestRunning', true);
		AjaxSoftwareHouse_GetSoftwareHousesPromise()
		.done(function (data) {
			$("#combo_softwarehouse").empty();

			cbxSoftwarehouse = new dhtmlXCombo("combo_softwarehouse", null, null, "image");
			cbxSoftwarehouse.setImagePath("/graphics/common/imgcombo/softwarehouses/");
			//cbxSoftwarehouse.setImagePath("/graphics/common/imgcombo/federations/");
			cbxSoftwarehouse.enableFilteringMode(true);
			cbxSoftwarehouse.load(data, function () {
				cbxSoftwarehouse.selectOption(0);
			});
		}).always(function () {
			$("#combo_softwarehouse").data('requestRunning', false);
		});
	}
	return $.when(null);
}

function getAffiliatedCompanies(container, id) {
	if (!$("#" + container).data('requestRunning')) {
		$("#" + container).data('requestRunning', true);
		var url = "/classes/dal/clsFederations/clsFederations.asp";

		$.get(url, { a: "GetAffiliatedCompanies", federation_id: id })
		.done(function (data) {
		    $("#" + container).empty();
		    $("#" + container).html(formatAffiliatedCompanies(data));
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Affiliated Companies",
				expire: -1,
				type: "errormessage"
			});
		}).always(function () {
			$("#" + container).data('requestRunning', false);
		});
	}
	return $.when(null);
}

function formatAffiliatedCompanies(obj) {
	var t = '';
	if (!$.isEmptyObject(obj)){
		var imglookupFed = '';
		var img = '';

		t = "<table cellpadding=1 width=100% border=0>";
		for (var i = 0; i < obj.AffiliationDetail.length; i++) {
			var id = obj.AffiliationDetail[i].federation_id;
			imglookupFed = '<img id=' + id + ' src ="/graphics/common/win_16x16/search.png" class="onmouseoverlookup" onClick="lookupFederation(this.id);">';
			img = '<img src="/graphics/common/imgcombo/federations/' + obj.AffiliationDetail[i].company_vat + '">';
			t += "<tr><td width=10%>" + imglookupFed + "</td><td width=10%>" + img + "</td><td>" + obj.AffiliationDetail[i].company_name + "</td></tr>";
		}
		t += "</table>";
	}
	return t;
}

function WindowAffiliatedCompanies(id) {
	var headerText = locale.main_page.lblAffiliatedCompanies;
	var formName = "a";
	var objWindows;
	var thisWindow;
	var formStructure = '';
	var x = 20;
	var y = 20;
	var width = 780;
	var height = 480;

	objWindows = new dhtmlXWindows();
	thisWindow = objWindows.createWindow(formName, x, y, width, height);
	objWindows.attachViewportTo(document.body);
	objWindows.window(formName).setText(headerText);
	objWindows.window(formName).center();

	var mydataset = "/classes/dal/clsFederations/clsFederations.asp?a=GetAffiliatedCompanies&federation_id=" + id;
	var dhxDataView = objWindows.window(formName).attachDataView({
		type: {
			template: "<table width=100%><tr><td width=10%>#company_vat#</td><td width=60%><b>#company_name#&nbsp;#jfc_s_desc#</b></td><td width=20%>#company_address_postcode#&nbsp;#company_address_localite#</td></tr><tr><td colspan=2>#nbc_desc#</td></tr></table>",
			padding: 2,
			height: 40,
			width: width
		},
		drag: true,
		select: true
	});
	dhxDataView.load(mydataset, "json");
}

/*ST BACKOFFICE DETAILS*/
function displayStCompanyBackOfficeDetails(company_vat, company_hbc_id, container, container2) {
	$("#" + container).empty();
	$("#" + container2).empty();
	$("#" + container + "UserInfo").empty();
	$("#" + container + "UserBillingInfo").empty();

	if (company_hbc_id !== undefined) {
		$.when(JSON_displayStCompanyBackOfficeDetails(company_vat, company_hbc_id, container))
		.done(function (data) {
			if (!$.isEmptyObject(data)) {
				JSONGetCompanyBackOfficeUserInfoPromise(company_vat, company_hbc_id, "stBackOfficeUserInfo");
				JSONGetCompanyBackOfficeUserBillingInfoPromise(company_vat, company_hbc_id, "stBackOfficeUserBillingInfo");
				JSON_displayStCompanyBackOfficeHistory(company_vat, company_hbc_id, container2);
			}
		});
	}
}

/*ST COMPANYWEB DETAILS*/
function displayStCompanyCWDetails(objEvent, container) {
	JSON_GetCompanyCompanyWebStateFromVat(objEvent.company_vat, "state");

	var strCWCompanyDetails = "<input type='hidden' name='event_id' id='event_id' value='" + objEvent.event_id + "' /><div id=CWCompanyDetails" + objEvent.company_vat + ">" + locale.main_page.CWInfoMsg + objEvent.company_vat + "</div>";
	strCWCompanyDetails = strCWCompanyDetails + "<div class=CWCyBarometer><canvas id=g" + objEvent.company_vat + "></canvas></div><div id=gv" + objEvent.company_vat + " style='visibility:hidden;position: absolute;'></div>";
	strCWCompanyDetails = strCWCompanyDetails + "<div id=CWCompanyMandates" + objEvent.company_vat + "></div>";

	$("#" + container).empty().html(strCWCompanyDetails);

	$.when(
		JSON_GetCWCompanyDetailsPromise(objEvent.company_vat),
		JSON_GetCWCompanyMandatesPromise(objEvent.company_vat)
	).done(function () {
		if ($("#gv" + objEvent.company_vat).length) {
			showGauge($("#gv" + objEvent.company_vat).text(), "g" + objEvent.company_vat);
		}
	});
}

/*EVENT DETAILS*/

function displayEventDetailsPromise(company_id, container) {
	var url = "/classes/dal/clsScheduler/events.asp";

	return $.get(url, { a: "GetEventHistory", company_id: company_id })
	.done(function (data) {
		$("#" + container).empty().html(formatEventDetails(data));
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Event Details",
			expire: -1,
			type: "errormessage"
		});
	});
}

function formatEventDetails(resp) {
	var t = '<table  width=100% border=0>';
	t += '<tr><td class="commontitle">' + locale.main_page.lblBOHistory + '</td></tr>';
	t += '<tr><td>' + resp + '</td></tr>';
	t += '</table>';

	return t;
}

var evtCalendarFilter;
function setCalendar(controlname) {
	evtCalendarFilter = new dhtmlXCalendarObject({ button: controlname });
	evtCalendarFilter.hideTime();
	evtCalendarFilter.showWeekNumbers();
	evtCalendarFilter.setDateFormat("%Y-%m-%d");

	evtCalendarFilter.attachEvent("onClick", function (date) {
		scheduler.config.xml_date = "%Y-%m-%d";
		var today = new Date(evtCalendarFilter.getFormatedDate("%Y-%m-%d", date));
		var todayplus1 = GetDateNowScheduler(today, 1);
		scheduler.config.agenda_start = new Date(today);
		scheduler.config.agenda_end = new Date(todayplus1);
		scheduler.setCurrentView(new Date(today), "agenda");
	});
}

function setComboEvtTypesForOpenTasks(ev) {
	var url = "/classes/dal/clsScheduler/events.asp";
	$.get(url, { a: "GetComboEventTypesWithImgForFunction" })
	.done(function (data) {
		// JSON string with image
		$("#combo_eventypes").empty();

		cbxEvtTyp = new dhtmlXCombo("combo_eventypes", null, null, "image");
		cbxEvtTyp.setImagePath("/graphics/common/win_16x16/");
		cbxEvtTyp.enableFilteringMode(true);
		cbxEvtTyp.load(data, function () {
			cbxEvtTyp.selectOption(0);
		});

		cbxEvtTyp.attachEvent("onChange", function (value, text) {
			var today = GetDateNowScheduler(0, 0);
			var endDate = GetDateNowScheduler(today, 1);
			var beginOftime = "1970-01-01";

			var evtview = 'agenda';
			//this is not good enough, it should filter on client instead of requery
			load_url = "../classes/dal/clsScheduler/events.asp?a=XML_GetCurrentOpenTasksForId&b=0&c=1&event_types=" + value; //webservice
			scheduler.clearAll();
			scheduler.load(load_url);

			//scheduler.config.agenda_start = new Date(curDateObj);

			scheduler.config.agenda_start = new Date(beginOftime);
			scheduler.config.agenda_end = new Date(endDate);
		});

		cbxEvtTyp.attachEvent("onSelectionChange", function () {
			//
		});
	})
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo event Types for Open Tasks",
			expire: -1,
			type: "errormessage"
		});
	});
}

function setComboEvtTypes(ev) {
	var url = "/classes/dal/clsScheduler/events.asp";

	$.get(url, { a: "GetComboEventTypesWithImgForFunction" })
	.done(function (data) {
		// JSON string with image
		$("#combo_eventypes").empty();

		cbxEvtTyp = new dhtmlXCombo("combo_eventypes", null, null, "image");
		cbxEvtTyp.setImagePath("/graphics/common/win_16x16/");
		cbxEvtTyp.enableFilteringMode(true);
		cbxEvtTyp.load(data, function () {
			cbxEvtTyp.selectOption(0);
		});

		cbxEvtTyp.attachEvent("onChange", function (value, text) {
			var today = evtCalendarFilter.getDate();
			var todayplus1 = GetDateNowScheduler(today, 1);
			var curDateObj = evtCalendarFilter.getDate();
			var evtview = 'day';
			//this is not good enough, it should filter on client instead of requery
			load_url = "../classes/dal/clsScheduler/events.asp?a=XML_GetEventsFiltered&b=0&c=1&event_type_id=" + value; //webservice
			scheduler.clearAll();
			scheduler.load(load_url);

			//scheduler.config.agenda_start = new Date(curDateObj);

			scheduler.config.agenda_start = new Date(today);
			scheduler.config.agenda_end = new Date(todayplus1);

			if (value === '%') {
				evtview = 'agenda';
			} else {
				evtview = 'day';
			}
			scheduler.setCurrentView(new Date(curDateObj), evtview);
		});
		cbxEvtTyp.attachEvent("onSelectionChange", function () {
			//
		});
	})
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Event Types",
			expire: -1,
			type: "errormessage"
		});
	});
}

function JS_GetEventHistory(scheduler, id, container) {
	if (scheduler.getState().new_event) {
		return false;
	} else {
		var ev = scheduler.getEvent(id);
		var url = "/classes/dal/clsScheduler/events.asp";

		$.ajax({
			url: [url],
			data: { a: "GetEventHistoryFromEvent", event_id: id },
			type: "GET",
			async: false
		}).done(function (data) {
			ev.my_template = "<div id=" + container + " class=eventhistory><b>" + data + "</b></div>";
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Event History",
				expire: -1,
				type: "errormessage"
			});
		});

		return true;
	}
}

/*function JS_RefreshEventHistory(id, container) {
	var url = "/classes/dal/clsScheduler/events.asp";

	$.get(url, { a: "GetEventHistoryFromEvent", event_id: id })
	.done(function (data) {
		$("#" + container).empty().html(data);
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Refreshed Event History",
			expire: -1,
			type: "errormessage"
		});
	});
}*/

function JS_SetEventCustomLightBox(scheduler) {
	//customize lightbox
	//ajax service to get event types
	// maybe calling the script like this might work
	//scheduler.Extensions.Add("../classes/dal/clsScheduler/AjaxEvents.js");
	var evt_opts = []; var evt_mails = []; var evt_users = [];

	var url = "/classes/dal/clsScheduler/events.asp";

	$.get(url, { a: "JSON_GetEventTypesForFunction" })
	.done(function (data) {
		for (var i = 0; i < data.length; i++) {
			evt_opts[i] = data[i];
		}
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Event Types",
			expire: -1,
			type: "errormessage"
		});
	});

	//get combobox data for operators list
	url = "/classes/dal/clsPerson/clsperson.asp";

	$.get(url, { i: 3 })
	.done(function (data) {
		for (var i = 0; i < data.length; i++) {
			evt_users[i] = data[i];
		}
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Operator List",
			expire: -1,
			type: "errormessage"
		});
	});

	// customize lightbox header
	scheduler.templates.lightbox_header = function (start, end, ev) {
		return "<div class=lightboxheader>" + scheduler.templates.event_header(ev.start_date, ev.end_date, ev) + "</div>";
	};

	scheduler.config.event_duration = 5; //trigger duration must be updated Event type onChange dropdown
	scheduler.config.auto_end_date = true;

	scheduler.config.buttons_right = [];

	scheduler.locale.labels.section_mailto = locale.main_page.lblEmailAddress;
	scheduler.locale.labels.section_mailoptions = locale.main_page.lblEmailOptions;

	scheduler.config.lightbox.sections = [
		{ name: locale.main_page.lblVatCompany, height: 20, type: "template", map_to: "company_vat" },
		{ name: locale.main_page.lblEventType, map_to: "event_type_id", type: "select", options: evt_opts },
		{ name: locale.main_page.lbloperator, map_to: "usr_id", type: "select", options: evt_users },
		{ name: locale.main_page.lblEventComment, height: 200, map_to: "text", type: "textarea", focus: true },
		{ name: locale.main_page.lblBOHistory, height: 200, type: "template", map_to: "my_template" },
		{ name: "time", height: 72, type: "calendar_time", map_to: "auto" }
	];
}

/*ST MENU LEADS:LEFT_PANE: LEAD_LIST*/

var myLeadsCount;
var maxLeadInList = 20;
var leadtb;
function JS_InitializeLeadToolbar(myinsidelayout) {
	AjaxLeads_CountleadsPromise()
	.done(function (data) {
		myLeadsCount = data;
		myinsidelayout.cells("a").setText(locale.main_page.leadList + ' : ' + myLeadsCount);

		if (myLeadsCount > maxLeadInList) {
			leadtb.disableItem(1);
		} else {
			leadtb.enableItem(1);
			leadtb.attachEvent("onClick", function (id) {
				AjaxLeads_Take5();
				JS_createJSONLeads_dataview(myinsidelayout, "a");
			});
		}
	});

	leadtb = myinsidelayout.cells("a").attachToolbar({
		icons_path: "/graphics/common/win_16x16/"
	});
	leadtb.setAlign("left");
	leadtb.addButton(1, 'left', 'take 5', 'dado_5.png', 'Backup-restore.png');
}

function SetDataviewTemplate() {
	var t = '';
	t += '<table width=100% border=0>';
	t += '<tr><td width=32px class=leadHeader>ID</td><td width=35pxclass=leadHeader>#lead_id#</td><td align=right><b>#lead_source_name#</b></td></tr>';
	t += '<tr><td width=32px><img src=/graphics/common/win_32x32/#lead_type_icon# /></td><td colspan=2 class=#lead_type_color#><b>#company_name#</b><br/>#company_vat#</td></tr>';
	t += '<tr><td colspan=3>#company_address_street#, #company_address_number#<br/>#company_address_postcode# #company_address_localite#<br/>tel:#company_tel# - gsm:#company_mobile#<br/>email:#company_mail#</td></tr>';
	t += '</table>';
	return t;
}

function JS_createJSONLeads_dataview(myinsidelayout, myCell, mydataset) {
	mydataset = "/classes/dal/clsLeads/clsLeads.asp?q=1";

	AjaxLeads_CountleadsPromise()
	.done(function (data) {
		myLeadsCount = data;
		myinsidelayout.cells("a").setText(locale.main_page.leadList + ' : ' + myLeadsCount);

		if (myLeadsCount > maxLeadInList) {
			leadtb.disableItem(1);
		} else {
			leadtb.enableItem(1);
		}
	});

	var dvTemplate = SetDataviewTemplate();

	myDataView = myinsidelayout.cells("a").attachDataView({
		type: {
			template: dvTemplate,
			padding: 5,
			height: 110,
			width: 270
		},
		drag: true,
		select: true
	});
	myDataView.load(mydataset, "json");

	myDataView.attachEvent("onItemDblClick", function (id) {
		var objLead = {
			lead_id: myDataView.get(id).lead_id,
			status_id: myDataView.get(id).status_id,
			user_id: myDataView.get(id).user_id,
			lang_id: myDataView.get(id).lang_id,
			source_id: myDataView.get(id).source_id,
			lead_type_id: myDataView.get(id).lead_type_id,
			backoffice_id: myDataView.get(id).backoffice_id,
			company_id: myDataView.get(id).company_id,
			prompt_date: myDataView.get(id).prompt_date,
			prompt_time: myDataView.get(id).prompt_time,
			company_name: myDataView.get(id).company_name,
			company_vat: myDataView.get(id).company_vat,
			company_address_street: myDataView.get(id).company_address_street,
			company_address_number: myDataView.get(id).company_address_number,
			company_address_postcode: myDataView.get(id).company_address_postcode,
			company_address_localite: myDataView.get(id).company_address_localite,
			company_tel: cleanUpCallNumber(myDataView.get(id).company_tel),
			company_mobile: cleanUpCallNumber(myDataView.get(id).company_mobile),
			company_mail: myDataView.get(id).company_mail,
			HBCompanyNotes: "notes",
			lead_comment: "comment"
		};

		JS_createWindow(id, myDataView, objLead);

		return true;
	});
}

/*ST MENU LEADS:DBLCLICK EVENT: POPUP LEAD DETAILS WINDOW*/
function JS_createWindow(id, myDataView, objLead) {
	var p = 0;
	var winleadlayout;
	dhxWins.forEachWindow(function () { p++; });
	if (p > 4) {
		alert(locale.main_page.alertMax5Windows);
		x = 150;//window position
		y = 80;
		return;
	}
	else {
		//myDataView.remove(myDataView.getSelected())
	}

	var w = 800;
	var h = 700;

	var win = dhxWins.createWindow(id, x, y, w, h);

	var headerText = p + 1 + ": " + objLead.company_name + " - " + locale.main_page.vat + ": " + objLead.company_vat + " (lead ID:" + objLead.lead_id + ")";
	win.setText(headerText);

	var wintoolbar = win.attachToolbar({
		icons_path: "../graphics/common/toolbars/16/"
	});

	wintoolbar.setAlign("left");
	var childMenuUrl = "../classes/dal/clsMenu/clsMenu.asp?a=DYNXML_GetToolBarChildMenu&id=" + clickedMenu;
	wintoolbar.loadStruct(childMenuUrl);
	wintoolbar.attachEvent("onClick", function (id) {
		JS_executeWintoolbarAction(id, win, objLead, myDataView);
	});

	win.keepInViewport(true);
	win.button("stick").show();

	winleadlayout = win.attachLayout("3E");
	winleadlayout.cells("a").setHeight(105);
	winleadlayout.cells("b").setHeight(200);

	winleadlayout.cells("a").setText(locale.main_page.winleadlayout_A);
	winleadlayout.cells("b").setText(locale.main_page.winleadlayout_B);
	winleadlayout.cells("c").setText(locale.main_page.winleadlayout_C);
	winleadlayout.cells("b").showInnerScroll();
	winleadlayout.cells("c").showInnerScroll();

	winleadlayout.cells("a").progressOn();
	winleadlayout.cells("b").progressOn();
	winleadlayout.cells("c").progressOn();

	var sld = "<div class=LeadDetails id=LeadDetails" + objLead.company_vat + "><table width=98% border=0>";
	sld += "<tr><td colspan=6><b>" + objLead.company_name + "</b></td></tr>";
	sld += "<tr><td class=CwCylblInfo>" + locale.main_page.lblNoEntreprise + "</td><td colspan=5>" + objLead.company_vat + "</td></tr>";
	sld += "<tr><td class=CwCylblInfo>" + locale.main_page.lblAdress + "</td><td colspan=5>" + objLead.company_address_street + " " + objLead.company_address_number + " " + objLead.company_address_postcode + " " + objLead.company_address_localite + "</td></tr>";
	sld += "<tr><td class=CwCylblInfo>" + locale.main_page.lblTel + "</td><td>" + objLead.company_tel + "</td><td class=CwCylblInfo>" + locale.main_page.lblGsm + "</td><td>" + objLead.company_mobile + "</td>";
	sld += "<td class=CwCylblInfo>" + locale.main_page.lblEmail + "</td><td>" + objLead.company_mail + "</td></tr>";
	sld += "</table></div>";
	winleadlayout.cells("a").attachHTMLString(sld);
	winleadlayout.cells("a").progressOff();

	var strHBCompanyDetails = "<div id=HBHistoriek" + objLead.company_vat + "></div><div id=HBCompanyDetails" + objLead.company_vat + ">" + locale.main_page.BackOfficeInfoMsg + objLead.company_vat + "</div>";
	strHBCompanyDetails += "<div class=STInfo id=Comments" + objLead.company_vat + "></div>";
	strHBCompanyDetails += "<div class=BOInfo id=HBCompanyNotes" + objLead.company_vat + "></div>";
	winleadlayout.cells("b").attachHTMLString(strHBCompanyDetails);

	$.when(
		JSON_GetHBCompanyDetailsPromise(objLead.company_vat, objLead.backoffice_id),
		ASPGetCompanyBackOfficeHistoryPromise(objLead.company_vat, objLead.backoffice_id),
		ASPGetLeadHistoryPromise(objLead),
		ASPGetCompanyHistoryPromise(objLead.company_vat, objLead.company_id)
	).done(function () {
		winleadlayout.cells("b").progressOff();
	});

	var strCWCompanyDetails = "<div id=CWCompanyDetails" + objLead.company_vat + ">" + locale.main_page.CWInfoMsg + objLead.company_vat + "</div>";
	strCWCompanyDetails = strCWCompanyDetails + "<div class=CWCyBarometer><canvas id=g" + objLead.company_vat + "></canvas></div><div id=gv" + objLead.company_vat + " style='visibility:hidden;position: absolute;'></div>";
	strCWCompanyDetails = strCWCompanyDetails + "<div id=CWCompanyMandates" + objLead.company_vat + "></div>";

	winleadlayout.cells("c").attachHTMLString(strCWCompanyDetails);

	$.when(
		JSON_GetCWCompanyDetailsPromise(objLead.company_vat),
		JSON_GetCWCompanyMandatesPromise(objLead.company_vat)
	).done(function () {
		if ($("#gv" + objLead.company_vat).length) {
			showGauge($("#gv" + objLead.company_vat).text(), "g" + objLead.company_vat);
		}

		winleadlayout.cells("c").progressOff();
	});

	x = x + 50;
	y = y + 30;
}

function canuserProceedPromise(company_id) {
	var url = "/classes/dal/clsScheduler/events.asp";
	return $.get(url, { a: "CanProceed", company_id: company_id })
	.fail(function (data) {
		dhtmlx.message({
			text: "Failure getting CanProceed variable",
			expire: -1,
			type: "errormessage"
		});
	});
}

function JS_executeWintoolbarAction(id, win, objLead, myDataView) {
	var canProceed = false;

	$.when(canuserProceedPromise(objLead.company_id))
	.done(function (data) {
		switch (data) {
			case "1":
				canProceed = true;
				break;
			default:
				break;
		}

		switch (id) {
			case wintoolbar.menu.leadcall:
				if (canProceed) {
					if ($("#HBHistoriek" + objLead.company_vat).length && $("#HBHistoriek" + objLead.company_vat).text().length > 0) {
						dhtmlx.confirm({
							type: "confirm-warning", ok: locale.labels.message_ok, cancel: locale.labels.message_cancel, text: locale.popup.msgalreadyevent, callback: function (result) {
								if (result) {
									JS_leadcall(objLead, win, myDataView);
								}
							}
						});
					} else {
						JS_leadcall(objLead, win, myDataView);
					}
				} else {
					dhtmlx.message({
						text: locale.popup.msgfutureevent,
						expire: -1,
						type: "errormessage"
					});
					dhtmlx.alert(locale.popup.msgfutureevent);
				}
				break;
			case wintoolbar.menu.leadtransfer:
				if (canProceed) {
					if ($("#HBHistoriek" + objLead.company_vat).length && $("#HBHistoriek" + objLead.company_vat).text().length > 0) {
						dhtmlx.confirm({
							type: "confirm-warning", ok: locale.labels.message_ok, cancel: locale.labels.message_cancel, text: locale.popup.msgalreadyevent, callback: function (result) {
								if (result) {
									JS_openformtransfer(win, objLead);
								}
							}
						});
					} else {
						JS_openformtransfer(win, objLead);
					}
				} else {
					dhtmlx.message({
						text: locale.popup.msgfutureevent,
						expire: -1,
						type: "errormessage"
					});
					dhtmlx.alert(locale.popup.msgfutureevent);
				}

				break;
			case wintoolbar.menu.leadcomment:
				if ($("#HBHistoriek" + objLead.company_vat).length && $("#HBHistoriek" + objLead.company_vat).text().length > 0) {
					dhtmlx.confirm({
						type: "confirm-warning", ok: locale.labels.message_ok, cancel: locale.labels.message_cancel, text: locale.popup.msgalreadyevent, callback: function (result) {
							if (result) {
								JS_openformhistory(win, objLead);
							}
						}
					});
				} else {
					JS_openformhistory(win, objLead);
				}
				break;
			case wintoolbar.menu.noaction:
				JS_openformnoaction(win, objLead);
				break;
			case wintoolbar.menu.quickedit:
				if (canProceed) {
					if ($("#HBHistoriek" + objLead.company_vat).length && $("#HBHistoriek" + objLead.company_vat).text().length > 0) {
						dhtmlx.confirm({
							type: "confirm-warning", ok: locale.labels.message_ok, cancel: locale.labels.message_cancel, text: locale.popup.msgalreadyevent, callback: function (result) {
								if (result) {
									JS_quickedit(objLead, win, myDataView);
								}
							}
						});
					} else {
						JS_quickedit(objLead, win, myDataView);
					}
				} else {
					dhtmlx.message({
						text: locale.popup.msgnomobileappforbasic,
						expire: -1,
						type: "errormessage"
					});
					dhtmlx.message({
						text: locale.popup.msgfutureevent,
						expire: -1,
						type: "errormessage"
					});
					dhtmlx.alert(locale.popup.msgfutureevent);
				}
				break;
			default:
				break;
		}
	});
}

function setEventCallHyperlink(calleeNumber, control) {
	var currentDate = new Date();
	var seconds = currentDate.getSeconds();
	if (seconds < 30) {
		seconds = "0";
	} else {
		seconds = "1";
	}
	var mySeed = currentDate.getMinutes() + seconds;
	var rng = new Math.seedrandom(mySeed);
	var caller = getValuefromCookie("TELESALESTOOLV2", "extension");
	var callee = '0' + calleeNumber;
	callee = cleanUpCallNumber(callee);
	var serviceurl = 'http://odm.int-outcome.be/webcall/call.asp?callerno=@caller&calleeno=@callee#' + rng.int32();
	var hlink = '<a target="callarea" href="' + serviceurl + '">' + control + '/></a>';

	return hlink;
}

function JS_EventCall(calleeNumber) {
	var currentDate = new Date();
	var seconds = currentDate.getSeconds();
	if (seconds < 30) {
		seconds = "0";
	} else {
		seconds = "1";
	}
	var mySeed = currentDate.getMinutes() + seconds;
	var rng = new Math.seedrandom(mySeed);

	var caller = getValuefromCookie("TELESALESTOOLV2", "extension");
	var callee = "0" + calleeNumber;
	callee = cleanUpCallNumber(callee);
	var serviceurl = "http://odm.int-outcome.be/webcall/call.asp?callerno=@caller&calleeno=@callee#" + rng.int32();
	serviceurl = serviceurl.replace("@caller", caller);
	serviceurl = serviceurl.replace("@callee", callee);
	//dhtmlx.alert(imgconfirm1 + serviceurl);

	window.open(serviceurl, 'callarea');
}

function GetComboEvtTypes(comboname) {
	var url = "/classes/dal/clsScheduler/events.asp";

	$.get(url, { a: "GetComboEventTypesWithImgForFunction" })
	.done(function (data) {
		// JSON string with image
		var cbxEvtTyp = new dhtmlXCombo(comboname, null, null, "image");
		cbxEvtTyp.setImagePath("/graphics/common/win_16x16/");
		cbxEvtTyp.enableFilteringMode(true);
		cbxEvtTyp.load(data, function () {
			cbxEvtTyp.selectOption(0);
		});
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Combo Event Types",
			expire: -1,
			type: "errormessage"
		});
	});
}

function JS_openformnoaction(win, objLead) {
	JS_GetCompanyFutureHistoryPromise(objLead.company_id)
	.done(function (data) {
		var formData = [
			{ type: "label", label: locale.main_page.lblnoaction },
			//{ type: "combo", label: locale.main_page.lblEventType, name: "event_type_id", inputLeft: 10, inputWidth: 300 },
			{ type: "editor", name: "text", position: "absolute", inputLeft: 10, inputWidth: 700, inputTop: 50, inputHeight: 150, value: data },
			{ type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 630, inputTop: 200 },
			{ type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 530, inputTop: 200 }
		];

		var formNoAction = win.attachForm(formData);

		formNoAction.attachEvent("onButtonClick", function (id) {
			switch (id) {
				case "btnsave":
					var text = formNoAction.getItemValue("text");
					if (text !== null && text.length > 0) {
						objLead.HBCompanyNotes = text;
						objLead.lead_comment = text;
					}

					objLead.trusted = 1;
					AjaxLeads_SetNoAction(objLead);
					win.close();
					myDataView.remove(myDataView.getSelected());
					break;
				case "btncancel":
					win.close();
					break;
				default:
			}
		});
	});
}

function JS_quickedit(objLead, win, myDataView) {
	PromoteLeadToEvent(objLead, win, myDataView, scheduler, load_url);
}

function JS_leadcall(objLead, win, myDataView) {
	var caller = getValuefromCookie("TELESALESTOOLV2", "extension");
	var callee = "0" + objLead.company_tel;
	var serviceurl = "http://odm.int-outcome.be/webcall/call.asp?callerno=@caller&calleeno=@callee#1001";
	serviceurl = serviceurl.replace("@caller", caller);
	serviceurl = serviceurl.replace("@callee", callee);
	//dhtmlx.alert(imgconfirm1 + serviceurl);
	PromoteLeadToEvent(objLead, win, myDataView, scheduler, load_url);
	window.open(serviceurl, 'callarea');
}

function JS_openformtransfer(win, objLead) {
	var formData = [
	 { type: "combo", label: locale.main_page.lblTransferLeadTo, name: "new_user_id", inputLeft: 10, inputWidth: 300 },
	 { type: "editor", name: "evt_history_text", position: "absolute", inputLeft: 10, inputWidth: 700, inputTop: 50, inputHeight: 150 },
	 { type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 630, inputTop: 200 },
	 { type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 530, inputTop: 200 }
	];

	var formTransfer = win.attachForm(formData);
	var userCombo = formTransfer.getCombo("new_user_id");
	userCombo.load("../classes/dal/clsPerson/clsperson.asp?i=2");
	userCombo.enableFilteringMode(true);
	formTransfer.attachEvent("onButtonClick", function (id) {
		switch (id) {
			case "btnsave":
				var evt_history_text = formTransfer.getItemValue("evt_history_text");
				if (evt_history_text !== null && evt_history_text.length > 0) {
					objLead.HBCompanyNotes = evt_history_text;
				}
				objLead.new_user_id = userCombo.getSelectedValue();
				objLead.trusted = 1;
				ASPTransferLead(objLead);
				win.close();
				myDataView.remove(myDataView.getSelected());
				break;
			case "btncancel":
				win.close();
				break;
			default:
		}
	});
}

function JS_openformhistory(win, objLead) {
	var notes = objLead.HBCompanyNotes;
	var comments = objLead.lead_comment;
	notes = notes.replace(/<br>/g, "");
	comments = comments.replace(/<br\s*\/?>/mg, "\n");
	comments = comments.replace(/<p\s*\/?>/mg, "\n");
	comments = comments.replace(/<\/p\s*\/?>/mg, "");
	formStructure = [
		{ type: "settings", position: "label-right" },
		{
			type: "block", width: 780, list: [
			   { type: "label", label: objLead.company_name },
			   { type: "editor", name: "evt_history_text", position: "absolute", inputLeft: 10, inputWidth: 700, inputTop: 30, inputHeight: 150 },
			   { type: "template", name: "lblST", value: "Sales Tool", position: "absolute", inputLeft: 10, inputTop: 185, inputWidth: 700 },
			   { type: "input", className: "BOInfo", name: "comment", value: comments, disabled: true, position: "absolute", inputLeft: 10, inputTop: 200, inputWidth: 700, inputHeight: 150, rows: 5 },
			   { type: "template", name: "lblBO", value: "Back Office", position: "absolute", inputLeft: 10, inputTop: 365, inputWidth: 700 },
			   { type: "input", className: "BOInfo", name: "HBCompanyNotes", value: notes, disabled: true, position: "absolute", inputLeft: 10, inputTop: 380, inputWidth: 700, inputHeight: 150, rows: 5 },
			   { type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 630, inputTop: 560 },
			   { type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 530, inputTop: 560 }
			]
		}
	];
	var myForm = win.attachForm(formStructure);

	myForm.attachEvent("onButtonClick", function (id) {
		switch (id) {
			case "btnsave":
				var evt_history_text = myForm.getItemValue("evt_history_text");
				if (evt_history_text !== null && evt_history_text.length > 0) {
					objLead.HBCompanyNotes = evt_history_text;
					ASPSetCompanyBackOfficeHistory(objLead);

					win.close();
					myDataView.remove(myDataView.getSelected());
					break;
				}
				else {
					dhtmlx.message({
						text: locale.main_page.msgemptycomment,
						expire: -1,
						type: "errormessage"
					});
					alert(locale.main_page.msgemptycomment);
				}
				break;
			case "btncancel":
				win.close();
				break;
			default:
				break;
		}
	});
}

/*UTILITIES FUNCTIONS*/
function getValuefromCookie(name, key) {
	var val;
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	var keys = parts[1].split("&");
	for (i = 0; i < keys.length; ++i) {
		val = keys[i];
		if (val.substring(0, key.length) === key) {
			var keyval = keys[i].split("=");
			var retval = keyval[1];

			break;
		}
	}
	return retval;
}

function getContractDate() {
	return locale.main_page.lblDate + ':' + GetEuroCurrentDate('/');
}

function getContractEndDate() {
	return GetEuroDatePlusXdays(0, 365, '/');
}

function SetSchedulerDateFormat(currentday, sep) {
	var result = new Date();
	if (currentday !== 0) {
		result = new Date(currentday);
	}
	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	result = yyyy + sep + mm + sep + dd + ' 00:00:00.000';
	return result;
}

function GetEuroCurrentDate(sep) {
	var result = new Date();
	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	result = dd + sep + mm + sep + yyyy;
	return result;
}

function GetEuroDatePlusXdays(currentday, numberOfDaysToAdd, sep) {
	var result = new Date();
	if (currentday !== 0) {
		result = new Date(currentday);
	}
	result.setDate(result.getDate() + numberOfDaysToAdd);
	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	result = dd + sep + mm + sep + yyyy;
	return result;
}

function parseDate(date) {
	var year = date.getFullYear();
	var month = (1 + date.getMonth()).toString();
	month = month.length > 1 ? month : '0' + month;
	var day = date.getDate().toString();
	day = day.length > 1 ? day : '0' + day;
	return year + '-' + month + '-' + day;
}

function GetDateNow() {
	var result = new Date();

	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	result = dd + "." + mm + "." + yyyy;
	return result;
}
function GetDateNowScheduler(currentday, numberOfDaysToAdd) {
	var result = new Date();
	if (currentday !== 0) {
		result = new Date(currentday);
	}
	result.setDate(result.getDate() + numberOfDaysToAdd);
	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	result = yyyy + "-" + mm + "-" + dd;
	return result;
}

function GetSalesToolDateFormat(currentday) {
	var result = new Date();
	if (!$.isEmptyObject(currentday))
	{
		if (currentday != 0)
		{
			result = new Date(currentday);
		}
	}

	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}

	result = yyyy + mm + dd;
	return result;
}
function GetSalesToolHourMinutes(sep) {
	if (sep.length === 0) {
		sep = ':';
	}
	var d = new Date();
	var h = addZero(d.getHours());
	var m = addZero(d.getMinutes());
	return h + sep + m;
}

function addZero(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}

function formatEuroDate(d) {
	if (NullUndefinedToNewVal(d, 0) === 0) { return ""; }
	var result = d;

	var dd = d.substr(6, 2);
	var mm = d.substr(4, 2);
	var yyyy = d.substr(0, 4);

	result = dd + "/" + mm + "/" + yyyy;
	return result;
}

function GetYear() {
	var result = new Date();
	var yyyy = result.getFullYear();
	return yyyy;
}

function addDays(days) {
	var result = new Date();
	result.setDate(result.getDate() + days);
	var dd = result.getDate();
	var mm = result.getMonth() + 1; //January is 0!
	var yyyy = result.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}

	if (mm < 10) {
		mm = '0' + mm;
	}
	result = dd + "." + mm + "." + yyyy;
	return result;
}

function GetTokenId() {
	var date = new Date();
	var components = [
		date.getYear(),
		date.getMonth(),
		date.getDate(),
		date.getHours(),
		date.getMinutes(),
		date.getSeconds(),
		date.getMilliseconds()
	];

	var id = components.join("");
	return id;
}
function GetStCompanyObjectPromise(company_id) {
	var url = "/classes/dal/clsCompany/clsCompany.asp";
	return $.get(url, { a: "JSONGetCompanyInfo", company_id: company_id })
	.done(function (data) {
	}).fail(function (data) {
		dhtmlx.message({
			text: "Failure getting Company Object",
			expire: -1,
			type: "errormessage"
		});
	});
}
function anyChar(val) {
	return /\S+/.test(val);
}

function anyDigit(val) {
	return /^\d+$/.test(val);
}

function IsvalidEmail(src) {
	var re = /^[a-zA-Z0-9&._-]+@[a-zA-Z0-9&._-]+\.[a-zA-Z]{2,6}$/;
	re.test(src);
}
function castDbLang(intlang) {
	var ret;
	if (intlang === 1) {
		ret = 'FR';
	} else {
		ret = 'NL';
	}
	return ret;
}

function NullUndefinedToString(value) {
	return value === null ? "" : value;
}
function NullUndefinedToNewVal(value, NewVal) {
	return value === null ? NewVal : value;
}

function cleanUpCallNumber(o) {
	var n = o;
	n = n.replace(/ /g, '');
	n = n.replace(/-/g, '');
	n = n.replace(/\+/g, '00');
	n = n.replace(/\//g, '');
	n = n.replace(/\./g, '');	
	n = n.replace(/\-/g, '');
	return n;
}

function JSONEscape(value) {
	var n = value;
	n = n.replace(/"/g, '\"');
	n = n.replace(/\\/g, '\\');
	n = n.replace(/\//g, '\/');
	n = n.replace(String.fromCharCode(10), '');
	n = n.replace(String.fromCharCode(13), '');
}