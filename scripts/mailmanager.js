﻿var insideLayout = '';
var dbmail = '';
var objMail = '';

function MM_DisplayMailEngineSetLayout()
{
    var tbl = '<table width=100% cellspacing=2 cellpadding=2 border=0>';
        tbl += '<tr>';
            tbl += '<td rowspan=4 style="width:50px;" class="btnSendMail">&nbsp;</td>';
            tbl += '<td class="commontitle"  style="width:40px;">' + locale.main_page.lblTo + '...</td>';
            tbl += '<td><input id=MailTo name=MailTo style="width:100%;"></td>';
        tbl += '</tr>';
        tbl += '<tr>';
            tbl += '<td class="commontitle"  style="width:40px;">' + locale.main_page.lblCC + '...</td>';
            tbl += '<td><input id=MailCC name=MailCC style="width:100%;"></td>';
        tbl += '</tr>';
        tbl += '<tr>';
            tbl += '<td class="commontitle"  style="width:40px;">' + locale.main_page.lblBCC + '...</td>';
            tbl += '<td><input id=MailBCC name=MailBCC style="width:100%;"></td>';
        tbl += '</tr>';
        tbl += '<tr>';
            tbl += '<td class="commontitle"  style="width:40px;">' + locale.main_page.lblSubject + '</td>';
            tbl += '<td><input id=Subject name=Subject style="width:100%;"></td>';
        tbl += '</tr>';
        tbl += '<tr>';
            tbl += '<td class="commontitle" colspan=3>' + locale.main_page.LblAttached + '</td>';
        tbl += '</tr>';
        tbl += '<tr>';
            tbl += '<td colspan=3><div id="data_container" style="border:1px solid #A4BED4; background-color:white;width:100%;"></div></td>';
        tbl += '</tr>';
        tbl += '<tr>';
            tbl += '<td colspan=3><div id="displayMailObj" style="border:1px solid #A4BED4;width:100%;height:735px;"></div></td>';
        tbl += '</tr>';
    tbl += '</table>';

    return tbl;
}

function MM_InitializeMailTemplateEngine(myinsidelayout) {
    myinsidelayout = myLayout.cells("a").attachLayout({ pattern: '2U' });
    myinsidelayout.cells("a").setText(locale.main_page.lblSearchCriteria);
    myinsidelayout.cells("b").setText(locale.main_page.lblSearchResult);
    myinsidelayout.cells("a").setWidth(350);
    MM_MailSearchEngine(myinsidelayout);
}
function MM_InitializeMailSearchEngine(myinsidelayout) {
    myinsidelayout = myLayout.cells("a").attachLayout({ pattern: '3J' });
    myinsidelayout.cells("a").setText(locale.main_page.lblSearchCriteria);
    myinsidelayout.cells("b").setText(locale.main_page.lblDisplay);
    myinsidelayout.cells("c").setText(locale.main_page.lblSearchResult);
    myinsidelayout.cells("a").setWidth(350);
    myinsidelayout.cells("a").setHeight(300);
    myinsidelayout.cells("b").collapse();
    MM_MailSearchEngine(myinsidelayout);
}
function MM_MailTemplateEngine(myinsidelayout) {
    insidelayout = myinsidelayout;
    var strA = '<p class="commontitle">' + locale.main_page.lblMailTemplate + '</p><div id=combo_mailtemplates style="width:280px;"></div>'
    strA += '<p class="commontitle">' + locale.main_page.lblEntreprise + '</p><div id=combo_companies style="width:280px;"></div>'
    strA += '<p class="commontitle">' + locale.main_page.lblVatCompany + '</p><div id=combo_companies_vat style="width:280px;"></div>'
    myinsidelayout.cells("a").attachHTMLString(strA);
    var resultGrid = myinsidelayout.cells("b").attachGrid();
    setComboMailTemplates(resultGrid);
    setComboCompanies(resultGrid);
    setComboCompaniesVAT(resultGrid);
    ConfigResultGrid(resultGrid);

    resultGrid.attachEvent("onRowSelect", function (id, ind) {
        //var mail = resultGrid.cells(id, 1).getValue();
        Ajax_GetEMailPromise(id)
        .done(function (data) {
            objMail = {
				mail_template_id: $("#mail_template_id").val(),
                Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
                MailTo: resultGrid.cells(id, 4).getValue(),
                MailCC: resultGrid.cells(id, 5).getValue(),
                MailBCC: resultGrid.cells(id, 6).getValue(),
                Subject: resultGrid.cells(id, 2).getValue(),
                Body: data,
                IsHTML: true,
                Attachment: resultGrid.cells(id, 7).getValue(),
            };

            var imgPrint = "<img id='sendmailbutton' src='/graphics/common/win_16x16/email.png' onClick='SendMail(objMail);' class=onmouseovermail>";
            var btnPrint = '<input class ="sendbutton" type="button" value="' + locale.main_page.lblSendMail + '">  ';
            var cellHeader = '&nbsp;&nbsp;&nbsp;' + locale.main_page.lblSubject + ' : ' + objMail.Subject + '  |  ' + locale.main_page.lblAdressee + ' : ' + objMail.MailTo // + ' ' + resultGrid.cells(id, 6).getValue() + ' ' + resultGrid.cells(id, 7).getValue()

            var mail = '<div id="objEmail">' + data + '</div>';
            myinsidelayout.cells("b").attachHTMLString(mail);
            myinsidelayout.cells("a").setWidth(350);

            myinsidelayout.cells("b").setText(imgPrint + cellHeader);
            myinsidelayout.cells("b").expand();
        });
    });
}

function MM_MailSearchEngine(myinsidelayout) {
    insidelayout = myinsidelayout;
    var strA = '<p class="commontitle">' + locale.main_page.lblMailTemplate + '</p><div id=combo_mailtemplates style="width:280px;"></div>'
    strA += '<p class="commontitle">' + locale.main_page.lblEntreprise + '</p><div id=combo_companies style="width:280px;"></div>'
    strA += '<p class="commontitle">' + locale.main_page.lblVatCompany + '</p><div id=combo_companies_vat style="width:280px;"></div>'
    myinsidelayout.cells("a").attachHTMLString(strA);
    var resultGrid = myinsidelayout.cells("c").attachGrid();
    setComboMailTemplates(resultGrid);
    setComboCompanies(resultGrid);
    setComboCompaniesVAT(resultGrid);
    ConfigResultGrid(resultGrid);

    resultGrid.attachEvent("onRowSelect", function (id, ind) {
        //var mail = resultGrid.cells(id, 1).getValue();
        Ajax_GetEMailPromise(id)
        .done(function (data) {
            objMail = {
				mail_template_id: $("#mail_template_id").val(),
                Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
                MailTo: resultGrid.cells(id, 4).getValue(),
                MailCC: resultGrid.cells(id, 5).getValue(),
                MailBCC: resultGrid.cells(id, 6).getValue(),
                Subject: resultGrid.cells(id, 2).getValue(),
                Body: data,
                IsHTML: true,
                Attachment: resultGrid.cells(id, 7).getValue(),
            };

            var imgPrint = "<img id='sendmailbutton' src='/graphics/common/win_16x16/email.png' onClick='SendMail(objMail);' class=onmouseovermail>";
            var btnPrint = '<input class ="sendbutton" type="button" value="' + locale.main_page.lblSendMail + '">  ';
            var cellHeader = '&nbsp;&nbsp;&nbsp;' + locale.main_page.lblSubject + ' : ' + objMail.Subject + '  |  ' + locale.main_page.lblAdressee + ' : ' + objMail.MailTo // + ' ' + resultGrid.cells(id, 6).getValue() + ' ' + resultGrid.cells(id, 7).getValue()

            var mail = '<div id="objEmail">' + data + '</div>';
            myinsidelayout.cells("b").attachHTMLString(mail);
            myinsidelayout.cells("a").setWidth(350);

            myinsidelayout.cells("b").setText(imgPrint + cellHeader);
            myinsidelayout.cells("b").expand();
        });
    });
}

function SendMail(objMail) {
    return AjaxMailer_reSendMail(objMail);
}

function ConfigResultGrid(resultGrid) {
    var tbHeader = locale.main_page.lblEntreprise + ',' + locale.main_page.wintoolbarToText + ',' + locale.main_page.lblSubject + ',' + locale.main_page.lblDate + ',' + locale.main_page.lblEmailAddress + ',1,2,3'
    resultGrid.setHeader(tbHeader);
    resultGrid.setInitWidths("250,200,400,130,250,0,0,0");
    resultGrid.setColAlign("left,left,left,left,left,left,left,left");
    resultGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
    resultGrid.setColSorting("str,str,str,str,str,str,str,str");
    resultGrid.init();
}

function setComboMailTemplates(resultGrid) {
    Ajax_GetComboMailTemplatesWithImg()
    .done(function (data) {
        cbxMailTemplates.load(data, function () {
            cbxMailTemplates.selectOption(0);
			$("#mail_template_id").val(cbxMailTemplates.getSelectedValue());
        });
    });
    $("#combo_mailtemplates").empty();
    cbxMailTemplates = new dhtmlXCombo("combo_mailtemplates", null, null, "image");
    cbxMailTemplates.setImagePath("/graphics/common/win_16x16/");
    cbxMailTemplates.enableFilteringMode(true);
    cbxMailTemplates.allowFreeText(false);


    cbxMailTemplates.attachEvent("onChange", function (value, text) {
		$("#mail_template_id").val(cbxMailTemplates.getSelectedValue());
        insidelayout.cells("b").collapse();
        insidelayout.cells("b").setText(locale.main_page.lblDisplay);
        resultGrid.clearAll();

        Ajax_GetMailsPromise('mail_template_id', value)
        .done(function (data) {
            resultGrid.parse(data, "json");
        })
        
    });
}

function setComboCompanies(resultGrid) {
    Ajax_GetComboCompaniesPromise('company_name')
    .done(function(data)
    {
        cbxCompany.load(data, function () {
            cbxCompany.selectOption(0);
        });
    });

    $("#combo_companies").empty();
    cbxCompany = new dhtmlXCombo("combo_companies", null, null);
    cbxCompany.enableFilteringMode(true);
    cbxCompany.allowFreeText(false);


    cbxCompany.attachEvent("onChange", function (value, text) {
        insidelayout.cells("b").collapse();
        insidelayout.cells("b").setText(locale.main_page.lblDisplay);
        resultGrid.clearAll();

        Ajax_GetMails('company_id', value)
        .done(function(data)
        {
            resultGrid.parse(data, "json");
        })
        
    });
}

function setComboCompaniesVAT(resultGrid) {
    Ajax_GetComboCompaniesPromise('company_vat')
    .done(function(data)
    {
        cbxCompanyvat.load(data, function () {
            cbxCompanyvat.selectOption(0);
        });
    })

    $("#combo_companies_vat").empty();
    cbxCompanyvat = new dhtmlXCombo("combo_companies_vat", null, null);
    cbxCompanyvat.enableFilteringMode(true);
    cbxCompanyvat.allowFreeText(false);

    cbxCompanyvat.attachEvent("onChange", function (value, text) {
        insidelayout.cells("b").collapse();
        insidelayout.cells("b").setText(locale.main_page.lblDisplay);
        resultGrid.clearAll();

        Ajax_GetMailsPromise('company_vat', text)
        .done(function (data) {
            resultGrid.parse(data, "json");
        });
        
    });
}

//AJAX GRID RESULTSET
function Ajax_GetMailsPromise(fieldname, fieldvalue) {
    var url = "/classes/dal/clsMailTemplates/clsMailTemplate.asp";

    return $.get(url, { a: "GetMails", [fieldname]: fieldvalue })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Mails",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetEMailPromise(fieldvalue) {
    var url = "/classes/dal/clsMailTemplates/clsMailTemplate.asp";
    
    return $.get(url, { a: "GetEMail", mail_id: fieldvalue })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Email",
            expire: -1,
            type: "errormessage"
        });
    });
}

//AJAX DROPDOWNS
function Ajax_GetComboMailTemplatesWithImgPromise() {
    var url = "/classes/dal/clsMailTemplates/clsMailTemplate.asp";

    return $.get(url, { a: "GetComboMailTemplatesWithImg" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Combo Mail templates With Image",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetComboCompaniesPromise(fieldname) {
    var url = "/classes/dal/clsCompany/clsCompany.asp";
    
    return $.get(url, {a: "GetComboCompaniesMailTemplates", fieldname: fieldname})
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Combo Companies",
            expire: -1,
            type: "errormessage"
        });
    });
}