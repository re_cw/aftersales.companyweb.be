var insidelayout;
var InhoudingsGrid;
var ManueleGrid;
var DagvGrid;
function MM_ExamplesEngineInitialize(myinsidelayout) {
    insidelayout = myinsidelayout;

    myinsidelayout.cells("a").setText(locale.examples.dagv);
    myinsidelayout.cells("a").progressOn();
    myinsidelayout.cells("b").setText(locale.examples.inh);
    myinsidelayout.cells("b").progressOn();
    myinsidelayout.cells("c").setText(locale.examples.man);
    myinsidelayout.cells("c").progressOn();

    InhoudingsGrid = insidelayout.cells("b").attachGrid();
    ManueleGrid = insidelayout.cells("c").attachGrid();
    DagvGrid = insidelayout.cells("a").attachGrid();

    GetDagvaardingenPromise()
    .done(function (data) {
        ConfigGrid(DagvGrid, data);
        myinsidelayout.cells("a").progressOff();
    });
    GetManueleEntriesPromise()
    .done(function (data) {
        ConfigGrid(ManueleGrid, data);
        myinsidelayout.cells("c").progressOff();
    });
    GetInhoudingsPlichtPromise()
    .done(function (data) {
        ConfigGrid(InhoudingsGrid, data);
        myinsidelayout.cells("b").progressOff();
    });
}

function ConfigGrid(grid, json) {
    var tbHeader = locale.examples.date + ",";
    tbHeader += locale.examples.vat + ",";
    tbHeader += locale.examples.name + ",";
    tbHeader += locale.examples.form + ",";
    tbHeader += locale.examples.town + ",";
    tbHeader += locale.examples.zip + ",";
    tbHeader += locale.examples.moreinfo + "";

    grid.setHeader(tbHeader);
    grid.setInitWidths("60,70,400,60,100,60,400");
    grid.setColAlign("left,left,left,left,left,left, left");
    grid.setColTypes("ro,ed,ro,ro,ro,ro,ro");
    grid.setColSorting("str,str,str,str,str,str,str");
    grid.enableEditEvents(true, true, true);

    grid.init();

    grid.clearAll();
    grid.parse(json, "json");
}

function GetInhoudingsPlichtPromise() {
    var url = "/classes/dal/clsExamples/examples.asp";

    return $.get(url, { a: "JSON_GetInhoudingen" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting InhoudingsPlicht",
            expire: -1,
            type: "errormessage"
        });
    });
}
function GetManueleEntriesPromise() {
    var url = "/classes/dal/clsExamples/examples.asp";

    return $.get(url, { a: "JSON_GetManueel" })
        .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Manuel Entries",
            expire: -1,
            type: "errormessage"
        });
    });;
}
function GetDagvaardingenPromise() {
    var url = "/classes/dal/clsExamples/examples.asp";

    return $.get(url, { a: "JSON_GetDagvaardingen" })    
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Dagvaardingen",
            expire: -1,
            type: "errormessage"
        });
    });;
}