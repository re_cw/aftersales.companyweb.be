﻿var insideLayout = '';
var mysublayout01 = '';
var curDateObj = '';
var qGrid = '';

var userid = "0";

function QM_InitializeTrialEngine(myinsidelayout) {
    insidelayout = myinsidelayout;
    userid = getValuefromCookie("TELESALESTOOLV2", "usrId");
    myinsidelayout.cells("a").setText(locale.main_page.lblDirtyLeads);
    myinsidelayout.cells("b").setText("TeleSales V02.00");
    myinsidelayout.cells("c").setText("BackOffice");
    myinsidelayout.cells("d").setText("Details");
    myinsidelayout.cells("a").setHeight(200);
    myinsidelayout.cells("d").setHeight(180);
    myinsidelayout.cells("a").showInnerScroll();
    myinsidelayout.cells("b").showInnerScroll();
    myinsidelayout.cells("c").showInnerScroll();

    mysublayout01 = myinsidelayout.cells("d").attachLayout({ pattern: '2U' });
    mysublayout01.cells("a").setText("TeleSales V02.00");
    mysublayout01.cells("b").setText("BackOffice");
    mysublayout01.cells("a").showInnerScroll();
    mysublayout01.cells("b").showInnerScroll();

    qGrid = myinsidelayout.cells("a").attachGrid();
    ConfigqGrid(qGrid);
    GetLeadQueue(qGrid);
}

function ConfigqGrid(qGrid) {
    var tbHeader = 'id,' + locale.main_page.lblEntreprise + ',' + locale.main_page.lblContact + ',' + locale.main_page.lblVatCompany + ',' + locale.main_page.lblCallNumber + ',@'
    qGrid.setColumnHidden(0, true);
    qGrid.setHeader(tbHeader);
    qGrid.setInitWidths("40,400,400,100,100,30");
    qGrid.setColAlign("left,left,left,left,left,left");
    qGrid.setColTypes("ro,ed,ed,ed,ed,ro");
    qGrid.setColSorting("str,str,str,str,str,img");
    qGrid.init();
    qGrid.attachEvent("onRowSelect", doOnRowSelectqGrid);
}

function doOnRowSelectqGrid(id, ind) {
    switch (ind){
        case 1:
            var o = 3;
            var c = escape(qGrid.cells(id, 1).getValue());
            LQ_GetTsGridCompanyByCompanyName(o, c);
            LQ_GetBoGridCompanyByCriteria(o, c, "HBC_FirmName");
            break;

        case 3:
            var company_vat = qGrid.cells(id, 3).getValue();
            company_vat = NullUndefinedToNewVal(company_vat, 0)
            if (company_vat != 0) { populateDirtyLeadEditor(company_vat) };
            return true;
            break;
        case 4:
            alert(qGrid.cells(id, 4).getValue())
        default:
            break;
    }
}

function GetLeadQueue(qGrid) {
    qGrid.clearAll();
    Ajax_GetLeadQueue(qGrid);
}

function Ajax_GetLeadQueue(qGrid) {
    dhtmlx.message({
        text: "Getting Leads Queue",
        expire: 500,
        type: "noticemessage"
    });

    var url = "/classes/dal/clsLeadQueue/clsLeadQueue.asp";

    $.get(url,{q:1})
    .done(function(data){
        qGrid.parse(data, "json");
    })
    .fail(function(resp){
        dhtmlx.message({
            text: "Error getting LeadQueue.",
            expire: -1,
            type: "errormessage"
        });
    });
}

function LeadQueueCall(calleeNumber) {
    var caller = getValuefromCookie("TELESALESTOOLV2", "extension");
    var callee = "0" + calleeNumber;
    var serviceurl = "http://odm.int-outcome.be/webcall/call.asp?callerno=@caller&calleeno=@callee#1001"
    serviceurl = serviceurl.replace("@caller", caller);
    serviceurl = serviceurl.replace("@callee", callee);
    dhtmlx.confirm({
        type:"confirm",
        text: imgconfirm1 + serviceurl,
        callback: function (result) {
            if (result == true) {window.open(serviceurl, 'callarea');
            };
        }
    });

    //window.open(serviceurl, 'callarea');
}

function LQ_GetCompaniesBackOfficeInfoByVatNumberPromise(company_vat) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
    
    return $.get(url, {q: 18, vat: company_vat})
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Companies Backoffice by VatNumber",
            expire: -1,
            type: "errormessage"
        });
    });
}

function populateDirtyLeadEditor(company_vat) {
    LQ_GetCompaniesBackOfficeInfoByVatNumberPromise(company_vat)
    .done(function(data){
        var lqFormStructure = [
            { type: "settings", position: "label-top" },
            { type: "label", label: locale.main_page.lblEventBackOffice },
            { type: "input", name: "HBC_FirmName", label: locale.main_page.lblEntreprise, value: objBackOffice[0].HBC_FirmName},
    
            { type: "input", name: "HBC_ID", label: 'BackOffice ID', value: objBackOffice[0].HBC_ID },
    
            { type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 470, inputTop: 440 },
            { type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 370, inputTop: 440 },
        ];

        var lqForm = myinsidelayout.cells("b").attachForm(lqFormStructure);

        lqForm.attachEvent("onButtonClick", function (id) {
            var res, num1, num2;
            num1 = lqForm.getItemValue("HBC_FirmName");
            num2 = lqForm.getItemValue("HBC_ID");
    
            alert(num1 + ' ' + num2);
        })
    });

}

//TELESALES INFOS PROCESSING
function LQ_GetTsGridCompanyByCompanyName(o, c) {
    resetResultContainers();
    tsGrid = mysublayout01.cells("a").attachGrid();
    LQ_ConfigGridTsCompanyByCompanyName(tsGrid);
    tsGrid.clearAll();

    GetTsCompanyByCompanyName(o, c)
    .done(function(data){
        if (data.length > 0){
            tsGrid.parse(data, "json");
        }
    });
}

function LQ_ConfigGridTsCompanyByCompanyName(tsGrid) {
    var tbHeader = 'Cy ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.vat + ',' + locale.main_page.lblTelephone + ',Event ID,BO ID,hbc_id'
    tsGrid.setColumnHidden(6, true);
    tsGrid.setHeader(tbHeader);
    tsGrid.setInitWidths("60,260,80, 90,60,60");
    tsGrid.setColAlign("left,left,left,left,left,left");
    tsGrid.setColTypes("ro,ro,ro,ro,ro,ed");
    tsGrid.setColSorting("str,str,str,str,str,str");
    tsGrid.init();
    tsGrid.attachEvent("onRowSelect", LQ_ts_doOnRowSelect);
}

function LQ_ts_doOnRowSelect(id, ind) {
    var company_id = tsGrid.cells(id, 0).getValue();
    LQ_GetTsCompanyByCompanyID(company_id);
    return true;
}

function LQ_GetTsCompanyByCompanyID(company_id) {
    resetResultContainers();
    if (anyDigit(company_id) != true) {
        dhtmlx.message({
            text: locale.popup.msgusedigitsonly,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(anino + locale.popup.msgusedigitsonly); return false
    }

    AjaxGetTsCompanyByCompanyIDPromise(company_id)
    .done(function(data){
        if (data.length > 0)
        {
            var formname = 'Form_TsCompanyEditor';
            myinsidelayout.cells("b").attachHTMLString(SetFormDiv_TsCompanyEditor(formname));
            DesignForm_TsCompanyEditor(formname, myinsidelayout, "b", result);
        }else{
            myinsidelayout.cells("b").attachHTMLString(anino);
        }
    });
}

function LQ_GetTsCompanyByBackOfficeID(boid) {
    var theCell = myinsidelayout.cells("c");
    resetResultContainers();
    AjaxGetTsCompanyByBackOfficeIDPromise(boid)
    .done(function(data){
        var formname = 'Form_TsCompanyEditor';
        myinsidelayout.cells("b").attachHTMLString(SetFormDiv_TsCompanyEditor(formname));
        DesignForm_TsCompanyEditor(formname, myinsidelayout, "b", result);
    }).fail(function(resp){
        myinsidelayout.cells("b").attachHTMLString(anino)
    });

    GetCompanyBackOfficeInfoByCompany_hbc_id(boid, theCell);
}

function SetFormDiv_TsCompanyEditor(formname) {
    f = '';
    f += '<div id="' + formname + '" style="width:100%;height:500px;"></div>';
    return f;
}

function DesignForm_TsCompanyEditor(formname, targetlayout, targetCell, result) {
    var genderOpts = [{ value: "M", text: "M" }, { value: "F", text: "F" }];
    var langOpts = [{ value: "1", text: "FR" }, { value: "2", text: "NL" }, { value: "3", text: "EN" }];
    var typeOpts = [];
    GetComboContactTypesPromise()
    .done(function(data){
        for (var j = 0; j < data.type.length; j++) {
            typeOpts.push({ value: data.type[j].value, text: data.type[j].text });
        }
    
        var titleOpts = []
        GetComboContactTypesPromise().
        done(function(data){
            for (var j = 0; j < data.title.length; j++) {
                titleOpts.push({ value: data.title[j].value, text: data.title[j].text });
            }
        });

        formStructure = [
        { type: "settings", position: "label-top" },
        {type: "fieldset", name: "company", offsetLeft: 10, label: locale.main_page.lblEntreprise,
            list: [
                   { type: "input", name: 'company_id', label: 'CY ID', value: result[0].company_id, inputWidth: 75, readonly: true },
                   { type: "input", name: 'company_name', label: locale.main_page.lblEntreprise, value: result[0].company_name, inputWidth: 200, required: true},
                   { type: "input", name: 'company_email', label: locale.main_page.lblEmailAddress, value: result[0].company_email, inputWidth: 200 },
                   { type: "newcolumn" },
                   { type: "input", name: 'company_hbc_id', label: 'BO ID', value: result[0].company_hbc_id, inputWidth: 75, offsetLeft:10 },
                   { type: "input", name: 'company_tel', label: locale.main_page.lblTelephone, value: result[0].company_tel, inputWidth: 100, required: true, offsetLeft: 10 },
                   { type: "newcolumn" },
                   { type: "input", name: 'company_vat', label: locale.main_page.lblVatCompany, value: result[0].company_vat, inputWidth: 100, required: true, offsetLeft: 10 },
                   { type: "input", name: 'company_mobile', label: locale.main_page.lblmobile, value: result[0].company_mobile, inputWidth: 100, offsetLeft: 10 },
                   { type: "newcolumn" },
                   { type: "combo", label: locale.main_page.lblLang, name: "company_lang_id", offsetLeft: 10, inputWidth: 50, required: true,
                       options: [{text: "FR", value: "1"}, {text: "NL", value: "2"}, {text: "EN", value: "3"}]},
            ]
        },
        {type: "fieldset", name: "company_address", offsetLeft: 10, label: locale.main_page.lblAdresstitle,
            list: [
                    { type: "input", name: 'company_address_id', label: 'CYAddr ID', value: result[0].company_id, inputWidth: 75, readonly: true, hidden:true },
                    { type: "input", name: 'company_address_street', label: locale.main_page.lblAdresstitle, value: result[0].company_email, inputWidth: 400, required: true },
                    { type: "input", name: 'company_address_boxnumber', label: 'box number', value: result[0].company_id, inputWidth: 50},
                    { type: "input", name: 'company_address_localite', label: 'localite', value: result[0].company_id, inputWidth: 400 },

                    { type: "newcolumn" },
                    { type: "input", name: 'company_address_number', label: 'number', value: result[0].company_id, inputWidth: 50, offsetLeft: 10, required: true },
                    { type: "input", name: 'company_address_floor', label: 'floor', value: result[0].company_id, inputWidth: 50, offsetLeft: 10 },
                    { type: "input", name: 'company_address_postcode', label: 'post code', value: result[0].company_id, inputWidth: 50, offsetLeft: 10 },
            ]
        },
         {
             type: "fieldset", name: "company_contact", offsetLeft: 10, label: locale.main_page.lblEventContact,
             list: [
                     { type: "input", name: 'contact_id', label: 'contact_id', value: result[0].company_id, inputWidth: 75, readonly: true, hidden: true },
                     { type: "select", name: "contact_title", label: locale.main_page.lblTitle, options: titleOpts, value: result[0].company_id, required: true, inputWidth: 300 },
                     { type: "input", name: 'contact_firstname', label: locale.main_page.lblFirstName, value: result[0].company_email, inputWidth: 300, required: true },
                     { type: "input", name: 'contact_lastname', label: locale.main_page.lblLastName, value: result[0].company_id, inputWidth: 300 },
                     { type: "input", name: 'contact_tel', label: locale.main_page.lblTelephone, value: result[0].company_id, inputWidth: 300 },
                     { type: "input", name: 'contact_mobile', label: locale.main_page.lblmobile, value: result[0].company_id, inputWidth: 300 },
                     { type: "input", name: 'contact_email', label: locale.main_page.lblEmailAddress, value: result[0].company_id, inputWidth: 300 },

                     { type: "newcolumn" },
                     { type: "select", name: "contact_type", label: locale.main_page.lblContactType, options: typeOpts, value: result[0].company_id, required: true, inputWidth: 100, offsetLeft: 10 },
                     { type: "select", name: "contact_lang_radical", label: locale.main_page.lblLang, options: langOpts, value: result[0].company_id, required: true, inputWidth: 50, offsetLeft: 10 },
                     { type: "select", name: "contact_gender", label: locale.main_page.lblGender, options: genderOpts, value: result[0].company_id, required: true, inputWidth: 50, offsetLeft: 10 },

             ]
         },
         { type: "button", name: "btndock", value: "dock", position: "absolute", inputLeft: 10, inputTop: 730 },
         { type: "button", name: "btnundock", value: "undock", position: "absolute", inputLeft: 80, inputTop: 730 },

         { type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 328, inputTop: 730 },
         { type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 428, inputTop: 730 },
        ];

        var myForm = new dhtmlXForm(formname, formStructure);
        myForm = targetlayout.cells(targetCell).attachForm(formStructure);

        myForm.attachEvent("onButtonClick", function (buttonid) {
            switch (buttonid) {
                case "btndock":
                    dockCell();
                    break;
                case "btnundock":
                    undockCell();
                    break;
                case "btnsave":
                    break;
                default:
                    break;
            }
        });
    });
}

function dockCell() {
    myinsidelayout.cells("b").dock();
    //myLayout.cells(getId()).dock();
}
function undockCell() {
    myinsidelayout.cells("b").undock(300, 10, 540, 810);
    //myLayout.cells(getId()).undock(550, 20, 400, 300);
}

function LQ_GetBoGridCompanyByCriteria(o, fieldvalue, fieldname) {
    resetResultContainers();
    boGrid = mysublayout01.cells("b").attachGrid();
    LQ_ConfigGridBoCompanyByCriteria(boGrid);
    boGrid.clearAll();

    GetBackOfficeCompanyByCriteriaPromise(o, fieldvalue, fieldname)
    .done(function(data)
    {
        if (data.rows.length > 0) {
            boGrid.parse(data, "json")
        }
    });
    ;
}

function LQ_ConfigGridBoCompanyByCriteria(boGrid) {
    var tbHeader = 'BO ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.vat + ',' + locale.main_page.lblEventContact + ',' + locale.main_page.lblTelephone + ',' + locale.main_page.lblGsm + ',Adresse'
    //tsGrid.setColumnHidden(5, true);
    //boGrid.setColumnHidden(6, true);
    boGrid.setHeader(tbHeader);
    boGrid.setInitWidths("60,260,80, 90,60,60,60");
    boGrid.setColAlign("left,left,left,left,left,left,left");
    boGrid.setColTypes("ed,ed,ed,ed,ed,ed,ed");
    boGrid.setColSorting("str,str,str,str,str,str,str");
    boGrid.init();
    boGrid.attachEvent("onRowSelect", LQ_bo_doOnRowSelect);
}

function LQ_bo_doOnRowSelect(id, ind) {
    var curBoid = boGrid.cells(id, 0).getValue();
    curBoid = parseInt(curBoid);

    //GetCompanyBackOfficeInfoByCompany_hbc_id(curBoid);
    LQ_GetTsCompanyByBackOfficeID(curBoid);
    return true;
}