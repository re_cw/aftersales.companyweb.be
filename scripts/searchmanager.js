var insideLayout = '';
var mysublayout01 = '';
var SearchCal = '';
var curDateObj = '';
var SearchGrid = '';

var tsGrid='';
var boGrid='';
var cwGrid='';

function SE_InitializeSearchEngine(myinsidelayout,id) {
    insidelayout = myinsidelayout;
    myinsidelayout.cells("a").setText(myToolbar.getItemText(id));
    myinsidelayout.cells("b").setText("TeleSales V02.00A");
    myinsidelayout.cells("c").setText("BackOffice");
    //myinsidelayout.cells("d").setText("SalesTool V01.00");
    myinsidelayout.cells("d").setText("Details");
    myinsidelayout.cells("a").setHeight(152);
    myinsidelayout.cells("a").attachHTMLString(SE_InitializeQueryControls(myinsidelayout));
    myinsidelayout.cells("b").showInnerScroll();
    myinsidelayout.cells("c").showInnerScroll();
    myinsidelayout.cells("d").setHeight(150);

    mysublayout01 = myinsidelayout.cells("d").attachLayout({ pattern: '2U' });
    mysublayout01.cells("a").setText("TeleSales V02.00");
    mysublayout01.cells("b").setText("BackOffice");
}

function SE_InitializeQueryControls(myinsidelayout) {
    var s;
    s = '';

    s += '<table width=100% cellspacing=0px cellpadding=0px style="background-color:#cdcdcd;">'
    s += '<tr><td>' + SetIDCollectionSearchTemplate() + '</td></tr>'
    s += '<tr><td>' + SetVarCollectionSearchTemplate() + '</td></tr>'
    s +='</table>'

    return s;
}

function SetIDCollectionSearchTemplate() {
	var hbc_id = GetURLParameter("hbc_id");
	
    var sIdCol='';
    sIdCol += '<table width=100% style="background-color:#ffffd2;"><tr>'
    sIdCol += '<td><p class="commontitle">Event ID</p><input type="text" name="eventidFilter" value="" id="eventidFilter" size=10 maxlength=15 onkeydown="if (event.keyCode == 13){GetTsCompanyByEventID();}">';
    sIdCol += '&nbsp;<input class=btnSendMail type=button id=btneventidFilter, value="' + locale.main_page.lblFilter + '" onclick="GetTsCompanyByEventID();">';
    sIdCol += '</td>'
    sIdCol += '<td><p class="commontitle">Company ID</p><input type="text" name="companyidFilter" value="" id="companyidFilter" size=10 maxlength=15 onkeydown="if (event.keyCode == 13){GetTsCompanyByCompanyID();}">';
    sIdCol += '&nbsp;<input class=btnSendMail type=button id=btncompanyidFilter, value="' + locale.main_page.lblFilter + '" onclick="GetTsCompanyByCompanyID();">';
    sIdCol += '</td>'
    sIdCol += '<td><p class="commontitle">BackOffice ID</p><input type="text" name="boidFilter" value="' + ($.isEmptyObject(hbc_id) ? '' : hbc_id) + '" id="boidFilter" size=10 maxlength=15 onkeydown="if (event.keyCode == 13){GetTsCompanyByBackOfficeID();}">';
    sIdCol += '&nbsp;<input class=btnSendMail type=button id=btnboidFilter, value="' + locale.main_page.lblFilter + '" onclick="GetTsCompanyByBackOfficeID();">';
    sIdCol += '</td>'
    //sIdCol += '<td><p class="commontitle">Fiche ID</p><input type="text" name="ficheidFilter" value="" id="ficheidFilter" size=10 maxlength=15>';
    //sIdCol += '&nbsp;<input class=btnSendMail type=button id=btnficheidFilter, value="' + locale.main_page.lblFilter + '" onclick="FilterGridByFicheID();">';
    //sIdCol += '</td>'
    sIdCol += '<td><p class="commontitle">Telesales info ID</p><input type="text" name="tsinfoidFilter" value="" id="ttidFilter" size=10 maxlength=15 onkeydown="if (event.keyCode == 13){GetTsCompanyByTTID();}">';
    sIdCol += '&nbsp;<input class=btnSendMail type=button id=btntsinfoidFilter, value="' + locale.main_page.lblFilter + '" onclick="GetTsCompanyByTTID();">';
    sIdCol += '</td>'
    sIdCol += '</tr></table>'
    return sIdCol;
}

function cbxStrOption() {
    var cb = '';
    cb = '<select id=cbxstropt name=cbxstropt><option value=0>is exactly</option><option value=1>start with</option><option value=2 selected>contains</option><option value=3>end with</option></select>';
    return cb;
}

function SetVarCollectionSearchTemplate() {
    var sVarCol = '';
    sVarCol += '<table width=100% style="background-color:#d9e8f5;"><tr>'
    sVarCol += '<td><p class="commontitle">Search Term</p>' + cbxStrOption() + '</td>';
    sVarCol += '<td><p class="commontitle">' + locale.main_page.lblEntreprise + '</p><input type="text" name="companynameFilter" value="" id="companynameFilter" size=20 onkeydown="if (event.keyCode == 13){GET_CompaniesByCompanyName();}">';
    sVarCol += '&nbsp;<input class=btnSendMail type=button id=btncompanyFilter, value="' + locale.main_page.lblFilter + '" onclick="GET_CompaniesByCompanyName();">';
    sVarCol += '</td>'

    sVarCol += '<td><p class="commontitle">' + locale.main_page.vat + '</p><input type="text" name="vatFilter" value="" id="vatFilter" size=10 maxlength=10 onkeydown="if (event.keyCode == 13){GetTsCompanyByCompanyVat();}">';
    sVarCol += '&nbsp;<input class=btnSendMail type=button id=btnvatFilter, value="' + locale.main_page.lblFilter + '" onclick="GetTsCompanyByCompanyVat();">';
    sVarCol += '</td>'

    sVarCol += '<td><p class="commontitle">' + locale.main_page.lblEventContact + '</p><input type="text" name="contactFilterFirst" value="" id="contactFilterFirst" size=10 onkeydown="if (event.keyCode == 13){GET_CompaniesByContactFirstName();}">';
	sVarCol += '<input type="text" name="contactFilterLast" value="" id="contactFilterLast" size=10 onkeydown="if (event.keyCode == 13){GET_CompaniesByContactLastName();}">'
	sVarCol += '&nbsp;<input class=btnSendMail type=button id=btncontactFilter, value="' + locale.main_page.lblFilter + '" onclick="GET_CompaniesByContact();">';
    sVarCol += '</td>'

    sVarCol += '<td><p class="commontitle">' + locale.main_page.lblTelephone + '</p><input type="text" name="phoneFilter" value="" id="phoneFilter" size=10 onkeydown="if (event.keyCode == 13){GET_CompaniesByPhone();}">';
    sVarCol += '&nbsp;<input class=btnSendMail type=button id=btnphoneFilter, value="' + locale.main_page.lblFilter + '" onclick="GET_CompaniesByPhone();">';
    sVarCol += '</td>'
    sVarCol += '</tr></table>'
    return sVarCol;
}

function resetResultContainers() {
    myinsidelayout.cells("b").attachHTMLString('');
    myinsidelayout.cells("c").attachHTMLString('');
}

function GetTsCompanyByEventID() {
    resetResultContainers();
    if (anyDigit(document.getElementById('eventidFilter').value) != true) {
        dhtmlx.message({
            text: locale.popup.msgusedigitsonly,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(anino + locale.popup.msgusedigitsonly);

        return false };
    AjaxGetTsCompanyByEventIDPromise($("#eventidFilter").val())
    .done(function(data)
    {
        if (data.length > 0) {
            myinsidelayout.cells("b").attachHTMLString(FormatResult(data));
        }
        else { myinsidelayout.cells("b").attachHTMLString(anino) }
    });

}

function GetTsCompanyByCompanyID() {
    resetResultContainers();
    if (anyDigit(document.getElementById('companyidFilter').value) != true) {
        dhtmlx.message({
            text: locale.popup.msgusedigitsonly,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(anino + locale.popup.msgusedigitsonly);
        return false
    };
    AjaxGetTsCompanyByCompanyIDPromise($("#companyidFilter").val())
    .done(function(data){
        if (data.length > 0)
        {
            myinsidelayout.cells("b").attachHTMLString(FormatResult(data));
        }else{
            myinsidelayout.cells("b").attachHTMLString(anino);
        }
    });
}

function GetTsCompanyByBackOfficeID() {
    resetResultContainers();
    if (anyDigit(document.getElementById('boidFilter').value) != true) {
        dhtmlx.message({
            text: locale.popup.msgusedigitsonly,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(anino + locale.popup.msgusedigitsonly);
        return false };
    var boid = document.getElementById('boidFilter').value;
    AjaxGetTsCompanyByBackOfficeIDPromise(boid)
    .done(function(data){
        myinsidelayout.cells("b").attachHTMLString(FormatResult(data));
    }).fail(function(resp){
        myinsidelayout.cells("b").attachHTMLString(anino);
    });

    GetCompanyBackOfficeInfoByCompany_hbc_id(boid);
}

function GetTsCompanyByCompanyVat() {
    resetResultContainers();
    if (anyDigit(document.getElementById('vatFilter').value) != true) {
        dhtmlx.message({
            text: locale.popup.msgusedigitsonly,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(anino + locale.popup.msgusedigitsonly);
        return false };
    AjaxGetTsCompanyByCompanyVatPromise($("#vatFilter").val())
    .done(function(data){
        myinsidelayout.cells("b").attachHTMLString(FormatResult(data));
    }).fail(function(resp){
        myinsidelayout.cells("b").attachHTMLString(anino);
    });

    AjaxGetCompanyBackOfficeInfoByVat($("#vatFilter").val())
    .done(function (data) {
        if (data.length > 0)
        {
            myinsidelayout.cells("c").attachHTMLString(FormatBackOfficeResult(data));
        } else {
            myinsidelayout.cells("c").attachHTMLString(anino);
        }
    });
}

function GetCompanyBackOfficeInfoByCompany_hbc_id(company_hbc_id, cell){
    var theCell;
    if (cell !== undefined)
    {
        theCell = cell
    }else{
        theCell = myinsidelayout.cells("c")
    }
    theCell.attachHTMLString('');
    AjaxGetCompanyBackOfficeInfoByBackOfficeIDPromise(company_hbc_id)
    .done(function(data){
        if (data.length >0)
        {
            theCell.attachHTMLString(FormatBackOfficeResult(data));
            AjaxCompanyBackOfficeHistoryPromise(data[0].HBC_Firmvat, company_hbc_id, "resBOHistory");
        }else{
            theCell.attachHTMLString(anino);
        }
    });
}

function GetTsCompanyByTTID() {
    resetResultContainers();
    AjaxGetTsCompanyByTTIDPromise($("#ttidFilter").text())
    .done(function (data) {
        myinsidelayout.cells("b").attachHTMLString(FormatResult(result));
    }).fail(function (data) {
        myinsidelayout.cells("b").attachHTMLString(anino);
    });
}

// TELESALES:flat table(single value) Format processing
function FormatResult(result) {
    var s = '';
    var contactmobile = '';
    var contactphone = '';
    var contactmail = '';
    var userFullName = '';
    var vatnav = '';

    for (var i = 0; i < result.length; i++)
    {
        var eventID = result[i].id;
        var addrfull = result[i].company_address_street + ' ' + result[i].company_address_number + ' ' + result[i].company_address_postcode + ' ' + result[i].company_address_localite
        var phon = result[i].company_tel;
        var mob = result[i].company_mobile;
        var mail = result[i].company_mail;
        vatnav = result[i].company_vat;
        userFullName = result[i].firstname + ' ' + result[i].lastname + ' (' + result[i].extension  + ')'
        if (phon.length != 0) {contactphone = '<img class=onmouseoverphone src="/graphics/common/win_16x16/Phone-icon16.png" onClick="JS_EventCall(' + result[i].company_tel + ');"> ' + result[i].company_tel;}

        if (mob.length != 0) {contactmobile = '<img class=onmouseovermobile src="/graphics/common/win_16x16/mobile16.png" onClick="JS_EventCall(' + result[i].company_mobile + ');"> ' + result[i].company_mobile;}

        //var contactmail = '<img class =onmouseovermail src="/graphics/common/win_16x16/email_16.png" onClick="MailContact(' + result[0].company_mail + ');"> ' + result[0].company_mail;
        var contactfull = contactphone + ' ' + contactmobile + ' '  + mail;
        var cwurl = '<a href="http://www.companyweb.be/page_companydetail.asp?vat=' + vatnav + '" target=_blank>'+vatnav+'</a>';

        s += '<table width=100%>'
        s += '<tr><td class="commontitle">' + locale.main_page.lblVatCompany + '</td><td class="testPageText"><b>' + cwurl + '</b></td>'
        s += '<td class="commontitle">COMPANY ID</td><td class="testPageText"><b>' + result[i].company_id + '</b></td>'
        s += '<td class="commontitle">BO ID</td><td class="testPageText"><b>' + result[i].company_hbc_id + '</b></td>'
        s += '<td class="commontitle">TT ID</td><td class="testPageText"><b>' + result[i].tt_id + '</b></td></tr>'

        s += '<tr><td class="commontitle" width=20%>' + locale.main_page.lblEntreprise + '</td><td colspan=7 class="testPageText"><b>' + result[i].company_name + ' ' + result[i].jfc_s_desc + '</b></td></tr>'
        s += '<tr><td colspan=8 class="testPageText">' + result[i].nbc_desc + '</td></tr>'
        s += '<tr><td colspan=8 class="testPageText">' + addrfull + '</td></tr>'
        s += '<tr><td colspan=8 class="testPageText">' + contactfull + '</td></tr>'
        s += '<tr><td colspan=8 ><hr></td></tr>'
        s += '</table>'

        if (result[i].company_hbc_id != '') {
            GetCompanyBackOfficeInfoByCompany_hbc_id(result[i].company_hbc_id);
            //AjaxCompanyBackOfficeHistory(result[0].company_vat, result[0].company_hbc_id, "resBOHistory");
        };

        //if valid event id then if event user is logged user then display command to go to scheduler for updates
        if (eventID != '') {
            var urltoevent = '';
            var imgownership = '';
			var imgblacklist = '';
			var usrrole = getValuefromCookie("TELESALESTOOLV2", "usrRoleId");
			var widthforblacklist = 22;
            var startdateHour = result[i].start_date;
            startdateHour = startdateHour.split(' ');
            startdateHour = startdateHour[1];
            var enddateHour = result[i].end_date;
            enddateHour = enddateHour.split(' ');
            enddateHour = enddateHour[1];

            var d = result[i].us_start_date;
            var euroStartDate = formatEuroDate(result[i].us_start_date);
            var euroEndDate = formatEuroDate(result[i].us_end_date);
            //if (result[0].usr_id == getValuefromCookie("TELESALESTOOLV2", "usrId")) {
            urltoevent = '<img src =/graphics/common/win_32x32/Calenda.png class=onmouseoverlookup onClick=loadEvent(' + eventID + ',"' + d + '");>';
            //}
            imgownership = '<img src =/graphics/common/win_32x32/Rotate-view-tool.png class=onmouseoverownership onClick=TakeEventOwnership(' + eventID + ');>';
			if (usrrole == "2" || usrrole == "1") {
				//imgblacklist = '<img src =/graphics/common/win_32x32/blacklist.png class=onmouseoverblacklist alt="blacklist" onclick=SetCompanyBlacklist(' + eventID + ');>';
				//widthforblacklist = 29				
			}

			
            //TELESALES EVENT INFO TABLE
            s += '<table width=100%>'
            s += '<tr><td class="commontitle"  width=' + widthforblacklist +'%>Event ID&nbsp;&nbsp;&nbsp;' + urltoevent + '&nbsp;&nbsp;&nbsp;' + imgownership + '&nbsp;&nbsp;&nbsp;' + imgblacklist +'</td><td class="schedulerEventType"><b>' + result[i].id + '</b></td></tr>'
            s += '<tr><td class="commontitle" width=22%>Event Owner</td><td class="schedulerEventType" id="eventfullname">' + userFullName + '</td></tr>'
            s += '<tr><td class="commontitle"  width=22%>' + locale.main_page.lblEventType + '</td><td class="schedulerEventType" style="color:' + result[i].event_type_textColor + '; background-color:' + result[i].event_type_color + '";><b>' + result[i].event_type_name + '</b></td></tr>'
            s += '<tr><td class="commontitle" width=22%>' + locale.main_page.lblStartDate + '</td><td class="schedulerDate">' + euroStartDate + ' ' + startdateHour + '</td></tr>'
            s += '<tr><td class="commontitle" width=22%>' + locale.main_page.lblEndDate + '</td><td class="schedulerDate">' + euroEndDate + ' ' + enddateHour + '</td></tr>'
            s += '<tr><td colspan=2 class="testPageText">' + result[i].text + '</td></tr>'
            s += '<tr><td colspan=8 ><hr></td></tr>'
            s += '</table>'
            s += '<div id="SearchComments"></div>';
            formatCommentTable(eventID);

            if (i == 0){
                objEvent = {
                    event_id: eventID,
                    company_id: result[i].company_id,
                    company_vat: result[i].company_vat,
                    company_hbc_id: result[i].company_hbc_id,
                    company_name: result[i].company_name,
                    event_type_id: result[i].event_type_id,
                    event_type_name: result[i].event_type_name,
                    event_start_date: result[i].event_start_date,
                    event_end_date: result[i].event_end_date,
                    event_text: result[i].text,
                    usr_id: result[i].usr_id,
                    //icon: scheduler.getEvent(id).icon,
                };
            }
        }
    }

    return s;
}

//TELESALES EVENT COMMENT TABLE
function formatCommentTable(event_id) {
    
    AjaxGetTsCompanyHistoryByEventID(event_id)
    .done(function(data)
    {
        var tc = ''; var j;

        tc += '<table width=100%>';
        for (j = 0; j < data.length; j++) {
            tc += '<tr><td valign=top class="nakedcellheader" width=115px>' + formatEuroDate(data[j].comment_date) + ' ' + data[j].comment_time + '</td><td class="testPageText"><b>' + data[j].event_type_name + '</b></td></tr>'
            tc += '<tr><td valign=top colspan=2 class="testPageText">' + data[j].comment + '</td></tr>'
        };

        tc += '</table>';

        $("#SearchComments").html(tc);
        
    });

}

// BACK OFFICE DATA TABLE
function FormatBackOfficeResult(resBO) {
    var s = '';
    var fullAddress = NullUndefinedToNewVal(resBO[0].HBC_Street, '') + ' ' + NullUndefinedToNewVal(resBO[0].HBC_HouseNr, '') + ' ' + NullUndefinedToNewVal(resBO[0].HBC_BusNr, '') + ' - ' + NullUndefinedToNewVal(resBO[0].HBC_Postcode, '') + ' ' + NullUndefinedToNewVal(resBO[0].HBC_gemeente,'')
    s += '<table width=100%>'
    s += '<tr><td class="commontitle"  width=20%>BackOffice ID</td><td class="testPageText"><b>' + resBO[0].HBC_ID + '</b></td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblVatCompany + '</td><td class="testPageText"><b>' + resBO[0].HBC_Firmvat + '</b></td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblEntreprise + '</td><td class="testPageText"><b>' + resBO[0].HBC_FirmName + '</b></td></tr>'
    s += '<tr><td class="commontitle" width=20%>' + locale.main_page.lblEventContact + '</td><td class="testPageText">' + resBO[0].HBC_ContactName + '</td></tr>'
    s += '<tr><td class="commontitle" width=20%>Nacebel</td><td class="testPageText">' + resBO[0].nbc_desc + '</td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblAdress + '</td><td class="testPageText">' + fullAddress + '</td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblTelephone + '</td><td class="testPageText">' + resBO[0].HBC_tel + '</td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblFax + '</td><td class="testPageText">' + resBO[0].HBC_fax + '</td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblGsm + '</td><td class="testPageText">' + resBO[0].HBC_gsm + '</td></tr>'
    s += '<tr><td class="commontitle"  width=20%>' + locale.main_page.lblEmail + '</td><td class="testPageText">' + resBO[0].HBC_contactemail + '</td></tr>'

    s += '<tr><td colspan=2><hr></td></tr>'
    s += '<tr><td colspan=2  class="testPageText" id=resBOHistory></td></tr>'
    s += '</table>';
    return s;
}

function CastSchedulerDate(val) {
    var newdate = '';
    var dte = val.replace(/\//g, '-');
    var res = dte.split('-');

    newdate = res[2] + '-' + res[1] + '-'+ res[0]
    return newdate;
}

function loadEvent(eventID, start_date) {
    loaded_event_id = 0;
    myinsidelayout = myLayout.cells("a").attachLayout({ pattern: '2U' });
    myinsidelayout.cells("a").setText(objEvent.company_name);

    //load full event in accordion right
    myinsidelayout.cells("b").setText(locale.main_page.lblEventDetail);
    JS_GetAgendaTask(myinsidelayout, "b");
    JS_createTaskDetailsLayout(myinsidelayout, "b");
    JS_GetTaskDetails(objEvent, myinsidelayout);

    //JS_GetAgendaTask(myinsidelayout, "a");
    var d = formatEuroDate(start_date);
    JS_createResultAgenda(myinsidelayout, "a", d);
}

function JS_createResultAgenda(myinsidelayout, myCell, start_date) {
    //customize views buttons in main agenda
    var sTabs = '<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>'
    sTabs +='<div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>'
    sTabs +='<div class="dhx_cal_tab" name="month_tab" style="right:280px;"></div>'
    //sTabs +='<div class="dhx_cal_tab" name="week_agenda_tab" style="right:76px;"></div>'
    //sTabs += '<div style="left:300px;"><input class="dhx_cal_tab dhx_cal_tab_standalone" style="width:80px; height:33px;" type=button name=btnswitchView id="btnswitchView" value="' + locale.main_page.btnswitchview30 + '" onClick="switchView();"></div>';
    var schedulerDate = new Date(CastSchedulerDate(start_date));

    myinsidelayout.cells(myCell).attachScheduler(schedulerDate, "day", sTabs);
    load_url = "classes/dal/clsScheduler/events.asp?a=XML_GetEvents&b=1&c=0"; //webservice
    scheduler.load(load_url);
}

function GET_CompaniesByCompanyName() {
    resetResultContainers();
    var o = document.getElementById('cbxstropt').value;
    var c = document.getElementById('companynameFilter').value;
    GetTsGridCompanyByCompanyName(o, c);
    GetBoGridCompanyByCriteria(o, c, "HBC_FirmName");
}

function GET_CompaniesByContactFirstName()
{
	resetResultContainers();
	
	var o = $("#cbxstropt").val();
    var firstName = $("#contactFilterFirst").val();
	
	if ($.trim(firstName)){
		GetTsGridCompanyByCriteria(o, firstName, "@contactFirstName");
		GetBoGridCompanyByCriteria(o, firstName, "HBC_ContactName");
	}
}
function GET_CompaniesByContactLastName()
{
	resetResultContainers();
	
	var o = $("#cbxstropt").val();
    var lastName = $("#contactFilterLast").val();
	
	if ($.trim(lastName)){
		GetTsGridCompanyByCriteria(o, lastName, "@contactLastName");
		GetBoGridCompanyByCriteria(o, lastName, "HBC_ContactName");
	}
}
function GET_CompaniesByContact() {
    resetResultContainers();
	
    var o = $("#cbxstropt").val();
    var firstName = $("#contactFilterFirst").val().trim();
	var lastName = $("#contactFilterLast").val().trim();
	
	if($.trim(firstName)){
		GET_CompaniesByContactFirstName();
	}else{
		GET_CompaniesByContactLastName();
	}
}

function GET_CompaniesByPhone() {
    resetResultContainers();
    var o = document.getElementById('cbxstropt').value;
    var c = document.getElementById('phoneFilter').value;
    GetTsGridCompanyByCriteria(o, c, "@phone");
    GetBoGridCompanyByCriteria(o, c, "@phone");
}

// GRID Format processing
function ConfigGridTsCompanyResult(tsGrid) {
    var tbHeader = 'Cy ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.vat + ',' + locale.main_page.lblTelephone + ',Event ID,BO ID,hbc_id'
    //tsGrid.setColumnHidden(5, true);
    tsGrid.setColumnHidden(6, true);
    tsGrid.setHeader(tbHeader);
    tsGrid.setInitWidths("60,260,80, 90,60,60");
    tsGrid.setColAlign("left,left,left,left,left,left");
    tsGrid.setColTypes("ro,ro,ro,ro,ro,ed");
    tsGrid.setColSorting("str,str,str,str,str,str");
    tsGrid.init();
    tsGrid.attachEvent("onRowSelect", ts_doOnRowSelect);
    tsGrid.attachEvent("onEditCell", ts_doOnEditCell);
}

function ts_doOnRowSelect(id, ind) {
    document.getElementById('companyidFilter').value = tsGrid.cells(id, 0).getValue();
    GetTsCompanyByCompanyID();
    return true;
}

function ts_doOnEditCell(stage, rId, cInd, nValue, oValue) {
    if (cInd != 5) { return };
    if (stage == 2) {
        var objCompany = {
            company_id: tsGrid.cells(rId, 0).getValue(),
            company_hbc_id: nValue,
            trusted: 1
        }
        var message = 'Company ID: ' + objCompany.company_id + ' updated with BO id:' + objCompany.company_hbc_id;
        dhtmlx.message({
            text: message,
            expire: 500,
            type: "successmessage"
        });
        dhtmlx.alert(message);

        AjaxCompany_UpdateBackOfficeID(objCompany);
        tsGrid.cells(rId, cInd).setValue(nValue);
    }
}

function GetTsGridCompanyByCompanyName(o, c) {
    tsGrid = mysublayout01.cells("a").attachGrid();
    ConfigGridTsCompanyResult(tsGrid);
    tsGrid.clearAll();
    GetTsCompanyByCompanyNamePromise(o, c)
    .done(function (data) {
        if (data.rows.length > 0)
        {
            tsGrid.parse(data, "json");
        }
    });
}

function GetTsGridCompanyByCriteria(o, fieldvalue, fieldname) {
    tsGrid = mysublayout01.cells("a").attachGrid();
    ConfigGridTsCompanyResult(tsGrid);
    tsGrid.clearAll();

    GetTsCompanyByContact(o, fieldvalue, fieldname)
    .done(function (data) {
        if (data.rows.length > 0) {
            tsGrid.parse(data, "json");
        }
    });
}

function GetBoGridCompanyByCriteria(o, fieldvalue, fieldname) {
    resetResultContainers();
    boGrid = mysublayout01.cells("b").attachGrid();
    ConfigGridBoCompanyByCriteria(boGrid);
    boGrid.clearAll();

    GetBackOfficeCompanyByCriteriaPromise(o, fieldvalue, fieldname)
    .done(function(data)
    {
        if (data.rows.length > 0) {
            boGrid.parse(data, "json")
        }
    });
}

function ConfigGridBoCompanyByCriteria(boGrid) {
    var tbHeader = 'BO ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.vat + ',' + locale.main_page.lblEventContact + ',' + locale.main_page.lblTelephone + ',' + locale.main_page.lblGsm + ',Adresse'
    //tsGrid.setColumnHidden(5, true);
    //boGrid.setColumnHidden(6, true);
    boGrid.setHeader(tbHeader);
    boGrid.setInitWidths("60,260,80, 90,60,60,60");
    boGrid.setColAlign("left,left,left,left,left,left,left");
    boGrid.setColTypes("ed,ed,ed,ed,ed,ed,ed");
    boGrid.setColSorting("str,str,str,str,str,str,str");
    boGrid.init();
    boGrid.attachEvent("onRowSelect", bo_doOnRowSelect);
}

function bo_doOnRowSelect(id, ind) {
    var curBoid = boGrid.cells(id, 0).getValue();
    curBoid = parseInt(curBoid);
    document.getElementById('boidFilter').value = curBoid;
    GetCompanyBackOfficeInfoByCompany_hbc_id(curBoid);
    GetTsCompanyByBackOfficeID();
    return true;
}

function TakeEventOwnership(event_id) {
    AjaxUpdateTsEventUserIDPromise(event_id)
    .done(function (data) {
        switch(data)
        {
            case 1:
                var fullname = getValuefromCookie("TELESALESTOOLV2", "firstname") + ' ' + getValuefromCookie("TELESALESTOOLV2", "lastname") + ' (' + getValuefromCookie("TELESALESTOOLV2", "extension") + ')';
                $("#eventfullname").text(fullname);
                $("#eventfullname").attr('class', 'schedulerEventTypeGreen');
            break;

            default:
            dhtmlx.message({
                text: locale.popup.msgsaveerror,
                expire: -1,
                type: "errormessage"
            });
            dhtmlx.alert(locale.popup.msgsaveerror);
            $("#eventfullname").attr('class', 'schedulerEventTypeRed');
            break;
        }
    });
}

function SetCompanyBlacklist(event_id) {
    AjaxUpdateTsBlackListPromise(event_id)
    .done(function (data) {
        dhtmlx.alert('Blacklisted');
    });
}

// SEARCH ENGINE DATA ACCESS LAYER

function AjaxUpdateTsEventUserIDPromise(event_id) {
    var url = "/classes/dal/clsSearch/clsSearch.asp";

    return $.get(url, { q: "UpdateTsEventUserID", event_id: event_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure Updating Event with new UserId",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxUpdateTsBlackListPromise(event_id) {
    var url = "/classes/dal/clsSearch/clsSearch.asp";

    return $.get(url, { q: "UpdateTsCompanyIsBlacklisted", event_id: event_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure Updating company state to blacklist",
            expire: -1,
            type: "errormessage"
        });
    });
}

//Json Object format
function AjaxGetTsCompanyByEventIDPromise(event_id) {
    var url = "/classes/dal/clsSearch/clsSearch.asp";

    return $.get(url, { q: "GetTsCompanyByEventID", event_id: event_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Company By eventId",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxGetTsCompanyByBackOfficeIDPromise(company_hbc_id) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';

    return $.get(url, { q: "GetTsCompanyByBackOfficeID", company_hbc_id: company_hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Company By BackofficeId",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxGetTsCompanyByCompanyIDPromise(company_id) {
    var url = "/classes/dal/clsSearch/clsSearch.asp";

    return $.get(url, { q: "GetTsCompanyByCompanyID", company_id: company_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Company by CompanyId",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxGetTsCompanyByCompanyVatPromise(company_vat) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';

    return $.get(url, { q: "GetTsCompanyByCompanyVat", company_vat: company_vat })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Company By companyvat",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxGetTsCompanyByTTIDPromise(tt_id) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';
    
    return $.get(url, { q: "GetTsCompanyByTTID", tt_id: tt_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Company By TTId",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxGetTsCompanyHistoryByEventID(event_id) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';
    return $.get(url, { q: "GetTsCompanyHistoryByEventID", event_id: event_id })
        .fail(function (data) {
            dhtmlx.message({
                text: "Failure getting TS CompanyHistory by eventId",
                expire: -1,
                type: "errormessage"
            });
        });

}

function AjaxGetCompanyBackOfficeInfoByVat(company_vat) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';

    return $.get(url, { q: "GetCompanyBackOfficeInfo", company_vat: company_vat })
        .fail(function (data) {
            dhtmlx.message({
                text: "Failure getting CompanyBackOffic By VAT",
                expire: -1,
                type: "errormessage"
            });
        });
}

function AjaxGetCompanyBackOfficeInfoByBackOfficeIDPromise(company_hbc_id) {
    var url = "/classes/dal/clsSearch/clsSearch.asp";

    return $.get(url, { q: "GetCompanyBackOfficeInfo", company_hbc_id: company_hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Backoffice Info from BackOfficeId",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxCompanyBackOfficeHistoryPromise(vatnumber, HBC_ID, container) {
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		$("#" + container).empty();
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
		return $.get(url, {q:"2", vat: vatnumber, HBC_ID: HBC_ID})
		.done(function(data){
			if (data.length > 0)
			{
				$("#" + container).html(data);
			}
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Backoffice History",
				expire: -1,
				type: "errormessage"
			});
		}).always(function()
		{
			$("#" + container).data('requestRunning',false);
		});
	}
	return $.when(null);
}

//Json grid format
function GetTsCompanyByCompanyNamePromise(cbxstrOpt, company_name) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';
        
    return $.get(url, { q: "GetTsCompanyByCompanyName", cbxstrOpt: cbxstrOpt, company_name: company_name })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Company By CompanyName",
            expire: -1,
            type: "errormessage"
        });
    });
}

/*function GetTsCompanyByCriteria(cbxstrOpt, fieldvalue, fieldname) {
    var resp = '';
    var xmlhttp = new XMLHttpRequest();
    var url = '/classes/dal/clsSearch/clsSearch.asp?q=GetTsCompanyByString&cbxstrOpt=' + cbxstrOpt + '&fieldname=' + fieldname + '&fieldvalue=' + fieldvalue
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            resp = xmlhttp.responseText;
        };
    };
    xmlhttp.open("GET", url, false);
    xmlhttp.send();
    return resp;
}*/

function GetTsCompanyByContact(cbxstrOpt, fieldvalue, fieldname) {
    var url = '/classes/dal/clsSearch/clsSearch.asp';
    
    return $.get(url, { q: "GetTsCompanyByContact", cbxstrOpt: cbxstrOpt, fieldname: fieldname, fieldvalue: fieldvalue })
        .fail(function (data) {
            dhtmlx.message({
                text: "Failure getting TS Company By Contact",
                expire: -1,
                type: "errormessage"
            });
        });
}

function GetBackOfficeCompanyByCriteriaPromise(cbxstrOpt, fieldvalue, fieldname) {
    var url = "/classes/dal/clsSearch/clsSearch.asp";

    return $.get(url, {q: "GetCompanyBackOfficeInfoByString", cbxstrOpt: cbxstrOpt,
        fieldname: fieldname, fieldvalue: fieldvalue })    
    .fail(function (data) {
            dhtmlx.message({
                text: "Failure getting Backoffice Company By Criteria Contacts",
                expire: -1,
                type: "errormessage"
            });
        });
}