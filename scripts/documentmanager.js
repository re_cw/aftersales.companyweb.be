var docRoot = '/XLIB';
var AfterSalesMailTypeId = 10;
var attachments = '';
var insidelayout;
var objWin;

function MM_DocumentTemplateEngineSetLayout() {
    var outlookSend = '<img id=outlooksenbutton src="/graphics/common/win_16x16/outlooksend.png" onclick="btnSaveTemplate_onClick();"';
    var tbl = '<table id=maindocmgtcontainer class=maindocmgtcontainer border=1><tr><td width=30% valign=top>'
    tbl += '<table width=30% cellspacing=2 cellpadding=2 border=0>'
    tbl += '<tr><td class="commontitle">' + locale.main_page.lblMailTemplate + '</td><td colspan=2><div id=combo_mailtemplates style="width:280px;"></div><input type="hidden" id="mail_template_id" name="mail_template_id" value="" /></td></tr>'
    tbl += '<tr><td colspan=3>&nbsp;</td></tr>'
    tbl += '<tr><td class="commontitle">' + locale.main_page.lblLang + '</td><td><div id=combo_doclangs style="width:120px;"></td><td align=right></div><input id=btn_submit type=button value=' + locale.main_page.lblsearch + ' onclick="reloadGrid();"></td></tr>'
    tbl += '</table>'
    tbl += '</td><td width=70% valign=top>'
    tbl += '<table width=100% cellspacing=2 cellpadding=2 border=0>'
    tbl += '<tr>'
    tbl += '<td rowspan=3 style="width:50px;" class="btnSendMail">' + outlookSend + '</td>'
    tbl += '<td class="commontitle"  style="width:40px;">' + locale.main_page.lblTo + '...</td><td><input id=MailTo name=MailTo style="width:100%;"></td></tr>'
    tbl += '<tr><td class="commontitle"  style="width:40px;">' + locale.main_page.lblCC + '...</td><td><input id=MailCC name=MailCC style="width:100%;"></td></tr>'
    tbl += '<tr><td class="nakedtitle"  style="width:40px;">' + locale.main_page.lblSubject + '</td><td><input id=Subject name=Subject style="width:100%;"></td></tr>'
    tbl += '<tr><td class="commontitle" colspan=3>' + locale.main_page.LblAttached + '</td></tr>'
    tbl += '<tr><td colspan=3><div id="data_container" style="border:1px solid #A4BED4; background-color:white;width:100%;"></div></td></tr>'
    tbl += '<tr><td colspan=3><div id="editorObj" style="border:1px solid #A4BED4;width:100%;height:735px;"></div></td></tr>'
    tbl += '</table>'
    tbl += '</td></tr></table>'

    return tbl;
}

function MM_DocumentEngineSetLayout(contactId) {
    var outlookSend = '<img id=outlooksenbutton src="/graphics/common/win_16x16/outlooksend.png" onclick="btnSendMail_onClick(\'' + contactId + '\');"';
    var tbl = '<table id=maindocmgtcontainer class=maindocmgtcontainer border=1><tr><td width=30% valign=top>'
    tbl += '<table width=30% cellspacing=2 cellpadding=2 border=0>'
    tbl += '<tr><td class="commontitle">' + locale.main_page.lblMailTemplate + '</td><td colspan=2><div id=combo_mailtemplates style="width:280px;"></div><input type="hidden" id="mail_template_id" name="mail_template_id" value="" /></td></tr>'
    tbl += '<tr><td colspan=3>&nbsp;</td></tr>'
    tbl += '<tr><td class="nakedcellheader" colspan=3>' + locale.main_page.LblAttachment + '</td></tr>'

    tbl += '<tr><td class="commontitle">' + locale.main_page.lblDocNature + '</td><td colspan=2><div id=combo_docnatures style="width:280px;"></div></td></tr>'
    tbl += '<tr><td class="commontitle">' + locale.main_page.lblDocType + '</td><td colspan=2><div id=combo_doctypes style="width:280px;"></div></td></tr>'
    tbl += '<tr><td class="commontitle">' + locale.main_page.lblLang + '</td><td><div id=combo_doclangs style="width:120px;"></td><td align=right></div><input id=btn_submit type=button value=' + locale.main_page.lblsearch + ' onclick="reloadGrid();"></td></tr>'
    tbl += '<tr><td colspan=3>&nbsp;</td></tr>'
    tbl += '<tr><td class="nakedcellheader" colspan=3>' + locale.main_page.lbldoclist + '</td></tr>'
    tbl += '<tr><td colspan=3><div id=gridbox style="width:100%;height:300px;"></div></td></tr>'
    tbl += '<tr><td colspan=3><div id=ASgridbox style="width:100%;height:200px;"></div></td></tr>'

    tbl += '</table>'
    tbl += '</td><td width=70% valign=top>'
    tbl += '<table width=100% cellspacing=2 cellpadding=2 border=0>'
    tbl += '<tr>'
    tbl += '<td rowspan=3 style="width:50px;" class="btnSendMail">' + outlookSend + '</td>'
    tbl += '<td class="commontitle"  style="width:40px;">' + locale.main_page.lblTo + '...</td><td><input id=MailTo name=MailTo style="width:100%;"></td></tr>'
    tbl += '<tr><td class="commontitle"  style="width:40px;">' + locale.main_page.lblCC + '...</td><td><input id=MailCC name=MailCC style="width:100%;"></td></tr>'
    tbl += '<tr><td class="nakedtitle"  style="width:40px;">' + locale.main_page.lblSubject + '</td><td><input id=Subject name=Subject style="width:100%;"></td></tr>'
    tbl += '<tr><td class="commontitle" colspan=3>' + locale.main_page.LblAttached + '</td></tr>'
    tbl += '<tr><td colspan=3><div id="data_container" style="border:1px solid #A4BED4; background-color:white;width:100%;"></div></td></tr>'
    tbl += '<tr><td colspan=3><div id="editorObj" style="border:1px solid #A4BED4;width:100%;height:735px;"></div></td></tr>'
    tbl += '</table>'
    tbl += '</td></tr></table>'

    return tbl;
}
var contactIndex = 0;
function MM_DocumentEngineInitialize(myinsidelayout, i, objWindows, objMail, objContact) {
    insidelayout = myinsidelayout;
    contactIndex = i;
    objWin = objWindows;
    GetComboMailTemplates(objMail, objContact);
    GetComboDocNatures();
    GetComboDocTypes();
    GetComboDocLangs(objContact);
    ConfigDocListGrid();
    attachments = '';
    InitializeAttachmentList();
    InitializeEditor();
}

function MM_DocumentTemplateEngineInitialize(myinsidelayout, i, objWindows, objMail) {
    contactIndex = i;
    insidelayout = myinsidelayout;
    objWin = objWindows;
    GetComboMailTemplates(objMail);
    GetComboDocLangs();
    ConfigDocListGrid();
    attachments = '';
    InitializeAttachmentList();
    InitializeEditor();
}

function reloadGrid() {
    var a = '?doc_nature_id=' + cbxDocNatures.getSelectedValue();
    var b = '&doc_type_id=' + cbxDocTypes.getSelectedValue();
    var c = '&doc_lang_id=' + cbxDocLangs.getSelectedValue();
    multirequest = a + b + c
    docsGrid.clearAll();
    Ajax_GetDocumentsListPromise("1", "1", multirequest)
    .done(function (data) {
        docsGrid.parse(data, "json");
    })
}

function ConfigDocListGrid() {
    docsGrid = new dhtmlXGridObject('gridbox');
    docsGrid.setImagePath("/graphics/common/win_16x16/");
    docsGrid.setHeader("Attach," + locale.main_page.lbldoclist + ",@,path,doc,docimg");//the headers of columns
    docsGrid.setColumnHidden(3, true);
    docsGrid.setColumnHidden(4, true);
    docsGrid.setColumnHidden(5, true);
    docsGrid.setInitWidths("50,255,30,50,50,50");          //the widths of columns
    docsGrid.setColAlign("right,left,left,left,left,left");       //the alignment of columns
    docsGrid.setColTypes("acheck,ro,link,ro,ro,ro");                //the types of columns
    docsGrid.setColSorting("int,str,str,str,str,str");          //the sorting types
    docsGrid.init();
    docsGrid.clearAll();
    docsGrid.attachEvent("onCheckbox", doOnCheck);
}

var docs = [];
function doOnCheck(rowId, cellInd, state) {
    var doc = docsGrid.cells(rowId, 4).getValue();
    var url = docsGrid.cells(rowId, 3).getValue();
    var docimg = '<img src=/graphics/common/win_16x16/' + docsGrid.cells(rowId, 5).getValue() + '>';
    var removeimg = '<img src=/graphics/common/win_16x16/delete1.png ondblclick=remove_data("' + url + '")>';
    if (state === true) {
        attList.add({ img: docimg, doc_file_name: doc, imgremove: removeimg }, 0)
        docs.push(url);
    }
    else {
        attList.remove(attList.getSelected());
    };
    populateAttachmentArray();
    return true;
}

function InitializeAfterSalesGrid() {
    AsGrid = new dhtmlXGridObject('ASgridbox');
    AsGrid.setImagePath("/graphics/common/win_16x16/");
    AsGrid.setHeader(locale.main_page.lblmodels + ",source");
    AsGrid.setColumnHidden(1, true);
    AsGrid.setInitWidths("300,10");
    AsGrid.setColAlign("left,left");
    AsGrid.setColTypes("ro,ro");
    AsGrid.setColSorting("str,str");
    AsGrid.init();
    AsGrid.clearAll();
}

var attList;
function InitializeAttachmentList() {
    attList = new dhtmlXDataView({
        container: "data_container",
        type: {
            template: "#img# #doc_file_name# #imgremove#",
            height: 30, padding: 3, margin: 3, border: 1
        },
        autowidth: 3,
    });
}

function remove_data(url) {
    attList.remove(attList.getSelected());
    var index = docs.indexOf(url);
    if (index > -1) {
        docs.splice(index, 1);
    }
    populateAttachmentArray();
}

function populateAttachmentArray() {
    var res = '';
    attachments = '';
    for (i = 0; i < docs.length; i++) {
        attachments += docs[i] + ";";
    }

    res = attachments.substring(0, (attachments.length - 1));
    attachments = res;
}

var mailEditor; var header = ''; var body = ''; var footer = '';
function InitializeEditor() {
    //header = '<br><br><img src="http://www.companyweb.be/img/mails/headerCWnl.png"><br>';

    var p = '<p style="font-family:Calibri, arial, sans-serif;font-size:11pt;">';
    var firstname = getValuefromCookie("TELESALESTOOLV2", "firstname") + ' '
    firstname = firstname.replace(/\+/g, " ");

    var lastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    lastname = lastname.replace(/\+/g, " ");

    var greetings = "<br><br>Met vriendelijke groeten,<br>Bien à vous,<br><br>"
    var fullName = '<b>' + firstname + lastname + '</b><br>'
    var logo = '<br><img src="http://www.companyweb.be/img/mails/logoCWnl.png"><br>';

    var imgfooter = '<img src="http://www.companyweb.be/img/mails/footerCWnl.png">';
    //footer += '<b>Companyweb</b>  |  Kantorenpark Everest - Leuvensesteenweg 248 D  |  1800 Vilvoorde  |  T: +32 2 752 17 60  |  E: <a href=mailto:admin@companyweb.be>admin@companyweb.be</a>'
    footer = p + greetings + fullName + logo + imgfooter + '</p>';

    mailEditor = new dhtmlXEditor({
        parent: "editorObj",
        toolbar: true,
        iconsPath: "/graphics/codebase/imgs/",
        content: header + body + footer,
    });
}

var cbxMailTemplates;
function GetComboMailTemplates(objMail, objContact) {
	if (!$("#combo_mailtemplates").data('requestRunning')){
		$("#combo_mailtemplates").data('requestRunning',true);
		$("#combo_mailtemplates").empty();
		
		cbxMailTemplates = new dhtmlXCombo("combo_mailtemplates", null, null, "image");
		cbxMailTemplates.setImagePath("/graphics/common/win_16x16/");
		cbxMailTemplates.enableFilteringMode(true);
		cbxMailTemplates.allowFreeText(false);

		Ajax_GetComboGenericMailTemplatesWithImgPromise()
		.done(function (data) {
			cbxMailTemplates.load(data, function () {
				cbxMailTemplates.selectOption(0);
				$("#mail_template_id").val(cbxMailTemplates.getSelectedValue());
			});
		}).always(function(){
			$("#combo_mailtemplates").data('requestRunning',false);
		});

		cbxMailTemplates.attachEvent("onChange", function (value, text) {
			$("#mail_template_id").val(cbxMailTemplates.getSelectedValue());
			var customerlang = objContact.lang_radical;
			Ajax_GetTemplateSubjectPromise(value, customerlang)
			.done(function (data) {
				$("#Subject").val(data);
			});

			if (value === AfterSalesMailTypeId) {
				InitializeAfterSalesGrid();
				showcustom('ASgridbox');
			}
			else {
				hidecustom('ASgridbox');
			}

			mailEditor = new dhtmlXEditor({
				parent: "editorObj",
				toolbar: true,
				iconsPath: "/graphics/codebase/imgs/",
				content: getMailBodyContent(value, objContact),
			});
			populateMailObject(objMail);
		});
	}
	return $.when(null);
}

function getMailBodyContent(value, objContact) {
    switch (clickedMenu) {
        case maintoolbar.menu.events://2
        case maintoolbar.menu.agenda:
        case maintoolbar.menu.search:
		case maintoolbar.menu.MRstatus:
		case maintoolbar.menu.settings:
            return castMailEventsBody(value, objContact);
            break;
        case maintoolbar.menu.documentmanagement: //7
            return castMailAdminBody(value, objContact);
            break;
        default:
            break;
    }
}

var combomailtypes = {
    option: { offer: "1", regularorder: "2", vcard: "7", IntegrationPackage: "8", IntReports: "9", trial: "11", retrial: "12", webmeeting: "13", orderblanco: "14", vcardAftersales: "15" }
}

function castMailAdminBody(value, objContact) {
    switch (value) {
        case combomailtypes.option.trial:
            return GetEmptyTrial(value, objContact);
            break;
        case combomailtypes.option.retrial:
            return GetEmptyTrial(value, objContact);
            break;
        case combomailtypes.option.IntegrationPackage:
        case combomailtypes.option.IntReports:
            return SetIntegrationPackage(value, objContact);
            break;
        case combomailtypes.option.vcard:
            return SetVCard(value, objContact);
            break;
        case combomailtypes.option.orderblanco:
            return SetOrder(value);
        case combomailtypes.option.vcardAftersales:
            return SetVCardAftersales(value, objContact);
        default:
            return SetDefaultGeneric(value, objContact);
            break;
    }
    InitializeObjMail();
}

function castMailEventsBody(value, objContact) {
    switch (value) {
        case combomailtypes.option.trial:
            return GetTrial(value, objContact);
            break;
        case combomailtypes.option.retrial:
            return GetTrial(value, objContact);
            break;
        case combomailtypes.option.IntegrationPackage:
        case combomailtypes.option.IntReports:
            return SetIntegrationPackage(value, objContact);
            break;
        case combomailtypes.option.vcard:
            return SetVCard(value, objContact);
            break;
        case combomailtypes.option.orderblanco:

            return SetOrder(value);

        default:
            return SetDefaultGeneric(value, objContact);
            break;
    }
    InitializeObjMail();
}
// unused so far
var objMailLocal;
function InitializeObjMail() {
    objMailLocal = {
        Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
        MailTo: document.getElementsByName('MailTo')[0].value,
        MailCC: document.getElementsByName('MailCC')[0].value,
        MailBCC: '',
        Subject: document.getElementsByName('Subject')[0].value,
        Body: mailEditor.getContent(),
        IsHTML: 1,
        Attachment: attachments,
        customerlang: objContact.lang_radical,
        trusted: 1,
        company_id: objCy.company_id,
        contact_id: objContact.contact_id, //contactIndex :i
        user_id: getValuefromCookie("TELESALESTOOLV2", "usrId"),
    };
}

function castAtDear(customerlang, customergender) {
    switch (customerlang) {
        case "FR":
            switch (customergender) {
                case "M":
                    return 'Cher';
                    break;
                case "F":
                    return 'Chère';
                    break;
				default:
					return 'Chèr(e)';
            }
        case "NL":
            return 'Geachte';
            break;
    }
}

function SetOrder(value) {
    var customerlang = objContact.lang_radical;
    var company_id = objCy.company_id;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');

    var docdata = Ajax_GetTemplateBodyOrder(value);
    docdata = docdata.split('|');
    var attDoc = docdata[0];
    var b = docdata[1]; //docdataAjax_GetTemplateBodyOrder(value);
    var res = '';
    var username = objContact.contact_title + ' ' + objContact.contact_lastname;
    var customergender = objContact.contact_gender;
    var dear = castAtDear(customerlang, customergender);
    var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction'];
    var newval = [dear, username, salesfirstname, saleslastname, hrfunction];
    var j;
    for (j = 0; j < find.length; j++) {
        var regexIn = new RegExp(find[j], "g")
        data = data.replace(regexIn, newval[j]);
    }
    var docName = attDoc.split('\\');
    var int = docName.length;

    var docimg = '<a href=showpdf.asp?type=bx&file=' + attDoc + ' target=_blank><img src=/graphics/common/win_16x16/pdf.png></a>';

    var removeimg = '' //'<img src=/graphics/common/win_16x16/delete1.png ondblclick=remove_data("' + attDoc.replace(/\\/g, '/') + '")>';
    attList.add({ img: docimg, doc_file_name: docName[int - 1], imgremove: removeimg }, 0)
    attachments = attDoc;
    //docs.push(attDoc);
    return b;
}

function SetDefaultGeneric(value, objContact) {
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');

    Ajax_GetTemplateBodyPromise(value, customerlang)
    .done(function (data) {
        var username = objContact.contact_title + ' ' + objContact.contact_lastname;
        var customergender = objContact.contact_gender;
        var dear = castAtDear(customerlang, customergender);

        var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction'];
        var newval = [dear, username, salesfirstname, saleslastname, hrfunction];
        var j;
        for (j = 0; j < find.length; j++) {
            var regexIn = new RegExp(find[j], "g")
            data = data.replace(regexIn, newval[j]);
        }

        mailEditor.setContent(data);        
    });
}

function GetVCard(objContact) {
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');
    var t = '';

    t += '<table width=320px ><tr><td><b>' + salesfirstname + ' ' + saleslastname + '</b></td></tr>';
    t += '<tr><td>' + hrfunction + '</td></tr>';
    t += '<tr><td><img src="http://www.companyweb.be/img/mails/cwbizcardfooter.png"></td></tr>';
    t += '</table>';

    return t;
}

function SetVCardAftersales(value, objContact) {
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');
    var vcard = GetVCard(objContact);

    Ajax_GetTemplateBodyPromise(value, customerlang)
    .done(function(data)
    {
        var username = objContact.contact_title + ' ' + objContact.contact_lastname;
        var customergender = objContact.contact_gender;
        var dear = castAtDear(customerlang, customergender);
        var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction', '@vcard'];
        var newval = [dear, username, salesfirstname, saleslastname, hrfunction, vcard];
        var j;
        for (j = 0; j < find.length; j++) {
            var regexIn = new RegExp(find[j], "g")
            data = data.replace(regexIn, newval[j]);
        }
        mailEditor.setContent(data);
    });
}

function SetVCard(value, objContact) {
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');
    var vcard = GetVCard(objContact);

    Ajax_GetTemplateBodyPromise(value, customerlang)
    .done(function(data)
    {
        var username = objContact.contact_title + ' ' + objContact.contact_lastname;
        var customergender = objContact.contact_gender;
        var dear = castAtDear(customerlang, customergender);
        var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction', '@vcard'];
        var newval = [dear, username, salesfirstname, saleslastname, hrfunction, vcard];
        var j;
        for (j = 0; j < find.length; j++) {
            var regexIn = new RegExp(find[j], "g")
            data = data.replace(regexIn, newval[j]);
        }
        var data = mailEditor.getContent() + data;
        mailEditor.setContent(data);
    });
}

function SetIntegrationPackage(value, objContact) {
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');

    Ajax_GetTemplateBodyPromise(value, customerlang)
    .done(function(data)
    {
        var username = objContact.contact_title + ' ' + objContact.contact_lastname ;
        var customergender = objContact.contact_gender;
        var dear = castAtDear(customerlang, customergender);
        var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction'];
        var newval = [dear, username, salesfirstname, saleslastname, hrfunction];
        var j;
        for (j = 0; j < find.length; j++) {
            var regexIn = new RegExp(find[j], "g")
            data = data.replace(regexIn, newval[j]);
        }
        mailEditor.setContent(data);
    });
}

// this is used for account executives
function GetTrial(value, objContact) {
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');

    Ajax_GetTemplateBodyPromise(value, customerlang)
    .done(function(body)
    {
        var data = body;
        var res = '';
        var username = objContact.contact_title + ' ' + objContact.contact_lastname;

        var customergender = objContact.contact_gender;
        var dear = castAtDear(customerlang, customergender);
        var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction'];
        var newval = [dear, username, salesfirstname, saleslastname, hrfunction];
        var j;
        for (j = 0; j < find.length; j++) {
            var regexIn = new RegExp(find[j], "g")
            data = data.replace(regexIn, newval[j]);
        };

        mailEditor.setContent(data);
    });
}

//this is used for higher access level than account executive
function GetEmptyTrial(value, objContact) {
    var username = '';
    var customerlang = objContact.lang_radical;
    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');
    $.when(Ajax_GetTemplateBodyPromise(value, customerlang), JSON_GetTrialLoginPwdPromise())
    .done(function(body, trial)
    {
        var data = body[0];
        var res = '';
        var login = trial[0].login;
        var password = trial[0].password;
        var customergender = objContact.contact_gender;

        var dear = castAtDear(customerlang, customergender);
        var find = ['@dear', '@username', '@salesfirstname', '@saleslastname', '@hrfunction', '@login', '@password'];
        var newval = [dear, username, salesfirstname, saleslastname, hrfunction, login, password];
        var j;
        for (j = 0; j < find.length; j++) {
            var regexIn = new RegExp(find[j], "g")
            data = data.replace(regexIn, newval[j]);
        };

        mailEditor.setContent(data);
    });
}

var cbxDocNatures;
function GetComboDocNatures() {
	if (!$("#combo_docnatures").data('requestRunning')){
		$("#combo_docnatures").data('requestRunning',true);
		$("#combo_docnatures").empty();
		cbxDocNatures = new dhtmlXCombo("combo_docnatures", null, null, "image");
		cbxDocNatures.setImagePath("/graphics/common/win_16x16/");
		cbxDocNatures.enableFilteringMode(true);
		cbxDocNatures.allowFreeText(false);

		Ajax_GetComboDocNaturesWithImgPromise()
		.done(function (data) {
			cbxDocNatures.load(data, function () {
				cbxDocNatures.selectOption(0);
			});
		}).always(function(){
			$("#combo_docnatures").data('requestRunning',false);
		});

		cbxDocNatures.attachEvent("onChange", function (value, text) {
			docsGrid.clearAll();
			var c = '?doc_lang_id=' + cbxDocLangs.getSelectedValue();
			Ajax_GetDocumentsListPromise('doc_nature_id', value, c)
			.done(function (data) {
				docsGrid.parse(data, "json");
			})
		});
	}
	return $.when(null);
}

var cbxDocTypes;
function GetComboDocTypes() {
	if (!$("#combo_doctypes").data('requestRunning')){
		$("#combo_doctypes").data('requestRunning',true);
		$("#combo_doctypes").empty();
		cbxDocTypes = new dhtmlXCombo("combo_doctypes", null, null, "image");
		cbxDocTypes.setImagePath("/graphics/common/win_16x16/");
		cbxDocTypes.enableFilteringMode(true);
		cbxDocTypes.allowFreeText(false);

		Ajax_GetComboDocTypesWithImgPromise()
		.done(function (data) {
			cbxDocTypes.load(data, function () {
				cbxDocTypes.selectOption(0);
			});
		}).always(function(){
			$("#combo_doctypes").data('requestRunning',false);
		});

		cbxDocTypes.attachEvent("onChange", function (value, text) {
			docsGrid.clearAll();
			var c = '?doc_lang_id=' + cbxDocLangs.getSelectedValue();
			Ajax_GetDocumentsListPromise('doc_type_id', value, c)
			.done(function (data) {
				docsGrid.parse(data, "json");
			})
		});
	}
	return $.when(null);
}

var cbxDocLangs;
function GetComboDocLangs(objContact) {
	if (!$("#combo_doclangs").data('requestRunning')){
		$("#combo_doclangs").data('requestRunning',true);
		$("#combo_doclangs").empty();
		cbxDocLangs = new dhtmlXCombo("combo_doclangs", "languages", null, "image");
		cbxDocLangs.setImagePath("/graphics/common/win_16x16/");
		cbxDocLangs.enableFilteringMode(true);
		cbxDocLangs.allowFreeText(false);
	
	
		cbxDocLangs.attachEvent("onChange", function (value, text) {
			docsGrid.clearAll();
			Ajax_GetDocumentsListPromise('doc_lang_id', value, '')
			.done(function (data) {
				docsGrid.parse(data, "json");
			})
		});
	
		Ajax_GetDocLangsWithImgPromise()
		.done(function (data) {
			cbxDocLangs.load(data, function () {
				var customerlang = objContact.lang_radical;
				if (customerlang) {
					switch (customerlang) {
						case "NL":
							customerlang = 1;
							break;
						default:
							customerlang = 0;
							break;
					}
					cbxDocLangs.selectOption(customerlang);
				}
			});
		}).always(function(){
			$("#combo_doclangs").data('requestRunning',false);
		});
	}
	return $.when(null);
}

//AJAX DROPDOWNS
function Ajax_GetComboGenericMailTemplatesWithImgPromise() {
    var url = "/classes/dal/clsMailTemplates/clsMailTemplate.asp";
    return $.get(url, { a: "GetComboGenericMailTemplatesWithImg" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failed to get combo generic mail templates with images",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetComboDocNaturesWithImgPromise() {
    var url = "/classes/dal/clsDocuments/clsDocument.asp";

    return $.get(url, { a: "GetComboDocNaturesWithImg" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failed getting Combo Doc natures with Images",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetComboDocTypesWithImgPromise() {
    var url = "/classes/dal/clsDocuments/clsDocument.asp";

    return $.get(url, { a: "GetComboDocTypesWithImg" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failed getting Combo Doc Types with Images",
            expire: -1,
            type: "errormessage"
        });
    });
   
}

function Ajax_GetDocLangsWithImgPromise() {
    var url = "/classes/dal/clsDocuments/clsDocument.asp";

    return $.get(url, { a: "GetComboDocLangsWithImg" })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Failed to get Document languages with Images",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetDocumentsListPromise(fieldname, fieldvalue, multirequest) {
    var url = "/classes/dal/clsDocuments/clsDocument.asp" + multirequest;

    return $.get(url, { a: "GetDocumentsList", [fieldname]: fieldvalue})
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Document List",
            expire: -1,
            type: "errormessage"
        });
    });
}

function changeCursor(element, cursor) {
    var elementToChange = document.getElementById(element);
    document.body.style.cursor = cursor;
}

function btnSendMail_onClick(contactId, companyId) {
	if ($("#outlooksenbutton").data('requestRunning')){
		return $.when(null);
	}
	sendOptionalMail(contactId, companyId);
}

function enableSendOption()
{
	$("#outlooksenbutton").data('requestRunning', false);
	$("#outlooksenbutton").prop('disabled', false);
    $("#outlooksenbutton").show();
    $("#outlooksenbutton").css({'cursor' : 'default'});
    $("#editorObj").css({ 'cursor': 'default' });

    switch (cbxMailTemplates.getSelectedValue()) {
        case combomailtypes.option.trial:
        case combomailtypes.option.retrial:
            //refresh trial component here
            JSON_displayStCompanyBackOfficeDetails(objEvent.company_vat, objEvent.company_hbc_id, "stBackOffice");
            JSONGetCompanyBackOfficeUserInfoPromise(objEvent.company_vat, objEvent.company_hbc_id, "stBackOfficeUserInfo");
            break;
        default:
            break;
    }
	if (objWin != null) {
        objWin.progressOff();
    }

}
function doForMail(objMail)
{
	return $.when(AjaxMailer_reSendMailPromise(objMail))
	.then(function(data,status, jqxhr)
	{
		if (data === ""){
			return data;
		}else{
			return $.Deferred().reject(data);
		}
	})
	.done(function (data) {
		switch (data) {
			case "":
				returnMail = 1;
				break;
			default:
				returnMail = data;
				break;
		}
	}).fail(function(data)
	{
		dhtmlx.message({
			text: 'error encountered in doForMail',
			expire: -1,
			type: "errormessage"
		});
	});
}

function createObjMail(contactId)
{
	switch (clickedMenu)//mainMenuSource : menu: {events: "2", documentmanagement: "7"}
    {
        case maintoolbar.menu.documentmanagement:
            var mTo = document.getElementsByName('MailTo')[0].value;
            var mCC = document.getElementsByName('MailCC')[0].value;

            if (!ValidateEmail(mTo, "To")) {
                return null;
            }
            if (!isNullOrWhitespace(mCC) && !ValidateEmail(mCC, "Carbon Copy")) {
                return null;
            }

            var objMail = {
				mail_template_id: $("#mail_template_id").val(),
                Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
                MailTo: mTo,
                MailCC: mCC,
                MailBCC: varGlobalMailBcc,
                Subject: document.getElementsByName('Subject')[0].value,
                Body: mailEditor.getContent(),
                IsHTML: 1,
                Attachment: attachments,
                customerlang: getValuefromCookie("TELESALESTOOLV2", "lang_radical"),
                trusted: 1,
                company_id: 0, //objCy.company_id,
                contact_id: 0, //objContact.ContactDetail[contactIndex].contact_id, //contactIndex :i
                user_id: getValuefromCookie("TELESALESTOOLV2", "usrId"),
            };
			return objMail;
        default:
            var mTo = document.getElementsByName('MailTo')[0].value;
            var mCC = document.getElementsByName('MailCC')[0].value;

            if (!ValidateEmail(mTo, "To")) {
                return 0;
            }
            if (!isNullOrWhitespace(mCC) && !ValidateEmail(mCC, "Carbon Copy")) {
                return 0;
            }

            var objMail = {
				mail_template_id: $("#mail_template_id").val(),
                Mailfrom: getValuefromCookie("TELESALESTOOLV2", "email1"),
                MailTo: mTo,
                MailCC: mCC,
                MailBCC: varGlobalMailBcc,
                Subject: document.getElementsByName('Subject')[0].value,
                Body: mailEditor.getContent(),
                IsHTML: 1,
                Attachment: attachments,
                customerlang: castDbLang(cbxDocLangs.getSelectedValue()),
                trusted: 1,
                company_id: $("#company_id").val(),
                contact_id: contactId,
                user_id: getValuefromCookie("TELESALESTOOLV2", "usrId"),
            };
            return objMail;
    }
}
function doForTrials(contactId)
{
	return Ajax_SetCompanyBackOfficeAfterTrialSendPromise(contactId)
	.done(function(data)
	{
		dhtmlx.message({
			text: 'Done doing stuff for Trials',
			expire: 500,
			type: "successmessage"
		});
	}).fail(function(data)
	{
		dhtmlx.message({
			text: 'error encountered in doing stuff for trials',
			expire: -1,
			type: "errormessage"
		});
	});
}
function disableSendoption()
{
	$("#outlooksenbutton").data('requestRunning', true);
	if (objWin != null) {
        objWin.progressOn();
    }
	
	$("#outlooksenbutton").prop('disabled', true);
    $("#outlooksenbutton").hide();
    $("#outlooksenbutton").css({ 'cursor': 'not-allowed' });
    $("#editorObj").css({ 'cursor': 'not-allowed' });
}
function sendOptionalMail(contactId) {

	disableSendoption();
	
    switch (clickedMenu)//mainMenuSource : menu: {events: "2", documentmanagement: "7"}
    {
        case maintoolbar.menu.events: //2
        case maintoolbar.menu.agenda:
        case maintoolbar.menu.search:
		case maintoolbar.menu.MRstatus:
		case maintoolbar.menu.settings:
            switch (cbxMailTemplates.getSelectedValue()) {
                case combomailtypes.option.trial:
                case combomailtypes.option.retrial:
                    doForTrials(contactId)
					.done(function(data)
					{
						var objMail = createObjMail(contactId);
						
						if (!$.isEmptyObject(objMail))
							doForMail(objMail)
							.done(function(data)
							{
								dhtmlx.message({
									text: "Successfully done doForMail in Trials",
									expire: 500,
									type: "successmessage"
								});
							}).fail(function(data)
							{
								dhtmlx.message({
									text: "Error DoForMail mail for Trials",
									expire: -1,
									type: "errormessage"
								});
							});
					}).always(function(data)
					{
						enableSendOption();
					});
					break;
                default:
                    var objMail = createObjMail(contactId);
					
					if (!$.isEmptyObject(objMail))
					{
							doForMail(objMail)
							.done(function(data)
							{
								dhtmlx.message({
									text: "Successfully done doForMail in Trials",
									expire: 500,
									type: "successmessage"
								});
							}).fail(function(data)
							{
								dhtmlx.message({
									text: "Error DoForMail mail for Trials",
									expire: -1,
									type: "errormessage"
								});
							});
					}
					enableSendOption();
                    break;
            }

            break;
        case maintoolbar.menu.documentmanagement: //7
			var objMail = createObjMail(contactId);
			
			if (!$.isEmptyObject(objMail))
			{
				doForMail(objMail)
				.done(function(data)
				{
					dhtmlx.message({
						text: "Successfully done doForMail for DocManager",
						expire: 500,
						type: "successmessage"
					});
				}).fail(function(data)
				{
					dhtmlx.message({
						text: "Error DoForMail mail for DocManager",
						expire: -1,
						type: "errormessage"
					});
				});
			}
			enableSendOption();
            break;
        default:
			var objMail = createObjMail(contactId);
			
			if (!$.isEmptyObject(objMail))
			{
				doForMail(objMail)
				.done(function(data)
				{
					dhtmlx.message({
						text: "Successfully done doForMail in Default",
						expire: 500,
						type: "successmessage"
					});
				}).fail(function(data)
				{
					dhtmlx.message({
						text: "Error DoForMail mail for Default",
						expire: -1,
						type: "errormessage"
					});
				});
			}
			enableSendOption();
            break;
    }
}
function isNullOrWhitespace(input) {
    if (typeof input === 'undefined' || input == null) return true;

    return input.replace(/\s/g, '').length < 1;
}

function ValidateEmail(inputText, fieldText) {
    var mailformat = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (inputText.match(mailformat)) {
        return true;
    }
    else {
        var message = locale.popup.msgemailerror + "<br /> ->" + fieldText;
        dhtmlx.message({
            text: message,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(message);
        return false;
    }
}

var objTrialCredentials;

function JSON_GetTrialLoginPwdPromise() {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    return $.get(url, {q:11})
    .fail(function(data){
        dhtmlx.message({
            text: "Error getting Trial Logins",
            expire: -1,
            type: "errormessage"
        });
    });

}

function JSON_GetReTrialLoginPwdPromise(contactId) {
	var cyId = $("#company_id").val();
	AjaxContact_GetContactFromCompanyPromise(contactId, cyId)
	.done(function(data)
	{
		var objContact = data;
		
		var HBU_ID = objContact.contact_HBU_ID;
		var HBC_Firmvat = objContact.company_vat;
		var HBC_ID = objContact.company_hbc_id;
		
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
		
		return $.get(url, { q : 13, HBU_ID : HBU_ID, HBC_ID : HBC_ID, vat : HBC_FirmVat})
		.fail(function(data)
		{
			dhtmlx.message({
				text: "Error getting ReTrial Login",
				expire: -1,
				type: "errormessage"
			});
		});
	});
}

function Ajax_GetTemplateSubjectPromise(id, customerlang) {
    var url = "/classes/dal/clsMailer/clsMailer.asp";

    return $.get(url, { a: 5, mail_template_id: id, customerlang: customerlang })
    .fail(function (data) {
        dhtmlx.message({
            text: "Error getting Template Subject",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetTemplateBodyPromise(id, customerlang) {
    var url = "/classes/dal/clsMailer/clsMailer.asp";

    return $.get(url, { a: 4, mail_template_id: id, customerlang: customerlang })
    .fail(function (data) {
        dhtmlx.message({
            text: "Error getting Template Body",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetTemplateBodyOrder(id) {
    var xmlhttp = new XMLHttpRequest();
    var customerlang = objContact.ContactDetail != null ? objContact.ContactDetail[contactIndex].lang_radical : castDbLang(cbxDocLangs.getSelectedValue());;
    var params = populateobjBoCompanyOrderInfo();
    var url = "/classes/dal/clsMailer/clsMailer.asp?a=4&mail_template_id=" + id + '&customerlang=' + customerlang;
    var resp = '';
    xmlhttp.open("POST", url, false);
    //Send the proper header information along with the request
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            resp = xmlhttp.responseText;
        };
    };

    xmlhttp.send(params);
    return resp;
}
function populateobjBoCompanyOrderInfo() {
    var user_id = getValuefromCookie("TELESALESTOOLV2", "usrId");

    var salesfirstname = getValuefromCookie("TELESALESTOOLV2", "firstname");
    salesfirstname = salesfirstname.replace(/\+/g, ' ');
    var saleslastname = getValuefromCookie("TELESALESTOOLV2", "lastname");
    saleslastname = saleslastname.replace(/\+/g, ' ');
    var hrfunction = getValuefromCookie("TELESALESTOOLV2", "hrfuncname");
    hrfunction = hrfunction.replace(/\+/g, ' ');

    var m_order_date = GetEuroCurrentDate('-');
    var m_end_date = GetEuroDatePlusXdays(0, 365, '-');
    var m_package = 1;
    var m_mobapps = 1;

    var lang = objContact.ContactDetail != null ? objContact.ContactDetail[contactIndex].lang_radical : castDbLang(cbxDocLangs.getSelectedValue());

    var firstName = objContact.ContactDetail != null ? objContact.ContactDetail[contactIndex].contact_firstname : '';

    var lastName = objContact.ContactDetail != null ? objContact.ContactDetail[contactIndex].contact_lastname : '';

    var emailAddress = objContact.ContactDetail != null ? objContact.ContactDetail[contactIndex].contact_email : ''

    var params = "company_id=" + encodeURIComponent(objEvent.company_id);
    params += "&event_id=" + encodeURIComponent(objEvent.event_id);
    params += "&company_hbc_id=" + encodeURIComponent(objEvent.company_hbc_id);
    params += "&company_name=" + encodeURIComponent(objEvent.company_name);
    params += "&company_vat=" + encodeURIComponent(objEvent.company_vat);

    params += "&customerlang=" + encodeURIComponent(lang);
    params += "&user_id=" + encodeURIComponent(user_id);
    params += "&salesfirstname=" + encodeURIComponent(salesfirstname);
    params += "&saleslastname=" + encodeURIComponent(saleslastname);
    params += "&hrfunction=" + encodeURIComponent(hrfunction);

    params += "&company_tel=" + encodeURIComponent(objCy.company_tel);

    params += "&company_address_street=" + encodeURIComponent(objCy.company_address_street);
    params += "&company_address_number=" + encodeURIComponent(objCy.company_address_number);
    params += "&company_address_boxnumber=" + encodeURIComponent(objCy.company_address_boxnumber);
    params += "&company_address_postcode=" + encodeURIComponent(objCy.company_address_postcode);
    params += "&company_address_localite=" + encodeURIComponent(objCy.company_address_localite);

    params += "&contact_firstname=" + encodeURIComponent(firstName);
    params += "&contact_lastname=" + encodeURIComponent(lastName);

    params += "&contact_email=" + encodeURIComponent(emailAddress);
    params += "&lang_radical=" + encodeURIComponent(castLangRadical(objCy.lang_radical));

    params += "&order_date=" + encodeURIComponent(m_order_date);
    params += "&end_date=" + encodeURIComponent(m_end_date);
    params += "&package=" + encodeURIComponent(m_package);
    params += "&mobapps=" + encodeURIComponent(m_mobapps);
    params += "&trusted=" + encodeURIComponent(1);

    return params;
}

function populateobjBoUserTrialInfo() {
    var params = "ID=" + encodeURIComponent(objContact.ContactDetail[contactIndex].contact_id);
    params += "&HBU_ID=" + encodeURIComponent(NullUndefinedToNewVal(objContact.ContactDetail[contactIndex].contact_HBU_ID, "0"));
    params += "&HBU_HBC_ID=" + encodeURIComponent(NullUndefinedToNewVal(objEvent.company_hbc_id, "0"));
    params += "&HBU_Origin=14";
    params += "&HBU_TrialOrigin=SALESTOOL";
    params += "&HBU_UserName=" + encodeURIComponent(objTrialCredentials[0].login);
    params += "&HBU_Zoeknaam=" + encodeURIComponent(objTrialCredentials[0].login);
    params += "&HBU_Password=" + encodeURIComponent(objTrialCredentials[0].password);
    params += "&HBU_LastUpdateUser=WEBTRY";
    params += "&HBU_AccLvl_BLOCKED=0";
    params += "&HBU_AccLvl_DmBLOCKED=0";
    params += "&HBU_AccLvl_Website=1";
    params += "&HBU_Aantal_Website=25";
    params += "&HBU_Einde_Website=" + encodeURIComponent(objTrialCredentials[0].End_Website);
    params += "&HBU_AccLvl_Followups=0";
    params += "&HBU_Aantal_Followups=0";
    params += "&HBU_AccLvl_FirstReports=0";
    params += "&HBU_Aantal_FirstReports=0";
    params += "&HBU_intlang=" + encodeURIComponent(objContact.ContactDetail[contactIndex].contact_lang_id);
    params += "&HBU_AccLvl_Mandaten=1";
    return params;
}
function populateobjBoGlobalTrialInfo(objContact, objCy, objTrial) {
    var params = "company_id=" + encodeURIComponent(objCy.company_id);
    params += "&event_id=" + encodeURIComponent($("#event_id").val());
    params += "&HBC_ID=" + encodeURIComponent(NullUndefinedToNewVal(objCy.company_hbc_id, "0"));
    params += "&HBC_FirmVat=" + encodeURIComponent(NullUndefinedToNewVal(objCy.company_vat, "0"));
    params += "&HBC_FirmName=" + encodeURIComponent(objCy.company_name);
    params += "&HBC_ContactName=" + encodeURIComponent(objContact.contact_firstname + " " + objContact.contact_lastname);
    params += "&HBC_ContactEmail=" + encodeURIComponent(objContact.contact_email);
    params += "&HBC_Tel=" + encodeURIComponent(objCy.company_tel);
    params += "&HBC_origin=14";
    params += "&HBC_intLang=" + encodeURIComponent(castLangRadical(objCy.lang_radical));
    params += "&HBC_ExtCode=" + encodeURIComponent(objTrial.login);
    params += "&HBC_nota=" + encodeURIComponent(objTrial.login);
    params += "&HBC_ConcurrentID=0";
    params += "&HBC_DatamarketID=0";
    params += "&contact_id=" + encodeURIComponent(objContact.contact_id);
    params += "&HBU_ID=" + encodeURIComponent(NullUndefinedToNewVal(objContact.contact_HBU_ID, "0"));
    params += "&HBU_HBC_ID=" + encodeURIComponent(NullUndefinedToNewVal(objCy.company_hbc_id, "0"));
    params += "&HBU_Origin=14";
    params += "&HBU_TrialOrigin=SALESTOOL";
    params += "&HBU_UserName=" + encodeURIComponent(objTrial.login);
    params += "&HBU_Zoeknaam=" + encodeURIComponent(objTrial.login);
    params += "&HBU_Password=" + encodeURIComponent(objTrial.password);
    params += "&HBU_LastUpdateUser=WEBTRY";
    params += "&HBU_AccLvl_BLOCKED=0";
    params += "&HBU_AccLvl_DmBLOCKED=0";
    params += "&HBU_AccLvl_Website=1";
    params += "&HBU_Aantal_Website=25";
    params += "&HBU_Einde_Website=" + encodeURIComponent(objTrial.End_Website);
    params += "&HBU_AccLvl_Followups=0";
    params += "&HBU_Aantal_Followups=0";
    params += "&HBU_AccLvl_FirstReports=0";
    params += "&HBU_Aantal_FirstReports=0";
    params += "&HBU_intlang=" + encodeURIComponent(objContact.contact_lang_id);
    params += "&HBU_AccLvl_Mandaten=1";
    params += "&HBU_FollowupEmail=" + encodeURIComponent(objContact.contact_email);;
    return params;
}

function castLangRadical(radical) {
    switch (radical) {
        case "FR":
            return "1";
            break;
        case "NL":
            return "2";
            break;
        default:
            return "1";
            break;
    }
}

function Ajax_SetCompanyBackOfficeAfterTrialSendPromise(contactId, value) {	
	var cyId = $("#company_id").val();
	
	return $.when(
		AjaxContact_GetContactFromCompanyPromise(contactId, cyId), 
		JSONGetCompanyFromId(cyId),
		JSON_GetTrialLoginPwdPromise()
	).done(function(contactObj, cyObj, trialObj)
	{
		var objContact = contactObj[0];
		var objCy = cyObj[0];
		var objTrial = trialObj[0];		
		
		var b = mailEditor.getContent();
		
		var find = ['@login', '@password'];
		var newval = [objTrial.login, objTrial.password];
		var j;
		for (j = 0; j < find.length; j++) {
			var regexIn = new RegExp(find[j], "g")
			b = b.replace(regexIn, newval[j]);
		};
		
		mailEditor.setContent(b);
		
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp?q=12";
		var params = populateobjBoGlobalTrialInfo(objContact, objCy, objTrial);
	
		$.post(url, params)
		.then(function(data,status, jqxhr)
		{
			if (data === "1"){
				return data;
			}else{
				return $.Deferred().reject(data);
			}
		})
		.done(function (data) {
				dhtmlx.message({
					text: locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
				dhtmlx.alert(imgalertsaveok + locale.main_page.lblSaveSuccess);
		}).fail(function(data)
		{
				dhtmlx.message({
					text: locale.main_page.lblsavefailed,
					expire: -1,
					type: "errormessage"
				});
				dhtmlx.alert(imgalertsavenok + locale.main_page.lblsavefailed);
		});
		
	});
}