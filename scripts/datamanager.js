﻿var insideLayout = '';
var trialCal = '';
var curDateObj = '';
var dataMGrid = '';

var userid = "0";

function DataM_Initialize(myinsidelayout) {
    insidelayout = myinsidelayout;
    userid = getValuefromCookie("TELESALESTOOLV2", "usrId");
    myinsidelayout.cells("a").setText('TeleSales Info');
    myinsidelayout.cells("b").setText('BackOffice Info');
    myinsidelayout.cells("a").showInnerScroll();
    myinsidelayout.cells("b").showInnerScroll();

    dataMGrid = myinsidelayout.cells("a").attachGrid();

    //myinsidelayout.cells("a").setText(locale.main_page.lblDirtyLeads + iframecall);

    ConfigdataMGrid(dataMGrid);
    GetCompaniesWithoutBackOfficeId(dataMGrid);
}

function ConfigdataMGrid(dataMGrid) {
    var tbHeader = 'Co ID,Bo ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.lblVatCompany + ',Event ID,Start Date,Event Type'

    dataMGrid.setHeader(tbHeader);
    dataMGrid.setInitWidths("60,60,,100,60,140,");
    dataMGrid.setColAlign("left,left,left,left,left,left,left");
    dataMGrid.setColTypes("ro,ed,ro,ro,ro,ro,ro");
    dataMGrid.setColSorting("str,str,str,str,str,str,str");
    dataMGrid.init();
    dataMGrid.attachEvent("onRowSelect", doOnRowSelectdataMGrid);
    dataMGrid.attachEvent("onEditCell", function (stage, rId, cInd, nValue, oValue) {
        if (stage === 2) {
            var objCompany = {
                company_id: dataMGrid.cells(rId, 0).getValue(),
                company_hbc_id: nValue,
                trusted: 1
            }
            var message = 'Company ID: ' + objCompany.company_id + ' updated with BO id:' + objCompany.company_hbc_id;
            dhtmlx.message({
                text: message,
                expire: 500,
                type: "successmessage"
            });
            dhtmlx.alert(message);
            dataMGrid.cells(rId, cInd).setValue(nValue);
            AjaxCompany_UpdateBackOfficeID(objCompany);
            dataMGrid.deleteRow(rId);
        }
    });
}

function doOnRowSelectdataMGrid(id, ind) {
    var company_vat = dataMGrid.cells(id, 3).getValue();
    GetCompaniesBackOfficeInfoByVatNumber(company_vat);
    return true;
}

function GetCompaniesWithoutBackOfficeId(dataMGrid) {
    dataMGrid.clearAll();
    AjaxCompany_JSONGetCompaniesWithoutBackOfficeIdPromise()
    .done(function (data) {
        dataMGrid.parse(data, "json");
    })
}

function AjaxCompany_JSONGetCompaniesWithoutBackOfficeIdPromise() {
    var url = "/classes/dal/clsCompany/clsCompany.asp";

    return $.get(url, { a: "JSONGetCompaniesWithoutBackOfficeId" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Companies without backoffice IDs",
            expire: -1,
            type: "errormessage"
        });
    });
}

function GetCompaniesBackOfficeInfoByVatNumber(company_vat) {
    var mydataset = "/classes/dal/clsBackOffice/clsBackOffice.asp?q=18&vat=" + company_vat
    var dhxDataView = myinsidelayout.cells("b").attachDataView({
        type: {
            template: "<table width=100%><tr><td><b>#HBC_ID#</b></td></tr><tr><td>#HBC_Firmvat#</td></tr><tr><td><b>#HBC_FirmName#</b></td></tr></table>",
            padding: 2,
            height: 100,
            //width: 100
        },
        drag: true,
        select: true
    });
    dhxDataView.load(mydataset, "json");
}