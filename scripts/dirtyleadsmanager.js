﻿var insideLayout = '';
var mysublayout01 = '';
var curDateObj = '';
var qGrid = '';

var userid = "0";
var formname = 'Form_TsCompanyEditor';

function QM_InitializeDirtyLeadsEngine(myinsidelayout) {
    insidelayout = myinsidelayout;
    userid = getValuefromCookie("TELESALESTOOLV2", "usrId");
    myinsidelayout.cells("a").setText(locale.main_page.lblDirtyLeads);
    myinsidelayout.cells("b").setText("Companyweb Search");
    myinsidelayout.cells("c").setText("Details");
    myinsidelayout.cells("a").setHeight(150);
    myinsidelayout.cells("b").setHeight(250);
    myinsidelayout.cells("a").showInnerScroll();
    myinsidelayout.cells("b").showInnerScroll();
    myinsidelayout.cells("c").showInnerScroll();

    mysublayout01 = myinsidelayout.cells("c").attachLayout({ pattern: '3L' });
    mysublayout01.cells("a").setText("TeleSales V02.00");
    mysublayout01.cells("b").setText("BackOffice");
    mysublayout01.cells("c").setText("BackOffice Details");
    mysublayout01.cells("a").showInnerScroll();
    mysublayout01.cells("b").showInnerScroll();
    mysublayout01.cells("c").showInnerScroll();

    qGrid = myinsidelayout.cells("a").attachGrid();
    ConfigqGrid(qGrid);
    GetLeadQueue(qGrid);
    PopulateCompanywebSearch(myinsidelayout.cells("b"), "");
    QM_InitializeTsEditToolbar(mysublayout01.cells("a"));
    mysublayout01.cells("a").attachHTMLString(SetFormDiv_TsCompanyEditor(formname));
    DesignEmptyForm_TsCompanyEditor(formname, mysublayout01, "a");
}

function SetTelesalesVatAndCompany(vat, company) {
    formEditor.setItemValue("company_vat", vat);
    formEditor.setItemValue("company_name", company);
}
function PopulateCompanywebSearch(space, fName) {
    space.attachURL("/searchframe.asp?fName=" + fName);
}
function ConfigqGrid(qGrid) {
    var tbHeader = 'id,' + locale.main_page.lblEntreprise + ',' + locale.main_page.lblContact + ',' + locale.main_page.lblVatCompany + ',' + locale.main_page.lblCallNumber + ',@'
    qGrid.setColumnHidden(0, true);
    qGrid.setHeader(tbHeader);
    qGrid.setInitWidths("40,400,400,100,100,30");
    qGrid.setColAlign("left,left,left,left,left,left");
    qGrid.setColTypes("ro,ed,ed,ed,ed,ro");
    qGrid.setColSorting("str,str,str,str,str,img");
    qGrid.init();
    qGrid.attachEvent("onRowSelect", doOnRowSelectqGrid);
}
function doOnRowSelectqGrid(id, ind) {
    formEditor.setItemValue('lq_id', id);
    var c = escape(qGrid.cells(id, 1).getValue());
    var telephone = qGrid.cells(id, 4).getValue();
    var contact = qGrid.cells(id, 2).getValue();

    LQ_ResetValuesToForm();

    switch (ind) {
        case 1:
        case 2:
        case 3:
        case 4:
            var o = 3;

            LQ_GetTsGridCompanyByCompanyName(o, c);
            LQ_GetBoGridCompanyByCriteria(o, c, "HBC_FirmName");
            LQ_SetCompanywebInputField(myinsidelayout.cells("b"), c);

            var entreprise = qGrid.cells(id, 1).getValue();

            var tva = qGrid.cells(id, 3).getValue();
            var contact = qGrid.cells(id, 2).getValue();
            var tel = qGrid.cells(id, 4).getValue();

            if (entreprise.trim().length > 0) {
                formEditor.setItemValue("company_name", entreprise.trim());
            }
            tva = parseInt(tva);

            if (tva > 0) {
                formEditor.setItemValue("company_vat", tva);
                populateDirtyLeadEditor(company_vat, c);
            }

            if (tel.trim().length > 0) {
                formEditor.setItemValue('company_tel', tel);
            }

            if (contact.trim().length > 0) {
                formEditor.setItemValue('contact_lastname', contact);
            }

            formEditor.setItemValue('contact_type', "6");
            formEditor.setItemValue('newFiche', "FALSE");

            break;
        default:
            break;
    }
}
function ValidAlphaNumericWhiteSpace(v) {
    var output = v.match(/^[a-z0-9\s]*$/gi);
    return output;
}
function ValidBTW(inputBtw) {
    var inputString = inputBtw.toString().trim();
    var xStr = "";

    if (inputString.substring(0, 2) === "BE") {
        formEditor.setItemValue('uncheckedVAT', "FALSE");
        xStr = inputString.replace(/\D/g, '');

        if (xStr.length === 10) {
            var left = xStr.substring(0, 8);
            var right = xStr.slice(-2);

            var modlo = "";
            var diff = "";
            modlo = left % 97;
            diff = 97 - right;
            if (modlo === diff) {
                return true;
            }
        }
        return false;
    }
    var message = locale.popup.msguncheckedvat;
    dhtmlx.message({
        text: message,
        expire: -1,
        type: "errormessage"
    });
    dhtmlx.alert(message);
    formEditor.setItemValue('uncheckedVAT', "TRUE");
    return true;
}

function LQ_SetCompanywebInputField(field, fName) {
    PopulateCompanywebSearch(field, fName.replace(/<\/?[^>]+(>|$)/g, ""))
}
var QM_Tstb;
function QM_InitializeTsEditToolbar(myinsidelayout) {
    QM_Tstb = myinsidelayout.attachToolbar({
        icons_path: "/graphics/common/win_16x16/",
    });
    QM_Tstb.setAlign("left");
    QM_Tstb.addButton(1, 'left', 'Undock', 'documents.png', 'Backup-restore.png');
    QM_Tstb.addButton(2, 'left', 'Save', 'disk_e.png', 'Backup-restore.png');
    QM_Tstb.addButton(3, 'left', 'Clear', 'Certificate.png', 'Backup-restore.png');
    QM_Tstb.addButton(4, 'left', 'Delete', 'delete.png', 'Backup-restore.png');
    QM_Tstb.attachEvent("onClick", function (id) {
        switch (id) {
            case "1": // undock
                undockCell();
                break;
            case "2": // save
                if (formEditor.validate()) {
                    $.when(LQ_CreateLeadFromDirtyLead())
                    .done(function (data) {
                        LQ_ResetValuesToForm();
                    });
                }
                break;
            case "3": // clear
                LQ_ResetValuesToForm();
                break;
            case "4":
                LQ_RemoveEntry();
                break;
            default:
                break;
        }
    });
};

function GetLeadQueue(qGrid) {
    dhtmlx.message({
        text: "Getting Leads Queue",
        expire: 500,
        type: "noticemessage"
    });

    qGrid.clearAll();

    Ajax_GetLeadQueue(qGrid)
    .done(function (data) {
        qGrid.parse(data, "json");
    }).fail(function (resp) {
        dhtmlx.message({
            text: "Error getting Leads Queue",
            expire: -1,
            type: "errormessage"
        });
    });
}

function Ajax_GetLeadQueue(qGrid) {
    var url = "/classes/dal/clsLeadQueue/clsLeadQueue.asp";

    return $.get(url, { q: 1 })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Lead Queue",
            expire: -1,
            type: "errormessage"
        });
    });
}

function LeadQueueCall(calleeNumber) {
    var caller = getValuefromCookie("TELESALESTOOLV2", "extension");
    var callee = "0" + calleeNumber;
    var serviceurl = "http://odm.int-outcome.be/webcall/call.asp?callerno=@caller&calleeno=@callee#1001"
    serviceurl = serviceurl.replace("@caller", caller);
    serviceurl = serviceurl.replace("@callee", callee);
    dhtmlx.confirm({
        type: "confirm",
        text: imgconfirm1 + serviceurl,
        callback: function (result) {
            if (result === true) {
                window.open(serviceurl, 'callarea');
            };
        }
    });

    //window.open(serviceurl, 'callarea');
}

var objBackOffice;
function LQ_GetCompaniesBackOfficeInfoByVatNumberPromise(company_vat) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp?q=18&vat=" + company_vat;

    return $.get(url, { vat: company_vat })
    .done(function (data) {
        objBackOffice = data;
    }).fail(function (resp) {
        dhtmlx.message({
            text: "Error getting Backoffice by VatNumber",
            expire: -1,
            type: "errormessage"
        });
    });
}

function populateDirtyLeadEditor(company_vat, company_name) {
    $.when(LQ_GetCompaniesBackOfficeInfoByVatNumberPromise(company_vat))
    .done(function (data) {
        var company_name;
        if (objBackOffice.length > 0) {
            firmname = objBackOffice[0].HBC_FirmName;
        }
        else {
            firmname = company_name;
        };

        formEditor.setItemValue('company_name', firmname);
        formEditor.setItemValue('company_vat', company_vat);
    })
}

//TELESALES INFOS PROCESSING
function LQ_GetTsGridCompanyByCompanyName(o, c) {
    //resetResultContainers();
    //tsGrid = mysublayout01.cells("c").attachGrid();
    //LQ_ConfigGridTsCompanyByCompanyName(tsGrid);
    //tsGrid.clearAll();
    //var tsGridJsonResp = '';
    //tsGridJsonResp = GetTsCompanyByCompanyName(o, c);
    //if (tsGridJsonResp.length > 0) { tsGrid.parse(tsGridJsonResp, "json") };
}

function LQ_ConfigGridTsCompanyByCompanyName(tsGrid) {
    var tbHeader = 'Cy ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.vat + ',' + locale.main_page.lblTelephone + ',Event ID,BO ID,hbc_id'
    tsGrid.setColumnHidden(6, true);
    tsGrid.setHeader(tbHeader);
    tsGrid.setInitWidths("60,260,80, 90,60,60");
    tsGrid.setColAlign("left,left,left,left,left,left");
    tsGrid.setColTypes("ro,ro,ro,ro,ro,ed");
    tsGrid.setColSorting("str,str,str,str,str,str");
    tsGrid.init();
    tsGrid.attachEvent("onRowSelect", LQ_ts_doOnRowSelect);
}

function LQ_ts_doOnRowSelect(id, ind) {
    var company_id = tsGrid.cells(id, 0).getValue();
    LQ_ResetValuesToForm();
    LQ_GetTsCompanyByCompanyID(company_id);
    return true;
}

function LQ_ResetValuesToForm() {
    formEditor.setItemValue('company_id', '');
    formEditor.setItemValue('company_name', '');
    formEditor.setItemValue('company_email', '');
    formEditor.setItemValue('company_hbc_id', '');
    formEditor.setItemValue('company_tel', '');
    formEditor.setItemValue('company_vat', '');
    formEditor.setItemValue('company_mobile', '');
    formEditor.setItemValue('company_lang_id', '');
    formEditor.setItemValue('company_address_id', '');
    formEditor.setItemValue('company_address_street', '');
    formEditor.setItemValue('company_address_boxnumber', '');
    formEditor.setItemValue('company_address_localite', '');
    formEditor.setItemValue('company_address_number', '');
    formEditor.setItemValue('company_address_floor', '');
    formEditor.setItemValue('company_address_postcode', '');
    formEditor.setItemValue('contact_id', '');
    formEditor.setItemValue('contact_title', '');
    formEditor.setItemValue('contact_firstname', '');
    formEditor.setItemValue('contact_lastname', '');
    formEditor.setItemValue('contact_tel', '');
    formEditor.setItemValue('contact_mobile', '');
    formEditor.setItemValue('contact_email', '');
    formEditor.setItemValue('contact_type', '');
    formEditor.setItemValue('contact_lang_radical', '');
    formEditor.setItemValue('contact_gender', '');
    formEditor.setItemValue('newFiche', 'TRUE')
}

function LQ_SetValuesToForm(result) {
    formEditor.setItemValue('company_id', result[0].company_id);
    formEditor.setItemValue('company_name', result[0].company_name);
    formEditor.setItemValue('company_email', result[0].company_email);
    formEditor.setItemValue('company_hbc_id', result[0].company_hbc_id);
    formEditor.setItemValue('company_tel', result[0].company_tel);
    formEditor.setItemValue('company_vat', result[0].company_vat);
    formEditor.setItemValue('company_mobile', result[0].company_mobile);
    formEditor.setItemValue('company_lang_id', result[0].company_lang_id);
    formEditor.setItemValue('company_address_id', result[0].company_address_id);
    formEditor.setItemValue('company_address_street', result[0].company_address_street);
    formEditor.setItemValue('company_address_boxnumber', result[0].company_address_boxnumber);
    formEditor.setItemValue('company_address_localite', result[0].company_address_localite);
    formEditor.setItemValue('company_address_number', result[0].company_address_number);
    formEditor.setItemValue('company_address_floor', result[0].company_address_floor);
    formEditor.setItemValue('company_address_postcode', result[0].company_address_postcode);
    formEditor.setItemValue('contact_id', result[0].contact_id);
    formEditor.setItemValue('contact_title', result[0].contact_title);
    formEditor.setItemValue('contact_firstname', result[0].contact_firstname);
    formEditor.setItemValue('contact_lastname', result[0].contact_lastname);
    formEditor.setItemValue('contact_tel', result[0].contact_tel);
    formEditor.setItemValue('contact_mobile', result[0].contact_mobile);
    formEditor.setItemValue('contact_email', result[0].contact_email);
    formEditor.setItemValue('contact_type', result[0].contact_type);
    formEditor.setItemValue('contact_lang_radical', result[0].contact_lang_radical);
    formEditor.setItemValue('contact_gender', result[0].contact_gender);
    formEditor.setItemValue('newFiche', result[0].newFiche)
}

//click on telesales gridview to load data into docking window
function LQ_GetTsCompanyByCompanyID(company_id) {
    if (anyDigit(company_id) !== true) {
        dhtmlx.message({
            text: locale.popup.msgusedigitsonly,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(anino + locale.popup.msgusedigitsonly); return false
    };

    AjaxGetTsCompanyByCompanyIDPromise(company_id)
    .done(function (data) {
        if (data.length > 0) {
            LQ_SetValuesToForm(data);
        }
    });
}

//click on backoffice gridview to load data into BackOffice panel
function LQ_GetTsCompanyByBackOfficeID(boid) {
    var theCell = mysublayout01.cells("c");

    GetCompanyBackOfficeInfoByCompany_hbc_id(boid, theCell);
}

function SetFormDiv_TsCompanyEditor(formname) {
    f = '';
    f += '<div id="' + formname + '" style="width:100%;height:500px;"></div>';
    return f;
}

//unused as it passes data directly at design mode
function DesignForm_TsCompanyEditor(formname, targetlayout, targetCell, result) {
    var genderOpts = [{ value: "M", text: "M" }, { value: "F", text: "F" }];
    var langOpts = [{ value: "1", text: "FR" }, { value: "2", text: "NL" }, { value: "3", text: "EN" }];
    var typeOpts = [];
    GetComboContactTypesPromise()
    .done(function (data) {
        for (var j = 0; j < data.type.length; j++) {
            typeOpts.push({ value: data.type[j].value, text: data.type[j].text })
        }
    });

    var titleOpts = []
    GetComboContactTitlesPromise()
    .done(function (data) {
        for (var j = 0; j < data.title.length; j++) {
            titleOpts.push({ value: data.title[j].value, text: data.title[j].text })
        }
    });

    formStructure = [
    { type: "settings", position: "label-top" },
    {
        type: "fieldset", name: "company", offsetLeft: 10, label: locale.main_page.lblEntreprise,
        list: [
               { type: "input", label: 'uncheckedVAT', name: 'uncheckedVAT', value: result[0].uncheckedVAT },
               { type: "input", label: 'newFiche', name: 'newFiche', value: result[0].newFiche },
               { type: "input", name: 'company_id', label: 'CY ID', value: result[0].company_id, inputWidth: 75, readonly: true },
               { type: "input", name: 'company_name', label: locale.main_page.lblEntreprise, value: result[0].company_name, inputWidth: 200, required: true },
               { type: "input", name: 'company_email', label: locale.main_page.lblEmailAddress, value: result[0].company_email, inputWidth: 200 },
               { type: "newcolumn" },
               { type: "input", name: 'company_hbc_id', label: 'BO ID', value: result[0].company_hbc_id, inputWidth: 75, offsetLeft: 10 },
               { type: "input", name: 'company_tel', label: locale.main_page.lblTelephone, value: result[0].company_tel, inputWidth: 100, required: true, validate: "[0-9]+", offsetLeft: 10 },
               { type: "newcolumn" },
               { type: "input", name: 'company_vat', label: locale.main_page.lblVatCompany, value: result[0].company_vat, inputWidth: 100, required: true, validate: "ValidBTW", offsetLeft: 10 },
               { type: "input", name: 'company_mobile', label: locale.main_page.lblmobile, value: result[0].company_mobile, inputWidth: 100, offsetLeft: 10 },
               { type: "newcolumn" },
               {
                   type: "combo", label: locale.main_page.lblLang, name: "company_lang_id", offsetLeft: 10, inputWidth: 50, required: true, validate: "ValidAlphaNumeric",
                   options: [{ text: "FR", value: "1" }, { text: "NL", value: "2" }, { text: "EN", value: "3" }]
               },
        ]
    },
    {
        type: "fieldset", name: "company_address", offsetLeft: 10, label: locale.main_page.lblAdresstitle,
        list: [
                { type: "input", name: 'company_address_id', label: 'CYAddr ID', value: result[0].company_address_id, inputWidth: 75, readonly: true, hidden: true },
                { type: "input", name: 'company_address_street', label: locale.main_page.lblAdresstitle, value: result[0].company_address_street, inputWidth: 400, required: true, validate: "[A-Za-z ]+" },
                { type: "input", name: 'company_address_boxnumber', label: 'box number', value: result[0].company_address_boxnumber, inputWidth: 50 },
                { type: "input", name: 'company_address_localite', label: 'localite', value: result[0].company_address_localite, inputWidth: 400 },

                { type: "newcolumn" },
                { type: "input", name: 'company_address_number', label: 'number', value: result[0].company_address_number, inputWidth: 50, offsetLeft: 10, required: true, validate: "[0-9]+" },
                { type: "input", name: 'company_address_floor', label: 'floor', value: result[0].company_address_floor, inputWidth: 50, offsetLeft: 10 },
                { type: "input", name: 'company_address_postcode', label: 'post code', value: result[0].company_address_postcode, inputWidth: 50, offsetLeft: 10 },
        ]
    },
     {
         type: "fieldset", name: "company_contact", offsetLeft: 10, label: locale.main_page.lblEventContact,
         list: [
                 { type: "input", name: 'contact_id', label: 'contact_id', value: result[0].contact_id, inputWidth: 75, readonly: true, hidden: true },
                 { type: "select", name: "contact_title", label: locale.main_page.lblTitle, options: titleOpts, value: result[0].contact_title, required: true, validate: "[0-9]+", inputWidth: 300 },
                 { type: "input", name: 'contact_firstname', label: locale.main_page.lblFirstName, value: result[0].contact_firstname, inputWidth: 300, required: true, validate: "[A-Za-z ]+" },
                 { type: "input", name: 'contact_lastname', label: locale.main_page.lblLastName, value: result[0].contact_lastname, inputWidth: 300, required: true, validate: "[A-Za-z ]+" },
                 { type: "input", name: 'contact_tel', label: locale.main_page.lblTelephone, value: result[0].contact_tel, inputWidth: 300 },
                 { type: "input", name: 'contact_mobile', label: locale.main_page.lblmobile, value: result[0].contact_mobile, inputWidth: 300 },
                 { type: "input", name: 'contact_email', label: locale.main_page.lblEmailAddress, value: result[0].contact_email, inputWidth: 300 },

                 { type: "newcolumn" },
                 { type: "select", name: "contact_type", label: locale.main_page.lblContactType, options: typeOpts, value: result[0].contact_type, required: true, validate: "ValidAplhaNumeric", inputWidth: 100, offsetLeft: 10 },
                 { type: "select", name: "contact_lang_radical", label: locale.main_page.lblLang, options: langOpts, value: result[0].contact_lang_radical, required: true, validate: "ValidAplhaNumeric", inputWidth: 50, offsetLeft: 10 },
                 { type: "select", name: "contact_gender", label: locale.main_page.lblGender, options: genderOpts, value: result[0].contact_gender, required: true, validate: "ValidAplhaNumeric", inputWidth: 50, offsetLeft: 10 },

         ]
     },
     { type: "button", name: "btndock", value: "dock", position: "absolute", inputLeft: 10, inputTop: 730 },
     { type: "button", name: "btnundock", value: "undock", position: "absolute", inputLeft: 80, inputTop: 730 },

     { type: "button", name: "btnsave", value: locale.main_page.btnsave, position: "absolute", inputLeft: 328, inputTop: 730 },
     { type: "button", name: "btncancel", value: locale.main_page.btncancel, position: "absolute", inputLeft: 428, inputTop: 730 },
    ];

    var myForm = new dhtmlXForm(formname, formStructure);
    myForm = targetlayout.cells(targetCell).attachForm(myForm);
    myForm.enableLiveValidation(true);

    myForm.attachEvent("onButtonClick", function (buttonid) {
        switch (buttonid) {
            case "btndock":
                dockCell();
                break;
            case "btnundock":
                undockCell();
                break;
            case "btnsave":
                break;
            default:
                break;
        }
    })
}

function dockCell() {
    mysublayout01.cells("a").dock();
    //myLayout.cells(getId()).dock();
}
function undockCell() {
    mysublayout01.cells("a").undock(300, 10, 540, 810);
    //myLayout.cells(getId()).undock(550, 20, 400, 300);
}

function LQ_GetBoGridCompanyByCriteria(o, fieldvalue, fieldname) {
    //resetResultContainers();
    boGrid = mysublayout01.cells("b").attachGrid();
    LQ_ConfigGridBoCompanyByCriteria(boGrid);
    boGrid.clearAll();

    GetBackOfficeCompanyByCriteriaPromise(o, fieldvalue, fieldname)
    .done(function (data) {
        if (data.rows.length > 0) {
            boGrid.parse(data, "json")
        }
    });
}

function LQ_ConfigGridBoCompanyByCriteria(boGrid) {
    var tbHeader = 'BO ID,' + locale.main_page.lblEntreprise + ',' + locale.main_page.vat + ',' + locale.main_page.lblEventContact + ',' + locale.main_page.lblTelephone + ',' + locale.main_page.lblGsm + ',Adresse'
    //tsGrid.setColumnHidden(5, true);
    //boGrid.setColumnHidden(6, true);
    boGrid.setHeader(tbHeader);
    boGrid.setInitWidths("60,260,80, 90,60,60,60");
    boGrid.setColAlign("left,left,left,left,left,left,left");
    boGrid.setColTypes("ed,ed,ed,ed,ed,ed,ed");
    boGrid.setColSorting("str,str,str,str,str,str,str");
    boGrid.init();
    boGrid.attachEvent("onRowSelect", LQ_bo_doOnRowSelect);
}

function LQ_bo_doOnRowSelect(id, ind) {
    var curBoid = boGrid.cells(id, 0).getValue();
    var entreprise = boGrid.cells(id, 1).getValue();
    var tva = boGrid.cells(id, 2).getValue();
    var contact = boGrid.cells(id, 3).getValue();
    var tel = boGrid.cells(id, 4).getValue();
    var mobile = boGrid.cells(id, 5).getValue();

    curBoid = parseInt(curBoid);
    if (curBoid > 0) {
        formEditor.setItemValue("company_hbc_id", curBoid);
    }
    if (entreprise.trim().length > 0) {
        formEditor.setItemValue("company_name", entreprise.trim());
    }
    tva = parseInt(tva);

    if (tva > 0) {
        formEditor.setItemValue("company_vat", "BE0" + tva);
    }

    if (tel.trim().length > 0) {
        formEditor.setItemValue('company_tel', tel);
    }
    if (mobile.trim().length > 0) {
        formEditor.setItemValue('contact_mobile', mobile);
    }
    if (contact.trim().length > 0) {
        formEditor.setItemValue('contact_lastname', contact);
    }

    //GetCompanyBackOfficeInfoByCompany_hbc_id(curBoid);
    LQ_GetTsCompanyByBackOfficeID(curBoid);
    return true;
}

var formEditor;
function DesignEmptyForm_TsCompanyEditor(formname, targetlayout, targetCell) {
    var genderOpts = [{ value: "M", text: "M" }, { value: "F", text: "F" }];
    var langOpts = [{ value: "1", text: "FR" }, { value: "2", text: "NL" }, { value: "3", text: "EN" }];
    var typeOpts = [];
    var titleOpts = [];

    $.when(GetComboContactTypesPromise(), GetComboContactTitlesPromise())
        .done(function (types, titles) {
            for (var j = 0; j < types[0].type.length; j++) {
                typeOpts.push({ value: types[0].type[j].value, text: types[0].type[j].text })
            }
            for (var j = 0; j < titles[0].title.length; j++) {
                titleOpts.push({ value: titles[0].title[j].value, text: titles[0].title[j].text })
            }

            formStructure = [
                { type: "settings", position: "label-top" },
                {
                    type: "fieldset", name: "company", offsetLeft: 10, label: locale.main_page.lblEntreprise,
                    list: [
                        { type: "hidden", label: 'uncheckedVAT', name: 'uncheckedVAT', value: "FALSE" },
                        { type: "hidden", name: 'newFiche', label: 'newFiche', value: "TRUE" },
                        { type: "input", name: 'company_id', label: '<div class=nakedtitleNotEditable>CY ID</div>', value: "", inputWidth: 75, readonly: true },
                        { type: "input", name: 'company_name', label: locale.main_page.lblEntreprise, value: "", inputWidth: 200, required: true },
                        { type: "input", name: 'company_mail', label: locale.main_page.lblEmailAddress, value: "", inputWidth: 200 },
                        { type: "newcolumn" },
                        { type: "input", name: 'company_hbc_id', label: '<div class=nakedtitle style="display:inline">BO ID</div>', value: "", inputWidth: 75, offsetLeft: 10 },
                        { type: "input", name: 'company_tel', label: locale.main_page.lblTelephone, value: "", inputWidth: 100, required: true, validate: "[0-9 ]+", offsetLeft: 10 },
                        { type: "newcolumn" },
                        { type: "input", name: 'company_vat', label: locale.main_page.lblVatCompany, value: "", inputWidth: 100, required: true, validate: "ValidBTW", offsetLeft: 10 },
                        { type: "input", name: 'company_mobile', label: locale.main_page.lblmobile, value: "", inputWidth: 100, offsetLeft: 10 },
                        { type: "input", name: 'lq_id', label: 'lq_id', value: "", inputWidth: 100, offsetLeft: 10, readonly: true, hidden: true },
                        { type: "newcolumn" },
                        {
                            type: "combo", label: locale.main_page.lblLang, name: "company_lang_id", offsetLeft: 10, inputWidth: 50, required: true, validate: "ValidNumeric",
                            options: [{ text: "FR", value: "1" }, { text: "NL", value: "2" }, { text: "EN", value: "3" }]
                        }
                    ]
                },
                {
                    type: "fieldset", name: "company_address", offsetLeft: 10, label: locale.main_page.lblAdresstitle,
                    list: [
                            { type: "input", name: 'company_address_id', label: 'CYAddr ID', value: "", inputWidth: 75, readonly: true, hidden: true },
                            { type: "input", name: 'company_address_street', label: locale.main_page.lblstreet, value: "", inputWidth: 400, required: true, validate: "[A-Za-z ]+" },
                            { type: "input", name: 'company_address_boxnumber', label: locale.main_page.lblboxnumber, value: "", inputWidth: 50 },
                            { type: "input", name: 'company_address_localite', label: locale.main_page.lbllocalite, value: "", required: true, validate: "[A-Za-z ]+", inputWidth: 400 },

                            { type: "newcolumn" },
                            { type: "input", name: 'company_address_number', label: locale.main_page.lblstreetnumber, value: "", inputWidth: 50, offsetLeft: 10, required: true, validate: "ValidNumeric" },
                            { type: "input", name: 'company_address_floor', label: locale.main_page.lblfloor, value: "", inputWidth: 50, offsetLeft: 10 },
                            { type: "input", name: 'company_address_postcode', label: locale.main_page.lblpostcode, value: "", inputWidth: 50, offsetLeft: 10, required: true, validate: "ValidNumeric" },
                    ]
                },
                {
                    type: "fieldset", name: "company_contact", offsetLeft: 10, label: locale.main_page.lblEventContact,
                    list: [
                             { type: "input", name: 'contact_id', label: 'contact_id', value: "", inputWidth: 75, readonly: true, hidden: true },
                             { type: "select", name: "contact_title", label: locale.main_page.lblTitle, options: titleOpts, value: "", required: true, validate: "[0-9]+", inputWidth: 300 },
                             { type: "input", name: 'contact_firstname', label: locale.main_page.lblFirstName, value: "", inputWidth: 300, required: true, validate: "[A-Za-z ]+" },
                             { type: "input", name: 'contact_lastname', label: locale.main_page.lblLastName, value: "", inputWidth: 300, required: true, validate: "[A-Za-z ]+" },
                             { type: "input", name: 'contact_tel', label: locale.main_page.lblTelephone, value: "", inputWidth: 300 },
                             { type: "input", name: 'contact_mobile', label: locale.main_page.lblmobile, value: "", inputWidth: 300 },
                             { type: "input", name: 'contact_email', label: locale.main_page.lblEmailAddress, value: "", inputWidth: 300 },

                             { type: "newcolumn" },
                             { type: "select", name: "contact_type", label: locale.main_page.lblContactType, options: typeOpts, value: "", required: true, inputWidth: 100, offsetLeft: 10 },
                             { type: "select", name: "contact_lang_radical", label: locale.main_page.lblLang, options: langOpts, value: "", required: true, inputWidth: 50, offsetLeft: 10 },
                             { type: "select", name: "contact_gender", label: locale.main_page.lblGender, options: genderOpts, value: "", required: true, inputWidth: 50, offsetLeft: 10 },

                    ]
                },
            ];

            formEditor = new dhtmlXForm(formname, formStructure);
            formEditor = targetlayout.cells(targetCell).attachForm(formStructure);
        });
}

function setobjLead() {
    var company_stringVat = formEditor.getItemValue('company_vat').toString();

    var prefix = company_stringVat.substring(0, 2).toLowerCase();
    var re = new RegExp("^[A-Za-z]+$");
    if (re.test(prefix)) {
        company_stringVat = company_stringVat.slice(-(company_stringVat.length - 3));
    }

    var objLead = {
        company_id: formEditor.getItemValue('company_id'),
        company_name: formEditor.getItemValue('company_name'),
        company_vat: company_stringVat,

        company_tel: cleanUpCallNumber(formEditor.getItemValue('company_tel')),
        company_mobile: cleanUpCallNumber(formEditor.getItemValue('company_mobile')),
        company_mail: formEditor.getItemValue('company_mail'),
        newFiche: formEditor.getItemValue('newFiche'),
        uncheckedVAT: formEditor.getItemValue('uncheckedVAT'),

        company_hbc_id: formEditor.getItemValue('company_hbc_id'),

        company_lang_id: formEditor.getItemValue('company_lang_id'),
        company_address_type_id: 4, //default general address
        //company_address_id: formEditor.getItemValue('company_address_id'),
        company_address_street: formEditor.getItemValue('company_address_street'),
        company_address_boxnumber: formEditor.getItemValue('company_address_boxnumber'),
        company_address_localite: formEditor.getItemValue('company_address_localite'),
        company_address_number: formEditor.getItemValue('company_address_number'),
        company_address_floor: formEditor.getItemValue('company_address_floor'),
        company_address_postcode: formEditor.getItemValue('company_address_postcode'),
        //contact_id:formEditor.getItemValue('contact_id'),
        contact_title_id: formEditor.getItemValue('contact_title'),
        contact_firstname: formEditor.getItemValue('contact_firstname'),
        contact_lastname: formEditor.getItemValue('contact_lastname'),
        contact_tel: cleanUpCallNumber(formEditor.getItemValue('contact_tel')),
        contact_mobile: cleanUpCallNumber(formEditor.getItemValue('contact_mobile')),
        contact_email: formEditor.getItemValue('contact_email'),
        contact_type_id: formEditor.getItemValue('contact_type'),
        contact_lang_id: formEditor.getItemValue('contact_lang_radical'),
        contact_lang_radical: '',
        contact_gender: formEditor.getItemValue('contact_gender'),

        backoffice_id: formEditor.getItemValue('company_hbc_id'),
        user_id: getValuefromCookie("TELESALESTOOLV2", "usrId"),
        lang_id: formEditor.getItemValue('company_lang_id'), //customer lang
        source_id: 28, //dirtylead
        lead_type_id: 2, //new
        status_id: 1,
        enabled: 1,
        prompt_date: GetSalesToolDateFormat(),
        prompt_time: GetSalesToolHourMinutes(':'),

        lead_comment: 'SYSTEM:DIRTY LEAD TO LEAD PROCESSING',
        lq_id: formEditor.getItemValue('lq_id'),
        trusted: 1,
    };

    return objLead;
};

function encodeURI(objLead) {
    var params = "company_id=" + encodeURIComponent(objLead.company_id);
    params += "&company_hbc_id=" + encodeURIComponent(objLead.company_hbc_id);
    params += "&company_name=" + encodeURIComponent(objLead.company_name);
    params += "&company_vat=" + encodeURIComponent(objLead.company_vat);
    params += "&company_tel=" + encodeURIComponent(objLead.company_tel);
    params += "&company_mobile=" + encodeURIComponent(objLead.company_mobile);
    params += "&company_mail=" + encodeURIComponent(objLead.company_mail);

    params += "&company_lang_id=" + encodeURIComponent(objLead.company_lang_id);
    params += "&company_address_type_id=" + encodeURIComponent(objLead.company_address_type_id);
    params += "&company_address_street=" + encodeURIComponent(objLead.company_address_street);
    params += "&company_address_number=" + encodeURIComponent(objLead.company_address_number);
    params += "&company_address_boxnumber=" + encodeURIComponent(objLead.company_address_boxnumber);
    params += "&company_address_floor=" + encodeURIComponent(objLead.company_address_floor);
    params += "&company_address_postcode=" + encodeURIComponent(objLead.company_address_postcode);
    params += "&company_address_localite=" + encodeURIComponent(objLead.company_address_localite);

    params += "&contact_title_id=" + encodeURIComponent(objLead.contact_title_id);
    params += "&contact_firstname=" + encodeURIComponent(objLead.contact_firstname);
    params += "&contact_lastname=" + encodeURIComponent(objLead.contact_lastname);
    params += "&contact_tel=" + encodeURIComponent(objLead.contact_tel);
    params += "&contact_mobile=" + encodeURIComponent(objLead.contact_mobile);
    params += "&contact_email=" + encodeURIComponent(objLead.contact_email);
    params += "&contact_type_id=" + encodeURIComponent(objLead.contact_type_id);
    params += "&contact_lang_id=" + encodeURIComponent(objLead.contact_lang_id);
    params += "&contact_lang_radical=" + encodeURIComponent(objLead.contact_lang_radical);
    params += "&contact_gender=" + encodeURIComponent(objLead.contact_gender);

    params += "&backoffice_id=" + encodeURIComponent(objLead.backoffice_id);
    params += "&user_id=" + encodeURIComponent(objLead.user_id);
    params += "&lang_id=" + encodeURIComponent(objLead.lang_id);
    params += "&source_id=" + encodeURIComponent(objLead.source_id);
    params += "&status_id=" + encodeURIComponent(objLead.status_id);
    params += "&lead_type_id=" + encodeURIComponent(objLead.lead_type_id);
    params += "&enabled=" + encodeURIComponent(objLead.enabled);
    params += "&prompt_date=" + encodeURIComponent(objLead.prompt_date);
    params += "&prompt_time=" + encodeURIComponent(objLead.prompt_time);
    params += "&lead_comment=" + encodeURIComponent(objLead.lead_comment);
    params += "&lq_id=" + encodeURIComponent(objLead.lq_id);
    params += "&newFiche=" + encodeURIComponent(objLead.newFiche);
    params += "&uncheckedVAT=" + encodeURIComponent(objLead.uncheckedVAT);
    params += "&trusted=1";

    return params;
}

function LQ_RemoveEntry() {
    var url = "/classes/dal/clsLeadQueue/clsLeadQueue.asp?q=2";
    $.post(url, encodeURI(setobjLead()))
    .done(function (data) {
        if (data === 1) {
            dhtmlx.message({
                text: "Lead removed!",
                expire: 500,
                type: "successmessage"
            });

            GetLeadQueue(qGrid);
            LQ_ResetValuesToForm();
        } else {
            dhtmlx.message({
                text: locale.main_page.lblActionCancelled,
                expire: -1,
                type: "errormessage"
            });
            dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled);
        }
    }).fail(function (resp) {
        dhtmlx.message({
            text: locale.main_page.lblActionCancelled,
            expire: -1,
            type: "errormessage"
        });
        dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled + "\r\n" + resp);
    });
}
function LQ_CreateLeadFromDirtyLead() {
    var url = "/classes/dal/clsLeads/clsLeads.asp?q=8";
    var params = encodeURI(setobjLead()); // create transfer Object

    return $.post(url, params)
    .done(function (data) {
        switch (data) {
            //OK
            case "1":
                dhtmlx.message({
                    text: locale.main_page.lblActionCancelled,
                    expire: 500,
                    type: "successmessage"
                });
                dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblSaveSuccess)
                break;

                //duplicate
            case "50":
                dhtmlx.message({
                    text: locale.main_page.lblDirtyLeadDuplicate,
                    expire: -1,
                    type: "errormessage"
                });
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblDirtyLeadDuplicate + "\r\n");
                break;

                //fallback
            default:
                dhtmlx.message({
                    text: locale.main_page.lblActionCancelled,
                    expire: -1,
                    type: "errormessage"
                });
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled + "\r\n" + resp);
                break;
        }
    }).fail(function (resp) {
        dhtmlx.message({
            text: "Error Creating Dirty Lead",
            expire: -1,
            type: "errormessage"
        });
    });
}