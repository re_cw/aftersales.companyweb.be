﻿var myLayout;

var maintoolbar = {
    dashboard: { fiche: "9", contact: "11", history: "12", searchcw: "13", searchinternet: "14" }
}

function dontOnLoad() {
    myLayout = new dhtmlXLayoutObject({ parent: document.body, pattern: "3L" });
    myLayout.cells("a").attachObject("controls");
    myLayout.attachHeader("my_logo");
    myLayout.attachFooter("my_copy");
    document.getElementById("my_logo").style.display = "";
    document.getElementById("my_copy").style.display = "";

    var cell_a_width = 250;
    myLayout.cells("a").setWidth(cell_a_width);

    myToolbar = myLayout.attachToolbar({
        icons_path: "../graphics/common/imgs/"
    });
    myToolbar.setAlign("right");
    myToolbar.loadStruct("../classes/dal/clsMenu/clsMenu.asp?a=DYNXML_GetToolBarParentMenu");
    //add titles to layouts
    SetTranslations(myLayout);
    //attach function to toolbar click
    myToolbar.attachEvent("onClick", function (id) {
        SetSideBar(myLayout, id, cell_a_width);
        myLayout.cells("a").setText(myToolbar.getItemText(id));
    });

    // do not load this if id is not set // the switch case default value is the dashboard
    if (typeof (id) != 'undefined' && some_variable != null) {
        SetSideBar(myLayout, id, cell_a_width);
    }
}

function SetSideBar(myLayout, id, cell_a_width) {
    var mySidebar;

    mySidebar = myLayout.cells("a").attachSidebar({
        template: "tiles",
        width: cell_a_width,
        icons_path: "graphics/sidebar/",
        json: "../classes/dal/clsMenu/clsMenu.asp?a=JSON_GetSideBarMenu&id=" + id
    });
    mySidebar.attachEvent("onSelect", function (id, lastId) {
        updateLayoutB(myLayout, mySidebar, id, lastId);
    });
}

function updateLayoutB(myLayout, mySidebar, id, lastId) {
    var objSubMenu = mySidebar.cells(id).getText();
    var menutext = objSubMenu.text;
    myLayout.cells("b").setText(menutext);

    switch (id) {
        case maintoolbar.dashboard.fiche:
            //myLayout.cells("c").hideHeader();
            myLayout.cells("c").collapse();
            myForm = myLayout.cells("c").detachObject();

            myGrid = myLayout.cells("b").attachGrid();
            myGrid.setHeader("Sales,edtxt,ed,Price,In Store,Shipping,Bestseller,Date of Publication");
            myGrid.setInitWidths("50,150,100,80,80,80,80,200");
            myGrid.setColAlign("right,left,left,right,center,left,center,center");
            myGrid.setColTypes("dyn,edtxt,ed,price,ch,co,ra,ro");
            myGrid.getCombo(5).put(2, 2);
            myGrid.setColSorting("int,str,str,int,str,str,str,date");
            myGrid.init();
            myGrid.enableAlterCss("even", "uneven");
            myGrid.setImagePath("graphics/codebase/imgs/")
            myGrid.loadXML("../classes/dal/XML/grid.xml");

            myGrid.attachEvent("onMouseOver", function (id, ind) {
                //note: id= record id(the row) ind = current mouse on column
                myLayout.cells("c").setText(myGrid.cellById(id, 1).getValue());
            });

            break;

        case maintoolbar.dashboard.contact:

            //myLayout.cells("c").showHeader();
            myLayout.cells("c").expand();
            myLayout.cells("c").setHeight(200);
            myEditor = myLayout.cells("c").attachEditor({ content: "Fiche History is written here" });
            myEditor.toolbar = true;
            myEditor.iconsPath = "../codebase/imgs/dhxeditor_skyblue/";
            //myRibbon = myLayout.attachRibbon({icons_path: "../graphics/ribbon/",json: "../classes/dal/JSON/data_attached.json"});
            break;
        case maintoolbar.dashboard.searchcw:
            myLayout.cells("c").collapse();
            myLayout.cells("b").attachURL("http://www.companyweb.be/gratisbtwopzoeken_recherchertvagratuit.asp?vComp=" + varvat, true);
            break;

        case maintoolbar.dashboard.searchinternet:
            myLayout.cells("c").collapse();
            myLayout.cells("b").attachURL("https://www.google.be/", true);
        default:
    }
}

function SetTranslations(myLayout) {
    myLayout.cells("a").setText(locale.layout.header_a);
    myLayout.cells("b").setText(locale.layout.header_b);
    myLayout.cells("c").setText(locale.layout.header_c);

    $("#cwcounterindividual").text(locale.counter.individual);
    $("#cwcounterglobal").text(locale.counter.global);
}