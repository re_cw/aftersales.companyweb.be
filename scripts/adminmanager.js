﻿var insideLayout = '';
var mysublayout01 = '';
var curDateObj = '';
var qGrid = '';

var userid = "0";
var formname = 'Form_TsCompanyEditor';

function ADM_InitializeAdminEngine(myinsidelayout) {
    insidelayout = myinsidelayout;
    userid = getValuefromCookie("TELESALESTOOLV2", "usrId");
    insidelayout.cells("a").setText(locale.layout.header_a);
    insidelayout.cells("b").setText("TeleSales V02.00");

    insidelayout.cells("a").setWidth(200);

    insidelayout.cells("a").showInnerScroll();
    insidelayout.cells("b").showInnerScroll();

    insidelayout.cells("a").attachHTMLString(getMenus());
}

function getMenus() {
    var arrMenuImg = ['search.png', 'searchglobe.png'];
    var arrMenuName = [locale.main_page.lblquicksearch, locale.main_page.lblquicksearchwindow];

    var m = '';
    m += '<table class=LeadDetails cellpadding=5 cellspacing= 5>';
    m += '<tr class=onmouseoverlookup onclick=loadPanel(0);><td><img id=search class=onmouseoverlookup src=/graphics/common/win_32x32/' + arrMenuImg[0] + '></td><td class=nakedcellheader width=99%>' + arrMenuName[0] + '</td></tr>';
    m += '<tr class=onmouseoverlookup onclick=loadPanel(1);><td><img id=search class=onmouseoverlookup src=/graphics/common/win_32x32/' + arrMenuImg[1] + '></td><td class=nakedcellheader width=99%>' + arrMenuName[1] + '</td></tr>';
    m += '</table>';

    return m;
}

function loadPanel(id) {
    switch (id) {
        case 0:
            insidelayout.cells("b").attachHTMLString(getQuickSearchHtml());
            break;
        case 1:
            window.open('bofilter.asp', '_blank');
            break;
        default:
            break;
    }
}

function getQuickSearchHtml() {
    var result = '';
    result += setQueryContainer();

    return result;
}

function setQueryContainer() {
    t = '';
    t += '<table width=50% class="LeadDetails">';
    t += '<tr><td class="commontitle">field</td><td class="commontitle">value</td></tr>';
    t += '<tr><td>';
    t += '<select id="cbxFieldName" name="cbxFieldName" class="maindocmgtcontainer">';
    t += '<option value="bo_id">BO ID</option>';
    t += '<option value="event_id">Event ID</option>';
    t += '<option value="company_id">Company ID</option>';
    t += '<option value="company_vat">Vat Number</option>';
    t += '</select>';
    t += '</td><td>';
    t += '<input id="criterion" name="criterion" type="text" maxlength=50 style="width: 99%;"  onkeydown="if (event.keyCode == 13){ASP_GetTsCompanyByBackOfficeID();}" />';
    t += '</td></tr><tr><td></td><td><input id="btnBoID" type="button" value=' + locale.main_page.lblsearch + ' onclick="ASP_GetTsCompanyByBackOfficeID();"/></td></tr></table><hr /><div id="formatResult"></div>';

    return t;
}

function ASP_GetTsCompanyByBackOfficeID() {
    if (anyDigit(document.getElementById('criterion').value) != true) { return '&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + locale.popup.msgusedigitsonly + '</span>'; return false };

    var fieldname = document.getElementById('cbxFieldName').value;
    var id = document.getElementById('criterion').value;
    var result;
    switch (fieldname) {
        case 'bo_id':
            AjaxGetTsCompanyByBackOfficeIDPromise(id)
            .done(function (data) {
                $("#formatResult").empty().html(ASP_FormatResult(data));
            });

            break;
        case 'event_id':
            AjaxGetTsCompanyByEventIDPromise(id)
            .done(function (data) {
                $("#formatResult").empty().html(ASP_FormatResult(data));
            });

            break;
        case 'company_id':
            AjaxGetTsCompanyByCompanyIDPromise(id)
            .done(function (data) {
                $("#formatResult").empty().html(ASP_FormatResult(data));
            });

            break;
        case 'company_vat':
            AjaxGetTsCompanyByCompanyVatPromise(id)
			.done(function (data) {
			    $("#formatResult").empty().html(ASP_FormatResult(data));
			});

            break;
        default:
            $("#formatResult").empty().html('&nbsp;&nbsp;<span class=nakedtitleNotEditable>' + fieldname + ' [ ' + id + ' ] : ' + locale.main_page.lblnoresult + '</span>');
            break;
    }
}

function ASP_FormatResult(result) {
    var s = ''; var contactmobile = ''; var contactphone = ''; var contactmail = ''; var userFullName = ''; var vatnav = '';

    var eventID = result[0].id;
    var addrfull = result[0].company_address_street + ' ' + result[0].company_address_number + ' ' + result[0].company_address_postcode + ' ' + result[0].company_address_localite
    var phon = result[0].company_tel;
    var mob = result[0].company_mobile;
    var mail = result[0].company_mail;
    vatnav = result[0].company_vat;
    userFullName = result[0].firstname + ' ' + result[0].lastname + ' (' + result[0].extension + ')'
    if (phon.length != 0) { contactphone = '<img class=onmouseoverphone src="/graphics/common/win_16x16/Phone-icon16.png" onClick="localJS_EventCall(' + result[0].company_tel + ');"> ' + result[0].company_tel; }

    if (mob.length != 0) { contactmobile = '<img class=onmouseovermobile src="/graphics/common/win_16x16/mobile16.png" onClick="localJS_EventCall(' + result[0].company_mobile + ');"> ' + result[0].company_mobile; }

    var contactfull = contactphone + ' ' + contactmobile + ' ' + mail;
    var cwurl = '<a href="http://www.companyweb.be/page_companydetail.asp?vat=' + vatnav + '" target=_blank>' + vatnav + '</a>';

    s += '<table width=100%>'
    s += '<tr><td class="commontitle">' + locale.main_page.lblVatCompany + '</td><td class="testPageText"><b>' + cwurl + '</b></td>'
    s += '<td class="commontitle">COMPANY ID</td><td class="testPageText"><b>' + result[0].company_id + '</b></td>'
    s += '<td class="commontitle">BO ID</td><td class="testPageText"><b>' + result[0].company_hbc_id + '</b></td>'
    s += '<td class="commontitle">TT ID</td><td class="testPageText"><b>' + result[0].tt_id + '</b></td></tr>'

    s += '<tr><td class="commontitle" width=20%>' + locale.main_page.lblEntreprise + '</td><td colspan=7 class="testPageText"><b>' + result[0].company_name + ' ' + result[0].jfc_s_desc + '</b></td></tr>'
    s += '<tr><td colspan=8 class="testPageText">' + result[0].nbc_desc + '</td></tr>'
    s += '<tr><td colspan=8 class="testPageText">' + addrfull + '</td></tr>'
    s += '<tr><td colspan=8 class="testPageText">' + contactfull + '</td></tr>'
    s += '<tr><td colspan=8 ><hr></td></tr>'
    s += '</table>'

    if (result[0].company_hbc_id != '') {
        //GetCompanyBackOfficeInfoByCompany_hbc_id(result[0].company_hbc_id);
        //AjaxCompanyBackOfficeHistory(result[0].company_vat, result[0].company_hbc_id, "resBOHistory");
    };

    //if valid event id then if event user is logged user then display command to go to scheduler for updates
    if (eventID != '') {
        var urltoevent = '';
        var startdate = result[0].start_date;
        startdate = startdate.split(' ');
        startdate = startdate[0];
        var d = result[0].us_start_date;
        var euroStartDate = formatEuroDate(result[0].us_start_date);
        var euroEndDate = formatEuroDate(result[0].us_end_date);
        if (result[0].usr_id == getValuefromCookie("TELESALESTOOLV2", "usrId")) {
            urltoevent = '<img src =/graphics/common/win_16x16/Calendar.png class=onmouseoverlookup onClick=loadEvent(' + eventID + ',"' + d + '");>';
        }

        //TELESALES EVENT INFO TABLE
        s += '<table width=100%>'
        s += '<tr><td class="commontitle"  width=22%>Event ID</td><td class="schedulerEventType"><b>' + result[0].id + '</b>  ' + urltoevent + '</td></tr>'
        s += '<tr><td class="commontitle" width=22%>Event Owner</td><td class="schedulerEventType">' + userFullName + '</td></tr>'
        s += '<tr><td class="commontitle"  width=22%>' + locale.main_page.lblEventType + '</td><td class="schedulerEventType" style="color:' + result[0].event_type_textColor + '; background-color:' + result[0].event_type_color + '";><b>' + result[0].event_type_name + '</b></td></tr>'
        s += '<tr><td class="commontitle" width=22%>' + locale.main_page.lblStartDate + '</td><td class="schedulerDate">' + euroStartDate + '</td></tr>'
        s += '<tr><td class="commontitle" width=22%>' + locale.main_page.lblEndDate + '</td><td class="schedulerDate">' + euroEndDate + '</td></tr>'
        s += '<tr><td colspan=2 class="testPageText">' + result[0].text + '</td></tr>'
        s += '<tr><td colspan=8 ><hr></td></tr>'
        s += '</table>'
        s += ASP_formatCommentTable(eventID);

        var objEvent = {
            event_id: eventID,
            company_id: result[0].company_id,
            company_vat: result[0].company_vat,
            company_hbc_id: result[0].company_hbc_id,
            company_name: result[0].company_name,
            event_type_id: result[0].event_type_id,
            event_type_name: result[0].event_type_name,
            event_start_date: result[0].event_start_date,
            event_end_date: result[0].event_end_date,
            event_text: result[0].text,
            usr_id: result[0].usr_id,
            //icon: scheduler.getEvent(id).icon,
        };
    }

    return s;
}

//TELESALES EVENT COMMENT TABLE
function ASP_formatCommentTable(event_id) {
    var tc = ''; var j;
    var oc = AjaxGetTsCompanyHistoryByEventID(event_id);
    tc += '<table width=100%>';
    for (j = 0; j < oc.length; j++) {
        tc += '<tr><td valign=top class="nakedcellheader" width=115px>' + formatEuroDate(oc[j].comment_date) + ' ' + oc[j].comment_time + '</td><td class="testPageText"><b>' + oc[j].event_type_name + '</b></td></tr>'
        tc += '<tr><td valign=top colspan=2 class="testPageText">' + oc[j].comment + '</td></tr>'
    };

    tc += '</table>';
    return tc;
}