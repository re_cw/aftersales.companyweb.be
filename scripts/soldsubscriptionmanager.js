var dhxWins;
var soldSubscription;
var insidelayout;
var soldSubscriptionGrid;
var evtAccSoldSubscription;
var soldSubscriptionWintoolbarMenu = {
    menu: { leadcall: "15", leadtransfer: "16", quickedit: "29" }
};
function MM_SoldSubscriptionEngineInitialize(myinsidelayout) {
    insidelayout = myinsidelayout;

    myinsidelayout.cells("a").setText(locale.soldSubscription.soldSubscription);
    myinsidelayout.cells("a").progressOn();

    soldSubscriptionGrid = insidelayout.cells("a").attachGrid();

	GetSoldSubscriptionPromise()
	.done(function(data)
	{
		ConfigsoldSubscriptionGrid(soldSubscriptionGrid, data);
	}).always(function(){
		insidelayout.cells("a").progressOff();
	});
}
function reloadSoldSubscriptionGrid(grid, json)
{
	grid.clearAll();
	grid.parse(json, "json");
}
function ConfigsoldSubscriptionGrid(grid, json) {
    var tbHeader = locale.soldSubscription.payedOn + ",";
	tbHeader += locale.soldSubscription.einde + ",";
    tbHeader += locale.soldSubscription.vat + ",";
    tbHeader += locale.soldSubscription.name + ",";
    tbHeader += locale.soldSubscription.note;

    grid.setHeader(tbHeader);
    grid.setInitWidths("60,60,90,240,*");
    grid.setColAlign("left,left,left,left,left");
    grid.setColTypes("ro,ro,ro,ro,ro");
    grid.setColSorting("str,str,str,str,str");
    //grid.enableEditEvents(true, true, true);

    grid.init();

    grid.clearAll();
    grid.parse(json, "json");
	
	grid.attachEvent("onRowDblClicked", function(rId){
		var formName = "soldSubscription";
		dhxWins = new dhtmlXWindows();
		soldSubscription = dhxWins.createWindow(formName, 20, 20, 780, 680);
		dhxWins.attachViewportTo(document.body);
		dhxWins.window(formName).setText("detail");
		dhxWins.window(formName).center();
		dhxWins = soldSubscription.attachLayout("1C");
		evtAccSoldSubscription = dhxWins.cells("a").attachAccordion({
			dnd: true,
			icons_path: "/graphics/common/win_16x16/",
			items: [
                { id: "luCompany", text: locale.main_page.lblEntreprise, open: true },
                { id: "luBo", text: locale.main_page.winleadlayout_B, open: false },
				{ id: "luHistoriek", text: "historiek", open: false },
                { id: "luCex", text: "CEX", open:false}
			]
		});
		
		var divLuCompany = '<div id="luCompany" class="STInfo"></div>';
		var divContact = '<div id="luContact" class="STInfo"></div>';
		var divLuCyBusinessData = '<div id="luBusinessData" class="STInfo"></div>';
		var divHistory = '<div id="soldSubscriptionEventHistory" class="eventhistorymini"></div>';
	
		var divBackOffice = '<p class="commontitle">' + locale.main_page.lblEventBackOffice + '</p><div id="luBackOffice" class="STInfo"></div>';
		var divBackOfficeUserInfo = '<div id="luBackOfficeUserInfo" class="STInfo"></div>';
		divBackOfficeUserInfo += '<div id="luBackOfficeUserBillingInfo" class="STInfo"></div>';
		var divBackOfficehistory = '<div id="luBackOfficeHistory" class="eventhistorymini"></div>';
	
		var luHistory = '<div id="soldSubscriptionhistory" class="soldSubscriptionhistory"></div>';
	
		var attachLuCompanyInfos = divLuCompany + divContact + divLuCyBusinessData + divHistory;
		var attachLubackOfficeInfos = divBackOffice + divBackOfficehistory + divBackOfficeUserInfo;
	
		evtAccSoldSubscription.cells("luCompany").attachHTMLString(attachLuCompanyInfos);
		evtAccSoldSubscription.cells("luBo").attachHTMLString(attachLubackOfficeInfos);
		evtAccSoldSubscription.cells("luHistoriek").attachHTMLString(luHistory);
	
		evtAccSoldSubscription.cells("luCompany").showInnerScroll();
		evtAccSoldSubscription.cells("luBo").showInnerScroll();
		evtAccSoldSubscription.cells("luHistoriek").showInnerScroll();
		evtAccSoldSubscription.cells("luCex").showInnerScroll();
		
		var soldSubscriptionWintoolbar = soldSubscription.attachToolbar({
        icons_path: "../graphics/common/toolbars/16/",
		});
		
		var childMenuUrl = "../classes/dal/clsMenu/clsMenu.asp?a=DYNXML_GetToolBarChildMenu&id=29"
		soldSubscriptionWintoolbar.loadStruct(childMenuUrl);
		soldSubscriptionWintoolbar.attachEvent("onClick", function (id) {
        JS_executeSoldSubscriptionWintoolbarAction(id);
		});
		
		GetSoldSubscriptionDetailPromise(rId)
		.done(function(objLu)
		{
			GetSoldSubscriptionHistoryPromise(rId)
			.done(function(data){
				$('#soldSubscriptionhistory').html(FormatSoldSubscriptionHistory(data));
			});
			
			if (objLu.vat > 0)
			{
				// we have lift off!
				JSONGetCompanyFromVat(objLu.vat)
				.done(function(objCys)
				{
					var objCy = objCys[0];
					evtAccSoldSubscription.cells("luBo").progressOn();
					evtAccSoldSubscription.cells("luCompany").progressOn();
					
					$.when(
						displayLuCompanyDetailsPromise(objCy.company_id, "luCompany"),
						displayLuCompanycontactsPromise(objCy.company_id, "luContact"),
						displayStCompanyBusinessData(objCy.company_id, objCy.company_name, "luBusinessData"),
						displayEventDetailsPromise(objCy.company_id, "soldSubscriptionEventHistory")
					).done(function()
					{
						JSON_GetCompanyCompanyWebStateFromHbcId(rId, 'state');
					})
					.always(function()
					{
						evtAccSoldSubscription.cells("luCompany").progressOff();
					});
					
					
					$.when(
						displayStCompanyBackOfficeDetails(objCy.company_vat, objCy.company_hbc_id, "luBackOffice", "luBackOfficeHistory")
					).always(function()
					{
						evtAccSoldSubscription.cells("luBo").progressOff();
					});
				});				
			}
		});
			
		evtAccSoldSubscription.cells("luCex").progressOn();

		GetCexHistoryPromise(rId)
        .done(function (objHi) {
            evtAccSoldSubscription.cells("luCex").attachHTMLString(FormatCexHistory(objHi));
            GetCexChartPromise(rId)
            .done(function (data) {
				if (!$.isEmptyObject(data)){
					GetChart("cexChart", formatJSONChart(data));
				}else{
					$("#cexChart").text("NO DATA AVAILABLE");
				}
            });
			GetCexExperiencePromise(rId)
			.done(function(data)
			{
				if (!$.isEmptyObject(data)){
					GetCexAbsoluteScore("cexAbsolute", formatAbsoluteNumbers(data));
					GetExperienceScore("cexExperience", formatJSONExperience(data));
				}else{
					$("#cexAbsolute").text("NO DATA AVAILABLE");
					$("#cexExperience").text("NO DATA AVAILABLE");
				}
			});
			
        }).always(function () {
            evtAccSoldSubscription.cells("luCex").progressOff();
        });
});
}
function hideInSoldSubscriptionListPromise()
{
	var url = "/classes/dal/clsSoldSubscription/clsSoldSubscription.asp?a=POST_HideEntry";
    var objCompany = {
        company_hbc_id: $("#company_hbc_id").val()
    };
	
	var params = $.param(objCompany);
	
	return $.post(url, params)
	.done(function(data)
	{
		switch(data){
			case "1": 
				dhtmlx.message({
					text: "Entry removed from grid",
					expire: 500,
					type: "successmessage"
				});
				break;
			default:
				dhtmlx.message({
					text: "Unexpected response while removing entry from grid.",
					expire: -1,
					type: "errormessage"
				});
			break;
		}
	}).fail(function(data){
		dhtmlx.message({
			text: "Error removing entry",
			expire: -1,
			type: "errormessage"
		});
	});
}
function PromoteSoldSubscriptionToEvent()
{
	insidelayout.cells("a").progressOn();
	
	var url = "/classes/dal/clsSoldSubscription/clsSoldSubscription.asp?a=POST_PromoteToEvent";
    var objCompany = {
		company_vat: $("#company_vat").val(),
		company_id: $("#company_id").val(),
		company_name: $("#company_name").text(),
		trusted: 1
    };
	
	var params = $.param(objCompany);
	
	$.post(url, params)
	.done(function(data)
	{
		switch(data)
		{
			case "1":
				hideInSoldSubscriptionListPromise().done(function(data){
					GetSoldSubscriptionPromise()
					.done(function(data)
					{
						reloadSoldSubscriptionGrid(soldSubscriptionGrid, data);
					});
				});
				dhtmlx.message({
					text: locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
				soldSubscription.close();
				dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblSaveSuccess);
			break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblActionCancelled,
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled + "\n" + data.responseText);
				break;
		}
	}).fail(function(resp)
	{
		dhtmlx.message({
	        text: "Error converting SoldSubscription to event.",
	        expire: -1,
	        type: "errormessage"
	    });
	}).always(function()
	{
		insidelayout.cells("a").progressOff();
	});
}

function GetSoldSubscriptionDetailPromise(hbc_id)
{
	var url = "/classes/dal/clsSoldSubscription/clsSoldSubscription.asp";
	
	return $.get(url, { a: "JSON_GetSoldSubscriptionDetails", hbc_id: hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Low usage Details",
            expire: -1,
            type: "errormessage"
        });
    });
}
function GetSoldSubscriptionPromise() {
    var url = "/classes/dal/clsSoldSubscription/clsSoldSubscription.asp";

    return $.get(url, { a: "JSON_GetSoldSubscription" })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Low usage",
            expire: -1,
            type: "errormessage"
        });
    });
}
function GetSoldSubscriptionHistoryPromise(hbc_id)
{
    var url = "/classes/dal/clsSoldSubscription/clsSoldSubscription.asp";

    return $.get(url, { a: "JSON_GetSoldSubscriptionHistory", hbc_id: hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting Low usage History",
            expire: -1,
            type: "errormessage"
        });
    });
}
function FormatSoldSubscriptionDetail(objLu)
{
	var t = '';
	t += "<table cellpadding=2 width=100% border=0>";
	t += "<input type='hidden' name='event_id' id='event_id' value='" + objLu.id + "'>";
	t += '<tr><td colspan=2 class="commontitle">' + objLu.id + " - " + locale.main_page.lblEntreprise + '</td></tr>';
	t += "<tr><td colspan=2><span class=eventCompanyName id=eventTitle><span id='company_name'>" + objLu.name + "</span>&nbsp;<span id='soldSubscriptionState'></span></td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" + locale.main_page.lblNoEntreprise + "</td><td class=STInfo>" + objLu.vat + "</td></tr>";
	t += "</table>";
	t += '<div id="soldSubscriptionhistory" class="soldSubscriptionhistory"></div>';
	
	return t;
}

function FormatSoldSubscriptionHistory(objHi)
{
    var t = '<table  width=100% border=0>';
    t += '<tr><td class="commontitle">' + locale.main_page.lblBOHistory + '</td></tr>';
    t += '<tr><td><textarea style="width:100%; height: 500px;">' + objHi.note + '</textarea></td></tr>';
    t += '</table>'

    return t;
}
function displayLuCompanyDetailsPromise(company_id, container) {
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning',true);
		
		var url = "/classes/dal/clsCompany/clsCompany.asp";
		return $.get(url, { a: "JSONGetCompanyInfo", company_id: company_id })
		.done(function (data) {
			$("#" + container).empty().html(formatLuCompanyDetails(data))
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Details",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
	}
	return $.when(null);
}
function formatLuCompanyDetails(objCy) {
    var postcodenr = "'" + objCy.company_address_postcode + "'";
    var postcode = '<img class=onmouseoverlookup src="/graphics/common/win_32x32/bpost32.png" onClick="displayCompaniesbyPostalCode(' + postcodenr + ');">';
    var phonenr = "'" + objCy.company_tel + "'";
    var mobilenr = "'" + objCy.company_mobile + "'";
    var company_id = "'" + objCy.company_id + "'";

    var company_vat = "'" + objCy.company_vat + "'";
    var cwnfo = '<img id="cwnfo" class=onmouseovercwnfo src="/graphics/common/win_32x32/cw32.png" onclick="cwnavigate(' + company_vat + ');">';
    var company_name = objCy.company_name;
    company_name = company_name.replace(/ /g, '+');
    var webnfo = '<img id=cwnfo class=onmouseoverlookup src=/graphics/common/win_32x32/searchglobe.png onclick=webnavigate("' + company_name + '");>';

    var stToolBox = webnfo + '&nbsp;&nbsp;' + cwnfo + '&nbsp;&nbsp;' + postcode + '&nbsp;&nbsp;';

    var t = ''; //'<div id=objCytoolBox class=objCytoolBox>' + companyedit + '&nbsp;&nbsp;' + webnfo + '&nbsp;&nbsp;' + cwnfo + '&nbsp;&nbsp;' + postcode + '&nbsp;&nbsp;' + mobileurl + '&nbsp;&nbsp;' + phoneurl + '&nbsp;&nbsp;' + abo + '&nbsp;&nbsp;' + contact + '</div>';
    t += "<input type='hidden' name='company_id' id='company_id' value='" + objCy.company_id + "' />";
    t += "<input type='hidden' name='company_vat' id='company_vat' value='" + objCy.company_vat + "' />";
	t += "<input type='hidden' name='company_hbc_id' id='company_hbc_id' value='" + objCy.company_hbc_id + "' />";
    t += "<table cellpadding=2 width=100% border=0>";
    t += '<tr><td colspan=2 align=right>' + stToolBox + '</td></tr>';
    t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEntreprise + '</td></tr>';
    t += "<tr><td colspan=2><span class=eventCompanyName id=eventTitle><span id='company_name'>" + objCy.company_name + "</span> " + objCy.jfc_s_desc + "(" + objCy.lang_name + ")<span style='float:right'><input type='button' value='...' onClick='UpdateWithCompanyInfo(" + objCy.company_vat + ")' /></span></td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" +locale.main_page.lblCompanyStatus + "</td><td class=STInfo><span id='state'></span></td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo width=105px>" + locale.main_page.lblNoEntreprise + "</td><td class=STInfo>" + objCy.company_vat + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblAdress + "</td><td class=STInfo id='company_address'>" + objCy.company_address_street + ", " + objCy.company_address_number + " " + objCy.company_address_boxnumber + " - " + objCy.company_address_postcode + " " + objCy.company_address_localite + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblActivity + "</td><td class=STInfo>" + objCy.nbc_desc + "</td></tr>"
    t += "<tr><td colspan=2>"
    t += "<table width=100%><tr class=CwCyInfo><td class=CwCylblInfo width=16%>" + locale.main_page.lblTel + "</td><td class=STInfo width=16%>" + objCy.company_tel + "</td>";
    t += "<td class=CwCylblInfo width=16%>" + locale.main_page.lblGsm + "</td><td class=STInfo width=16%>" + objCy.company_mobile + "</td>";
    t += "<td class=CwCylblInfo width=16%>" + locale.main_page.lblEmail + "</td><td class=STInfo width=16%>" + objCy.company_mail + "</td></tr></table>";
    t += "</td></tr>";
    t += "</table>";
    return t;
}

function displayLuCompanycontactsPromise(company_id, container) {
	if (!$("#" + container).data('requestRunning')){
		$("#" + container).data('requestRunning', true);
		var url = "/classes/dal/clsContact/clsContact.asp";
		$("#" + container).empty();
		
		return $.get(url, { a: "JSONGetStContactDetails", company_id: company_id })
		.done(function (data) {
			$("#" + container).html(formatLuContactDetails(data));
		}).fail(function (data) {
			dhtmlx.message({
				text: "Failure getting Company Contacts",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
	}
	return $.when(null);
}
function formatLuContactDetails(obj) {
    var spacer, remove, contact, phoneurl, phonenr, mobileurl, mobilenr, purchaseorder, abo, bestelbon

    var t = "<table cellpadding=1 width=100% border=0>";
    t += '<tr><td colspan=2 class="commontitle">' + locale.main_page.lblEventContact + '</td></tr>';
    for (var i = 0; i < obj.length; i++) {
        spacer = '&nbsp;&nbsp;'
        remove = '<img class =onmouseoverdel src="/graphics/common/win_16x16/male-user-remove.png" onClick="DeleteContact(' + obj[i].contact_id + ');">';
        contact = '<img class =onmouseoveredit src="/graphics/common/win_16x16/male-user-edit.png" onClick="EditContact(' + obj[i].contact_id + ', ' + obj[i].company_id + ', ' + obj[i].company_address_id +');">';
        phonenr = "'" + obj[i].contact_tel + "'";
        mobilenr = "'" + obj[i].contact_mobile + "'";

        t += "<tr id='contact_id" + obj[i].contact_id + "'><td width=85% class=eventContactHeader>ContactId:" + obj[i].contact_id + ' ' + obj[i].contact_title + " " + obj[i].contact_firstname + " " + obj[i].contact_lastname + ", " + obj[i].contact_type + " (" + obj[i].lang_name + ")</td>";
        t += "<td align=right></td></tr>"
        t += "<tr><td><table width=100% border=0>"
        t += "<tr class=CwCyInfo><td class=CwCylblInfo width=50px>" + locale.main_page.lblTel + "</td><td class=STInfo width=100px>" + obj[i].contact_tel + "</td>";
        t += "<td class=CwCylblInfo width= width=50px>" + locale.main_page.lblGsm + "</td><td class=STInfo width=100px>" + obj[i].contact_mobile + "</td>";
        t += "<td class=CwCylblInfo width= width=50px>"+ locale.main_page.lblEmail + "</td><td class=STInfo width=200px>" + obj[i].contact_email + "</td></tr>";
        t += "</table>"
    }
    t += "</table>";

    return t;
}

function JS_executeSoldSubscriptionWintoolbarAction(id)
{
	switch (id)
	{
		case soldSubscriptionWintoolbarMenu.menu.quickedit:
			PromoteSoldSubscriptionToEvent();
		break;
		
	}
}