﻿var locale = {
    date: {
        month_full: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        month_short: ["Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Aoû", "Sep", "Oct", "Nov", "Déc"],
        day_full: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        day_short: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]
    },
    ribbon: {
        file: {
            blockname: "File",
            open: ["Open", "File...", "Recent"],
            save: "Save",
            close: "Close"
        }
    },
    layout: {
        header_a: "Navigation",
        header_b: "Container",
        header_c: "Editor"
    },
    counter: {
        individual: "Individual Result ",
        global: "Global Result "
    },
    labels : {
        welcomeMessage: "Hello Logon:",
        dhx_cal_today_button: "Aujourd'hui",
        day_tab: "Jour",
        week_tab: "Semaine",
        month_tab: "Mois",
        new_event: "Nouvel événement",
        icon_save: "Enregistrer",
        icon_cancel: "Annuler",
        icon_details: "Détails",
        icon_edit: "Modifier",
        icon_delete: "Effacer",
        confirm_closing: "",
        confirm_deleting: "L'événement sera effacé sans appel, êtes-vous sûr ?",
        section_description: "Description",
        section_time: "Période",
        full_day: "Journée complète",
        confirm_recurring: "Voulez-vous éditer toute une série d'évènements répétés?",
        section_recurring: "Intervals",
        button_recurring: "Deactivated",
        button_recurring_open: "Activated",
        button_edit_series: "Edit range",
        button_edit_occurrence: "Edit copy",
        agenda_tab: "Day",
        date: "Date",
        description: "Description",
        year_tab: "Year",
        week_agenda_tab: "Day",
        grid_tab: "Grid",
        drag_to_create: "Drag to create",
        drag_to_move: "Drag to move",
        message_ok: "OK",
        message_cancel: "Cancel"
    }
};