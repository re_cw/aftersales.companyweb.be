<%@  codepage="65001" %>
<%
	'The CodePage directive above needs to be present in order for the Server.HTMLEncode function
	'to convert French special characters correctly
%>
<%
	Response.CodePage = 65001	'UTF-8
%>
<%
	'Prevent IE from caching the response of this paga
	Response.CacheControl = "no-cache"
	Response.AddHeader "Pragma", "no-cache"
	Response.Expires = -1
%>

<%
Private Function IsNotNullorEmpty(input)
	dim IsNotNullorEmpty_Output
	IsNotNullorEmpty_Output = true
	'vbEmpty (uninitialized) or vbNull (no valid data)
	if VarType(input) = 0 OR VarType(input) = 1 Then
		IsNotNullorEmpty_Output = false
	End if
	If VarType(input) = 8 Then
		If input = "" Then
			IsNotNullorEmpty_Output = False
		End If
	End If
	IsNotNullorEmpty = IsNotNullorEmpty_Output
End Function

Private Function HTMLEncode(ByVal Text, Byval KeepHTMLFormatting)
	Dim strOutput
	Dim lngCodePage

	if (False = IsNotNullorEmpty(Text)) Then
		HTMLEncode = Text
		Exit Function
	End if

	'Response.CodePage = 65001	'UTF-8

	strOutput = Server.HTMLEncode(Text)
	If KeepHTMLFormatting Then
		strOutput = Replace(strOutput, "&lt;b&gt;","<b>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/b&gt;","</b>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;i&gt;","<i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/i&gt;","</i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;u&gt;","<i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/u&gt;","</i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;strong&gt;","<strong>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/strong&gt;","</strong>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;em&gt;","<em>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/em&gt;","</em>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;br&gt;","<br>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;br /&gt;","<br />", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;br/&gt;","<br />", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;ul&gt;","<ul>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/ul&gt;","</ul>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;li&gt;","<li>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/li&gt;","</li>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;p&gt;","<p>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/p&gt;","</p>", 1, -1, 1)
		strOutput = Replace(strOutput, "&amp;nbsp;","&nbsp;", 1, -1, 1)
		strOutput = Replace(strOutput, "'", "&lsquo;")
	End If
	'	'Change CodePage back to what it was
'	Response.CodePage = lngCodePage

	HTMLEncode = strOutput

End Function

	dim vConnStr
	vConnStr = "Provider=sqloledb;Data Source=10.81.15.3,1433;Network Library=DBMSSOCN;Initial Catalog=odmSearch_sub;User ID=odmTst;Password=CMt3J8gy;"

    function ShowDate(pdate,itLang)
        ShowDate = ""
        if itLang = 1 then
            if len(pdate) = 8 then
            ShowDate = "Inactif depuis " & mid(pdate,1,4) &"-"& mid(pdate,5,2) &"-"&  mid(pdate,7,2)
            else
            ShowDate = "Inactif"
            end if
        else
            if len(pdate) = 8 then
            ShowDate = "Inactief sinds " & mid(pdate,1,4) &"-"& mid(pdate,5,2) &"-"&  mid(pdate,7,2)
            else
            ShowDate = "Inactief"
            end if
        end if
    end function

    intLang = 0
    if Request.Cookies("intLang")<> "" then intLang = clng(Request.Cookies("intLang"))

    dim msgNoResult(2)
    msgNoResult(1) = "No resultat"
    msgNoResult(0) = "Geen resultaat"
    dim msgEntries(2)
    msgEntries(1) = ""
    msgEntries(0) = " gevonden "
    dim msgLimit(2)
    msgLimit(1) = " trouve - limite "
    msgLimit(0) = "  gevonden - limiet "

    dim lblVAT(2)
    lblVAT(1) = "Tva"
    lblVAT(0) = "Btw"

    dim lblCompany(2)
    lblCompany(1) = "Firme"
    lblCompany(0) = "Firma"

    dim lblType(2)
    lblType(1) = "Forme"
    lblType(0) = "Vorm"

    dim lblAdres(2)
    lblAdres(1) = "Adresse"
    lblAdres(0) = "Adres"

    dim lblCity(2)
    lblCity(1) = "Lieu"
    lblCity(0) = "Locatie"

    dim lblMoreInfo(2)
    lblMoreInfo(0) = "Meer info"
    lblMoreInfo(1) = "Plus d'info"

    dim seqid
    dim conn
    dim RECcount

    dim ObjRS
    dim strSQL
    dim d
    dim sr_VAT

    Dim SR_EindDatum
    Dim cscore
    Dim retval
    Dim sr_name
    Dim SR_CurrentJurVorm
    Dim SR_straatnaam
    Dim City
    dim resultstr
    dim linkin
    dim strcolor
    dim rowstr
    dim PSID
    dim check

    seqid = Request.QueryString("seqid")

    resultstr = ""
    RECcount = 0

    PSID = request.QueryString("PSID")
    check = request.querystring("check")

    if seqid <> ""  then
            Set conn = server.CreateObject ("adodb.connection")
            conn.Open(vConnStr)

            Set d=Server.CreateObject("Scripting.Dictionary")

            if seqid < 0 then
               seqid  = abs(seqid)
               strSQL = "select isnull(max(seqnr),0) from odmsearch.dbo.SearchQueue where Login = '" & myLogin & "' and seqnr >= "  & seqid
               retval  = conn.execute(strSQL)
		       if retval(0) <> "0" then seqid = cstr(retval(0))
            end if

		    Set ObjRS = server.CreateObject ("adodb.recordset")
            'SR_rowid    SR_seqnr    SR_timestamp            SR_vat      SR_name
            ' SR_nameOrigin SR_PostCodeOrigin SR_Score

            strSQL = " select SR_seqnr,sr_NameOrigin,SR_PostCodeOrigin,SR_vat,SR_name,SR_postcode,SR_Score"
            strSQL = strSQL & " ,fu_EindDatum,fu_vorm,fu_NameOrigin,fu_AdresOrigin,fu_Name,fu_StraatNaam"
            strSQL = strSQL & " ,fu_HuisNr,fu_BusNr,fu_PostCode,fu_Gemeente, f_PrefLangID "
            strSQL = strSQL & " From  odmsearch_sub.dbo.SearchCompanyResult"
            if intLang = 1 then
            strSQL = strSQL & " inner join odmdbase_sub2.dbo.Cw_Firma_UNIQUE_FR on sr_VAT = fu_vat "
            else
            strSQL = strSQL & " inner join odmdbase_sub2.dbo.Cw_Firma_UNIQUE_NL on sr_VAT = fu_vat "
            end if
			strSQL = strSQL & " inner join odmdbase_sub2.dbo.Cw_Firma on fu_vat = f_vat "
            strSQL = strSQL & " Where sr_seqnr = " & seqid
            strSQL = strSQL & " order by SR_Score desc,SR_PostCodeOrigin,Sr_name "
            'response.write strSQL
            ObjRS.Open strSQL,conn,0,1
            do while not ObjRS.eof
		        if not d.Exists("v" & ObjRS("SR_vat")) then
			        call d.Add("v" & ObjRS("SR_vat"),"")
			        sr_VAT = "BE0" & ObjRS("SR_vat") & ""

			        SR_EindDatum = ObjRS("fu_EindDatum")
			        cscore = ObjRS("sr_score")
			        sr_name = ObjRS("fu_name") & ""
		            if ObjRS("SR_name") & "" <>  ObjRS("fu_name") & "" and ObjRS("SR_name") & ""  <> "" then
		                sr_name = ObjRS("SR_name") & " (" & ObjRS("fu_name") & ")"
		            end if
			        SR_CurrentJurVorm = ObjRS("fu_vorm") & ""
			        SR_straatnaam = ObjRS("fu_StraatNaam") & ""
	    	        if ObjRS("fu_HuisNr") & "" <> "" then SR_straatnaam = SR_straatnaam & " " & ObjRS("fu_HuisNr")
	    	        if ObjRS("fu_BusNr") & "" <> "" then SR_straatnaam = SR_straatnaam & " " & ObjRS("fu_BusNr")
	    	        City  =  ObjRS("fu_PostCode") & " " & ObjRS("fu_Gemeente")
                    if ObjRS("SR_PostCodeOrigin") = 1 and ObjRS("fu_PostCode") <> ObjRS("sr_postcode") then
                            if intLang = 1 then
                            City = City & " (aussi " & ObjRS("sr_postcode") & " - établissement)"
                            else
                            City = City & " (ook " & ObjRS("sr_postcode") & " via vestiging)"
                            end if
                    end if
	    	        if len(sr_name) > 50 then sr_name = left(sr_Name & "",50)  & " ..."
			        'linkIn = "btwzoeklijst.asp?ret=3&check=" & check& "&PSID=" & PSID & "&VAT=" & sr_VAT
					linkIn = "#"
'fu_EindDatum,fu_NameOrigin,fu_AdresOrigin,fu_Name,fu_StraatNaam,fu_HuisNr,fu_BusNr,fu_PostCode,fu_Gemeente
			        if RECcount mod 2 = 0 then
				        strcolor = "#F0F8FF"
			        else
				        strcolor = "#FFFFFF"
			        end if

			        rowstr = ""
			        rowstr = rowstr & "<tr>" & vbcrlf
			        if SR_EindDatum <> 0 then
			            rowstr = rowstr & "<td bgcolor=""" & strcolor & """><img border=""0"" src=""/assets/images/img_Failed.gif"" class=""vtip"" title=""" & HTMLEncode(ShowDate(SR_EindDatum,intLang), True) & """ alt=""failed"" /></td>" & vbcrlf
			        else
				        rowstr = rowstr & "<td bgcolor=""" & strcolor & """><img border=""0"" src=""/assets/images/pixel.gif"" width=""12"" height=""12"" alt=""trsp_pix"" /></td>" & vbcrlf
			        end if
			        rowstr = rowstr & "<td bgcolor=""" & strcolor & """ onclick=""parent.formEditor.setItemValue('company_vat', '" & HTMLEncode(sr_vat, True) & "');parent.formEditor.setItemValue('company_name','" & HTMLEncode(sr_name, True) & "');parent.formEditor.setItemValue('company_address_postcode','" & HTMLEncode(ObjRS("fu_PostCode"),True) & "');parent.formEditor.setItemValue('company_address_localite', '" & HTMLEncode(ObjRS("fu_Gemeente"),True) & "');parent.formEditor.setItemValue('company_address_street', '" & HTMLEncode(ObjRS("fu_StraatNaam"), True) & "');parent.formEditor.setItemValue('contact_lang_radical', '" & ObjRS("f_PrefLangID") &"');parent.formEditor.setItemValue('company_lang_id', '" & ObjRS("f_PrefLangID") &"');parent.formEditor.setItemValue('company_address_number', '" & ObjRS("fu_HuisNr") & "');""><u>" & sr_VAT & "</u></td>" & vbcrlf
			        rowstr = rowstr & "<td bgcolor=""" & strcolor & """ class='leftalign'>" & HTMLEncode(sr_name, True) & "</td>" & vbcrlf
			        rowstr = rowstr & "<td bgcolor=""" & strcolor & """ class='leftalign'>" & HTMLEncode(SR_CurrentJurVorm, True) & "</td>" & vbcrlf
			        rowstr = rowstr & "<td bgcolor=""" & strcolor & """ class='leftalign'>" & HTMLEncode(SR_straatnaam, True) & "</td>" & vbcrlf
			        rowstr = rowstr & "<td bgcolor=""" & strcolor & """ class='leftalign'>" & HTMLEncode(City, True) & "</td>" & vbcrlf
                    rowstr = rowstr & "<td bgcolor=""" & strcolor & """ class='leftalign'><a href='http://www.companyweb.be/Page_CompanyDetail.asp?vat=" & sr_VAT & "' class='link smaller' target='_blank'><u>" &  HTMLEncode(lblMoreInfo(intLang), True) & "</u></a>"
			        rowstr = rowstr & "</tr>" & vbcrlf

			        resultstr = resultstr & rowstr
			        RECcount = d.count
		            if RECcount => 100 then exit do
                end if
			        ObjRS.MoveNext
		        loop
        set d = nothing
		ObjRS.close
		set ObjRS = nothing

end if
conn.Close
set conn = nothing

%>
<div class="tablewrapper divscroll">
    <table border="0" class="tablesorter smaller">
        <thead>
            <tr>
                <th colspan="2"><%= HTMLEncode(lblVAT(intLang), True) %></th>
                <%
		            If RECcount = 0 Then
                        response.Write("<th class=""leftalign"">" & HTMLEncode(lblCompany(intLang), True) & " (" & HTMLEncode(msgNoResult(intLang), True) & ")</th>")
	                Else
                        Session("LastSearchID") = seqid
	                    response.Write("<th class=""leftalign"">" & HTMLEncode(lblCompany(intLang), True) & " (" & RECcount & " " & HTMLEncode(msgEntries(intLang), True) & ")</th>")
 		            End If
                %>
                <th class="leftalign"><%= HTMLEncode(lblType(intLang), True) %></th>
                <th class="leftalign"><%= HTMLEncode(lblAdres(intLang), True) %></th>
                <th class="leftalign"><%= HTMLEncode(lblCity(intLang), True) %></th>
                <th>
                    <img src="img/blank.gif" width="60" height="1" /></th>
            </tr>
        </thead>
        <tbody>
            <%=resultstr%>
        </tbody>
    </table>
</div>
<div class="tablefooter"></div>
<!-- / .tablefooter -->