<%option Explicit
    Response.Buffer = True
    Response.CacheControl = "no-cache"
    Response.AddHeader "Pragma", "no-cache"
    Response.Expires = -1

Dim objUtility

class sysUtilities

    public sub Class_Initialize()
        set objUtility = new sysUtilities
    end sub

    public function castnumbersOnly(s)
        Dim re, oRegExp
        re = "[^0-9]"
        Set oRegExp = New RegExp
            oRegExp.Global = True
            oRegExp.Pattern = re
            s = oRegExp.Replace(s, "")
        castnumbersOnly = s 
    end function

    public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
        if mm < 10 then mm = "0" & mm
        dd = day(now())
        if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    public function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function

    '<end region PRIVATE METHODS & FUNCTIONS>
    public sub Class_Terminate()
        set objUtility = nothing
    end sub

end class
%>
