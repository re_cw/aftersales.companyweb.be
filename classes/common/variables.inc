﻿<%
Response.CodePage = 65001    
Response.CharSet = "utf-8"

    CONST C_SYS_APP_NAME		="TeleSales Tool V02.00.00"
    CONST C_SYS_COOKIE_NAME		= "TELESALESTOOLV2"
	CONST C_SYS_DEFAULT_LANG	= "FR"
	
    CONST C_APP_NAME_LOGO = "<span class=""appTele"">TELE</span><span class=""appSales"">SALES</span><span class=""appTool"">TOOL</span><br><span class=""appVersion"">V02.00.00</span>"
    CONST C_APP_COMPANY_LOGO = "<img alt=""CompanyWebLogo"" src=""graphics/logo-nl-500.png"" style=""width:150px;height:auto;"" />"
   
    Dim C_SYS_APP_HEADER
    C_SYS_APP_HEADER= "<table class=""headerMainTable"" border=1><tr><td align=""left"">" & C_APP_COMPANY_LOGO & "</td><td align=""right"">" & C_APP_NAME_LOGO & "</td></tr></table>"   

    Dim C_SYS_APP_FOOTER
	C_SYS_APP_FOOTER		= "<span class=""appFooter"">Copyright &copy 2015 - " & year(date()) & " Companyweb :: Telesales Tool v02.00.00 :: Kantorenpark Everest :: Leuvensesteenweg 248D, 1800 Vilvoorde  :: T: +32 2 752 17 60 :: <a href=""mailto:info@companyweb.be"">info@companyweb.be</a></span>"
        'C_SYS_APP_FOOTER_UNUSED		= "<div id=""bottomFooter"" class=""bottomFooter""><span class=""appFooter"">Copyright &copy 2015 - " & year(date()) & " Companyweb :: Telesales Tool v02.00.00 :: Kantorenpark Everest :: Leuvensesteenweg 248D, 1800 Vilvoorde  :: T: +32 2 752 17 60 :: <a href=""mailto:info@companyweb.be"">info@companyweb.be</a></span></div>"
		
 'mimic enumeration constants for navigation
    CONST sys_menu_id_dashboard=1
    CONST sys_menu_id_e_new=2
    CONST sys_menu_id_search=3
    CONST sys_menu_id_schedule=4
    CONST sys_menu_id_trials=5
    CONST sys_menu_id_admin=6
    CONST sys_menu_id_samples=7
    CONST sys_menu_id_logoff=8    
  
 %>
