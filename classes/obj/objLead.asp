﻿<%
        
class clsObjLead

public sub Class_initialize()
    
end sub

'object integrity
Dim m_trusted, m_LQ_id

'object data    
Dim m_lead_id, m_user_id, m_status_id, m_lang_id, m_source_id, m_lead_type_id, m_backoffice_id, m_company_id, m_company_address_id
Dim m_prompt_date, m_prompt_time, m_company_name, m_company_vat, m_company_address_street, m_company_address_number, m_company_address_postcode
Dim m_company_address_localite, m_company_tel, m_company_mobile, m_company_mail, m_HBCompanyNotes, m_company_address_boxnumber
Dim m_company_address_floor

Dim m_company_hbc_id, m_company_lang_id, m_company_address_type_id
Dim m_contact_id, m_contact_title, m_contact_firstname, m_contact_lastname, m_contact_tel, m_contact_mobile, m_contact_email
Dim m_contact_type, m_contact_lang_radical, m_contact_gender, m_contact_lang_id, m_contact_title_id, m_contact_type_id
Dim m_enabled, m_lead_comment 

Dim m_newFiche, m_uncheckedVAT
'object additonal parameters
Dim m_new_user_id
	property get newFiche() : newFiche = m_newFiche : end property
	property let newFiche(varval) : m_newFiche = varval : end property

	property get uncheckedVAT() : uncheckedVAT = m_uncheckedVAT : end property
	property let uncheckedVAT(varval) : m_uncheckedVAT = varval : end property
	
    property get lq_id() : LQ_id = m_lq_id : end property 
    property let lq_id(varval) : m_lq_id = varval : end property

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property


    property get lead_id() : lead_id = m_lead_id : end property 
    property let lead_id(varval) : m_lead_id = varval : end property
    
    property get user_id() : user_id = m_user_id : end property 
    property let user_id(varval) : m_user_id = varval : end property
    
    property get status_id() : status_id = m_status_id : end property 
    property let status_id(varval) : m_status_id = varval : end property
    
    property get lang_id() : lang_id = m_lang_id : end property 
    property let lang_id(varval) : m_lang_id = varval : end property
    
    property get source_id() : source_id = m_source_id : end property 
    property let source_id(varval) : m_source_id = varval : end property
    
    property get lead_type_id() : lead_type_id = m_lead_type_id : end property 
    property let lead_type_id(varval) : m_lead_type_id = varval : end property
    
    property get backoffice_id() : backoffice_id = m_backoffice_id : end property 
    property let backoffice_id(varval) : m_backoffice_id = varval : end property
    
    property get company_id() : company_id = m_company_id : end property 
    property let company_id(varval) : m_company_id = varval : end property
    
    property get prompt_date() : prompt_date = m_prompt_date : end property 
    property let prompt_date(varval) : m_prompt_date = varval : end property 

    property get prompt_time() : prompt_time = m_prompt_time : end property 
    property let prompt_time(varval) : m_prompt_time = varval : end property

    property get company_name() : company_name = m_company_name : end property 
    property let company_name(varval) : m_company_name = varval : end property

    property get company_vat() : company_vat = m_company_vat : end property 
    property let company_vat(varval) : m_company_vat = varval : end property

    property get company_address_id() : company_address_id = m_company_address_id : end property 
    property let company_address_id(varval) : m_company_address_id = varval : end property

    property get company_address_street() : company_address_street = m_company_address_street : end property 
    property let company_address_street(varval) : m_company_address_street = varval : end property

    property get company_address_number() : company_address_number = m_company_address_number : end property 
    property let company_address_number(varval) : m_company_address_number = varval : end property

    property get company_address_boxnumber() : company_address_boxnumber = m_company_address_boxnumber : end property 
    property let company_address_boxnumber(varval) : m_company_address_boxnumber = varval : end property
    
    property get company_address_floor() : company_address_floor = m_company_address_floor : end property 
    property let company_address_floor(varval) : m_company_address_floor = varval : end property
    

    property get company_address_postcode() : company_address_postcode = m_company_address_postcode : end property 
    property let company_address_postcode(varval) : m_company_address_postcode = varval : end property

    property get company_address_localite() : company_address_localite = m_company_address_localite : end property 
    property let company_address_localite(varval) : m_company_address_localite = varval : end property

    property get company_tel() : company_tel = m_company_tel : end property 
    property let company_tel(varval) : m_company_tel = varval : end property

    property get company_mobile() : company_mobile = m_company_mobile : end property 
    property let company_mobile(varval) : m_company_mobile = varval : end property

    property get company_mail() : company_mail = m_company_mail : end property 
    property let company_mail(varval) : m_company_mail = varval : end property

    property get HBCompanyNotes() : HBCompanyNotes = m_HBCompanyNotes : end property 
    property let HBCompanyNotes(varval) : m_HBCompanyNotes = varval : end property
 
    property get new_user_id() : new_user_id = m_new_user_id : end property 
    property let new_user_id(varval) : m_new_user_id = varval : end property

    '''''
    property get company_hbc_id() : company_hbc_id = m_company_hbc_id : end property 
    property let company_hbc_id(varval) : m_company_hbc_id = varval : end property

    property get company_lang_id() : company_lang_id = m_company_lang_id : end property 
    property let company_lang_id(varval) : m_company_lang_id = varval : end property

    property get company_address_type_id() : company_address_type_id = m_company_address_type_id : end property 
    property let company_address_type_id(varval) : m_company_address_type_id = varval : end property

    property get contact_id() : contact_id = m_contact_id : end property 
    property let contact_id(varval) : m_contact_id = varval : end property

    property get contact_title_id() : contact_title_id = m_contact_title_id : end property 
    property let contact_title_id(varval) : m_contact_title_id = varval : end property

    property get contact_title() : contact_title = m_contact_title : end property 
    property let contact_title(varval) : m_contact_title = varval : end property

    property get contact_firstname() : contact_firstname = m_contact_firstname : end property 
    property let contact_firstname(varval) : m_contact_firstname = varval : end property

    property get contact_lastname() : contact_lastname = m_contact_lastname : end property 
    property let contact_lastname(varval) : m_contact_lastname = varval : end property

    property get contact_tel() : contact_tel = m_contact_tel : end property 
    property let contact_tel(varval) : m_contact_tel = varval : end property

    property get contact_mobile() : contact_mobile = m_contact_mobile : end property 
    property let contact_mobile(varval) : m_contact_mobile = varval : end property

    property get contact_email() : contact_email = m_contact_email : end property 
    property let contact_email(varval) : m_contact_mobile = varval : end property

    property get contact_type_id() : contact_type_id = m_contact_type_id : end property 
    property let contact_type_id(varval) : m_contact_type_id = varval : end property

    property get contact_type() : contact_type = m_contact_type : end property 
    property let contact_type(varval) : m_contact_type = varval : end property
 
    property get contact_lang_id() : contact_lang_id = m_contact_lang_id : end property 
    property let contact_lang_id(varval) : m_contact_lang_id = varval : end property
    
    property get contact_lang_radical() : contact_lang_radical = m_contact_lang_radical : end property 
    property let contact_lang_radical(varval) : m_contact_lang_radical = varval : end property

    property get contact_gender() : contact_gender = m_contact_gender : end property 
    property let contact_gender(varval) : m_contact_gender = varval : end property

    property get enabled() : enabled = m_enabled : end property 
    property let enabled(varval) : m_enabled = varval : end property

    property get lead_comment() : lead_comment = m_lead_comment : end property 
    property let lead_comment(varval) : m_lead_comment = varval : end property
    
      
    Public Sub populateLeadObject()
            m_LQ_id = trim(request("LQ_id"))
            m_lead_id = trim(request("lead_id"))
            m_user_id= trim(request("user_id"))
            m_status_id= trim(request("status_id"))
            m_lang_id= trim(request("lang_id")) 
            m_source_id= trim(request("source_id")) 
            m_lead_type_id= trim(request("lead_type_id")) 
            m_backoffice_id= trim(request("backoffice_id")) 
            m_company_id= trim(request("company_id")) 
            m_prompt_date= trim(request("prompt_date")) 
            m_prompt_time= trim(request("prompt_time")) 
            m_company_name= trim(request("company_name")) 
            m_company_vat= trim(request("company_vat")) 
            m_company_address_street= trim(request("company_address_street")) 
            m_company_address_number= trim(request("company_address_number")) 
            m_company_address_postcode= trim(request("company_address_postcode")) 
            m_company_address_localite= trim(request("company_address_localite")) 
            m_company_tel= trim(request("company_tel")) 
            m_company_mobile= trim(request("company_mobile")) 
            m_company_mail= trim(request("company_mail")) 
            m_HBCompanyNotes= trim(request("HBCompanyNotes")) 
            m_trusted = trim(request("trusted"))
            m_new_user_id = trim(request("new_user_id"))
            m_contact_id = trim(request("contact_id"))
            m_company_hbc_id = trim(request("company_hbc_id"))
            m_company_lang_id = trim(request("company_lang_id"))
            m_company_address_type_id = trim(request("company_address_type_id"))
            m_contact_title_id = trim(request("contact_title_id"))
            m_contact_firstname = trim(request("contact_firstname"))
            m_contact_lastname = trim(request("contact_lastname"))
            m_contact_tel = trim(request("contact_tel"))
            m_contact_mobile = trim(request("contact_mobile"))
            m_contact_email = trim(request("contact_email"))
            m_contact_type_id = trim(request("contact_type_id"))
            m_contact_type = trim(request("contact_type"))
            m_contact_lang_id = trim(request("contact_lang_id"))
            m_contact_lang_radical = trim(request("contact_lang_radical"))
            m_contact_gender = trim(request("contact_gender"))
            m_enabled = trim(request("enabled"))
            m_lead_comment = trim(request("lead_comment"))
			m_newFiche = trim(request("newFiche"))
			m_uncheckedVAT = trim(request("uncheckedVAT"))
    end sub
 
    Public Sub responseWritepopulateLeadObject()
        response.write "lQ_id : " & LQ_id & "<br>" 
        response.write "lead_id : " & lead_id & "<br>" 
        response.write "user_id : " & user_id & "<br>"  
        response.write "status_id : " & status_id & "<br>" 
        response.write "lang_id : " & lang_id & "<br>" 
        response.write "source_id : " & source_id & "<br>"
        response.write "lead_type_id : " & lead_type_id & "<br>"
        response.write "backoffice_id : " & backoffice_id & "<br>"
        response.write "company_id : " & company_id & "<br>" 
        response.write "prompt_date : " & prompt_date & "<br>" 
        response.write "prompt_time : " & prompt_time & "<br>" 
        response.write "company_name : " & company_name & "<br>"
        response.write "company_vat : " & company_vat & "<br>" 
        response.write "company_address_street : " & company_address_street & "<br>" 
        response.write "company_address_number : " & company_address_number & "<br>"  
        response.write "company_address_postcode : " & company_address_postcode & "<br>" 
        response.write "company_address_localite : " & company_address_localite & "<br>"  
        response.write "company_tel : " & company_tel & "<br>"  
        response.write "company_mobile : " & company_mobile & "<br>" 
        response.write "company_mail : " & company_mail & "<br>"
        response.write "HBCompanyNotes : " & HBCompanyNotes & "<br>" 
        response.write "trusted : " & trusted  & "<br>" 
        response.write "new_user_id : " & new_user_id  & "<br>" 
    
        response.write "company_hbc_id : " & company_hbc_id &  "<br>" 
        response.write "company_lang_id : " & company_lang_id &  "<br>"
        response.write "company_address_type_id : " & company_address_type_id &  "<br>"
        response.write "contact_id : " & contact_id &  "<br>"
        response.write "contact_title_id : " & contact_title_id &  "<br>"
        response.write "contact_firstname : " & contact_firstname &  "<br>"
        response.write "contact_lastname : " & contact_lastname &  "<br>"
        response.write "contact_tel : " & contact_tel &  "<br>"
        response.write "contact_mobile : " & contact_mobile &  "<br>"
        response.write "contact_email : " & contact_email &  "<br>"
        response.write "contact_type_id : " & contact_type_id &  "<br>"
        response.write "contact_type : " & contact_type &  "<br>"
        response.write "contact_lang_id : " & contact_lang_id &  "<br>"
        response.write "contact_lang_radical : " & contact_lang_radical &  "<br>"
        response.write "contact_gender : " & contact_gender &  "<br>"
        response.write "enabled : " & enabled &  "<br>"
        response.write "lead_comment : " & lead_comment &  "<br>"   
		response.write "newFiche : " & newFiche &  "<br>" 
		response.write "uncheckedVAT : " & uncheckedVAT &  "<br>" 
    end sub 
        
 end class     
%>