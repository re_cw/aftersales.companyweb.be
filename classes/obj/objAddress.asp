<%
       
 class clsObjAddress
    
    'object data    
Dim m_company_address_id, m_company_id, m_address_id, m_address_type_id
Dim m_company_address_street, m_company_address_number, m_company_address_postcode, m_company_address_localite
Dim m_company_address_boxnumber, m_company_address_floor


    property get company_address_id() : company_address_id = m_company_address_id : end property 
    property let company_address_id(varval) : m_company_address_id = varval : end property
    
    property get company_id() : company_id = m_company_id : end property 
    property let company_id(varval) : m_company_id = varval : end property
    
    property get address_id() : address_id = m_address_id : end property 
    property let address_id(varval) : m_address_id = varval : end property

    property get address_type_id() : address_type_id = m_address_type_id : end property 
    property let address_type_id(varval) : m_address_type_id = varval : end property
           
    property get company_address_street() : company_address_street = m_company_address_street : end property 
    property let company_address_street(varval) : m_company_address_street = varval : end property

    property get company_address_number() : company_address_number = m_company_address_number : end property 
    property let company_address_number(varval) : m_company_address_number = varval : end property

    property get company_address_boxnumber() : company_address_boxnumber = m_company_address_boxnumber : end property 
    property let company_address_boxnumber(varval) : m_company_address_boxnumber = varval : end property
    
    property get company_address_floor() : company_address_floor = m_company_address_floor : end property 
    property let company_address_floor(varval) : m_company_address_floor = varval : end property
    
    property get company_address_postcode() : company_address_postcode = m_company_address_postcode : end property 
    property let company_address_postcode(varval) : m_company_address_postcode = varval : end property

    property get company_address_localite() : company_address_localite = m_company_address_localite : end property 
    property let company_address_localite(varval) : m_company_address_localite = varval : end property 
    
       
end class    
     %>