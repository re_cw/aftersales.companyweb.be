<%

class clsObjPdf
'pdf object data
Dim m_font, m_fontBold
Dim m_datum, m_contact, m_bedrijf, m_email, m_straat, m_plaats, m_telefoon, m_ondnr, m_einddatum
Dim m_prod1, m_pr1, m_prod2, m_pr2, m_som, m_operatordata, m_basic, m_standard, m_premium250, m_premium500, m_international
Dim m_aantalstandardmobapp, m_aantalpremiummobapp


'object data
Dim m_user_id, m_event_id        
Dim m_company_id, m_company_hbc_id, m_company_name, m_company_vat
Dim m_customerlang, m_salesfirstname, m_saleslastname, m_hrfunction
        
Dim m_company_address_street, m_company_address_number, m_company_address_boxnumber
Dim m_company_address_postcode, m_company_address_localite, m_company_tel, m_company_mail

Dim m_contact_firstname, m_contact_lastname, m_contact_email, m_lang_radical
        
Dim m_package, m_mobapps, m_order_date, m_end_date, m_trusted

    property get user_id() : user_id = m_user_id : end property 
    property let user_id(varval) : m_user_id = varval : end property
    
    property get event_id() : event_id = m_event_id : end property 
    property let event_id(varval) : m_event_id = varval : end property

    property get company_name() : company_name = m_company_name : end property 
    property let company_name(varval) : m_company_name = varval : end property

    property get company_vat() : company_vat = m_company_vat : end property 
    property let company_vat(varval) : m_company_vat = varval : end property

    property get salesfirstname () : salesfirstname  = m_salesfirstname  : end property 
    property let salesfirstname (varval) : m_salesfirstname  = varval : end property

    property get saleslastname () : saleslastname  = m_saleslastname  : end property 
    property let saleslastname (varval) : m_saleslastname  = varval : end property
    
    property get hrfunction() : hrfunction = m_hrfunction : end property 
    property let hrfunction(varval) : m_hrfunction = varval : end property

    property get customerlang() : customerlang = m_customerlang : end property 
    property let customerlang(varval) : m_customerlang = varval : end property

    property get contact_firstname () : contact_firstname  = m_contact_firstname  : end property 
    property let contact_firstname (varval) : m_contact_firstname  = varval : end property

    property get contact_lastname () : contact_lastname  = m_contact_lastname  : end property 
    property let contact_lastname (varval) : m_contact_lastname  = varval : end property

    property get contact_email() : contact_email = m_contact_email : end property 
    property let contact_email(varval) : m_contact_email = varval : end property

    property get lang_radical() : lang_radical = m_lang_radical : end property 
    property let lang_radical(varval) : m_lang_radical = varval : end property

    property get package() : package = m_package : end property 
    property let package(varval) : m_package = varval : end property

    property get mobapps() : mobapps = m_mobapps : end property 
    property let mobapps(varval) : m_mobapps = varval : end property

    property get order_date() : order_date = m_order_date : end property 
    property let order_date(varval) : m_order_date = varval : end property

    property get end_date() : end_date = m_end_date : end property 
    property let end_date(varval) : m_end_date = varval : end property



    property get company_id() : company_id = m_company_id : end property 
    property let company_id(varval) : m_company_id = varval : end property

    property get company_hbc_id() : company_hbc_id = m_company_hbc_id : end property 
    property let company_hbc_id(varval) : m_company_hbc_id = varval : end property
    
    property get company_address_street() : company_address_street = m_company_address_street : end property 
    property let company_address_street(varval) : m_company_address_street = varval : end property

    property get company_address_number() : company_address_number = m_company_address_number : end property 
    property let company_address_number(varval) : m_company_address_number = varval : end property

    property get company_address_boxnumber() : company_address_boxnumber = m_company_address_boxnumber : end property 
    property let company_address_boxnumber(varval) : m_company_address_boxnumber = varval : end property

    property get company_address_postcode() : company_address_postcode = m_company_address_postcode : end property 
    property let company_address_postcode(varval) : m_company_address_postcode = varval : end property

    property get company_address_localite() : company_address_localite = m_company_address_localite : end property 
    property let company_address_localite(varval) : m_company_address_localite = varval : end property

    property get company_tel() : company_tel = m_company_tel : end property 
    property let company_tel(varval) : m_company_tel = varval : end property

    property get company_mail() : company_mail = m_company_mail : end property 
    property let company_mail(varval) : m_company_mail = varval : end property

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property


    Public Sub populateobjBoCompanyOrderInfo() 
       
        m_user_id = request("user_id")
        m_company_id = request("company_id")
        m_event_id= request("event_id")
        m_company_hbc_id= request("company_hbc_id")
        m_company_name= request("company_name")
        m_company_vat= request("company_vat")

        m_customerlang= request("customerlang")
        m_salesfirstname= request("salesfirstname")
        m_saleslastname= request("saleslastname")
        m_hrfunction= request("hrfunction")

        m_company_address_street   = request("company_address_street")
        m_company_address_number   = request("company_address_number")
        m_company_address_boxnumber= request("company_address_boxnumber")
        m_company_address_postcode = request("company_address_postcode")
        m_company_address_localite = request("company_address_localite")
        m_company_tel= request("company_tel")

        m_contact_firstname= request("contact_firstname")
        m_contact_lastname = request("contact_lastname")
        m_contact_email = request("contact_email")
        m_lang_radical  = request("lang_radical")

        m_order_date = request("order_date")
        m_end_date = request("end_date")
        m_package = request("package")
        m_mobapps = request("mobapps")
        m_trusted = request("trusted")

    end sub

    Public Sub responseWriteobjBoCompanyOrderInfo() 
        
        response.write "user_id: " & user_id & "<br>"
        response.write "company_id: " & company_id & "<br>"
        response.write "event_id: " & event_id & "<br>"
        response.write "company_hbc_id: " & company_hbc_id & "<br>"
        response.write "company_name: " & company_name & "<br>"
        response.write "company_vat: " & company_vat & "<br>"
    
        response.write "customerlang: " & customerlang & "<br>"
        response.write "salesfirstname: " & salesfirstname & "<br>"
        response.write "saleslastname: " & saleslastname & "<br>"
        response.write "hrfunction: " & hrfunction & "<br>"

        response.write "company_address_street: " & company_address_street & "<br>"
        response.write "company_address_number: " & company_address_number & "<br>"
        response.write "company_address_boxnumber: " & company_address_boxnumber & "<br>"
        response.write "company_address_postcode: " & company_address_postcode & "<br>"
        response.write "company_address_localite: " & company_address_localite & "<br>"
        response.write "company_tel: " & company_tel & "<br>"

        response.write "contact_firstname: " & contact_firstname & "<br>"
        response.write "contact_lastname: " & contact_lastname & "<br>"
        response.write "contact_email: " & contact_email & "<br>"
        response.write "lang_radical:  " & lang_radical & "<br>"

        response.write "order_date: " & order_date & "<br>"
        response.write "end_date: " & end_date & "<br>"

        response.write "package: " & package & "<br>"
        response.write "mobapps: " & mobapps & "<br>"
        response.write "trusted: " & trusted

    end sub



end class
       
    %>