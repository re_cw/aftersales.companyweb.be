<%

class clsObjBoUser

dim m_HBU_recordcount, m_HBU_euroDate, m_ID

dim m_HBU_ID, m_HBU_HBC_ID, m_HBU_origin, m_HBU_intLang, m_HBU_userName, m_HBU_Password, m_HBU_FollowupEmail, m_HBU_LastUpdate, m_HBU_LastUpdateUser
dim m_HBU_CreationDate, m_HBU_AccLvl_BLOCKED, m_HBU_AccLvl_Website, m_HBU_Aantal_Website, m_HBU_Einde_Website
dim m_HBU_AccLvl_Mandaten, m_HBU_AccLvl_Followups, m_HBU_Aantal_Followups, m_HBU_AccLvl_FirstReports
dim m_HBU_Aantal_FirstReports,	m_HBU_AccLvl_JrcaXml,	m_HBU_Aantal_JrcaXml
dim m_HBU_Einde_JrcaXml,	m_HBU_AccLvl_Starters,	m_HBU_Type_Starters,	m_HBU_Einde_Starters
dim m_HBU_AccLvl_Failures,	m_HBU_Type_Failures,	m_HBU_Einde_Failures,	m_HBU_AccLvl_DataCheck,	m_HBU_Einde_DataCheck
dim m_HBU_AccLvl_DataRequest,	m_HBU_Einde_DataRequest,	m_HBU_AccLvl_BusAna,	m_HBU_Einde_BusAna,	m_HBU_ZoekNaam,	m_HBU_PopUpCount
dim m_HBU_TrialOrigin,	m_HBU_AccLvl_DmBlocked,	m_HBU_Aantal_BusAna, m_HBU_AccLvl_BetalingsErvaring, m_HBU_Aantal_BetalingsErvaring
dim m_HBU_AccLvl_XMLFollowups,	m_HBU_Password_EndDate,	m_HBU_Previous_Password
dim m_HBU_BetErv_Password,	m_HBU_MobAdm_HBU_Password,	m_HBU_HitsPerJaar, m_HBU_AccLvl_International,	m_HBU_CustomerUID, m_HBU_ReportLayout, m_HBU_Mailing
dim m_HBU_ActiveFollowUps, m_HBU_HitDates, m_HBU_HitNumbers    

dim m_HBU_HBO_PrintDate, m_HBU_HBO_tot, m_HBU_HBO_BetaaldOp, m_HBU_HBO_prijs, m_HBU_HBO_Info

    property get HBU_recordcount() : HBU_recordcount = m_HBU_recordcount : end property
    property let HBU_recordcount(varval) : m_HBU_recordcount = varval : end property

    property get HBU_euroDate() : HBU_euroDate = m_HBU_euroDate : end property
    property let HBU_euroDate(varval) : m_HBU_euroDate = varval : end property

    property get ID() : ID = m_ID : end property
    property let ID(varval) : m_ID = varval : end property

    property get HBU_ID() : HBU_ID = m_HBU_ID : end property
    property let HBU_ID(varval) : m_HBU_ID = varval : end property 
	
    property get HBU_HBC_ID() : HBU_HBC_ID = m_HBU_HBC_ID : end property
	property let HBU_HBC_ID(varval) : m_HBU_HBC_ID = varval : end property
    
    property get HBU_Origin() : HBU_Origin = m_HBU_Origin : end property
	property let HBU_Origin(varval) : m_HBU_Origin = varval : end property
    
    property get HBU_intLang() : HBU_intLang = m_HBU_intLang : end property
	property let HBU_intLang(varval) : m_HBU_intLang = varval : end property
    
    property get HBU_UserName() : HBU_UserName = m_HBU_UserName : end property
	property let HBU_UserName(varval) : m_HBU_UserName = varval : end property
    
    property get HBU_Password() : HBU_Password = m_HBU_Password : end property
	property let HBU_Password(varval) : m_HBU_Password = varval : end property
    
    property get HBU_FollowupEmail() : HBU_FollowupEmail = m_HBU_FollowupEmail : end property
	property let HBU_FollowupEmail(varval) : m_HBU_FollowupEmail = varval : end property
    
    property get HBU_LastUpdate() : HBU_LastUpdate = m_HBU_LastUpdate : end property
	property let HBU_LastUpdate(varval) : m_HBU_LastUpdate = varval : end property
    
    property get HBU_LastUpdateUser() : HBU_LastUpdateUser = m_HBU_LastUpdateUser : end property
	property let HBU_LastUpdateUser(varval) : m_HBU_LastUpdateUser = varval : end property
    
    property get HBU_CreationDate() : HBU_CreationDate = m_HBU_CreationDate : end property
	property let HBU_CreationDate(varval) : m_HBU_CreationDate = varval : end property
    
    property get HBU_AccLvl_BLOCKED() : HBU_AccLvl_BLOCKED = m_HBU_AccLvl_BLOCKED : end property
	property let HBU_AccLvl_BLOCKED(varval) : m_HBU_AccLvl_BLOCKED = varval : end property
    
    property get HBU_AccLvl_Website() :HBU_AccLvl_Website = m_HBU_AccLvl_Website : end property
	property let HBU_AccLvl_Website(varval) : m_HBU_AccLvl_Website = varval : end property
    
    property get HBU_Aantal_Website() :HBU_Aantal_Website = m_HBU_Aantal_Website : end property
	property let HBU_Aantal_Website(varval) : m_HBU_Aantal_Website = varval : end property
    
    property get HBU_Einde_Website() :HBU_Einde_Website = m_HBU_Einde_Website : end property
    property let HBU_Einde_Website(varval) : m_HBU_Einde_Website = varval : end property
     
    
	property get HBU_AccLvl_Mandaten() :HBU_AccLvl_Mandaten = m_HBU_AccLvl_Mandaten : end property
	property let HBU_AccLvl_Mandaten(varval) : m_HBU_AccLvl_Mandaten = varval : end property
    
    property get HBU_AccLvl_Followups() : HBU_AccLvl_Followups = m_HBU_AccLvl_Followups : end property
	property let HBU_AccLvl_Followups(varval) : m_HBU_AccLvl_Followups = varval : end property
    
    property get HBU_Aantal_Followups() : HBU_Aantal_Followups = m_HBU_Aantal_Followups : end property
	property let HBU_Aantal_Followups(varval) : m_HBU_Aantal_Followups = varval : end property
    
    property get HBU_AccLvl_FirstReports() :HBU_AccLvl_FirstReports = m_HBU_AccLvl_FirstReports : end property
	property let HBU_AccLvl_FirstReports(varval) : m_HBU_AccLvl_FirstReports = varval : end property
    
    property get HBU_Aantal_FirstReports() :HBU_Aantal_FirstReports = m_HBU_Aantal_FirstReports : end property
	property let HBU_Aantal_FirstReports(varval) : m_HBU_Aantal_FirstReports = varval : end property
    
    property get HBU_AccLvl_JrcaXml() :HBU_AccLvl_JrcaXml = m_HBU_AccLvl_JrcaXml : end property
	property let HBU_AccLvl_JrcaXml(varval) : m_HBU_AccLvl_JrcaXml = varval : end property
    
    property get HBU_Aantal_JrcaXml() :HBU_Aantal_JrcaXml = m_HBU_Aantal_JrcaXml : end property
	property let HBU_Aantal_JrcaXml(varval) : m_HBU_Aantal_JrcaXml = varval : end property
    
    property get HBU_Einde_JrcaXml() : HBU_Einde_JrcaXml = m_HBU_Einde_JrcaXml : end property
	property let HBU_Einde_JrcaXml(varval) : m_HBU_Einde_JrcaXml = varval : end property
    
    property get HBU_AccLvl_Starters() :HBU_AccLvl_Starters = m_HBU_AccLvl_Starters : end property
	property let HBU_AccLvl_Starters(varval) : m_HBU_AccLvl_Starters = varval : end property
    
    property get HBU_Type_Starters() :HBU_Type_Starters = m_HBU_Type_Starters : end property
	property let HBU_Type_Starters(varval) : m_HBU_Type_Starters = varval : end property
    
    property get HBU_Einde_Starters() : HBU_Einde_Starters = m_HBU_Einde_Starters : end property
	property let HBU_Einde_Starters(varval) : m_HBU_Einde_Starters = varval : end property
    
    property get HBU_AccLvl_Failures() :HBU_AccLvl_Failures = m_HBU_AccLvl_Failures : end property
	property let HBU_AccLvl_Failures(varval) : m_HBU_AccLvl_Failures = varval : end property
    
    property get HBU_Type_Failures() :HBU_Type_Failures = m_HBU_Type_Failures : end property
	property let HBU_Type_Failures(varval) : m_HBU_Type_Failures = varval : end property
    
    property get HBU_Einde_Failures() :HBU_Einde_Failures = m_HBU_Einde_Failures : end property
	property let HBU_Einde_Failures(varval) : m_HBU_Einde_Failures = varval : end property
    
    property get HBU_AccLvl_DataCheck() :HBU_AccLvl_DataCheck = m_HBU_AccLvl_DataCheck : end property
	property let HBU_AccLvl_DataCheck(varval) : m_HBU_AccLvl_DataCheck = varval : end property
    
    property get HBU_Einde_DataCheck() :HBU_Einde_DataCheck = m_HBU_Einde_DataCheck : end property
	property let HBU_Einde_DataCheck(varval) : m_HBU_Einde_DataCheck = varval : end property
    
    property get HBU_AccLvl_DataRequest() :HBU_AccLvl_DataRequest = m_HBU_AccLvl_DataRequest : end property
	property let HBU_AccLvl_DataRequest(varval) : m_HBU_AccLvl_DataRequest = varval : end property
    
    property get HBU_Einde_DataRequest() :HBU_Einde_DataRequest = m_HBU_Einde_DataRequest : end property
	property let HBU_Einde_DataRequest(varval) : m_HBU_Einde_DataRequest = varval : end property
    
    property get HBU_AccLvl_BusAna() :HBU_AccLvl_BusAna = m_HBU_AccLvl_BusAna : end property
	property let HBU_AccLvl_BusAna(varval) : m_HBU_AccLvl_BusAna = varval : end property
    
    property get HBU_Einde_BusAna() :HBU_Einde_BusAna = m_HBU_Einde_BusAna : end property
	property let HBU_Einde_BusAna(varval) : m_HBU_Einde_BusAna = varval : end property
    
    property get HBU_ZoekNaam() :HBU_ZoekNaam = m_HBU_ZoekNaam : end property
	property let HBU_ZoekNaam(varval) : m_HBU_ZoekNaam = varval : end property
    
    property get HBU_PopUpCount() :HBU_PopUpCount = m_HBU_PopUpCount : end property
	property let HBU_PopUpCount(varval) : m_HBU_PopUpCount = varval : end property
    
    property get HBU_TrialOrigin() :HBU_TrialOrigin = m_HBU_TrialOrigin : end property
	property let HBU_TrialOrigin(varval) : m_HBU_TrialOrigin = varval : end property
    
    property get HBU_AccLvl_DmBlocked() :HBU_AccLvl_DmBlocked = m_HBU_AccLvl_DmBlocked : end property
	property let HBU_AccLvl_DmBlocked(varval) : m_HBU_AccLvl_DmBlocked = varval : end property
    
    property get HBU_Aantal_BusAna() :HBU_Aantal_BusAna = m_HBU_Aantal_BusAna : end property
	property let HBU_Aantal_BusAna(varval) : m_HBU_Aantal_BusAna = varval : end property
    
    property get HBU_AccLvl_BetalingsErvaring() :HBU_AccLvl_BetalingsErvaring = m_HBU_AccLvl_BetalingsErvaring : end property
	property let HBU_AccLvl_BetalingsErvaring(varval) : m_HBU_AccLvl_BetalingsErvaring = varval : end property
    
    property get HBU_Aantal_BetalingsErvaring() :HBU_Aantal_BetalingsErvaring = m_HBU_Aantal_BetalingsErvaring : end property
	property let HBU_Aantal_BetalingsErvaring(varval) : m_HBU_Aantal_BetalingsErvaring = varval : end property
    
    property get HBU_AccLvl_XMLFollowups() :HBU_AccLvl_XMLFollowups = m_HBU_AccLvl_XMLFollowups : end property
	property let HBU_AccLvl_XMLFollowups(varval) : m_HBU_AccLvl_XMLFollowups = varval : end property
    
    property get HBU_Password_EndDate() :HBU_Password_EndDate = m_HBU_Password_EndDate : end property
	property let HBU_Password_EndDate(varval) : m_HBU_Password_EndDate = varval : end property
    
    property get HBU_Previous_Password() :HBU_Previous_Password = m_HBU_Previous_Password : end property
	property let HBU_Previous_Password(varval) : m_HBU_Previous_Password = varval : end property
    
    property get HBU_BetErv_Password() :HBU_BetErv_Password = m_HBU_BetErv_Password : end property
	property let HBU_BetErv_Password(varval) : m_HBU_BetErv_Password = varval : end property
    
    property get HBU_MobAdm_HBU_Password() :HBU_MobAdm_HBU_Password = m_HBU_MobAdm_HBU_Password : end property
	property let HBU_MobAdm_HBU_Password(varval) : m_HBU_MobAdm_HBU_Password = varval : end property
    
    property get HBU_HitsPerJaar() :HBU_HitsPerJaar = m_HBU_HitsPerJaar : end property
	property let HBU_HitsPerJaar(varval) : m_HBU_HitsPerJaar = varval : end property
    
    property get HBU_AccLvl_International() :HBU_AccLvl_International = m_HBU_AccLvl_International : end property
	property let HBU_AccLvl_International(varval) : m_HBU_AccLvl_International = varval : end property
    
    property get HBU_CustomerUID() : HBU_CustomerUID = m_HBU_CustomerUID : end property
	property let HBU_CustomerUID(varval) : m_HBU_CustomerUID = varval : end property
    
    property get HBU_ReportLayout() : HBU_ReportLayout = m_HBU_ReportLayout : end property
	property let HBU_ReportLayout(varval) : m_HBU_ReportLayout = varval : end property
    
    property get HBU_Mailing() : HBU_Mailing = m_HBU_Mailing : end property
    property let HBU_Mailing(varval) : m_HBU_Mailing = varval : end property

    property get HBU_ActiveFollowUps() : HBU_ActiveFollowUps = m_HBU_ActiveFollowUps : end property
    property let HBU_ActiveFollowUps(varval) : m_HBU_ActiveFollowUps = varval : end property

    property get HBU_HBO_PrintDate() : HBU_HBO_PrintDate = m_HBU_HBO_PrintDate : end property
    property let HBU_HBO_PrintDate(varval) : m_HBU_HBO_PrintDate = varval : end property

    property get HBU_HBO_tot() : HBU_HBO_tot = m_HBU_HBO_tot : end property
    property let HBU_HBO_tot(varval) : m_HBU_HBO_tot = varval : end property

    property get HBU_HBO_BetaaldOp() : HBU_HBO_BetaaldOp = m_HBU_HBO_BetaaldOp : end property
    property let HBU_HBO_BetaaldOp(varval) : m_HBU_HBO_BetaaldOp = varval : end property

    property get HBU_HBO_prijs() : HBU_HBO_prijs = m_HBU_HBO_prijs : end property
    property let HBU_HBO_prijs(varval) : m_HBU_HBO_prijs = varval : end property

    property get HBU_HBO_Info() : HBU_HBO_Info = m_HBU_HBO_Info : end property
    property let HBU_HBO_Info(varval) : m_HBU_HBO_Info = varval : end property

  
    Public Sub populateBoUserObject()
        m_ID = request("ID") 
        m_HBU_ID = request("HBU_ID") 
        m_HBU_HBC_ID = request("HBC_ID") 
        m_HBU_origin = request("HBU_origin")
        m_HBU_intLang = request("HBU_intLang")
        m_HBU_userName = request("HBU_userName") 
        m_HBU_Password = request("HBU_Password") 
        m_HBU_FollowupEmail = request("HBU_FollowupEmail") 
        m_HBU_LastUpdate = request("HBU_LastUpdate") 
        m_HBU_LastUpdateUser = request("HBU_LastUpdateUser") 
        m_HBU_CreationDate = request("HBU_CreationDate") 
        m_HBU_AccLvl_BLOCKED = request("HBU_AccLvl_BLOCKED") 
        m_HBU_AccLvl_Website = request("HBU_AccLvl_Website") 
        m_HBU_Aantal_Website = request("HBU_Aantal_Website") 
        m_HBU_Einde_Website = request("HBU_Einde_Website") 
        m_HBU_AccLvl_Mandaten = request("HBU_AccLvl_Mandaten") 
        m_HBU_AccLvl_Followups = request("HBU_AccLvl_Followups") 
        m_HBU_Aantal_Followups = request("HBU_Aantal_Followups") 
        m_HBU_AccLvl_FirstReports = request("HBU_AccLvl_FirstReports") 
        m_HBU_Aantal_FirstReports = request("HBU_Aantal_FirstReports")	
        m_HBU_AccLvl_JrcaXml = request("HBU_AccLvl_JrcaXml")	
        m_HBU_Aantal_JrcaXml = request("HBU_Aantal_JrcaXml")
        m_HBU_Einde_JrcaXml = request("HBU_Einde_JrcaXml")	
        m_HBU_AccLvl_Starters = request("HBU_AccLvl_Starters")	
        m_HBU_Type_Starters = request("HBU_Type_Starters")	
        m_HBU_Einde_Starters = request("HBU_Einde_Starters") 
        m_HBU_AccLvl_Failures = request("HBU_AccLvl_Failures")	
        m_HBU_Type_Failures = request("HBU_Type_Failures")	
        m_HBU_Einde_Failures = request("HBU_Einde_Failures")	
        m_HBU_AccLvl_DataCheck = request("HBU_AccLvl_DataCheck")	
        m_HBU_Einde_DataCheck = request("HBU_Einde_DataCheck")
        m_HBU_AccLvl_DataRequest = request("HBU_AccLvl_DataRequest")	
        m_HBU_Einde_DataRequest = request("HBU_Einde_DataRequest")	
        m_HBU_AccLvl_BusAna = request("HBU_AccLvl_BusAna")	
        m_HBU_Einde_BusAna = request("HBU_Einde_BusAna")	
        m_HBU_ZoekNaam = request("HBU_ZoekNaam")	
        m_HBU_PopUpCount = request("HBU_PopUpCount")
        m_HBU_TrialOrigin = request("HBU_TrialOrigin")	
        m_HBU_AccLvl_DmBlocked = request("HBU_AccLvl_DmBlocked")	
        m_HBU_Aantal_BusAna = request("HBU_Aantal_BusAna") 
        m_HBU_AccLvl_BetalingsErvaring = request("HBU_AccLvl_BetalingsErvaring") 
        m_HBU_Aantal_BetalingsErvaring = request("HBU_Aantal_BetalingsErvaring")
        m_HBU_AccLvl_XMLFollowups = request("HBU_AccLvl_XMLFollowups")	
        m_HBU_Password_EndDate = request("HBU_Password_EndDate")	
        m_HBU_Previous_Password = request("HBU_Previous_Password")
        m_HBU_BetErv_Password = request("HBU_BetErv_Password")	
        m_HBU_MobAdm_Password = request("HBU_MobAdm_Password")	
        m_HBU_HitsPerJaar = request("HBU_HitsPerJaar") 
        m_HBU_AccLvl_International = request("HBU_AccLvl_International")	
        m_HBU_CustomerUID = request("HBU_CustomerUID") 
        m_HBU_ReportLayout = request("HBU_ReportLayout") 
        m_HBU_Mailing = request("HBU_Mailing")
        m_HBU_ActiveFollowUps = request("HBU_ActiveFollowUps")
        m_HBU_HBO_PrintDate = request("HBU_HBO_PrintDate")
        m_HBU_HBO_tot = request("HBU_HBO_tot")
        m_HBU_HBO_BetaaldOp = request("HBU_HBO_BetaaldOp")
        m_HBU_HBO_prijs = request("HBU_HBO_prijs")
        m_HBU_HBO_Info  = request("HBU_HBO_Info")
        m_HBU_HitDates  = request("HBU_HitDates")
        m_HBU_HitNumbers  = request("HBU_HitNumbers")
    end sub


    public Sub responseWriteBoUserObject()
		response.write "ID:" & ID & "<br>" 
		response.write "HBU_ID:" & HBU_ID & "<br>" 
		response.write "HBU_HBC_ID:" & HBU_HBC_ID & "<br>"
		response.write "HBU_origin:" & HBU_origin & "<br>"
		response.write "HBU_intLang:" & HBU_intLang & "<br>"
		response.write "HBU_userName:" & HBU_userName & "<br>"
		response.write "HBU_Password:" & HBU_Password & "<br>" 
		response.write "HBU_FollowupEmail:" & HBU_FollowupEmail & "<br>"
		response.write "HBU_LastUpdate:" & HBU_LastUpdate & "<br>"
		response.write "HBU_LastUpdateUser:" & HBU_LastUpdateUser & "<br>"
		response.write "HBU_CreationDate:" & HBU_CreationDate & "<br>" 
		response.write "HBU_AccLvl_BLOCKED:" & HBU_AccLvl_BLOCKED & "<br>" 
		response.write "HBU_AccLvl_Website:" & HBU_AccLvl_Website & "<br>"
		response.write "HBU_Aantal_Website:" & HBU_Aantal_Website & "<br>"
		response.write "HBU_Einde_Website:" & HBU_Einde_Website& "<br>" 
		response.write "HBU_AccLvl_Mandaten:" & HBU_AccLvl_Mandaten& "<br>" 
		response.write "HBU_AccLvl_Followups:" & HBU_AccLvl_Followups& "<br>" 
		response.write "HBU_Aantal_Followups:" & HBU_Aantal_Followups & "<br>"
		response.write "HBU_AccLvl_FirstReports:" & HBU_AccLvl_FirstReports& "<br>"
		response.write "HBU_Aantal_FirstReports:" & HBU_Aantal_FirstReports& "<br>"	
		response.write "HBU_AccLvl_JrcaXml:" & HBU_AccLvl_JrcaXml& "<br>"	
		response.write "HBU_Aantal_JrcaXml:" & HBU_Aantal_JrcaXml& "<br>"
		response.write "HBU_Einde_JrcaXml:" & HBU_Einde_JrcaXml & "<br>" 
		response.write "HBU_AccLvl_Starters:" & HBU_AccLvl_Starters& "<br>"
		response.write "HBU_Type_Starters:" & HBU_Type_Starters & "<br>" 
		response.write "HBU_Einde_Starters:" & HBU_Einde_Starters& "<br>" 
		response.write "HBU_AccLvl_Failures:" & HBU_AccLvl_Failures& "<br>" 	
		response.write "HBU_Type_Failures:" & HBU_Type_Failures& "<br>" 
		response.write "HBU_Einde_Failures:" & HBU_Einde_Failures& "<br>" 	
		response.write "HBU_AccLvl_DataCheck:" & HBU_AccLvl_DataCheck& "<br>" 	
		response.write "HBU_Einde_DataCheck:" & HBU_Einde_DataCheck& "<br>"
		response.write "HBU_AccLvl_DataRequest:" & HBU_AccLvl_DataRequest& "<br>" 	
		response.write "HBU_Einde_DataRequest:" & HBU_Einde_DataRequest & "<br>"	
		response.write "HBU_AccLvl_BusAna:" & HBU_AccLvl_BusAna& "<br>"	
		response.write "HBU_Einde_BusAna:" & HBU_Einde_BusAna& "<br>" 	
		response.write "HBU_ZoekNaam:" & HBU_ZoekNaam& "<br>" 	
		response.write "HBU_PopUpCount:" & HBU_PopUpCount& "<br>" 
		response.write "HBU_TrialOrigin:" & HBU_TrialOrigin& "<br>" 	
		response.write "HBU_AccLvl_DmBlocked:" & HBU_AccLvl_DmBlocked& "<br>" 	
		response.write "HBU_Aantal_BusAna:" & HBU_Aantal_BusAna& "<br>" 
		response.write "HBU_AccLvl_BetalingsErvaring:" & HBU_AccLvl_BetalingsErvaring& "<br>" 
		response.write "HBU_Aantal_BetalingsErvaring:" & HBU_Aantal_BetalingsErvaring& "<br>"
		response.write "HBU_AccLvl_XMLFollowups:" & HBU_AccLvl_XMLFollowups& "<br>" 	
		response.write "HBU_Password_EndDate:" & HBU_Password_EndDate& "<br>" 	
		response.write "HBU_Previous_Password:" & HBU_Previous_Password& "<br>"
		response.write "HBU_BetErv_Password:" & HBU_BetErv_Password& "<br>"
		response.write "m_HBU_MobAdm_Password:" & m_HBU_MobAdm_Password& "<br>"
		response.write "HBU_HitsPerJaar:" & HBU_HitsPerJaar& "<br>"
		response.write "HBU_AccLvl_International:" & HBU_AccLvl_International& "<br>"	
		response.write "HBU_CustomerUID:" & HBU_CustomerUID & "<br>"
		response.write "HBU_ReportLayout:" & HBU_ReportLayout& "<br>"
		response.write "HBU_Mailing:" & HBU_Mailing& "<br>"
		response.write "HBU_ActiveFollowUps:" & HBU_ActiveFollowUps& "<br>"
		response.write "HBU_HBO_PrintDate:" & HBU_HBO_PrintDate& "<br>"
		response.write "HBU_HBO_tot:" & HBU_HBO_tot& "<br>"
		response.write "HBU_HBO_BetaaldOp:" & HBU_HBO_BetaaldOp& "<br>" 
		response.write "HBU_HBO_prijs:" & HBU_HBO_prijs& "<br>"
		response.write "HBU_HBO_Info:" & HBU_HBO_Info& "<br>"
		response.write "HBU_HitDates:" & HBU_HitDates& "<br>" 
		response.write "HBU_HitNumbers:" & HBU_HitNumbers& "<br>" 
    end sub

end class
%>

   