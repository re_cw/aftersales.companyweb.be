<%
        
class clsObjComment

'object integrity
Dim m_trusted

'object data 

dim m_comment_id, m_comment_event_type_id, m_comment_usr_id, m_comment_event_id, m_comment_lead_id, m_comment_company_vat, m_comment_company_id
dim m_comment_date, m_comment_time, m_comment, m_AuditUserUpdated
dim m_mailto, m_mailoptions  

    property get comment_id() : comment_id = m_comment_id : end property 
    property let comment_id(varval) : m_comment_id = varval : end property

    property get comment_event_type_id() : comment_event_type_id = m_comment_event_type_id : end property 
    property let comment_event_type_id(varval) : m_comment_event_type_id = varval : end property
    
    property get comment_usr_id() : comment_usr_id = m_comment_usr_id : end property 
    property let comment_usr_id(varval) : m_comment_usr_id = varval : end property
    
    property get comment_event_id() : comment_event_id = m_comment_event_id : end property 
    property let comment_event_id(varval) : m_comment_event_id = varval : end property
    
    property get comment_lead_id() : comment_lead_id = m_comment_lead_id : end property 
    property let comment_lead_id(varval) : m_comment_lead_id = varval : end property
    
    property get comment_company_vat() : comment_company_vat = m_comment_company_vat : end property 
    property let comment_company_vat(varval) : m_comment_company_vat = varval : end property
	
	property get comment_company_id() : comment_company_id = m_comment_company_id : end property 
    property let comment_company_id(varval) : m_comment_company_id = varval : end property
	
    property get comment_date() : comment_date = m_comment_date : end property 
    property let comment_date(varval) : m_comment_date = varval : end property
    
    property get comment_time() : comment_time = m_comment_time : end property 
    property let comment_time(varval) : m_comment_time = varval : end property
    
    property get comment() : comment = m_comment : end property 
    property let comment(varval) : m_comment = varval : end property
 
    property get AuditUserUpdated() : AuditUserUpdated = m_AuditUserUpdated : end property 
    property let AuditUserUpdated(varval) : m_AuditUserUpdated = varval : end property 

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property
 
    property get mailto() : mailto = m_mailto : end property 
    property let mailto(varval) : m_mailto = varval : end property

    property get mailoptions() : mailoptions = m_mailoptions : end property 
    property let mailoptions(varval) : m_mailoptions = varval : end property
       
    Public Sub populateCommentObject()

            m_comment_id = request("comment_id")
            m_comment_event_type_id = request("comment_event_type_id") 
            m_comment_usr_id = request("comment_usr_id") 
            m_comment_event_id = request("comment_event_id") 
            m_comment_lead_id = request("comment_lead_id") 
            m_comment_company_vat = request("comment_company_vat") 
			m_comment_company_id = request("comment_company_id") 
            m_comment_date = request("comment_date") 
            m_comment_time = request("comment_time") 
            m_comment = request("comment") 
            m_AuditUserUpdated = request("AuditUserUpdated")  
            m_trusted = request("trusted")
            m_mailto =  request("mailto")
            m_mailoptions =  request("mailoptions")
    
    end sub

    Public Sub responseWriteCommentObject()

        response.write "comment_id : " & comment_id  & "<br>"  
        response.write "comment_event_type_id : " & comment_event_type_id  & "<br>"  
        response.write "comment_usr_id : " & comment_usr_id  & "<br>"  
        response.write "comment_event_id : " & comment_event_id  & "<br>" 
        response.write "comment_lead_id : " & comment_lead_id  & "<br>" 
        response.write "comment_company_vat : " & comment_company_vat  & "<br>"  
		response.write "comment_company_id : " & comment_company_id & "<br />" 
        response.write "comment_date : " & comment_date  & "<br>"
        response.write "comment_time : " & comment_time  & "<br>"
        response.write "comment : " & comment  & "<br>" 
        response.write "AuditUserUpdated : " & AuditUserUpdated  & "<br>"
        response.write "trusted : " & trusted  & "<br>" 
        response.write "mailto : " & mailto  & "<br>" 
        response.write "mailoptions : " & mailoptions  & "<br>" 
    
    end sub
     
 end class     
%>