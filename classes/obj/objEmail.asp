<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objContact.asp" -->
<!-- #include virtual ="/classes/dal/clsPdf/clsPdf.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<%

         
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Response.CodePage = 65001    
Response.CharSet = "utf-8" 

CONST C_TransferObjectFolder    = "/xtmpTO"

CONST cdoSendUsingPort = 2	'Send the message using the network (SMTP over the network).
CONST cdoAnonymous = 0      'Do not authenticate
CONST cdoBasic = 1          'Basic (clear-text) authentication
CONST cdoNTLM = 2           'NTLM authentication

'CONST config_MailSMTPServer     = "smtp.gmail.com"  '"localhost"
'CONST config_MailSMTPServerPort = 465 '25
'CONST config_MailSMTPUsername   = "info@companyweb.be"
'CONST config_MailSMTPPwd        = "cw.info.123456"
'CONST config_MailUseSSL         = true

CONST config_MailSMTPServer     = "localhost"
CONST config_MailSMTPServerPort = 25
CONST config_MailSMTPUsername   = ""
CONST config_MailSMTPPwd        = ""
CONST config_MailUseSSL         = False

     
class clsObjEmail

'mail object
  Dim objMailer, objConfig, strSchema, strAttachments, i
  Dim objVCal
        
'object data    
  Dim m_Mailfrom, m_MailTo, m_MailCC, m_MailBCC, m_Subject, m_Body
  Dim m_IsHTML, m_Attachment, m_Error, m_customerlang

  Dim m_mail_id, m_order_list_token_id, m_event_id, m_company_id, m_contact_id
  Dim m_user_id, m_mail_template_id, m_mail_creationdate, m_event_type_id

'object VCal
 Dim m_DTStart, m_DTEnd, m_DTSTAMP, m_UID, m_Location, m_Summary, m_Description, m_filename, m_filenameVCard

'mail Options from Scheduler
Dim m_optSendMail, m_optSendVCal, m_optSendVCard

    property get Mailfrom() : Mailfrom = m_Mailfrom : end property 
    property let Mailfrom(varval) : m_Mailfrom = varval : end property
    
    property get MailTo() : MailTo = m_MailTo : end property 
    property let MailTo(varval) : m_MailTo = varval : end property
    
    property get MailCC() : MailCC = m_MailCC : end property 
    property let MailCC(varval) : m_MailCC = varval : end property

    property get MailBCC() : MailBCC = m_MailBCC : end property 
    property let MailBCC(varval) : m_MailBCC = varval : end property
           
    property get Subject() : Subject = m_Subject : end property 
    property let Subject(varval) : m_Subject = varval : end property

    property get Body() : Body = m_Body : end property 
    property let Body(varval) : m_Body = varval : end property

    property get IsHTML() : IsHTML = m_IsHTML : end property 
    property let IsHTML(varval) : m_IsHTML = varval : end property
    
    property get Attachment() : Attachment = m_Attachment : end property 
    property let Attachment(varval) : m_Attachment = varval : end property
    
    property get Error() : Error = m_Error : end property 
    property let Error(varval) : m_Error = varval : end property

    property get customerlang() : customerlang = m_customerlang : end property 
    property let customerlang(varval) : m_customerlang = varval : end property

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property
   
    property get mail_id() : mail_id = m_mail_id : end property 
    property let mail_id(varval) : m_mail_id = varval : end property
 
    property get order_list_token_id() : order_list_token_id = m_order_list_token_id : end property 
    property let order_list_token_id(varval) : m_order_list_token_id = varval : end property  

    property get event_type_id() : event_type_id = m_event_type_id : end property 
    property let event_type_id(varval) : m_event_type_id = varval : end property

    property get event_id() : event_id = m_event_id : end property 
    property let event_id(varval) : m_event_id = varval : end property

    property get company_id() : company_id = m_company_id : end property 
    property let company_id(varval) : m_company_id = varval : end property

    property get contact_id() : contact_id = m_contact_id : end property 
    property let contact_id(varval) : m_contact_id = varval : end property

    property get user_id() : user_id = m_user_id : end property 
    property let user_id(varval) : m_user_id = varval : end property

    property get mail_template_id() : mail_template_id = m_mail_template_id : end property 
    property let mail_template_id(varval) : m_mail_template_id = varval : end property

    property get mail_creationdate() : mail_creationdate = m_mail_creationdate : end property 
    property let mail_creationdate(varval) : m_mail_creationdate = varval : end property

'mail Options from Scheduler properties
    property get optSendMail() : optSendMail = m_optSendMail : end property 
    property let optSendMail(varval) : m_optSendMail = varval : end property

    property get optSendVCal() : optSendVCal = m_optSendVCal : end property 
    property let optSendVCal(varval) : m_optSendVCal = varval : end property

    property get optSendVCard() : optSendVCard = m_optSendVCard : end property 
    property let optSendVCard(varval) : m_optSendVCard = varval : end property

 'object VCal properties   
    property get DTStart() : DTStart = m_DTStart : end property 
    property let DTStart(varval) : m_DTStart = varval : end property

    property get DTEnd() : DTEnd = m_DTEnd : end property 
    property let DTEnd(varval) : m_DTEnd = varval : end property

    property get DTSTAMP() : DTSTAMP = m_DTSTAMP : end property 
    property let DTSTAMP(varval) : m_DTSTAMP = varval : end property

    property get UID() : UID = m_UID : end property 
    property let UID(varval) : m_UID = varval : end property

    property get Location() : Location = m_Location : end property 
    property let Location(varval) : m_Location = varval : end property

    property get Summary() : Summary = m_Summary : end property 
    property let Summary(varval) : m_Summary = varval : end property

    property get Description() : Description = m_Description : end property 
    property let Description(varval) : m_Description = varval : end property

    property get filename() : filename = m_filename : end property 
    property let filename(varval) : m_filename = varval : end property

    property get filenameVCard() : filenameVCard = m_filenameVCard : end property 
    property let filenameVCard(varval) : m_filenameVCard = varval : end property

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property


'<START PUBLIC CALLS>===========================================================    
    public function Send()
        Send = pSend()
        if len(trim(Send))=0 then Send = Save()
    end function
	public function SendNotification()
		SendNotification = pSendNotification()
		if len(trim(SendNotification))=0 then SendNotification = Save()
	end function

    public function SendWithOptions()
        SendWithOptions = pSendWithOptions()
		if len(trim(SendWithOptions))=0 then SendWithOptions = Save()
    end function

    public function reSend()
        reSend = preSend()
		if len(trim(reSend))=0 then reSend = Save()
    end function

    public function Save()
        Save = pSave()
    end function

    public function SendGenericTemplate()
        SendGenericTemplate=pGetMailItemsByTemplateID()
    end function
	
	public function SendTemplateSubject()
		SendTemplateSubject = pSendTemplateSubject()
	end function
	
Private Function IsNotNullorEmpty(input)
	dim IsNotNullorEmpty_Output
	IsNotNullorEmpty_Output = true
	'vbEmpty (uninitialized) or vbNull (no valid data)
	if VarType(input) = 0 OR VarType(input) = 1 Then
		IsNotNullorEmpty_Output = false
	End if
	If VarType(input) = 8 Then
		If input = "" Then 
			IsNotNullorEmpty_Output = False
		End If
	End If
	IsNotNullorEmpty = IsNotNullorEmpty_Output

End Function
'<START REGION PRIVATE BUSINESS LOGIC>===========================================================
private function pSendWithOptions()
    Dim oRs, itemFieldName, i, d, b, SalesInfo
    Dim objContact
    set objContact = new clsObjcontact
    
    objContact.contact_id = contact_id
        GetContactDetailsByContactId(objContact)
    Mailfrom = request.Cookies ("TELESALESTOOLV2")("email1")
    MailTo   = objContact.contact_email
    
    itemFieldName = "mail_item_" & objContact.contact_lang_radical
    set oRs=Server.CreateObject("ADODB.Recordset") 
        GetMailItemsByEventTypeId event_type_id, oRs
        if oRs.recordcount = 0 then exit function
            oRs.moveFirst
            oRs.move 0
                Summary() = .fields(itemFieldName)
            oRs.move 1
                do while not oRs.eof
                    if oRs.Fields("mail_item_tag")="@notifyVCal"  and optSendVCal() = 0 then oRs.Movenext
                    if oRs.Fields("mail_item_tag")="@notifyVCard"  and optSendVCard() = 0 then oRs.Movenext
                        b = b &  .fields(itemFieldName) & chr(10) & chr(13)
                    oRs.Movenext
                loop
            oRs.Close : set oRs = nothing

        'b = b & pGetsalesInfo() 
           'b = b & chr(10) & chr(13) & "http://www.companyweb.be"
        Subject() = Summary()
        Body() = CastBodyMetaTags(b, objContact)
        CompleteHtmlBody()
        
    if optSendVCal = 1 then
        'VCalendar Event
        Description() = Summary()
        Location() = "Your Desk"
        CreateVCal()
    end if
    if optSendVCard = 1 then
        'CreateVCard
    end if

    ConfigMailObject()
    CreateObjectMailer()

On Error Resume Next
            objMailer.Send
            delFile Attachment()
           If Err.Number <> 0 Then Error()  = Error() & vbcrlf & err.Description
            pSendWithOptions = Error()
end function

private function pGetsalesInfo()
Dim s
    s = chr(10) & chr(13)
            s= Request.Cookies ("TELESALESTOOLV2")("firstname") & " " & Request.Cookies ("TELESALESTOOLV2")("lastname") & chr(10)
            s =s & Request.Cookies ("TELESALESTOOLV2")("hrfunction") & chr(10)
            s =s & Request.Cookies ("TELESALESTOOLV2")("phone1")& chr(10)
            s =s & Request.Cookies ("TELESALESTOOLV2")("email1") 
            
pGetsalesInfo = s            

end function

private function CastBodyMetaTags(b, objContact)
    Dim i, sd, ed, startDate, username, dear, startHour, endHour, firstname, salesfirstname, saleslastname, hrfunction, login, password
    salesfirstname=Request.Cookies ("TELESALESTOOLV2")("firstname")
    saleslastname=Request.Cookies ("TELESALESTOOLV2")("lastname")
    hrfunction = Request.Cookies ("TELESALESTOOLV2")("hrfunction")
    sd = split(DTStart()," ")
    ed = split(DTEnd()," ")
    startDate = formatEuroDate(sd(0))
    startHour = sd(1)
    endHour = ed(1)
    username = objContact.contact_title & " " & objContact.contact_lastname
    firstname = objContact.contact_firstname
    
    dear = "Geachte"
    select case objContact.contact_lang_radical
        case "FR"
                select case objContact.contact_gender
                    case "M"
                        dear = "Cher"
                    case "F"
                        dear = "Chère"
                end select
        case "NL"
            dear = "Geachte"
        case else
            dear = ""
    end select

    arrkey= array("@dear","@userName","@startDate","@startHour","@endHour","@firstname","@filename","@filenameVCard", "@getSalesInfo", "@login", "@password","@salesfirstname","@saleslastname","@hrfunction")
    arrvar= array(dear,userName,startDate,startHour,endHour,firstname, filename(), filenameVCard(), pGetsalesInfo(),login, password, salesfirstname, saleslastname, hrfunction)
    for i = lbound(arrkey) to ubound(arrkey)
        b = replace(b,arrkey(i), arrvar(i) ,1,-1, vbtextcompare)
    next
    
    CastBodyMetaTags = b

end function

    
        
    private function preSend()
        MailFrom   = request.Cookies ("TELESALESTOOLV2")("email1")
        MailTo     = request("MailTo") 
        MailCC     = request("MailCC") 
        MailBCC    = request("MailBCC") 
        Subject    = request("Subject")
        Body       = request("Body") 
        IsHTML     = request("IsHTML")
        Attachment = request("Attachment")
		contact_id = request("contact_id")
		
		Dim objContact
		set objContact = new clsObjcontact
		
		objContact.contact_id = contact_id
			GetContactDetailsByContactId(objContact)

		mail_template_id = request("mail_template_id")
		select case mail_template_id()
			case 17
				if objContact.contact_lang_radical = "NL" then
					attachment = "/xlib/xtemplates/Algemene info Companyweb.docx"
				else
					attachment = "/xlib/xtemplates/Informations générales Companyweb.docx"
				end if
		end select
		Body = Join(Split(Body, ">"), ">" & vbCrLf)
        ConfigMailObject()
        CreateObjectMailer()

        On Error Resume Next
            objMailer.Send
            If Err.Number <> 0 Then Error()  = Error() & vbcrlf & err.Description
            preSend = Error()
    end function       
    private function pSendNotification()
		MailFrom = request.Form("MailFrom")
		MailTo = request.Form("MailTo")
		Subject = request.Form("Subject")
		Body = request.Form("Body")
		IsHTML = true
		
		validateobjMailAddresses()
		
		ConfigMailObject()
        CreateObjectMailer()
		
		On Error Resume Next
        objMailer.Send

        If Err.Number <> 0 Then 
			Error()  = Error() & vbcrlf & err.Description
			pSendNotification = Error()
		End if
        
	end function
    private function pSend()       
        populateMailObject()
        validateobjMailAddresses()
        validateobjMailBody()
        CompleteHtmlBody()    
        ConfigMailObject()
        CreateObjectMailer()
    
        On Error Resume Next
            objMailer.Send
            If Err.Number <> 0 Then Error()  = Error() & vbcrlf & err.Description
            pSend = Error()
    end function


    private function pSave()
    populateMailObject()
    CompleteHtmlBody()
    Dim objConn, oRs, strSql
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

		' select case mail_template_id()
			' case 17
				' if customerlang() = "nl" then
					' Attachment = "/xlib/xtemplates/Algemene info Companyweb.docx"
				' else
					' Attachment = "/xlib/xtemplates/Informations générales Companyweb.docx"
				' end if
		' end select		
    
        strSql = "select TOP 1 * from dbo.mails WHERE 1=0"
        set oRs=Server.CreateObject("ADODB.Recordset") 
            oRs.open strSql, objConn, 1, 3
                dim tmpBody
                tmpBody=server.htmlencode(Body)

           with oRs
                .addnew
					if IsNotNullorEmpty(order_list_token_id) then
						.fields("order_list_token_id")=order_list_token_id
					end if 
					
					if isnumeric(event_id) then
						.fields("event_id")=event_id
					end if
					if isnumeric(company_id) then
						.fields("company_id")=company_id
					end if
					if isnumeric(contact_id) then
						.fields("contact_id")=contact_id
					end if
                    .fields("user_id")=user_id
                    .fields("customerlang")=toUnicode(customerlang)
					
					if isnumeric(mail_template_id) then
						.fields("mail_template_id")=mail_template_id
					end if
					
                    .fields("mail_creationdate")=mail_creationdate
                    .fields("MailTo")=toUnicode(MailTo)
                    .fields("MailCC")=toUnicode(MailCC)
                    .fields("MailBCC")=toUnicode(MailBCC)
                    .fields("Subject")=toUnicode(Subject)
                    .fields("Body")=toUnicode(Body)
                    .fields("Attachment")=toUnicode(Attachment)
                .update
                
           end with
    
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing
           
            If Err.Number <> 0 Then Error()  = Error() & vbcrlf & err.Description
            pSave = Error()
    
    end function
    
    Public Sub populateMailObject()
		Mailfrom  = request.Cookies ("TELESALESTOOLV2")("email1")
			
        MailTo    = request("MailTo") 
        MailCC    = request("MailCC") 
        MailBCC = request("MailBCC") 
        Subject    = request("Subject")
        Body       = request("Body") 
        IsHTML     = request("IsHTML")
        Attachment = request("Attachment")
        customerlang= request("customerlang")
        trusted    = request("trusted")
        mail_id = request("mail_id")
        order_list_token_id = request("order_list_token_id")
        event_id = request("event_id")
        company_id = request("company_id")
        contact_id = request("contact_id")
        user_id = request("user_id")
        mail_template_id = request("mail_template_id")
        mail_creationdate = now()
    end sub
     
    private Sub ConfigMailObject()
    
        'Initialize objects and variables
	        Set objMailer = CreateObject("CDO.Message")
            Set objConfig = CreateObject("CDO.Configuration")
            strSchema = "http://schemas.microsoft.com/cdo/configuration/"
	        Error = vbNullString

        'Set parameters
        With objConfig.Fields
            .Item(strSchema & "sendusing") = cdoSendUsingPort
            .Item(strSchema & "smtpserver") = config_MailSMTPServer
            .Item(strSchema & "smtpserverport") = config_MailSMTPServerPort
            .Item(strSchema & "smtpconnectiontimeout") = 60
            If config_MailSMTPUsername <> vbNullString Then
                .Item(strSchema & "smtpauthenticate") = cdoBasic
                .Item(strSchema & "sendusername") = config_MailSMTPUsername
                .Item(strSchema & "sendpassword") = config_MailSMTPPwd
            Else
                .Item(strSchema & "smtpauthenticate") = cdoAnonymous
            End If
            .Item(strSchema & "smtpusessl") = config_MailUseSSL
            .Update
        End With

    end sub        

    private sub CreateObjectMailer()
        With objMailer
            Set .Configuration = objConfig
            .From = Mailfrom()
            .To = MailTo()
                If MailCC <> "" Then .CC = MailCC()
                If MailBCC <> "" Then .BCC = MailBCC()
            .Subject = Subject()
            If IsHTML Then 
				.HTMLBody = Body() 
			Else 
				.TextBody = Body()
			End if
        
            If Attachment <> vbNullString Then
				dim dic
				set dic = Server.CreateObject("Scripting.Dictionary")
				
                strAttachments = split(Attachment,";")
                for i = lbound(strAttachments) to ubound(strAttachments)
					If NOT dic.Exists(UCASE(server.MapPath(strAttachments(i)))) Then
						dic.Add UCASE(server.MapPath(strAttachments(i))), "NotEmpty"
						.AddAttachment server.MapPath(strAttachments(i))
					End if
                next
				set dic = Nothing
            End If
        End With
    
    end sub

    private sub validateobjMailAddresses()
        Dim err : Err = ""
            if not IsValidEmail(Mailfrom()) then Err = Err &  "Invalid FROM Email address" & vbcrlf
            if not IsValidEmail(MailTo()) then Err = Err &  "Invalid TO Email address" & vbcrlf
            if not IsValidEmail(MailCC()) then Err = Err &  "Invalid CC Email address" & vbcrlf
            if not IsValidEmail(MailBCC()) then Err = Err &  "Invalid BCC Email address" & vbcrlf
            If len(trim(Subject())) then Err = Err & "Mail Subject is Empty" & vbcrlf
            If len(trim(Body())) then Err = Err & "Mail Body is Empty" & vbcrlf    
            Error() = Error() & Err
    end sub

    private sub validateobjMailBody()
        dim arrSearch, arrErrMsg, i, Err
            arrSearch = Array("@dear","@quote1","@package","@nationalreport","@globalitem","@alerts","@mobileapps","@intreports","@price", "@quote2","@quote3","@quote4","@username","@usertitle","@phone1","@mail1")
            arrErrMsg = Array("@dear","@quote1","@package","@nationalreport","@globalitem","@alerts","@mobileapps","@intreports","@price", "@quote2","@quote3","@quote4","@username","@usertitle","@phone1","@mail1")
            'arrErrMsg = Array(locale.poErr.dear,locale.poErr.quote1,locale.poErr.package,locale.poErr.nationalreport,locale.poErr.globalitem,locale.poErr.alerts,locale.poErr.mobileapps,locale.poErr.intreports,locale.poErr.price, locale.poErr.quote2,locale.poErr.quote3,locale.poErr.quote4,locale.poErr.username,locale.poErr.usertitle,locale.poErr.phone1,locale.poErr.mail1)
            for i = lbound(arrSearch) to ubound(arrSearch)-1
                if instr(1,arrSearch(i), Body(), vbtextcompare) > 0 then Err = Err & arrErrMsg(i) & vbcrlf
            next   
        Error() = Error() & Err
    end sub

  private Sub CompleteHtmlBody()
    Dim htmlHeader , htmlFooter
	dim splittedTags
    htmlHeader ="<html><head><title></title><meta http-equiv=""Content-Type""  content=""text/html charset=UTF-8"" /></head><body>" 
    htmlFooter="</body></html>"
    Body() = htmlHeader & Body() & htmlFooter
	
	Body() = Join(Split(Body(), ">"), ">" & vbCrLf)
	
    'Body() = Replace(Body(), "<", vbCrLf & "<")
	'Body() = Replace(Body(), ">", ">" & vbCrLf)
    end sub


    private function IsValidEmail(strEAddress)
         Dim objRegExpr
         Set objRegExpr = New RegExp
         objRegExpr.Pattern = "^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[\w-\.]*[a-zA-Z0-9]\.[a-zA-Z]{2,7}$"
         objRegExpr.Global = True
         objRegExpr.IgnoreCase = False
         IsValidEmail = objRegExpr.Test(strEAddress)
         Set objRegExpr = Nothing
    end Function


'*****<START REGION VCALENDAR OBJECT>
 
    private function createVCal()
        Dim FullPathName
        UID() = CreateGUID()
        filename() = UID & "_vcal.ics"
        FullPathName = C_TransferObjectFolder & "/" & UID & "_vcal.ics"
        Response.Buffer = True
	    server.ScriptTimeout = 3000
	    Set objFSO = CreateObject("Scripting.FileSystemObject")
	    Dim f : Set f = objFSO.CreateTextFile(server.mapPath(FullPathName), TRUE)
    
        with f
            .WriteLine("BEGIN:VCALENDAR")
                .WriteLine("PRODID:Microsoft CDO for Microsoft Exchange")
                .WriteLine("VERSION:2.0")
                .WriteLine("METHOD:PUBLISH")
                .WriteLine("TZID:Romance Standard Time")
                .WriteLine("BEGIN:VEVENT")
                .WriteLine("DTSTAMP:" & DTSTAMP)
                .WriteLine("DTStart:" & dateToUTC(DTStart))
                .WriteLine("DTEnd:" & dateToUTC (DTEnd))
                .WriteLine("UID:" & UID)
                .WriteLine("SAFE-CHAR:/ NON-US-ASCII")
                .WriteLine("LOCATION:" & Location)
                .WriteLine("SUMMARY;LANGUAGE=nl-be:" & Summary)
                .WriteLine("DESCRIPTION;LANGUAGE=nl-be:" & Summary())
                .WriteLine("SEQUENCE:0")
                .WriteLine("PRIORITY:1")
                .WriteLine("CLASS:Personal")
                .WriteLine("STATUS:CONFIRMED")
                .WriteLine("TRANSP:OPAQUE")
                .WriteLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY")
                .WriteLine("X-MICROSOFT-CDO-INSTTYPE:0")
                .WriteLine("BEGIN:VALARM")
                .WriteLine("TRIGGER:PT15M")
                .WriteLine("ACTION:DISPLAY")
                .WriteLine("DESCRIPTION:Reminder")
                .WriteLine("END:VALARM")
                .WriteLine("END:VEVENT")
                .WriteLine("END:VCALENDAR")  
            .close
        end with
        Attachment()= FullPathName

    end function
    
    Private Sub delFile(fileDEL)
	    Set fs=createobject("Scripting.FileSystemObject") 
		    if fs.FileExists(fileDEL) then
			    fs.DeleteFile fileDEL, True
		    end if
	    set fs=nothing
    End Sub
    
    private Function dateToUTC (sDate)
	    Dim iHour, iMin, iYear, iMonth, iDay, tDate, tTime 
	    tDate = DateValue(split(sDate, " ")(0))
	    tTime = TimeValue(split(sDate, " ")(1))
	    iHour = Hour(tTime) 'Hour(DateAdd("h", 4, tTime)) 'correction for US Eastern time
	    iMin = Minute(tTime)
	    iYear = Year(tDate)
	    iMonth = Month(tDate)
	    iDay = Day(tDate)
	    Dim s : s = iYear
	    if iMonth < 10 then s = s & "0"
		    s = s & iMonth
	    if iDay < 10 then s = s & "0"
		    s = s & iDay & "T"
	    if iHour < 10 then s = s & "0"
		    s = s & iHour
	    if iMin < 10 then s = s & "0"
		    s = s & iMin & "00Z"
	    dateToUTC = s
    End Function

    private function CreateGUID()
        Randomize Timer
        Dim tmpTemp1,tmpTemp2,tmpTemp3
        tmpTemp1 = Right(String(15,48) & CStr(CLng(DateDiff("s","1/1/2000",Date()))), 15)
        tmpTemp2 = Right(String(5,48) & CStr(CLng(DateDiff("s","12:00:00 AM",Time()))), 5)
        tmpTemp3 = Right(String(5,48) & CStr(Int(Rnd(1) * 100000)),5)
        CreateGUID = tmpTemp1 & tmpTemp2 & tmpTemp3
    End Function
    
    private function castNewLine(varval) 
        Dim s : s = "\n"
        Dim br : br = "<br>"
        Dim crlf : crlf = chr(10) & chr(13)
            castNewLine= replace(varval, br, s, 1,-1,vbtextcompare)
            castNewLine= replace(varval, crlf, s, 1,-1,vbtextcompare)
    end function    
      
'*****<END REGION VCALENDAR OBJECT>

private function GetContactDetailsByContactId(objContact)

        Dim objConn, objRS, strSql, sErrMsg, objCmd
        
    strSql = "select * from [dbo].[vw_GetContacts] where contact_id=" & objContact.contact_id
    Dim title_name
      
            set objConn=Server.CreateObject("ADODB.Connection")
            set objRS=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                objRS.open strSql, objConn
        
        with objRS
            if not .bof and not .eof then
                objContact.contact_lang_radical= .fields("lang_radical")
                title_name = "title_name_" & objContact.contact_lang_radical
                objContact.contact_lang_id = .fields("contact_lang_id") 
                objContact.contact_gender = .fields("contact_gender") 
                objContact.contact_title = .fields(title_name)
                objContact.contact_firstname = .fields("contact_firstname")
                objContact.contact_lastname = .fields("contact_lastname")
                objContact.contact_email = .fields("contact_email")
                    
            end if
        end with    
    
    GetContactDetailsByContactId = 1
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    if err.number <> 0 then GetContactDetailsByContactId = err.description

end function

Private function GetMailItemsByEventTypeId(event_type_id, oRs)
    Dim objConn, strSql
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

        strSql = "select * from [dbo].[vw_GetEventType_MailTemplate] WHERE event_type_id=" & event_type_id & "  order by mail_item_order"
        oRs.open strSql, objConn, 1, 3       
end function

private function pSendTemplateSubject()
	populateMailObject()
	dim objConn, oRs, strSql, mailSubjecy, s
	mailSubject = "mail_template_subject_" & customerlang()
       set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=1     '3:adModeReadWrite 1:adModeRead
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if
		
	set oRs=Server.CreateObject("ADODB.Recordset")
    
    strSql = "select " & mailSubject & " from [dbo].[vw_GetMail_Templates] WHERE mail_template_id=" & mail_template_id() & " order by " & mailSubject    
    
    oRs.open strSql, ObjConn, 2, 3 
    with oRs
        if not .bof and not .eof then
            do while not .eof
                s= s & toUnicode(.fields(mailSubject))
            .movenext
            loop
        end if
    end with

	pSendTemplateSubject = s
	
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
		
end function
private function pGetMailItemsByTemplateID()
    populateMailObject()
    Dim objConn, oRs, strSql, mailItem, s     
     mailItem = "mail_item_" & customerlang()
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=1     '3:adModeReadWrite 1:adModeRead
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    
    select case mail_template_id()
        case 14
            Dim objPdf : set objPdf = new sysPdf
                Attachment = objPdf.AttachPdfBlanco() & "|"
            set objPdf = nothing
    end select
    
    set oRs=Server.CreateObject("ADODB.Recordset")
    
    strSql = "select " & mailItem & " from [dbo].[vw_GetMailTemplate] WHERE mail_template_id=" & mail_template_id() & " order by mail_item_order"    

    oRs.open strSql, ObjConn, 2, 3 
    with oRs
        if not .bof and not .eof then
            do while not .eof
                s= s & .fields(mailItem)
            .movenext
            loop
        end if
    end with

    pGetMailItemsByTemplateID = Attachment & s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function


private function formatEuroDate(sdate)
    dim dd, mm, yy
        yy = left(sdate, 4)
        dd = right(sdate, 2)
        mm = mid(sdate,6,2)
    formatEuroDate = dd & "/" & mm & "/" & yy
end function

    private Sub cleanupObjects()
        Set objMailer = Nothing
	    Set objConfig = Nothing
        Set objVCal = nothing
        Set objContact = nothing
        Set objConn = nothing
    end sub

    public sub Class_Terminate()
        cleanupObjects()    
    end sub
   
end class    
%>