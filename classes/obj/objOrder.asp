<%
        
class clsObjOrder
'object data 

dim m_order_list_token_id, m_item_package, m_item_alert, m_item_mobileApp, m_item_intRep, m_item_Cymanagement
dim m_order_company_id, m_order_company_vat, m_order_contact_id, m_order_event_id, m_order_user_id, m_trusted
dim m_order_item_alert_price, m_order_item_alert_qty  
dim m_order_item_mobileApp_price, m_order_item_mobileApp_qty
dim m_order_item_intRep_price, m_order_item_intRep_qty
    dim m_isbestelbon

    property get order_list_token_id() : order_list_token_id = m_order_list_token_id : end property 
    property let order_list_token_id(varval) : m_order_list_token_id = varval : end property

    property get item_package() : item_package = m_item_package : end property 
    property let item_package(varval) : m_item_package = varval : end property
    
    property get item_alert() : item_alert = m_item_alert : end property 
    property let item_alert(varval) : m_item_alert = varval : end property
    
    property get item_mobileApp() : item_mobileApp = m_item_mobileApp : end property 
    property let item_mobileApp(varval) : m_item_mobileApp = varval : end property
    
    property get item_intRep() : item_intRep = m_item_intRep : end property 
    property let item_intRep(varval) : m_item_intRep = varval : end property
    
    property get item_Cymanagement() : item_Cymanagement = m_item_Cymanagement : end property 
    property let item_Cymanagement(varval) : m_item_Cymanagement = varval : end property

    property get order_company_id() : order_company_id = m_order_company_id : end property 
    property let order_company_id(varval) : m_order_company_id = varval : end property

    property get order_company_vat() : order_company_vat = m_order_company_vat : end property 
    property let order_company_vat(varval) : m_order_company_vat = varval : end property

    
    property get order_contact_id() : order_contact_id = m_order_contact_id : end property 
    property let order_contact_id(varval) : m_order_contact_id = varval : end property
    
    property get order_event_id() : order_event_id = m_order_event_id : end property 
    property let order_event_id(varval) : m_order_event_id = varval : end property
 
    property get order_user_id() : order_user_id = m_order_user_id : end property 
    property let order_user_id(varval) : m_order_user_id = varval : end property

    property get order_item_alert_price() : order_item_alert_price = m_order_item_alert_price : end property 
    property let order_item_alert_price(varval) : m_order_item_alert_price = varval : end property

    property get order_item_alert_qty() : order_item_alert_qty = m_order_item_alert_qty : end property 
    property let order_item_alert_qty(varval) : m_order_item_alert_qty = varval : end property
 
    property get order_item_mobileApp_price() : order_item_mobileApp_price = m_order_item_mobileApp_price : end property 
    property let order_item_mobileApp_price(varval) : m_order_item_mobileApp_price = varval : end property

    property get order_item_mobileApp_qty() : order_item_mobileApp_qty = m_order_item_mobileApp_qty : end property 
    property let order_item_mobileApp_qty(varval) : m_order_item_mobileApp_qty = varval : end property
 
    property get order_item_intRep_price() : order_item_intRep_price = m_order_item_intRep_price : end property 
    property let order_item_intRep_price(varval) : m_order_item_intRep_price = varval : end property

    property get order_item_intRep_qty() : order_item_intRep_qty = m_order_item_intRep_qty : end property 
    property let order_item_intRep_qty(varval) : m_order_item_intRep_qty = varval : end property

    property get isbestelbon() : isbestelbon = m_isbestelbon : end property 
    property let isbestelbon(varval) : m_isbestelbon = varval : end property

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property
    
    Public Sub populateOrderObject()
        m_order_list_token_id = request("order_list_token_id")
        m_item_package = request("item_package") 
        m_item_alert = request("item_alert") 
        m_item_mobileApp = request("item_mobileApp") 
        m_item_intRep = request("item_intRep") 
        m_item_Cymanagement = request("item_Cymanagement") 
        m_order_company_id = request("order_company_id")
        m_order_company_vat = request("order_company_vat")
        m_order_contact_id = request("order_contact_id") 
        m_order_event_id = request("order_event_id") 
        m_order_user_id = request("order_user_id")
        m_order_item_alert_price = request("order_item_alert_price")
        m_order_item_alert_qty = request("order_item_alert_qty")
        m_order_item_mobileApp_price = request("order_item_mobileApp_price")
        m_order_item_mobileApp_qty = request("order_item_mobileApp_qty")
        m_order_item_intRep_price = request("order_item_intRep_price")
        m_order_item_intRep_qty = request("order_item_intRep_qty")
        m_trusted = request("trusted")
        m_isbestelbon = request("isbestelbon")
    end sub
    
        Public Sub populateOrderObjectDummy()
            item_Cymanagement= "14"
            item_alert= "-1"
            item_intRep= "-1"
            item_mobileApp= "-1"
            item_package= "43"
            order_company_id = "388"
            order_company_vat = "439731979"
            order_contact_id= "13"
            order_event_id= "233"
            order_list_token_id= "1163141441917"
            order_user_id = 1 
            trusted = 1
        end sub


     
 end class     
%>