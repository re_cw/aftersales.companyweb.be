<%


class clsObjItem

'items table    
Dim m_item_id, m_item_parent_id, m_item_category_id, m_item_name_FR, m_item_name_NL, m_item_price, m_item_promoprice
Dim m_item_ispromo, m_item_order, m_item_enabled, m_item_longdesc_FR, m_item_longdesc_NL
Dim m_item_qty

'items categories table
Dim m_item_category_name, m_item_category_order

Dim m_trusted

    property get item_id() : item_id = m_item_id : end property 
    property let item_id(varval) : m_item_id = varval : end property

    property get item_parent_id() : item_parent_id = m_item_parent_id : end property 
    property let item_parent_id(varval) : m_item_parent_id = varval : end property

    property get item_category_id() : item_category_id = m_item_category_id : end property 
    property let item_category_id(varval) : m_item_category_id = varval : end property

    property get item_name_FR() : item_name_FR = m_item_name_FR : end property 
    property let item_name_FR(varval) : m_item_name_FR = varval : end property

    property get item_name_NL() : item_name_NL = m_item_name_NL : end property 
    property let item_name_NL(varval) : m_item_name_NL = varval : end property

    property get item_price() : item_price = m_item_price : end property 
    property let item_price(varval) : m_item_price = varval : end property

    property get item_promoprice() : item_promoprice = m_item_promoprice : end property 
    property let item_promoprice(varval) : m_item_promoprice = varval : end property

    property get item_ispromo() : item_ispromo = m_item_ispromo : end property 
    property let item_ispromo(varval) : m_item_ispromo = varval : end property

    property get item_order() : item_order = m_item_order : end property 
    property let item_order(varval) : m_item_order = varval : end property

    property get item_enabled() : item_enabled = m_item_enabled : end property 
    property let item_enabled(varval) : m_item_enabled = varval : end property

    property get item_longdesc_FR() : item_longdesc_FR = m_item_longdesc_FR : end property 
    property let item_longdesc_FR(varval) : m_item_longdesc_FR = varval : end property

    property get item_longdesc_NL() : item_longdesc_NL = m_item_longdesc_NL : end property 
    property let item_longdesc_NL(varval) : m_item_longdesc_NL = varval : end property

    property get item_category_name() : item_category_name = m_item_category_name : end property 
    property let item_category_name(varval) : m_item_category_name = varval : end property

    property get item_category_order() : item_category_order = m_item_category_order : end property 
    property let item_category_order(varval) : m_item_category_order = varval : end property
    
    property get item_qty() : item_qty = m_item_qty : end property 
    property let item_qty(varval) : m_item_qty = varval : end property

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property


    public Sub populateItemObject()
        'used in conjunction with ajax requests
        m_item_id = request("item_id") 
        m_item_parent_id = request("item_parent_id") 
        m_item_category_id = request("item_category_id") 
        m_item_name_FR = request("item_name_FR") 
        m_item_name_NL = request("item_name_NL") 
        m_item_qty = request("item_qty") 
        m_item_price = request("item_price") 
        m_item_promoprice = request("item_promoprice") 
        m_item_ispromo = request("item_ispromo") 
        m_item_order = request("item_order") 
        m_item_enabled = request("item_enabled") 
        m_item_longdesc_FR = request("item_longdesc_FR") 
        m_item_longdesc_NL = request("item_longdesc_NL")
        m_item_category_name = request("item_category_name")  
        m_item_category_order = request("item_category_order") 
    end sub


end class


%>
