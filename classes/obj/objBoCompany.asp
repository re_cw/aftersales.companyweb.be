<%

class clsObjBoCompany

dim m_recordcount, m_euroDate, m_event_id
Dim m_HBC_ID,m_HBC_Firmvat,m_HBC_FirmName,m_HBC_ContactName,m_HBC_Street,m_HBC_HouseNr,m_HBC_BusNr,m_HBC_Postcode
Dim m_HBC_gemeente,m_HBC_fax,m_HBC_tel,m_HBC_gsm,m_HBC_contactemail,m_HBC_LastUpdate,m_HBC_LastUpdateUser,m_HBC_CreationDate
Dim m_HBC_Nota,m_HBC_HBT_ID,m_HBC_origin,m_HBC_intLang,m_HBC_ExtCode,m_HBC_ExtUID,m_HBC_ConcurrentID,m_HBC_KredietVerzID
Dim m_HBC_DatamarketID,m_HBC_BetaalTermijn,m_HBC_website,m_HBC_Betalingservaring,m_HBC_CompleteBlock,m_HBC_jurvormid
Dim m_HBC_CountryCode,m_HBC_EindeContract,m_HBC_HBG_ID,m_HBC_CanRetrial,m_HBC_ReadContractBE,m_HBC_OvernameConcurrentID
Dim m_HBC_OvernameAboConc,m_HBC_Opzeg,m_HBC_Opzegdatum,m_HBC_Mailing, m_company_id

    property get recordcount() : recordcount = m_recordcount : end property
    property let recordcount(varval) : m_recordcount = varval : end property

    property get euroDate() : euroDate = m_euroDate : end property
    property let euroDate(varval) : m_euroDate = varval : end property
    
    property get event_id() : event_id = m_event_id : end property
	property let event_id(varval) : m_event_id = varval : end property

    property get HBC_ID() : HBC_ID = m_HBC_ID : end property
	property let HBC_ID(varval) : m_HBC_ID = varval : end property
    
    property get HBC_Firmvat() : HBC_Firmvat = m_HBC_Firmvat : end property
    property let HBC_Firmvat(varval) : m_HBC_Firmvat = varval : end property 

    property get HBC_FirmName() : HBC_FirmName = m_HBC_FirmName : end property
	property let HBC_FirmName(varval) : m_HBC_FirmName = varval : end property
    
    property get HBC_ContactName() : HBC_ContactName = m_HBC_ContactName : end property
	property let HBC_ContactName(varval) : m_HBC_ContactName = varval : end property
    
    property get HBC_Street() : HBC_Street = m_HBC_Street : end property
	property let HBC_Street(varval) : m_HBC_Street = varval : end property

    property get HBC_HouseNr() : HBC_HouseNr = m_HBC_HouseNr : end property
	property let HBC_HouseNr(varval) : m_HBC_HouseNr = varval : end property
    
    property get HBC_BusNr() : HBC_BusNr = m_HBC_BusNr : end property
	property let HBC_BusNr(varval) : m_HBC_BusNr = varval : end property

    property get HBC_Postcode() : HBC_Postcode = m_HBC_Postcode : end property
	property let HBC_Postcode(varval) : m_HBC_Postcode = varval : end property

    property get HBC_gemeente() : HBC_gemeente = m_HBC_gemeente : end property
	property let HBC_gemeente(varval) : m_HBC_gemeente = varval : end property
    
    property get HBC_fax() : HBC_fax = m_HBC_fax : end property
	property let HBC_fax(varval) : m_HBC_fax = varval : end property
    
    property get HBC_tel() :HBC_tel = m_HBC_tel : end property
	property let HBC_tel(varval) : m_HBC_tel = varval : end property

    property get HBC_gsm() :HBC_gsm = m_HBC_gsm : end property
	property let HBC_gsm(varval) : m_HBC_gsm = varval : end property
    
    property get HBC_contactemail() :HBC_contactemail = m_HBC_contactemail : end property
	property let HBC_contactemail(varval) : m_HBC_contactemail = varval : end property
    
    property get HBC_LastUpdate() :HBC_LastUpdate = m_HBC_LastUpdate : end property
    property let HBC_LastUpdate(varval) : m_HBC_LastUpdate = varval : end property
    
	property get HBC_LastUpdateUser() :HBC_LastUpdateUser = m_HBC_LastUpdateUser : end property
	property let HBC_LastUpdateUser(varval) : m_HBC_LastUpdateUser = varval : end property
  
    property get HBC_CreationDate() : HBC_CreationDate = m_HBC_CreationDate : end property
	property let HBC_CreationDate(varval) : m_HBC_CreationDate = varval : end property
  
    property get HBC_Nota() : HBC_Nota = m_HBC_Nota : end property
	property let HBC_Nota(varval) : m_HBC_Nota = varval : end property
    
    property get HBC_HBT_ID() :HBC_HBT_ID = m_HBC_HBT_ID : end property
	property let HBC_HBT_ID(varval) : m_HBC_HBT_ID = varval : end property
    
    property get HBC_origin() :HBC_origin = m_HBC_origin : end property
	property let HBC_origin(varval) : m_HBC_origin = varval : end property
    
    property get HBC_intLang() :HBC_intLang = m_HBC_intLang : end property
	property let HBC_intLang(varval) : m_HBC_intLang = varval : end property
    
    property get HBC_ExtCode() :HBC_ExtCode = m_HBC_ExtCode : end property
	property let HBC_ExtCode(varval) : m_HBC_ExtCode = varval : end property
    
    property get HBC_ExtUID() : HBC_ExtUID = m_HBC_ExtUID : end property
	property let HBC_ExtUID(varval) : m_HBC_ExtUID = varval : end property
    
    property get HBC_ConcurrentID() :HBC_ConcurrentID = m_HBC_ConcurrentID : end property
	property let HBC_ConcurrentID(varval) : m_HBC_ConcurrentID = varval : end property
    
    property get HBC_KredietVerzID() :HBC_KredietVerzID = m_HBC_KredietVerzID : end property
	property let HBC_KredietVerzID(varval) : m_HBC_KredietVerzID = varval : end property
        
    property get HBC_DatamarketID() : HBC_DatamarketID = m_HBC_DatamarketID : end property
	property let HBC_DatamarketID(varval) : m_HBC_DatamarketID = varval : end property
    
    property get HBC_BetaalTermijn() :HBC_BetaalTermijn = m_HBC_BetaalTermijn : end property
	property let HBC_BetaalTermijn(varval) : m_HBC_BetaalTermijn = varval : end property
    
    property get HBC_website() :HBC_website = m_HBC_website : end property
	property let HBC_website(varval) : m_HBC_website = varval : end property
    
    property get HBC_Betalingservaring() :HBC_Betalingservaring = m_HBC_Betalingservaring : end property
	property let HBC_Betalingservaring(varval) : m_HBC_Betalingservaring = varval : end property
    
    property get HBC_CompleteBlock() :HBC_CompleteBlock = m_HBC_CompleteBlock : end property
	property let HBC_CompleteBlock(varval) : m_HBC_CompleteBlock = varval : end property
    
    property get hbc_jurvormid() :hbc_jurvormid = m_hbc_jurvormid : end property
	property let hbc_jurvormid(varval) : m_hbc_jurvormid = varval : end property
    
    property get HBC_CountryCode() :HBC_CountryCode = m_HBC_CountryCode : end property
	property let HBC_CountryCode(varval) : m_HBC_CountryCode = varval : end property
    
    property get HBC_EindeContract() :HBC_EindeContract = m_HBC_EindeContract : end property
	property let HBC_EindeContract(varval) : m_HBC_EindeContract = varval : end property
    
    property get HBC_HBG_ID() :HBC_HBG_ID = m_HBC_HBG_ID : end property
	property let HBC_HBG_ID(varval) : m_HBC_HBG_ID = varval : end property
    
    property get hbc_CanRetrial() :hbc_CanRetrial = m_hbc_CanRetrial : end property
	property let hbc_CanRetrial(varval) : m_hbc_CanRetrial = varval : end property
    
    property get HBC_ReadContractBE() :HBC_ReadContractBE = m_HBC_ReadContractBE : end property
	property let HBC_ReadContractBE(varval) : m_HBC_ReadContractBE = varval : end property
    
    property get HBC_OvernameConcurrentID() :HBC_OvernameConcurrentID = m_HBC_OvernameConcurrentID : end property
	property let HBC_OvernameConcurrentID(varval) : m_HBC_OvernameConcurrentID = varval : end property
    
    property get HBC_OvernameAboConc() :HBC_OvernameAboConc = m_HBC_OvernameAboConc : end property
	property let HBC_OvernameAboConc(varval) : m_HBC_OvernameAboConc = varval : end property
    
    property get HBC_Opzeg() :HBC_Opzeg = m_HBC_Opzeg : end property
	property let HBC_Opzeg(varval) : m_HBC_Opzeg = varval : end property
    
    property get HBC_Opzegdatum() :HBC_Opzegdatum = m_HBC_Opzegdatum : end property
	property let HBC_Opzegdatum(varval) : m_HBC_Opzegdatum = varval : end property
    
    property get HBC_Mailing() :HBC_Mailing = m_HBC_Mailing : end property
	property let HBC_Mailing(varval) : m_HBC_Mailing = varval : end property
    
    property get company_id() :company_id = m_company_id : end property
	property let company_id(varval) : m_company_id = varval : end property


    Public Sub populateBoCompanyObject()
      m_company_id=request("company_id")
      m_event_id=request("event_id")
      m_HBC_ID=request("HBC_ID")
      m_HBC_Firmvat=request("HBC_Firmvat")
      m_HBC_FirmName=request("HBC_FirmName")
      m_HBC_ContactName=request("HBC_ContactName")
      m_HBC_Street=request("HBC_Street")
      m_HBC_HouseNr=request("HBC_HouseNr")
      m_HBC_BusNr=request("HBC_BusNr")
      m_HBC_Postcode=request("HBC_Postcode")
      m_HBC_gemeente=request("HBC_gemeente")
      m_HBC_fax=request("HBC_fax")
      m_HBC_tel=request("HBC_tel")
      m_HBC_gsm=request("HBC_gsm")
      m_HBC_contactemail=request("HBC_contactemail")
      m_HBC_LastUpdate=request("HBC_LastUpdate")
      m_HBC_LastUpdateUser=request("HBC_LastUpdateUser")
      m_HBC_CreationDate=request("HBC_CreationDate")
      m_HBC_Nota=request("HBC_Nota")
      m_HBC_HBT_ID=request("HBC_HBT_ID")
      m_HBC_origin=request("HBC_origin")
      m_HBC_intLang=request("HBC_intLang")
      m_HBC_ExtCode=request("HBC_ExtCode")
      m_HBC_ExtUID=request("HBC_ExtUID")
      m_HBC_ConcurrentID=request("HBC_ConcurrentID")
      m_HBC_KredietVerzID=request("HBC_KredietVerzID")
      m_HBC_DatamarketID=request("HBC_DatamarketID")
      m_HBC_BetaalTermijn=request("HBC_BetaalTermijn")
      m_HBC_website=request("HBC_website")
      m_HBC_Betalingservaring=request("HBC_Betalingservaring")
      m_HBC_CompleteBlock=request("HBC_CompleteBlock")
      m_hbc_jurvormid=request("hbc_jurvormid")
      m_HBC_CountryCode=request("HBC_CountryCode")
      m_HBC_EindeContract=request("HBC_EindeContract")
      m_HBC_HBG_ID=request("HBC_HBG_ID")
      m_hbc_CanRetrial=request("hbc_CanRetrial")
      m_HBC_ReadContractBE=request("HBC_ReadContractBE")
      m_HBC_OvernameConcurrentID=request("HBC_OvernameConcurrentID")
      m_HBC_OvernameAboConc=request("HBC_OvernameAboConc")
      m_HBC_Opzeg=request("HBC_Opzeg")
      m_HBC_Opzegdatum=request("HBC_Opzegdatum")
      m_HBC_Mailing=request("HBC_Mailing")
    end sub


    Public Sub responseWriteBoCompanyObject()
      response.write "company_id : " &  company_id & "<br>"
      response.write "event_id : " &  event_id & "<br>"
      response.write "HBC_ID : " & HBC_ID & "<br>"
      response.write "HBC_Firmvat : " & HBC_Firmvat & "<br>"
      response.write "HBC_FirmName : " & HBC_FirmName & "<br>"
      response.write "HBC_ContactName : " & HBC_ContactName & "<br>"
      response.write "HBC_Street : " & HBC_Street & "<br>"
      response.write "HBC_HouseNr : " & HBC_HouseNr & "<br>"
      response.write "HBC_BusNr : " & HBC_BusNr & "<br>"
      response.write "HBC_Postcode : " & HBC_Postcode & "<br>"
      response.write "HBC_gemeente : " & HBC_gemeente & "<br>"
      response.write "HBC_fax : " & HBC_fax & "<br>"
      response.write "HBC_tel : " & HBC_tel & "<br>"
      response.write "HBC_gsm : " & HBC_gsm & "<br>"
      response.write "HBC_contactemail : " & HBC_contactemail & "<br>"
      response.write "HBC_LastUpdate : " & HBC_LastUpdate & "<br>"
      response.write "HBC_LastUpdateUser : " & HBC_LastUpdateUser & "<br>"
      response.write "HBC_CreationDate : " & HBC_CreationDate & "<br>"
      response.write "HBC_Nota : " & HBC_Nota & "<br>"
      response.write "HBC_HBT_ID : " & HBC_HBT_ID & "<br>"
      response.write "HBC_origin : " & HBC_origin & "<br>"
      response.write "HBC_intLang : " & HBC_intLang & "<br>"
      response.write "HBC_ExtCode : " & HBC_ExtCode & "<br>"
      response.write "HBC_ExtUID : " & HBC_ExtUID & "<br>"
      response.write "HBC_ConcurrentID : " & HBC_ConcurrentID & "<br>"
      response.write "HBC_KredietVerzID : " & HBC_KredietVerzID & "<br>"
      response.write "HBC_DatamarketID : " & HBC_DatamarketID & "<br>"
      response.write "HBC_BetaalTermijn : " & HBC_BetaalTermijn & "<br>"
      response.write "HBC_website : " & HBC_website & "<br>"
      response.write "HBC_Betalingservaring : " & HBC_Betalingservaring & "<br>"
      response.write "HBC_CompleteBlock : " & HBC_CompleteBlock & "<br>"
      response.write "hbc_jurvormid : " & hbc_jurvormid & "<br>"
      response.write "HBC_CountryCode : " & HBC_CountryCode & "<br>"
      response.write "HBC_EindeContract : " & HBC_EindeContract & "<br>"
      response.write "HBC_HBG_ID : " & HBC_HBG_ID & "<br>"
      response.write "HBC_HBG_ID : " & hbc_CanRetrial & "<br>"
      response.write "HBC_ReadContractBE : " & HBC_ReadContractBE & "<br>"
      response.write "HBC_OvernameConcurrentID : " & HBC_OvernameConcurrentID & "<br>"
      response.write "HBC_OvernameAboConc : " & HBC_OvernameAboConc & "<br>"
      response.write "HBC_Opzeg : " & HBC_Opzeg & "<br>"
      response.write "HBC_Opzegdatum : " & HBC_Opzegdatum & "<br>"
      response.write "HBC_Mailing : " & HBC_Mailing & "<br>"
    end sub
end class
%>

   