<%

class clsObjCompanyAddress


'object data    
Dim p_company_address_street, p_company_address_number, p_company_address_postcode, p_company_address_localite
Dim p_company_address_boxnumber, p_company_address_floor, p_company_address_id
	
	property get company_address_id() : company_address_id = p_company_address_id : end property
	property let company_address_id(varval) : p_company_address_id = varval : end property
	
    property get company_address_street() : company_address_street = p_company_address_street : end property 
    property let company_address_street(varval) : p_company_address_street = varval : end property

    property get company_address_number() : company_address_number = p_company_address_number : end property 
    property let company_address_number(varval) : p_company_address_number = varval : end property

    property get company_address_boxnumber() : company_address_boxnumber = p_company_address_boxnumber : end property 
    property let company_address_boxnumber(varval) : p_company_address_boxnumber = varval : end property

    property get company_address_floor() : company_address_floor = p_company_address_floor : end property 
    property let company_address_floor(varval) : p_company_address_floor = varval : end property

    property get company_address_postcode() : company_address_postcode = p_company_address_postcode : end property 
    property let company_address_postcode(varval) : p_company_address_postcode = varval : end property

    property get company_address_localite() : company_address_localite = p_company_address_localite : end property 
    property let company_address_localite(varval) : p_company_address_localite = varval : end property

    property get trusted() : trusted = p_trusted : end property 
    property let trusted(varval) : p_trusted = varval : end property

    property get user_id() : user_id = p_user_id : end property 
    property let user_id(varval) : p_user_id = varval : end property


        Public Sub populateCompanyAddressObject()

            p_company_address_street = request("company_address_street")
            p_company_address_number = request("company_address_number")
            p_company_address_postcode = request("company_address_postcode")
            p_company_address_localite = request("company_address_localite") 
            p_company_address_boxnumber = request("company_address_boxnumber") 
            p_company_address_floor = request("company_address_floor")
            p_company_address_id = request("company_address_id")

    end sub


end class
       
    %>