<%

class clsObjcontact

Dim m_contact_id, m_contact_HBU_ID, m_contact_vat, m_contact_type, m_contact_lang_id, m_contact_firstname, m_contact_lastname
Dim m_contact_tel, m_contact_mobile, m_contact_email, m_contact_lang_name, m_contact_lang_radical
Dim m_contact_company_id, m_contact_type_id, m_contact_gender
Dim m_contact_title, m_contact_title_id, m_trusted, m_company_address_link_id, m_ct_company_address_id

    property get contact_id() : contact_id = m_contact_id : end property 
    property let contact_id(varval) : m_contact_id = varval : end property

        property get contact_HBU_ID() : contact_HBU_ID = m_contact_HBU_ID : end property
    property let contact_HBU_ID(varval) : m_contact_HBU_ID = varval : end property

    property get contact_vat() : contact_vat = m_contact_vat : end property 
    property let contact_vat(varval) : m_contact_vat = varval : end property

    property get contact_type() : contact_type = m_contact_type : end property 
    property let contact_type(varval) : m_contact_type = varval : end property

    property get contact_type_id() : contact_type_id = m_contact_type_id : end property 
    property let contact_type_id(varval) : m_contact_type_id = varval : end property

    property get contact_title_id() : contact_title_id = m_contact_title_id : end property 
    property let contact_title_id(varval) : m_contact_title_id = varval : end property

    property get contact_lang_id() : contact_lang_id = m_contact_lang_id : end property 
    property let contact_lang_id(varval) : m_contact_lang_id = varval : end property

    property get contact_gender() : contact_gender = m_contact_gender : end property 
    property let contact_gender(varval) : m_contact_gender = varval : end property

    property get contact_lang_name() : contact_lang_name = m_contact_lang_name : end property 
    property let contact_lang_name(varval) : m_contact_lang_name = varval : end property

    property get contact_lang_radical() : contact_lang_radical = m_contact_lang_radical : end property 
    property let contact_lang_radical(varval) : m_contact_lang_radical = varval : end property

    property get contact_firstname() : contact_firstname = m_contact_firstname : end property 
    property let contact_firstname(varval) : m_contact_firstname = varval : end property

    property get contact_lastname() : contact_lastname = m_contact_lastname : end property 
    property let contact_lastname(varval) : m_contact_lastname = varval : end property

    property get contact_tel() : contact_tel = m_contact_tel : end property 
    property let contact_tel(varval) : m_contact_tel = varval : end property

    property get contact_mobile() : contact_mobile = m_contact_mobile : end property 
    property let contact_mobile(varval) : m_contact_mobile = varval : end property

    property get contact_email() : contact_email = m_contact_email : end property 
    property let contact_email(varval) : m_contact_email = varval : end property

    property get contact_title() : contact_title = m_contact_title : end property 
    property let contact_title(varval) : m_contact_title = varval : end property

    property get contact_company_id() : contact_company_id = m_contact_company_id : end property 
    property let contact_company_id(varval) : m_contact_company_id = varval : end property

    property get company_address_link_id() : company_address_link_id = m_company_address_link_id : end property 
    property let company_address_link_id(varval) : m_company_address_link_id = varval : end property

    property get ct_company_address_id() : ct_company_address_id = m_ct_company_address_id : end property 
    property let ct_company_address_id(varval) : m_ct_company_address_id = varval : end property

    property get trusted() : trusted = m_trusted : end property 
    property let trusted(varval) : m_trusted = varval : end property

    public Sub populateContactObject()
    'used in conjunction with ajax requests
        m_contact_id = request("contact_id")
        m_contact_HBU_ID= request("contact_HBU_ID")
        m_contact_vat = request("contact_vat")
        m_contact_type = request("contact_type")
        m_contact_type_id = request("contact_type_id")
        m_contact_title_id = request("contact_title_id")
        m_contact_gender = request("contact_gender")
        m_contact_lang_id = request("contact_lang_id")
        m_contact_lang_radical = request("contact_lang_radical")
        m_contact_lang_name = request("contact_lang_name")
        m_contact_firstname = request("contact_firstname")
        m_contact_lastname = request("contact_lastname")
        m_contact_tel = request("contact_tel")
        m_contact_mobile = request("contact_mobile")
        m_contact_email = request("contact_email")
        m_contact_title = request("contact_title")
        m_contact_type = request("contact_type")
        m_contact_company_id = request("contact_company_id")        
        m_company_address_link_id = request("company_address_link_id")
        m_ct_company_address_id = request("company_address_id")
        m_trusted = request("trusted")
    end sub
     
end class            
%>