﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objContact.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objStContact, obj

    Set objStContact = new sysContact
    Set obj = new  clsObjcontact

    select case request("a")
        case "JSONGetStContactDetails"
            objStContact.JSONGetStContactDetails()
        case "GetComboContactTypes"
            objStContact.GetComboContactTypes()
        case "GetComboContactTitles"
            objStContact.GetComboContactTitles()
        case "JSONGetStContactEmails"
            objStContact.JSONGetStContactEmails()
        case "JSONGetStContactDetailsFromHbcId"
            objStContact.JSONGetStContactDetailsFromHbcId()
        case "JSONGetContact"
            objStcontact.JSONGetContact()
		case "JSONGetContactFromCompany"
			objStContact.JSONGetContactFromCompany
        case "AddContact"
            objStContact.AddContact()
        case "EditContact"
            objStContact.UpdateContact()
        case "deleteContact" 
               objStContact.deleteContact()
        case else
    end select

class sysContact

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
          
        property get DevConnString()
            DevConnString = sConnStrDev
        end property
   
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get event_id()
        event_id=trim(request.querystring("event_id"))
        if len(event_id) = 0 then event_id = 0
    end property

    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property
    property get hbc_id()
        hbc_id = trim(request.QueryString("hbc_id"))
        if len(hbc_id) = 0 then hbc_id = -1
    end property
    property get contact_id()
        contact_id=trim(request.querystring("contact_id"))
        if len(contact_id) = 0 then contact_id = -1
    end property
    public function JSONGetStContactDetailsFromHbcId()
        Response.ContentType = "application/json" : response.write pJSONGetStContactDetailsFromHbcId()
    end function
    public function JSONGetStContactDetails() 
        'response.write server.htmlencode(pJSONGetStCompanyDetails()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetStContactDetails()
    end function
	public function JSONGetContactFromCompany()
		Response.ContentType = "application/json" : response.write pJSONGetContactFromCompany()
	end function
    public function JSONGetStContactEmails() 
        'response.write server.htmlencode(pJSONGetStCompanyDetails()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetStContactEmails()
    end function

    public function GetComboContactTypes()
        Response.ContentType = "application/json" : response.write pGetComboContactTypes()
    end function

    public function GetComboContactTitles()
        Response.ContentType = "application/json" : response.write pGetComboContactTitles()
    end function
    public function JSONGetContact()
        Response.ContentType = "application/json" : response.write pJSONGetContact()
    end function
    public function AddContact()
    AddContact = pAddContact()
    end function

    public function UpdateContact()
        UpdateContact = pUpdateContact()
    end function

    public function deleteContact()
        deleteContact = pdeleteContact()
    end function


    public function LinkContactToCompanyAddress(obj)
        LinkContactToCompanyAddress = pLinkContactToCompanyAddress(obj)
    end function 

'<start region PRIVATE METHODS & FUNCTIONS>
        private function pJSONGetStContactDetailsFromHbcId()
                Dim objConn, oRs, strSql, s
            
            strSql = " select C.company_id, CO.contact_id, CO.contact_HBU_ID, CO.contact_lang_id, "
            strSql = strSql & " CO.contact_title_id,CAC.contact_type_id,CO.contact_gender, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN TI.title_name_FR WHEN 2 THEN TI.title_name_NL ELSE TI.title_name_EN END AS [contact_title],"
            strSql = strSql & " CO.contact_firstname, CO.contact_lastname, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN CT.contact_type_name_FR WHEN 2 THEN CT.contact_type_name_NL ELSE CT.contact_type_name_EN END AS [contact_type], "
            strSql = strSql & " CO.contact_mobile, CO.contact_email, LA.lang_radical, LA.lang_name, "
            strSql = strSql & " CAC.link_id as company_address_link_id, CAC.company_address_id, CO.contact_tel "
            strSql = strSql & " from companies C "
            strSql = strSql & " inner join company_address CA on C.company_id = CA.company_id "
            strSql = strSql & " inner join link_company_address_contacts CAC on CA.company_address_id = CAC.company_address_id "
            strSql = strSql & " inner join contacts CO on CAC.contact_id = CO.contact_id "
            strSql = strSql & " left join ref_lang LA on CO.contact_lang_id = LA.lang_id "
            strSql = strSql & " left join ref_titles TI on CO.contact_title_id = TI.title_id "
            strSql = strSql & " left join ref_contact_types CT on CAC.contact_type_id = CT.contact_type_id "
            strSql = strSql & " where C.company_hbc_id = " & hbc_id()

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn

                s = "["
				s = s & RStoJSON(oRs)
				s = s & "]"
            oRs.Close : set oRs = nothing
            objConn.close : set objConn = nothing

            pJSONGetStContactDetailsFromHbcId = s
        end function
         private function pGetComboContactTitles()       
            Dim objConn, oRs, strSql, ContactTitleName, s
            ContactTitleName = "title_name_" & UserLang() 
            
            strSql = "SELECT title_id as value, title_name_" & UserLang() & " as text FROM dbo.[ref_titles]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                            s= "{""title"" :["
							s = s & RStoJSON(oRs)
							
                        s = s & "]}"
            pGetComboContactTitles = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function


     private function pGetComboContactTypes()       
            Dim objConn, oRs, strSql, ContactTypeName, s
            ContactTypeName = "contact_type_name_" & UserLang() 
            
            strSql = "SELECT contact_type_id as value, contact_type_name_" & UserLang() & " as text FROM dbo.[ref_contact_types]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             s= "{""type"" :["
			 s = s & RStoJSON(oRs)
			 s = s & "]}"
            pGetComboContactTypes = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

	private function pJSONGetContactFromCompany()
	    Dim objConn, oRs, strSql, s
            strSql = " select C.company_id, C.company_vat, C.company_hbc_id, CO.contact_id, CO.contact_HBU_ID, CO.contact_lang_id, "
            strSql = strSql & " CO.contact_title_id,CAC.contact_type_id,CO.contact_gender, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN TI.title_name_FR WHEN 2 THEN TI.title_name_NL ELSE TI.title_name_EN END AS [contact_title],"
            strSql = strSql & " CO.contact_firstname, CO.contact_lastname, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN CT.contact_type_name_FR WHEN 2 THEN CT.contact_type_name_NL ELSE CT.contact_type_name_EN END AS [contact_type], "
            strSql = strSql & " CO.contact_mobile, CO.contact_email, LA.lang_radical, LA.lang_name, "
            strSql = strSql & " CAC.link_id as company_address_link_id, CAC.company_address_id, CO.contact_tel "
            strSql = strSql & " from companies C "
            strSql = strSql & " inner join company_address CA on C.company_id = CA.company_id "
            strSql = strSql & " inner join link_company_address_contacts CAC on CA.company_address_id = CAC.company_address_id "
            strSql = strSql & " inner join contacts CO on CAC.contact_id = CO.contact_id "
            strSql = strSql & " left join ref_lang LA on CO.contact_lang_id = LA.lang_id "
            strSql = strSql & " left join ref_titles TI on CO.contact_title_id = TI.title_id "
            strSql = strSql & " left join ref_contact_types CT on CAC.contact_type_id = CT.contact_type_id "
            strSql = strSql & " where CO.contact_id = " & contact_id() & " "
			strSql = strSql & " AND C.company_id = " & company_id() & " "

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
                  
				  s = RStoJSON(oRs)
				  
        oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetContactFromCompany = s
	end function
private function pJSONGetContact()
    Dim objConn, oRs, strSql, s
            strSql = " select C.company_id, C.company_vat, C.company_hbc_id, CO.contact_id, CO.contact_HBU_ID, CO.contact_lang_id, "
            strSql = strSql & " CO.contact_title_id,CAC.contact_type_id,CO.contact_gender, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN TI.title_name_FR WHEN 2 THEN TI.title_name_NL ELSE TI.title_name_EN END AS [contact_title],"
            strSql = strSql & " CO.contact_firstname, CO.contact_lastname, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN CT.contact_type_name_FR WHEN 2 THEN CT.contact_type_name_NL ELSE CT.contact_type_name_EN END AS [contact_type], "
            strSql = strSql & " CO.contact_mobile, CO.contact_email, LA.lang_radical, LA.lang_name, "
            strSql = strSql & " CAC.link_id as company_address_link_id, CAC.company_address_id, CO.contact_tel "
            strSql = strSql & " from companies C "
            strSql = strSql & " inner join company_address CA on C.company_id = CA.company_id "
            strSql = strSql & " inner join link_company_address_contacts CAC on CA.company_address_id = CAC.company_address_id "
            strSql = strSql & " inner join contacts CO on CAC.contact_id = CO.contact_id "
            strSql = strSql & " left join ref_lang LA on CO.contact_lang_id = LA.lang_id "
            strSql = strSql & " left join ref_titles TI on CO.contact_title_id = TI.title_id "
            strSql = strSql & " left join ref_contact_types CT on CAC.contact_type_id = CT.contact_type_id "
            strSql = strSql & " where CO.contact_id = " & contact_id()

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
                  
				  s = RStoJSON(oRs)
				  
        oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetContact = s
end function
private function pJSONGetStContactDetails()

            Dim objConn, oRs, strSql, s
            
            strSql = " select C.company_id, CO.contact_id, CO.contact_HBU_ID, CO.contact_lang_id, "
            strSql = strSql & " CO.contact_title_id,CAC.contact_type_id,CO.contact_gender, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN TI.title_name_FR WHEN 2 THEN TI.title_name_NL ELSE TI.title_name_EN END AS [contact_title],"
            strSql = strSql & " CO.contact_firstname, CO.contact_lastname, "
            strSql = strSql & " CASE CO.contact_lang_id WHEN 1 THEN CT.contact_type_name_FR WHEN 2 THEN CT.contact_type_name_NL ELSE CT.contact_type_name_EN END AS [contact_type], "
            strSql = strSql & " CO.contact_mobile, CO.contact_email, LA.lang_radical, LA.lang_name, "
            strSql = strSql & " CAC.link_id as company_address_link_id, CAC.company_address_id, CO.contact_tel "
            strSql = strSql & " from companies C "
            strSql = strSql & " inner join company_address CA on C.company_id = CA.company_id "
            strSql = strSql & " inner join link_company_address_contacts CAC on CA.company_address_id = CAC.company_address_id "
            strSql = strSql & " inner join contacts CO on CAC.contact_id = CO.contact_id "
            strSql = strSql & " left join ref_lang LA on CO.contact_lang_id = LA.lang_id "
            strSql = strSql & " left join ref_titles TI on CO.contact_title_id = TI.title_id "
            strSql = strSql & " left join ref_contact_types CT on CAC.contact_type_id = CT.contact_type_id "
            strSql = strSql & " where C.company_id = " & company_id()

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
                
				s= "["
				s = s & RStoJSON(oRs)
				s = s & "]"

    oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetStContactDetails = s


end function

private function pJSONGetStContactEmails()

        Dim objConn, objRS, strSql, sErrMsg, objCmd
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=1 'adModeRead
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    strSql = "dbo.Contact_GET"
    Dim a, fullLabel
            a = company_id()          

    'on error resume next
    'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@company_id", 8, 1, len(a), a)            
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
    
    ' on error resume next
                set objRs = Server.CreateObject("ADODB.Recordset")
                objRs.CursorType = 0
                objRs.lockType = 1
                set objRs = .Execute()
            end with
    s= "["
            with objRS
                    if not .bof and not .eof then
                         
                        
                        do while not .eof
                            fullLabel = trim(.fields("contact_firstname")) & " " & trim(.fields("contact_lastname")) & " (" & trim(.fields("contact_email")) & ")"
                            s=s & "{"
                                s=s & """key"":""" & .fields("contact_id") & """"
                                s=s & ",""label"":""" & fullLabel & """"
                            s=s & "},"
                        .movenext
                        loop
                        s = left(s,len(s)-1)    
                end if
           end with    
    s = s & "]"
    end if
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pJSONGetStContactEmails = s


end function


























    private sub populateDummydata()
    
    obj.contact_id=0
    obj.contact_vat=0
    obj.contact_type=0
    obj.contact_type_id=9
    obj.contact_title_id=1
    obj.contact_gender=M
    obj.contact_lang_id=1
    obj.contact_lang_radical=""
    obj.contact_lang_name=""
    obj.contact_firstname="alain"
    obj.contact_lastname="dupont"
    obj.contact_tel="1231456"
    obj.contact_mobile="123456"
    obj.contact_email="dfqsddfqs%40dsds.be"
    obj.contact_title=1
    obj.contact_company_id=389
    obj.contact_company_address_id=0
    obj.company_address_link_id=0
    obj.ct_company_address_id=389
    end sub

private function pDeleteContact()
        Dim objConn, strSql, sErrMsg
        
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    objConn.beginTrans
        strSql = "delete from [dbo].[link_company_address_contacts] where contact_id=" & contact_id
        objconn.execute strsql

        strSql = "delete from [dbo].[contacts] where contact_id=" & contact_id
        objconn.execute strsql

    if err.number <> 0 then
        objConn.rollbacktrans
    else
        objConn.CommitTrans
    end if

    pDeleteContact=sErrMsg
    set objConn=nothing


end function


private function pAddContact()
        Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd
        
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    obj.populateContactObject
    'populateDummydata

        strSql = "dbo.Contact_ADD"
        'parameters types: '5: adDouble - '8: adBSTR - '201:adLongVarChar
        if objConn.State = 1 then    
    objConn.begintrans
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@contact_lang_id", 5, 1, len(obj.contact_lang_id), obj.contact_lang_id)
                .Parameters.Append .CreateParameter("@contact_title_id", 5, 1, len(obj.contact_title_id), obj.contact_title_id)
                .Parameters.Append .CreateParameter("@contact_gender", 8, 1, len(obj.contact_gender), obj.contact_gender)
                .Parameters.Append .CreateParameter("@contact_firstname", 8, 1, len(obj.contact_firstname),  obj.contact_firstname)
                .Parameters.Append .CreateParameter("@contact_lastname", 8, 1, len(obj.contact_lastname), obj.contact_lastname)
                .Parameters.Append .CreateParameter("@contact_tel", 8, 1, len(obj.contact_tel), obj.contact_tel)
                .Parameters.Append .CreateParameter("@contact_mobile", 8, 1, len(obj.contact_mobile), obj.contact_mobile)
                .Parameters.Append .CreateParameter("@contact_email", 8, 1, len(obj.contact_email), obj.contact_email)
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(usr_id()), usr_id())
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(usr_id()),usr_id())
                .Parameters.Append .CreateParameter("@contact_id", 5, 2, len(obj.contact_id),obj.contact_id)
    
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
    sErrMsg= sErrMsg & chr(10) & chr(13) & " objRsExecute : <br><br> " &  obj.contact_firstname &  chr(10) & chr(13)  & obj.contact_lastname &   chr(10) & chr(13)  & chr(10) & chr(13)  
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
    obj.contact_id=objCmd(10)
        end with
                if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"    
            set objRs= nothing
            
        end if

            
    set objRS = Server.CreateObject("ADODB.Recordset")

    strSql = "Select top 1 * from [dbo].[link_company_address_contacts]"
    objRs.open strSql, ObjConn, 2, 3
    with objRs
        .addnew
            .fields("contact_id") = obj.contact_id
            .fields("contact_type_id") = obj.contact_type_id
            .fields("company_address_id")= obj.ct_company_address_id
            .fields("AuditDateCreated") = now()
            .fields("AuditUserCreated") = usr_id()
        .update
    end with
     if err.number <> 0 then
        objConn.rollbacktrans
    else
        objConn.CommitTrans
    end if

    pAddContact=sErrMsg
    set objConn=nothing
end function





private function pUpdateContact()
        Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd
        
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

     
    'Set obj = new  clsObjcontact
    obj.populateContactObject
    'populateDummydata

        strSql = "dbo.Contact_UPDATE_By_contact_id"
        'parameters types: '5: adDouble - '8: adBSTR - '201:adLongVarChar
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@contact_lang_id", 5, 1, len(obj.contact_lang_id), obj.contact_lang_id)
                .Parameters.Append .CreateParameter("@contact_title_id", 5, 1, len(obj.contact_title_id), obj.contact_title_id)
                .Parameters.Append .CreateParameter("@contact_gender", 8, 1, len(obj.contact_gender), obj.contact_gender)
                .Parameters.Append .CreateParameter("@contact_firstname", 8, 1, len(obj.contact_firstname),  trim(obj.contact_firstname))
                .Parameters.Append .CreateParameter("@contact_lastname", 8, 1, len(obj.contact_lastname), trim(obj.contact_lastname))
                .Parameters.Append .CreateParameter("@contact_tel", 8, 1, len(obj.contact_tel), trim(obj.contact_tel))
                .Parameters.Append .CreateParameter("@contact_mobile", 8, 1, len(obj.contact_mobile), trim(obj.contact_mobile))
                .Parameters.Append .CreateParameter("@contact_email", 8, 1, len(obj.contact_email), trim(obj.contact_email))
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(usr_id()), usr_id())
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(usr_id()),usr_id())
                .Parameters.Append .CreateParameter("@contact_id", 5, 1, len(obj.contact_id),obj.contact_id)
                .Parameters.Append .CreateParameter("@company_address_link_id", 5, 1, len(obj.company_address_link_id),obj.company_address_link_id)
    .Parameters.Append .CreateParameter("@contact_type_id", 5, 1, len(obj.contact_type_id),obj.contact_type_id)
    
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
    sErrMsg= sErrMsg & chr(10) & chr(13) & " objRsExecute : <br><br> " &  obj.contact_firstname &  chr(10) & chr(13)  & obj.contact_lastname &   chr(10) & chr(13)  & chr(10) & chr(13)  
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
    
        end with
                if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"    
            set objRs= nothing
            
        end if
    pUpdateContact=sErrMsg
end function

private function pLinkContactToCompanyAddress(obj)
       
        '*** link contact to company address
        dim objRsL, strSqlL, link_ID
        set objRSL = Server.CreateObject("ADODB.Recordset")
        strSqlL = "SELECT link_id FROM dbo.link_company_address_contacts WHERE company_address_id=" & obj.company_address_id
        strSqlL = strSqlL & " AND contact_id=" & obj.contact_id
        objRsL.open strSqlL, objConn, 0, 1
        if objRsL.bof and objRsL.eof then strSql = "dbo.link_company_address_contacts_ADD" else strSql = "dbo.link_company_address_contacts_UPDATE"
        objRsL.close
        set objRsL = nothing
        'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@company_address_id", 5, 1, len(obj.company_address_id), obj.company_address_id)
                .Parameters.Append .CreateParameter("@contact_id", 5, 1, len(obj.contact_id), obj.contact_id)
                .Parameters.Append .CreateParameter("@contact_type_id", 5, 1, len(obj.contact_type_id), obj.contact_type_id)
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(obj.user_id), obj.user_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(obj.user_id), obj.user_id)
                .Parameters.Append .CreateParameter("@link_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
        end with
           if err.number <> 0 then sErrMsg= sErrMsg & strSqlL & " " & strsql &" : " &  err.description & "<br>"
            'objRs.Close
            set objRs= nothing
            link_ID=objCmd(4)
        end if

end function



'<end region PRIVATE METHODS & FUNCTIONS>
    public sub Class_Terminate()
        set objStContact = nothing
    end sub

end class
%>
