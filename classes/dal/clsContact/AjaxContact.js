﻿

function populateObject(obj) {
    var str = '';
    str = obj.contact_firstname; obj.contact_firstname = str.trim();
    str = obj.contact_lastname; obj.contact_lastname = str.trim();
    str = obj.contact_tel; obj.contact_tel = str.trim();
    str = obj.contact_mobile; obj.contact_mobile = str.trim();
    str = obj.contact_email; obj.contact_email = str.trim();

    var params = "contact_id=" + encodeURIComponent(obj.contact_id);
    params += "&contact_HBU_ID=" + encodeURIComponent(obj.contact_HBU_ID);
    params += "&contact_vat=" + encodeURIComponent(obj.contact_vat);
    params += "&contact_type=" + encodeURIComponent(obj.contact_type);
    params += "&contact_type_id=" + encodeURIComponent(obj.contact_type_id);
    params += "&contact_title_id=" + encodeURIComponent(obj.contact_title_id);
    params += "&contact_gender=" + encodeURIComponent(obj.contact_gender);
    params += "&contact_lang_id=" + encodeURIComponent(obj.contact_lang_id);
    params += "&contact_lang_radical=" + encodeURIComponent(obj.contact_lang_radical);
    params += "&contact_lang_name=" + encodeURIComponent(obj.contact_lang_name);
    params += "&contact_firstname=" + encodeURIComponent(obj.contact_firstname);
    params += "&contact_lastname=" + encodeURIComponent(obj.contact_lastname);
    params += "&contact_tel=" + encodeURIComponent(obj.contact_tel);
    params += "&contact_mobile=" + encodeURIComponent(obj.contact_mobile);
    params += "&contact_email=" + encodeURIComponent(obj.contact_email);
    params += "&contact_title=" + encodeURIComponent(obj.contact_title);
    params += "&contact_company_id=" + encodeURIComponent(obj.contact_company_id);
    params += "&company_address_link_id=" + encodeURIComponent(obj.company_address_link_id);
    params += "&company_address_id=" + encodeURIComponent(obj.company_address_id);
    
    params += "&trusted=1";
    return params;
}

function AjaxContact_AddContact(obj) {
	var url = "/classes/dal/clscontact/clscontact.asp?a=AddContact";
	var params = populateObject(obj);
	
	$.post(url,params)
	.done(function(data){
		switch(data)
		{
			case "":
				dhtmlx.message({
					text: locale.main_page.lblContactSuccess,
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblContactSuccess);
			break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblContactFail,
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblContactFail + "\n" + http.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error adding contact",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function AjaxContact_EditContact(obj) {
	var url = "/classes/dal/clscontact/clscontact.asp?a=EditContact";
    var params = populateObject(obj);
	
	$.post(url,params)
	.done(function(data){
		switch(data)
		{
			case "":
				dhtmlx.message({
					text: locale.main_page.lblContactSuccess,
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblContactSuccess);
			break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblContactFail,
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblContactFail + "\n" + http.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error Editing Contact.",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function AjaxContact_GetContactFromCompanyPromise(contact_id, company_id)
{
	var url = "/classes/dal/clscontact/clscontact.asp";
    return $.get(url, { a: "JSONGetContactFromCompany", contact_id: contact_id, company_id: company_id })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error Getting contact.",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxContact_GetContactPromise(contact_id)
{
    var url = "/classes/dal/clscontact/clscontact.asp";
    return $.get(url, { a: "JSONGetContact", contact_id: contact_id })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error Getting contact.",
            expire: -1,
            type: "errormessage"
        });
    });
}

function AjaxContact_DeleteContact(contact_id) {
	var url = "/classes/dal/clscontact/clscontact.asp";
	$.get(url, {a:"deleteContact", contact_id: contact_id})
	.done(function(data){
		switch(data)
		{
			case "":
				dhtmlx.message({
					text: locale.main_page.lblContactSuccess,
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblContactSuccess);
			break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblContactFail,
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblContactFail + "\n" + http.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error Deleting contact.",
	        expire: -1,
	        type: "errormessage"
	    });
	});
	
}
