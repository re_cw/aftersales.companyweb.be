﻿
<!-- #include virtual ="/classes/obj/objPdf.asp" -->
<%
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1


CONST pdfKey = "arnem"
CONST pdfExt = ".pdf"
CONST pdfRadical = "bestelbon_int_"
CONST cwMainPhone = " - 02 752 17 60"

class sysPdf

Dim pdf, objFSO
Dim pdfDoc, pdfField

public sub Class_Initialize()
    set pdf = Server.CreateObject("Persits.Pdf")
    set objFSO = Server.CreateObject("scripting.Filesystemobject") 
end sub
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property

    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
    
    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get Customerlang()
        Customerlang = request.querystring("Customerlang")    
        if len(trim(Customerlang)) = 0 then Customerlang = Userlang()
    end property

    Property get userFirstName()
        userFirstName = request.Cookies ("TELESALESTOOLV2")("firstname")
    end property

    Property get userLastName()
        userLastName = request.Cookies ("TELESALESTOOLV2")("lastname")
    end property
    
    Property get pdfUrlFolder()
        pdfUrlFolder = server.mappath("\xlib\xtemplates\")
    end property

    Property get pdfName()
        pdfName = pdfRadical & Customerlang &  pdfExt
    end property

    Property get pdfUrl()
        pdfUrl = pdfUrlFolder & "\" & pdfName
    end property
    
    property get outputFolder()
        outputFolder = server.mappath("\xtmpTO\") & "\"
    'outputFolder = "Y:\Companyweb\BestelBons\TST2\"
    end property

    property Get outputGuid()
        outputGuid = CreateGUID()
    end property

    property Get outputFileName()
        select case Customerlang()
            case "FR"
                outputFileName = "commande_" & outputGuid & pdfExt
            case "NL"
                outputFileName = "bestelbon_" & outputGuid & pdfExt
            case else
                outputFileName = "bestelbon_" & outputGuid & pdfExt
        end select
    end property

    property get outputFile()
        outputFile=outputFolder & outputFileName
    end property


    property Get OperatorInfo()
        select case Customerlang()
            case "FR"
                OperatorInfo = "Votre account manager : " & userFirstName() & cwMainPhone
            case "NL"
                OperatorInfo = "Uw account manager : " & userFirstName() & cwMainPhone
            case else
                OperatorInfo = "Uw account manager : " & userFirstName() & cwMainPhone
        end select
    end property


public function AttachPdfBlanco()
    set pdfDoc = pdf.openDocument(pdfUrl,pdfKey)

    dim arrFields, arrvals, i
    arrFields= array("datum","contact","bedrijf","email","straat","plaats","telefoon","ondnr","einddatum", "operatordata")

    dim oPo :  set oPo = new clsObjPdf
        oPo.populateobjBoCompanyOrderInfo() 'oPo.responseWriteobjBoCompanyOrderInfo()
        
    dim sContact, sAddr, sLoc
    sContact = oPo.contact_firstname & " " & oPo.contact_lastname
    sAddr = oPo.company_address_street & " " & oPo.company_address_number
    sLoc = oPo.company_address_postcode & " " & oPo.company_address_localite
    
    arrvals= array(oPo.order_date,sContact, oPo.company_name,oPo.contact_email,sAddr,sLoc, oPo.company_tel, oPo.company_vat, oPo.end_date,OperatorInfo() )

    for i = lbound(arrFields) to ubound(arrFields)
        Set pdfField = pdfDoc.Form.FindField(arrFields(i))
        pdfField.SetFieldValue arrvals(i), pdfDoc.Fonts("Helvetica")
    next
    set oPo = nothing

    Dim targetFileName
    targetFileName=outputFileName
    pdfDoc.Save outputFolder & targetFileName, true
    AttachPdfBlanco = "\xtmpTO\" & targetFileName
end function

public function AttachPdfRegular()

end function

'<END REGION JSON BUSINESS LOGIC>=============================================================

private function pAttachPdf(pdfType)


end function

private function CreateGUID()
  Randomize Timer
  Dim tmpTemp1,tmpTemp2,tmpTemp3
  tmpTemp1 = Right(String(15,48) & CStr(CLng(DateDiff("s","1/1/2000",Date()))), 15)
  tmpTemp2 = Right(String(5,48) & CStr(CLng(DateDiff("s","12:00:00 AM",Time()))), 5)
  tmpTemp3 = Right(String(5,48) & CStr(Int(Rnd(1) * 100000)),5)
  CreateGUID = tmpTemp1 & tmpTemp2 & tmpTemp3
End Function

    private sub cleanUpObjects()
        set objFSO = Nothing    
        set pdfDoc = nothing
        set pdf = Nothing
    end sub

    public sub Class_Terminate()
        cleanUpObjects
    end sub

end class

    
%>


