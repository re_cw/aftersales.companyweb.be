﻿<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objLead.asp" -->
<!-- #include virtual ="/classes/obj/objComment.asp" -->
<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312
    CONST EVT_TYP_COMPLETED =26

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objLeads, objLead
    Set objLeads = new sysLeads
    set objLead = new clsObjLead

    select case request.querystring("q")
        case 1 'individual-today's calltime refreshed on jscript time elapsed 
            objLeads.JSONGetLeads()
        
    case 2 'month view
        
    case 3 'transfer lead to another operator
            objLead.populateLeadObject()    
            response.write objLeads.ASPTransferLead(objLead)
        
    case 4 ' returns all comments related to lead id (thus not an event yet)
            objLead.populateLeadObject()
                objLead.trusted = 1
            response.write objLeads.ASPGetLeadHistory(objLead)
        
    case 5 'AjaxLeads_SetNoAction
            objLead.populateLeadObject()
            objLead.trusted = 1
            response.write objLeads.AjaxLeads_SetNoAction(objLead)
    case 6 'AjaxLeads_Take5
            objLeads.AjaxLeads_take5()
    case 7 'AjaxLeads_Countleads
            objLeads.AjaxLeads_Countleads()

    case 8 'LQ_CreateLeadFromDirtyLead
            objLead.populateLeadObject()
            response.write objLeads.LQ_CreateLeadFromDirtyLead(objLead)

    case else 'default is individual today's
            'objLeads.JSONGetLeads()
    end select
    
    set objLeads = nothing


class sysLeads
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
    
    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    'for testing purpose only
    'if len(trim(usr_id))= 0 then usr_id = 1
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property  

    property get extension()
        extension = request.Cookies("TELESALESTOOLV2")("extension")    
        if len(trim(extension)) = 0 then extension = C_DEFAULT_EXT
    end property

    property get selectedMonth()
        selectedMonth = request.querystring("mm")
        if len(trim(selectedMonth)) = 0 then selectedMonth = month(Date())
    end property
    
    property get selectedYear()
        selectedYear = request.querystring("yy")
        if len(trim(selectedYear)) = 0 then selectedYear = year(Date())    
    end property

    property get selectedDate()
        selectedDate = request.querystring("yymmdd")
        
        if len(trim(selectedDate)) = 0 then selectedDate = currentDate()   
    end property

    property get currentDate()
        dim mm, dd
        if len(month(date())) = 1 then mm = "0" & month(date()) else mm = month(date())
        if len(day(date())) = 1 then dd = "0" & day(date()) else dd = day(date())
        currentDate= 20160122 'year(Date()) & mm & dd            
    end property

    property get salesTool()
        salesTool = request.querystring("st")
        if len(trim(salesTool)) = 0 then salesTool = "on"  
    end property

    property get beginDate()
        if len(trim(request("beginDate"))) > 0 then beginDate = castDate(request("beginDate"))
    end property

    property get endDate()
        if len(trim(request("endDate"))) > 0 then beginDate = castDate(request("endDate"))
    end property

    private function castDate(byval varDate)
        dim mm, dd
        if len(month(varDate)) = 1 then mm = "0" & month(varDate) else mm = month(varDate)
        if len(day(varDate)) = 1 then dd = "0" & day(varDate) else dd = day(varDate)
        castDate= year(varDate) & mm & dd
    end function


'<start region PUBLIC METHODS & FUNCTIONS>
    public function JSONGetLeads() 
        'response.write server.htmlencode(pJSONGetLeads())
    Response.ContentType = "application/json" : response.write pJSONGetLeads()
    end function

    public function ASPTransferLead(objLead)  
        ASPTransferLead = pASPTransferLead(objLead)        
    end function

    public function ASPGetLeadHistory(objLead)
        ASPGetLeadHistory = pASPGetLeadHistory(objLead)
    end function

    public function AjaxLeads_SetNoAction(objLead)
        AjaxLeads_SetNoAction = pAjaxLeads_SetNoAction(objLead)
    end function

    public function AjaxLeads_take5()
       response.write pAjaxLeads_take5()
    end function

    public function AjaxLeads_Countleads()
        response.write pAjaxLeads_Countleads()
    end function

    public function LQ_CreateLeadFromDirtyLead(objLead)
        LQ_CreateLeadFromDirtyLead = pLQ_CreateLeadFromDirtyLead(objLead)
    end function

'<end region PUBLIC METHODS & FUNCTIONS>


'<start region PRIVATE METHODS & FUNCTIONS>
Private Function IsNotNullorEmpty(input)
	dim IsNotNullorEmpty_Output
	IsNotNullorEmpty_Output = true
	response.write "[input = " & VarType(input) & " = '" & input & "']<br>"
	'vbEmpty (uninitialized) or vbNull (no valid data)
	if VarType(input) = 0 OR VarType(input) = 1 Then
		IsNotNullorEmpty_Output = false
	End if
	If VarType(input) = 8 Then
		If input = "" Then 
			IsNotNullorEmpty_Output = False
		End If
	End If
	IsNotNullorEmpty = IsNotNullorEmpty_Output
	response.write "[ " & IsNotNullorEmpty_Output & "]"
End Function

private function pLQ_CreateLeadFromDirtyLead(obj)
    Dim skipDebug : skipDebug= true
    pLQ_CreateLeadFromDirtyLead=0
    if len(trim(obj.company_vat)) = 0 then exit function
    

    Dim objConn, objRs, strSql, sAnd, blnAddNew

    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 0 then exit function

'on error resume next
    objConn.BeginTrans
  

'create [dbo].[companies]
    blnAddNew=false
    strsql = "SELECT TOP 1 * FROM [dbo].[companies] WHERE company_vat=" & obj.company_vat
    set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
    if .bof and .eof then blnAddNew=true ': .addNew
    debugCompanies skipDebug, blnAddNew,objRS,obj, strsql

        if blnAddNew then .addNew
			if (IsNotNullorEmpty(obj.company_vat)) then
				.fields("company_vat") = obj.company_vat
			end if
			if (IsNotNullorEmpty(obj.company_name)) then
				.fields("company_name") = obj.company_name
			end if
			if (IsNotNullorEmpty(obj.lang_id)) then
				.fields("company_lang_id") = obj.lang_id
			end if
			if (IsNotNullorEmpty(obj.backoffice_id)) then
				.fields("company_hbc_id") = obj.backoffice_id
			end if
			if (IsNotNullorEmpty(obj.company_tel)) then
				.fields("company_tel") = obj.company_tel
			end if
			if (IsNotNullorEmpty(obj.company_mobile)) then
				.fields("company_mobile") = obj.company_mobile
			end if
			if (IsNotNullorEmpty(obj.company_mail)) then
				.fields("company_mail") = obj.company_mail
            end if
			
			if blnAddNew then    
                .fields("AuditUserCreated") = obj.user_id
            else
                .fields("AuditUserUpdated") = obj.user_id
                .fields("AuditDateUpdated") = now()
            end if
            .update
            obj.company_id = .fields("company_id")
            .close
        end with

'create [dbo].[company_address]
blnAddNew=false
strsql = "SELECT TOP 1 * FROM dbo.company_address WHERE company_id=" & obj.company_id
    set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
    if .bof and .eof then blnAddNew=true : .addNew
    debugCompany_address skipDebug, blnAddNew,objRS,obj, strsql 
		if (IsNotNullorEmpty(obj.company_id)) Then
        .fields("company_id") = obj.company_id
		End if
        if (IsNotNullorEmpty(obj.company_address_type_id)) Then
			.fields("address_type_id") = obj.company_address_type_id
		End if
        if (IsNotNullorEmpty(obj.company_address_street)) Then
			.fields("company_address_street") = obj.company_address_street
		End if
        if (IsNotNullorEmpty(obj.company_address_number)) Then
			.fields("company_address_number")  = obj.company_address_number
		End if
        if (IsNotNullorEmpty(obj.company_address_boxnumber)) Then
			.fields("company_address_boxnumber") = obj.company_address_boxnumber
		End if
        if (IsNotNullorEmpty(obj.company_address_floor)) Then
			.fields("company_address_floor") = obj.company_address_floor
		End if
        if (IsNotNullorEmpty(obj.company_address_postcode)) Then
			.fields("company_address_postcode") = obj.company_address_postcode
		End if
        if (IsNotNullorEmpty(obj.company_address_localite)) Then
			.fields("company_address_localite") = obj.company_address_localite
		End if
        if blnAddNew then    
                .fields("AuditUserCreated") = obj.user_id
            else
                .fields("AuditUserUpdated") = obj.user_id
                .fields("AuditDateUpdated") = now()
            end if
            .update
            obj.company_address_id = .fields("company_address_id")
            .close
    end with

     


'create [dbo].[contacts]
blnAddNew=false
strSql = "SELECT TOP 1 * FROM dbo.contacts WHERE 1=1"    
strsql = strsql & " AND UPPER(contact_firstname) LIKE '%" & ucase(obj.contact_firstname) & "%'"
strsql = strsql & " AND UPPER(contact_lastname) LIKE '%" & ucase(obj.contact_lastname) & "%'"

    set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
    if .bof and .eof then blnAddNew=true : .addNew
    debugContacts skipDebug, blnAddNew,objRS,obj, strsql
		if (IsNotNullorEmpty(obj.contact_lang_id)) Then
        .fields("contact_lang_id") = obj.contact_lang_id
		End if
		if (IsNotNullorEmpty(obj.contact_title_id)) Then
        .fields("contact_title_id") = obj.company_address_postcode
		End if
		if (IsNotNullorEmpty(obj.company_address_postcode)) Then
        .fields("contact_gender") = obj.company_address_postcode
		End if
		if (IsNotNullorEmpty(obj.contact_firstname)) Then
        .fields("contact_firstname") = obj.contact_firstname
		End if
		if (IsNotNullorEmpty(obj.contact_lastname)) Then
        .fields("contact_lastname") = obj.contact_lastname
		End if
		if (IsNotNullorEmpty(obj.contact_tel)) Then
        .fields("contact_tel") = obj.contact_tel
		End if
		if (IsNotNullorEmpty(obj.contact_mobile)) Then
        .fields("contact_mobile") = obj.contact_mobile
		End if
		if (IsNotNullorEmpty(obj.contact_email)) Then
        .fields("contact_email") = obj.contact_email
		End if

    if blnAddNew then    
            .fields("AuditUserCreated") = obj.user_id
        else
            .fields("AuditUserUpdated") = obj.user_id
            .fields("AuditDateUpdated") = now()
        end if
        .update
        obj.contact_id = .fields("contact_id")
        .close
    end with


'create [dbo].[link_company_address_contacts]
blnAddNew=false
strsql= "SELECT TOP 1 * FROM dbo.link_company_address_contacts WHERE company_address_id=" & obj.company_address_id
strsql = strsql & " AND contact_id=" & obj.contact_id
    
    set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
    if .bof and .eof then blnAddNew=true : .addNew
    debugCompany_address_contacts skipDebug, blnAddNew,objRS,obj, strsql
		if (IsNotNullorEmpty(obj.company_address_id)) Then
        .fields("company_address_id") = obj.company_address_id      
		End if
		if (IsNotNullorEmpty(obj.contact_id)) Then
        .fields("contact_id") = obj.contact_id
		End if
		if (IsNotNullorEmpty(obj.contact_type_id)) Then
        .fields("contact_type_id") = obj.contact_type_id
		End if
        if blnAddNew then    
            .fields("AuditUserCreated") = obj.user_id
        else
            .fields("AuditUserUpdated") = obj.user_id
            .fields("AuditDateUpdated") = now()
        end if
        .update
        .close
    end with

'create [dbo].[leads]
blnAddNew=false
strsql = "select top 1 * from [dbo].[leads] where company_id=" & obj.company_id & " and user_id=" & obj.user_id
    set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
    if .bof and .eof then blnAddNew=true : .addNew
    debugLeads skipDebug, blnAddNew, objRS, obj, strsql
        if (IsNotNullorEmpty(obj.status_id)) Then
        .fields("status_id") = obj.status_id
		End if
        if (IsNotNullorEmpty(obj.user_id)) Then
		.fields("user_id") = obj.user_id
		End if
        if (IsNotNullorEmpty(obj.lang_id)) Then
		.fields("lang_id") = obj.lang_id
		End if
        if (IsNotNullorEmpty(obj.source_id)) Then
		.fields("source_id") = obj.source_id
		End if
        if (IsNotNullorEmpty(obj.company_id)) Then
		.fields("company_id") = obj.company_id
		End if
        if (IsNotNullorEmpty(obj.company_vat)) Then
		.fields("company_vat") = obj.company_vat
		End if
        if (IsNotNullorEmpty(obj.backoffice_id)) Then
		.fields("backoffice_id") = obj.backoffice_id
		End if
        if (IsNotNullorEmpty(obj.lead_type_id)) Then
		.fields("lead_type_id") = obj.lead_type_id
		End if
        if (IsNotNullorEmpty(obj.prompt_date)) Then
		.fields("prompt_date") = obj.prompt_date
		End if
        if (IsNotNullorEmpty(obj.prompt_time)) Then
		.fields("prompt_time") = obj.prompt_time
		End if
        if (IsNotNullorEmpty(obj.enabled)) Then
		.fields("enabled") = obj.enabled
		End if
        if blnAddNew then    
            .fields("AuditUserCreated") = obj.user_id
        else
            .fields("AuditUserUpdated") = obj.user_id
            .fields("AuditDateUpdated") = now()
        end if
        .update
        .close
    end with


'create [dbo].[events]

'create [dbo].[comments]

'disable [dbo].[leads_queue]
strsql = "select top 1 * from [dbo].[leads_queue] where LQ_id=" & obj.lq_id
set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
        .fields("status_id")=1
        .update
        .close
    end with

'end process commit or rollback
        if Err.number = 0 then
            objConn.CommitTrans
            pLQ_CreateLeadFromDirtyLead = 1
        else
            objConn.RollbackTrans
            pLQ_CreateLeadFromDirtyLead = sErrMsg
        end if
set objConn = nothing

end function



private function pAjaxLeads_Countleads()
    pAjaxLeads_Countleads = 0
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    strSql = "dbo.GetLeads"
    Dim a
    a = usr_id()
        set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 0 then exit function

    set objCmd = Server.CreateObject("ADODB.Command")
    with objCmd
        .ActiveConnection = objConn
        .CommandType = 4 'adCmdStoredProc
        .CommandText =  strSql
        .NamedParameters = True
        .Parameters.Append .CreateParameter("@usr_id", 8, 1, len(a), a)       
    on error resume next
        set objRS = Server.CreateObject("ADODB.Recordset")
        set objRS = .Execute()
    
            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                "<br />Error in Class: sysLeads.pAjaxLeads_Countleads()" & _
                "<br />while triggering [set objRS = .Execute]."
                response.write sErrMsg
                exit function
            end if
        end with 
        with objRS
            if not .bof and not .eof then
                pAjaxLeads_Countleads = .recordcount
            end if
        end with
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

end function

private function pAjaxLeads_take5()
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    strSql = "dbo.Take5LeadsFromBucket"
    Dim a
    a = usr_id()
        set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
        if objConn.State = 0 then exit function
        
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@usr_id", 8, 1, len(a), a)       
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysLeads.pAjaxLeads_take5()" & _
                    "<br />while triggering [set objRS = .Execute]. on " & strSql
                    response.write sErrMsg
                    exit function
                end if
         end with
        pAjaxLeads_take5=sErrMsg
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

end function

private function pJSONGetLeads()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    Dim lead_source_name, lead_type_name, strLeadIcon, strLeadColor, irecordcount
    
    lead_source_name = "lead_source_name_" & userlang()
    lead_type_name = "lead_type_name_" & userlang()


    strSql = "dbo.GetLeads"
    dim a, b, c, d, e
        
    a = usr_id()
    irecordcount = 0
        
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with

    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@usr_id", 8, 1, len(a), a)       
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysLeads.pJSONGetLeads()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with       
                with objRS
                    if not .bof and not .eof then
                    
                        s= "["
                        do while not .eof
                        strLeadIcon= .fields("lead_type_icon")
                        strLeadColor= .fields("lead_type_color") 
                        if len(trim(.fields("lead_source_icon"))) > 0 then strLeadIcon= .fields("lead_source_icon")
                        if len(trim(.fields("lead_source_color"))) > 0 then strLeadColor= .fields("lead_source_color")

                             s=s & "{"
                            s=s & " ""lead_id"":""" & .fields("lead_id") & """"
                            s=s & " ,""status_id"":""" & .fields("status_id") & """"
                            s=s & " ,""user_id"":""" & .fields("user_id") & """"
                            s=s & " ,""lang_id"":""" & .fields("lang_id") & """"
                            s=s & " ,""source_id"":""" & .fields("source_id") & """"
                            s=s & " ,""lead_type_id"":""" & .fields("lead_type_id") & """"
                            s=s & " ,""backoffice_id"":""" & .fields("backoffice_id") & """"                        
                            s=s & " ,""company_id"":""" & .fields("company_id") & """"
                            s=s & " ,""prompt_date"":""" & .fields("prompt_date") & """"
                            s=s & " ,""prompt_time"":""" & .fields("prompt_time") & """"
                            s=s & " ,""company_name"":""" & .fields("company_name") & """"
                            s=s & " ,""company_vat"":""" & .fields("company_vat") & """"
                            s=s & " ,""lang_radical"":""" & .fields("lang_radical") & """"
                            s=s & " ,""lead_source_name"":"""  & .fields(lead_source_name) & """"
                            s=s & " ,""lead_type_name & "":""" & .fields(lead_type_name) & """"
                            s=s & " ,""company_address_street"":""" & .fields("company_address_street") & """"
                            s=s & " ,""company_address_number"":""" & .fields("company_address_number") & """"
                            s=s & " ,""company_address_postcode"":""" & .fields("company_address_postcode") & """"
                            s=s & " ,""company_address_localite"":""" & .fields("company_address_localite") & """"
                            s=s & " ,""company_tel"":""" & .fields("company_tel") & """"
                            s=s & " ,""company_mobile"":""" & .fields("company_mobile") & """"
                            s=s & " ,""company_mail"":""" & .fields("company_mail") & """"
                            s=s & " ,""lead_type_order"":""" & .fields("lead_type_order") & """"
                            s=s & " ,""lead_type_color"":""" & strLeadColor & """"
                            s=s & " ,""lead_type_textColor"":""" & .fields("lead_type_textColor") & """"
                            s=s & " ,""lead_type_icon"":""" & strLeadIcon & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        s = s & "]"    
                    end if
                end with
        
        pJSONGetLeads = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
end function


private sub debugTransferLead()
    objLead.trusted=1
    objLead.lead_id=392
    objLead.new_user_id=3
    objLead.HBCompanyNotes= "client est allemand"
    objLead.company_vat = 439759002
end sub

private function pASPTransferLead(objLead)
'debugTransferLead()
'this prevents any insert which is not coming from the interface
    if objLead.trusted <> 1 then exit function
    
    Dim debugMsg, comment_ID
    Dim objConn, objRS, strSql, sErrMsg
    
    strSql = "SELECT * FROM [dbo].[leads] WHERE lead_id=" & objLead.lead_id    
    
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
     
    if objConn.State <> 1 then exit function    
  
    set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strsql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysLeads.pASPTransferLead(objLead)" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            response.write sErrMsg
            exit function
        end if
    
        with objRS
                .fields("user_id")=objLead.new_user_id
                .fields("AuditUserUpdated")=objLead.user_id
                .fields("AuditDateUpdated")=now()
            .update      
            sErrMsg= 1
        end with
        
    Dim objComment
    set objComment = new clsObjComment
        objComment.comment_lead_id = objLead.lead_id
        objComment.comment_event_id = 0    
        objComment.comment_event_type_id = 15 'LEAD_TRANSFER
        objComment.comment_usr_id = usr_id()
        objComment.comment = objLead.HBCompanyNotes
        objComment.comment_date = SetDate()
        objComment.comment_time = SetTime()
        objComment.comment_company_vat = objLead.company_vat

        sErrMsg = AddComment(objComment)

        objRs.Close : set objRS = nothing
        objConn.close : set objConn = nothing  

        pASPTransferLead = sErrMsg

end function



    private function pAjaxLeads_SetNoAction(objLead)
    'this prevents any insert which is not coming from the interface
    if objLead.trusted <> 1 then exit function
    
    Dim debugMsg, comment_ID
    Dim objConn, objRS, strSql, sErrMsg
    
    strSql = "SELECT * FROM [dbo].[leads] WHERE lead_id=" & objLead.lead_id    
    
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
     
    if objConn.State <> 1 then exit function

    Dim objComment
    set objComment = new clsObjComment
        objComment.comment_lead_id = objLead.lead_id
        objComment.comment_event_id = 0    
        objComment.comment_event_type_id = EVT_TYP_COMPLETED
        objComment.comment_usr_id = usr_id()
        objComment.comment = objLead.HBCompanyNotes
        objComment.comment_date = SetDate()
        objComment.comment_time = SetTime()
        objComment.comment_company_vat = objLead.company_vat

        sErrMsg = AddComment(objComment)


     set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strsql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysLeads.pASPTransferLead(objLead)" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            response.write sErrMsg
            exit function
        end if
    
        with objRS
                .fields("enabled")=0
                .fields("AuditUserUpdated")=objLead.user_id
                .fields("AuditDateUpdated")=now()
            .update      
            sErrMsg= 1
        end with

    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing  

    pAjaxLeads_SetNoAction = sErrMsg

    end function


private function pASPGetLeadHistory(objLead)

'this prevents any insert which is not coming from the interface
    'if objLead.trusted <> 1 then exit function
    
    Dim debugMsg, event_type_name
    Dim objConn, objRS, strSql, sErrMsg
    event_type_name = "event_type_name_" & userlang()

    strSql = "SELECT * FROM [dbo].[vw_GetComments] WHERE comment_lead_id=" & objLead.lead_id & " ORDER BY AuditDateCreated DESC"   
    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with

    if objConn.State <> 1 then exit function

        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 1 'adCmdText
            .CommandText =  strSql
            on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                "<br />Error in Class: sysLeads.pASPGetLeadHistory()" & _
                "<br />while triggering [set objRS = .Execute]."
                pASPGetLeadHistory = sErrMsg
                exit function
            end if
         end with
    
        with objRS
            if not .bof and not .eof then
                dim tt, hdr            

                do while not .eof
                    tt = formatTimeStamp(.fields("comment_date"), .fields("comment_time"))
                    hdr = tt & " - " & .fields("firstname") & " " & .fields("lastname")  & " [" & .fields(event_type_name) & "] : "
                    s=s & hdr & .fields("comment") & "<br>"
                .movenext
                loop
            end if
        end with    
    s= replace(s, chr(10) & chr(13), "<br>")
    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pASPGetLeadHistory = s

end function


Private function AddComment(objComment)
    Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd
        blnNew=false
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if
    
    sErrMsg=1
    if objConn.State = 1 then
            strSql="dbo.Comments_ADD"                    
            
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@comment_event_type_id", 5, 1, len(objComment.comment_event_type_id), objComment.comment_event_type_id)
                .Parameters.Append .CreateParameter("@comment_usr_id", 5, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@comment_event_id", 8, 1, len(objComment.comment_event_id), objComment.comment_event_id)
                .Parameters.Append .CreateParameter("@comment_lead_id", 8, 1, len(objComment.comment_lead_id), objComment.comment_lead_id)
                .Parameters.Append .CreateParameter("@comment_company_vat", 8, 1, len(objComment.comment_company_vat), objComment.comment_company_vat)
                .Parameters.Append .CreateParameter("@comment_date", 5, 1, len(objComment.comment_date), objComment.comment_date)
                .Parameters.Append .CreateParameter("@comment_time", 8, 1, len(objComment.comment_time), objComment.comment_time)
                .Parameters.Append .CreateParameter("@comment", 8, 1, len(objComment.comment), objComment.comment)

                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@comment_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
            'objRs.Close
            set objRs= nothing
            set objComment = nothing
            comment_ID=objCmd(10)
        end if
      
    set objRS = nothing
    set objConn = nothing  
    AddComment=sErrMsg

end function




private function formatTimeStamp(sdate, stime)
dim dd, mm, yy
    yy = left(sdate, 4)
    dd = right(sdate, 2)
    mm = mid(sdate,5,2)

formatTimeStamp = dd & "-" & mm & "-" & yy & " " & stime

end function

public function SetDate()
    dim yy, mm, dd    
    yy = year(now()) 
    mm = month(now()) 
        if mm < 10 then mm = "0" & mm
    dd = day(now())
        if dd < 10 then dd = "0" & dd
    SetDate = yy & mm & dd
end function

public function SetTime()
    Dim hh, mm
    hh = hour(now())
    if hh < 10 then hh = "0" & hh
    mm = minute(now())
    if mm < 10 then mm = "0" & mm
    SetTime = hh & ":" & mm
end function


'<DEBUGGING METHODS>
    private sub debugCompanies(skipDebug, blnAddNew,objRS,obj, strsql)
        if skipDebug then exit sub
            response.write "<b>create [dbo].[companies]</b> - blnAddNew: "  & blnAddNew & "<br>"
            if not blnAddNew then
            do while not objRS.eof
                response.write objRS.fields("company_id") & ":" & objRS.fields("company_name") & "<br>"
                objRS.movenext
            loop
            objRS.movefirst
            end if
    
            response.write "strsql: "  & strsql & "<br>recordcount:" & objRS.recordcount & "<br>obj.company_vat: "  & obj.company_vat & "<br>obj.company_name: "  & obj.company_name & "<br>obj.lang_id: "  & obj.lang_id & "<br>obj.backoffice_id: "  & obj.backoffice_id & "<br>obj.user_id: "  & obj.user_id & "<br>"
    end sub

    private sub debugCompany_address(skipDebug, blnAddNew,objRS,obj, strsql)
        if skipDebug then exit sub
            'start debug
            response.write "<br><b>create [dbo].[company_address]</b> - blnAddNew: "  & blnAddNew & "<br>"
            if not blnAddNew then
            do while not objRS.eof
                response.write "company_id:" & objRS.fields("company_id") & " - address_type_id:" & objRS.fields("address_type_id") & "<br>"
                objRS.movenext
            loop
            objRS.movefirst
            end if
            response.write "strsql: "  & strsql & "<br>"
            response.write "recordcount:" & objRS.recordcount & "<br>"
            response.write "obj.company_address_id: "  & objRS.fields("company_address_id") & "<br>"
    end sub


    private sub debugContacts(skipDebug, blnAddNew,objRS,obj, strsql)
        if skipDebug then exit sub
                response.write "<b>create [dbo].[contacts]</b> - blnAddNew: "  & blnAddNew & "<br>"
            if not blnAddNew then    
            do while not objRS.eof
                    response.write "contact_id : " & objRS.fields("contact_id") & "<br>"
                    objRS.movenext
                loop
                objRS.movefirst
            end if
        response.write "strsql: "  & strsql & "<br>"
        response.write "recordcount:" & objRS.recordcount & "<br>"
        
        response.write "obj.contact_lang_id: "  & obj.contact_lang_id & "<br>"
        response.write "obj.contact_title_id: "  & obj.contact_title_id & "<br>"
        response.write "obj.contact_gender: "  & obj.contact_gender & "<br>"
        response.write "obj.contact_firstname: "  & obj.contact_firstname & "<br>"
        response.write "obj.contact_lastname: "  & obj.contact_lastname & "<br>"
        response.write "obj.contact_tel: "  & obj.contact_tel & "<br>"
        response.write "obj.contact_mobile: "  & obj.contact_mobile & "<br>"
        response.write "obj.contact_email: "  & obj.contact_email & "<br>"
    end sub

    private sub debugCompany_address_contacts( skipDebug, blnAddNew,objRS,obj, strsql)
        if skipDebug then exit sub
            response.write "<b>create [dbo].[link_company_address_contacts]</b> - blnAddNew: "  & blnAddNew & "<br>"
            if not blnAddNew then    
            do while not objRS.eof
                    response.write "company_address_id : " & objRS.fields("company_address_id") & "<br>"
                    objRS.movenext
                loop
                objRS.movefirst
            end if
            response.write "strsql: "  & strsql & "<br>"
            response.write "recordcount:" & objRS.recordcount & "<br>"
            response.write "obj.company_address_id: "  & obj.company_address_id & "<br>"
            response.write "obj.contact_id: "  & obj.contact_id & "<br>"
            response.write "obj.contact_type_id: "  & obj.contact_type_id & "<br>"
    end sub

    private sub debugLeads(skipDebug, blnAddNew,objRS,obj, strsql)
        if skipDebug then exit sub
            response.write "<b>create [dbo].[leads]</b> - blnAddNew: "  & blnAddNew & "<br>"
            if not blnAddNew then    
            do while not objRS.eof
                    response.write "lead_id : " & objRS.fields("lead_id") & "<br>"
                    objRS.movenext
                loop
                objRS.movefirst
            end if
            response.write "strsql: "  & strsql & "<br>recordcount:" & objRS.recordcount & "<br>obj.status_id: "  & obj.status_id & "<br>obj.user_id: "  & obj.user_id & "<br>obj.lang_id: "  & obj.lang_id & "<br>obj.source_id: "  & obj.source_id & "<br>obj.company_id: "  & obj.company_id & "<br>obj.company_vat: "  & obj.company_vat & "<br>obj.backoffice_id: "  & obj.backoffice_id & "<br>obj.lead_type_id: "  & obj.lead_type_id & "<br>obj.prompt_date: "  & obj.prompt_date & "<br>obj.prompt_time: "  & obj.prompt_time & "<br>obj.enabled: "  & obj.enabled & "<br>"
    end sub

'<end region PRIVATE METHODS & FUNCTIONS>
    public sub Class_Terminate()
        set objLead = nothing    
    end sub

end class

%>