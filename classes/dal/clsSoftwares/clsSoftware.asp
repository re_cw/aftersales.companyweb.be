﻿<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"

Dim objSoft
   
    set objSoft = new sysSoftwares

        select case request.QueryString("a")         
            case "GetComboSoftwaresWithImg"
                objSoft.GetComboSoftwaresWithImg
            case "GetCompany_Softwares"
                objSoft.JSONGetCompany_Softwares
            case "GetCompany_SoftwaresFromHbc"
                objSoft.JSONGetCompany_SoftwaresFromHbc
            case "deleteSoftware"
                objSoft.deleteCompany_Software        
            case else 'default should be read
        end select
    

class sysSoftwares

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property
        property get hbc_id()
        hbc_id = request("hbc_id")
        if len(trim(hbc_id)) = 0 then hbc_id = -1    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property

    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property

    property get link_id()
        link_id=request("link_id")
    end property

    public function GetComboSoftwaresWithImg()
        GetComboSoftwaresWithImg = pGetComboSoftwaresWithImg() :  response.write GetComboSoftwaresWithImg
    end function
    public function JSONGetCompany_SoftwaresFromHbc()
        Response.ContentType = "application/json" : response.write pJSONGetCompany_SoftwaresFromHbc()
    end function

    public function JSONGetCompany_Softwares() 
        Response.ContentType = "application/json" : response.write pJSONGetCompany_Softwares()
    end function

        public function deleteCompany_Software()
    response.write pdeleteCompany_Software()
    end function

'<START REGION JSON BUSINESS LOGIC>===========================================================
     private function pdeleteCompany_Software()
            Dim objConn, strSql
            strSql = "DELETE FROM [dbo].[link_company_soft_packs] WHERE [link_id]=" & link_id()
            set objConn=Server.CreateObject("ADODB.Connection")
            with objConn
                .open SalesToolConnString() , adOpenDynamic, adLockOptimistic, adcmdtext
                .execute strsql
                .close
            end with
        set objConn = nothing
        if err.number <> 0 then pdeleteCompany_Software = err.Description
    end function

    private function pGetComboSoftwaresWithImg()       
            Dim objConn, oRs, strSql, SoftPackName, s
            'SoftPackName = "soft_pack_name_" & UserLang() 
            strSql = " SELECT soft_pack_id, soft_pack_name_" & UserLang() & " as soft_pack_name, soft_pack_order, soft_pack_enabled, soft_pack_img "
            strSql = strSql & " , (select count(*) from link_company_soft_packs where dbo.link_company_soft_packs.soft_pack_id = dbo.ref_soft_packs.soft_pack_id) as aantal "
            strSql = strSql & " FROM dbo.ref_soft_packs "
            strSql = strSql & " WHERE (soft_pack_enabled = 1) "
            strSql = strSql & " ORDER BY soft_pack_order "
            'strSql = "SELECT * FROM dbo.[vw_GetSoftwares] order by soft_pack_order"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{""options"":["
                            do while not .eof
                                's = s & "{key:" & .fields(0) & ",label:'" & .fields(EventTypeName) & "'},"
                                s = s & "{""value"":""" & .fields(0) & """, ""img"": """ & .fields("soft_pack_img") & """, ""text"":""" & "[" & .fields("aantal") & "] " & .fields("soft_pack_name") & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboSoftwaresWithImg = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

    private function pJSONGetCompany_SoftwaresFromHbc 
        strSql = "select link_id, "
        strSql = strSql & " case C.company_lang_id when 1 then RS.soft_pack_name_FR when 2 then RS.soft_pack_name_NL else RS.soft_pack_name_EN END as SoftPackName, " 
        strSql = strSql & " RS.soft_pack_img, LCS.contract_end_date " 
        strSql = strSql & " from companies C "
        strSql = strSql & " inner join link_company_soft_packs LCS on C.company_id = LCS.company_id "
        strSql = strSql & " inner join ref_soft_packs RS on LCS.soft_pack_id = RS.soft_pack_id "
        strSql = strSQl & " where C.company_hbc_id=" & hbc_id()

		set objConn=Server.CreateObject("ADODB.Connection")
		set oRs=Server.CreateObject("ADODB.Recordset") 
		objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
		oRs.open strSql, objConn
		
		if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if
                                        
		oRs.Close : set oRs = nothing
		objConn.close : set objConn = nothing

    pJSONGetCompany_SoftwaresFromHbc = s
    end function
    private function pJSONGetCompany_Softwares()
    
    Dim a
        a = company_id() 
		strSql = "select link_id, "
        strSql = strSql & " case C.company_lang_id when 1 then RS.soft_pack_name_FR when 2 then RS.soft_pack_name_NL else RS.soft_pack_name_EN END as SoftPackName, " 
        strSql = strSql & " RS.soft_pack_img, LCS.contract_end_date " 
        strSql = strSql & " from companies C "
        strSql = strSql & " inner join link_company_soft_packs LCS on C.company_id = LCS.company_id "
        strSql = strSql & " inner join ref_soft_packs RS on LCS.soft_pack_id = RS.soft_pack_id "
        strSql = strSQl & " where C.company_id=" & a

		set objConn=Server.CreateObject("ADODB.Connection")
		set oRs=Server.CreateObject("ADODB.Recordset") 
		objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
		oRs.open strSql, objConn
		
		if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if

    oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetCompany_Softwares = s


end function


'<END REGION JSON BUSINESS LOGIC>=============================================================



public sub Class_Terminate()
    set objSoft = nothing
end sub


end class

%>