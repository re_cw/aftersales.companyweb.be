﻿
function AjaxSoftware_DeleteEntry(id) {
	var url = "/classes/dal/clsSoftwares/clsSoftware.asp";
	
	$.get(url, {a:"deleteSoftware", link_id: id})
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error Deleting Software",
            expire: -1,
            type: "errormessage"
        });
    });

}

function AjaxSoftware_GetSoftwaresPromise() {
    var url = "/classes/dal/clsSoftwares/clsSoftware.asp";
	
    return $.get(url, { a: "GetComboSoftwaresWithImg" })
    fail(function (resp) {
        dhtmlx.message({
            text: "Error getting software",
            expire: -1,
            type: "errormessage"
        });
    });

}
function AjaxSoftware_GetSoftwaresListFromHbcId(container, id) {
		$("#" + container).empty();
		var url = "/classes/dal/clsSoftwares/clsSoftware.asp";
		
        $.get(url, { a: "GetCompany_SoftwaresFromHbc", hbc_id: id })
		.done(function (data) {
			$("#" + container).html(formatListSoftwares(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting software from hbcid",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}
function AjaxSoftware_GetSoftwaresList(container, id) {
		$("#" + container).empty();
		var url = "/classes/dal/clsSoftwares/clsSoftware.asp";
		
		$.get(url, {a: "GetCompany_Softwares", company_id: id})
		.done(function(data){
			$("#"+ container).html(formatListSoftwares(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting software list",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

function formatListSoftwares(obj) {
    var t = '';
    if (!$.isEmptyObject(obj))
    {
        var img = '';
        var imgDel = '';
        t = "<table cellpadding=1 width=100% border=0>";
        for (var i = 0; i < obj.length; i++) {
            img = '<img src="/graphics/common/imgcombo/softwares/' + obj[i].soft_pack_img + '">';
            imgDel = '<img src ="/graphics/common/win_16x16/delete.png" class="onmouseoverdel" onClick="delItem(' + obj[i].link_id + ', category);">';
            t += "<tr><td width=10%>" + img + "</td><td>" + obj[i].SoftPackName + "</td><td align=right>" + imgDel.replace("category", "'software'") + "</td></tr>";
        }
        t += "</table>";
    }
    return t;
}