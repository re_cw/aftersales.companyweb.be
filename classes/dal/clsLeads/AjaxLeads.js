﻿function populateobjLead(objLead) {
	var params = "lead_id=" + encodeURIComponent(objLead.lead_id);
	params += "&user_id=" + encodeURIComponent(objLead.user_id);
	params += "&status_id=" + encodeURIComponent(objLead.status_id);
	params += "&lang_id=" + encodeURIComponent(objLead.lang_id);
	params += "&source_id=" + encodeURIComponent(objLead.source_id);
	params += "&lead_type_id=" + encodeURIComponent(objLead.lead_type_id);
	params += "&backoffice_id=" + encodeURIComponent(objLead.backoffice_id);
	params += "&company_id=" + encodeURIComponent(objLead.company_id);
	params += "&prompt_date=" + encodeURIComponent(objLead.prompt_date);
	params += "&prompt_time=" + encodeURIComponent(objLead.prompt_time);
	params += "&company_name=" + encodeURIComponent(objLead.company_name);
	params += "&company_vat=" + encodeURIComponent(objLead.company_vat);
	params += "&company_address_street=" + encodeURIComponent(objLead.company_address_street);
	params += "&company_address_number=" + encodeURIComponent(objLead.company_address_number);
	params += "&company_address_postcode=" + encodeURIComponent(objLead.company_address_postcode);
	params += "&company_address_localite=" + encodeURIComponent(objLead.company_address_localite);
	params += "&company_tel=" + encodeURIComponent(objLead.company_tel);
	params += "&company_mobile=" + encodeURIComponent(objLead.company_mobile);
	params += "&company_mail=" + encodeURIComponent(objLead.company_mail);
	params += "&HBCompanyNotes=" + encodeURIComponent(objLead.HBCompanyNotes);
	params += "&new_user_id=" + encodeURIComponent(objLead.new_user_id);
	params += "&trusted=1";

	return params;
}

function ASPTransferLead(objLead) {
	var url = "/classes/dal/clsLeads/clsLeads.asp?q=3";
	var params = populateobjLead(objLead);
	
	$.post(url, params)
	.done(function(data){
		switch(data)
		{
			case "1":
				dhtmlx.message({
					text: locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
				dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblSaveSuccess);
			break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblActionCancelled,
					expire: -1,
					type: "errormessage"
				});
				dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled + "\n" + http.responseText);
				break;
		}
	}).fail(function (resp) {
		dhtmlx.message({
			text: "Error transferring Lead",
			expire: -1,
			type: "errormessage"
		});
	});
}

function ASPGetLeadHistoryPromise(objLead) {
		$("#Comments" + objLead.company_vat).empty();
		
		var url = "/classes/dal/clsLeads/clsLeads.asp?q=4";
		var params = populateobjLead(objLead);
		return $.post(url, params)
		.done(function(data){
			$("#Comments" + objLead.company_vat).html(data); 
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Lead History",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#Comments" + objLead.company_vat).data('requestRunning',false);
		});
}

function AjaxLeads_SetNoAction(objLead) {
	var url = "/classes/dal/clsLeads/clsLeads.asp?q=5";
	var params = populateobjLead(objLead);
	
	$.post(url, params)
	.done(function(data){
		switch(data)
		{
			case "1":
				dhtmlx.message({
					text: locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
				dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblSaveSuccess);
				break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblActionCancelled,
					expire: -1,
					type: "errormessage"
					
				});
				dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled);
				break;
		}
	}).fail(function (resp) {
		dhtmlx.message({
			text: "Error setting No action",
			expire: -1,
			type: "errormessage"
		});
	});
}

function AjaxLeads_Take5() {
	var url = "/classes/dal/clsLeads/clsLeads.asp";
	
	$.get(url, {q:6})
	.done(function(data){
		if (data.length > 0)
		{
			dhtmlx.message({
				text: resp,
				expire: -1,
					type: "errormessage"
			});
			dhtmlx.alert(resp);
		}
	}).fail(function (resp) {
		dhtmlx.message({
			text: "Error taking a hike... I mean Five!",
			expire: -1,
			type: "errormessage"
		});
	});
}
function AjaxLeads_CountleadsPromise() {
	var url = "/classes/dal/clsLeads/clsLeads.asp";
	
	return $.get(url, { q: 7 })
	.fail(function (resp) {
		dhtmlx.message({
			text: "Error counting leads",
			expire: -1,
			type: "errormessage"
		});
	});
}