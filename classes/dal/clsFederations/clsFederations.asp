﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->

<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"

Dim objFed
   
    set objFed = new sysFederations

        select case request.QueryString("a")         
            case "GetComboFederationsWithImg"
                objFed.GetComboFederationsWithImg
            case "GetCompany_federations"
                objFed.JSONGetCompany_Federations
            case "GetCompany_federationsFromHbc"
                objFed.JSONGetCompany_FederationsFromHbc
            case "GetAffiliatedCompanies"
                objFed.JSONGetAffiliatedCompanies
            
            case "GetFederation_Companies"
            case "deleteFederation"
                objFed.deleteCompany_Federation
            case else 'default should be read
        end select
    

class sysFederations

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property
    property get hbc_id()
        hbc_id=trim(request.QueryString("hbc_id"))
        if len(hbc_id) = 0 then hbc_id = -1
    end property
    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property

    property get federation_id()
        federation_id=trim(request.querystring("federation_id"))
        if len(federation_id) = 0 then federation_id = 0
    end property

        property get link_id()
        link_id=request("link_id")
    end property

    public function GetComboFederationsWithImg()
        GetComboFederationsWithImg = pGetComboFederationsWithImg() :  response.write GetComboFederationsWithImg
    end function
    public function JSONGetCompany_FederationsFromHbc()
            Response.ContentType = "application/json" :response.write pJSONGetCompany_FederationsFromHbc()
    end function
    public function JSONGetCompany_Federations() 
        Response.ContentType = "application/json" :response.write pJSONGetCompany_Federations()
    end function

    public function JSONGetAffiliatedCompanies()
        Response.ContentType = "application/json" : response.write pJSONGetAffiliatedCompanies()
    end function

            public function deleteCompany_Federation()
    response.write pdeleteCompany_Federation()
    end function

'<START REGION JSON BUSINESS LOGIC>===========================================================
      private function pdeleteCompany_Federation()
            Dim objConn, strSql
            strSql = "DELETE FROM [dbo].[link_company_federations] WHERE [link_id]=" & link_id()
            set objConn=Server.CreateObject("ADODB.Connection")
            with objConn
                .open SalesToolConnString() , adOpenDynamic, adLockOptimistic, adcmdtext
                .execute strsql
                .close
            end with
        set objConn = nothing
        if err.number <> 0 then pdeleteCompany_Federation = err.Description
    end function

    private function pGetComboFederationsWithImg()       
            Dim objConn, oRs, strSql, s
            strSql = " SELECT federation_id, federation_name_" & UserLang() &" as federation_name, "
            strSql = strSql & " federation_img, federation_order, federation_enabled, "
            strSql = strSql & " (select count(*) from link_company_federations where dbo.link_company_federations.federation_id = dbo.ref_federations.federation_id) as aantal "
            strSql = strSql & " FROM dbo.ref_federations "
            strSql = strSql & " WHERE (federation_enabled = 1) "
            strSql = strSql & " ORDER BY federation_order "
            'strSql = "SELECT * FROM dbo.[vw_GetFederations] order by federation_order"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                    
                        s= "{""options"":["
                            do while not .eof
                                s = s & "{""value"":""" & .fields("federation_id") & """, ""img"": """ & .fields("federation_img") & """, ""text"":""" & "[" & .fields("aantal") & "] " & .fields("federation_name") & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboFederationsWithImg = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

    private function pJSONGetCompany_FederationsFromHbc
        Dim a, fedName
        a = hbc_id()  

    strSql = "select LCF.link_id, "
    strSql = strSql & " case C.company_lang_id when 1 then RF.federation_name_FR when 2 then RF.federation_name_NL else RF.federation_name_EN END as fedname,  "
    strSql = strSql & " RF.federation_id, RF.federation_img "
    strSql = strSql & " from companies C "
    strSql = strSql & " inner join link_company_federations AS LCF ON C.company_id = LCF.company_id "
    strSql = strSql & " join ref_federations RF ON LCF.federation_id = RF.federation_id "
    strSql = strSql & " where c.company_hbc_id =" & a

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
            s= "["        
           with oRs
                if not .bof and not .eof then                     

                        do while not .eof
                            s=s & "{"
                                s=s & " ""link_id"":""" & .fields("link_id") & """"
                                s=s & " ,""federationname"":""" & .fields("fedName") & """"
                                s=s & " ,""federation_id"":""" & .fields("federation_id") & """"
                                s=s & " ,""federation_img"":""" & .fields("federation_img") & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
   
                end if
           end with
        s = s & "]" 
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

        pJSONGetCompany_FederationsFromHbc = s
    end function

    private function pJSONGetCompany_Federations()
    
        Dim a, fedName
        a = company_id() 
        fedName = "federation_name_" & UserLang()  
        strSql = "select * from dbo.vw_GetCompany_Federations where company_id=" & a

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn

           s= "["               
           with oRs
                if not .bof and not .eof then                     

                        do while not .eof
                            s=s & "{"
                                s=s & " ""link_id"":""" & .fields("link_id") & """"
                                s=s & " ,""federationname"":""" & .fields(fedName) & """"
                                s=s & " ,""federation_id"":""" & .fields("federation_id") & """"
                                s=s & " ,""federation_img"":""" & .fields("federation_img") & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        
                end if
           end with
        s = s & "]"    
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

        pJSONGetCompany_Federations = s

    end function


    private function pJSONGetAffiliatedCompanies()
    Dim a, nbc_desc, jfc_s_desc
        a = federation_id() 
        nbc_desc = "nbc_desc_" & UserLang() 
        jfc_s_desc = "jfc_s_desc_" & UserLang() 
        strSql = "select * from dbo.vw_GetCompany_Affiliated where federation_id=" & a & " order by company_address_postcode"
        
        set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
            objConn.open SalesToolConnString()' , adopenstatic, adcmdreadonly, adcmdtext
            oRs.open strSql, objConn

        with oRs
                
        if not .bof and not .eof then                     
                        s= "["
                        do while not .eof
                            s=s & "{"
                                s=s & " ""link_id"":""" & .fields("link_id") & """"
                                s=s & " ,""company_id"":""" & .fields("company_id") & """"
                                s=s & " ,""company_name"":""" & .fields("company_name") & """"
                                s=s & " ,""company_vat"":""" & .fields("company_vat") & """"
                                s=s & " ,""company_address_postcode"":""" & .fields("company_address_postcode") & """"
                                s=s & " ,""company_address_localite"":""" & .fields("company_address_localite") & """"
                                s=s & " ,""nbc_desc"":""" & .fields(nbc_desc) & """"
                                s=s & " ,""jfc_s_desc"":""" & .fields(jfc_s_desc) & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        s = s & "]"    
                end if
           end with
    
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

        pJSONGetAffiliatedCompanies = s

    
    end function


'<END REGION JSON BUSINESS LOGIC>=============================================================



public sub Class_Terminate()
    set objFed = nothing
end sub


end class

%>