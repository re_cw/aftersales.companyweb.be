﻿
function AjaxFederation_DeleteEntry(id) {
	var url = "/classes/dal/clsFederations/clsFederations.asp";
	
	$.get(url, {a:"deleteFederation", link_id: id})
	.done(function(data){
		
	}).fail(function (resp) {
		dhtmlx.message({
			text: "Error Deleting federation Entry",
			expire: -1,
			type: "errormessage"
		});
	});
}

function AjaxFederation_GetFederationsPromise() {
	var url = "/classes/dal/clsFederations/clsFederations.asp";
	
	return $.get(url, { a: "GetComboFederationsWithImg" })
	.fail(function (resp) {
		dhtmlx.message({
			text: "Error getting Federation",
			expire: -1,
			type: "errormessage"
		});
	});
}
function AjaxFederation_GetFederationsListFromHbcId(container, id) {
		$("#" + container).empty();
		var url = "/classes/dal/clsFederations/clsFederations.asp";
		
		$.get(url, { a: "GetCompany_federationsFromHbc", hbc_id: id })
		.done(function (data) {
			$("#" + container).html(formatListFederations(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Federation from HBCID",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}
function AjaxFederation_GetFederationsList(container, id) {
		$("#" + container).empty();
		var url = "/classes/dal/clsFederations/clsFederations.asp";
		
		$.get(url, {a:"GetCompany_federations",company_id: id})
		.done(function(data){
			$("#" + container).html(formatListFederations(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Federations List",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

	function formatListFederations(obj) {
		var t = '';
		if (!$.isEmptyObject(obj)) {
			var imglookupFed = '';
			var img = '';
			var imgDel = '';
			t = "<table cellpadding=1 width=100% border=0>";
			for (var i = 0; i < obj.length; i++) {
				var id = obj[i].federation_id;
				imglookupFed = '<img id=' + id + ' src ="/graphics/common/win_16x16/search.png" class="onmouseoverlookup" onClick="WindowAffiliatedCompanies(this.id);">';
				img = '<img src="/graphics/common/imgcombo/federations/' + obj[i].federation_img + '">';
				imgDel = '<img src ="/graphics/common/win_16x16/delete.png" class="onmouseoverdel" onClick="delItem(' + obj[i].link_id + ', category);">';
				t += "<tr><td width=10%>" + imglookupFed + "</td><td width=10%>" + img + "</td><td>" + obj[i].federationname + "</td><td align=right>" + imgDel.replace("category", "'federation'") + "</td></tr>";
			}
			t += "</table>";
		}
		return t;
	}