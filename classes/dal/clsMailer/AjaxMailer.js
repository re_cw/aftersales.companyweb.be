﻿var company_vat;

function populateMailObject(objMail) {
	var params = "";
	if (objMail != null)
	{
    params = "order_list_token_id=" + encodeURIComponent(objMail.order_list_token_id);
    params += "&event_id=" + encodeURIComponent(objMail.event_id);
    params += "&company_id=" + encodeURIComponent(objMail.company_id);
    params += "&contact_id=" + encodeURIComponent(objMail.contact_id);
    params += "&user_id=" + encodeURIComponent(objMail.user_id);
    params += "&mail_template_id=" + encodeURIComponent(objMail.mail_template_id);
    params += "&Mailfrom=" + encodeURIComponent(objMail.Mailfrom);
    params += "&MailTo=" + encodeURIComponent(objMail.MailTo);
    params += "&MailCC=" + encodeURIComponent(objMail.MailCC);
    params += "&MailBCC=" + encodeURIComponent(objMail.MailBCC);
    params += "&Subject=" + encodeURIComponent(objMail.Subject);
    params += "&Body=" + encodeURIComponent(objMail.Body);
    params += "&IsHTML=" + encodeURIComponent(objMail.IsHTML);
    params += "&Attachment=" + encodeURIComponent(objMail.Attachment);
    params += "&customerlang=" + encodeURIComponent(objMail.customerlang);
    params += "&trusted=1";
    }
    
    return params;
}

function AjaxMailer_SendMailPromise(objMail) {
	var url = "/classes/dal/clsMailer/clsMailer.asp?a=1";
    var params = populateMailObject(objMail);
	
	return $.post(url,params)
	.done(function(data){
		switch(data)
		{
			case "":
				var imgok = '<img src=/graphics/common/win_32x32/Gnome-Face-Smile-Big-32.png>';
				dhtmlx.message({
					text: locale.main_page.lblMailSuccess,
					expire: 500,
					type: "successmessage"
					
				});
				dhtmlx.alert(imgok + locale.main_page.lblMailSuccess);
				
				return 1;
			default:
			    var imgnok = '<img src=/graphics/common/win_32x32/Gnome-Face-Crying-32.png>';
				dhtmlx.message({
					text: locale.main_page.lblMailFail,
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert({ type: "confirm-error", title: locale.main_page.lblMailFail, text: imgnok + data.responseText });
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error setting mail.",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}
function HTMLGetBodyByIdPromise(mailId)
{
	var url = "/classes/dal/clsMailer/clsMailer.asp";

    return $.get(url, { a: 70, mail_id: mailId })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error getting body by id",
            expire: -1,
            type: "errormessage"
        });
    });
}
function JSONGetEmailByIdPromise(mailId)
{
    var url = "/classes/dal/clsMailer/clsMailer.asp";

    return $.get(url, { a: 69, mail_id: mailId })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error getting mail by id",
            expire: -1,
            type: "errormessage"
        });
    });
}
function AjaxMailer_reSendMailPromise(objMail) {
	
	var url = "/classes/dal/clsMailer/clsMailer.asp?a=3";
    var params = populateMailObject(objMail);
	
	return $.post(url,params)
	.then(function(data)
	{
		if (data === ""){
			return data;
		}else{
			return $.Deferred().reject(data);
		}
	})
	.done(function(data){
				var imgok = '<img src=/graphics/common/win_32x32/Gnome-Face-Smile-Big-32.png>';
				dhtmlx.message({
					text: locale.main_page.lblMailSuccess,
					expire: 500,
					type: "successmessage"
					
				});
				dhtmlx.alert(imgok + locale.main_page.lblMailSuccess);
	}).fail(function (resp) {			    
		var imgnok = '<img src=/graphics/common/win_32x32/Gnome-Face-Crying-32.png>';
		dhtmlx.message({
			text: locale.main_page.lblMailFail,
			expire: -1,
			type: "errormessage"
		});
		dhtmlx.alert({ type: "confirm-error", title: locale.main_page.lblMailFail, text: imgnok + resp.responseText });
		
	    dhtmlx.message({
	        text: "Error ReSendMail",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}
