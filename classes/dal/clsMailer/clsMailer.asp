<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>

<!-- #include virtual ="/classes/obj/objEmail.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"

Dim objMailer, objMail
   
    set objMailer = new sysMailers
    set objMail = new clsObjEmail

        select case request.QueryString("a")         
            case 1 'send Email
                response.write objMailer.Send()
            case 2
                response.write objMailer.Save()
            case 3
                response.write objMailer.reSend()
            case 4
                response.write objMailer.SendGenericTemplate()
			case 5 
				response.write objMailer.SendTemplateSubject()
            case 69
                response.write objMailer.GetEmailById()
			case 70
				response.write objMailer.GetBodyById()
			case 99
				response.write objMailer.SendNotification()
        end select
    

class sysMailers

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property

    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property
    property get mail_id()
        mail_id=trim(request.QueryString("mail_id"))
        if len(mail_id) = 0 then mail_id = 0
    end property

    public function Send()
        Send = pSend()
    end function
	public function GetBodyById()
		response.contentType = "text/html" : response.write pGetBodyById()
	end function
	public function GetEmailById()
        response.ContentType = "application/json" : response.Write pGetEmailById()
    end function
	public function SendNotification()
		SendNotification = pSendNotification()
	end function

    public function reSend()
        reSend = preSend()
    end function

    public function Save()
        Save = pSave()
    end function

    public function SendGenericTemplate()
        SendGenericTemplate = pSendGenericTemplate()
    end function

	public function SendTemplateSubject()
		SendTemplateSubject = pSendTemplateSubject()
	end function


'<START REGION BUSINESS LOGIC>===========================================================
Function URLDecode(ByVal str)
 URLDecode = str
End Function

'// Replacement function for the above
Function URLDecodeHex(sMatch, lhex_digits, lpos, ssource)
URLDecodeHex = chr("&H" & lhex_digits)
End Function

private function pGetBodyById()
    Dim objConn, oRs, strSql, s            
    strSql = "SELECT Body FROM mails WHERE mail_id="  & mail_id()

    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    oRs.open strSql, objConn

	s = URLDecode(oRs.fields("Body"))

    pGetBodyById = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function
private function pGetEmailById()
    Dim objConn, oRs, strSql, s            
    strSql = "SELECT MailTo, MailCC, MailBCC, Subject, Attachment FROM mails WHERE mail_id="  & mail_id()

    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    oRs.open strSql, objConn

	s = RStoJSON(oRs)

    pGetEmailById = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function
private function pSendNotification()
	pSendNotification = objMail.SendNotification()
End function
    private function pSend()
        pSend = objMail.Send()
    end function

    private function preSend()
        preSend = objMail.reSend()
    end function

    private function pSave()
        pSave = objMail.Save()
    end function


    private function pSendGenericTemplate()
        pSendGenericTemplate = objMail.SendGenericTemplate()
    end function
	
	private function pSendTemplateSubject()
		pSendTemplateSubject = objMail.SendTemplateSubject()
	end function
       
'<END REGION BUSINESS LOGIC>=============================================================

    public sub Class_Terminate()
        set objMail = nothing
        set objMailer = nothing
    end sub

end class

%>