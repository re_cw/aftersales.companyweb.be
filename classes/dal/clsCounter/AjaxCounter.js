﻿function setSalesPosition(targetElement) {
	var url = '/classes/dal/clsCounter/clsCounter.asp'
	$.get(url, {q:3})
	.done(function(data){
			var img = data.MonthlyPosition;
			var d = $("."+targetElement);
			d.css("background-image", "url(/graphics/podium/" + img + ".png)");
	}).fail(function(){
		dhtmlx.message({
			text: "Error setting sales position",
			expire: -1,
			type: "errormessage"
		});
	});
}


function setCounter(counterType, targetElement) {
		if (!counterType) {
			$("#" + targetElement).text = "";
			$("#" + targetElement).data('requestRunning', false);
			return;
		} else {
			var url = "/classes/dal/clsCounter/clsCounter.asp";
			$.get(url, {q:counterType})
			.done(function(data){
				$("#"+targetElement).html(data);
			}).fail(function(){
				dhtmlx.message({
					text: "Error setting counter.",
					expire: -1,
					type: "errormessage"
				});
			}).always(function(){
				$("#" + targetElement).data('requestRunning', false);
			});
		}
}

function SetPositionAndCounter()
{
	setSalesPosition('cwsalesPosition');  
	setCounter(1, 'odglobal');
	setCounter(2, 'odindividual');
}