﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "EN"

Dim objCounter
    Set objCounter = new sysCounter
    
    select case request.querystring("q")
        case 1 'global
            response.write objCounter.GetGlobalCountervalue
        case 2 'local
            response.write objCounter.GetIndividualCountervalue
        case 3 'getSalesInfo
            objCounter.JSONGetSalesPosition
    end select
    
    set objCounter = nothing


class sysCounter
   
    '*** connection strings ***
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
    
    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    'for testing purpose only
    if len(trim(usr_id))= 0 then usr_id = 0
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property   

    property get firstName()
        firstName = request.Cookies("TELESALESTOOLV2")("firstName")    
        'if len(trim(firstName)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    public function GetGlobalCountervalue()
        response.write pGetSalesCountervalue(1)
    end function
    public function GetIndividualCountervalue()
        response.write pGetSalesCountervalue(2)
    end function

    public function JSONGetSalesPosition()
        Response.ContentType = "application/json" : response.write pJSONGetSalesPosition()
    end function
    


private function pGetSalesCountervalue(blnIndividual)
    Dim objConn, objRS, strSql, sWhere, varVal
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 0 then set objConn = nothing : exit function : end if
    varval=0    
    select case blnIndividual
        case 1
            strsql = "select [teverkopenabo] as retval from vw_GlobalSalesCounter"
        case 2
            strsql = "select [verkocht] as retval  from vw_PersonalSalesCounter  where aanbrenger = '" & firstname() & "'"
    end select
    
        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysCounter.pGetSalesCountervalue(blnIndividual)" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

         with objRS
            if not .bof and not .eof then       
            .moveFirst 
                varVal = cdbl(objRs.Fields("retval"))
            else
                varval=0
            end if
        end with
        
        pGetSalesCountervalue = varval
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing       
    
end function


private function pJSONGetSalesPosition()
    
    Dim objConn, objRS, strSql, sWhere, varVal
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 0 then set objConn = nothing : exit function : end if

    strSql = "select A.Verkocht as TotalSales, (select COUNT(1)+1 from vw_PersonalMonthlyCounter as B where B.bedrag > isnull(M.bedrag,0)) as MonthlyPosition "
    strSql = strSql & " from  vw_PersonalSalesCounter as A left join vw_PersonalMonthlyCounter as M on M.aanbrenger = A.aanbrenger "

    'strsql = "select A.[bedrag] as TotalSales, (select COUNT(1)+1 from vw_PersonalMonthlyCounter as B where B.bedrag > A.bedrag ) as MonthlyPosition from  vw_PersonalMonthlyCounter as A "
    sWhere = " where a.aanbrenger = '" & firstname() & "'"
    
    strsql = strsql & sWhere
    
        'on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysCounter.pGetSalesPosition()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

         with objRS
                    if not .bof and not .eof then
                        do while not .eof
                            s= "{"
                            s=s & """TotalSales"":""" & .fields("TotalSales") & """"
                            s=s & " ,""MonthlyPosition"":""" & .fields("MonthlyPosition") & """"
                            s=s & " ,""firstname"":""" & firstname() & """"
                            s=s & "}"
                        .movenext
                        loop                    
                    else            
                        s= "{""TotalSales"":""0"" ,""MonthlyPosition"":""0"" ,""firstname"":""" & firstname() & """}"
                    end if        
                end with
        
        pJSONGetSalesPosition = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing       
    

end function

Function IIf(i,j,k)
	If i Then IIf = j Else IIf = k
End Function
end class    
    
    %>