﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->

<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"

Dim objCompetitor
   
    set objCompetitor = new sysCompetitors

        select case request.QueryString("a")         
            case "GetComboCompetitorsWithImg"
                objCompetitor.GetComboCompetitorsWithImg
            case "GetCompany_Competitors"
                objCompetitor.JSONGetCompany_Competitors
            case "GetCompany_CompetitorsFromHbc"
                objCompetitor.JSONGetCompany_CompetitorsFromHbc
            case "deleteCompetitor"
                objCompetitor.deleteCompany_Competitor
            case else 'default should be read
        end select
    

class sysCompetitors

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get link_id()
        link_id=request("link_id")
    end property

    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property
    property get hbc_id()
        hbc_id = trim(request.QueryString("hbc_id"))
        if len(hbc_id) = 0 then hbc_id = -1
    end property
    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property

    public function JSONGetCompany_CompetitorsFromHbc()
        Response.ContentType = "application/json" : response.write pJSONGetCompany_CompetitorsFromHbc()
    end function

    public function GetComboCompetitorsWithImg()
        GetComboCompetitorsWithImg = pGetComboCompetitorsWithImg() :  response.write GetComboCompetitorsWithImg
    end function

    public function JSONGetCompany_Competitors() 
        Response.ContentType = "application/json" : response.write pJSONGetCompany_Competitors()
    end function

    public function deleteCompany_Competitor()
    response.write pdeleteCompany_Competitor()
    end function

'<START REGION JSON BUSINESS LOGIC>===========================================================
 
    private function pdeleteCompany_Competitor()
            Dim objConn, oRs, strSql, CompetitorName, s
            strSql = "DELETE FROM [dbo].[link_company_competitors] WHERE [link_id]=" & link_id()
            set objConn=Server.CreateObject("ADODB.Connection")
            with objConn
                .open SalesToolConnString() , adOpenDynamic, adLockOptimistic, adcmdtext
                .execute strsql
                .close
            end with
        set objConn = nothing
        if err.number <> 0 then pdeleteCompany_Competitor = err.Description
    end function



    private function pGetComboCompetitorsWithImg()       
            Dim objConn, oRs, strSql, CompetitorName, s
            CompetitorName = "competitor_name_" & UserLang() 
            strSql = strSql & " SELECT competitor_id as value, competitor_img as img, "
            strSql = strSql & "'[' + convert(nvarchar(50),(select count(*) from link_company_competitors where dbo.link_company_competitors.competitor_id = dbo.ref_competitors.competitor_id)) + '] ' + competitor_name_" & UserLang() & " as text "
            strSql = strSql & " FROM dbo.ref_competitors "
            strSql = strSql & " WHERE (competitor_enabled = 1) "
            strSql = strSql & " ORDER BY competitor_order "
            'strSql = "SELECT * FROM dbo.[vw_GetCompetitors]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
			 s= "{""options"":["
             
				s = s & RStoJSON(oRs)
						
			s = s & "]}"
            pGetComboCompetitorsWithImg = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function


private function pJSONGetCompany_CompetitorsFromHbc
        Dim a, competitorname
        a = hbc_id() 

        strSql = "select link_id, "
        strSql = strSql & " case C.company_lang_id when 1 then RC.competitor_name_FR when 2 then RC.competitor_name_NL else RC.competitor_name_EN END as competitorname, "
        strSql = strSql & " RC.competitor_img, LCC.contract_end_date  "
        strSql = strSql & " from companies C "
        strSql = strSql & " inner join link_company_competitors LCC on C.company_id = LCC.company_id "
        strSql = strSql & " inner join ref_competitors RC on LCC.competitor_id = RC.competitor_id "
        strSql = strSql & " where c.company_hbc_id=" & a
   
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn

    	if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if
    
    oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetCompany_CompetitorsFromHbc = s
end function
private function pJSONGetCompany_Competitors()
    
    Dim a, competitorname
        a = company_id() 
        strSql = "select link_id, "
        strSql = strSql & " case C.company_lang_id when 1 then RC.competitor_name_FR when 2 then RC.competitor_name_NL else RC.competitor_name_EN END as competitorname, "
        strSql = strSql & " RC.competitor_img, LCC.contract_end_date  "
        strSql = strSql & " from companies C "
        strSql = strSql & " inner join link_company_competitors LCC on C.company_id = LCC.company_id "
        strSql = strSql & " inner join ref_competitors RC on LCC.competitor_id = RC.competitor_id "
        strSql = strSql & " where c.company_id=" & a

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
                              
	    if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if
    
    oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetCompany_Competitors = s


end function
'<END REGION JSON BUSINESS LOGIC>=============================================================



public sub Class_Terminate()
    set objCompetitor = nothing
end sub


end class

%>