﻿
function AjaxCompetitor_DeleteEntry(id) {
    var url = "/classes/dal/clsCompetitors/clsCompetitor.asp";
	
	$.get(url, { a:"deleteCompetitor", link_id:id})
	.done(function(data){
		dhtmlx.message({
			text: locale.main_page.lblSaveSuccess,
			expire: 500,
			type: "successmessage"
		});
	}).fail(function(resp){
		dhtmlx.message({
			text: locale.main_page.lblActionCancelled,
			expire: -1,
			type: "errormessage"
		});
	});
}

function AjaxCompetitor_GetCompetitorsPromis() {
	var url = "/classes/dal/clsCompetitors/clsCompetitor.asp";
	
	return $.get(url, { a:"GetComboCompetitorsWithImg"})
	.fail(function(resp){
		dhtmlx.message({
			text: locale.main_page.lblActionCancelled,
			expire: -1,
			type: "errormessage"
		});
	});
}

function AjaxCompetitor_GetCompetitorsList(container, id) {
		$("#" + container).empty();
		var url = "/classes/dal/clsCompetitors/clsCompetitor.asp";
		$.get(url, {a:"GetCompany_Competitors", "company_id":id})
		.done(function(data){
			$("#" + container).html(formatListCompetitors(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Competitors List",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}
function AjaxCompetitor_GetCompetitorsListFromHbcId(container, id) {
		$("#" + container).empty();
		
		var url = "/classes/dal/clsCompetitors/clsCompetitor.asp";
		$.get(url, { a: "GetCompany_CompetitorsFromHbc", hbc_id: id })
		.done(function (data) {
			$("#" + container).html(formatListCompetitors(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Competitors List From HBCID",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

function formatListCompetitors(obj) {
    var t = '';
    if (!$.isEmptyObject(obj))
    {
        var img = '';
        var imgDel = '';
        t = "<table cellpadding=1 width=100% border=0>";
        for (var i = 0; i < obj.length; i++) {
            img = '<img src="/graphics/common/imgcombo/competitors/' + obj[i].competitor_img + '">';
            imgDel = '<img src ="/graphics/common/win_16x16/delete.png" class="onmouseoverdel" onClick="delItem(' + obj[i].link_id + ', category);">';
            t += "<tr><td width=25px>" + img + "</td><td>" + obj[i].competitorname + "</td><td>" + obj[i].contract_end_date + "</td><td width=25px  align=right>" + imgDel.replace("category", "'competitor'") + "</td></tr>";
        }
        t += "</table>";
    }
    return t;
}

