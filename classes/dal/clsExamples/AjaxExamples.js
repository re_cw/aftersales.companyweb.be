
function JSON_GetDagvaardingen(container)
{
	var url = "/classes/dal/clsExamples/examples.asp";
	$.get(url, {a:"JSON_GetDagvaardingen"})
	.done(function(data){
		container.attachHTMLString(GenerateTable(b.dagvaardingen));
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error getting Dagv",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function JSON_GetInhoudingen(container)
{
	var url = "/classes/dal/clsExamples/examples.asp";
	$.get(url, {a:"JSON_GetInhoudingen"})
	.done(function(data){
		container.attachHTMLString(GenerateTable(b.inhoudingen));
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error getting Inh",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function JSON_GetManueel(container)
{
	var url = "/classes/dal/clsExamples/examples.asp";
	$.get(url, {a:"JSON_GetManueel"})
	.done(function(data){
		container.attachHTMLString(GenerateTable(b.handmatig));
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error getting Man",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function GenerateTable(json)
{
	var html = "";
	html += "<table class='dhtmlxGrid'>";
	html += "<tr>";
	html += "<th>date</th>";
	html += "<th>vat</th>";
	html += "<th>name</th>";
	html += "<th>vorm</th>";
	html += "<th>town</th>";
	html += "<th>zip</th>";
	html += "<th>moreinfo</th>";
	html += "</tr>";
	
	for (var i = 0; i < json.length; i++)
	{
		html += "<tr>";
		html += "<td>"+ json[i].date + "</td>";
		html += "<td>"+ json[i].vat + "</td>";
		html += "<td>"+ json[i].name+"</td>";
		html += "<td>"+ json[i].vorm+"</td>";
		html += "<td>"+ json[i].town+"</td>";
		html += "<td>"+ json[i].zip+"</th>";
		html += "<td>"+ json[i].moreinfo+"</th>";
		html += "</tr>";
	}
	html += "</tr>";
	
	return html;
	
}