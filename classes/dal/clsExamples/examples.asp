<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<%
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"

Dim objExample

set objExample = new sysExample

	select case request.Querystring("a")
		case "JSON_GetDagvaardingen"
			objExample.JSON_GetDagvaardingen
		case "JSON_GetInhoudingen"
			objExample.JSON_GetInhoudingen
		case "JSON_GetManueel"
			objExample.JSON_GetManueel
	end select
	
	set objExample = nothing
	
class sysExample
    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get BoConnString()
        BoConnString = sConnBoString
    end property
	
	public function JSON_GetDagvaardingen()
		JSON_GetDagvaardingen=pJSON_GetDagvaardingen(): Response.ContentType = "application/json": response.write JSON_GetDagvaardingen
	end function
		public function JSON_GetInhoudingen()
		JSON_GetInhoudingen=pJSON_GetInhoudingen(): Response.ContentType = "application/json": response.write JSON_GetInhoudingen
	end function
		public function JSON_GetManueel()
		JSON_GetManueel=pJSON_GetManueel(): Response.ContentType = "application/json": response.write JSON_GetManueel
	end function
	
	private function pJSON_GetDagvaardingen()
	    Dim objConn, oRs, strSql
	
    strSql = "SELECT qs_id, qs_datum, qs_vat, qs_" & Userlang() & "Name as qs_Name, qs_"&Userlang()& "vorm as qs_vorm, qs_" & Userlang() & "Gemeente as qs_gemeente, qs_postcode, qs_more from ST_QuickSamples where qs_type = 'DAGVAARDING' ORDER BY qs_datum desc"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pGetCompanyHistory = strSql
        oRs.open strSql, objConn
		
		s= "{""rows"":["
		with oRs
            if not .bof and not .eof then
                .moveFirst 
                do while not .eof
					s = s & "{"
					s = s & """id"":" & .fields("qs_id") & ", "
					s = s & """data"":["
					s = s & """" & .fields("qs_datum") & """," 
					s = s & """" & .fields("qs_vat") & ""","
					s = s & """" & .fields("qs_Name") & ""","
					s = s & """" & .fields("qs_Vorm") & ""","
					s = s & """" & .fields("qs_Gemeente") & ""","
					s = s & """" & .fields("qs_PostCode") & ""","
					s = s & """" & .fields("qs_more") & """]},"
                    .movenext
                loop       
				s = left(s, (len(s)-1))  
			end if
		End With
		s = s & "]}"  
		pJSON_GetDagvaardingen = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function

	private function pJSON_GetInhoudingen()
	    Dim objConn, oRs, strSql
	
    strSql = "SELECT qs_id, qs_datum, qs_vat, qs_" & Userlang() & "Name as qs_Name, qs_"&Userlang()& "vorm as qs_vorm, qs_" & Userlang() & "Gemeente as qs_gemeente, qs_postcode, qs_more from ST_QuickSamples where qs_type = 'INHOUDING' ORDER BY qs_datum desc"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pGetCompanyHistory = strSql
        oRs.open strSql, objConn
		
		s= "{""rows"":["
		with oRs
            if not .bof and not .eof then
                .moveFirst 
                do while not .eof
					s = s & "{"
					s = s & """id"": " & .fields("qs_id") & ", "
					s = s & """data"": ["
					s = s & """" & .fields("qs_datum") & """," 
					s = s & """" & .fields("qs_vat") & ""","
					s = s & """" & .fields("qs_Name") & ""","
					s = s & """" & .fields("qs_Vorm") & ""","
					s = s & """" & .fields("qs_Gemeente") & ""","
					s = s & """" & .fields("qs_PostCode") & ""","
					s = s & """" & .fields("qs_more") & """]},"
                    .movenext
                loop       
				s = left(s, (len(s)-1))  
			end if
		End With
		s = s & "]}"  
		pJSON_GetInhoudingen = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function
	
	private function pJSON_GetManueel()
	    Dim objConn, oRs, strSql
	
    strSql = "SELECT qs_id, qs_datum, qs_vat, qs_" & Userlang() & "Name as qs_Name, qs_"&Userlang()& "vorm as qs_vorm, qs_" & Userlang() & "Gemeente as qs_gemeente, qs_postcode, qs_more from ST_QuickSamples where qs_type = 'HANDMATIG' ORDER BY qs_datum desc"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pGetCompanyHistory = strSql
        oRs.open strSql, objConn
		
		s= "{""rows"":["
		with oRs
            if not .bof and not .eof then
                .moveFirst 
                do while not .eof
					s = s & "{"
					s = s & """id"": " & .fields("qs_id") & ", "
					s = s & """data"": ["
					s = s & """" & .fields("qs_datum") & """," 
					s = s & """" & .fields("qs_vat") & ""","
					s = s & """" & .fields("qs_Name") & ""","
					s = s & """" & .fields("qs_Vorm") & ""","
					s = s & """" & .fields("qs_Gemeente") & ""","
					s = s & """" & .fields("qs_PostCode") & ""","
					s = s & """" & .fields("qs_more") & """]},"
                    .movenext
                loop       
				s = left(s, (len(s)-1))  
			end if
		End With
		s = s & "]}"  
		pJSON_GetManueel = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function
end class
%>
