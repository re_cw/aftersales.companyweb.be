﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
option explicit
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->

<%
Const C_CW_LOGO_IMG = "../../../graphics/logo-nl-500.png"
Const C_LOGIN_BG = "../../../graphics/bg_login.jpg"
Const C_LOGIN_PAGE_SOURCE = "login.asp"
Const C_LOGIN_SUCCESS = "main.asp"
Const C_LOGIN_FAILED = "error.asp"

    select case request("a")
    case -1
        Response.Cookies("TELESALESTOOLV2").Expires = DateAdd("d",-1,Now())
        session.Abandon
    end select

class LoginUser    

    'local variables to authenticate the user
    Dim m_pagesource, m_errLogin, m_errPassword, m_userLogin, m_userPassword
    
    'local variables for data propagation through the application by use of User Object
    Dim m_currentUser, m_currentPassword, m_lang_radical, m_lang_name, m_role_name
    
    Dim m_usr_id, m_usr_lang_id, m_usr_role_id, m_extension

    Dim m_firstname,m_lastname, m_phone1, m_email1, m_hr_function, m_hr_functionid, m_picture

    

'<start region PROPERTIES>
    property get objConnString()
        objConnString = sConnString    
    end property

    property get pagesource()
        pagesource = m_pagesource
    end property
        property let pagesource(varVal)
            m_pagesource=varVal
        end property
  
    property get errLogin()
        if len(trim(request.form("txtLogin"))) = 0 then errLogin = "Login cannot be empty"
    end property
    
    property get errPassword()
        if len(trim(request.form("txtPassword"))) = 0 then errPassword = "Password cannot be empty"
    end property

    property get userLogin()
        Dim sStr
        sStr=request.form("txtLogin")
        if len(trim(sStr)) > 0 then
            userLogin = sStr
        else
            m_errLogin = "Login cannot be empty"  
    end if
    end Property
        property let userLogin(varVal)
            m_userLogin=varVal
        end property

    property get userPassword()
        Dim sStr
        sStr=request.form("txtPassword")
        if len(trim(sStr)) > 0 then
            userPassword = sStr
        else
            m_errPassword = "Password cannot be empty"      
    end if
    end property
            property let userPassword(varVal)
             m_userPassword=varVal
            end property

    Property get currentUser()
        currentUser=m_currentUser
    end property
        Property let currentUser(varVal)
            m_currentUser=varVal
        end property

        Property get currentPassword()
        currentPassword=m_currentPassword
    end property
        Property let currentPassword(varVal)
            m_currentPassword=varVal
        end property

    Property get lang_radical()
        lang_radical=m_lang_radical
    end property
        Property let lang_radical(varVal)
            m_lang_radical=varVal
        end property

    Property get lang_name()
        lang_name=m_lang_name
    end property
        Property let lang_name(varVal)
            m_lang_name=varVal
        end property

    Property get role_name()
        role_name=m_role_name
    end property
        Property let role_name(varVal)
            m_role_name=varVal
        end property

    Property get usr_id()
        usr_id=m_usr_id
    end property
        Property let usr_id(varVal)
            m_usr_id=varVal
        end property

    Property get usr_lang_id()
        usr_lang_id=m_usr_lang_id
    end property
        Property let usr_lang_id(varVal)
            m_usr_lang_id=varVal
        end property

    Property get usr_role_id()
        usr_role_id=m_usr_role_id
    end property
        Property let usr_role_id(varVal)
            m_usr_role_id=varVal
        end property
    Property get extension()
        extension=m_extension
    end property
    Property let extension(varVal)
        m_extension=varVal
    end property

    Property get firstname(): firstname=m_firstname: end property
    Property let firstname(varVal):  m_firstname=varVal: end property

    Property get lastname(): lastname=m_lastname: end property
    Property let lastname(varVal):  m_lastname=varVal: end property

    Property get hr_function(): hr_function=m_hr_function: end property
    Property let hr_function(varVal):  m_hr_function=varVal: end property

    Property get phone1(): phone1=m_phone1: end property
    Property let phone1(varVal):  m_phone1=varVal: end property
    
    Property get email1(): email1=m_email1: end property
    Property let email1(varVal):  m_email1=varVal: end property

    Property get hr_functionid(): hr_functionid=m_hr_functionid: end property
    Property let hr_functionid(varVal):  m_hr_functionid=varVal: end property
    
    Property get picture(): picture=m_picture: end property
    Property let picture(varVal):  m_picture=varVal: end property
    
    
'<end region PROPERTIES>

'<start region PUBLIC METHODS & FUNCTIONS>
    sub Authenticate()
        pAuthenticate()
    end sub

    sub UpdateLangageSettings()
        pUpdateLangageSettings()
    end sub

    function DisplayLoginForm()
        DisplayLoginForm = pDisplayLoginForm()
        response.write DisplayLoginForm 
    end function

'<end region PUBLIC METHODS & FUNCTIONS>

'<start region PRIVATE METHODS & FUNCTIONS>
    private sub pAuthenticate()
    Dim function_name
        Dim objConn, oRs, strSql    
        strSql = "SELECT * FROM vw_UsersLogin WHERE usr_id > 0 and usr_login='@userLogin' and usr_password='@userPassword'"
        strSql = replace(strSql, "@userLogin", userLogin, 1,3,1)
        strSql = replace(strSql, "@userPassword", userPassword, 1,3,1)
       
        set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open objConnString()
        'if Err.number = 0 and objConn.Errors.Count = 0 then
        oRs.open strSql, objConn
        with oRs
            if not .bof and not .eof then
                Do while not .eof
                    currentUser = .fields("usr_login")
                    currentPassword = .fields("usr_password")
                    lang_radical = .fields("lang_radical")
                    lang_name = .fields("lang_name")
                    role_name = .fields("role_name")
                    usr_id = CInt(.fields("usr_id"))
                    usr_lang_id = CInt(.fields("usr_lang_id"))
                    usr_role_id = CInt(.fields("usr_role_id"))
                    extension = .fields("extension")
                    firstname = .fields("firstname")
                    lastname = .fields("lastname")
                    phone1 = .fields("phone1")
                    email1 = .fields("email1")
                        function_name = "function_name_" & lang_radical
                    hr_function = .fields(function_name)
                    hr_functionid = .fields("function_id")
                    picture = .fields("picture")
                .movenext
                loop
                pSetSessionsVariables
                pBuildCookie
                pagesource= C_LOGIN_SUCCESS
                else
                    pagesource= C_LOGIN_FAILED
                end if
        end with
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing
        pnavigate
        'end if
    end sub

    private sub pSetSessionsVariables()      
        Session.LCID=2067'Dutch Belgium
        Session.Timeout=1440'1440 minutes = 24hrs
        Session("lang_radical")=objUser.lang_radical
        Session("lang_name")=objUser.lang_name
        Session("role_name")=objUser.role_name
        Session("currentUser")=objUser.currentUser
        Session("currentPassword")=objUser.currentPassword
        Session("usr_id")=Cint(objUser.usr_id)
        Session("usr_lang_id")=CInt(objUser.usr_lang_id)
        Session("usr_role_id")=CInt(objUser.usr_role_id)
        Session("extension")=CInt(objUser.extension)
        Session("firstname")=objUser.firstname
        Session("lastname")=objUser.lastname
        Session("phone1")=objUser.phone1
        Session("email1")=objUser.email1
        Session("hrfuncname")=objUser.hr_function
        Session("hrfunctionid")=objUser.hr_functionid
        Session("picture")=objUser.picture
    end sub

    private sub pBuildCookie()
        response.buffer =true
            Response.cookies("TELESALESTOOLV2").expires=date() + 365    
            Response.Cookies ("TELESALESTOOLV2")("lang_radical")=objUser.lang_radical
            Response.Cookies ("TELESALESTOOLV2")("lang_name")=objUser.lang_name
            Response.Cookies ("TELESALESTOOLV2")("role_name")=objUser.role_name
            Response.Cookies ("TELESALESTOOLV2")("currentUser")=objUser.currentUser
            Response.Cookies ("TELESALESTOOLV2")("currentPassword")=objUser.currentPassword
            Response.Cookies ("TELESALESTOOLV2")("usrId")=objUser.usr_id
            Response.Cookies ("TELESALESTOOLV2")("usrRoleId")=objUser.usr_role_id
            Response.Cookies ("TELESALESTOOLV2")("usr_lang_id")=objUser.usr_lang_id
            Response.Cookies ("TELESALESTOOLV2")("extension")=objUser.extension
            Response.Cookies ("TELESALESTOOLV2")("firstname")=objUser.firstname
            Response.Cookies ("TELESALESTOOLV2")("lastname")=objUser.lastname
            Response.Cookies ("TELESALESTOOLV2")("phone1")=objUser.phone1
            Response.Cookies ("TELESALESTOOLV2")("email1")=objUser.email1
            Response.Cookies ("TELESALESTOOLV2")("hrfuncname")=objUser.hr_function
            Response.Cookies ("TELESALESTOOLV2")("hrfunctionid")=objUser.hr_functionid
            Response.Cookies ("TELESALESTOOLV2")("picture")=objUser.picture
    end sub

    private sub pnavigate()
        response.redirect pagesource
    end sub


    private function pDisplayLoginForm()
    dim s, errMsg
      s = "<html><head><title>TeleSales Tool</title><meta charset=""utf-8"" />"
    s=s & "<link href=""css/login.css"" rel=""stylesheet"" />"
    s=s & "<link href=""css/animate.min.css"" rel=""stylesheet"" /></head><body>"

    If request.querystring("t")=1 then    
        objUser.pagesource= C_LOGIN_PAGE_SOURCE
        errMsg = objUser.errLogin & " " & objUser.errPassword
        if len(trim(errMsg)) = 0 then objUser.Authenticate
    end if

s=s & "<div class=""centerFlex""><div id=container class=""animated zoomIn"" style=""margin:1%; width:676px; height:470px;""><table class=""loginMainTable"">"
s=s & "<tr><td style=""height:125px; margin-top:30px;margin-left:60px;margin-bottom:25px;"" colspan=2>&nbsp;&nbsp;&nbsp;<img alt=""CompanyWebLogo"" src=" & C_CW_LOGO_IMG & " style=""width:300px;height:auto;"" /></td></tr>"
s=s & "<tr><td></td><td style=""height:80px;width:350px; line-height: 25px; text-align: right; padding-right: 45px;""><span class=""appTele"">TELE</span><span class=""appSales"">SALES</span><span class=""appTool"">TOOL</span><br><span class=""appVersion"">V02.00.00</span></td></tr><tr>"
s=s & "<td id=""errMsg"" colspan=2 style=""height:25px; text-align:right; padding-right: 45px; font-family:tahoma, arial, sans-serif; font-size:12pt; font-weight: 900; color:red;"">" & errMsg & "</td></tr>"
s=s & "<tr><td></td><td style=""height:120px;text-align:right;""><form method=""post"" name=""formLogin"" action=" & C_LOGIN_PAGE_SOURCE & "?t=1>"
s=s & "<table style=""margin-left:auto; margin-right:auto;width: 320px;""><tr><td class=""loginLabel"">Login</td>"
s=s & "<td style=""text-align: right""><input id=""txtLogin"" name=""txtLogin"" type=""text"" class=""txtInput"" /></td></tr><tr>"
s=s & "<td class=""loginLabel"">Password</td><td style=""text-align: right""><input id=""txtPassword"" name=""txtPassword"" type=""password"" class=""txtInput"" /></td></tr>"
s=s & "<tr><td colspan=2 style=""text-align: right""><input id=""Submit1"" type=""submit"" value=""Login"" class=""butt"" /></td></tr></table></form></td></tr><tr>"
s=s & "<td style=""height:55px"" colspan=""2""></td></tr><tr><td class=""appCopyright"" style=""height:30px;text-align:center;"" colspan=2>copyright &copy; 2016 Companyweb</td></tr></table></div></div>"
s=s & "</body></html>"

    pDisplayLoginForm = s

    end function

'<end region PRIVATE METHODS & FUNCTIONS>
    
    public sub Class_Terminate()
            
    end sub
end Class


%>

