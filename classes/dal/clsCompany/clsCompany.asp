﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objCompany.asp" -->
<!-- #include virtual ="/classes/obj/objCompanyAddress.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objStCompany, objCompany, objDbCompany

    Set objStCompany = new sysCompany
    Set objCompany = new clsObjCompany
	Set objCompanyAddress = new clsObjCompanyAddress

    select case request("a")
        case "JSONGetStCompanyCombo"
            objStCompany.JSONGetStCompanyCombo()
        case "GetStCompanyDetails"
            objStCompany.GetStCompanyDetails()
        case "JSONGetCompanyInfo"
            objStCompany.JSONGetStCompanyDetails()
		case "JSONGetCompanyInfoHbc"
			objStCompany.JSONGetStCompanyDetailsFromHBC()
        case "SaveCompanyBusinessData" 
            objCompany.populateCompanyObject()     
            objStCompany.SaveCompanyBusinessData(objCompany)
        case "GetComboCompaniesMailTemplates"
            objStCompany.GetComboCompaniesMailTemplates()
        case "UpdateBackOfficeID"
            objCompany.populateCompanyObject()
            objStCompany.UpdateBackOfficeID(objCompany)
		case "UpdateAddress"
			objCompanyAddress.populateCompanyAddressObject()
			objStCompany.UpdateAddress(objCompanyAddress)
		case "UpdateCompanyName"
		    objCompany.populateCompanyObject()
            objStCompany.UpdateCompanyName(objCompany)
        case "JSONGetCompaniesWithoutBackOfficeId"
            objStCompany.JSONGetCompaniesWithoutBackOfficeId()
        case "GetCompanyIdFromContactId"
            objStCompany.GetCompanyIdFromContactId()
        case "JSONGetCompanyInfoFromVat"
            objStCompany.JSONGetCompanyInfoFromVat()
		case "ConvertBackofficeToSalesTool"
			objStCompany.ConvertBackofficeToSalesTool()
    case else
    end select


    

class sysCompany


    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
	property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
            
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    'for testing purpose only
    if len(trim(usr_id))= 0 then usr_id = 0
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get event_id()
        event_id=trim(request.querystring("event_id"))
        if len(event_id) = 0 then event_id = 0
    end property

	property get company_hbc_id()
		company_hbc_id=trim(request.querystring("company_hbc_id"))
		if len(company_hbc_id) = 0 then company_hbc_id = 0
	end property
	property get contact_id()
        contact_id=trim(request.querystring("contact_id"))
        if len(contact_id) = 0 then contact_id = -1
    end property
    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property

    property get fieldname()
        fieldname=trim(request.querystring("fieldname"))
        if len(fieldname) = 0 then fieldname = 0
    end property
    property get company_vat()
        company_vat = trim(request.QueryString("company_vat"))
        if len(company_vat) = 0 then company_vat = 0
    end property
    property get fieldvalue()
        fieldvalue=trim(request.querystring("fieldvalue"))
        if len(fieldvalue) = 0 then fieldvalue = 0
    end property
    public function JSONGetCompanyInfoFromVat
        Response.ContentType = "application/json" : response.write pJSONGetCompanyInfoFromVat() 
    end function
    public function JSONGetStCompanyCombo()
        Response.ContentType = "application/json" : response.write pJSONGetStCompanyCombo() 
    end function

    public function GetStCompanyDetails()
        GetStCompanyDetails = pGetStCompanyDetails() : response.write GetStCompanyDetails 
    end function

	public function JSONGetStCompanyDetailsFromHBC()
		Response.ContentType = "application/json" : response.write pJSONGetStCompanyDetailsFromHBC
	end function
	
    public function JSONGetStCompanyDetails() 
        'response.write server.htmlencode(pJSONGetStCompanyDetails()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetStCompanyDetails()
    end function

    public function SaveCompanyBusinessData(objCompany)
        SaveCompanyBusinessData = pSaveCompanyBusinessData(objCompany)
    end function

	public function UpdateAddress(objCompanyAddress)
		Response.ContentType = "text/plain": response.write pUpdateAddress(objCompanyAddress)
	end function
	public function UpdateCompanyName(objCompany)
		Response.ContentType = "text/plain": response.write pUpdateCompanyName(objCompany)
	end function
    public function UpdateBackOfficeID(objCompany)
        UpdateBackOfficeID = pUpdateBackOfficeID(objCompany)
    end function

    public function GetComboCompaniesMailTemplates()
        GetComboCompaniesMailTemplates = pGetComboCompaniesMailTemplates() : response.write GetComboCompaniesMailTemplates
    end function
    public function GetCompanyIdFromContactId()
        Response.ContentType = "text/html" : response.write pGetCompanyIdFromContactId()
    end function
    public function JSONGetCompaniesWithoutBackOfficeId()
        Response.ContentType = "application/json" : response.write pJSONGetCompaniesWithoutBackOfficeId()
    end function
	public function ConvertBackofficeToSalesTool()
		Response.ContentType = "application/json" : response.write pConvertBackofficeToSalesTool()
	end function

'<start region PRIVATE METHODS & FUNCTIONS>
    private function pConvertBackofficeToSalesTool()
	pConvertBackofficeToSalesTool = 0
	'if company_vat is not supplied, break!
	if len(trim(company_vat())) = 0 then exit function
	
	Dim objConn, objRs, strSql, company_id, company_address_id, company_contact_id
	dim strSql2, objRS2, objConn2
	set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open
    end with
		
    if objConn.State = 0 then exit function
	
	objConn.BeginTrans
	
    set objConn2=Server.CreateObject("ADODB.Connection")

    objConn2.ConnectionString = BackOfficeConnString()
    objConn2.CursorLocation = 3
    objConn2.Open

	if objConn2.State = 0 then exit function
	
	
	strsql = "SELECT TOP 1 * FROM [dbo].[companies] WHERE company_vat=" & company_vat()
	
	set objRs = Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3
    with objRs
		if .bof and .eof then
            strSql2 = "SELECT TOP 1 HBC_ID, HBC_FirmName, HBC_tel, HBC_contactemail, F_HoofdNace, f_PrefLangId "
			strSql2 = strSql2 & " FROM [dbo].[HB_Company] " 
			strSql2 = strSql2 & " left join [dbo].[CW_Firma] on HBC_Firmvat = F_VAT "
			strSql2 = strSql2 & " where HBC_Firmvat=" & company_vat() 
			Response.write strSql2
			
            set objRs2 = Server.CreateObject("ADODB.Recordset")
                
            objRS2.open strSql2, objConn2, 1, 3
			if not objRS2.bof and not objRS2.eof then
				.AddNew
			
				.fields("company_vat") = company_vat()
				.fields("company_hbc_id") = objRS2.fields("HBC_ID")
				.fields("company_name") = objRS2.fields("HBC_FirmName")
				.fields("company_lang_id") = objRS2.fields("f_PrefLangId")
				.fields("company_tel") = objRS2.fields("HBC_tel")
				.fields("company_mail") = objRS2.fields("HBC_contactemail")
				.fields("company_nace") = objRS2.fields("F_HoofdNace")
				
				.fields("AuditUserCreated") = usr_id()
				.fields("AuditDateCreated") = now()
				.Update
				
				company_id = .fields("company_id")
				.close
				objRS2.close
			else
				Exit function
			End if
		Else
			Exit function
		end if
	end with
		
	strsql = "SELECT TOP 1 * FROM dbo.company_address WHERE company_id=" & company_id
	set objRs = Server.CreateObject("ADODB.Recordset")
	objRS.open strSql, objConn, 1, 3
		
	with objRS
		if .eof and .bof then
			
            strSql2 = "SELECT TOP 1 HBC_Street, HBC_HouseNr, HBC_BusNr, HBC_Postcode, HBC_gemeente "
			strSql2 = strSql2 & " FROM [dbo].[HB_Company] " 
			strSql2 = strSql2 & " where HBC_Firmvat=" & company_vat() 
                
            set objRs2 = Server.CreateObject("ADODB.Recordset")
                
            objRS2.open strSql2, objConn2, 1, 3
			if not objRS2.bof and not objRS2.eof then		
				.addNew
			
				.fields("company_id") = company_id
				.fields("address_type_id") = 4
				.fields("company_address_street") = objRS2.fields("HBC_Street")
				.fields("company_address_number") = objRS2.fields("HBC_HouseNr")
				.fields("company_address_boxnumber") = objRS2.fields("HBC_BusNr")
				.fields("company_address_postcode") = objRS2.fields("HBC_Postcode")
				.fields("company_address_localite") = objRS2.fields("HBC_gemeente")
				
				.fields("AuditUserCreated") = usr_id()
				.fields("AuditDateCreated") = now()
				.Update
				company_address_id = .fields("company_address_id")
				.close
				objRS2.close
			Else
				Exit function
			End if
		Else
			Exit function
		End if
	end with
	
	strsql = "SELECT TOP 1 * FROM dbo.contacts WHERE 1=0"
	set objRs = Server.CreateObject("ADODB.Recordset")
	objRS.open strSql, objConn, 1, 3
		
	with objRS
		if .eof and .bof then
            strSql2 = "SELECT TOP 1 HBC_ContactName, HBC_tel, HBC_contactemail, HBU_ID, HBU_intLang "
			strSql2 = strSql2 & " FROM [dbo].[HB_Company] " 
			strSql2 = strSql2 & " LEFT JOIN [dbo].[HB_USERS] on HBC_ID = HBU_HBC_ID " 
			strSql2 = strSql2 & " where HBC_Firmvat=" & company_vat() 
			
            set objRs2 = Server.CreateObject("ADODB.Recordset")
                
            objRS2.open strSql2, objConn2, 1, 3
			if not objRS2.bof and not objRS2.eof then
				.addNew
			
				.fields("contact_lang_id") = objRS2.fields("HBU_intLang")
				.fields("contact_firstname") = objRS2.fields("HBC_ContactName")
				.fields("contact_lastname") = objRS2.fields("HBC_ContactName")
				.fields("contact_tel") = objRS2.fields("HBC_tel")
				.fields("contact_email") = objRS2.fields("HBC_contactemail")
				.fields("contact_HBU_ID") = objRS2.fields("HBU_ID")
				
				.fields("AuditUserCreated") = usr_id()
				.fields("AuditDateCreated") = now()
				.Update
				company_contact_id = .fields("contact_id")
				.close
				objRS2.close
			Else
				Exit function
			End if
		Else
			Exit function
		End if
	end with
	
	strsql = "SELECT TOP 1 * FROM dbo.link_company_address_contacts WHERE 1=0"
	set objRs = Server.CreateObject("ADODB.Recordset")
	objRS.open strSql, objConn, 1, 3
		
	with objRS
		if .eof and .bof then               
				.addNew
			
				.fields("company_address_id") = company_address_id
				.fields("contact_id") = company_contact_id
				.fields("contact_type_id") = 6
				
				.fields("AuditUserCreated") = usr_id()
				.fields("AuditDateCreated") = now()
				.Update
				company_contact_id = .fields("contact_id")
				.close
		Else
			Exit function
		End if
	end with
	
	if Err.number = 0 then
		objConn.CommitTrans
		pConvertBackofficeToSalesTool = 1
	Else
		objConn.RollbackTrans
		pConvertBackofficeToSalesTool = pConvertBackofficeToSalesTool & sErrMsg
	end if
	
	set objConn = nothing
	set objConn2 = nothing
end function
    private function pJSONGetCompanyInfoFromVat()       
    Dim objConn, oRs, strSql, CompetitorName, s
            
    strSql = "select C.company_id, c.company_vat, c.company_hbc_id, c.company_name, "
    strSql = strSql & " C.company_tel, C.company_mobile, c.company_mail, C.company_nace, "
    strSql = strSql & " CA.company_address_id, CA.company_address_street, CA.company_address_number, CA.company_address_boxnumber, "
    strSql = strSql & " CA.company_address_floor, CA.company_address_postcode, CA.company_address_localite, "
    strSql = strSql & " L.lang_radical, L.lang_name, KBO.nbc_desc_" & userlang() & " as nbc_desc, F.jfc_s_desc_" & userlang() & " as jfc_s_desc "
    strSql = strSql & " from companies C  "
    strSql = strSql & " left join company_address CA on C.company_id = CA.company_id "
    strSql = strSql & " left join ref_lang L on C.company_lang_id = L.lang_id "
    strSql = strSql & " left join ref_KBO_Nacebel2008 KBO on C.company_nace = KBO.nbc_code "
    strSql = strSql & " left join ref_legal_forms F on C.jfc_code = F.jfc_code "
    strSql = strSql & " where C.company_vat = " & company_vat()

	'response.write strsql
    
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    oRs.open strSql, objConn
             
        with oRs
            if not .bof and not .eof then    
				s = "[" & RStoJSON(oRs) & "]"
			end if
        End With
            pJSONGetCompanyInfoFromVat = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
end function

private function pJSONGetStCompanyCombo()       
    Dim objConn, oRs, strSql, CompetitorName, s
            
    strSql = "SELECT company_id, company_img, company_name as text FROM [dbo].[vw_GetCompaniesCombo] WHERE usr_id=" & usr_id() & " and company_id is not null order by company_name asc"
    
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    oRs.open strSql, objConn
         s= "{options:["
        with oRs
			if not oRs.bof and not oRs.eof then
				s = RStoJSON(oRs)
			end if
        End With
		s = s & "]}"
        
		pJSONGetStCompanyCombo = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
end function
private function pGetCompanyIdFromContactId()
        Dim objConn, objRS, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State <> 1 then exit function

    strSql = "select C.company_id " 
    strSql = strSql & "  from companies C "
    strSql = strSql & "  inner join company_address CA on C.company_id = CA.company_id "
    strSql = strSql & " inner join link_company_address_contacts LCAC on CA.company_address_id = LCAC.company_address_id "
    strSql = strSql & "  inner join contacts CO on LCAC.contact_id = CO.contact_id "
    strSql = strSql & " WHERE CO.contact_id = " & contact_id() 
    
    set objRS=Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3 

        with objRS
            if not .bof and not .eof then
            
            .moveFirst 
                    do while not .eof
                        s = .fields("company_id")
                    .movenext
                loop       
            end if
        end with


        pGetCompanyIdFromContactId = s

        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function
Private function pJSONGetCompaniesWithoutBackOfficeId()
    Dim objConn, objRS, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State <> 1 then exit function
    Dim eventTypeName
    eventTypeName = "event_Type_Name_" & userlang()

    strSql = "SELECT companies.company_id, companies.company_vat, companies.company_hbc_id, companies.company_name, event_types.event_type_name_FR, " 
    strSql = strSql & " events.id, events.start_date, events.end_date, events.text, events.usr_id, " & eventTypeName
    strSql = strSql & " FROM companies INNER JOIN "
    strSql = strSql & " events ON companies.company_id = events.company_id INNER JOIN "
    strSql = strSql & " event_types ON events.event_type_id = event_types.event_type_id "
    strSql = strSql & " WHERE(companies.company_hbc_id IS NULL) "
    strSql = strSql & " AND(events.usr_id =" & usr_id() & ")" 
    strSql = strSql & " order by start_date "

    set objRS=Server.CreateObject("ADODB.Recordset")
    objRS.open strSql, objConn, 1, 3 

        with objRS
            if not .bof and not .eof then
            
            .moveFirst 
                 s= "{rows:["
                    do while not .eof
                        s = s & "{id:""" & toUnicode(.fields("id")) & """, data: [""" & toUnicode(.fields("company_id")) & """,""" & toUnicode(.fields("company_hbc_id"))  & """,""" & toUnicode(.fields("company_name"))  & """, """ & toUnicode(.fields("company_vat"))  & """, """ & toUnicode(.fields("id"))  & """, """  & toUnicode(.fields("start_date"))  & """, """  & toUnicode(.fields(eventTypeName))  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
            s = s & "]}"  
            end if
        end with


        pJSONGetCompaniesWithoutBackOfficeId = s

        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function

private function pUpdateAddress(objCompany)
	Dim objConn, objRS, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State <> 1 then exit function
    
    strSql = "SELECT company_address_street ,company_address_number, company_address_boxnumber, company_address_postcode, company_address_localite,AuditUserUpdated,AuditDateUpdated FROM company_address where company_address_id=" & objCompanyAddress.company_address_id
	
	set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn, 2, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysCompany.pUpdateCompanyweb(objCompanyAddress)<br />while triggering [objRs.open strsql:] " & strsql       
			exit function
        end if
        with objRS
            if not .bof and not .eof then   
                .fields("company_address_street")=objCompanyAddress.company_address_street()
				.fields("company_address_number")=objCompanyAddress.company_address_number()
				.fields("company_address_boxnumber")=objCompanyAddress.company_address_boxnumber()
				.fields("company_address_postcode")=objCompanyAddress.company_address_postcode()
				.fields("company_address_localite")=objCompanyAddress.company_address_localite()
                .fields("AuditUserUpdated")=usr_id()
                .fields("AuditDateUpdated")=now()
            .update      
            sErrMsg= 1    
    end if
        end with

    If Err.number <> 0 then sErrMsg = Err.Description else sErrMsg=1
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing
    pUpdateAddress=sErrMsg
	
end function
private function pUpdateCompanyName(objCompany)
    Dim objConn, objRS, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State <> 1 then exit function
    
    strSql = "SELECT company_name,AuditUserUpdated,AuditDateUpdated from dbo.companies where company_id=" & objCompany.company_id

    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn, 2, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysCompany.pUpdateCompanyName(objCompany)<br />while triggering [objRs.open strsql:] " & strsql       
    exit function
        end if
        with objRS
            if not .bof and not .eof then   
                .fields("company_name")=objCompany.company_name()
                .fields("AuditUserUpdated")=usr_id()
                .fields("AuditUserUpdated")=usr_id()
                .fields("AuditDateUpdated")=now()
                .fields("AuditDateUpdated")=now()
            .update      
            sErrMsg= 1    
    end if
        end with

    If Err.number <> 0 then sErrMsg = Err.Description else sErrMsg=1
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing
    pUpdateCompanyName=sErrMsg
end function
private function pUpdateBackOfficeID(objCompany)
if objCompany.trusted <> 1 then exit function
    
    Dim objConn, objRS, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State <> 1 then exit function
    
    strSql = "SELECT company_hbc_id, company_id,AuditUserUpdated,AuditDateUpdated from dbo.companies where company_id=" & objCompany.company_id

    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn, 2, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysCompany.pUpdateBackOfficeID(objCompany)<br />while triggering [objRs.open strsql:] " & strsql       
    exit function
        end if
        with objRS
            if not .bof and not .eof then   
                .fields("company_hbc_id")=objCompany.company_hbc_id
                .fields("AuditUserUpdated")=usr_id()
                .fields("AuditDateUpdated")=now()
            .update      
            sErrMsg= 1    
    end if
        end with

    If Err.number <> 0 then sErrMsg = Err.Description else sErrMsg=1
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing
    pUpdateBackOfficeID=sErrMsg
end function


private function pSaveCompanyBusinessData(objCompany)
if objCompany.trusted <> 1 then exit function
    Dim objConn, objRS, strSql, sErrMsg, fieldName, fieldValue, dateValue

    select case objCompany.category
        case "competitor"
            strsql = "select * from [dbo].[link_company_competitors] where company_id=" & objCompany.company_id & " and competitor_id=" & objCompany.competitor_id
            fieldName ="competitor_id"
            fieldValue =objCompany.competitor_id
        case "software"
            strsql = "select * from [dbo].[link_company_soft_packs] where company_id=" & objCompany.company_id & " and soft_pack_id=" & objCompany.software_id
            fieldName ="soft_pack_id"
            fieldValue =objCompany.software_id
        case "federation"
            strsql = "select * from [dbo].[link_company_federations] where company_id=" & objCompany.company_id & " and federation_id=" & objCompany.federation_id
            fieldName ="federation_id"
            fieldValue =objCompany.federation_id
        case "softwarehouse"
            strsql = "select * from [dbo].[link_company_softwarehouses] where company_id=" & objCompany.company_id & " and softwarehouse_link_id=" & objCompany.softwarehouse_link_id
            fieldName ="softwarehouse_link_id"
            fieldValue =objCompany.softwarehouse_link_id

    end select

    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
     
    if objConn.State <> 1 then exit function    
    
    
    set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strsql, objConn, 2, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysLeads.pSaveCompanyBusinessData(objLead)" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            'response.write sErrMsg
            exit function
        end if
 
        with objRS
            if .bof and .eof then 
            .addnew 
                 .fields("company_id")=objCompany.company_id
                 .fields(fieldName) = trim(fieldValue)
                 if objCompany.category="competitor" then .fields("contract_end_date") = trim(objCompany.end_date)
                .fields("AuditUserCreated")=usr_id()
            else
                if objCompany.category="competitor" then .fields("contract_end_date") = trim(objCompany.end_date)
                .fields("AuditUserUpdated")=usr_id()
                .fields("AuditDateUpdated")=now()
            end if
            .update      
            sErrMsg= 1
        end with
    If Err.number <> 0 then sErrMsg = Err.Description
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing
    pSaveCompanyBusinessData=sErrMsg
end function

private function pJSONGetStCompanyDetailsFromHBC()

        Dim objConn, objRS, strSql, sErrMsg, objCmd
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=1 'adModeRead
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    strSql = "dbo.Company_GET_HBC"
    Dim a, b, c, d, e
            a = userlang()
            b = company_hbc_id()

    'on error resume next
    'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@userlang", 8, 1, len(a), a)            
                .Parameters.Append .CreateParameter("@hbc_id", 8, 1, len(b), b)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
    
    ' on error resume next
                set objRs = Server.CreateObject("ADODB.Recordset")
                objRs.CursorType = 0
                objRs.lockType = 1
                set objRs = .Execute()
            end with
            with objRS
                if not .bof and not .eof then

                    Dim nbc_desc, jfc_s_desc, s
                    nbc_desc = "nbc_desc_" & userlang()
                    jfc_s_desc = "jfc_s_desc_" & userlang()
                     
                        do while not .eof
                            s = "{"
                            s=s & " ""company_vat"":""" & toUnicode(.fields("company_vat")) & """"
                            s=s & " ,""company_name"":""" & toUnicode(.fields("company_name")) & """"
                            s=s & " ,""lang_radical"":""" & toUnicode(.fields("lang_radical")) & """"
                            s=s & " ,""lang_name"":""" & toUnicode(.fields("lang_name")) & """"
                            s=s & " ,""jfc_s_desc"":""" & toUnicode(.fields(jfc_s_desc)) & """"
                            s=s & " ,""company_address_street"":""" & toUnicode(.fields("company_address_street")) & """"
                            s=s & " ,""company_address_number"":""" & toUnicode(.fields("company_address_number")) & """"
                            s=s & " ,""company_address_boxnumber"":""" & toUnicode(.fields("company_address_boxnumber")) & """"
                            s=s & " ,""company_address_floor"":""" & toUnicode(.fields("company_address_floor")) & """"
                            s=s & " ,""company_address_postcode"":""" & toUnicode(.fields("company_address_postcode")) & """"
                            s=s & " ,""company_address_localite"":""" & toUnicode(.fields("company_address_localite")) & """"
                            s=s & " ,""nbc_desc"":""" & toUnicode(.fields(nbc_desc)) & """"
                            s=s & " ,""company_nace"":""" & toUnicode(.fields("company_nace")) & """"
                            s=s & " ,""company_tel"":""" & toUnicode(.fields("company_tel")) & """"
                            s=s & " ,""company_mobile"":""" & toUnicode(.fields("company_mobile")) & """"
                            s=s & " ,""company_mail"":""" & toUnicode(.fields("company_mail")) & """"
                            s=s & " ,""company_id"":""" & toUnicode(.fields("company_id")) & """"
                            s=s & " ,""company_address_id"":""" & toUnicode(.fields("company_address_id")) & """"
    
                            s=s & " }"
                        .movenext
                        loop  
                end if
           end with    
    end if
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pJSONGetStCompanyDetailsFromHBC = s


end function

private function pJSONGetStCompanyDetails()

        Dim objConn, objRS, strSql, sErrMsg, objCmd
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=1 'adModeRead
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    strSql = "dbo.Company_GET"
    Dim a, b, c, d, e
            a = userlang()
            b = company_id()

    'on error resume next
    'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@userlang", 8, 1, len(a), a)            
                .Parameters.Append .CreateParameter("@company_id", 8, 1, len(b), b)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
    
    ' on error resume next
                set objRs = Server.CreateObject("ADODB.Recordset")
                objRs.CursorType = 0
                objRs.lockType = 1
                set objRs = .Execute()
            end with
            with objRS
                if not .bof and not .eof then

                    Dim nbc_desc, jfc_s_desc, s
                    nbc_desc = "nbc_desc_" & userlang()
                    jfc_s_desc = "jfc_s_desc_" & userlang()
                     
                        do while not .eof
                            s = "{"
                            s=s & " ""company_vat"":""" & toUnicode(.fields("company_vat")) & """"
                            s=s & " ,""company_name"":""" & toUnicode(.fields("company_name")) & """"
                            s=s & " ,""lang_radical"":""" & toUnicode(.fields("lang_radical")) & """"
                            s=s & " ,""lang_name"":""" & toUnicode(.fields("lang_name")) & """"
                            s=s & " ,""jfc_s_desc"":""" & toUnicode(.fields(jfc_s_desc)) & """"
                            s=s & " ,""company_address_street"":""" & toUnicode(.fields("company_address_street")) & """"
                            s=s & " ,""company_address_number"":""" & toUnicode(.fields("company_address_number")) & """"
                            s=s & " ,""company_address_boxnumber"":""" & toUnicode(.fields("company_address_boxnumber")) & """"
                            s=s & " ,""company_address_floor"":""" & toUnicode(.fields("company_address_floor")) & """"
                            s=s & " ,""company_address_postcode"":""" & toUnicode(.fields("company_address_postcode")) & """"
                            s=s & " ,""company_address_localite"":""" & toUnicode(.fields("company_address_localite")) & """"
                            s=s & " ,""nbc_desc"":""" & toUnicode(.fields(nbc_desc)) & """"
                            s=s & " ,""company_nace"":""" & toUnicode(.fields("company_nace")) & """"
                            s=s & " ,""company_tel"":""" & toUnicode(.fields("company_tel")) & """"
                            s=s & " ,""company_mobile"":""" & toUnicode(.fields("company_mobile")) & """"
                            s=s & " ,""company_mail"":""" & toUnicode(.fields("company_mail")) & """"
                            s=s & " ,""company_id"":""" & .fields("company_id") & """"
                            s=s & " ,""company_address_id"":""" & .fields("company_address_id") & """"
							s=s & " ,""company_hbc_id"":""" & .fields("company_hbc_id") & """"
    
                            s=s & " }"
                        .movenext
                        loop   
                end if
           end with    
    end if
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pJSONGetStCompanyDetails = s


end function

        private function pGetComboCompaniesMailTemplates()       
            Dim objConn, oRs, strSql, s, sfield
            sfield= fieldName()
            
            strSql = "SELECT DISTINCT * FROM dbo.[vw_GetComboCompaniesMailTemplates] WHERE user_id="  & usr_id()
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            do while not .eof
                                's = s & "{key:" & .fields(0) & ",label:'" & toUnicode(.fields(EventTypeName))) & "'},"
                                s = s & "{value:""" & .fields(0) & """, text:""" & toUnicode(.fields(sfield)) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboCompaniesMailTemplates = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function






    private function createObjects()    
        set objCompany = new clsObjCompany
    
        set objDbCompany = new clsObjCompany
    end function

    private function cleanUpObjects()
        set objCompany = nothing
        set objDbCompany = nothing
    end function

    public function formatDate(varDate)
        yy = year(varDate) 
        mm = month(varDate) 
            if mm < 10 then mm = "0" & mm
        dd = day(varDate)
            if dd < 10 then dd = "0" & dd
        SetDate = yy & "-" & mm & "-" & dd
    end function

    public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

'<end region PRIVATE METHODS & FUNCTIONS>
    public sub Class_Terminate()
        set objCy = nothing
    end sub

end class
%>
