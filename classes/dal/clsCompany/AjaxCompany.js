﻿function populateobjCompany(obj) {
    var params = "company_id=" + encodeURIComponent(obj.company_id);
    params += "&company_hbc_id=" + encodeURIComponent(obj.company_hbc_id);
    params += "&item_id=" + encodeURIComponent(obj.item_id);
    params += "&category=" + encodeURIComponent(obj.category);
    params += "&end_date=" + encodeURIComponent(obj.end_date);
	params += "&company_name=" + encodeURIComponent(obj.company_name);
    params += "&trusted=1";
    return params;
}

function populateobjCompanyAddress(objCy)
{
	params  = "company_address_id=" + encodeURIComponent(objCy.company_address_id);
	params += "&company_address_street=" + encodeURIComponent(objCy.company_address_street);
    params += "&company_address_number=" + encodeURIComponent(objCy.company_address_number);
	params += "&company_address_boxnumber=" + encodeURIComponent(objCy.company_address_boxnumber);
    params += "&company_address_postcode=" + encodeURIComponent(objCy.company_address_postcode);
    params += "&company_address_localite=" + encodeURIComponent(objCy.company_address_localite);
	
	return params;
}

function AjaxCompany_UpdateAddressPromise(objCy)
{
		var url = "/classes/dal/clsCompany/clsCompany.asp?a=UpdateAddress";
		var params = populateobjCompanyAddress(objCy);
		
		return $.post(url, params)
        .fail(function (resp) {
		    dhtmlx.message({
		        text: "Error Updating Address",
		        expire: -1,
		        type: "errormessage"
		    });
		});
}
function AjaxCompany_UpdateNamePromise(objCy)
{
		var url = "/classes/dal/clsCompany/clsCompany.asp?a=UpdateCompanyName";
		var params = populateobjCompany(objCy)
        .fail(function (resp) {
            dhtmlx.message({
                text: "Error updating name",
                expire: -1,
                type: "errormessage"
            });
        });
		
		return $.post(url,params);
}
function AjaxCompany_UpdateNameAndStreet(objCy)
{
	if (objCy && objCy.company_address_id && objCy.company_id)
	{
		$.when(AjaxCompany_UpdateNamePromise(objCy), AjaxCompany_UpdateAddressPromise(objCy))
		.done(function(name, street){
			if (name[0] == 1){
				dhtmlx.message({
					text: '[NAME]' + locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
			}else{
				dhtmlx.message({
					text: '[NAME]' + locale.main_page.lblActionCancelled,
					expire: -1,
					type: "errormessage"
				});
			}
			
			if (street[0] == 1)
			{
				dhtmlx.message({
					text: '[STREET]' + locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
			}else{
				dhtmlx.message({
					text: '[STREET]' + locale.main_page.lblActionCancelled,
					expire: -1,
					type: "errormessage"
				});
			}
		})
            .fail(function (resp) {
		    dhtmlx.message({
		        text: "Error Updating Name & Street",
		        expire: -1,
		        type: "errormessage"
		    });
		});;			
	}
}
function JSONGetCompanyFromId(company_id)
{
        var url = "/classes/dal/clsCompany/clsCompany.asp";
        return $.get(url, { a: "JSONGetCompanyInfo", company_id: company_id })
        .done(function (data) {
            
        }).fail(function (data) {
            dhtmlx.message({
                text: "Failure getting Company From Id",
                expire: -1,
                type: "errormessage"
            });
        });
}
function ConvertBackofficeToSalesTool(company_vat)
{
	var url = "/classes/dal/clsCompany/clsCompany.asp";
	
	return $.get(url, { a: "ConvertBackofficeToSalesTool", company_vat: company_vat })
	.done(function(data)
	{
		if (data == "1")
		{
			dhtmlx.message({
				text: "Failure Converting Backoffice To SalesTool From Vat",
				expire: 500,
				type: "successmessage"
			});
		}else{
			dhtmlx.message({
				text: "Failure Converting Backoffice To SalesTool From Vat",
				expire: -1,
				type: "errormessage"
			});
		}
	}).fail(function(data)
	{
		dhtmlx.message({
			text: "Major Failure Converting Backoffice To SalesTool From Vat",
			expire: -1,
			type: "errormessage"
		});
	});
}
function JSONGetCompanyFromVat(company_vat) {
    var url = "/classes/dal/clsCompany/clsCompany.asp";
    return $.get(url, { a: "JSONGetCompanyInfoFromVat", company_vat: company_vat })
    .done(function (data) {

    }).fail(function (data) {
		if (!data.responseText.trim())
		{
			ConvertBackofficeToSalesTool(company_vat);			
		}
        dhtmlx.message({
            text: "Failure getting Company From Vat",
            expire: -1,
            type: "errormessage"
        });
    });
}
function AjaxCompany_UpdateBackOfficeID(obj) {
	var url = "/classes/dal/clsCompany/clsCompany.asp?a=UpdateBackOfficeID";
	var params = populateobjCompany(obj);
	
	$.post(url,params)
	.done(function(data){
		if (data == 1) {
			dhtmlx.message({
				text: locale.main_page.lblSaveSuccess,
				expire: 500,
				type: "successmessage"
			});
		}else{
			dhtmlx.message({
				text: locale.main_page.lblActionCancelled,
				expire: -1,
				type: "errormessage"
			});
		}
	}).fail(function(resp){
		dhtmlx.message({
			text: locale.main_page.lblActionCancelled + resp,
			expire: -1,
			type: "errormessage"
		});
	});  
    
}
function GetCompanyFromContactId(contact_id)
{
    var url = "/classes/dal/clsCompany/clsCompany.asp";

    return $.get(url, { a: "GetCompanyIdFromContactId", contact_id: contact_id })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Failure getting company from contact Id",
            expire: -1,
            type: "errormessage"
        });
    });
}
function ASPSaveCompanyBusinessData(obj) {
	var url = "/classes/dal/clsCompany/clsCompany.asp?a=SaveCompanyBusinessData";
    var params = populateobjCompany(obj);
    
	$.post(url,params)
	.done(function(data){
		if (data == 1) {
			dhtmlx.message({
				text: locale.main_page.lblSaveSuccess,
				expire: 500,
				type: "successmessage"
			});
		}else{
			dhtmlx.message({
				text: locale.main_page.lblActionCancelled,
				expire: -1,
				type: "errormessage"
			});
		}
	}).fail(function(resp){
		dhtmlx.message({
			text: locale.main_page.lblActionCancelled + resp,
			expire: -1,
			type: "errormessage"
		});
	});
}

