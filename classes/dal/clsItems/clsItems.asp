﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objItem.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"
CONST C_BASIC_HR_FUNCTION= 4
CONST C_ENU_PACKAGES = 1
CONST C_ENU_GLOBALITEMS = 2
CONST C_ENU_MANAGEMENT = 3
CONST C_ENU_ALERTS = 4
CONST C_ENU_MOBILEAPPS = 5
CONST C_ENU_INTREPORTS = 6
CONST C_ENU_TRANSARR = 7
CONST C_ENU_RESIGN = 11

Dim objItem
   
    set objItem = new sysItems
    
        select case request.QueryString("a")         
            case "GetComboPackages"
                objItem.GetComboPackages
            case "GetComboAlerts"
                objItem.GetComboAlerts
            case "GetComboMobileApps"
                objItem.GetComboMobileApps
            case "GetComboIntReports"
                objItem.GetComboIntReports
            case "GetTransitionalArrangement"
                objItem.GetTransitionalArrangement
            case "GetNationalReport"
                objItem.GetNationalReport
            case "GetPriceByItemID"
                objItem.GetPriceByItemID
            case "GetMailTemplate"
                objItem.GetMailTemplate
            case "GetComboPackagesDefaultOptions"
                objItem.GetComboPackagesDefaultOptions
            case else 'default should be read
        end select
    

class sysItems

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get hr_function_id()
        hr_function_id =Request.Cookies ("TELESALESTOOLV2")("hrfunctionid")
        if len(trim(hr_function_id)) = 0 then hr_function_id = C_BASIC_HR_FUNCTION    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get Customerlang()
        Customerlang = request.querystring("Customerlang")    
        if len(trim(Customerlang)) = 0 then Customerlang = Userlang()
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property

    property get mail_template_id()
        mail_template_id=trim(request.querystring("mail_template_id"))
        if len(mail_template_id) = 0 then mail_template_id = 1 '(default purchase order)
    end property

    property get packageID()
        packageID=trim(request.querystring("packageID"))
        if len(packageID) = 0 then packageID = 0 '(default purchase order)
    end property
    property get item_ID()
        item_ID=trim(request.querystring("item_ID"))
        if len(item_ID) = 0 then item_ID = 0 '(default purchase order)
    end property
    

    public function GetComboPackages()
        response.write pGetComboByCategory(C_ENU_PACKAGES)
    end function
    public function GetComboPackagesDefaultOptions()
        response.write pGetComboPackagesDefaultOptions()
    end function


    public function GetComboAlerts()
        response.write pGetComboByCategory(C_ENU_ALERTS)
    end function

    public function GetComboMobileApps()
        response.write pGetComboByCategory(C_ENU_MOBILEAPPS)
    end function

    public function GetComboIntReports()
        response.write pGetComboByCategory(C_ENU_INTREPORTS)
    end function

    public function GetTransitionalArrangement()
        response.write pGetTransitionalArrangement()
    end function

    public function GetPriceByItemID()
        response.write pGetPriceByItemID()
    end function

    public function GetNationalReport()
    dim i 
        select case packageID()
            case  1 'basic
                i =8
            case 2 'standard
                i=9
            case 3, 42, 43 'premium, 250, 500
                i=10
            case else
                i =9
        end select    
        response.write pGetItemsList(i)
    end function

    public function GetMailTemplate()
        response.write pGetMailTemplate()
    end function


    public function GetComboFederationsWithImg()
        GetComboFederationsWithImg = pGetComboFederationsWithImg() :  response.write GetComboFederationsWithImg
    end function

    public function JSONGetItemsbyCategory() 
        Response.ContentType = "application/json" :response.write pJSONGetItemsbyCategory()
    end function

    




'<START REGION JSON BUSINESS LOGIC>===========================================================
 
private function GetMailStructure()  
Dim beginHtml, endHtml, imgHeader, imgFooter, ext
Dim m
ext = ".png"
imgHeader = "http://www.companyweb.be/img/mails/headerCW" & Customerlang()  & ext 
imgFooter = "http://www.companyweb.be/img/mails/footerCW" & Customerlang() & ext
beginHtml="<html><head><title>Companyweb</title><style type=""text/css""> body { font: 12px/1.5 Verdana, Arial, Helvetica, sans-serif; background: #e6e6e6; color: #222; }  a { font: 12px/1.5 Verdana, Arial, Helvetica, sans-serif; color: #222; text-decoration: none; }  .biglink { font: 14px/1.5 Verdana, Arial, Helvetica, sans-serif; color: #eb3c34; text-decoration: none; font-weight:bold; } </style></head><body><table width=""640"" align=""center"" bgcolor=""white"" height=""100%""><tr valign=""top""><td>"    
endHtml="</td></tr><tr valign=top><td><img src=" & imgFooter & " width=640 height=90 alt=companyweb /></table></body></html>"
     
dim mailTable, packageHeader, price, transarr, footer, cySticker, lblMailType
    mailTable = " style=""font-family:tahoma, arial, sans-serif;font-size:14px;color: #525252;border:0px; border-collapse: separate; border-spacing: 5px;  width:600px"""
    lastName =" style=""font-family:tahoma, arial, sans-serif;font-size:14px;"
    packageHeader = " style=""font-family:tahoma, arial, sans-serif; font-weight: 700; font-size:14px; color: #323c78; text-decoration: underline; """
    price = " style=""font-weight: 700;"""
    transarr = " style=""font-family:tahoma, arial, sans-serif;text-align:justify;font-size:11px; color: #323c78;"""
    footer = " style=""font-family:tahoma, arial, sans-serif; font-size:10px; color:#6286a0; justify-content: center; text-align:center; """
    cySticker=" style=""font-family:tahoma, arial, sans-serif; font-size:11px; color:#3f4f6f; """
    cyCtdate=" style=""font-family:tahoma, arial, sans-serif; font-size:12px; color:#000000;font-weight: 700;text-align:right; """
    lblMailType =" style=""font-family:tahoma, arial, sans-serif; font-size:16px; color:#000000;font-weight: 700;text-align:center;text-decoration: underline; """
     
m = "<table " & mailTable & ">"
m = m & "<tr><td" & cySticker & "><img src=" & imgHeader & " width=640 height=90 alt=companyweb/></td></tr>"
m = m & "<tr><td" & cyCtdate & ">@contractdate</td></tr>"
m = m & "<tr><td" & lblMailType & "><div id=""§mailTitle"">@mailTitle</div></td></tr>"
   
m = m & "<tr><td" & cySticker & ">@cysticker</td></tr>"

m = m & "<tr><td" & lastName & "><div id=""§dear"">@dear §lastname</div></td></tr>"
m = m & "<tr><td><div id=""§quote1"">@quote1</div></td></tr>" 


m = m & "<tr><td><div id=""§package""" & packageHeader &">@package</div></td></tr>"

m = m & "<tr><td><div id=""§nationalreport"">@nationalreport</div></td></tr>"
m = m & "<tr><td><div id=""§globalitem"">@globalitem</div></td></tr>"
m = m & "<tr><td><div id=""§management""></div></td></tr>"
m = m & "<tr><td><div id=""§alerts"">@alerts</div></td></tr>"
m = m & "<tr><td><div id=""§mobileapps"">@mobileapps</div></td></tr>"
m = m & "<tr><td><div id=""§intreports"">@intreports</div></td></tr>"
m = m & "<tr><td>&nbsp;</td></tr>"
m = m & "<tr><td " & price & "><div id=""§amount"">@price</div></td></tr>"
m = m & "<tr><td><div " & price & " id=""§additionalMemo""></div></td></tr>"
m = m & "<tr><td><div " & transarr & " id=""§transarr""></div></td></tr>"

m = m & "<tr><td><div id=""§quote2"">@quote2</div></td></tr>"
m = m & "<tr><td><div id=""§quote3"">@quote3</div></td></tr>"
m = m & "<tr><td><div id=""§quote4"">@quote4</div></td></tr>"
m = m & "<tr><td>&nbsp;</td></tr>"
m = m & "<tr><td " & price & "><div id=""§username"">@username</div></td></tr>"
m = m & "<tr><td><div id=""§usertitle"">@usertitle</div></td></tr>"
m = m & "<tr><td>@phone1 | @mail1</td></tr></table>"
GetMailStructure = beginHtml & m & endHtml
end function



    private function pGetMailTemplate()
    Dim objConn, oRs, strSql, mailItem, itemLongDesc, s, fullDesc
            mailItem = "mail_item_" & CustomerLang() 
            itemLongDesc = "item_longdesc_" & UserLang()
            s= GetMailStructure()
            strSql = "SELECT * FROM [dbo].[vw_GetMailTemplate] where mail_template_id=" & mail_template_id()
           
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst
                    do while not .eof
    
                        s = replace(s, .fields("mail_item_tag"), .fields(mailItem), 1, -1, vbtextcompare)
                    
                        .movenext
                    loop
                    end if
                end with
    
        Dim UserName, userTitle, phone1, mail1 , hrfunctionid   
            UserName = Request.Cookies ("TELESALESTOOLV2")("firstname") & " " & request.Cookies ("TELESALESTOOLV2")("lastname")
            userTitle = Request.Cookies ("TELESALESTOOLV2")("hrfuncname")    
            userTitle = Replace(userTitle,"+"," ")
            hrfunctionid =Request.Cookies ("TELESALESTOOLV2")("hrfunctionid")
            phone1 = Request.Cookies ("TELESALESTOOLV2")("phone1")
            mail1 = Request.Cookies ("TELESALESTOOLV2")("email1")

            s = replace(s,"@username",UserName)
            s = replace(s,"@usertitle",userTitle)
            s = replace(s,"@phone1",phone1)
            s = replace(s,"@mail1",mail1)
            s = replace(s,"@globalitem",pGetItemsList(C_ENU_GLOBALITEMS))
    

            pGetMailTemplate =s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

     end function



private function pGetItemsList(categoryID)
Dim objConn, oRs, strSql, itemName, itemLongDesc, s, fullDesc
            itemName = "item_name_" & CustomerLang() 
            itemLongDesc = "item_longdesc_" & CustomerLang() 
            
            strSql = "SELECT * FROM dbo.[vw_GetItemsBycategoryID] WHERE item_category_id=" & categoryID 
   
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        's= "<ul>"
                            do while not .eof
                                fullDesc = .fields(itemName)
                                s = s & "<li>"  & fullDesc & "</li>"
                            .movenext
                        loop       
                    's = s & "</ul>"    
                end if
                End With
            pGetItemsList = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
end function

private function pGetTransitionalArrangement()
Dim objConn, oRs, strSql, itemName, itemLongDesc, s, fullDesc
            itemName = "item_name_" & CustomerLang() 
            itemLongDesc = "item_longdesc_" & CustomerLang() 
            
            strSql = "SELECT * FROM dbo.[vw_GetItemsBycategoryID] WHERE item_category_id=" & C_ENU_TRANSARR
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                    
                            do while not .eof
                                fullDesc = "<b><u>" & .fields(itemName) & "</u></b><br>" & .fields(itemLongDesc)
                                s = s & fullDesc
                            .movenext
                        loop       
                    
                end if
                End With
            pGetTransitionalArrangement = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
end function
private function pGetResignationLetter()
			Dim objConn, oRs, strSql, itemName, itemLongDesc, s, fullDesc
            itemName = "item_name_" & CustomerLang() 
            itemLongDesc = "item_longdesc_" & CustomerLang() 
            
            strSql = "SELECT * FROM dbo.[vw_GetItemsBycategoryID] WHERE item_category_id=" & C_ENU_RESIGN
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                    
                            do while not .eof
                                fullDesc = "<b><u>" & .fields(itemName) & "</u></b><br>" & .fields(itemLongDesc)
                                s = s & fullDesc
                            .movenext
                        loop       
                    
                end if
                End With
            pGetTransitionalArrangement = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
end function

    private function pGetPriceByItemID()
        Dim objConn, oRs, strSql
        pGetPriceByItemID = 0                    
        strSql = "SELECT item_price FROM dbo.[vw_GetItemsBycategoryID] WHERE item_id=" & item_ID() 
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                    pGetPriceByItemID = .fields("item_price")
                end if
                End With

        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing
    end function

    private function pGetComboByCategory(categoryID)       
            Dim objConn, oRs, strSql, itemName, itemLongDesc, s, fullDesc
            itemName = "item_name_" & UserLang() 
            itemLongDesc = "item_longdesc_" & UserLang() 
            
            strSql = "SELECT * FROM dbo.[vw_GetItemsBycategoryID] WHERE item_category_id=" & categoryID & " AND item_hrfunction_id >=" & hr_function_id() & " order by item_order"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                    
                        s= "{options:["
                            s = s & "{value:""-1"", text:""select...""},"
                            do while not .eof
                                fullDesc = .fields(itemName) & vbtab & "(€" & .fields("item_price") & ")"
                                s = s & "{value:""" & .fields("item_id") & """, text:""" & fullDesc & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboByCategory = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

    private function pGetComboPackagesDefaultOptions()       
            Dim objConn, oRs, strSql, itemName, itemLongDesc, s, fullDesc
            itemName = "item_name_" & UserLang() 
            itemLongDesc = "item_longdesc_" & UserLang() 
            
            strSql = "SELECT * FROM dbo.[vw_GetItemsBycategoryID] WHERE item_id=" & item_ID
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "["   
                            do while not .eof
                                s = s & "{""default_alerts_id"":""" & .fields("item_default_alerts_id") & """, ""default_mobileapps_id"":""" & .fields("item_default_mobileapps_id") & """, ""default_intreports_id"":""" & .fields("item_default_intreports_id") & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]"    
                    
                end if
                End With
            pGetComboPackagesDefaultOptions = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function


    private function pJSONGetItemsbyCategory(categoryID)
    Dim a
    Dim itemName, itemLongDesc
        itemName = "item_name_" & UserLang() 
        itemLongDesc = "item_longdesc_" & UserLang()

        strSql = "SELECT * FROM dbo.[vw_GetItemsBycategoryID] WHERE item_category_id=" & categoryID ' & " AND item_hrfunction_id >=" & hr_function_id()
        set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
            objConn.open SalesToolConnString()' , adopenstatic, adcmdreadonly, adcmdtext
            oRs.open strSql, objConn

        with oRs
                
        if not .bof and not .eof then                     
                        s= "["
                        do while not .eof
                            s=s & "{"
                                s=s & " ""item_id"":""" & .fields("item_id") & """"
                                s=s & " ,""item_parent_id"":""" & .fields("item_parent_id") & """"
                                s=s & " ,""item_category_id"":""" & .fields("item_category_id") & """"
                                s=s & " ,""item_price"":""" & .fields("item_price") & """"
                                s=s & " ,""item_promoprice"":""" & .fields("item_promoprice") & """"
                                s=s & " ,""item_ispromo"":""" & .fields("item_ispromo") & """"
                                s=s & " ,""item_order"":""" & .fields("item_order") & """"
                                s=s & " ,""item_enabled"":""" & .fields("item_enabled") & """"
                                s=s & " ,""itemName"":""" & .fields(itemName) & """"
                                s=s & " ,""itemLongDesc"":""" & .fields(itemLongDesc) & """"

                                s=s & " ,""item_category_name"":""" & .fields("item_category_name") & """"
                                s=s & " ,""item_category_order"":""" & .fields("item_category_order") & """"
                                
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        s = s & "]"    
                end if
           end with
    
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

        pJSONGetItemsbyCategory = s

    
    end function


'<END REGION JSON BUSINESS LOGIC>=============================================================



public sub Class_Terminate()
    set objFed = nothing
end sub


end class

%>