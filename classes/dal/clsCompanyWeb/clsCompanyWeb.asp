﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objCW
    Set objCW = new sysCompanyWeb
    
    select case request.querystring("q")
        case "1" 'Get CW info based on vat number
            objCW.JSONGetCompanyCompanyWebInfo()
        
        case "2" 'Get CW Mandates info based on vat number
            objCW.JSONGetCompanyCompanyWebMandates()
        case "3" 'Get CW Mandates info based on vat number
            objCW.JSONGetCompanyCompanyWebInfoFromHbcId()
        case "4" 'Get CW Mandates info based on vat number
            objCW.JSONGetCompanyCompanyWebMandatesFromHbcId()
        case "70"
            objCW.JSONGetCompanyCompanyWebStateFromHbcId()
        case "80"
            objCW.JSONGetCompanyCompanyWebStateFromVat()
    case else 'default tbd
            
    end select
    
    set objCW = nothing


class sysCompanyWeb


    property get BOConnString()
        BOConnString = sConnBoString 'sConnStrBackOfficeDev 'sConnStrBackOfficeProd
    end property



    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    'for testing purpose only
    if len(trim(usr_id))= 0 then usr_id = 1
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get CompanyVat()
        CompanyVat=trim(request.querystring("vat"))
        if len(CompanyVat) = 0 then CompanyVat = 0
    end property
        property get hbc_id()
        hbc_id=trim(request.querystring("hbc_id"))
        if len(hbc_id) = 0 then hbc_id = -1
    end property

    '<start region PUBLIC METHODS & FUNCTIONS>
    public function JSONGetCompanyCompanyWebStateFromVat()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyCompanyWebStateFromVat()
    end function
        public function JSONGetCompanyCompanyWebStateFromHbcId()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyCompanyWebStateFromHbcId()
    end function
    public function JSONGetCompanyCompanyWebInfo() 
        'response.write server.htmlencode(pJSONGetCompanyCompanyWebInfo()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetCompanyCompanyWebInfo()
    end function

    public function JSONGetCompanyCompanyWebMandates() 
        'response.write server.htmlencode(pJSONGetCompanyCompanyWebMandates()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetCompanyCompanyWebMandates()
    end function
    public function JSONGetCompanyCompanyWebInfoFromHbcId() 
        'response.write server.htmlencode(pJSONGetCompanyCompanyWebInfo()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetCompanyCompanyWebInfoFromHbcId()
    end function

    public function JSONGetCompanyCompanyWebMandatesFromHbcId() 
        'response.write server.htmlencode(pJSONGetCompanyCompanyWebMandates()):response.end
        Response.ContentType = "application/json" : response.write pJSONGetCompanyCompanyWebMandatesFromHbcId()
    end function

'<end region PUBLIC METHODS & FUNCTIONS>


'<start region PRIVATE METHODS & FUNCTIONS>
   function JurSitTrans(intID)
      dim numID
      JurSitTrans = ""
      numID = 0

      select case userlang()
        case "FR"
            select case numID
                case 0 : JurSitTrans = "Actif"
                case 1 : JurSitTrans = "Création juridique (inactif)"
                case 2 : JurSitTrans = "Prorogation"
                case 3 : JurSitTrans = "Remplacement du numéro"
                case 5 : JurSitTrans = "Fin de mission"
                case 6 : JurSitTrans = "Arrêtée pour cause de remplacement du numéro"
                case 10 : JurSitTrans = "Dissolution de plein droit par arrivée à terme"
                case 11 : JurSitTrans = "Cessation des activités en Belgique"
                case 12 : JurSitTrans = "Dissolution anticipée - Liquidation"
                case 13 : JurSitTrans = "Dissolution judiciaire et nullité"
                case 14 : JurSitTrans = "Clôture de liquidation"
                case 15 : JurSitTrans = "Cessation"
                case 16 : JurSitTrans = "Cessation d'activité en personne physique"
                case 17 : JurSitTrans = "Transfert d'une entreprise de personne physique"
                case 18 : JurSitTrans = "Cessation de l'identification"
                case 19 : JurSitTrans = "Cessation d'une entreprise EDRL ou Non UE"
                case 20 : JurSitTrans = "Réunion des parts en une seule main"
                case 21 : JurSitTrans = "Fusion par absorption"
                case 22 : JurSitTrans = "Fusion par constitution d'une nouvelle société"
                case 23 : JurSitTrans = "Scission"
                case 24 : JurSitTrans = "Scission par absorption"
                case 25 : JurSitTrans = "Scission par constitution de nouvelles sociétés"
                case 26 : JurSitTrans = "Scission mixte"
                case 30 : JurSitTrans = "Concordat avant faillite"
                case 31 : JurSitTrans = "Concordat après faillite"
                case 40 : JurSitTrans = "Sursis provisoire"
                case 41 : JurSitTrans = "Sursis définitif"
                case 42 : JurSitTrans = "Révocation du sursis"
                case 43 : JurSitTrans = "Fin du sursis"
                case 48 : JurSitTrans = "Ouverture de faillite"
                case 49 : JurSitTrans = "Ouverture de faillite"
                case 50 : JurSitTrans = "Ouverture de faillite"
                case 51 : JurSitTrans = "Clôture de faillite"
                case 52 : JurSitTrans = "Clôture de faillite"
                case 53 : JurSitTrans = "Clôture de faillite"
                case 75 : JurSitTrans = "Radiation administrative"
                case 90 : JurSitTrans = "Nouveaux statuts"
                case 91 : JurSitTrans = "Sursis de paiement"
                case 100 : JurSitTrans = "Identification de l'entreprise"
                case 999 : JurSitTrans = "Dossier annulé"
                case else : JurSitTrans = "Dossier inactif"
            end select
        case "NL"
            select case numID
                case 0 : JurSitTrans = "Actief"
                case 1 : JurSitTrans = "Juridische oprichting (nog niet actief)"
                case 2 : JurSitTrans = "Verlenging"
                case 3 : JurSitTrans = "Vervanging"
                case 5 : JurSitTrans = "Einde opdracht"
                case 6 : JurSitTrans = "Stopzetting wegens vervanging"
                case 10 : JurSitTrans = "Ontbinding rechtswege door termijn verloop"
                case 11 : JurSitTrans = "Activiteitstopzetting in Belgïe"
                case 12 : JurSitTrans = "Vervroegde ontbinding - Vereffening"
                case 13 : JurSitTrans = "Juridische ontbinding of nietigheid"
                case 14 : JurSitTrans = "Sluiting van vereffening"
                case 15 : JurSitTrans = "Stopzetting"
                case 16 : JurSitTrans = "Stopzetting activiteit natuurlijke persoon"
                case 17 : JurSitTrans = "Overdracht van een onderneming natuurlijke persoon"
                case 18 : JurSitTrans = "Stopgezet in bekendmaking"
                case 19 : JurSitTrans = "Stopgezette EDRL of Niet-EU onderneming"
                case 20 : JurSitTrans = "Verzameling aandelen in hoofde van één persoon"
                case 21 : JurSitTrans = "Fusie"
                case 22 : JurSitTrans = "Fusie door oprichting van een nieuwe vennootschap"
                case 23 : JurSitTrans = "Splitsing"
                case 24 : JurSitTrans = "Splitsing door opslorping"
                case 25 : JurSitTrans = "Splitsing door oprichting van nieuwe vennootschappen"
                case 26 : JurSitTrans = "Gemengde splitsing"
                case 30 : JurSitTrans = "Konkordaat vóór faling"
                case 31 : JurSitTrans = "Konkordaat na faling"
                case 40 : JurSitTrans = "Voorlopige opschorting van betaling"
                case 41 : JurSitTrans = "Definitieve opschorting van betaling"
                case 42 : JurSitTrans = "Herroeping van de opschorting"
                case 43 : JurSitTrans = "Einde van de opschorting"
                case 48 : JurSitTrans = "Opening faillissement"
                case 49 : JurSitTrans = "Opening faillissement"
                case 50 : JurSitTrans = "Opening faillissement"
                case 51 : JurSitTrans = "Sluiting faillissement"
                case 52 : JurSitTrans = "Sluiting faillissement"
                case 53 : JurSitTrans = "Sluiting faillissement"
                case 75 : JurSitTrans = "Administratief doorgehaald"
                case 90 : JurSitTrans = "Nieuwe statuten"
                case 91 : JurSitTrans = "Uitstel van betaling"
                case 100 : JurSitTrans = "Bekendmaking van de onderneming"
                case 999 : JurSitTrans = "Geannuleerd dossier"
                case else : JurSitTrans = "Activiteit stopgezet"
            end select        
        case "EN" 
            select case numID
                case 0 : JurSitTrans = "Active"
                case 1 : JurSitTrans = "Legal establishment (not yet active)"
                case 2 : JurSitTrans = "Prolongation"
                case 3 : JurSitTrans = "Prolongation"
                case 5 : JurSitTrans = "End of the assignment"
                case 6 : JurSitTrans = "Cessation by replacement "
                case 10 : JurSitTrans = "Automatic dissolution by the expiry of the term"
                case 11 : JurSitTrans = "Cessation of activity in Belgium"
                case 12 : JurSitTrans = "Early dissolution  - liquidation"
                case 13 : JurSitTrans = "Legal dissolution or nullity "
                case 14 : JurSitTrans = "Conclude the liquidation process"
                case 15 : JurSitTrans = "Cessation"
                case 16 : JurSitTrans = "Cessation activity natural person"
                case 17 : JurSitTrans = "Transfer of a business natural person"
                case 18 : JurSitTrans = "Stopped by publication"
                case 19 : JurSitTrans = "Stopped EDRL or Non-EU enterprise"
                case 20 : JurSitTrans = "Collection of shares on the part of one person"
                case 21 : JurSitTrans = "Fusion"
                case 22 : JurSitTrans = "Fusion by establishment of a new enterprise"
                case 23 : JurSitTrans = "Division"
                case 24 : JurSitTrans = "Division by absorption"
                case 25 : JurSitTrans = "Division by establishment of a new enterprises"
                case 26 : JurSitTrans = "Mixed division"
                case 30 : JurSitTrans = "Concordat prior to bankruptcy"
                case 31 : JurSitTrans = "Concordat after bankruptcy"
                case 40 : JurSitTrans = "Temporary suspension of payment"
                case 41 : JurSitTrans = "Definitive suspension of payment"
                case 42 : JurSitTrans = "Revoking the suspension"
                case 43 : JurSitTrans = "End of the suspension"
                case 48 : JurSitTrans = "Opening bankruptcy"
                case 49 : JurSitTrans = "Opening bankruptcy"
                case 50 : JurSitTrans = "Opening bankruptcy"
                case 51 : JurSitTrans = "Closure bankruptcy"
                case 52 : JurSitTrans = "Closure bankruptcy"
                case 53 : JurSitTrans = "Closure bankruptcy"
                case 75 : JurSitTrans = "Administrative cancellaton"
                case 90 : JurSitTrans = "New Statutes"
                case 91 : JurSitTrans = "Postponement of payment"
                case 100 : JurSitTrans = "Disclosure of the company"
                case 999 : JurSitTrans = "Cancelled file"
                case else : JurSitTrans = "Activity ended"
            end select
      end select
end function

        private function pJSONGetCompanyCompanyWebStateFromHbcId()
        Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

        strSql = " SELECT H.HBC_ID, F.F_CurrentStatus, F.F_EindDatum, F.F_CurrentJurSitu "
        strSql = strSql & " FROM HB_COMPANY H"
        strSql = strSql & " INNER JOIN CW_FIRMA F on H.HBC_FirmVat = F.F_VAT "
        strSQl = strSql & " WHERE H.HBC_ID = " & hbc_id()

        set objConn=Server.CreateObject("ADODB.Connection")
    set objRS = Server.CreateObject("ADODB.Recordset") 
    objConn.open BOConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRS.open strSql, objConn
    dim statusText
                with objRS
                    if not .bof and not .eof then
                        do while not .eof
                            
                            if (len(.fields("F_EindDatum")) = 8) then
                                statusText = JurSitTrans(.fields("F_CurrentJurSitu")) & " : " & .fields("F_EindDatum")
                            else
                                if (.fields("F_EindDatum") <> "0" AND .fields("f_einddatum") <> "") then
                                    statusText = JurSitTrans(.fields("F_CurrentJurSitu"))
                                else
                                    statusText = JurSitTrans(0)
                                end if
                            end if

                            s = """hbc_id"": """ & hbc_id() & """"
                            s = s & ", ""state"": """ & statusText & """"
                            s = s & ", ""f_currentStatus"": """ & .fields("F_CurrentJurSitu") & """"
                            s = s & ", ""f_eindDatum"": """ & .fields("f_einddatum") & """"
                            .movenext
                        loop                        
                    end if
                end with
        s = "{" & s & "}"    
        pJSONGetCompanyCompanyWebStateFromHbcId = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

    end function

    private function pJSONGetCompanyCompanyWebStateFromVat()
        Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

        strSql = " SELECT H.HBC_ID, F.F_CurrentStatus, F.F_EindDatum, F.F_CurrentJurSitu "
        strSql = strSql & " FROM CW_FIRMA F"
        strSql = strSql & " LEFT JOIN HB_COMPANY H on F.F_VAT = H.HBC_FirmVat "
        strSQl = strSql & " WHERE F.F_VAT = " & CompanyVat()
        
        set objConn=Server.CreateObject("ADODB.Connection")
    set objRS = Server.CreateObject("ADODB.Recordset") 
    objConn.open BOConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRS.open strSql, objConn
    dim statusText

                with objRS
                    if not .bof and not .eof then
                        do while not .eof
                            
                        if (len(.fields("F_EindDatum")) = 8) then
                            statusText = JurSitTrans(.fields("F_CurrentJurSitu")) & " : " & .fields("F_EindDatum")
                        else
                            if (.fields("F_EindDatum") <> "0" AND .fields("f_einddatum") <> "") then
                                statusText = JurSitTrans(.fields("F_CurrentJurSitu"))
                            else
                                statusText = JurSitTrans(0)
                            end if
                        end if
                            s = """hbc_id"": """ & .fields("hbc_id") & """"
                            s = s & ", ""state"": """ & statusText & """"
                            s = s & ", ""f_currentStatus"": """ & .fields("F_CurrentJurSitu") & """"
                            s = s & ", ""f_eindDatum"": """ & .fields("f_einddatum") & """"
                            s = s & ", ""F_currentJurSitu"": """ & .fields("F_CurrentJurSitu") & """"
                            .movenext
                        loop                        
                    end if
                end with
        s = "{" & s & "}"    
        pJSONGetCompanyCompanyWebStateFromVat = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

    end function

private function pJSONGetCompanyCompanyWebInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = " SELECT FU.FU_VAT, FU.FU_Name, FU.Fu_Vorm, FU.fu_StraatNaam, "    
    strSql = strSql & " FU.fu_HuisNr, FU.fu_BusNr, FU.fu_PostCode, "
    strSql = strSql & " FU.fu_Gemeente, F.f_begindatum, F.F_EindDatum, F.F_CurrentStatus, "
    strSql = strSql & " F.F_Kapitaal, F.F_MUNT, F.F_Omzet, F.F_Werknemers, "
    strSql = strSql & " F.F_BrutoMarge, F.F_Resultaat, F.F_Score, F.f_kredielimiet, "
    strSql = strSql & " F.F_LastBookyear, f.F_PrefLangID, JFC.jfc_l_desc_" & userlang() & " as jfc_l_desc, JFC.jfc_s_desc_" & userlang() & " as jfc_s_desc, "
    strSql = strSql & " N.nbc_desc_" & userlang() & " as nbc_desc, F_CurrentJurSitu"
    strSql = strSql & " from CW_Firma F "
    strSql = strSql & " INNER JOIN CW_Firma_unique_NL FU on F.F_VAT = FU.FU_VAT "
    strSql = strSql & " INNER JOIN KBO_JuridicalFormCodes JFC on F.F_CurrentJurSitu = jfc_code "
    strSql = strSql & " INNER JOIN KBO_Nacebel2008 N on F.F_HoofdNACE = N.nbc_code "
    strSQL = strSql & " where F.F_VAT = " & CompanyVat()
    
    set objConn=Server.CreateObject("ADODB.Connection")
    set objRS = Server.CreateObject("ADODB.Recordset") 
    objConn.open BOConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRS.open strSql, objConn

                with objRS
                    if not .bof and not .eof then
                        
                        do while not .eof
                            dim statusText
                        dim eindDatum
                        if (len(.fields("f_einddatum")) = 8) then
                            statusText = JurSitTrans(.fields("F_CurrentJurSitu")) & " : " & .fields("F_EindDatum")
                        else
                            if (.fields("F_EindDatum") <> "0" AND .fields("f_einddatum") <> "") then
                                statusText = JurSitTrans(.fields("F_CurrentJurSitu"))
                            else
                                statusText = JurSitTrans(0)
                            end if
                        end if
                             
                            s= """fu_VAT"":""" & .fields("FU_VAT") & """"
                            s=s & " ,""fu_Name"":""" & .fields("FU_Name") & """"
                            s=s & " ,""fu_vorm"":""" & .fields("Fu_Vorm") & """"
                            s=s & " ,""fu_StraatNaam"":""" & .fields("fu_StraatNaam") & """"
                            s=s & " ,""fu_HuisNr"":""" & .fields("fu_HuisNr") & """"
                            s=s & " ,""fu_BusNr"":""" & .fields("fu_BusNr") & """"
                            s=s & " ,""fu_Postcode"":""" & .fields("fu_PostCode") & """"
                            s=s & " ,""fu_gemeente"":""" & .fields("fu_Gemeente") & """"
                            s=s & " ,""F_BeginDatum"":""" & .fields("f_begindatum") & """"
                            s=s & " ,""F_AfsluitDatum"":""" & .fields("F_EindDatum") & """"
                            s=s & " ,""F_CurrentStatus"":""" & .fields("F_CurrentStatus") & """"
                            s=s & " ,""F_Kapitaal"":""" & .fields("F_Kapitaal") & """"
                            s=s & " ,""F_Munt"":""" & .fields("F_MUNT") & """"
                            s=s & " ,""F_Omzet"":""" & .fields("F_Omzet") & """"
                            s=s & " ,""F_Werknemers"":""" & .fields("F_Werknemers") & """"
                            s=s & " ,""F_BrutoMarge"":""" & .fields("F_BrutoMarge") & """"
                            s=s & " ,""F_Resultaat"":""" & .fields("F_Resultaat") & """"
                            s=s & " ,""F_Score"":""" & .fields("F_Score") & """"
                            s=s & " ,""F_kredielimiet"":""" & .fields("F_kredielimiet") & """"
                            s=s & " ,""F_LastBookyear"":""" & .fields("F_LastBookyear") & """"
                            s=s & " ,""F_PrefLangID"":""" & .fields("F_PrefLangID") & """"
                            s=s & " ,""jfc_s_desc"":""" & .fields("jfc_s_desc") & """"
                            s=s & " ,""jfc_l_desc"":""" & .fields("jfc_l_desc") & """"
                            s=s & " ,""nbc_desc"":""" & .fields("nbc_desc") & """"
                            s=s & " ,""gc_desc"":""" & statusText & """"
                        .movenext
                        loop                        
                    end if
                end with
        s = "{" & s & "}"    
        pJSONGetCompanyCompanyWebInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function


private function pJSONGetCompanyCompanyWebMandates()

    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = "select M.FM_VAT, M.FM_PersoonVoorNaam, M.FM_PersoonNaam, M.FM_Begin, M.FM_Einde, "    
    strSql = strSql & " case F.f_PrefLangID when 1 then C.fc_desc_fr when 2 then c.fc_desc_nl else c.fc_desc_en end as fc_desc "    
    strSql = strSql & " FROM CW_FirmaMandaten M "    
    strSql = strSql & " INNER JOIN KBO_FunctionCodes C ON M.FM_FunctionCode = C.fc_code "    
    strSql = strSql & " INNER JOIN CW_Firma F ON M.FM_VAT = F.F_VAT "
    strSql = strSql & " WHERE M.FM_VAT = " & CompanyVat()
    
    set objConn=Server.CreateObject("ADODB.Connection")
    set objRS = Server.CreateObject("ADODB.Recordset") 
    objConn.open BOConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRS.open strSql, objConn
	
        if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if

        pJSONGetCompanyCompanyWebMandates = s
        objRs.close : set objRs = nothing
        objConn.close : set objConn = nothing

end function
    private function pJSONGetCompanyCompanyWebInfoFromHbcId()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = " SELECT FU.FU_VAT, FU.FU_Name, FU.Fu_Vorm, FU.fu_StraatNaam, "    
    strSql = strSql & " FU.fu_HuisNr, FU.fu_BusNr, FU.fu_PostCode, "
    strSql = strSql & " FU.fu_Gemeente, F.f_begindatum, F.F_EindDatum, F.F_CurrentStatus, "
    strSql = strSql & " F.F_Kapitaal, F.F_MUNT, F.F_Omzet, F.F_Werknemers, "
    strSql = strSql & " F.F_BrutoMarge, F.F_Resultaat, F.F_Score, F.f_kredielimiet, "
    strSql = strSql & " F.F_LastBookyear, f.F_PrefLangID, JFC.jfc_l_desc_" & userlang() & " as jfc_l_desc, JFC.jfc_s_desc_" & userlang() & " as jfc_s_desc, "
    strSql = strSql & " N.nbc_desc_" & userlang() & " as nbc_desc, F_CurrentJurSitu"
    strSql = strSql & " from CW_Firma F "
    strSql = strSql & " INNER JOIN CW_Firma_unique_NL FU on F.F_VAT = FU.FU_VAT "
    strSql = strSql & " INNER JOIN KBO_JuridicalFormCodes JFC on F.F_CurrentJurSitu = jfc_code "
    strSql = strSql & " INNER JOIN KBO_Nacebel2008 N on F.F_HoofdNACE = N.nbc_code "
    strSql = strSql & " INNER JOIN HB_COMPANY H on F.F_VAT = H.HBC_FirmVat "
    strSQL = strSql & " where H.HBC_ID = " & hbc_id()
    
    set objConn=Server.CreateObject("ADODB.Connection")
    set objRS = Server.CreateObject("ADODB.Recordset") 
    objConn.open BOConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRS.open strSql, objConn

                with objRS
                    if not .bof and not .eof then
                        do while not .eof
                                                     dim statusText

                        if (len(.fields("F_EindDatum")) = 8) then
                            statusText = JurSitTrans(.fields("F_CurrentJurSitu")) & " : " & .fields("F_EindDatum")
                        else
                            if (.fields("F_EindDatum") <> "0" AND .fields("f_einddatum") <> "") then
                                statusText = JurSitTrans(.fields("F_CurrentJurSitu"))
                            else
                                statusText = JurSitTrans(0)
                            end if
                        end if

                            s= """fu_VAT"":""" & .fields("FU_VAT") & """"
                            s=s & " ,""fu_Name"":""" & .fields("FU_Name") & """"
                            s=s & " ,""fu_vorm"":""" & .fields("Fu_Vorm") & """"
                            s=s & " ,""fu_StraatNaam"":""" & .fields("fu_StraatNaam") & """"
                            s=s & " ,""fu_HuisNr"":""" & .fields("fu_HuisNr") & """"
                            s=s & " ,""fu_BusNr"":""" & .fields("fu_BusNr") & """"
                            s=s & " ,""fu_Postcode"":""" & .fields("fu_PostCode") & """"
                            s=s & " ,""fu_gemeente"":""" & .fields("fu_Gemeente") & """"
                            s=s & " ,""F_BeginDatum"":""" & .fields("f_begindatum") & """"
                            s=s & " ,""F_AfsluitDatum"":""" & .fields("F_EindDatum") & """"
                            s=s & " ,""F_CurrentStatus"":""" & .fields("F_CurrentStatus") & """"
                            s=s & " ,""F_Kapitaal"":""" & .fields("F_Kapitaal") & """"
                            s=s & " ,""F_Munt"":""" & .fields("F_MUNT") & """"
                            s=s & " ,""F_Omzet"":""" & .fields("F_Omzet") & """"
                            s=s & " ,""F_Werknemers"":""" & .fields("F_Werknemers") & """"
                            s=s & " ,""F_BrutoMarge"":""" & .fields("F_BrutoMarge") & """"
                            s=s & " ,""F_Resultaat"":""" & .fields("F_Resultaat") & """"
                            s=s & " ,""F_Score"":""" & .fields("F_Score") & """"
                            s=s & " ,""F_kredielimiet"":""" & .fields("F_kredielimiet") & """"
                            s=s & " ,""F_LastBookyear"":""" & .fields("F_LastBookyear") & """"
                            s=s & " ,""F_PrefLangID"":""" & .fields("F_PrefLangID") & """"
                            s=s & " ,""jfc_s_desc"":""" & .fields("jfc_s_desc") & """"
                            s=s & " ,""jfc_l_desc"":""" & .fields("jfc_l_desc") & """"
                            s=s & " ,""nbc_desc"":""" & .fields("nbc_desc") & """"
                            s=s & " ,""gc_desc"":""" & statustext & """"
                        .movenext
                        loop                        
                    end if
                end with
        s = "{" & s & "}"    
        pJSONGetCompanyCompanyWebInfoFromHbcId = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function


private function pJSONGetCompanyCompanyWebMandatesFromHbcId()

    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = "select M.FM_VAT, M.FM_PersoonVoorNaam, M.FM_PersoonNaam, M.FM_Begin, M.FM_Einde, "    
    strSql = strSql & " case F.f_PrefLangID when 1 then C.fc_desc_fr when 2 then c.fc_desc_nl else c.fc_desc_en end as fc_desc "    
    strSql = strSql & " FROM CW_FirmaMandaten M "    
    strSql = strSql & " INNER JOIN KBO_FunctionCodes C ON M.FM_FunctionCode = C.fc_code "    
    strSql = strSql & " INNER JOIN CW_Firma F ON M.FM_VAT = F.F_VAT "
    strSql = strSql & " INNER JOIN HB_Company H ON M.FM_VAT = H.HBC_FirmVAT"
    strSql = strSql & " WHERE H.HBC_ID = " & hbc_id()
    
    set objConn=Server.CreateObject("ADODB.Connection")
    set objRS = Server.CreateObject("ADODB.Recordset") 
    objConn.open BOConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRS.open strSql, objConn

        if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if

        pJSONGetCompanyCompanyWebMandatesFromHbcId = s
        objRs.close : set objRs = nothing
        objConn.close : set objConn = nothing

end function



'<end region PRIVATE METHODS & FUNCTIONS>


end class
%>
