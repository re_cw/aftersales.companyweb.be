﻿
//Database Operations
function JSON_GetCWCompanyDetailsFromHbcIdPromise(hbc_id) {
		$("#CWCompanyDetails" + hbc_id).empty();
		var url = "/classes/dal/clsCompanyWeb/clsCompanyWeb.asp";
		
		$.get(url, { q: 3, hbc_id: NullUndefinedToNewVal(hbc_id, "0") })
		.done(function (data) {
			var objCWCy = data;
			var gaugeValue = parseInt(objCWCy.F_Score);
			$("#CWCompanyDetails" + hbc_id).empty().html(SetCwInfoToTable(objCWCy));
			$("#gv" + hbc_id).html(gaugeValue / 10);
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Failed to get CWCompany Details From HBCID",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#CWCompanyDetails" + hbc_id).data('requestRunning',false);
		});
}

function JSON_GetCWCompanyDetailsPromise(vatnumber) {
		$("#CWCompanyDetails" + vatnumber).empty();
		var url = "/classes/dal/clsCompanyWeb/clsCompanyWeb.asp";
		
		$.get(url, {q:1, vat: NullUndefinedToNewVal(vatnumber,"0")})
		.done(function(data){
				var objCWCy = data;
				var gaugeValue = parseInt(objCWCy.F_Score);
				$("#CWCompanyDetails" + vatnumber).html(SetCwInfoToTable(objCWCy));
				$("#gv"+vatnumber).html(gaugeValue / 10);
		}).fail(function(resp){
			dhtmlx.message({
				text: "Failed to get CWCompany Details",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#CWCompanyDetails" + vatnumber).data('requestRunning',false);
		});	
}
function JSON_GetCWCompanyMandatesFromHbcIdPromise(hbc_id) {
		$("#CWCompanyMandates" + hbc_id).empty();
		var url = "/classes/dal/clsCompanyWeb/clsCompanyWeb.asp";
	
		return $.get(url, { q: 4, hbc_id: NullUndefinedToNewVal(hbc_id, "0") })
		.done(function (data) {
			var objCWMandates = data;
			$("#CWCompanyMandates" + hbc_id).html(SetCwMandatesToTable(objCWMandates));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Failed to get CWCompany MandatesFrom HbcId",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#CWCompanyMandates" + hbc_id).data('requestRunning', false);
		});
}
function JSON_GetCWCompanyMandatesPromise(vatnumber) {
		$("#CWCompanyMandates" + vatnumber).empty();
		var url = "/classes/dal/clsCompanyWeb/clsCompanyWeb.asp";
		
		return $.get(url, { q:2, vat: NullUndefinedToNewVal(vatnumber,"0")})
		.done(function(data){
				var objCWMandates = data;
				$("#CWCompanyMandates" + vatnumber).html(SetCwMandatesToTable(objCWMandates));
		}).fail(function(resp){
			dhtmlx.message({
				text: "Failed to get CWCompany Mandates",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#CWCompanyMandates" + vatnumber).data('requestRunning',false);
		});
}
function JSON_GetCompanyCompanyWebStateFromHbcId(hbcId, container) {
		$("#" + container).empty();
		var url = "/classes/dal/clsCompanyWeb/clsCompanyWeb.asp";
		
		return $.get(url, { q: 70, HBC_ID: NullUndefinedToNewVal(hbcId, "0") })
		.done(function (data) {
			if (!$.isEmptyObject(data)) {
				var text = '';
				switch (data.f_currentStatus) {
					case "0":
						text = '<strong class="success-nobackground">' + data.state + '</strong>';
						break;
					default:
						text = '<strong class="warning-nobackground">' + data.state + '(' + data.f_eindDatum + ')</strong>';
						break;
				}
				$("#" + container).html(text);
			}
		}).fail(function(resp){
			dhtmlx.message({
				text: "Failed to get CWCompany State from HBCID",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning', false);
		});
}
function JSON_GetCompanyCompanyWebStateFromVat(vatnumber, container)
{
		$("#" + container).empty();
		var url = "/classes/dal/clsCompanyWeb/clsCompanyWeb.asp";

		return $.get(url, { q: 80, vat: NullUndefinedToNewVal(vatnumber, "0") })
		.done(function (data) {
			if (!$.isEmptyObject(data)) {
				var text = '';
				switch(data.f_currentStatus)
				{
					case "0":
						text = '<strong class="success-nobackground">' + data.state + '</strong>';
						break;
					default:
						text = '<strong class="warning-nobackground">' + data.state + '(' + data.f_eindDatum + ')</strong>';
						break;
				}
				$("#" + container).html(text);
			}
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Failed to get CWCompany State from VAT",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(data){
			$("#" + container).data('requestRunning', false);
		});
}

//Formatting Operations
function CurrencyFormatted(number) {
    var numberStr = parseFloat(number).toFixed(2).toString();
    var numFormatDec = numberStr.slice(-2); /*decimal 00*/
    numberStr = numberStr.substring(0, numberStr.length - 3); /*cut last 3 strings*/
    var numFormat = new Array;
    while (numberStr.length > 3) {
        numFormat.unshift(numberStr.slice(-3));
        numberStr = numberStr.substring(0, numberStr.length - 3);
    }
    numFormat.unshift(numberStr);
    return "€ " + numFormat.join('.') + ',' + numFormatDec; /*format 000.000.000,00 */
}


function SetCwInfoToTable(objCWCy) {

    var t = "<table cellpadding=2 width=580px>";
    t += "<tr><td colspan=3><span class=CwCyName>" + objCWCy.fu_Name + " (" + objCWCy.fu_vorm + ")</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblCreditLimit + "</td><td>" + CurrencyFormatted(objCWCy.F_kredielimiet) + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblNoEntreprise + "</td><td>" + objCWCy.fu_VAT + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblAdress + "</td><td>" + objCWCy.fu_StraatNaam + " " + objCWCy.fu_HuisNr + " " + objCWCy.fu_BusNr + "</td></tr>";
    t += "<tr class=CwCyInfo><td></td><td colspan=2>" + objCWCy.fu_Postcode + " " + objCWCy.fu_gemeente + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblCompanyStatus + "</td><td colspan=2>" + objCWCy.gc_desc + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblActivity + "</td><td colspan=2>" + objCWCy.nbc_desc + "</td></tr>"
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblTurnOver + "</td><td colspan=2>" + CurrencyFormatted(objCWCy.F_Omzet) + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblResult + "</td><td colspan=2>" + CurrencyFormatted(objCWCy.F_Resultaat) + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblGrossMargin + "</td><td colspan=2>" + CurrencyFormatted(objCWCy.F_BrutoMarge) + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblWorkers + "</td><td colspan=2>" + objCWCy.F_Werknemers + "</td></tr>";
    t += "<tr class=CwCyInfo><td class=CwCylblInfo>" + locale.main_page.lblLastBalance + "</td><td colspan=2>" + objCWCy.F_LastBookyear + "</td></tr>";
    t += "</table>";

    return t;
}

function SetCwMandatesToTable(objCWMandates) {
    var t = "<table cellpadding=2 width=80%>";
    t += "<tr><td colspan=4 class=CwCylblMandatesTitle>" + locale.main_page.lblManagement + "</td></tr>"
    t += "<tr><td class=CwCylblMandates>" + locale.main_page.lblMandateName + "</td><td class=CwCylblMandates>" + locale.main_page.lblMandateDesc + "</td><td class=CwCylblMandates>" + locale.main_page.lblMandateBegin + "</td><td class=CwCylblMandates>" + locale.main_page.lblMandateEnd + "</td></tr>";
    for (i = 0; i < objCWMandates.length; i++) {
        t += "<tr><td class=CwCylblMandatesInfo>" + objCWMandates[i].FM_PersoonNaam + " " + objCWMandates[i].FM_PersoonVoorNaam + "</td><td class=CwCylblMandatesInfo>" + objCWMandates[i].fc_desc + "</td><td class=CwCylblMandatesInfo>" + objCWMandates[i].FM_Begin + "</td><td class=CwCylblMandatesInfo>" + objCWMandates[i].FM_Einde + "</td></tr>";
    }
    t += "</table>";
    return t;
}