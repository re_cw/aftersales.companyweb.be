var company_vat;

function populateOrderObject(objOrder) {
    var params = "order_list_token_id=" + encodeURIComponent(objOrder.order_list_token_id);
    params += "&item_package=" + encodeURIComponent(objOrder.item_package);
    params += "&item_alert=" + encodeURIComponent(objOrder.item_alert);
    params += "&item_mobileApp=" + encodeURIComponent(objOrder.item_mobileApp);
    params += "&item_intRep=" + encodeURIComponent(objOrder.item_intRep);
    params += "&item_Cymanagement=" + encodeURIComponent(objOrder.item_Cymanagement);
    params += "&order_company_id=" + encodeURIComponent(objOrder.order_company_id);
    params += "&order_company_vat=" + encodeURIComponent(objOrder.order_company_vat);
    params += "&order_contact_id=" + encodeURIComponent(objOrder.order_contact_id);
    params += "&order_event_id=" + encodeURIComponent(objOrder.order_event_id);
    params += "&order_user_id=" + encodeURIComponent(objOrder.order_user_id);
    params += "&order_item_alert_price=" + encodeURIComponent(objOrder.order_item_alert_price);
    params += "&order_item_alert_qty=" + encodeURIComponent(objOrder.order_item_alert_qty);
    params += "&order_item_mobileApp_price=" + encodeURIComponent(objOrder.order_item_mobileApp_price);
    params += "&order_item_mobileApp_qty=" + encodeURIComponent(objOrder.order_item_mobileApp_qty);
    params += "&order_item_intRep_price=" + encodeURIComponent(objOrder.order_item_intRep_price);
    params += "&order_item_intRep_qty=" + encodeURIComponent(objOrder.order_item_intRep_qty);
    params += "&order_saved_with_sucess=" + encodeURIComponent(objOrder.order_saved_with_sucess);
    params += "&isbestelbon=" + encodeURIComponent(objOrder.isbestelbon);
    params += "&trusted=1";
    return params;
}


function AjaxOrders_SaveOrderPromise(objOrder) {
	var url = "/classes/dal/clsOrders/clsOrders.asp?a=1";
	var params = populateOrderObject(objOrder);
	
	return $.post(url, params)
	.done(function(data){
		
	}).fail(function(resp){
	    dhtmlx.message({
	        text: "Error Saving Order",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}
