﻿<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objOrder.asp" -->
<!-- #include virtual ="/classes/dal/clsComments/clsComments.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"
CONST C_SUBSCRIPTION_SOLD = 17
CONST C_SUBSCRIPTION_SOLD_TEXT = "Event closed by system, subscription sold"

CONST C_CUSTOMIZED_OFFER = "SYSTEM: Custom Offer Sent"     
CONST C_FOLLOWUP_ORDER = 4
CONST C_RECALL = 5
Dim objOrder, OrderObject

   
    set objOrder = new sysOrders
    set OrderObject = new clsObjOrder

    select case request.querystring("a")
    case 1
        objOrder.SaveOrder
    case else
    end select


class sysOrders

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    public function SaveOrder()
        SaveOrder = pSaveOrder()        
    end function

'<START REGION BUSINESS LOGIC>===========================================================



    private function pSaveOrder()
        Dim objConn, objRsOrder, objRsItems, objRsEvent, strSql, sErrMsg, objCmd
        Dim arrItems(4), i
        Dim arrPrice(3), arrQty(3)
        Dim event_type_id

        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if
    
    OrderObject.populateOrderObject
    objConn.begintrans
    
    'insert order items
        set objRsItems=Server.CreateObject("ADODB.Recordset")
        strSql = "select top 1 * from [dbo].[orders_list] where 1=0"
        objRsItems.open strSql, ObjConn, 2, 3 
        
        arrItems(0)=OrderObject.item_package
        arrItems(1)=OrderObject.item_alert
        arrItems(2)=OrderObject.item_mobileApp
        arrItems(3)=OrderObject.item_intRep
        arrItems(4)=OrderObject.item_Cymanagement

        arrPrice(1) = OrderObject.order_item_alert_price
        arrQty(1) = OrderObject.order_item_alert_qty
        arrPrice(2) = OrderObject.order_item_mobileApp_price
        arrQty(2) = OrderObject.order_item_mobileApp_qty
        arrPrice(3) = OrderObject.order_item_intRep_price
        arrQty(3) = OrderObject.order_item_intRep_qty

        for i = lbound(arrItems) to ubound(arritems)-1
            if arrItems(i) > 0 then
                with objRsItems
                    .addnew
                        .fields("order_list_token_id") = OrderObject.order_list_token_id
                        .fields("order_item_id") = arrItems(i)
                        .fields("order_item_price") = arrPrice(i)
                        .fields("order_item_qty") = arrQty(i)
                        .fields("AuditDateCreated") = now()
                        .fields("AuditUserCreated") = usr_id()
                    .update
                end with
            end if
        next
    if err.number <> 0 then sErrMsg = sErrMsg & err.description & "<br>"

    'create order
        set objRsOrder=Server.CreateObject("ADODB.Recordset")
        strSql = "select top 1 * from [dbo].[orders] where 1=0"
        objRsOrder.open strSql, ObjConn, 2, 3
            with objRsOrder
                .addnew
                    .fields("order_list_token_id") = OrderObject.order_list_token_id
                    .fields("event_id") = OrderObject.order_event_id
                    .fields("company_id") = OrderObject.order_company_id
                    .fields("contact_id") = OrderObject.order_contact_id
                    .fields("user_id") = usr_id()
                    .fields("order_creationdate") = now()
                    .fields("AuditDateCreated") = now()
                    .fields("AuditUserCreated") = usr_id()
                .update
            end with    
    if err.number <> 0 then sErrMsg = sErrMsg & err.description & "<br>"

    if OrderObject.isbestelbon = "false" then
    event_type_id = C_SUBSCRIPTION_SOLD
    strComment=C_SUBSCRIPTION_SOLD_TEXT
    'update event to closed state
        set objRsEvent=Server.CreateObject("ADODB.Recordset")
        strsql = "select * from [dbo].[events] where id=" &  OrderObject.order_event_id
        objRsEvent.open strSql, ObjConn, 2, 3
         with objRsEvent
                    .fields("event_type_id") = C_SUBSCRIPTION_SOLD
                    .fields("readonly") = true
                    .fields("visible") = false
                    .fields("text") = C_SUBSCRIPTION_SOLD_TEXT
                    .fields("AuditDateUpdated") = now()
                    .fields("AuditUserCreated") = usr_id()
                .update
         end with 
    if err.number <> 0 then sErrMsg = sErrMsg & err.description & "<br>"
    else
    event_type_id = C_RECALL
    strComment=C_CUSTOMIZED_OFFER
    end if

    if err.number <> 0 then 
        objConn.rollbacktrans 
        pSaveOrder=sErrMsg
    else 
        objConn.CommitTrans
        pSaveOrder=1
        AddCommentObject event_type_id, strComment'add History Entry
    end if
    
    set objConn=nothing
    end function

private function AddCommentObject(event_type_id, strComment)
    objComment.comment_event_type_id = event_type_id
    objComment.comment_usr_id = usr_id()
    objComment.comment_event_id = OrderObject.order_event_id
    objComment.comment_lead_id = 0
    objComment.comment_company_vat = OrderObject.order_company_vat
	objComment.comment_company_id = OrderObject.order_company_id
    objComment.comment_date = objsysComment.SetDate()
    objComment.comment_time = objsysComment.SetTime()
    objComment.comment=strComment
    AddCommentObject = objsysComment.AddComment(objComment)
end function

      
'<END REGION BUSINESS LOGIC>=============================================================

public function CreateGUID()
  Randomize Timer
  Dim tmpTemp1,tmpTemp2,tmpTemp3
  tmpTemp1 = Right(String(15,48) & CStr(CLng(DateDiff("s","1/1/2000",Date()))), 15)
  tmpTemp2 = Right(String(5,48) & CStr(CLng(DateDiff("s","12:00:00 AM",Time()))), 5)
  tmpTemp3 = Right(String(5,48) & CStr(Int(Rnd(1) * 100000)),5)
  CreateGUID = tmpTemp1 & tmpTemp2 & tmpTemp3
End Function

    public sub Class_Terminate()
        set objOrder = nothing        
    end sub

end class

%>