﻿
<!-- #include virtual ="/classes/dal/connector.inc" -->
<% 
Dim objPerson
    Set objPerson = new sysPerson

select case request.querystring("i")
    case 0
    case 1
        objPerson.JSON_GetUserFriendlyInfo()
    case 2
        objPerson.JSON_GetUserList()
    case 3
        objPerson.JSON_GetComboUserList()
	case 4
        objPerson.JSON_GetMeetingRoomSettingsForUser()
	case 5
        objPerson.AJAX_AddMeetingRoom()
	case 6
        objPerson.AJAX_EditMeetingRoom()
	case 7
        objPerson.AJAX_ChangeMeetingRoomStatus()
    case else
end select

set objPerson = nothing

CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"

class sysPerson

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property

    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property


'<start region PUBLIC METHODS & FUNCTIONS>

    public function JSON_GetUserFriendlyInfo()
        Response.ContentType = "application/json" : response.write pJSON_GetUserFriendlyInfo()
    end function

    public function JSON_GetUserList()
        Response.ContentType = "application/json" : response.write pJSON_GetUserList()
    end function

    public function JSON_GetComboUserList()
        Response.ContentType = "application/json": response.write pJSON_GetComboUserList()
    end function
	
	public function JSON_GetMeetingRoomSettingsForUser()
        Response.ContentType = "application/json": response.write pJSON_GetMeetingRoomSettingsForUser()
    end function
	
	public function AJAX_AddMeetingRoom()
        Response.ContentType = "application/json": response.write pAddMeetingRoom()
    end function
	
	public function AJAX_EditMeetingRoom()
        Response.ContentType = "application/json": response.write pEditMeetingRoom()
    end function
	
	public function AJAX_ChangeMeetingRoomStatus()
        Response.ContentType = "application/json": response.write pAJAX_ChangeMeetingRoomStatus()
    end function
	
	
	
'<end region PUBLIC METHODS & FUNCTIONS>




'<start region PRIVATE METHODS & FUNCTIONS>

    private function pJSON_GetUserFriendlyInfo()
    Dim objConn, oRs, strSql,  s,d
        strSql = "SELECT * FROM [dbo].[vw_app_sys_users_friendly_info]  WHERE ([usr_id] =" & usr_id & ")"
           
        set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext            
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                with oRs
                    if not .bof and not .eof then
                    s= "{""person"" :["
                        do while not .eof                
                            s=s & "{"
                            s=s & " ""usr_id"":""" & .fields("usr_id") & """"
                            s=s & " ,""usr_login"":""" & .fields("usr_login") & """"
                            s=s & " ,""role_name"":""" & .fields("role_name") & """"
                            s=s & " ,""firstname"":""" & .fields("firstname") & """"
                            s=s & " ,""lastname"":""" & .fields("lastname") & """"
                            s=s & " ,""extension"":""" & .fields("extension") & """"
                            s=s & " ,""lang_name"":""" & .fields("lang_name") & """"
							s=s & " ,""mr_active"":""" & .fields("mr_active") & """"
                            s=s & " },"
                        .movenext
                        loop   
                    end if
                End With
                s = left(s,len(s)-1)
                s = s & "]}"
            
            pJSON_GetUserFriendlyInfo = s
            
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function

private function pJSON_GetUserList()
    Dim objConn, oRs, strSql,  s,d, fullname
        strSql = "SELECT * FROM [dbo].[vw_app_sys_users_friendly_info]"
           
        set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext            
                oRs.cursorlocation = 3
                oRs.open strSql, objConn,1 ,1
                with oRs
                    if not .bof and not .eof then
                    s= "{""options"" :["
                        do while not .eof  
                            fullname = .fields("usr_id") & ". " & .fields("firstname") & " " & .fields("lastname") & " (" & .fields("extension") & ") (" & .fields("lang_name") & ")"            
                            s=s & "{"
                            s=s & " ""value"":""" & .fields("usr_id") & """"
                            s=s & " ,""text"":""" & fullname & """"
                            s=s & " },"
                        .movenext
                        loop   
                    end if
                End With
                s = left(s,len(s)-1)
                s = s & "]}"
            
            pJSON_GetUserList = s
            
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function




private function pJSON_GetComboUserList()
    Dim objConn, oRs, strSql,  s,d, fullname
        strSql = "SELECT * FROM [dbo].[vw_app_sys_users_friendly_info]"
           
        set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext            
                oRs.cursorlocation = 3
                oRs.open strSql, objConn,1 ,1
                 with oRs
                    if not .bof and not .eof then    
                        s= "["
                        do while not .eof
                            fullname = .fields("usr_id") & ". " & .fields("firstname") & " " & .fields("lastname") & " (" & .fields("extension") & ") (" & .fields("lang_name") & ")"            
                            s=s & "{"
                                s=s & """key"":""" & .fields("usr_id") & """"
                                s=s & ",""label"":""" & fullname & """"
                            s=s & "},"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        s = s & "]"    
                    end if
                end with
    pJSON_GetComboUserList=s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function

private function pJSON_GetMeetingRoomSettingsForUser()
    Dim objConn, oRs, strSql,  s,d, fullname
        strSql = "select * from [dbo].[meetingroom] where mr_usr_id = " + usr_id()
           
        set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext            
                oRs.cursorlocation = 3
                oRs.open strSql, objConn,1 ,1
                 with oRs
                    if not .bof and not .eof then    
                        do while not .eof          
                            s=s & "{"
                                s=s & """name"":""" & .fields("mr_name") & """"
                                s=s & ",""sessionID"":""" & .fields("mr_sessionID") & """"
								s=s & ",""isNew"":""0"""
                            s=s & "},"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
					else
						s=s & "{"
							s=s & """name"": """""
							s=s & ",""sessionID"": ""0"" "
							s=s & ",""isNew"":""1"""
						s=s & "}"
                    end if
                end with
    pJSON_GetMeetingRoomSettingsForUser=s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function

private function pAddMeetingRoom()
	Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd, objName, objSessionId
	
	set objConn=Server.CreateObject("ADODB.Connection")
	with objConn
		.ConnectionString = SalesToolConnString()
		.CommandTimeOut = 15
		.mode=3 'adModeReadWrite
		.CursorLocation = 3 '2 adUseServer 3 adUseClient
		.Open    
	end with

	if objConn.State = 0 then 
	set objConn = nothing
		exit function
	end if
	
	set objName = request("user_mr_name")
	set objSessionId = request("user_mr_sessionID")
	
	if Len(objName) > 3 then
		strSql = " insert into meetingroom (mr_usr_id,mr_active, mr_name, mr_sessionID) values (" & usr_id()
		strSql = strSql & ",0,'" & objName & "','" & objSessionId & "')"
		'Response.Write("<br>strSql:" & strSql)
		objConn.Execute(strSql)      
		pAddMeetingRoom="{""success"": ""true"" }"
	else
		pAddMeetingRoom="{""responseText"": ""Meeting roomname to short"" }"
	end if
	

	objConn.close : set objConn = nothing
end function

private function pEditMeetingRoom()
	Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd, objName, objSessionId
	
	set objConn=Server.CreateObject("ADODB.Connection")
	with objConn
		.ConnectionString = SalesToolConnString()
		.CommandTimeOut = 15
		.mode=3 'adModeReadWrite
		.CursorLocation = 3 '2 adUseServer 3 adUseClient
		.Open
	end with

	if objConn.State = 0 then 
	set objConn = nothing
		exit function
	end if
	
	set objName = request("user_mr_name")
	set objSessionId = request("user_mr_sessionID")
	
	if Len(objName) > 3 then
		strSql = " update meetingroom set mr_name='" & objName & "', mr_sessionID='" & objSessionId
		strSql = strSql & "' where mr_usr_id = " & usr_id()
		'Response.Write("<br>strSql:" & strSql)
		objConn.Execute(strSql)
		pEditMeetingRoom="{""success"": ""true"" }"
	else
		pEditMeetingRoom="{""responseText"": ""Meeting roomname to short"" }"
	end if
           
    objConn.close : set objConn = nothing
end function

private function pAJAX_ChangeMeetingRoomStatus()
	Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd
	
	set objConn=Server.CreateObject("ADODB.Connection")
	with objConn
		.ConnectionString = SalesToolConnString()
		.CommandTimeOut = 15
		.mode=3 'adModeReadWrite
		.CursorLocation = 3 '2 adUseServer 3 adUseClient
		.Open
	end with

	if objConn.State = 0 then 
	set objConn = nothing
		exit function
	end if

	strSql = " update meetingroom set mr_active=(CASE mr_active WHEN 1 THEN 0 ELSE 1 END)"
	strSql = strSql & " where mr_usr_id = " & usr_id()
	'Response.Write("<br>strSql:" & strSql)
	objConn.Execute(strSql)
           
    pAJAX_ChangeMeetingRoomStatus="{""success"": ""true"" }"
    objConn.close : set objConn = nothing
end function


end class

%>