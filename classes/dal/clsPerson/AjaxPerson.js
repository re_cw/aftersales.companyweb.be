﻿
function JSON_GetUserFriendlyInfo() {
		$("#UserFriendlyInfo").empty();
		$("#UserMeetingStatus").empty();
    
		var url = "/classes/dal/clsPerson/clsPerson.asp";
		$.get(url, { i: 1})
		.done(function(data){
			var text = data.person[0].usr_id + ". " + data.person[0].firstname + " " + data.person[0].lastname + ", " + data.person[0].role_name + " [" + data.person[0].usr_login + "] - (" + data.person[0].extension + ") " + data.person[0].lang_name;		
			var statustext = "meeting status: ";
			if(data.person[0].mr_active == "1" ){
				statustext = statustext + "<span style='color:green;'>online</span>";
				if($('div[title="Meeting"]').length > 0)
					$('div[title="Meeting"]').find("img").attr('src','/graphics/common/win_16x16/Pin-green.png');
			}else if(data.person[0].mr_active == "0" ){
				statustext = statustext + "<span style='color:red;'>offline</span>";
				if($('div[title="Meeting"]').length > 0)
					$('div[title="Meeting"]').find("img").attr('src','/graphics/common/win_16x16/Pin-red.png');
			}else{
				statustext = statustext + "<span style='color:orange;'>set up</span>";
				if($('div[title="Meeting"]').length > 0)
					$('div[title="Meeting"]').find("img").attr('src','/graphics/common/win_16x16/Pin-red.png');
			}
			$("#UserMeetingStatus").html(statustext);
			$("#UserFriendlyInfo").text(text);
		})
		.fail(function(){
			dhtmlx.message({
				text: "Error getting UserInfo.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#UserFriendlyInfo").data('requestRunning',false);
		});
}

function JSON_GetMeetingRoomSettingsForUser() {
		var url = "/classes/dal/clsPerson/clsPerson.asp";
		return $.get(url, { i: 4})
		.fail(function(){
			dhtmlx.message({
				text: "Error getting UserInfo.",
				expire: -1,
				type: "errormessage"
			});
		});
}

function AjaxMR_AddMeetingRoom(obj) {
	var url = "/classes/dal/clsPerson/clsPerson.asp?i=5";
    var params = "user_mr_name=" + encodeURIComponent(obj.user_mr_name);
    params += "&user_mr_sessionID=" + encodeURIComponent(obj.user_mr_sessionID);
	
	$.post(url,params)
	.done(function(data){
		switch(data.success)
		{
			case "true":
				dhtmlx.message({
					text: "Meeting room edited",
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + "Meeting room edited");
			break;
			default:
				dhtmlx.message({
					text: "Meeting room edited failed",
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + "Meeting room edited failed" + "\n" + data.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error editing meeting room",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function AjaxMR_EditMeetingRoom(obj) {
	var url = "/classes/dal/clsPerson/clsPerson.asp?i=6";
    var params = "user_mr_name=" + encodeURIComponent(obj.user_mr_name);
    params += "&user_mr_sessionID=" + encodeURIComponent(obj.user_mr_sessionID);
	
	$.post(url,params)
	.done(function(data){
		switch(data.success)
		{
			case "true":
				dhtmlx.message({
					text: "Meeting room edited",
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + "Meeting room edited");
			break;
			default:
				dhtmlx.message({
					text: "Meeting room edited failed",
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + "Meeting room edited failed" + "\n" + data.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error editing meeting room",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

function AjaxMR_ChangeMeetingRoomStatus() {
	var url = "/classes/dal/clsPerson/clsPerson.asp?i=7";
	var params = "";
	$.post(url,params)
	.done(function(data){
		switch(data.success)
		{
			case "true":
				dhtmlx.message({
					text: "Meeting room status changed",
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + "Meeting room status changed");
			break;
			default:
				dhtmlx.message({
					text: "Meeting room edited failed",
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + "Meeting room edited failed" + "\n" + data.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error editing meeting room",
	        expire: -1,
	        type: "errormessage"
	    });
	});
}

