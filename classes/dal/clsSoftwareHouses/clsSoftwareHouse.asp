﻿<!-- #include virtual ="/classes/dal/connector.inc" -->

<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"

Dim objSoftHouse
   
    set objSoftHouse = new sysSoftwareHouses

        select case request.QueryString("a")         
            case "GetComboSoftwareHousesWithImg"
                objSoftHouse.GetComboSoftwareHousesWithImg
            case "GetCompany_SoftwareHouse"
                objSoftHouse.JSONGetCompany_SoftwareHouses
            case "GetCompany_SoftwareHouseFromHbcId"
                objSoftHouse.JSONGetCompany_SoftwareHousesFromHbcId
            case "deleteSoftwareHouse"
                objSoftHouse.deleteCompany_SoftwareHouse        
            case else 'default should be read
        end select
    

class sysSoftwareHouses

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang
    

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get hbc_id()
        hbc_id = trim(request.QueryString("hbc_id"))
        if len(hbc_id) = 0 then hbc_id = -1
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property

    property get company_id()
        company_id=trim(request.querystring("company_id"))
        if len(company_id) = 0 then company_id = 0
    end property

    property get link_id()
        link_id=request("link_id")
    end property

    public function GetComboSoftwareHousesWithImg()
        GetComboSoftwareHousesWithImg = pGetComboSoftwareHousesWithImg() :  response.write GetComboSoftwareHousesWithImg
    end function
        public function JSONGetCompany_SoftwareHouses() 
        Response.ContentType = "application/json" : response.write pJSONGetCompany_SoftwareHouses()
    end function
    public function JSONGetCompany_SoftwareHousesFromHbcId
        response.ContentType = "application/json" : response.Write pJSONGetCompany_SoftwareHousesFromHbcId()
    end function
        public function deleteCompany_SoftwareHouse()
    response.write pdeleteCompany_SoftwareHouse()
    end function

'<START REGION JSON BUSINESS LOGIC>===========================================================
     private function pdeleteCompany_SoftwareHouse()
            Dim objConn, strSql
            strSql = "DELETE FROM [dbo].[link_company_softwarehouses] WHERE [link_id]=" & link_id()
            set objConn=Server.CreateObject("ADODB.Connection")
            with objConn
                .open SalesToolConnString() , adOpenDynamic, adLockOptimistic, adcmdtext
                .execute strsql
                .close
            end with
        set objConn = nothing
        if err.number <> 0 then pdeleteCompany_SoftwareHouse = err.Description
    end function

    private function pGetComboSoftwareHousesWithImg()       
            Dim objConn, oRs, strSql, SoftHouseName, softPackName, s, strText
            SoftHouseName = "soft_house_name_" & UserLang() 
            softPackName = "soft_pack_name_" & UserLang() 

            strSql = "SELECT * FROM dbo.[vw_GetSoftwareHouses] order by [soft_house_order]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            do while not .eof
                                strText = .fields(SoftHouseName) & " - " & .fields(softPackName)
                                's = s & "{key:" & .fields(0) & ",label:'" & .fields(EventTypeName) & "'},"
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields("soft_house_img") & """, text:""" & strText & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboSoftwareHousesWithImg = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

    private function pJSONGetCompany_SoftwareHousesFromHbcId()
        Dim a, SoftHouseName, softPackName , strText
        a = company_id() 
        SoftHouseName = "soft_house_name_" & UserLang()
        softPackName = "soft_pack_name_" & UserLang()    
        strSql = "select * from dbo.vw_GetCompany_SoftwaresHouses where company_id=" & a

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
                
     s= "["
    with oRs
                if not .bof and not .eof then

                     
                       
                        do while not .eof
                            strText = .fields(SoftHouseName) & " - " & .fields(softPackName)
                            s=s & "{"
                                s=s & " ""link_id"":""" & .fields("link_id") & """"
                                s=s & " ,""SoftHouseName"":""" & strText & """"                          
                                s=s & " ,""soft_house_img"":""" & .fields("soft_house_img") & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                          
                end if
           end with
    s = s & "]"  
    oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetCompany_SoftwareHousesFromHbcId = s
    end function
    private function pJSONGetCompany_SoftwareHouses()
    
    Dim a, SoftHouseName, softPackName , strText
        a = company_id() 
        SoftHouseName = "soft_house_name_" & UserLang()
        softPackName = "soft_pack_name_" & UserLang()    
        strSql = "select * from dbo.vw_GetCompany_SoftwaresHouses where company_id=" & a

            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
                                        s= "["
    with oRs
                if not .bof and not .eof then

                     

                        do while not .eof
                            strText = .fields(SoftHouseName) & " - " & .fields(softPackName)
                            s=s & "{"
                                s=s & " ""link_id"":""" & .fields("link_id") & """"
                                s=s & " ,""SoftHouseName"":""" & strText & """"                          
                                s=s & " ,""soft_house_img"":""" & .fields("soft_house_img") & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                          
                end if
           end with
     s = s & "]" 
    
    oRs.Close : set oRs = nothing
    objConn.close : set objConn = nothing

    pJSONGetCompany_SoftwareHouses = s


end function


'<END REGION JSON BUSINESS LOGIC>=============================================================



public sub Class_Terminate()
    set objSoftHouse = nothing
end sub


end class

%>