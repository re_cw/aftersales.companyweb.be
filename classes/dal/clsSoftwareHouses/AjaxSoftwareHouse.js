﻿
function AjaxSoftwareHouse_DeleteEntry(id) {
	var url = "/classes/dal/clsSoftwareHouses/clsSoftwareHouse.asp";
	
	$.get(url, { a: "deleteSoftwareHouse", link_id: id})
	.done(function(data){
		
	}).fail(function (resp) {
		dhtmlx.message({
			text: "Error Deleting software Entry",
			expire: -1,
			type: "errormessage"
		});
	});
}

function AjaxSoftwareHouse_GetSoftwareHousesPromise() {
	var url = "/classes/dal/clsSoftwareHouses/clsSoftwareHouse.asp";
	
	return $.get(url, { a: "GetComboSoftwareHousesWithImg" })
	.fail(function (resp) {
		dhtmlx.message({
			text: "Error getting Software Houses",
			expire: -1,
			type: "errormessage"
		});
	});
}

function AjaxSoftwareHouse_GetSoftwareHousesListFromHbcId(container, id)
{
		$("#" + container).empty();
		var url = "/classes/dal/clsSoftwareHouses/clsSoftwareHouse.asp";
	
		$.get(url, { a: "GetCompany_SoftwareHouseFromHbcId", hbc_id: id })
		.done(function (data) {
			$("#" + container).html(formatListSoftwareHouses(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Software houses from HBCID",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

function AjaxSoftwareHouse_GetSoftwareHousesList(container, id) {
		$("#" + container).empty();
		var url = "/classes/dal/clsSoftwareHouses/clsSoftwareHouse.asp";
		
		$.get(url, {a: "GetCompany_SoftwareHouse", company_id: id})
		.done(function(data){
			$("#" + container).html(formatListSoftwareHouses(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Softwarehouses list",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

function formatListSoftwareHouses(obj) {
	var t = '';
	if (!$.isEmptyObject(obj)){

		var img = '';
		var imgDel = '';
		t = "<table cellpadding=1 width=100% border=0>";
		for (var i = 0; i < obj.length; i++) {
			img = '<img src="/graphics/common/imgcombo/softwarehouses/' + obj[i].soft_house_img + '">';
			imgDel = '<img src ="/graphics/common/win_16x16/delete.png" class="onmouseoverdel" onClick="delItem(' + obj[i].link_id + ', category);">';
			t += "<tr><td width=10%>" + img + "</td><td>" + obj[i].SoftHouseName + "</td><td align=right>" + imgDel.replace("category", "'softwarehouse'") + "</td></tr>";
		}
		t += "</table>";
	}
	return t;
}