﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"
CONST C_BASIC_HR_FUNCTION= 4

    CONST C_EQUAL = "="
    CONST C_LIKE = "like"
    CONST C_IN = "in"

Dim objDocument, objMail
   
    set objDocument = new sysDocument

    select case request.QueryString("a")         
        case "GetComboDocNaturesWithImg"
            objDocument.GetComboDocNaturesWithImg
        case "GetComboDocTypesWithImg"
            objDocument.GetComboDocTypesWithImg
        case "GetDocNamesWithImg"
            objDocument.GetDocNamesWithImg
        case "GetComboDocLangsWithImg"
            objDocument.GetComboDocLangsWithImg    
        case "GetDocumentsList"
            objDocument.GetDocumentsList    
    case else 'default should be read
    end select
    

class sysDocument

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang   

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get ProdConnString()
        ProdConnString = sConnStrBackOfficeProd
    end property

    property get BOConnString()
        BOConnString = sConnStrBackOfficeProd
    end property

    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get hr_function_id()
        hr_function_id =Request.Cookies ("TELESALESTOOLV2")("hrfunctionid")
        if len(trim(hr_function_id)) = 0 then hr_function_id = C_BASIC_HR_FUNCTION    
    end property
    
    property get doc_root_id()
        doc_root_id=request("doc_root_id")
    end property
    
    property get doc_nature_id()
        doc_nature_id=request("doc_nature_id")
    end property

    property get doc_type_id()
        doc_type_id=request("doc_type_id")
    end property

    property get doc_lang_id()
        doc_lang_id=request("doc_lang_id")
    end property


    public function GetComboDocNaturesWithImg()
        GetComboDocNaturesWithImg = pGetComboDocNaturesWithImg() :  response.write GetComboDocNaturesWithImg
    end function
    
    public function GetComboDocTypesWithImg()
        GetComboDocTypesWithImg = pGetComboDocTypesWithImg() :  response.write GetComboDocTypesWithImg
    end function
    
    public function GetDocNamesWithImg()
        GetDocNamesWithImg = pGetDocNamesWithImg() :  response.write GetDocNamesWithImg
    end function
 
    public function GetComboDocLangsWithImg()
        GetComboDocLangsWithImg = pGetComboDocLangsWithImg() :  response.write GetComboDocLangsWithImg
    end function
   
    public function GetDocumentsList()
        GetDocumentsList = pGetDocumentsList() : response.write GetDocumentsList
    end function

    
    

'<START REGION JSON BUSINESS LOGIC>===========================================================

'get list of available document natures(json format)
    private function pGetComboDocNaturesWithImg()       
        Dim objConn, oRs, strSql, fieldText, fieldImg, s
            fieldText = "doc_nature_name_" & UserLang() 
            fieldImg = "doc_nature_img"
            strSql = "SELECT * FROM dbo.[vw_GetDocNatures] WHERE hr_function_id >=" & hr_function_id()
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            do while not .eof
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields(fieldImg) & """, text:""" & .fields(fieldText) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboDocNaturesWithImg = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

'get list of available document types(json format)
    private function pGetComboDocTypesWithImg()       
        Dim objConn, oRs, strSql, fieldText, fieldImg, s
            fieldText = "doc_type_name_" & UserLang() 
            fieldImg = "doc_type_img"
            strSql = "SELECT * FROM dbo.[vw_GetDocTypes] WHERE hr_function_id >=" & hr_function_id()
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            do while not .eof
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields(fieldImg) & """, text:""" & .fields(fieldText) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboDocTypesWithImg = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

    'get list of available document lang(json format)
    private function pGetComboDocLangsWithImg()       
        Dim objConn, oRs, strSql, fieldText, fieldImg, s
            fieldText = "lang_name"
            fieldImg = "lang_img"
            strSql = "SELECT * FROM dbo.[vw_GetLangs]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            do while not .eof
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields(fieldImg) & """, text:""" & .fields(fieldText) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboDocLangsWithImg = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

'get list of sent mails based on criteria
private function pGetDocumentsList()
    Dim objConn, oRs, strSql, sqlAnd, s
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if 
           
    strSql = "SELECT * FROM dbo.[vw_GetDocuments] Where 1=1 "
    sqlAnd = pBuildCriterionString()        

    strSql = strSql & sqlAnd 
	strSql = strSql & " ORDER BY [doc_file_desc_NL] "
	
    set oRs=Server.CreateObject("ADODB.Recordset")
        oRs.open strSql, objConn, 1, 3 
        s= "{rows:["
        with oRs
            if not .bof and not .eof then
            Dim doctext  , doclink      
            doctext = "doc_file_desc_"  & UserLang()
            
            .moveFirst 
                    do while not .eof
                        doclink = "<img src=/graphics/common/win_16x16/" & .fields("doc_img") & ">^" & .fields("doc_fullPath")
                        s = s & "{id:""" & .fields("doc_id") & """, data: [""0"", """ & .fields(doctext)  & """, """  & doclink  & """, """ & .fields("doc_fullPath")  & """, """ & .fields("doc_file_name")& """ ,  """ & .fields("doc_img") & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
            end if
        end with
		s = s & "]}"  
        pGetDocumentsList = s
    
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

end function

    Private function pBuildCriterionString()
    dim s
        s = s & castObjProperty("doc_root_id", C_EQUAL, request("doc_root_id"))
        s = s & castObjProperty("doc_nature_id", C_EQUAL, request("doc_nature_id"))
        s = s & castObjProperty("doc_type_id", C_EQUAL, request("doc_type_id"))
        s = s & castObjProperty("doc_lang_id", C_EQUAL, request("doc_lang_id"))
    pBuildCriterionString = s
            
    end function

    private function castObjProperty(fieldName, operator, value)
        Dim s : s= ""
        Dim sQ : sQ = ""
        if operator = C_LIKE then sQ="'" else sQ=""
        if len(trim(value)) > 0 then s = " and " & fieldname & " " & operator & " " & sQ & value & sQ
        castObjProperty = s
    end function

                      
       
'<END REGION JSON BUSINESS LOGIC>=============================================================

    public sub Class_Terminate()
        set objDocument = nothing   
    end sub


end class

%>