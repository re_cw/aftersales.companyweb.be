﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>

<%


Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<%
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objCex

set objCex = new sysCex

    select case request.querystring("a")
        case "JSONcex_history"
            objCex.JSONcex_history
		case "JSONcex_paymentExperience"
			objCex.JSONcex_paymentExperience
        case "JSONcex_chart"
            objCex.JSONcex_chart
    end select

class sysCex
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
	property get hbc_id()
		hbc_id = request("hbc_id")
		if len(trim(hbc_id)) = 0 then hbc_id = 0
	end property
	property get user_id()
		user_id = request.Cookies("TELESALESTOOLV2")("usrId")    
        if len(trim(user_id)) = 0 then user_id = -1
	end property
	public function JSONcex_paymentExperience()
		JSONcex_paymentExperience = pJSONcex_paymentExperience() : Response.ContentType = "application/json" : response.write JSONcex_paymentExperience
    end function
	public function JSONcex_history()
        JSONcex_history = pJSONcex_history() : Response.ContentType = "application/json" : response.write JSONcex_history
    end function
	
    public function JSONcex_chart()
        JSONcex_chart = pJSONcex_chart() : Response.ContentType = "application/json" : response.write JSONcex_chart
    end function
	
	private function pJSONcex_paymentExperience
	    	    Dim objConn, oRs, strSql

    strSql = "select Cex_Period, Cex_Score, ISNULL(cex_XMLhits,0) as cex_XMLhits, ISNULL(cex_AantalAlerts,0) as cex_AantalAlerts, ISNULL(cex_AantalInFollowupXML,0) as cex_AantalInFollowupXML, "
	strSql = strSql & " ISNULL(cex_mobilehits, 0) as cex_mobilehits, ISNULL(cex_RappHits,0) as cex_RappHits, ISNULL(cex_WebHits,0) as cex_WebHits, ISNULL(cex_FirstHits,0) as cex_FirstHits, cex_beterv "
    strSql = strSql & " from cex_history where CEX_HBC_ID = " & hbc_id()
    strSql = strSql & " and cex_minmonths = 0 "
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BackOfficeConnString() , adopenstatic, adcmdreadonly, adcmdtext
        
        oRs.open strSql, objConn
		s = RStoJSon(oRs)
		
		pJSONcex_paymentExperience = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing  
	end function

    private function pJSONcex_chart
    Dim objConn, oRs, strSql

    dim labelsstr
    dim datastr

    strSql = "select top 12 Cex_Period, Cex_Score, ISNULL(cex_XMLhits,0) as cex_XMLhits, ISNULL(cex_AantalAlerts,0) as cex_AantalAlerts, ISNULL(cex_AantalInFollowupXML,0) as cex_AantalInFollowupXML, "
	strSql = strSql & " ISNULL(cex_mobilehits, 0) as cex_mobilehits, ISNULL(cex_RappHits,0) as cex_RappHits, ISNULL(cex_WebHits,0) as cex_WebHits, ISNULL(cex_FirstHits,0) as cex_FirstHits "
    strSql = strSql & " from cex_history where CEX_HBC_ID = " & hbc_id()
    strSql = strSql & " and cex_period > 201406 order by Cex_Period DESC "
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BackOfficeConnString() , adopenstatic, adcmdreadonly, adcmdtext
		
        oRs.open strSql, objConn
		
        if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if
		
		pJSONcex_chart = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
    end function

    private function pJSONcex_history
    	    Dim objConn, oRs, strSql

    strSql = " select cex_score, cex_hbc_id, HBC_firmname, cex_maxAlerts, cex_AantalAlerts, "
    strSql = strSql & "cex_maxInfollowupXML, cex_AantalInFollowUpXML, cex_MobileActive, cex_MobileHits, "
    strSql = strSql & "cex_XMLAvailable, cex_XMLHits, cex_FirstAvailable, cex_FirstHits, "
    strSql = strSql & "cex_WebAvailable, cex_WebHits, cex_RappHits, cex_BetErv "
    strSql = strSql & " from cex_history "
	strSql = strSql & " inner join HB_Company on HBC_ID = cex_hbc_id "
    strSql = strSql & " where cex_period = year(dateadd(month, -1, GETDATE())) * 100 + MONTH(dateadd(month, -1, GETDATE())) "
    strSql = strsql & " and CEX_HBC_ID = " & hbc_id()
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BackOfficeConnString() , adopenstatic, adcmdreadonly, adcmdtext
        
        oRs.open strSql, objConn
		s = RStoJSon(oRs)
		
		pJSONcex_history = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
    end function
end class
%>