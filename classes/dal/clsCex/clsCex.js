﻿function GetCexHistoryPromise(hbc_id) {
    var url = "/classes/dal/clsCex/clsCex.asp";

    return $.get(url, { a: "JSONcex_history", hbc_id: hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting CEX history",
            expire: -1,
            type: "errormessage"
        });
    });
}
function GetCexExperiencePromise(hbc_id)
{
    var url = "/classes/dal/clsCex/clsCex.asp";
    return $.get(url, { a: "JSONcex_paymentExperience", hbc_id: hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting CEX Chart Experience",
            expire: -1,
            type: "errormessage"
        });
    });
	
}

function GetCexChartPromise(hbc_id)
{
    var url = "/classes/dal/clsCex/clsCex.asp";
    return $.get(url, { a: "JSONcex_chart", hbc_id: hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Failure getting CEX Chart Data",
            expire: -1,
            type: "errormessage"
        });
    });
}
function formatAbsoluteNumbers(data)
{
		switch(data.cex_beterv)
	{
		case "0":
			data.cex_XMLhits = parseFloat(data.cex_XMLhits);
			data.cex_AantalAlerts = parseFloat(data.cex_AantalAlerts);
			data.cex_AantalInFollowupXML = parseFloat(data.cex_AantalInFollowupXML);
			data.cex_mobilehits = parseFloat(data.cex_mobilehits);
			data.cex_RappHits = parseFloat(data.cex_RappHits);
			data.cex_WebHits = parseFloat(data.cex_WebHits);
			data.cex_FirstHits = parseFloat(data.cex_FirstHits);
			data.cex_beterv = parseFloat(data.cex_beterv);
			break;
		default:
			data.cex_XMLhits = parseFloat(data.cex_XMLhits);
			data.cex_AantalAlerts = parseFloat(data.cex_AantalAlerts);
			data.cex_AantalInFollowupXML = parseFloat(data.cex_AantalInFollowupXML);
			data.cex_mobilehits = parseFloat(data.cex_mobilehits);
			data.cex_RappHits = parseFloat(data.cex_RappHits);
			data.cex_WebHits = parseFloat(data.cex_WebHits);
			data.cex_FirstHits = parseFloat(data.cex_FirstHits);
			data.cex_beterv = data.cex_XMLhits + data.cex_AantalAlerts + data.cex_AantalInFollowupXML + data.cex_mobilehits + data.cex_RappHits + data.cex_WebHits + data.cex_FirstHits;
			break;
	}
	
	return data;
}
function formatJSONExperience(data)
{
	switch(data.cex_beterv)
	{
		case "0":
			data.cex_XMLhits = parseFloat(data.cex_XMLhits) *2;
			data.cex_AantalAlerts = parseFloat(data.cex_AantalAlerts)*2;
			data.cex_AantalInFollowupXML = parseFloat(data.cex_AantalInFollowupXML)*3;
			data.cex_mobilehits = parseFloat(data.cex_mobilehits);
			data.cex_RappHits = parseFloat(data.cex_RappHits)*5;
			data.cex_WebHits = parseFloat(data.cex_WebHits);
			data.cex_FirstHits = parseFloat(data.cex_FirstHits);
			data.cex_beterv = parseFloat(data.cex_beterv);
			break;
		default:
			data.cex_XMLhits = parseFloat(data.cex_XMLhits) *2;
			data.cex_AantalAlerts = parseFloat(data.cex_AantalAlerts)*2;
			data.cex_AantalInFollowupXML = parseFloat(data.cex_AantalInFollowupXML)*3;
			data.cex_mobilehits = parseFloat(data.cex_mobilehits);
			data.cex_RappHits = parseFloat(data.cex_RappHits)*5;
			data.cex_WebHits = parseFloat(data.cex_WebHits);
			data.cex_FirstHits = parseFloat(data.cex_FirstHits);
			data.cex_beterv = data.cex_XMLhits + data.cex_AantalAlerts + data.cex_AantalInFollowupXML + data.cex_mobilehits + data.cex_RappHits + data.cex_WebHits + data.cex_FirstHits;
			break;
	}
	
	data.cex_XMLhits = (data.cex_XMLhits / data.Cex_Score) * 100;
	data.cex_AantalAlerts =(data.cex_AantalAlerts / data.Cex_Score) * 100;
	data.cex_AantalInFollowupXML = (data.cex_AantalInFollowupXML / data.Cex_Score) * 100;
	data.cex_mobilehits = (data.cex_mobilehits / data.Cex_Score) * 100;
	data.cex_RappHits = (data.cex_RappHits / data.Cex_Score) * 100;
	data.cex_WebHits = (data.cex_WebHits / data.Cex_Score) * 100;
	data.cex_FirstHits = (data.cex_FirstHits / data.Cex_Score) * 100;
	
	data.cex_beterv = (data.cex_AantalAlerts / data.Cex_Score) * 100;
	
    return data;
}
function formatJSONChart(data)
{	
	for (var i = 0; i < data.length; i++)
	{
		data[i].cex_XMLhits = parseInt(data[i].cex_XMLhits)*2;
		
		data[i].cex_AantalAlerts = parseInt(data[i].cex_AantalAlerts)*2;
		
		data[i].cex_AantalInFollowupXML = parseInt(data[i].cex_AantalInFollowupXML)*3;
		
		data[i].cex_mobilehits = parseInt(data[i].cex_mobilehits)*2;
		
		data[i].cex_RappHits = parseInt(data[i].cex_RappHits)*5;
		data[i].cex_WebHits = parseInt(data[i].cex_WebHits);
		data[i].cex_FirstHits = parseInt(data[i].cex_FirstHits);
	}
	
    return data;
}
function GetCexAbsoluteScore(canvasId, data)
{
	if (!$.isEmptyObject(data)){
		var barChart = new dhtmlXChart({
			view: "bar",
			container: canvasId,
			value: "#cex_AantalAlerts#",
			color: "#8B0000",
			gradient: "rising",
			width: 60,
			tooltip: {"template": "Alerts"},
			xAxis: {
				"title": "Periode",
				"template": "#Cex_Period#"
			},
			yAxis: {
				start: -1,
			},
			legend:{
				values: [
					{text:"XMLhits", color: "#CCCC00"},
					{text:"Alerts", color: "#8B0000"},
					{text:"FollowupXML", color: "#a7ee70"},
					{text:"MobileHits", color: "#8A2BE2"},
					{text:"RappHits", color: "#808000"},
					{text:"WebHits", color: "#00FFFF"},
					{text:"FirstHits", color: "#FF00FF"}
				]
			},
			label: "#cex_AantalAlerts#"
		});
		
			barChart.addSeries({
			value: "#cex_AantalInFollowupXML#",
			color: "#a7ee70",
			tooltip: {
				"template": "FollowupXML"
			},
			label: "#cex_AantalInFollowupXML#"
		});
				barChart.addSeries({
			value: "#cex_mobilehits#",
			color: "#8A2BE2",
			tooltip: {
				"template": "MobileHits"
			},
			label: "#cex_mobilehits#"
		});
				barChart.addSeries({
			value: "#cex_XMLhits#",
			color: "#CCCC00",
			tooltip: {
				"template": "XMLhits"
			},
			label: "#cex_XMLhits#"
		});
				barChart.addSeries({
			value: "#cex_FirstHits#",
			color: "#FF00FF",
			tooltip: {
				"template": "FirstHits"
			},
			label: "#cex_FirstHits#"
		});
				barChart.addSeries({
			value: "#cex_WebHits#",
			color: "#00FFFF",
			tooltip: {
				"template": "WebHits"
			},
			label: "#cex_WebHits#"
		});
					barChart.addSeries({
			value: "#RappHits#",
			color: "#808000",
			tooltip: {
				"template": "RappHits"
			},
			label: "#RappHits#"
		});	
		barChart.parse(data, "json");
	}
}
function GetExperienceScore(canvasId, data)
{
	if (!$.isEmptyObject(data)){
		var barChart = new dhtmlXChart({
			view: "bar",
			container: canvasId,
			value: "#cex_AantalAlerts#",
			color: "#8B0000",
			gradient: "rising",
			width: 60,
			tooltip: {"template": "Alerts"},
			xAxis: {
				"title": "Periode",
				"template": "#Cex_Period#"
			},
			yAxis: {
				start: -1,
				stop: 4000
			},
			legend:{
				values: [
					{text:"XMLhits", color: "#CCCC00"},
					{text:"Alerts", color: "#8B0000"},
					{text:"FollowupXML", color: "#a7ee70"},
					{text:"MobileHits", color: "#8A2BE2"},
					{text:"RappHits", color: "#808000"},
					{text:"WebHits", color: "#00FFFF"},
					{text:"FirstHits", color: "#FF00FF"}
				]
			},
			label: "#cex_AantalAlerts#"
		});
		
			barChart.addSeries({
			value: "#cex_AantalInFollowupXML#",
			color: "#a7ee70",
			tooltip: {
				"template": "FollowupXML"
			},
			label: "#cex_AantalInFollowupXML#"
		});
				barChart.addSeries({
			value: "#cex_mobilehits#",
			color: "#8A2BE2",
			tooltip: {
				"template": "MobileHits"
			},
			label: "#cex_mobilehits#"
		});
				barChart.addSeries({
			value: "#cex_XMLhits#",
			color: "#CCCC00",
			tooltip: {
				"template": "XMLhits"
			},
			label: "#cex_XMLhits#"

		});
				barChart.addSeries({
			value: "#cex_FirstHits#",
			color: "#FF00FF",
			tooltip: {
				"template": "FirstHits"
			},
			label: "#cex_FirstHits#"
		});
				barChart.addSeries({
			value: "#cex_WebHits#",
			color: "#00FFFF",
			tooltip: {
				"template": "WebHits"
			},
			label: "#cex_WebHits#"
		});
					barChart.addSeries({
			value: "#RappHits#",
			color: "#808000",
			tooltip: {
				"template": "RappHits"
			},
			label: "#RappHits#"
		});	
		barChart.parse(data, "json");
	}
}

function GetChart(canvasId,data)
{
	if (!$.isEmptyObject(data)){
		var barChart = new dhtmlXChart({
			view:"stackedBar",
			container: canvasId,
			value: "#cex_XMLhits#",
			color: "#CCCC00",
			label: "#cex_XMLhits#",
			width: 60,
			xAxis: {
				"title": "Periode",
				"template": "#Cex_Period#"
			},
			yAxis: {
				title: "Score",
				start: -1,
			},
			legend:{
				values: [
					{text:"XMLhits", color: "#CCCC00"},
					{text:"Alerts", color: "#8B0000"},
					{text:"FollowupXML", color: "#a7ee70"},
					{text:"MobileHits", color: "#8A2BE2"},
					{text:"RappHits", color: "#808000"},
					{text:"WebHits", color: "#00FFFF"},
					{text:"FirstHits", color: "#FF00FF"}
				]
			}, tooltip:{
				"template": "XMLhits"
			},
			gradient:"rising"
		});
		barChart.addSeries({
			value: "#cex_AantalAlerts#",
			color: "#8B0000",
			tooltip: {
				"template": "Alerts"
			},
			label: "#cex_AantalAlerts#"
		});
			barChart.addSeries({
			value: "#cex_AantalInFollowupXML#",
			color: "#a7ee70",
			tooltip: {
				"template": "FollowupXML"
			},
			label: "#cex_AantalInFollowupXML#"
		});
			barChart.addSeries({
			value: "#cex_mobilehits#",
			color: "#8A2BE2",
			tooltip: {
				"template": "MobileHits"
			},
			label: "#cex_mobilehits#"
		});
			barChart.addSeries({
			value: "#cex_RappHits#",
			color: "#808000",
			tooltip: {
				"template": "RappHits"
			},
			label: "#cex_RappHits#"

		});
			barChart.addSeries({
			value: "#cex_WebHits#",
			color: "#00FFFF",
			tooltip: {
				"template": "WebHits"
			},
			label: "#cex_WebHits#"
		});
				barChart.addSeries({
			value: "#cex_FirstHits#",
			color: "#FF00FF",
			tooltip: {
				"template": "FirstHits"
			},
			label: "#cex_FirstHits#"
		});
		
		barChart.parse(data, "json");
	}
}
function FormatCexHistory(cexHistory) {
    var t = '<table>';
	
    if (!$.isEmptyObject(cexHistory)) {
		t += "<tr >";
		t += "<td width = 100><b>Firma </b></td>";
		t += "<td align = 'right'><b>Score  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Max Alerts  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b># Alerts  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Max fol-up XML  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b># fol-up XML  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Mob Act  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Mob hits  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>XML av.  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>XML Hits  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>First av.  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>First Hits  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Web av.  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Web Hits  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Rapp Hits  </b></td>";
		t += "<td width = '7'></td>";
		t += "<td align = 'right'><b>Bet Erv  </b></td>";
		t += "<td width = '7'></td>";
		t += "</tr>";
		t += "<tr><td colspan = 40><hr></td></tr>";

		var naam = cexHistory.HBC_firmname;
		
		if (naam.length > 16) {
			naam = naam.substr(0, 15) & "...";
		}
		t += "<tr><td>" + naam + "</td>";
		t += "<td align = 'right'>" + cexHistory.cex_score + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_maxAlerts + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_AantalAlerts + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_maxInfollowupXML + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_AantalInFollowUpXML + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_MobileActive + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_MobileHits + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_XMLAvailable + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_XMLHits + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_FirstAvailable + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_FirstHits + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_WebAvailable + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_WebHits + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_RappHits + "</td>";
		t += "<td align = 'right' colspan = 2>" + cexHistory.cex_BetErv + "</td>";
		t += "</tr>";
	}
    
    t += '</table>';
    t += '<div id="cexChart" style="width: 100%; height: 400px"></div>';
	t += '<div id="cexExperience" style="width: 100%; height: 400px"></div>';
	t += '<div id="cexAbsolute" style="width: 100%; height: 400px"></div>';
    return t;
}