﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->

<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "EN"
CONST C_DEFAULT_EXT = 312

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objCalls
    Set objCalls = new sysCalls
    
    select case request.querystring("q")
        case 1 'individual-today's calltime refreshed on jscript time elapsed 
            objCalls.JSONGetCallDetails()
        case 2 'month view
        case else 'default is individual today's
            objCalls.JSONGetCallDetails()
    end select
    
    set objCalls = nothing


class sysCalls
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property

    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    'for testing purpose only
    if len(trim(usr_id))= 0 then usr_id = 1
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property  

    property get extension()
        extension = request.Cookies("TELESALESTOOLV2")("extension")    
        if len(trim(extension)) = 0 then extension = C_DEFAULT_EXT
    end property

    property get selectedMonth()
        selectedMonth = request.querystring("mm")
        if len(trim(selectedMonth)) = 0 then selectedMonth = month(Date())
    end property
    
    property get selectedYear()
        selectedYear = request.querystring("yy")
        if len(trim(selectedYear)) = 0 then selectedYear = year(Date())    
    end property

    property get selectedDate()
        selectedDate = request.querystring("yymmdd")
        
        if len(trim(selectedDate)) = 0 then selectedDate = currentDate()   
    end property

    property get currentDate()
        dim mm, dd
        if len(month(date())) = 1 then mm = "0" & month(date()) else mm = month(date())
        if len(day(date())) = 1 then dd = "0" & day(date()) else dd = day(date())
        currentDate= 20160118 'year(Date()) & mm & dd            
    end property

    property get salesTool()
        salesTool = request.querystring("st")
        if len(trim(salesTool)) = 0 then salesTool = "on"  
    end property

    property get beginDate()
        if len(trim(request("beginDate"))) > 0 then beginDate = castDate(request("beginDate"))
    end property

    property get endDate()
        if len(trim(request("endDate"))) > 0 then beginDate = castDate(request("endDate"))
    end property

    private function castDate(byval varDate)
        dim mm, dd
        if len(month(varDate)) = 1 then mm = "0" & month(varDate) else mm = month(varDate)
        if len(day(varDate)) = 1 then dd = "0" & day(varDate) else dd = day(varDate)
        castDate= year(varDate) & mm & dd
    end function


'<start region PUBLIC METHODS & FUNCTIONS>
    public function JSONGetCallDetails() 
        Response.ContentType = "application/json" : response.write pJSONGetCallDetails()
    end function
'<end region PUBLIC METHODS & FUNCTIONS>


'<start region PRIVATE METHODS & FUNCTIONS>

private function pJSONGetCallDetails()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    strSql = "dbo.GetCallDetailRecords"
    dim a, b, c, d, e
    
    
    a = salesTool() : b = extension() : c = selectedDate() : d = beginDate() : e = endDate()
        
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with

    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@salestool", 8, 1, len(a), a)       
            .Parameters.Append .CreateParameter("@src", 8, 1, len(b), b)      
            .Parameters.Append .CreateParameter("@currentDate", 8, 1, 8, c)      
            .Parameters.Append .CreateParameter("@beginDate", 8, 1, 8, d) 
            .Parameters.Append .CreateParameter("@endDate", 8, 1, 8, e)
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysCalls.pJSONGetCallDetails()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with       
		 s= "{""calldetail"" :["
                with objRS                       
					if not oRs.bof and not oRs.eof then
						s = s & RStoJSON(oRs)
					end if                         
                end with
         s = s & "]}" 
        pJSONGetCallDetails = s
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing
    end if
end function

'<end region PRIVATE METHODS & FUNCTIONS>

end class
%>