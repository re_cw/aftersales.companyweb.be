﻿function JSON_WS_GetCallDetails() {
		$("#UserDailyCallTime").empty();
		
		var ext = getValuefromCookie("TELESALESTOOLV2", "extension")

		var url = "http://boservices.companyweb.be/jsonCalltimeToday.asp"

		$.get(url, { callerid:ext})
		.done(function(data){
			var objCall = data;
			var html = "Date: " + objCall.calldetail[0].selectedDate + "<br>Call Time: " + objCall.calldetail[0].calltime + "<br>Calls: " + objCall.calldetail[0].numbcall;
			$("#UserDailyCallTime").html(html);
		}).fail(function(){
			dhtmlx.message({
				text: "Error getting calldetails.",
				expire: -1,
				type: "errormessage"
			});
			$("#UserDailyCallTime").html("N/A");
		}).always(function(){
			$("#UserDailyCallTime").data('requestRunning',false);
		});
}