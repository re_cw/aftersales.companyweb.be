﻿<!-- #include virtual ="/classes/dal/connector.inc" -->

<% 
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1   

Dim objLog
    Set objLog = new sysLogs
    
    select case request.querystring("q")
        case 1 'AddNew sysLog
            objLog.populateSysLogObject()
            objLog.AddSysLog()
        case 2 'read sysLog GetJSONLeadsSysLog
            objLog.GetJSONLeadsSysLog()    
        case 3 'read GetJSONEventsSysLog
            objLog.GetJSONEventsSysLog()
        case 4 'read sysLog GetXMLLeadsSysLog
            objLog.GetXMLLeadsSysLog()    
        case 5 'read sysLog GetXMLEventsSysLog
            objLog.GetXMLEventsSysLog()
        case else 'no default
            
    end select
    
    set objLog = nothing


class sysLogs

Dim m_syslog_id, m_syslog_user_id, m_syslog_event_type_id, m_syslog_lead_id, m_syslog_event_id
Dim m_syslog_log, m_syslog_date, m_syslog_time, m_syslog_GetLeadsLogs     
        

    property get syslog_id() : syslog_id = m_syslog_id : end property 
    property let syslog_id(varval) : m_syslog_id = varval : end property

    property get syslog_user_id() : syslog_user_id = m_syslog_user_id : end property 
    property let syslog_user_id(varval) : m_syslog_user_id = varval : end property

    property get syslog_event_type_id() : syslog_event_type_id = m_syslog_event_type_id : end property 
    property let syslog_event_type_id(varval) : m_syslog_event_type_id = varval : end property

    property get syslog_lead_id() : syslog_lead_id = m_syslog_lead_id : end property 
    property let syslog_lead_id(varval) : m_syslog_lead_id = varval : end property

    property get syslog_event_id() : syslog_event_id = m_syslog_event_id : end property 
    property let syslog_event_id(varval) : m_syslog_event_id = varval : end property

    property get syslog_log() : syslog_log = m_syslog_log : end property 
    property let syslog_log(varval) : m_syslog_log = varval : end property

    property get syslog_date() : syslog_date = m_syslog_date : end property 
    property let syslog_date(varval) : m_syslog_date = varval : end property

    property get syslog_time() : syslog_time = m_syslog_time : end property 
    property let syslog_time(varval) : m_syslog_time = varval : end property

    property get syslog_GetLeadsLogs() : syslog_GetLeadsLogs = m_syslog_GetLeadsLogs : end property 
    property let syslog_GetLeadsLogs(varval) : m_syslog_GetLeadsLogs = varval : end property

    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property

    property get SalesToolConnString()
        SalesToolConnString = sConnStrDev
    end property
    
    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    if len(trim(usr_id))= 0 then usr_id = 1
    end property
    
    private function castDate(byval varDate)
        dim mm, dd
        if len(month(varDate)) = 1 then mm = "0" & month(varDate) else mm = month(varDate)
        if len(day(varDate)) = 1 then dd = "0" & day(varDate) else dd = day(varDate)
        castDate= year(varDate) & mm & dd
    end function    



'<start region PUBLIC METHODS & FUNCTIONS>
public function populateSysLogObject()
    syslog_user_id=usr_id()
    syslog_event_type_id= 5
    syslog_lead_id = 377
    syslog_event_id= 0
    syslog_log = "New log insertion into database for class testing purpose"
end function



public function GetJSONLeadsSysLog()
    syslog_GetLeadsLogs = 1
    Response.ContentType = "application/json" : response.write pJSONGetSysLog()
end function

public function GetJSONEventsSysLog()
    syslog_GetLeadsLogs = 0
    Response.ContentType = "application/json" : response.write pJSONGetSysLog()
end function

public function GetXMLLeadsSysLog()
    syslog_GetLeadsLogs = 1    
    Response.ContentType = "application/xml": response.write pXMLGetSysLog()
end function

public function GetXMLEventsSysLog()
    syslog_GetLeadsLogs = 0    
    Response.ContentType = "application/xml": response.write pXMLGetSysLog()
end function

public function AddSysLog()
    AddSysLog = pAddSysLog()
end function

'<end region PUBLIC METHODS & FUNCTIONS>


'<start region PRIVATE METHODS & FUNCTIONS>

private function pJSONGetSysLog()

    Dim viewname
    if syslog_GetLeadsLogs = 1 then 
        viewname = "[dbo].[vw_GetLeadsLogs]"
    else
        viewname = "[dbo].[vw_GetEventsLogs]"
    end if

    Dim objConn, objRS, strSql, sErrMsg, s

    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        
    if objConn.State <> 1 then exit function

    strsql = "SELECT * FROM " & viewname
    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysLogs.pGetSysLog()" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            'response.write sErrMsg
            exit function
        end if

    with objRs
        if not .BOF and not .EOF then
            s= "{""SysLog"" :["
            do while not .eof
                s=s & "{"
                    s=s & " ""syslog_id"":""" & .fields("syslog_id") & """"
                    s=s & " ,""syslog_date"":""" & .fields("syslog_date") & """"
                    s=s & " ,""syslog_time"":""" & .fields("syslog_time") & """"
                    s=s & " ,""firstname"":""" & .fields("firstname") & """"
                    s=s & " ,""lastname"":""" & .fields("lastname") & """"
                    s=s & " ,""company_name"":""" & .fields("company_name") & """"
                    s=s & " ,""company_vat"":""" & .fields("company_vat") & """"                        
                    s=s & " ,""event_type_name_EN"":""" & .fields("event_type_name_EN") & """"
                    s=s & " ,""syslog_log"":""" & .fields("syslog_log") & """"
                    s=s & " },"
            .movenext
            loop
            s = left(s,len(s)-1)
            s = s & "]}"
        end if
    end with
    pJSONGetSysLog = s
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing


end function


private function pXMLGetSysLog()

    Dim viewname
    if syslog_GetLeadsLogs = 1 then 
        viewname = "[dbo].[vw_GetLeadsLogs]"
    else
        viewname = "[dbo].[vw_GetEventsLogs]"
    end if

    Dim objConn, objRS, strSql, sErrMsg, s

    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        
    if objConn.State <> 1 then exit function

    strsql = "SELECT * FROM " & viewname
    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysLogs.pGetSysLog()" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            'response.write sErrMsg
            exit function
        end if
    
    with objRs
        if not .BOF and not .EOF then
            s= "<?xml version=""1.0"" encoding=""utf-8"" ?><syslog>"
                do while not .eof                
                    s=s & "<syslog_id=""" & .fields("syslog_id") & """"
                    s=s & " syslog_date=""" & .fields("syslog_date") & """"
                    s=s & " syslog_time=""" & .fields("syslog_time") & """"
                    s=s & " firstname=""" & .fields("firstname") & """"
                    s=s & " lastname=""" & .fields("lastname") & """"
                    s=s & " company_name=""" & .fields("company_name") & """"
                    s=s & " company_vat=""" & .fields("company_vat") & """"
                    s=s & " event_type_name_EN=""" & .fields("event_type_name_EN") & """"
                    s=s & " syslog_log=""" & .fields("syslog_log") & """"
                    s=s & " />"
                .movenext
                loop   
        end if
   End With
            s = s & "</syslog>"

    pXMLGetSysLog = s
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing


end function


private function pAddSysLog()
    
    Dim objConn, objRS, strSql, sErrMsg

    strSql = "SELECT TOP 1 * FROM [dbo].[app_sys_logs]"    
    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        
    if objConn.State <> 1 then exit function    
  
    set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strsql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysLogs.pAddSysLog()" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            'response.write sErrMsg
            exit function
        end if
    
        with objRS
            .addNew
                .fields("syslog_user_id")= syslog_user_id()
                .fields("syslog_event_type_id")= syslog_event_type_id()
                .fields("syslog_lead_id")= syslog_lead_id()
                .fields("syslog_event_id")= syslog_event_id()
                .fields("syslog_log")= syslog_log()
                .fields("syslog_date")= SetDate()
                .fields("syslog_time")= SetTime()
                .fields("AuditDateCreated")=now()
                .fields("AuditUserCreated")= syslog_user_id()
            .update
        end with
        
        objRs.Close : set objRS = nothing
        objConn.close : set objConn = nothing  

        pAddSysLog= sErrMsg    
end function



    private function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    private function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function


'<end region PRIVATE METHODS & FUNCTIONS>
public sub Class_Terminate()
        
end sub

end class
%>