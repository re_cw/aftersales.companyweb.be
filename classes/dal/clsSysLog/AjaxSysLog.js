﻿function GetLeadLogs(controlname) {

    GetJSONLeadsSysLog(controlname);

}
function GetJSONLeadsSysLog(controlname) {
    var xmlhttp = new XMLHttpRequest();
    var url = "/classes/dal/clsSysLog/clsSysLog.asp?q=2";

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (typeof (xmlhttp.responseText) !== "undefined" && (xmlhttp.responseText) != null && (xmlhttp.responseText) != "") {
                var objsyslog = JSON.parse(xmlhttp.responseText);
                
                document.getElementById(controlname).innerHTML = SetSysLogToTable(objsyslog);
            }
            ;
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function SetSysLogToTable(objsyslog) {
    
    var t = "<table cellpadding=0 border=0 width=800px><tr><td colspan=4 class=CwCylblMandates>Leads System Events</td></tr>";
    t += "<tr class=LeadDetails><td>when</td><td>who</td><td>company</td><td>event type</td></tr>"
        for (i = 0; i < objsyslog.SysLog.length; i++) {
            t += "<tr class=syslogtext><td>" + objsyslog.SysLog[i].syslog_date + " " + objsyslog.SysLog[i].syslog_time + "</td>";
        t += "<td>" + objsyslog.SysLog[i].firstname + " " + objsyslog.SysLog[i].lastname + "</td>";
        t += "<td>" + objsyslog.SysLog[i].company_name + " " + objsyslog.SysLog[i].company_vat + "</td>";
        t += "<td>" + objsyslog.SysLog[i].event_type_name_EN + "</td><tr>";
        
        t += "<tr class=syslogtext><td colspan=4>" + objsyslog.SysLog[i].syslog_log + "</td></tr>";
    }

    t += "</table>";
    
    return t;

}