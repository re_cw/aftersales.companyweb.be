﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objLead.asp" -->
<%
CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objLead, objsysLeadQ
    
    set objsysLeadQ = new sysLeadsQueue
    set objLead = new clsObjLead

    select case request.querystring("q")
        case 1 
            objsysLeadQ.JSONGetLeadsQueue()
		case 2
			objLead.populateLeadObject()
			response.write objsysLeadQ.RemoveEntry(objLead)
        
        case else 'default is individual today's
            'objLeads.JSONGetLeads()
    end select
    
    



class sysLeadsQueue
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property

    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property

	public function RemoveEntry(objLead)
		RemoveEntry = pRemoveEntry(objLead)
	End function 

    public function JSONGetLeadsQueue()
        Response.ContentType = "application/json" : response.write pJSONGetLeadsQueue()
    end function

	Private function pRemoveEntry(obj)
	if len(trim(obj.lq_id)) = 0 Then 
		pRemoveEntry = -1
		exit function
	End if
	
	Dim objConn, objRs, strSql
	Dim blnSkip : blnSkip = false
	
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 0 then 
		pRemoveEntry = -2 
		exit function
	End if
	
	objConn.BeginTrans
	
	strSql = "SELECT TOP 1 * FROM [salestool].[dbo].[leads_queue] WHERE LQ_Id=" & obj.lq_id & "AND user_id = " & obj.user_id
	set objRs = Server.CreateObject("ADODB.Recordset")
	objRS.open strSql, objConn, 1, 3
	with objRs
		if .bof and .eof then blnSkip=true 'not found -- or no permission!
		
		if blnSkip=False then
			.fields("status_id") = 8
			'.fields("AuditUserUpdated") = obj.user_id
            '.fields("AuditDateUpdated") = now()
		End if
		.update
		.close
	end with
	'end process commit or rollback
       if Err.number = 0 then
           objConn.CommitTrans
           pRemoveEntry = 1
       else
           objConn.RollbackTrans
           pRemoveEntry = sErrMsg
       end if
	
	set objConn = nothing
	
	End function

    private function pJSONGetLeadsQueue()

        Dim objConn, objRS, strSql, sErrMsg
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
    
        if objConn.State = 0 then 
            set objConn = nothing
            exit function
        end if

        'strSql = "SELECT [LQ_id],[status_id],[user_id],[lang_id],[company_vat],[backoffice_id],[lead_type_id],[lead_Firma],[lead_Contact],[lead_Tel] "
        'strSql = strSql & " ,[lead_Email],[AuditDateCreated],[AuditUserCreated],[company_id],[jfc_code],[company_nace] "
        'strSql = strSql & " FROM [salestool].[dbo].[leads_queue] "
        strSql = "select * from [dbo].[vw_GetDirtyLeads] "
        strSql = strSql & " where [user_id]=" & usr_id() & " AND status_id=0"
        strSql = strSql & " order by lead_type_id"


        set objRS=Server.CreateObject("ADODB.Recordset")
        objRS.open strSql, objConn, 1, 3 

        with objRS
		                 s= "{""rows"":["
            if not .bof and not .eof then
            
            .moveFirst 
                    do while not .eof
                        'doclink = "<img src=/graphics/common/win_16x16/" & .fields("doc_img") & ">^" & .fields("doc_fullPath")
                        s = s & "{""id"":""" & .fields("LQ_id") & """, ""data"": [""" & .fields("LQ_id") & """,""" & .fields("lead_Firma")  & """, """ & .fields("lead_Contact")  & """, """ & .fields("company_vat")  & """, """  & .fields("lead_Tel")  & """, """  & .fields("calleenumber")  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
             
            end if
			s = s & "]}" 
        end with


        pJSONGetLeadsQueue = s

        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing


    end function
'<END REGION JSON BUSINESS LOGIC>=============================================================

    public sub Class_Terminate()
        set objsysLeadQ = nothing
        set objLead = nothing
    end sub

end class

    
%>


