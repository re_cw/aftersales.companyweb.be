﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/obj/objComment.asp" -->
<!-- #include virtual ="/classes/obj/objEmail.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"
CONST C_TRANSFER_LEAD = "SYSTEM: TRANSFER LEAD"

Dim objEvent
   
    set objEvent = new sysEvents

        select case request.QueryString("a")
			case "CanProceed"
				objEvent.CanProceed
			case "XML_GetCurrentOpenTasksForId"
				objEvent.XML_GetCurrentOpenTasksForId
			case "XML_GetCurrentOpenTasks"
				objEvent.XML_GetCurrentOpenTasks
            case "XML_GetEvents" 'read
                objEvent.XML_GetEvents
			case "XML_GetEventsNF"
				objEvent.XML_GetEventsNF
            case "XML_GetEventsFiltered" 'read          
                objEvent.XML_GetEvents
			case "GetFutureEvents"
				objEvent.GetFutureEvents
            case "GetEventHistory" 'read
                objEvent.GetEventHistory
            case "GetEventHistoryFromHbcId"
                objEvent.GetEventHistoryFromHbcId
			case "GetEventHistoryFromEvent" 'read
                objEvent.GetEventHistoryFromEvent
			case "GetCompanyHistory"
				objEvent.GetCompanyHistory
            case "XML_SaveEvents" 'save/update/delete 
                response.write objEvent.XML_SaveEvents    
            
            case "XML_GetEventTypes"
                objEvent.XML_GetEventTypes
            
            case "GetComboEventTypesWithImg"
                objEvent.GetComboEventTypesWithImg
			case "GetComboEventTypesWithImgForFunction"
				objEvent.GetComboEventTypesWithImgForFunction
            case "GetComboEventTypes"
                objEvent.GetComboEventTypes
			case "GetComboEventTypesForFunction"
                objEvent.GetComboEventTypesForFunction
            case "GetComboEventTypesKeyVal"
                objEvent.GetComboEventTypesKeyVal
			case "GetComboEventTypesKeyValForFunction"
                objEvent.GetComboEventTypesKeyValForFunction
            case "GetComboEventTypesOptionVal"
                objEvent.GetComboEventTypesOptionVal
			case "GetComboEventTypesOptionValForFunction"
                objEvent.GetComboEventTypesOptionValForFunction
            case "JSON_GetEvents" 'read
                objEvent.JSON_GetEvents
            case "JSON_GetEventTypes"
				objEvent.JSON_GetEventTypes
			case "JSON_GetEventTypesForFunction"
				objEvent.JSON_GetEventTypesForFunction
			case "JSON_GetEventsCountForToday"
				objEvent.JSON_GetEventsCountForToday
			case "JSON_GetEventsCountForTomorrow"
				objEvent.JSON_GetEventsCountForTomorrow
			case "JSON_GetEventsCountForThisWeek"
				objEvent.JSON_GetEventsCountForThisWeek
        case else 'default should be read
    end select

    set objEvent = nothing

class sysEvents

    Dim m_lang_radical
    Dim m_usr_id
    Dim m_usr_role_id
    Dim m_userLang
	Dim m_event_type_ids

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
	property get event_types()
		event_types = request("event_types")
	end property
	
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    property get function_id()
        function_id=request.Cookies("TELESALESTOOLV2")("hrfunctionid")
		if len(trim(function_id)) = 0 then function_id = 5
    end property
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property
	property get company_id()
		company_id=request("company_id")
	end property

    property get event_type_id()
        event_type_id=request("event_type_id")
        if len(trim(event_type_id)) = 0 then event_type_id = 0
    end property

    property get useColors()
        useColors=request("b")
        if len(trim(useColors)) = 0 then useColors = 1
    end property
    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property
    property get hbc_id()
        hbc_id = request("hbc_id")
        if (len(hbc_id)) = 0 then hbc_id = -1
    end property
    sub GetUserLang() 'not used ?
        pGetUserLang()
    end sub
    Sub GetUserLevel()
        pGetUserLevel()
    end sub
    
	private function pEncode(str)
		if (VarType(str) = 8) then
			pEncode = Server.HTMLEncode(str)
		Else
			pEncode = str
		end if
	end function
	public function CanProceed()
		CanProceed = pCanProceed()
		response.write CanProceed
	end function
	public function JSON_GetEventsCountForToday
		JSON_GetEventsCountForToday = pJSON_GetEventsCountForToday() : Response.ContentType = "application/json": response.write JSON_GetEventsCountForToday
	end function
	public function JSON_GetEventsCountForThisWeek
		JSON_GetEventsCountForThisWeek = pJSON_GetEventsCountForThisWeek() : Response.ContentType = "application/json": response.write JSON_GetEventsCountForThisWeek
	end function
	public function JSON_GetEventsCountForTomorrow
		JSON_GetEventsCountForTomorrow = pJSON_GetEventsCountForTomorrow() : Response.ContentType = "application/json": response.write JSON_GetEventsCountForTomorrow
	end function
    public function GetEventHistoryFromHbcId
        GetEventHistoryFromHbcId = pGetEventHistoryFromHbcId()
        response.write GetEventHistoryFromHbcId
    end function
	public function XML_GetCurrentOpenTasksForId()
		XML_GetCurrentOpenTasksForId = pGetCurrentOpenTasksForEventId()
		Response.ContentType = "application/xml"
		response.write XML_GetCurrentOpenTasksForId
	end function
	public function XML_GetCurrentOpenTasks()
		XML_GetCurrentOpenTasks = pGetCurrentOpenTasks()
		Response.ContentType = "application/xml"
		response.write XML_GetCurrentOpenTasks
	end function
	public function GetFutureEvents()
		GetFutureEvents=pGetFutureEvents(): Response.Write GetFutureEvents
	end function
    public function XML_GetEvents()
        XML_GetEvents=pGetEvents():Response.ContentType = "application/xml":  response.write XML_GetEvents
    end function
    public function XML_GetEventsNF()
        XML_GetEventsNF=pGetEventsNF():Response.ContentType = "application/xml":  response.write XML_GetEventsNF
    end function
    public function GetEventHistory()
        'GetEventHistory = pGetEventHistory2() : response.write GetEventHistory 
    GetEventHistory = pGetEventHistory() : response.write GetEventHistory 
    end function
	public function GetEventHistoryFromEvent()
		GetEventHistoryFromEvent = pGetEventHistoryFromEvent() 
		response.Write GetEventHistoryFromEvent
	end function
    
	public function GetCompanyHistory()
		GetCompanyHistory = pGetCompanyHistory() : Response.ContentType = "application/json": response.write GetCompanyHistory
	End function

    public function JSON_GetEvents()
        JSON_GetEvents=pJSON_GetEvents(): Response.ContentType = "application/json": response.write JSON_GetEvents
    end function

     public function JSON_GetEventTypes()
        JSON_GetEventTypes=pJSON_GetEventTypes(): Response.ContentType = "application/json": response.write JSON_GetEventTypes
    end function
	public function JSON_GetEventTypesForFunction()
		JSON_GetEventTypesForFunction = pJSON_GetEventTypesForFunction() : Response.ContentType = "application/json" : response.write JSON_GetEventTypesForFunction
	end function


    public function XML_GetEventTypes()
        XML_GetEventTypes=pXML_GetEventTypes(): Response.ContentType = "application/xml": response.write XML_GetEventTypes
    end function

    public function GetComboEventTypes()
        GetComboEventTypes = pGetComboEventTypes(): Response.ContentType = "application/xml": response.write GetComboEventTypes
    end function
	public function GetComboEventTypesForFunction()
		GetComboEventTypesForFunction = pGetComboEventTypesForFunction() : Response.ContentType = "application/xml" : response.write GetComboEventTypesForFunction()
	end function
    public function GetComboEventTypesWithImg()
    GetComboEventTypesWithImg = pGetComboEventTypesWithImg() 
	response.write GetComboEventTypesWithImg
    end function

	public function GetComboEventTypesWithImgForFunction()
		GetComboEventTypesWithImgForFunction = pGetComboEventTypesWithImgForFunction() 
		response.write GetComboEventTypesWithImgForFunction
	end function
    public function GetComboEventTypesKeyVal()  
     GetComboEventTypesKeyVal = pGetComboEventTypesKeyVal() :Response.ContentType = "application/json":  response.write GetComboEventTypesKeyVal
    end function
    public function GetComboEventTypesKeyValForFunction()
		GetComboEventTypesKeyValForFunction = pGetComboEventTypesKeyValForFunction() : Response.ContentType = "application/json" : response.write GetComboEventTypesKeyValForFunction
	end function
    public function GetComboEventTypesOptionVal()
        GetComboEventTypesOptionVal = pGetComboEventTypesOptionVal() : response.write GetComboEventTypesOptionVal
    end function
	
	public function GetComboEventTypesOptionValForFunction()
        GetComboEventTypesOptionValForFunction = pGetComboEventTypesOptionValForFunction() : response.write GetComboEventTypesOptionValForFunction
    end function
	
    public function XML_SaveEvents()
        Response.ContentType = "application/xml" : XML_SaveEvents = pXML_SaveEvents()
    end function


    private sub pGetUserLang()
        lang_radical = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(lang_radical)) = 0 then lang_radical = C_DEFAULT_LANG
    end sub

    private sub pGetUserLevel()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end sub


'<START REGION XML BUSINESS LOGIC>===========================================================
private function pJSON_GetEventsCountForThisWeek
	Dim objConn, oRs, strSql, s
	
	strSql = " SELECT MAX(ET.event_type_name_" & userLang() & ") as [event_type_name], COUNT(E.event_type_id) as [events] "
	strSql = strSql & " FROM [salestool].[dbo].[events] AS E "
	strSql = strSql & " join [salestool].[dbo].[event_types] AS ET "
	strSql = strSql & " on E.event_Type_id = ET.event_type_id "
	strSql = strSql & " where (convert(date, start_date) between convert(date,dateadd(day,1,getdate())) AND convert(date, dateadd(day,7,getdate()))) AND E.usr_id = " & usr_id()
	strSql = strSql & " GROUP BY E.event_type_id "
	
	set objConn=Server.CreateObject("ADODB.Connection")
	set oRs=Server.CreateObject("ADODB.Recordset") 
		objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
		oRs.open strSql, objConn
	
	s = "[" & RStoJSON(oRs) & "]"
	
	pJSON_GetEventsCountForThisWeek = s '(server.HTMLEncode(s)
	oRs.close : set oRs = nothing
	objConn.close : set objConn = nothing
end function
private function pJSON_GetEventsCountForTomorrow
	Dim objConn, oRs, strSql, s
	
	strSql = " SELECT MAX(ET.event_type_name_" & userLang() & ") as [event_type_name], COUNT(E.event_type_id) as [events] "
	strSql = strSql & " FROM [salestool].[dbo].[events] AS E "
	strSql = strSql & " join [salestool].[dbo].[event_types] AS ET "
	strSql = strSql & " on E.event_Type_id = ET.event_type_id "
	strSql = strSql & " where convert(date, start_date) = convert(date, dateadd(day,1,getdate())) AND E.usr_id = " & usr_id()
	strSql = strSql & " GROUP BY E.event_type_id "
	
	set objConn=Server.CreateObject("ADODB.Connection")
	set oRs=Server.CreateObject("ADODB.Recordset") 
		objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
		oRs.open strSql, objConn
	
	s = "[" & RStoJSON(oRs) & "]"
	
	pJSON_GetEventsCountForTomorrow = s '(server.HTMLEncode(s)
	oRs.close : set oRs = nothing
	objConn.close : set objConn = nothing
end function
private function pJSON_GetEventsCountForToday
	Dim objConn, oRs, strSql, s
	
	strSql = " SELECT MAX(ET.event_type_name_" & userLang() & ") as [event_type_name], COUNT(E.event_type_id) as [events] "
	strSql = strSql & " FROM [salestool].[dbo].[events] AS E "
	strSql = strSql & " join [salestool].[dbo].[event_types] AS ET "
	strSql = strSql & " on E.event_Type_id = ET.event_type_id "
	strSql = strSql & " where convert(date, start_date) =convert(date, getdate()) AND E.usr_id = " & usr_id()
	strSql = strSql & " GROUP BY E.event_type_id "
	
	set objConn=Server.CreateObject("ADODB.Connection")
	set oRs=Server.CreateObject("ADODB.Recordset") 
		objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
		oRs.open strSql, objConn
	
	s = "[" & RStoJSON(oRs) & "]"
	
	pJSON_GetEventsCountForToday = s '(server.HTMLEncode(s)
	oRs.close : set oRs = nothing
	objConn.close : set objConn = nothing

end function
 private function pGetEventHistoryFromHbcId
    Dim debugMsg, event_type_name
    Dim objConn, objRS, strSql, sErrMsg

    strSql = " SELECT comment_date, comment_time, comment, event_type_name,firstname,lastname, mail_id, comment_id "
    strSql = strSql & " FROM ( "
    strSql = strSql & " SELECT C.comment_date, C.comment_time,C.comment, E.event_type_name_" & userlang() & " as event_type_name, H.firstname, H.lastname, null as mail_id, C.comment_id, C.AuditDateCreated "
    strSql = strSql & " FROM dbo.comments AS C "
    strSql = strSql & " INNER JOIN dbo.event_types AS E ON C.comment_event_type_id = E.event_type_id "
    strSql = strSql & " INNER JOIN dbo.users AS U ON C.comment_usr_id  = U.usr_id "
    strSql = strSql & " INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id " 
    strSql = strSql & " INNER JOIN dbo.companies AS CO ON C.comment_company_id = CO.company_id "
    strSql = strSql & " where CO.company_hbc_id = " & hbc_id() & " "
    strSql = strSql & " UNION ALL "
    strSql = strSql & " select cast(CONVERT(char(8), M.mail_creationdate, 112) as int) as comment_date, cast(CONVERT(char(5), M.mail_creationdate, 108) as nvarchar) as comment_time, M.Subject as comment, T.mail_template_name_" & userlang() & " as event_type_name, H.firstname, H.LastName, M.mail_id, null as comment_id, M.mail_creationdate as AuditDateCreated "
    strSql = strSql & " FROM [salestool].[dbo].[mails] M "
    strSql = strSql & " inner join mail_templates T on M.mail_template_id = T.mail_template_id "
    strSql = strSql & " INNER JOIN dbo.users AS U ON M.user_id  = U.usr_id "
    strSql = strSql & " INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id "
    strSql = strSql & " INNER JOIN dbo.companies AS CO ON M.company_id = CO.company_id "
    strSql = strSql & " where CO.company_hbc_id = " & hbc_id() & " "
    strSql = strSql & " ) AS S "
    strSql = strSql & " order by S.AuditDateCreated desc"
    'strSql = "SELECT * FROM [dbo].[vw_GetComments_onBoID] WHERE companyid =" & company_id() & " ORDER BY AuditDateCreated DESC"  
	'strSql = "SELECT C.comment_date, C.comment_time,C.comment, E.event_type_name_" & userlang() & " as event_type_name, H.firstname, H.lastname "
	'strSql = strSql & "FROM dbo.comments AS C "
	'strSql = strSql & "INNER JOIN dbo.event_types AS E ON C.comment_event_type_id = E.event_type_id "
	'strSql = strSql & "INNER JOIN dbo.users AS U ON C.comment_usr_id  = U.usr_id "
	'strSql = strSql & "INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id "
    'strSql = strSql & "INNER JOIN dbo.companies AS CO ON C.comment_company_id = CO.company_id "
	'strSql = strSql & "where CO.company_hbc_id = " & hbc_id() & " "
	'strSql = strSql & "order by C.AuditDateCreated desc "
    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with

    if objConn.State <> 1 then exit function

        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 1 'adCmdText
            .CommandText =  strSql
            on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                "<br />Error in Class: sysEvents.pGetEventHistory()" & _
                "<br />while triggering [set objRS = .Execute]."
                pGetEventHistory = sErrMsg
                exit function
            end if
         end with
    
        with objRS
            if not .bof and not .eof then
                dim tt, hdr            
                do while not .eof
                    tt = formatTimeStamp(.fields("comment_date"), .fields("comment_time"))
                    hdr = tt & " - " & toUnicode(.fields("firstname")) & " " & toUnicode(.fields("lastname"))  & " [" & .fields("event_type_name") & "] : "
                    s=s & "<hr><p>" & hdr & toUnicode(.fields("comment")) & "</p>"
                    if .fields("mail_id") & "" <> "" then
                    s=s & "<button type='button' onclick='openWindowMail(" & .fields("mail_id") & ")'>Mail</button>"
                    end if
                .movenext
                loop
            end if
        end with    
    s= replace(s, chr(10) & chr(13), "<br>")
    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pGetEventHistoryFromHbcId = s
end function
 private function pGetCompanyHistory()
    Dim objConn, oRs, strSql
	Dim compId
	compId = Request.QueryString("companyId")
	
	if NOT Isnumeric(compId) Then
	compId	= -1
	End if
	
    strSql = "SELECT E.text as [text], U.usr_login as [USR], E.start_date as [start_date] FROM [dbo].[events] AS E JOIN users AS U on E.usr_id = U.usr_id WHERE E.company_id=" & compId & " ORDER BY E.start_date DESC"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pGetCompanyHistory = strSql
        oRs.open strSql, objConn
		
		s= "{""history"":["
		with oRs
            if not .bof and not .eof then
                .moveFirst 
                do while not .eof
                    s = s & "{""text"":""" & toUnicode(.fields("text")) & """, ""date"": """ & .fields("start_date") & """, ""usr"":""" & toUnicode(.fields("usr")) & """},"
                    .movenext
                loop       
				s = left(s, (len(s)-1))  
			end if
		End With
		s = s & "]}"  
		pGetCompanyHistory = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
    end function
	
    private function pGetEventsNF()
    Dim objConn, objCmd, objParam, objRS, strSql, sErrMsg
    
    Dim event_type_name
        event_type_name = "event_type_name_" & userlang()

    strSql = "dbo.Events_GET_NoFinished"
    dim a, b, c, d, e
        
    a = usr_id()
    b = userlang()
    c = useColors()
    d = useIcons()
    e = event_type_id()
          
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@usr_id", 5, 1, len(a), a)
            .Parameters.Append .CreateParameter("@userlang", 8, 1, len(b), b)
            .Parameters.Append .CreateParameter("@useColor", 8, 1, len(c), c)
            .Parameters.Append .CreateParameter("@useIcons", 8, 1, len(d), d)
            .Parameters.Append .CreateParameter("@event_type_id", 5, 1, len(e), e)
    
     on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()

            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysEvents.pGetEventsNF()" & _
                    "<br />while triggering [set objRS = .Execute]." 
                    response.write sErrMsg
                exit function
            end if
       end with       
                with objRS
                    if not .bof and not .eof then
                        pGetEventsNF = .fields(0)
                    end if
                end with

    
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
 end function

 private function pGetCurrentOpenTasksForEventId()
	Dim objConn, oRs, strSql
	
	dim evtTypes
	evtTypes = event_types()
	
	strSQL = "select * from ( "
	strSQL = strsql & " select top 10 id, text, CONVERT(VARCHAR(19),start_date,120) as start_date, CONVERT(VARCHAR(19), end_date, 120) as end_date, [readonly], usr_id, E.event_type_id, c.company_name, c.company_vat, "
	strSQL = strsql & " c.company_hbc_id, c.company_id, T.event_type_name_" & Userlang() & " as event_type_name,'#666666' as color,"
	strSQL = strSQL & " T.event_type_textcolor as textColor, T.event_type_icon as icon "
	strSQL = strSQL & " from events AS E "
	strSQL = strSQL & " join companies AS C on E.company_id = C.company_id "
	strSQL = strSQL & " join event_types AS T on E.event_type_id = T.event_type_id "
	strSQL = strSQL & " where E.usr_id=" & usr_id() & " AND E.event_type_id NOT IN (17,19,21,26) AND E.start_date < (select CAST(GETDATE() as date)) "
	
	if (len(evtTypes) > 0) then
	strSQL = strSQL & " AND E.event_type_id IN("& evtTypes & ") "
	end if
	strSQL = strSQL & " ) as part1 "
	strSQL = strSQL & " union all " 
	strSQL = strSQL & " select * from ( "
	strSQL = strSQL & " select id, text, CONVERT(VARCHAR(19),start_date,120) as start_date, CONVERT(VARCHAR(19), end_date, 120) as end_date, [readonly], usr_id, E.event_type_id, c.company_name, c.company_vat, "
	strSQL = strSQL & " c.company_hbc_id, c.company_id, T.event_type_name_" & Userlang() & " as event_type_name,'#FFFA00' as color, "
	strSQL = strSQL & " T.event_type_textcolor as textColor, T.event_type_icon as icon "
	strSQL = strSQL & " from events as E "
	strSQL = strSQL & " join companies AS C on E.company_id = C.company_id "
	strSQL = strSQL & " join event_types AS T on E.event_type_id = T.event_type_id "
	strSQL = strSQL & " where E.usr_id=" & usr_id() & " AND E.event_type_id NOT IN (17,19,21,26) AND E.start_date > (select CAST(GETDATE() as date)) AND E.start_date < (select CAST(GETDATE()+2 as date)) "
	
	if (len(evtTypes) > 0) then
	strSQL = strSQL & " AND E.event_type_id IN("& evtTypes & ") "
	end if
	strSQL = strSQL & " ) as part2 "
	
	set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    pGetCurrentOpenTasksForEventId = strSql
    oRs.open strSql, objConn
    
	dim xmlOutput
	xmlOutput = "<?xml version=""1.0"" ?><data>"
	
	if not oRs.eof then
		Do While Not oRs.eof
			xmlOutput = xmlOutput & "<event id=""" & pEncode(oRs("id")) & """>"
			xmlOutput = xmlOutput & "<text><![CDATA[" & pEncode(oRs("text")) & "]]></text>"
			xmlOutput = xmlOutput & "<start_date>" & pEncode(oRs("start_date")) & "</start_date>"
			xmlOutput = xmlOutput & "<end_date>" & pEncode(oRs("end_date")) & "</end_date>"
			xmlOutput = xmlOutput & "<usr_id>" & pEncode(oRs("usr_id")) & "</usr_id>"
			xmlOutput = xmlOutput & "<event_type_id>" & pEncode(oRs("event_type_id")) & "</event_type_id>"
			xmlOutput = xmlOutput & "<company_name><![CDATA[" & pEncode(oRs("company_name")) & "]]></company_name>"
			xmlOutput = xmlOutput & "<company_vat>" & pEncode(oRs("company_vat")) & "</company_vat>"
			xmlOutput = xmlOutput & "<company_hbc_id>" & pEncode(oRs("company_hbc_id")) & "</company_hbc_id>"
			xmlOutput = xmlOutput & "<company_id>" & pEncode(oRs("company_id")) & "</company_id>"
			xmlOutput = xmlOutput & "<event_type_name>" & pEncode(oRs("event_type_name")) & "</event_type_name>"
			xmlOutput = xmlOutput & "<icon>" & pEncode(oRs("icon")) & "</icon>"
			xmlOutput = xmlOutput & "</event>"
			oRs.MoveNext
		Loop
	end if
	xmlOutput = xmlOutput & "</data>"
	pGetCurrentOpenTasksForEventId = xmlOutput        
    
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing    
 end function
 
 private function pGetCurrentOpenTasks()
	Dim objConn, oRs, strSql
  
	strSQL = "select * from ( "
	strSQL = strsql & " select top 10 id, text, CONVERT(VARCHAR(19),start_date,120) as start_date, CONVERT(VARCHAR(19), end_date, 120) as end_date, [readonly], usr_id, E.event_type_id, c.company_name, c.company_vat, "
	strSQL = strsql & " c.company_hbc_id, c.company_id, T.event_type_name_" & Userlang() & " as event_type_name,'#666666' as color,"
	strSQL = strSQL & " T.event_type_textcolor as textColor, T.event_type_icon as icon "
	strSQL = strSQL & " from events AS E "
	strSQL = strSQL & " join companies AS C on E.company_id = C.company_id "
	strSQL = strSQL & " join event_types AS T on E.event_type_id = T.event_type_id "
	strSQL = strSQL & " where E.usr_id=" & usr_id() & " AND E.event_type_id NOT IN (17,19,21,26) AND E.start_date <= (select CAST(GETDATE() as date)) "

	strSQL = strSQL & " ) as part1 "
	strSQL = strSQL & " union all " 
	strSQL = strSQL & " select * from ( "
	strSQL = strSQL & " select id, text, CONVERT(VARCHAR(19),start_date,120) as start_date, CONVERT(VARCHAR(19), end_date, 120) as end_date, [readonly], usr_id, E.event_type_id, c.company_name, c.company_vat, "
	strSQL = strSQL & " c.company_hbc_id, c.company_id, T.event_type_name_" & Userlang() & " as event_type_name,'#FFFA00' as color, "
	strSQL = strSQL & " T.event_type_textcolor as textColor, T.event_type_icon as icon "
	strSQL = strSQL & " from events as E "
	strSQL = strSQL & " join companies AS C on E.company_id = C.company_id "
	strSQL = strSQL & " join event_types AS T on E.event_type_id = T.event_type_id "
	strSQL = strSQL & " where E.usr_id=" & usr_id() & " AND E.event_type_id NOT IN (17,19,21,26) AND E.start_date > (select CAST(GETDATE() as date)) AND E.start_date < (select CAST(GETDATE()+2 as date)) "
	
	strSQL = strSQL & " ) as part2 "
	
	set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    pGetCurrentOpenTasks = strSql
    oRs.open strSql, objConn
    
	dim xmlOutput
	xmlOutput = "<?xml version=""1.0"" ?><data>"
	
	if not oRs.eof then
		Do While Not oRs.eof
			xmlOutput = xmlOutput & "<event id=""" & pEncode(oRs("id")) & """>"
			xmlOutput = xmlOutput & "<text><![CDATA[" & pEncode(oRs("text")) & "]]></text>"
			xmlOutput = xmlOutput & "<start_date>" & pEncode(oRs("start_date")) & "</start_date>"
			xmlOutput = xmlOutput & "<end_date>" & pEncode(oRs("end_date")) & "</end_date>"
			xmlOutput = xmlOutput & "<usr_id>" & pEncode(oRs("usr_id")) & "</usr_id>"
			xmlOutput = xmlOutput & "<event_type_id>" & pEncode(oRs("event_type_id")) & "</event_type_id>"
			xmlOutput = xmlOutput & "<company_name><![CDATA[" & pEncode(oRs("company_name")) & "]]></company_name>"
			xmlOutput = xmlOutput & "<company_vat>" & pEncode(oRs("company_vat")) & "</company_vat>"
			xmlOutput = xmlOutput & "<company_hbc_id>" & pEncode(oRs("company_hbc_id")) & "</company_hbc_id>"
			xmlOutput = xmlOutput & "<company_id>" & pEncode(oRs("company_id")) & "</company_id>"
			xmlOutput = xmlOutput & "<event_type_name>" & pEncode(oRs("event_type_name")) & "</event_type_name>"
			xmlOutput = xmlOutput & "<icon>" & pEncode(oRs("icon")) & "</icon>"
			xmlOutput = xmlOutput & " </event>"
			oRs.MoveNext
		Loop
	end if
	xmlOutput = xmlOutput & "</data>"
	pGetCurrentOpenTasks = xmlOutput        
    
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing    
 end function
 
 private function pGetEvents()
    Dim objConn, objCmd, objParam, objRS, strSql, sErrMsg
    
    Dim event_type_name
        event_type_name = "event_type_name_" & userlang()

    strSql = "dbo.Events_GET"
    dim a, b, c, d, e
        
    a = usr_id()
    b = userlang()
    c = useColors()
    d = useIcons()
    e = event_type_id()
          
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@usr_id", 5, 1, len(a), a)
            .Parameters.Append .CreateParameter("@userlang", 8, 1, len(b), b)
            .Parameters.Append .CreateParameter("@useColor", 8, 1, len(c), c)
            .Parameters.Append .CreateParameter("@useIcons", 8, 1, len(d), d)
            .Parameters.Append .CreateParameter("@event_type_id", 5, 1, len(e), e)
    
     on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()

            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysEvents.pGetEvents()" & _
                    "<br />while triggering [set objRS = .Execute]." 
                    response.write sErrMsg
                exit function
            end if
       end with       
                with objRS
                    if not .bof and not .eof then
                        pGetEvents = .fields(0)
                    end if
                end with

    
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
    end function
	
private function pXML_GetEventTypes()       
    Dim objConn, oRs, strSql
        
    strSql = "SELECT * FROM [dbo].[vw_scheduler_XML_GetEventTypes]"
        
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pXML_GetEventTypes = strSql
        oRs.open strSql, objConn
            if not oRs.bof and not oRs.eof then pXML_GetEventTypes = oRs.fields(0)        
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing    
end function   
    
private function pGetComboEventTypesForFunction()       
    Dim objConn, oRs, strSql, s
            
    strSql = " SELECT E.event_type_id, E.event_type_name_" & Userlang() & "as event_type_name "
	strSql = strSql & " FROM dbo.event_types E"
	strSql = strSql & " inner join link_event_types_function F "
	strSql = strSql & " ON F.event_type_id = E.event_type_id "
	strSql = strSql & " where E.event_type_visible = 1 "
	strSql = strSql & " AND F.function_id = " & function_id()
            
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        oRs.open strSql, objConn
        with oRs
            if not .bof and not .eof then
            .moveFirst 
                s= "<?xml version='1.0' encoding='utf-8' ?><complete>"
                    do while not .eof
                        s = s & "<option value='" &  server.HTMLEncode(.fields("event_type_id")) & "'>" & server.HTMLEncode(toUnicode(.fields("event_type_name"))) & "</option>"
                    .movenext
                loop       
            s = s & "</complete>"    
        end if
        End With
    pGetComboEventTypesForFunction = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function
private function pGetComboEventTypes() 
    Dim objConn, oRs, strSql, EventTypeName, s
    EventTypeName = "event_type_name_EN" '& UserLang() 
            
    strSql = "SELECT * FROM dbo.[vw_event_types]"
            
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        oRs.open strSql, objConn
        with oRs
            if not .bof and not .eof then
            .moveFirst 
                s= "<?xml version='1.0' encoding='utf-8' ?><complete>"
                    do while not .eof
                        s = s & "<option value=""" &  server.HTMLEncode(.fields(0)) & """>" & server.HTMLEncode(toUnicode(.fields(EventTypeName))) & "</option>"
                    .movenext
                loop       
            s = s & "</complete>"    
        end if
        End With
    pGetComboEventTypes = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function


    private function pGetComboEventTypesKeyValForFunction()       
            Dim objConn, oRs, strSql, s
            
            strSql = " SELECT E.event_type_id as [key], E.event_type_name_" & userLang & " as label "
			strSql = strSql & " FROM dbo.event_types E "
			strSql = strSql & " inner join link_event_types_function F "
			strSql = strSql & " ON F.event_type_id = E.event_type_id "
			strSql = strSql & " where E.event_type_visible = 1 "
			strSql = strSql & " AND F.function_id = " & function_id()
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
            
			s = RStoJSON(oRs)
			
            pGetComboEventTypesKeyValForFunction = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function
	
    private function pGetComboEventTypesKeyVal()       
            Dim objConn, oRs, strSql, EventTypeName, s
            EventTypeName = "event_type_name_" & UserLang() 
            
            strSql = "SELECT event_type_id as [key], event_type_name_" & userLang & " as label FROM dbo.[vw_event_types]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
            
			s = RStoJSON(oRs)
			
            pGetComboEventTypesKeyVal = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

    private function pGetComboEventTypesWithImg()       
            Dim objConn, oRs, strSql, EventTypeName, s
            EventTypeName = "event_type_name_" & UserLang() 
            
            strSql = "SELECT * FROM dbo.[vw_event_types]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            s = s & "{value:""%"", img: ""cancel.png"", text:""Select...""},"
                        do while not .eof
                                's = s & "{key:" & .fields(0) & ",label:'" & toUnicode(.fields(EventTypeName)) & "'},"
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields("event_type_icon") & """, text:""" & toUnicode(.fields(EventTypeName)) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboEventTypesWithImg = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function
    private function pGetComboEventTypesWithImgForFunction()       
            Dim objConn, oRs, strSql, s       
            strSql = "SELECT E.event_type_id, E.event_type_code, event_type_name_" & UserLang() & " as EventTypeName, "
			strSql = strSql & " E.event_type_order, E.event_type_duration_min, E.event_type_color, "
			strSql = strSql & " E.event_type_textColor, E.event_type_enabled, E.event_type_icon, E.event_type_visible, "
			strSql = strSql & " E.event_type_mail_template_id "
			strSql = strSql & " FROM dbo.event_types E "
			strSql = strSql & " inner join link_event_types_function F "
			strSql = strSql & " ON F.event_type_id = E.event_type_id "
			strSql = strSql & " where E.event_type_visible = 1 "
			strSql = strSql & " AND F.function_id = " & function_id()
			
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                            s = s & "{value:""%"", img: ""cancel.png"", text:""Select...""},"
                        do while not .eof
                                's = s & "{key:" & .fields(0) & ",label:'" & toUnicode(.fields("EventTypeName")) & "'},"
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields("event_type_icon") & """, text:""" & toUnicode(.fields("EventTypeName")) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboEventTypesWithImgForFunction = s '(server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

	private function pGetComboEventTypesOptionValForFunction()       
        Dim objConn, oRs, strSql, EventTypeName, s
        EventTypeName = "event_type_name_EN" '& UserLang() 
            
            strSql = "SELECT E.event_type_id, E.event_type_name_" & userLang() & " as event_type_name "
            strSql = strSql & " FROM dbo.event_types E  "
			strSql = strSql & " inner join link_event_types_function F "
			strSql = strSql & " ON F.event_type_id = E.event_type_id "
			strSql = strSql & " where E.event_type_visible = 1 " 
			strSql = strSql & " AND F.function_id = " & function_id()
			
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
            with oRs
                if not .bof and not .eof then
                .moveFirst
                s= "<complete>"
                    do while not .eof
                        s = s & "<option value='" & .fields("event_type_id") & "'>" & toUnicode(.fields("event_type_name")) & "</option>"
                        .movenext
                    loop       
                end if
                End With
                s = s & "</complete>"    
            pGetComboEventTypesOptionValForFunction = server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function
    private function pGetComboEventTypesOptionVal()       
        Dim objConn, oRs, strSql, EventTypeName, s
        EventTypeName = "event_type_name_EN" '& UserLang() 
            
            strSql = "SELECT * FROM dbo.[vw_event_types]"
            
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
            with oRs
                if not .bof and not .eof then
                .moveFirst
                s= "<complete>"
                    do while not .eof
                        s = s & "<option value=" & .fields(0) & ">" & toUnicode(.fields(EventTypeName)) & "</option>"
                        .movenext
                    loop       
                end if
                End With
                s = s & "</complete>"    
            pGetComboEventTypesOptionVal = server.HTMLEncode(s)
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function

	Private Function IsNotNullorEmpty(input)
	dim IsNotNullorEmpty_Output
	IsNotNullorEmpty_Output = true
	'vbEmpty (uninitialized) or vbNull (no valid data)
	if VarType(input) = 0 OR VarType(input) = 1 Then
		IsNotNullorEmpty_Output = false
	End if
	If VarType(input) = 8 Then
		If input = "" Then 
			IsNotNullorEmpty_Output = False
		End If
	End If
	IsNotNullorEmpty = IsNotNullorEmpty_Output

End Function
	
    private function pXML_SaveEvents()
    Dim objConn, oRs, strSql
    Dim actionValues
    Dim i, text, sD, eD, event_type_id, newuserID, newEventId
        
        actionValues = request.form("!nativeeditor_status")
        i = trim(request.Form("id"))
        text = trim(request.Form("text"))
		text = toUnicode(text)
        sD = trim(request.Form("start_date"))
        eD = trim(request.Form("end_date"))
        event_type_id = trim(request.Form("event_type_id"))
        newuserID = trim(request.Form("usr_id"))
        mailoptions= trim(request.Form("mailoptions"))

    Dim objComment
    set objComment = new clsObjComment
        objComment.comment_event_id = i
        objComment.comment_event_type_id = event_type_id
        objComment.comment_usr_id = usr_id()
        objComment.comment = text
        objComment.comment_date = SetDate()
        objComment.comment_time = SetTime()
        objComment.comment_company_vat = trim(request.Form("company_vat"))
		objComment.comment_company_id = trim(request.Form("company_id"))

    set objConn=Server.CreateObject("ADODB.Connection")

        select case actionValues
            case "inserted"
                strSql = "SELECT * FROM events where 1=0"
                
        case "deleted"
                'strSql = "DELETE FROM events where id=" & i   
                'objConn.open sConnStrDev
                'objConn.execute strSql
                'pXML_SaveEvents= "record deleted."
                exit function
            case else 'default is updated'
                actionValues="updated"
                strSql = "SELECT * FROM events where id=" & i & " AND ISNULL(readonly,0) <> 1"        
                
        end select
    
        set oRs=Server.CreateObject("ADODB.Recordset") 
        oRs.cursorlocation = 3
        objConn.open SalesToolConnString()
        oRs.open strSql, objConn, 3,3,1
        with oRs
			select case actionValues
				case "inserted"
					.addnew
					.fields("usr_id")=usr_id()
					
					.fields("start_date")=sD
					.fields("end_date")=eD
				case "updated"
					if .EOF Then
						pXML_SaveEvents= "failure: record locked."
						Exit Function
					End if
					select case event_type_id
						case "17", "19", "21", "29"
							dim datetimeVar
							datetimeVar = SetDateWithSeperator("-") & " " & SetTime()
							.fields("start_date") = datetimeVar
							.fields("end_date") =   datetimeVar
						case else		
							.fields("start_date")=sD
							.fields("end_date")=eD						
					end select
					if usr_id() <> newuserID then
						dim objCommentTransfer
						set objCommentTransfer = new clsObjComment
						objCommentTransfer.comment_event_id = i
						objCommentTransfer.comment_event_type_id = event_type_id
						objCommentTransfer.comment_usr_id = usr_id()
						objCommentTransfer.comment = C_TRANSFER_LEAD & " : " & usr_id() & " -> " & newuserID
						objCommentTransfer.comment_date = SetDate()
						objCommentTransfer.comment_time = SetTime()
						objCommentTransfer.comment_company_vat = trim(request.Form("company_vat"))
						objCommentTransfer.comment_company_id = trim(request.Form("company_id"))
						AddComment(objCommentTransfer)
					end if
			end select
			
			.fields("text")=text
			.fields("usr_id")=newuserID
			.fields("event_type_id")=event_type_id
			.fields("visible")=1
			.update
			
			newEventId = toUnicode(.fields("id"))
        end with

        oRs.close : set oRs = nothing    
        objConn.close : set objConn = nothing
        'objComment.comment = "[" & actionValues & "] : " & "[" & sD & " - " & eD & "]<br>" & text
        AddComment(objComment)

        'if mail Option Email=checked in Event CustomLightBox then we send email with selected options
        
        
    if instr(1,mailoptions,"1", vbtextcompare) > 0 then 
            Dim objMail
            set objMail = new clsObjEmail
                with objMail
                    if instr(1,mailoptions,"1", vbtextcompare) > 0 then .optSendMail=1 else .optSendMail=0
                    if instr(1,mailoptions,"2", vbtextcompare) > 0 then .optSendVCal=1 else .optSendVCal=0
                    if instr(1,mailoptions,"3", vbtextcompare) > 0 then .optSendVCard=1 else .optSendVCard=0
                    .contact_id = trim(request.Form("mailto"))
                    .DTStart = sD
                    .DTEnd = eD
                    .event_id = trim(request.Form("id"))
                    .event_type_id = trim(request.Form("event_type_id"))
                    .SendWithOptions
                end with
            
        end if    
		pXML_SaveEvents = "<?xml version=""1.0"" ?><data><action type=""" & actionValues & """ sid=""" & i & """ tid=""" & newEventId & """ /></data>"
    end function

'<END REGION XML BUSINESS LOGIC>=============================================================


'<START REGION JSON BUSINESS LOGIC>----------------------------------------------------------

    private function pJSON_GetEvents()
        
        Dim objConn, oRs, strSql, arrayName, output
        
        strSql = "SELECT * FROM [dbo].[vw_scheduler_JSON_GetEvents]"
        
        set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        oRs.open strSql, objConn
                if not oRs.bof and not oRs.eof then pJSON_GetEvents = oRs.fields(0)        
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing
    end function
	private function pJSON_GetEventTypesForFunction()
	    Dim objConn, oRs, strSql

        strSql = "SELECT E.event_type_id as [key], E.event_type_name_" & userLang() & " as label "
		strSql = strSql & " FROM dbo.event_types E "
		strSql = strSql & " inner join link_event_types_function F "
		strSql = strSql & " ON F.event_type_id = E.event_type_id "
		strSql = strSql & " where E.event_type_visible = 1 "
		strSql = strSql & " AND F.function_id = " & function_id()
        
		set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
            objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext

            oRs.open strSql, objConn
			
		if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if
			
		pJSON_GetEventTypesForFunction = s       
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing
	end function
        private function pJSON_GetEventTypes()       
        Dim objConn, oRs, strSql, event_type_name

        strSql = "SELECT event_type_id as [key], event_type_name_" & userLang() & " as label FROM dbo.[vw_event_types]"
        
    set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
            objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
            pJSON_GetEventTypes = strSql
            oRs.open strSql, objConn
			
		if not oRs.bof and not oRs.eof then
			s = "[" & RStoJSON(oRs) & "]"
		else
			s = "[]"
		end if
			
     pJSON_GetEventTypes = s       
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    
    end function

    'this is phased out (along with the transform view I guess ->vw_scheduler_JSON_GetEventTypes
    private function pJSON_GetEventTypes2()       
        Dim objConn, oRs, strSql

        strSql = "SELECT * FROM [dbo].[vw_scheduler_JSON_GetEventTypes]"
        
    set objConn=Server.CreateObject("ADODB.Connection")
        set oRs=Server.CreateObject("ADODB.Recordset") 
            objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
            pJSON_GetEventTypes = strSql
            oRs.open strSql, objConn
                if not oRs.bof and not oRs.eof then pJSON_GetEventTypes = oRs.fields(0)        
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    
    end function

private function pGetFutureEvents()
    Dim objConn, objRs, strSql, s, sfield, event_type_name
    event_type_name = "event_type_name_" & userlang()

    strSql = "SELECT E.start_date, E.text, P.firstname, P.lastname, ET.event_type_name_" & userlang() & " as event_type_name FROM [dbo].[events] AS E "
	strsql = strsql & " join dbo.event_types as ET on E.event_type_id = ET.event_type_id "
	strSQL = strsql & " join dbo.users as U on E.usr_id = U.usr_id"
	strsql = strsql & " join dbo.HR_Persons AS P on U.person_id = P.person_id "
	strsql = strsql & " WHERE E.company_id = " & company_id() & " AND E.start_date > GETDATE() "
	strsql = strsql & " ORDER BY E.AuditDateCreated DESC "
    
	set objConn=Server.CreateObject("ADODB.Connection")
	set objRs=Server.CreateObject("ADODB.Recordset") 
    objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
    objRs.open strSql, objConn

	with objRS
		if not .bof and not .eof then
			.moveFirst
			dim tt, hdr            
			do while not .eof
				tt = .fields("start_date")
				hdr = tt & " - " & toUnicode(.fields("firstname")) & " " & toUnicode(.fields("lastname"))  & " [" & .fields("event_type_name") & "] : "
				s=s & "" & hdr & toUnicode(.fields("text")) & "<br>"
			.movenext
			loop
		end if
	end with   
   
    s= replace(s, chr(10) & chr(13), "<br>")
        
	pGetFutureEvents = s
	
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing
end function

private function pGetEventHistoryFromEvent()
   
    Dim debugMsg, event_type_name
    Dim objConn, objRS, strSql, sErrMsg
	
     strSql = " SELECT comment_date, comment_time, comment, event_type_name,firstname,lastname, mail_id, comment_id "
    strSql = strSql & " FROM ( "
    strSql = strSql & " SELECT C.comment_date, C.comment_time,C.comment, E.event_type_name_" & userlang() & " as event_type_name, H.firstname, H.lastname, null as mail_id, C.comment_id, C.AuditDateCreated "
    strSql = strSql & " FROM dbo.comments AS C "
    strSql = strSql & " INNER JOIN dbo.event_types AS E ON C.comment_event_type_id = E.event_type_id "
    strSql = strSql & " INNER JOIN dbo.users AS U ON C.comment_usr_id  = U.usr_id "
    strSql = strSql & " INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id " 
    strSql = strSql & " where C.comment_event_id = " & event_id() & " "
    strSql = strSql & " UNION ALL "
    strSql = strSql & " select cast(CONVERT(char(8), M.mail_creationdate, 112) as int) as comment_date, cast(CONVERT(char(5), M.mail_creationdate, 108) as nvarchar) as comment_time, M.Subject as comment, T.mail_template_name_" & userlang() & " as event_type_name, H.firstname, H.LastName, M.mail_id, null as comment_id, M.mail_creationdate as AuditDateCreated "
    strSql = strSql & " FROM [salestool].[dbo].[mails] M "
    strSql = strSql & " inner join mail_templates T on M.mail_template_id = T.mail_template_id "
    strSql = strSql & " INNER JOIN dbo.users AS U ON M.user_id  = U.usr_id "
    strSql = strSql & " INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id "
    strSql = strSql & " where M.company_id IN (select distinct company_id from events where id =" & event_id() & ") "
    strSql = strSql & " ) AS S "
    strSql = strSql & " order by S.AuditDateCreated desc"
	    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with

    if objConn.State <> 1 then exit function

        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 1 'adCmdText
            .CommandText =  strSql
            on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                "<br />Error in Class: sysEvents.pGetEventHistoryFromEvent()" & _
                "<br />while triggering [set objRS = .Execute]."
                pGetEventHistoryFromEvent = sErrMsg
                exit function
            end if
         end with
    
        with objRS
            if not .bof and not .eof then
                dim tt, hdr            
                do while not .eof
                    tt = formatTimeStamp(.fields("comment_date"), .fields("comment_time"))
                    hdr = tt & " - " & toUnicode(.fields("firstname")) & " " & toUnicode(.fields("lastname"))  & " [" & .fields("event_type_name") & "] : "
                    s=s & "<hr><p>" & hdr & toUnicode(.fields("comment")) & "</p>"
                    if .fields("mail_id") & "" <> "" then
                        s=s & "<button type='button' onclick='openWindowMail(" & .fields("mail_id") & ")'>Mail</button>"
                    end if
                .movenext
                loop
            end if
        end with    
    s= replace(s, chr(10) & chr(13), "<br>")
    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pGetEventHistoryFromEvent = s

end function

private function pCanProceed()
	pCanProceed = "0"
    Dim objConn, objRS, strSql
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 0 then set objConn = nothing : exit function : end if
    varval=0    
	
	strSQL = "SELECT COUNT(*) FROM events where company_id = " & company_id() & " and start_date > getdate()"
    
        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysCounter.pGetSalesCountervalue(blnIndividual)" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

         with objRS
            if not .bof and not .eof then       
            .moveFirst 
                varVal = cdbl(objRs.Fields(0))
            else
                varval=0
            end if
        end with
        
		if (varVal = 0) then
				pCanProceed = "1"
		end if
		
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing     
end function

private function pGetEventHistory()
   
    Dim debugMsg, event_type_name
    Dim objConn, objRS, strSql, sErrMsg
	
     strSql = " SELECT comment_date, comment_time, comment, event_type_name,firstname,lastname, mail_id, comment_id "
    strSql = strSql & " FROM ( "
    strSql = strSql & " SELECT C.comment_date, C.comment_time,C.comment, E.event_type_name_" & userlang() & " as event_type_name, H.firstname, H.lastname, null as mail_id, C.comment_id, C.AuditDateCreated "
    strSql = strSql & " FROM dbo.comments AS C "
    strSql = strSql & " INNER JOIN dbo.event_types AS E ON C.comment_event_type_id = E.event_type_id "
    strSql = strSql & " INNER JOIN dbo.users AS U ON C.comment_usr_id  = U.usr_id "
    strSql = strSql & " INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id " 
    strSql = strSql & " where C.comment_company_id = " & company_id() & " "
    strSql = strSql & " UNION ALL "
    strSql = strSql & " select cast(CONVERT(char(8), M.mail_creationdate, 112) as int) as comment_date, cast(CONVERT(char(5), M.mail_creationdate, 108) as nvarchar) as comment_time, M.Subject as comment, T.mail_template_name_" & userlang() & " as event_type_name, H.firstname, H.LastName, M.mail_id, null as comment_id, M.mail_creationdate as AuditDateCreated "
    strSql = strSql & " FROM [salestool].[dbo].[mails] M "
    strSql = strSql & " inner join mail_templates T on M.mail_template_id = T.mail_template_id "
    strSql = strSql & " INNER JOIN dbo.users AS U ON M.user_id  = U.usr_id "
    strSql = strSql & " INNER JOIN dbo.HR_persons AS H ON U.person_id = H.person_id "
    strSql = strSql & " where M.company_id = " & company_id() & " "
    strSql = strSql & " ) AS S "
    strSql = strSql & " order by S.AuditDateCreated desc"
	    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with

    if objConn.State <> 1 then exit function

        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 1 'adCmdText
            .CommandText =  strSql
            on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
            If Err.number <> 0 then
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                "<br />Error in Class: sysEvents.pGetEventHistory()" & _
                "<br />while triggering [set objRS = .Execute]."
                pGetEventHistory = sErrMsg
                exit function
            end if
         end with
    
        with objRS
            if not .bof and not .eof then
                dim tt, hdr            
                do while not .eof
                    tt = formatTimeStamp(.fields("comment_date"), .fields("comment_time"))
                    hdr = tt & " - " & toUnicode(.fields("firstname")) & " " & toUnicode(.fields("lastname"))  & " [" & .fields("event_type_name") & "] : "
                    s=s & "<hr><p>" & hdr & toUnicode(.fields("comment")) & "</p>"
                    if .fields("mail_id") & "" <> "" then
                        s=s & "<button type='button' onclick='openWindowMail(" & .fields("mail_id") & ")'>Mail</button>"
                    end if
                .movenext
                loop
            end if
        end with    
    s= replace(s, chr(10) & chr(13), "<br>")
    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pGetEventHistory = s

end function


Private Function pgetEventHistory2()
    Dim objConn, objCmd, objRS, strSql,  sErrMsg
        
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = SalesToolConnString()
        .CommandTimeOut = 15
        .mode=1	'Read-only
        .CursorLocation = 3 '2 adUseServer 3 adUseClient
        .Open    
    end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

        strSql="dbo.Comments_GET"
        set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@comment_event_id", 8, 1, len(event_id()), event_id())
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with

            with objRS
                if not .bof and not .eof then
                    Dim event_type_name, tt, hdr, s
                    event_type_name = "event_type_name_" & userlang()

                    do while not .eof
                        tt = formatTimeStamp(.fields("comment_date"), .fields("comment_time"))
                        hdr = tt & " - " & toUnicode(.fields("firstname")) & " " & toUnicode(.fields("lastname"))  & " [" & .fields(event_type_name) & "] : "
                        s=s & hdr & toUnicode(.fields("comment")) & "<br>"
                    .movenext
                    loop
                end if
            end with    
        s= replace(s, chr(10) & chr(13), "<br>")
    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    pGetEventHistory2 = s

end function


Private function AddComment(objComment)
    Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd
        blnNew=false
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    if objConn.State = 1 then
            strSql="dbo.Comments_ADD"                    
            
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@comment_event_type_id", 5, 1, len(objComment.comment_event_type_id), objComment.comment_event_type_id)
                .Parameters.Append .CreateParameter("@comment_usr_id", 5, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@comment_event_id", 8, 1, len(objComment.comment_event_id), objComment.comment_event_id)
                .Parameters.Append .CreateParameter("@comment_lead_id", 8, 1, 1, 0)
                .Parameters.Append .CreateParameter("@comment_company_vat", 8, 1, len(objComment.comment_company_vat), objComment.comment_company_vat)
				.Parameters.Append .CreateParameter("@comment_company_id", 8, 1, len(objComment.comment_company_id), objComment.comment_company_id)
                .Parameters.Append .CreateParameter("@comment_date", 5, 1, len(objComment.comment_date), objComment.comment_date)
                .Parameters.Append .CreateParameter("@comment_time", 8, 1, len(objComment.comment_time), objComment.comment_time)
                .Parameters.Append .CreateParameter("@comment", 8, 1, len(objComment.comment), objComment.comment)

                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@comment_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
            'objRs.Close
            set objRs= nothing
            set objComment = nothing
            comment_ID=objCmd(10)
        end if  

end function


    private function formatTimeStamp(sdate, stime)
    dim dd, mm, yy
        yy = left(sdate, 4)
        dd = right(sdate, 2)
        mm = mid(sdate,5,2)
    formatTimeStamp = dd & "-" & mm & "-" & yy & " " & stime
    end function

	public function SetDateWithSeperator(seperator)
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDateWithSeperator = yy & seperator & mm & seperator & dd
    end function
	
    public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    public function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function


'<END REGION JSON BUSINESS LOGIC>----------------------------------------------------------
end class

''get all items form request.form
'Dim Item, fieldName, fieldValue
'For Each Item In Request.Form
'    fieldName = Item
'    fieldValue = Request.Form(Item)
'    Response.Write fieldName & " = " & fieldValue & "<br>"
'Next

'***** phased out, replaced by stored procedure    
'       private function pXML_GetEvents()       
'        Dim objConn, oRs, strSql
        
'        strSql = "SELECT * FROM [dbo].[vw_scheduler_XML_GetEvents]"
        
'        set objConn=Server.CreateObject("ADODB.Connection")
'        set oRs=Server.CreateObject("ADODB.Recordset") 
'            objConn.open sConnStrDev , adopenstatic, adcmdreadonly, adcmdtext
'            oRs.open strSql, objConn
'                if not oRs.bof and not oRs.eof then pXML_GetEvents = oRs.fields(0)        
'            oRs.close : set oRs = nothing
'            objConn.close : set objConn = nothing
'    end function


%>