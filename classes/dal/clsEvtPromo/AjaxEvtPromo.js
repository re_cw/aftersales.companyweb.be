﻿var company_vat;

function populateobjLead(objLead) {
    var params = "lead_id=" + encodeURIComponent(objLead.lead_id);
    params += "&user_id=" + encodeURIComponent(objLead.user_id);
    params += "&status_id=" + encodeURIComponent(objLead.status_id);
    params += "&lang_id=" + encodeURIComponent(objLead.lang_id);
    params += "&source_id=" + encodeURIComponent(objLead.source_id);
    params += "&lead_type_id=" + encodeURIComponent(objLead.lead_type_id);
    params += "&backoffice_id=" + encodeURIComponent(objLead.backoffice_id);
    params += "&company_id=" + encodeURIComponent(objLead.company_id);
    params += "&prompt_date=" + encodeURIComponent(objLead.prompt_date);
    params += "&prompt_time=" + encodeURIComponent(objLead.prompt_time);
    params += "&company_name=" + encodeURIComponent(objLead.company_name);
    params += "&company_vat=" + encodeURIComponent(objLead.company_vat);
    params += "&company_address_street=" + encodeURIComponent(objLead.company_address_street);
    params += "&company_address_number=" + encodeURIComponent(objLead.company_address_number);
    params += "&company_address_postcode=" + encodeURIComponent(objLead.company_address_postcode);
    params += "&company_address_localite=" + encodeURIComponent(objLead.company_address_localite);
    params += "&company_tel=" + encodeURIComponent(objLead.company_tel);
    params += "&company_mobile=" + encodeURIComponent(objLead.company_mobile);
    params += "&company_mail=" + encodeURIComponent(objLead.company_mail);
    params += "&HBCompanyNotes=" + encodeURIComponent(objLead.HBCompanyNotes);
    params += "&new_user_id=" + encodeURIComponent(objLead.new_user_id);
    params += "&trusted=1";
    
    company_vat = encodeURIComponent(objLead.company_vat);
    return params;
}

function PromoteLeadToEvent(objLead, win, myDataView, scheduler, load_url) {
    var url = "/classes/dal/clsEvtPromo/clsEvtPromo.asp?q=1";
	var params = populateobjLead(objLead);
	
	$.post(url,params)
	.done(function(data){
		switch(data)
		{
			case "1":
				dhtmlx.message({
					text: locale.main_page.lblSaveSuccess,
					expire: 500,
					type: "successmessage"
				});
                dhtmlx.alert(imgalertsaveok2 + locale.main_page.lblSaveSuccess);
				myDataView.remove(myDataView.getSelected());
                win.close();
                scheduler.load(load_url);
			break;
			default:
				dhtmlx.message({
					text: locale.main_page.lblActionCancelled,
					expire: -1,
					type: "errormessage"
				});
                dhtmlx.alert(imgalertsavenok2 + locale.main_page.lblActionCancelled + "\n" + http.responseText);
				break;
		}
	}).fail(function (resp) {
	    dhtmlx.message({
	        text: "Error converting lead to event.",
	        expire: -1,
	        type: "errormessage"
	    });
	});    
}
function addEventToScheduler(scheduler, objLead) {
    
    
    scheduler.setEvent(999,{
        start_date: "25-02-2016 14:00",
        end_date: "25-02-2016 15:00",
        text: "Meeting"
    });

scheduler.setCurrentView();
}