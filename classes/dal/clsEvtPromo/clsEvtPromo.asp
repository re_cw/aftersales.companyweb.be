﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/obj/objLead.asp" -->
<!-- #include virtual ="/classes/obj/objCompany.asp" -->
<!-- #include virtual ="/classes/obj/objAddress.asp" -->
<!-- #include virtual ="/classes/obj/objContact.asp" -->

<% 
Response.CodePage = 65001    
Response.CharSet = "utf-8"
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_ADDR_TYP_MAIN = 4 'main address
CONST C_LEAD_DEFAULT_EVENT = 7 'CALL
CONST C_LEAD_DEFAULT_EVENT_TEXT = "CALL"
CONST C_DEFAULT_CONTACT_TYPE = 6 'general
CONST C_DEFAULT_CONTACT_GENDER = "M"
CONST C_DEFAULT_CONTACT_TITLE = 1 'Monsieur
CONST C_DEFAULT_CONTACT_LANG = 1 'FR
CONST C_DEFAULT_CONVERSION_MSG = "SYSTEM: Lead converted to Event by application"

Dim objLead, objCwCompany, objBoCompany, objLeadCompany, objDbCompany
Dim objCwAddress, objBoAddress, objLeadAddress, objDbAddress
Dim objCwContact, objBoContact, objLeadContact, objDbContact

Dim globerr
Dim objEvtPromo


    set objEvtPromo = new sysEvtPromo

    select case request.querystring("q")
        case 1 'promote Lead to Event

        response.write objEvtPromo.PromoteLeadToEvent()
        
        case 90
           objEvtPromo.TestpopulateObjBo()
        case 98 'unit testing
            objEvtPromo.unitTestingSetBestTimeFrame()
        case 99 'unit test
            objEvtPromo.UnitTestPromoteLeadToEvent()
    case else 'no default
        
    end select
    


class sysEvtPromo

    public function TestpopulateObjBo()
     createObjects()    
    'objLead.populateLeadObject() 
    'objLead.user_id=1
    'objLead.company_vat=439744649
    'objLead.backoffice_id=16949    
   
     populateObjBo()
    end function

    public function unitTestingSetBestTimeFrame()
    'createObjects()    
    'objLead.populateLeadObject() 
    'objLead.user_id=1    
    'response.write"<br>SetBestTimeFrame=" &  SetBestTimeFrame
    end function

     property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    Public function  PromoteLeadToEvent()
        cleanUpObjects()
        createObjects()
        objLead.populateLeadObject()    
        globerr = populateObjCw()
        populateObjBo()
        populateObjLeadCompany()
        populateObjdbToInsert()
        PromoteLeadToEvent = AddCompanyToDatabase()

    end function
Private Function IsNotNullorEmpty(input)
	dim IsNotNullorEmpty_Output
	IsNotNullorEmpty_Output = true
	'vbEmpty (uninitialized) or vbNull (no valid data)
	if VarType(input) = 0 OR VarType(input) = 1 Then
		IsNotNullorEmpty_Output = false
	End if
	If VarType(input) = 8 Then
		If input = "" Then 
			IsNotNullorEmpty_Output = False
		End If
	End If
	IsNotNullorEmpty = IsNotNullorEmpty_Output

End Function
    private function createObjects()
        set objLead = new clsObjLead
        set objCwCompany = new clsObjCompany
        set objBoCompany = new clsObjCompany
        set objLeadCompany = new clsObjCompany
        set objDbCompany = new clsObjCompany
    
        set objCwAddress = new clsObjAddress
        set objBoAddress = new clsObjAddress
        set objLeadAddress = new clsObjAddress
        set objDbAddress = new clsObjAddress

        set objCwContact = new clsObjcontact
        set objBoContact = new clsObjcontact
        set objLeadContact = new clsObjcontact
        set objDbContact = new clsObjcontact
    end function

    private function cleanUpObjects()
        set objLead = nothing
        set objEvtPromo = nothing
    
        set objCwCompany = nothing
        set objBoCompany = nothing
        set objLeadCompany = nothing
        set objDbCompany = nothing

        set objCwAddress = nothing
        set objBoAddress = nothing
        set objLeadAddress = nothing
        set objDbAddress = nothing

        set objCwContact = nothing
        set objBoContact = nothing
        set objLeadContact = nothing
        set objDbContact = nothing
    end function

    private function AddCompanyToDatabase()

        Dim objConn, objRS, strSql, blnNew, sErrMsg, objCmd
        blnNew=false
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if

    on error resume next
    objConn.BeginTrans
    
'*** insert/update company
        dim objRsCy, strSqlCy
        set objRsCy = Server.CreateObject("ADODB.Recordset")
        
        if  len(trim(objDbCompany.company_vat)) > 8 then
            strSqlCy = "SELECT company_id FROM dbo.companies WHERE company_vat=" & objDbCompany.company_vat
        else
            strSqlCy = "SELECT company_id FROM dbo.companies WHERE UPPER(company_name) LIKE '" & ucase(objDbCompany.company_name) & "'"
        end if    
        
        objRsCy.open strSqlCy, objConn, 0, 1   
            if objRsCy.bof and objRsCy.eof then strSql = "dbo.Company_ADD" else strSql = "dbo.Company_UPDATE"
            objRsCy.Close
        set objRsCy = nothing
            sErrMsg= sErrMsg & globerr & chr(10)
            sErrMsg= sErrMsg & chr(10) & " sqlcheck: " & strSqlCy
        'parameters types: '5: adDouble - '8: adBSTR - '201:adLongVarChar
        if objConn.State = 1 then      
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@company_vat", 8, 1, len(objDbCompany.company_vat), objDbCompany.company_vat)
                .Parameters.Append .CreateParameter("@backoffice_id", 8, 1, len(objDbCompany.company_hbc_id), objDbCompany.company_hbc_id)
                .Parameters.Append .CreateParameter("@company_name", 200, 1, len(objDbCompany.company_name), objDbCompany.company_name)
                .Parameters.Append .CreateParameter("@company_pref_lang_id", 5, 1, len(objDbCompany.company_pref_lang_id), objDbCompany.company_pref_lang_id)
                .Parameters.Append .CreateParameter("@jfc_code", 5, 1, len(objDbCompany.jfc_code), objDbCompany.jfc_code)
                .Parameters.Append .CreateParameter("@company_nace", 5, 1, len(objDbCompany.company_nace), objDbCompany.company_nace)
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@Company_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2, 1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then
                sErrMsg= sErrMsg & chr(10) & " insert/update company : " & err.description
                sErrMsg= sErrMsg & chr(10) & " strsql : " & strSql  
                sErrMsg= sErrMsg  & " company_vat : " & objDbCompany.company_vat   
                sErrMsg= sErrMsg & chr(10) & " : company_name:" & objDbCompany.company_name
                sErrMsg= sErrMsg & chr(10) & " : company_pref_lang_id:" & objDbCompany.company_pref_lang_id            
                sErrMsg= sErrMsg & chr(10) & " : jfc_code:" & objDbCompany.jfc_code
                sErrMsg= sErrMsg & chr(10) & " : company_nace:" & objDbCompany.company_nace
                sErrMsg= sErrMsg & chr(10) & " : user_id:" & objLead.user_id
                sErrMsg= sErrMsg & chr(10)
            end if
    
            objDbCompany.company_id=objCmd(8)
            set objRs= nothing
        end if


        '*** insert address
        dim objRsA, strSqlA
        set objRSA = Server.CreateObject("ADODB.Recordset")
        strSqlA = "SELECT company_address_id FROM dbo.company_address WHERE company_id=" & objDbCompany.company_id
        strSqlA = strsqlA & " AND address_type_id=" & objdbAddress.address_type_id 
        objRsA.open strSqlA, objConn,  0, 1
        if objRsA.bof and objRsA.eof then strSql = "dbo.Company_address_ADD" else strSql = "dbo.Company_address_UPDATE"
        objRsA.Close
        set objRsA = nothing
        'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@company_id", 5, 1, len(objDbCompany.company_id), objDbCompany.company_id)
                .Parameters.Append .CreateParameter("@address_type_id", 5, 1, len(objdbAddress.address_type_id), objdbAddress.address_type_id)
                .Parameters.Append .CreateParameter("@company_address_street", 200, 1, len(objdbAddress.company_address_street), objdbAddress.company_address_street)
                .Parameters.Append .CreateParameter("@company_address_number", 8, 1, len(objdbAddress.company_address_number), objdbAddress.company_address_number)
                .Parameters.Append .CreateParameter("@company_address_boxnumber", 8, 1, len(objdbAddress.company_address_boxnumber), objdbAddress.company_address_boxnumber)
                .Parameters.Append .CreateParameter("@company_address_floor", 8, 1, len(objdbAddress.company_address_floor), objdbAddress.company_address_floor)
                .Parameters.Append .CreateParameter("@company_address_postcode", 8, 1, len(objdbAddress.company_address_postcode), objdbAddress.company_address_postcode)
                .Parameters.Append .CreateParameter("@company_address_localite", 200, 1, len(objdbAddress.company_address_localite), objdbAddress.company_address_localite)
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@company_address_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2, 1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            
            if err.number <> 0 then sErrMsg= sErrMsg & "<br> Failure address<br>SQL: " & strsql &" : " &  err.description & "<br>"
                sErrMsg= sErrMsg  & " company_id : " & objDbCompany.company_id & "<br>"   
                sErrMsg= sErrMsg & chr(10) & " : address_type_id:" & objdbAddress.address_type_id & "<br>"
    sErrMsg= sErrMsg & chr(10) & " : le(company_address_street):" & len(objdbAddress.company_address_street) & "<br>"
                sErrMsg= sErrMsg & chr(10) & " : company_address_street:" & objdbAddress.company_address_street  & "<br>"           
                sErrMsg= sErrMsg & chr(10) & " : company_address_number:" & objdbAddress.company_address_number & "<br>"
                sErrMsg= sErrMsg & chr(10) & " : company_address_boxnumber:" & objdbAddress.company_address_boxnumber & "<br>"
                sErrMsg= sErrMsg & chr(10) & " : company_address_floor:" & objdbAddress.company_address_floor & "<br>"
                sErrMsg= sErrMsg & chr(10) & " : company_address_postcode:" & objdbAddress.company_address_postcode & "<br>"
                sErrMsg= sErrMsg & chr(10) & " : company_address_localite:" & objdbAddress.company_address_localite & "<br>"
                sErrMsg= sErrMsg & chr(10) & " : user_id:" & objLead.user_id
                sErrMsg= sErrMsg & chr(10)        
    'objRs.Close
            objdbAddress.company_address_id=objCmd(10)
            set objRs= nothing
        end if

'-----------------------------------------------------------------------------------------------------------------------------          
'*** <START REGION>insert contact        
''*** this works but is not activated as contact info are in general not provided

'        dim objRsC, strSqlC, sAnd
'        set objRSC = Server.CreateObject("ADODB.Recordset")
                
'        if len(trim(objDbContact.contact_firstname)) = 0 then objDbContact.contact_firstname=  objDbCompany.company_name
'        if len(trim(objDbContact.contact_lastname)) = 0 then objDbContact.contact_lastname= objDbCompany.company_name
        
'        strSqlC = "SELECT contact_id FROM dbo.contacts WHERE 1=1"    
'        sAnd= " AND UPPER(contact_firstname) LIKE '%" & ucase(objDbContact.contact_firstname) & "%'"
'        sAnd = sAnd & " AND UPPER(contact_lastname) LIKE '%" & ucase(objDbContact.contact_lastname) & "%'"
        
'        strSqlC = strSqlC & sAnd
   
'        objRsC.open strSqlC, objConn,  0, 1
'        if objRsC.bof and objRsC.eof then 
'            strSql = "dbo.Contact_ADD" 
'        else 
'            objDbContact.contact_id = objRsC.fields(0)
'            strSql = "dbo.Contact_UPDATE"    
'        end if
'
'        objRsC.Close
'        set objRsC = nothing
'  'response.write strSql
'        'parameters types: '5: adDouble - '8: adBSTR - '201:adLongVarChar
'        if objConn.State = 1 then    
'            set objCmd = Server.CreateObject("ADODB.Command")
'            with objCmd
'                .ActiveConnection = objConn
'                .CommandType = 4 'adCmdStoredProc
'                .CommandText =  strSql
'                .NamedParameters = True
'                .Parameters.Append .CreateParameter("@contact_HBU_ID", 5, 1, len(objDbContact.contact_HBU_ID), objDbContact.contact_HBU_ID)
'                .Parameters.Append .CreateParameter("@contact_lang_id", 5, 1, len(objDbContact.contact_lang_id), objDbContact.contact_lang_id)
'                .Parameters.Append .CreateParameter("@contact_title_id", 5, 1, len(objDbContact.contact_title_id), objDbContact.contact_title_id)
'                .Parameters.Append .CreateParameter("@contact_gender", 8, 1, len(objDbContact.contact_gender), objDbContact.contact_gender)
'                .Parameters.Append .CreateParameter("@contact_firstname", 8, 1, len(objDbContact.contact_firstname),  objDbContact.contact_firstname)
'                .Parameters.Append .CreateParameter("@contact_lastname", 8, 1, len(objDbContact.contact_lastname), objDbContact.contact_lastname)
'                .Parameters.Append .CreateParameter("@contact_tel", 8, 1, len(objDbContact.contact_tel), objDbContact.contact_tel)
'                .Parameters.Append .CreateParameter("@contact_mobile", 8, 1, len(objDbContact.contact_mobile), objDbContact.contact_mobile)
'                .Parameters.Append .CreateParameter("@contact_email", 8, 1, len(objDbContact.contact_email), objDbContact.contact_email)
'                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objLead.user_id), objLead.user_id)
'                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objLead.user_id), objLead.user_id)
'                .Parameters.Append .CreateParameter("@contact_id", 5, 2, len(objDbContact.contact_id), objDbContact.contact_id)
'                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
'                set objRS = Server.CreateObject("ADODB.Recordset")
'                    
'                    sErrMsg= sErrMsg & chr(10) & " *** insert/update contact : "
'                    sErrMsg= sErrMsg & chr(10) & " strsql : " & strSql
'                    sErrMsg= sErrMsg & chr(10) & " : contact_firstname:" & objDbContact.contact_firstname
'                    sErrMsg= sErrMsg & chr(10) & " : contact_lastname:" & objDbContact.contact_lastname            
'                    sErrMsg= sErrMsg & chr(10) & " : contact_lang_id:" & objDbContact.contact_lang_id
'                    sErrMsg= sErrMsg & chr(10) & " : contact_title_id:" & objDbContact.contact_title_id
'                    sErrMsg= sErrMsg & chr(10) & " : contact_gender:" & objDbContact.contact_gender
'                    sErrMsg= sErrMsg & chr(10) & " : contact_tel:" & objDbContact.contact_tel
'                    sErrMsg= sErrMsg & chr(10) & " : contact_mobile:" & objDbContact.contact_mobile
'                    sErrMsg= sErrMsg & chr(10) & " : contact_email:" & objDbContact.contact_email
'                    sErrMsg= sErrMsg & chr(10) & " : user_id:" & objLead.user_id
'                    sErrMsg= sErrMsg & chr(10)
'                objRS.CursorType = 0
'                objRS.lockType = 1
'                set objRS = .Execute()
'    
'        end with
'            'objRs.Close
'            if strSql = "dbo.Contact_ADD" then objDbContact.contact_id=objCmd(11)
'                if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"    
'            set objRs= nothing
'        end if
'
'        
'        '*** link contact to company address
'        dim objRsL, strSqlL, link_ID
'        set objRSL = Server.CreateObject("ADODB.Recordset")
'        strSqlL = "SELECT link_id FROM dbo.link_company_address_contacts WHERE company_address_id=" & objDbAddress.company_address_id
'        strSqlL = strSqlL & " AND contact_id=" & objDbContact.contact_id
'        objRsL.open strSqlL, objConn, 0, 1
'        if objRsL.bof and objRsL.eof then strSql = "dbo.link_company_address_contacts_ADD" else strSql = "dbo.link_company_address_contacts_UPDATE"
'        objRsL.close
'        set objRsL = nothing
'        'parameters types: '5: adDouble - '8: adBSTR
'        if objConn.State = 1 then    
'            set objCmd = Server.CreateObject("ADODB.Command")
'            with objCmd
'                .ActiveConnection = objConn
'                .CommandType = 4 'adCmdStoredProc
'                .CommandText =  strSql
'                .NamedParameters = True
'                .Parameters.Append .CreateParameter("@company_address_id", 5, 1, len(objDbAddress.company_address_id), objDbAddress.company_address_id)
'                .Parameters.Append .CreateParameter("@contact_id", 5, 1, len(objDbContact.contact_id), objDbContact.contact_id)
'                .Parameters.Append .CreateParameter("@contact_type_id", 5, 1, len(objDbContact.contact_type_id), objDbContact.contact_type_id)
'                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objLead.user_id), objLead.user_id)
'                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objLead.user_id), objLead.user_id)
'                .Parameters.Append .CreateParameter("@link_id", 5, 2)
'                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
'                set objRS = Server.CreateObject("ADODB.Recordset")
'                objRS.CursorType = 0
'                objRS.lockType = 1
'                set objRS = .Execute()
'        end with
'           if err.number <> 0 then sErrMsg= sErrMsg & strSqlL & " " & strsql &" : " &  err.description & "<br>"
'            'objRs.Close
'            set objRs= nothing
'            link_ID=objCmd(5)
'        end if
'
''*** <END REGION>insert contact  
''---------------------------------------------------------------------------------------------------------------


        '*** insert default CALL Event into user's agenda
        dim objRsE, strSqlE
        set objRSE = Server.CreateObject("ADODB.Recordset")
        strSqlE = "SELECT id FROM dbo.events WHERE usr_id=" & objLead.user_id & " AND company_id=" & objDbCompany.company_id
        objRsE.open strSqlE, objConn, 0, 1
        if objRsE.bof and objRsE.eof then strSql = "dbo.Events_ADD" else strSql = "dbo.Events_UPDATE"
        objRsE.close
        set objRsE = nothing

        Dim evtStartDate, evtEndDate, eventID
        evtStartDate =  SetBestTimeFrame()
        evtEndDate =  GetNextTimeFrame(evtStartDate, 5)
       
        'parameters types: '5: adDouble - '8: adBSTR -'7: adDate 
        if objConn.State = 1 then
            Dim strDefaultText
            strDefaultText= objDbCompany.company_name & " : " & C_LEAD_DEFAULT_EVENT_TEXT
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@event_type_id", 5, 1, len(C_LEAD_DEFAULT_EVENT), C_LEAD_DEFAULT_EVENT)
                .Parameters.Append .CreateParameter("@company_id", 5, 1, len(objDbCompany.company_id), objDbCompany.company_id)
                .Parameters.Append .CreateParameter("@text", 8, 1, len(strDefaultText), strDefaultText)
                .Parameters.Append .CreateParameter("@start_date", 7, 1, len(evtStartDate), evtStartDate)
                .Parameters.Append .CreateParameter("@end_date", 7, 1, len(evtEndDate), evtEndDate)
                .Parameters.Append .CreateParameter("@usr_id", 5, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"        
            'objRs.Close
            set objRs= nothing
            eventID=objCmd(8)
        end if
 
        '*** insert initial comment into comment table (Lead converted to Event by application on [CALL button] trigger)
    if objConn.State = 1 then
            strSql="dbo.Comments_ADD"
            Dim comment, comment_date, comment_time, comment_ID
            comment = C_DEFAULT_CONVERSION_MSG
            comment_date = SetDate()
            comment_time = SetTime()
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@comment_event_type_id", 5, 1, len(C_LEAD_DEFAULT_EVENT), C_LEAD_DEFAULT_EVENT)
                .Parameters.Append .CreateParameter("@comment_usr_id", 5, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@comment_event_id", 8, 1, len(eventID), eventID)
                .Parameters.Append .CreateParameter("@comment_lead_id", 8, 1, len(objLead.lead_id), objLead.lead_id)
                .Parameters.Append .CreateParameter("@comment_company_vat", 8, 1, len(objDbCompany.company_vat), objDbCompany.company_vat)
				.Parameters.Append .CreateParameter("@comment_company_id", 8, 1, len(objDbCompany.company_id), objDbCompany.company_id)
                .Parameters.Append .CreateParameter("@comment_date", 5, 1, len(comment_date), comment_date)
                .Parameters.Append .CreateParameter("@comment_time", 8, 1, len(comment_time), comment_time)
                .Parameters.Append .CreateParameter("@comment", 8, 1, len(comment), comment)

                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objLead.user_id), objLead.user_id)
                .Parameters.Append .CreateParameter("@comment_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
            'objRs.Close
            set objRs= nothing
            comment_ID=objCmd(10)
        end if      




        '*** update Lead to disable it from the list (enabled=false) use a staightforward sql update statement
        dim strSqlD
                ', AuditDateUpdated='" & now() & "'
        strSqlD = "UPDATE dbo.leads SET enabled=0 ,AuditUserUpdated=" & objLead.user_id & " WHERE lead_id=" & objLead.lead_id
        objConn.execute(strSqlD)

        if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
        
        if Err.number = 0 then
            objConn.CommitTrans
            AddCompanyToDatabase = 1
    
        else
            objConn.RollbackTrans
            AddCompanyToDatabase = sErrMsg
        end if

    objConn.Close
    Set objConn = nothing
    cleanUpObjects()

    end function


    private function SetBestTimeFrame()
        Dim oConn, objRS, strSql, blnNew, tmpDate
        
        set oConn=Server.CreateObject("ADODB.Connection")
        with oConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if oConn.State = 0 then exit function
        set objRS = Server.CreateObject("ADODB.Recordset")
        Dim eventStartDate
        eventStartDate = roundtime(now(), 5)
                   
        strSql = "SELECT events.*, event_types.event_type_duration_min FROM event_types INNER JOIN events ON event_types.event_type_id = events.event_type_id "
        strSql = strSql & " WHERE usr_id=" & objLead.user_id & " AND (start_date >='" & eventStartDate  & "' OR end_date >='" & eventStartDate & "') ORDER BY start_date"
  
        'response.write strsqL

        objRS.open strSql, oConn, 0, 1
        with objRs
            if not .bof and not .eof then
                do while not .eof        
                    if cdate(.fields("start_date")) <= cdate(eventStartDate) then
                        eventStartDate = .fields("end_date")                
                        tmpDate = eventStartDate
                    else
                        tmpDate =eventStartDate
                        exit do
                    end if
                .movenext
                loop
            else
                tmpDate =eventStartDate
            end if
        end with
    
        SetBestTimeFrame=cdate(roundtime(tmpDate,5))
        objRS.close
        set objRS = nothing
        oConn.Close
        Set oConn = nothing
    
    end function



    private function GetNextTimeFrame(vTime, nbMinutes)   
        'ex: NbMinutes= 5 -> get next +5mins timeframe
        GetNextTimeFrame = dateadd("n",nbMinutes ,vTime)
    end function

    private function GetWeekDay(theDate)
        do while not Weekday(theDate,vbMonday) <= 5
            theDate= DateAdd("d",1,theDate)        
        loop
        GetWeekDay = year(theDate) & "-" & month(theDate) & "-" & day(theDate)
    end function

    private function roundtime(vtime, NbMinutes) 
    'nbMinutes round to x minutes, ex: NbMinutes= 5 -> round to next 5minutes
        v_month = month(vtime)
        v_day = day(vtime)
        v_year = year(vtime)
        v_hour = hour(vtime)
        v_minute = round(minute(vtime)/NbMinutes) * NbMinutes
        if v_minute >= 60 then
            v_minute = 0
			v_hour = v_hour + 1
		end if
		
        if v_hour > 17 then  '18 (old value:24) because we must fix these values according to opening hours in agenda : 08:00 & 18:00
			v_hour = 9       '8' (old value:0)   
			v_day = v_day + 1        
        end if
        roundtime = GetWeekDay(v_year & "-" & v_month & "-" & v_day) & " " & v_hour & ":" & v_minute & ":00"
        'roundtime = cdate(v_year & "-" & v_month & "-" & v_day & " " & v_hour & ":" & v_minute & ":00")
    end function



    private function populateObjdbToInsert()
      
        objDbCompany.company_name = chooseValue(objCwCompany.company_name, objBoCompany.company_name, objLeadCompany.company_name)
        objDbCompany.company_hbc_id = chooseValue(objCwCompany.company_hbc_id, objBoCompany.company_hbc_id, objLeadCompany.company_hbc_id)
        objDbCompany.company_vat = chooseValue(objCwCompany.company_vat, objBoCompany.company_vat, objLeadCompany.company_vat)
        objDbCompany.company_pref_lang_id = chooseValue(objCwCompany.company_pref_lang_id, objBoCompany.company_pref_lang_id, objLeadCompany.company_pref_lang_id)
        objDbCompany.jfc_code = chooseValue(objCwCompany.jfc_code, objBoCompany.jfc_code, objLeadCompany.jfc_code)
        objDbCompany.company_nace = chooseValue(objCwCompany.company_nace, objBoCompany.company_nace, objLeadCompany.company_nace)

        objDbAddress.address_type_id = chooseValue(objCwAddress.address_type_id, objBoAddress.address_type_id, objLeadAddress.address_type_id)
        objDbAddress.company_address_street = chooseValue(objCwAddress.company_address_street, objBoAddress.company_address_street, objLeadAddress.company_address_street)
        objDbAddress.company_address_number = chooseValue(objCwAddress.company_address_number, objBoAddress.company_address_number, objLeadAddress.company_address_number)
        objDbAddress.company_address_boxnumber = chooseValue(objCwAddress.company_address_boxnumber, objBoAddress.company_address_boxnumber, objLeadAddress.company_address_boxnumber)
        objDbAddress.company_address_floor = chooseValue(objCwAddress.company_address_floor, objBoAddress.company_address_floor, objLeadAddress.company_address_floor)
        objDbAddress.company_address_postcode = chooseValue(objCwAddress.company_address_postcode, objBoAddress.company_address_postcode, objLeadAddress.company_address_postcode)
        objDbAddress.company_address_localite = chooseValue(objCwAddress.company_address_localite, objBoAddress.company_address_localite, objLeadAddress.company_address_localite)

        objDbContact.contact_firstname = chooseValue(objCwContact.contact_firstname, objBoContact.contact_firstname, objLeadContact.contact_firstname)
        objDbContact.contact_lastname = chooseValue(objCwContact.contact_lastname, objBoContact.contact_lastname, objLeadContact.contact_lastname)
        objDbContact.contact_gender = C_DEFAULT_CONTACT_GENDER 'chooseValue(objCwContact.contact_gender, objBoContact.contact_gender, objLeadContact.contact_gender)
        objDbContact.contact_tel = chooseValue(objCwContact.contact_tel, objBoContact.contact_tel, objLeadContact.contact_tel)
        objDbContact.contact_mobile = chooseValue(objCwContact.contact_mobile, objBoContact.contact_mobile, objLeadContact.contact_mobile)
        objDbContact.contact_email = chooseValue(objCwContact.contact_email, objBoContact.contact_email, objLeadContact.contact_email)
        objDbContact.contact_vat = chooseValue(objCwContact.contact_vat, objBoContact.contact_vat, objLeadContact.contact_vat)
        
        objDbContact.contact_HBU_ID = chooseValue(objCwContact.contact_HBU_ID, objBoContact.contact_HBU_ID, objLeadContact.contact_HBU_ID)
        objDbContact.contact_lang_id = chooseValue(objCwContact.contact_lang_id, objBoContact.contact_lang_id, objLeadContact.contact_lang_id)
        objDbContact.contact_type_id = C_DEFAULT_CONTACT_TYPE    
        objDbContact.contact_title_id = C_DEFAULT_CONTACT_TITLE

        objDbCompany.company_name = escapeSingleQuote(objDbCompany.company_name)
        objDbAddress.company_address_street = escapeSingleQuote(objDbAddress.company_address_street)
        objDbAddress.company_address_localite = escapeSingleQuote(objDbAddress.company_address_localite)
        objDbContact.contact_firstname = escapeSingleQuote(objDbContact.contact_firstname)
        objDbContact.contact_lastname = escapeSingleQuote(objDbContact.contact_lastname)
    
    objDbContact.contact_tel = castnumbersOnly(objDbContact.contact_tel)
    objDbContact.contact_mobile = castnumbersOnly(objDbContact.contact_mobile)


    globerr=globerr & "objDbCompany:"  & objDbCompany.company_name &  chr(10)
    globerr=globerr & "objDbCompany:"  & objDbContact.contact_tel &  chr(10)
    end function

    private function chooseValue(valCw, valBo, valLead)
    dim err
        if len(trim(valCw)) > 0 then 
            chooseValue = valCw
            'globerr=globerr & "picked valCw:" & valCw & chr(10)
            exit function
        elseif len(trim(valBo)) > 0 then
            chooseValue = valBo
            'globerr=globerr & "picked valBo:" & valBo & chr(10)
            exit function
        elseif len(trim(valLead)) > 0 then
            chooseValue = valLead
            'globerr=globerr & "picked valLead:" & valLead & chr(10)
        end if

    'globerr=globerr & err
    end function

    private function escapeSingleQuote(varval)
        if len(trim(varval)) > 0 then
            escapeSingleQuote = replace(varval, "'", "''")
        end if
    end function

    private function populateObjLeadCompany()
        objLeadCompany.user_id = objLead.user_id
        objLeadCompany.company_id = objLead.company_id
        objLeadCompany.company_hbc_id = objLead.backoffice_id
        objLeadCompany.company_name = objLead.company_name
        objLeadCompany.company_vat = objLead.company_vat
        objLeadCompany.company_address_street = objLead.company_address_street
        objLeadCompany.company_address_number = objLead.company_address_number
        objLeadCompany.company_address_postcode = objLead.company_address_postcode
        objLeadCompany.company_address_localite = objLead.company_address_localite
        
    populateObjLeadAddress()
    populateObjLeadContact()
    end function

    private function populateObjLeadAddress()
        objLeadAddress.address_type_id = C_ADDR_TYP_MAIN
        objLeadAddress.company_address_street = objLead.company_address_street
        objLeadAddress.company_address_number = objLead.company_address_number
        objLeadAddress.company_address_boxnumber = objLead.company_address_boxnumber
        objLeadAddress.company_address_floor = objLead.company_address_floor
        objLeadAddress.company_address_postcode = objLead.company_address_postcode
        objLeadAddress.company_address_localite = objLead.company_address_localite
    end function

    private function populateObjLeadContact()
        objLeadContact.contact_company_id = objLead.company_id
        objLeadContact.contact_tel = objLead.company_tel
        objLeadContact.contact_mobile = objLead.company_mobile
        objLeadContact.contact_email  = objLead.company_mail
        objLeadContact.contact_lang_id = C_DEFAULT_CONTACT_LANG
    end function


    private function populateObjBo()
        Dim a, b, c
            b = userlang()
            
        Dim jfc_s_desc, jfc_l_desc, nbc_desc, HBcon_Name
            jfc_s_desc = "jfc_s_desc_" + b
            jfc_l_desc = "jfc_l_desc_" + b
            nbc_desc = "nbc_desc_" + b

        Dim objConn, objCmd, objParam, strSql, objRsBo
        strSql = "dbo.ST_GetBackOfficeCompanyInfo"    
        a = objLead.company_vat
        c = objLead.backoffice_id
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .CursorLocation = 3
            .Open    
        end with
		
        'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then 
			if (IsNotNullorEmpty(c)) Then
				c = "0"
			end if
			
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@vatcode", 8, 1, len(a), a)
                .Parameters.Append .CreateParameter("@hbc_id", 5, 1, len(c), c)
            on error resume next
                set objRsBo = Server.CreateObject("ADODB.Recordset")
                objRsBo.CursorType = 0
                objRsBo.lockType = 1
                set objRsBo = .Execute()
             if err.number <> 0 then populateObjBo = "populateObjBo:" & err.Description   
            end with

            objBoCompany.company_hbc_id = objRsBo.fields("HBC_ID")
            objBoCompany.company_name = objRsBo.fields("HBC_FirmName")
            objBoCompany.company_vat = objRsBo.fields("HBC_Firmvat")
            objBoCompany.company_address_street = objRsBo.fields("HBC_Street")
            objBoCompany.company_address_number = objRsBo.fields("HBC_HouseNr")
            objBoCompany.company_address_boxnumber = objRsBo.fields("HBC_BusNr")
            objBoCompany.company_address_postcode = objRsBo.fields("HBC_Postcode")
            objBoCompany.company_address_localite = objRsBo.fields("HBC_gemeente")
            objBoCompany.jfc_code = objRsBo.fields("hbc_jurvormid")
            objBoCompany.company_jurForm = objRsBo.fields(jfc_s_desc)
            objBoCompany.company_pref_lang_id = objRsBo.fields("HBC_intLang")          
 
            objBoAddress.address_type_id = C_ADDR_TYP_MAIN
            objBoAddress.company_address_street = objBoCompany.company_address_street
            objBoAddress.company_address_number = objBoCompany.company_address_number
            objBoAddress.company_address_boxnumber = objBoCompany.company_address_boxnumber
            objBoAddress.company_address_floor = objBoCompany.company_address_floor
            objBoAddress.company_address_postcode = objBoCompany.company_address_postcode
            objBoAddress.company_address_localite = objBoCompany.company_address_localite
           
            objBoContact.contact_firstname = objRsBo.fields("HBC_ContactName")
            objBoContact.contact_lastname = objRsBo.fields("HBC_ContactName")
            objBoContact.contact_tel = objRsBo.fields("HBC_Tel")
            objBoContact.contact_mobile = objRsBo.fields("HBC_gsm")
            objBoContact.contact_email = objRsBo.fields("HBC_contactemail")
            objBoContact.contact_vat = objRsBo.fields("HBC_Firmvat")
            objBoContact.contact_lang_id = objRsBo.fields("HBC_intLang")

            objRsBo.close
            set objRsBo = nothing
            objConn.close
            set objConn = nothing
        end if
    end function

    private function populateObjCw()
        Dim a, b
            b = userlang()
 
        Dim jfc_s_desc, jfc_l_desc, nbc_desc, gc_desc
                jfc_s_desc = "jfc_s_desc_" + b
                jfc_l_desc = "jfc_l_desc_" + b
                nbc_desc = "nbc_desc_" + b
                gc_desc = "gc_desc_" + b    

            Dim objConn, objCmd, objParam,  strSql, objRsCw 
            strSql = "dbo.ST_GetCompanyWebCompanyInfo"    
            a = objLead.company_vat

        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .CursorLocation = 3
            .Open    
        end with

        'parameters types: '5: adDouble - '8: adBSTR
        if objConn.State = 1 then    
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@vatcode", 8, 1, len(a), a)            
            on error resume next
                set objRsCw = Server.CreateObject("ADODB.Recordset")
                objRsCw.CursorType = 0
                objRsCw.lockType = 1
                set objRsCw = .Execute()
            if err.number <> 0 then populateObjCw ="populateObjCw: " &  err.Description
            end with

                objCwCompany.company_name = objRsCw.fields("fu_Name")
                objCwCompany.company_vat = objRsCw.fields("fu_VAT")
                objCwCompany.company_address_street = objRsCw.fields("fu_StraatNaam")
                objCwCompany.company_address_number = objRsCw.fields("fu_HuisNr")
                objCwCompany.company_address_boxnumber = objRsCw.fields("fu_BusNr")
                objCwCompany.company_address_postcode = objRsCw.fields("fu_PostCode")
                objCwCompany.company_address_localite = objRsCw.fields("fu_Gemeente")
                objCwCompany.jfc_code = objRsCw.fields("F_CurrentJurVorm")
                objCwCompany.company_jurForm = objRsCw.fields(jfc_s_desc)
                objCwCompany.company_nace = objRsCw.fields("F_HoofdNACE")
                objCwCompany.company_pref_lang_id = objRsCw.fields("f_PrefLangID")

                objCwAddress.address_type_id = C_ADDR_TYP_MAIN
                objCwAddress.company_address_street = objCwCompany.company_address_street
                objCwAddress.company_address_number = objCwCompany.company_address_number
                objCwAddress.company_address_boxnumber = objCwCompany.company_address_boxnumber
                objCwAddress.company_address_floor = objCwCompany.company_address_floor
                objCwAddress.company_address_postcode = objCwCompany.company_address_postcode
                objCwAddress.company_address_localite = objCwCompany.company_address_localite

                objCwContact.contact_firstname = objRsCw.fields("FCo_Name")
                objCwContact.contact_lastname = objRsCw.fields("FCo_Name")
                objCwContact.contact_tel = objRsCw.fields("FCo_Tel")
                objCwContact.contact_mobile = objRsCw.fields("FCo_GSM")
                objCwContact.contact_email = objRsCw.fields("FCo_Email")
                objCwContact.contact_vat = objRsCw.fields("FCo_Vat")
                objCwContact.contact_lang_id = objRsCw.fields("FCo_LangID")

            objRsCw.close
            set objRsCw = nothing
            objConn.close
            set objConn = nothing
    end if
    
    end function


    public function castnumbersOnly(s)
    Dim re, oRegExp
        re = "[^0-9]"
        Set oRegExp = New RegExp
        oRegExp.Global = True
        oRegExp.Pattern = re

        s = oRegExp.Replace(s, "")
        castnumbersOnly = s 
    end function

    public function escapeMyShit(myval)
        escapeMyShit=replace(myval, "'","''")
    end function

    public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    public function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function

'<end region PRIVATE METHODS & FUNCTIONS>
    public sub Class_Terminate()
        set objLead = nothing
        set objEvtPromo = nothing
    
        set objCwCompany = nothing
        set objBoCompany = nothing
        set objLeadCompany = nothing
        set objDbCompany = nothing

        set objCwAddress = nothing
        set objBoAddress = nothing
        set objLeadAddress = nothing
        set objDbAddress = nothing

        set objCwContact = nothing
        set objBoContact = nothing
        set objLeadContact = nothing
        set objDbContact = nothing
    
    end sub

end class
%>