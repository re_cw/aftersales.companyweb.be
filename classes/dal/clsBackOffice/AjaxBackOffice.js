function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value.replace(/(?:\r\n|\r|\n|\n\r)/g, '<br />');;
}

function populateobjLead(objLead) {
    var params = "lead_id=" + encodeURIComponent(objLead.lead_id);
    params += "&user_id=" + encodeURIComponent(objLead.user_id);
    params += "&status_id=" + encodeURIComponent(objLead.status_id);
    params += "&lang_id=" + encodeURIComponent(objLead.lang_id);
    params += "&source_id=" + encodeURIComponent(objLead.source_id);
    params += "&lead_type_id=" + encodeURIComponent(objLead.lead_type_id);
    params += "&backoffice_id=" + encodeURIComponent(objLead.backoffice_id);
    params += "&company_id=" + encodeURIComponent(objLead.company_id);
    params += "&prompt_date=" + encodeURIComponent(objLead.prompt_date);
    params += "&prompt_time=" + encodeURIComponent(objLead.prompt_time);
    params += "&company_name=" + encodeURIComponent(objLead.company_name);
    params += "&company_vat=" + encodeURIComponent(objLead.company_vat);
    params += "&company_address_street=" + encodeURIComponent(objLead.company_address_street);
    params += "&company_address_number=" + encodeURIComponent(objLead.company_address_number);
    params += "&company_address_postcode=" + encodeURIComponent(objLead.company_address_postcode);
    params += "&company_address_localite=" + encodeURIComponent(objLead.company_address_localite);
    params += "&company_tel=" + encodeURIComponent(objLead.company_tel);
    params += "&company_mobile=" + encodeURIComponent(objLead.company_mobile);
    params += "&company_mail=" + encodeURIComponent(objLead.company_mail);
    params += "&HBCompanyNotes=" + encodeURIComponent(objLead.HBCompanyNotes);
    params += "&new_user_id=" + encodeURIComponent(objLead.new_user_id);
    params += "&trusted=1";

    return params;
}

function populateobjBoUser(objrequest) {
    var params = "ID=" + encodeURIComponent(objrequest.ID);
    params += "&HBC_ID=" + encodeURIComponent(objHBCy.HBCyDetail[0].HBC_ID);
    params += "&LastUpdateUser=" + encodeURIComponent(objrequest.LastUpdateUser);
    params += "&container=" + encodeURIComponent(objrequest.LastUpdateUser);
    return params;
}

function JSONGetCompanyInfoFromCompanywebPromise(VAT) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp"
    return $.get(url, { q: 70, vat: VAT })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Failed getting CompanywebInfo from Companyweb",
            expire: -1,
            type: "errormessage"
        });
    });
}

function JSONGetCompanyBackOfficeMobileInfo(HBC_ID) {
		$("#objBoMobileInfo").empty();
		
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp"
		$.get(url, { q: 30, HBC_ID: HBC_ID })
		.done(function (data) {
			$("#objBoMobileInfo").html(SetBoMobileInfoToTable(data));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Backoffice Mobile Info.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#objBoMobileInfo").data('requestRunning',false);
		});
}

function JSONGetCompanyBackOfficeUserInfoPromise(vatnumber, HBC_ID, Usercontainer) {
		$("#" + Usercontainer).empty();
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp"
	
		return $.get(url, { q: 5, vat: vatnumber, HBC_ID: HBC_ID })
		.done(function (data) {
			JSONGetCompanyBackOfficeUserHitsInfo(HBC_ID, data);
			JSONGetCompanyBackOfficeMobileInfo(HBC_ID);
	
			$("#" + Usercontainer).append(SetBoUserInfoToTable(data));
			
		})
		.fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Backoffice UserInfo.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + Usercontainer).data('requestRunning',false);
		});
}

function JSONGetCompanyBackOfficeUserBillingInfoPromise(vatnumber, HBC_ID, container) {
		$("#" + container).empty();
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
	
		return $.get(url, { q: 6, vat: vatnumber, HBC_ID: HBC_ID })
		.done(function (data) {
			if (!$.isEmptyObject(data)) {
				$("#" + container).html(SetBoUserBillingInfoToTable(data));
			}
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Backoffice Billing Info.",
				expire: -1,
				type: "errormessage"
			});
			
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

function JSONGetCompanyBackOfficeUserLoginInfoPromise(HBC_ID) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
    return $.get(url, { q: 7, HBC_ID: HBC_ID })
        .fail(function (resp) {
        dhtmlx.message({
            text: "Error getting Backoffice UserLogin Info.",
            expire: -1,
            type: "errormessage"
        });
    });
}

function JSONCompanyBackOfficeUserCancelTrialPromis(HBU_ID) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    return $.get(url, { q: 14, HBU_ID: HBU_ID })
    .fail(function () {
        dhtmlx.message({
            text: "Error Cancelling User Trial.",
            expire: -1,
            type: "errormessage"
        });
    });
}

function JSONGetCompanyBackOfficeUserHitsInfo(HBC_ID, objBoUser) {
		$("#objBoUserHits").empty();
		
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
		$.get(url, { q: 8, HBC_ID: HBC_ID })
		.done(function (data) {
			$("#objBoUserHits").html(SetBoUserHitsInfoToTable(data, objBoUser));
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting CompanyBackoffice UserHits Info.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#objBoUserHits").data('requestRunning',false);
		});
}

function JSONGetCompanyVatFromBackOfficeId(hbc_id)
{
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    return $.get(url, { q: 80, HBC_ID: hbc_id })
    .fail(function (data) {
        dhtmlx.message({
            text: "Error getting CompanyVat from BackOffice",
            expire: -1,
            type: "errormessage"
        });
    });
}

function JSONGetCompanyBackOfficeUserLogDetailsPromis(HBC_ID) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";

    return $.get(url, { q: 9, HBC_ID: HBC_ID })
    .fail(function (data) {
        dhtmlx.message({
            text: "Error getting Backoffice User Log Details.",
            expire: -1,
            type: "errormessage"
        });
    });
}

function JSONGetCompanyBackOfficeUserLogHitPagesPromis(HBC_ID, selectedDate) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
    return $.get(url, { q: 10, HBC_ID: HBC_ID, selectedDate: selectedDate })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error getting Backoffice Users logs",
            expire: -1,
            type: "errormessage"
        });
    });
}

var displayMailEditor
function openWindowMail(mail_id)
{
    var formName = "frmMail" + mail_id;
    var objWindows;
    var thisWindow;
    var formStructure = '';
    var x = 20;
    var y = 20;
    var width = 780;
    var height = 680;

    objWindows = new dhtmlXWindows();
    thisWindow = objWindows.createWindow(formName, x, y, width, height);
    objWindows.attachViewportTo(document.body);
    thisWindow.center();
    thisWindow.maximize();
    objWindows = thisWindow.attachLayout("1C");

    thisWindow.attachEvent("onClose", function (win) {
        return true;
    });

    JSONGetEmailByIdPromise(mail_id)
    .done(function (data) {
        var headertext = data.Subject;
        thisWindow.setText(headertext);

        objWindows.cells("a").setText("Info");
        objWindows.cells("a").showInnerScroll();

        objWindows.cells("a").attachHTMLString(MM_DisplayMailEngineSetLayout());

        $("#MailTo").val(data.MailTo).prop('disabled', true);
        $("#Subject").val(data.Subject).prop('disabled', true);
        $("#MailBCC").val(data.MailBCC).prop('disabled', true);
        $("#MailCC").val(data.MailCC).prop('disabled', true);

		HTMLGetBodyByIdPromise(mail_id)
		.done(function(html)
		{
			var decodedHtml = html;
			
		    displayMailEditor = new dhtmlXEditor({
				parent: "displayMailObj",
				toolbar: true,
				iconsPath: "/graphics/codebase/imgs/",
				content: decodedHtml
			});
			displayMailEditor.setReadonly(true);	
		});			
    });
    
}
function openWindowMailLogs(hbc_id)
{

}

function openWindowDetailLogs(hbc_id) {
    //dhtmlx.alert((objBoUser[0].HBC_ID));
    var headerText = locale.main_page.lblhits;
    var formName = "cp";
    var objWindows;
    var thisWindow;
    var formStructure = '';
    var x = 20;
    var y = 20;
    var width = 780;
    var height = 680;

    objWindows = new dhtmlXWindows();
    thisWindow = objWindows.createWindow(formName, x, y, width, height);
    objWindows.attachViewportTo(document.body);
    objWindows.window(formName).setText(headerText);
    objWindows.window(formName).center();
    objWindows = thisWindow.attachLayout("2U")
    objWindows.cells("a").setWidth(180);

    objWindows.cells("a").setText(locale.main_page.lblDate);
    objWindows.cells("b").setText(locale.labels.icon_details);
    objWindows.cells("a").showInnerScroll();
    objWindows.cells("b").showInnerScroll();

    objWindows.cells("a").attachHTMLString('<div id=dategrid  style="width:150px;height:600px;"></div>');
    objWindows.cells("b").attachHTMLString('<div id=detailgrid  style="width:100%;height:600px;"></div>');
    InitializeDateGrid();
    InitializeDetailGrid();

    JSONGetCompanyBackOfficeUserLogDetailsPromis(hbc_id)
    .done(function (data) {
        if (data.rows.length > 0) {
            DateGrid.parse(data, "json");
        }
    });
}

var DateGrid;
function InitializeDateGrid() {
    DateGrid = new dhtmlXGridObject('dategrid');
    DateGrid.setImagePath("/graphics/common/win_16x16/");
    DateGrid.setHeader(locale.labels.date + ',Hits,cId');
    DateGrid.setColumnHidden(2, true);
    DateGrid.setInitWidths("90,40,40");
    DateGrid.setColAlign("left,left,left");
    DateGrid.setColTypes("ro,ro,ro");
    DateGrid.setColSorting("str,int, int");
    DateGrid.init();
    DateGrid.clearAll();
    DateGrid.attachEvent("onRowSelect", function (id, ind) {
        DetailGrid.clearAll();
        var hcb_id = DateGrid.cells(id, 2).getValue();
        JSONGetCompanyBackOfficeUserLogHitPagesPromis(hcb_id, id)
        .done(function (data) {
            DetailGrid.parse(data, "json");
        }).fail(function (response) {
            dhtmlx.message({
                text: "Error initializing DateGrid.",
                expire: -1,
                type: "errormessage"
            });
        });
    });
}

var DetailGrid;
function InitializeDetailGrid() {
    DetailGrid = new dhtmlXGridObject('detailgrid');
    DetailGrid.setImagePath("/graphics/common/win_16x16/");
    DetailGrid.setHeader(locale.labels.date + ',Login,File,Hits');
    DetailGrid.setInitWidths("90,160,250,40");
    DetailGrid.setColAlign("left,left,left,left");
    DetailGrid.setColTypes("ro,ro,ro,ro");
    DetailGrid.setColSorting("str,str,str,int");
    DetailGrid.init();
    DetailGrid.clearAll();
}

function setAccLevelDisplay(levelid) {
    var s = '>';
    if (levelid !== "0")
    { s = ' class=acclevellocked>&nbsp;&nbsp;<img src =/graphics/common/win_16x16/Locked.png>&nbsp;&nbsp;' + locale.main_page.acclevellocked }
    else { s = ' class=acclevelunlocked>&nbsp;&nbsp;<img src =/graphics/common/win_16x16/unLocked.png>&nbsp;&nbsp;' + locale.main_page.acclevelunlocked }
    return s;
}

function setFlag(v) {
    var s = '&nbsp;&nbsp;<img src =/graphics/common/win_16x16/flag-red.png>';
	if (!$.isEmptyObject(v))
	{
		if (v !== "0")
		{ 
			s = '&nbsp;&nbsp;<img src =/graphics/common/win_16x16/flag-green.png>';
		}
	}
	
    return s;
}

function SetBoUserInfoToTable(objBoUser) {
    var t = '<table width=98%  class=BOInfo border=0>';
    t += '<tr><td class="BOCyName">' + locale.main_page.lblEventBackOfficeUserInfo + '</td></tr>';
    t += '<tr><td valign=top>';
    t += SetBoUserTrialInfoToTable(objBoUser);
    t += '</td></tr>';
    t += '<tr><td valign=top>';
    t += SetBoUserLoginInfoToTable(objBoUser);
    t += '</td></tr>';
    t += '<tr><td valign=top>';
    t += '<div id="objBoUserHits"></div>';
    t += '<div id="objBoMobileInfo"></div>';
    t += '</td></tr>';

    t += '</table>';

    return t;
}

function SetBoUserTrialInfoToTable(objBoUser) {
    var accLevelDisplay;

    var t = '<table width=100%  class=commontitle border=0>';

    for (var i = 0; i < objBoUser.length; i++) {
        var endWebSite = formatEuroDate(objBoUser[i].Einde_Website);

        t += '<tr><td colspan=6 class="commontitle">' + locale.main_page.lbltrial + '</td></tr>';
        t += '<tr><td colspan=6 class=nakedtitle>' + locale.main_page.lblAccLvlInfoCom + '</td></tr>';
        t += '<tr><td colspan=6' + setAccLevelDisplay(objBoUser[i].AccLvl_BLOCKED); + '</td></tr>';

        t += '<tr>'
        t += '<td class=nakedcellheader width=15%>' + locale.main_page.lbloption + '</td>';
        t += '<td class=nakedcellheader width=30px>' + locale.main_page.lblstatus + '</td>';
        t += '<td class=nakedcellheader>' + locale.main_page.lblNbWeek + '</td>';
        t += '<td class=nakedcellheader width=15%>' + locale.main_page.lbloption + '</td>';
        t += '<td class=nakedcellheader width=30px>' + locale.main_page.lblstatus + '</td>';
        t += '<td class=nakedcellheader>' + locale.main_page.lblNbWeek + '</td>';
        t += '</tr>';

        t += '<tr>'
        t += '<td class=nakedtitle>' + locale.main_page.lblWebSite + '</td>'
        t += '<td>' + setFlag(objBoUser[i].AccLvl_Website); + '</td>';
        t += '<td> [ ' + objBoUser[i].Aantal_Website + ' ] ' + locale.main_page.lblstoppedon + ' : ' + endWebSite + '</td>';
        t += '<td class=nakedtitle>' + locale.main_page.lblManagement + '</td>'
        t += '<td>' + setFlag(objBoUser[i].AccLvl_Mandaten); + '</td>'
        t += '<td></td>'
        t += '</tr>';

        t += '<tr>'
        t += '<td class=nakedtitle>' + locale.main_page.lblAlerts + '</td>'
        t += '<td>' + setFlag(objBoUser[i].AccLvl_Followups); + '</td>';
        t += '<td> [ ' + objBoUser[i].Aantal_Followups + ' ] ' + locale.main_page.lblactiveFollowUps + ' : [ ' + objBoUser[i].ActiveFollowUps + ' ]</td>';
        t += '<td class=nakedtitle>' + locale.main_page.lblFinAnalysis + '</td>'
        t += '<td>' + setFlag(objBoUser[i].AccLvl_FirstReports); + '</td>'
        t += '<td> [ ' + objBoUser[i].Aantal_FirstReports + ' ] </td>';
        t += '</tr>';

        t += '<tr>'
        t += '<td class=nakedtitle>' + locale.main_page.lblComPay + '</td>'
        t += '<td>' + setFlag(objBoUser[i].AccLvl_BetalingsErvaring); + '</td>';
        t += '<td> [ ' + objBoUser[i].Aantal_BetalingsErvaring + ' ] </td>';
        t += '<td class=nakedtitle>' + locale.main_page.lblIntReports + '</td>'
        t += '<td>' + setFlag(objBoUser[i].HBU_AccLvl_International); + '</td>'
        t += '<td></td>'
        t += '</tr>';
    }

    t += '</table>';

    return t;
}

function SetBoUserLoginInfoToTable(objBoUser) {
    var clsnm = 'nakedtitle';
    var t = '<table width=100% border=0>';
    t += '<tr><td colspan=7 class="commontitle" id=loginlist>' + locale.main_page.lblLogins + '</td></tr>';
    t += '<tr>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblID + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblLang + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblLogin + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblPassword + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblEndDate + '</td>';
    t += '<td class=nakedcellheader width=49%>' + locale.main_page.lblEmailAddress + '</td>';
    t += '<td width=1%></td>';
    t += '</tr>';
    for (var i = 0; i < objBoUser.length; i++) {
        if (objBoUser[i].AccLvl_BLOCKED === "1")
        {
            clsnm = 'lockedtrial'
        } else {
            clsnm = 'nakedtitle'
        }

        t += '<tr id="' + objBoUser[i].ID + '">';
        t += '<td class=' + clsnm + '>' + objBoUser[i].ID + '</td>';
        t += '<td class=' + clsnm + '>' + castDbLang(objBoUser[i].intLang) + '</td>';
        t += '<td class=' + clsnm + '>' + objBoUser[i].UserName + '</td>';
        t += '<td class=' + clsnm + '>' + objBoUser[i].Password + '</td>';
        t += '<td class=' + clsnm + '>' + formatEuroDate(objBoUser[i].Einde_Website) + '</td>';
        t += '<td class=' + clsnm + '>' + objBoUser[i].FollowupEmail + '</td>';
        t += '<td class=nakedtitle><img class =onmouseoverdel src="/graphics/common/win_16x16/Remove-ticket.png" onClick="CancelTrial(' + objBoUser[i].ID + ');"</td>';
        t += '</tr>';
    }
    t += '</table>';
    return t;
}

function SetBoUserBillingInfoToTable(objBoUserBills) {
    var t = '<table width=98% border=0>';
    t += '<tr><td colspan=6 class="commontitle">' + locale.main_page.lblLastInvoice + '</td></tr>';
    t += '<tr>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblInvoice + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblmadeon + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblAccuntil + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblAmount + '</td>';
    t += '<td class=nakedcellheader width=15%>' + locale.main_page.lblpaidOn + '</td>';
    t += '<td class=nakedcellheader width=45%>' + locale.main_page.lbltext + '</td>';
    t += '</tr>';
    for (var i = 0; i < objBoUserBills.length; i++) {
        var totDate = objBoUserBills[i].HBO_tot.split('/');
        var totJsDate = new Date(totDate[2], totDate[1], totDate[0], 0, 0, 0, 0);

        var customStyle = '';

        if (totJsDate < new Date()) {
            customStyle = ' style="color:#D3D3D3;"'
        }

        t += '<tr>';
        t += '<td class=nakedtitle' + customStyle + '>' + objBoUserBills[i].HBO_FactNr + '</td>';
        t += '<td class=nakedtitle' + customStyle + '>' + objBoUserBills[i].HBO_PrintDate + '</td>';
        t += '<td class=nakedtitle' + customStyle + '>' + objBoUserBills[i].HBO_tot + '</td>';
        t += '<td class=nakedtitle' + customStyle + '>' + objBoUserBills[i].HBO_prijs + '</td>';
        t += '<td class=nakedtitle' + customStyle + '>' + objBoUserBills[i].HBO_BetaaldOp + setFlag(objBoUserBills[i].HBO_BetaaldOp) + '</td>';
        t += '<td class=nakedtitle' + customStyle + '>' + objBoUserBills[i].HBO_Info + '</td>';
        t += '</tr>';
    }
    t += '</table>';
    return t;
}
function SetBoMobileInfoToTable(objBoMobileInfo) {
    var t = '<table width=100% border=0>';
    t += '<tr><td colspan=4 class="commontitle">Mobile App</td></tr>';
    t += '<tr>';
    t += '<td class="nakedcellheader">' + locale.mobile.login + '</td>';
    t += '<td class="nakedcellheader">' + locale.mobile.device + '</td>';
    t += '<td class="nakedcellheader">' + locale.mobile.account + '</td>';
    t += '<td class="nakedcellheader">' + locale.mobile.lastupdate + '</td></tr>';

    for (var i = 0; i < objBoMobileInfo.length; i++) {
        var customStyle = '';
        if (objBoMobileInfo[i].mu_MobileBlocked === 1) {
            customStyle = 'style="text-decoration: line-through; color: red;"'
        } else {
            var expDate = objBoMobileInfo[i].mu_Vervaldag.toString();
            var expJsDate = new Date(expDate.substring(0, 4), expDate.substring(4, 6), expDate.substring(6, 8));
            if (expJsDate < new Date()) {
                customStyle = 'style="color: #D3D3D3;"'
            }
        }

        t += '<tr>';
        t += '<td class="nakedtitle" ' + customStyle + '>' + objBoMobileInfo[i].mu_Email + '<br />' + objBoMobileInfo[i].mu_Password + '</td>';
        t += '<td class="nakedtitle" ' + customStyle + '>' + objBoMobileInfo[i].mu_DeviceName + '<br />' + objBoMobileInfo[i].mu_InstallID + '</td>'
        t += '<td class="nakedtitle" ' + customStyle + '>' + objBoMobileInfo[i].mu_accounttype + '<br />' + objBoMobileInfo[i].mu_Vervaldag + '</td>';
        t += '<td class="nakedtitle" ' + customStyle + '>' + objBoMobileInfo[i].mu_LastUpdateBy + '<br />' + objBoMobileInfo[i].mu_LastUpdate + '</td>';
        t += '</tr>';
    }

    t += '</table>';

    return t;
}

function SetBoUserHitsInfoToTable(objBoUserHits, objBoUser) {
    cmdE = '<img src=/graphics/common/win_16x16/download.png onclick=showcustom("hitcontainer"); style=cursor:pointer;>&nbsp;&nbsp;';
    cmdC = '<img src=/graphics/common/win_16x16/Upload.png onclick=hidecustom("hitcontainer"); style=cursor:pointer;>&nbsp;&nbsp;';
    var sumHits = 0;
    var limit = 0;
    var detailLogs = '';

    for (var i = 0; i < objBoUserHits.length; i++) {
        sumHits += parseInt(objBoUserHits[i].HitNumbers);
    }

    for (var i = 0; i < objBoUser.length; i++) {
        limit += parseInt(objBoUser[i].Aantal_Website);
        detailLogs = '&nbsp;&nbsp;<img src=/graphics/common/win_16x16/Test-paper.png onclick="openWindowDetailLogs(' + objBoUser[i].HBC_ID + ');" style="cursor:pointer;">';
    }

    var lblHitratio = '';
    lblHitratio = sumHits.toString() + '|' + (limit - sumHits) + ' (LIMIT: ' + limit.toString() + ')';

    var t = '<table width=100% border=0>';
    t += '<tr><td class="commontitle">' + cmdE + cmdC + detailLogs + ' ' + locale.main_page.lblhits + ': ' + lblHitratio + '</td></tr>';
    t += '<table width=100% border=0 id=hitcontainer style=display:none;>';
    t += '<tr><td colspan=2 class="commontitle" id=loginlist>' + locale.main_page.lblWeeklyHits + ' ' + lblHitratio + '</td></tr>';
    t += '<tr>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblDate + '</td>';
    t += '<td class=nakedcellheader width=10%>' + locale.main_page.lblNbWeek + '</td>';
    t += '</tr>';

    for (var i = 0; i < objBoUserHits.length; i++) {
        t += '<tr>';
        t += '<td class=nakedtitle>' + objBoUserHits[i].HitDates + '</td>';
        t += '<td class=nakedtitle>' + objBoUserHits[i].HitNumbers + '</td>';
        t += '</tr>';
    }

    t += '</table>';
    t += '</td></tr></table>';

    return t;
}

//used in Event Menu > Event Click right layout BackOffice accordion pane
function JSON_displayStCompanyBackOfficeDetails(vatnumber, HBC_ID, container) {
		$("#" + container).empty();
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
		return $.get(url, { q: 1, vat: vatnumber, HBC_ID: HBC_ID })
		.done(function (data) {
			if ($.isEmptyObject(data))
			{
				var message = '<div class=BOCyName>' + locale.main_page.BackOfficeInfoMsg + vatnumber + '</p>'
				$("#" + container).html(message);
				
			} else {
				$("#" + container).html(SetBoInfoToTable(data));
				
			}            
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting ST Backoffice Details",
				expire: -1,
				type: "errormessage"
			});
			
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

function JSON_displayStCompanyBackOfficeHistory(vatnumber, HBC_ID, container) {
		$("#" + container).empty();
		
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
		$.get(url, { q: 2, vat: vatnumber, HBC_ID: HBC_ID })
		.done(function (data) {
			if (!$.isEmptyObject(data)) {
				$("#" + container).html(data);
			}
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Backoffice Details.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}

//info used in Lead Menu > dblClick Lead > popup Window
function JSON_GetHBCompanyDetailsPromise(vatnumber, HBC_ID) {
		$("#HBCompanyDetails").empty();
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
		return $.get(url, { q: 1, vat: vatnumber, HBC_ID: HBC_ID })
		.done(function (data) {
			if ($.isEmptyObject(data))
			{
				var message = '<div class=BOCyName>' + locale.main_page.BackOfficeInfoMsg + vatnumber + '</p>';
				$("#HBCompanyDetails" + vatnumber).html(message);
			} else {
				$("#HBCompanyDetails" + vatnumber).html(SetBoInfoToTable(data));
			}
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting HBCompany Details.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#HBCompanyDetails").data('requestRunning',false);
		});
}

function ASPGetCompanyFutureHistory(container, company_id) {
		var url = "/classes/dal/clsScheduler/events.asp";
	
		$.get(url, { a: "GetFutureEvents", company_id: company_id })
		.done(function (data) {
			if (data.length > 0) {
				var responseTekst = "<div class='alert-box error'>" + data + "</div>";
				var innerTekst = $("#" + container).html();
				$("#" + container).html(responseTekst + innerTekst);
			}
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Company FutureHistory.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}
function ASPGetCompanyHistoryPromise(vatnumber, company_id) {
    var container = "HBHistoriek" + vatnumber;

		$("#" + container).empty();
		
		var url = "/classes/dal/clsScheduler/events.asp";
	
		return $.get(url, { a: "GetEventHistory", company_id: company_id })
		.done(function (data) {
			if (data.length > 0) {
				var responseTekst = "<div class='alert-box notice'>" + data + "</div>";
				var innerTekst = $("#" + container).html();
	
				$("#" + container).html(responseTekst + innerTekst);
			}
	
			ASPGetCompanyFutureHistory(container, company_id);
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Company History.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#" + container).data('requestRunning',false);
		});
}
function ASPGetCompanyBackOfficeHistoryPromise(vatnumber, HBC_ID) {
		$("#HBCompanyNotes").empty();
		
		var url = "/classes/dal/clsBackOffice/clsBackOffice.asp";
	
		return $.get(url, ({ q: 2, vat: vatnumber, HBC_ID: HBC_ID }))
		.done(function (data) {
			if (data.length > 0) {
				$("#HBCompanyNotes" + vatnumber).empty().html(data);
			}
		}).fail(function (resp) {
			dhtmlx.message({
				text: "Error getting Company Backoffice history.",
				expire: -1,
				type: "errormessage"
			});
		}).always(function(){
			$("#HBCompanyNotes").data("requestRunning", false);
		});
}

function ASPSetCompanyBackOfficeHistory(objLead) {
    var url = "/classes/dal/clsBackOffice/clsBackOffice.asp?q=3";
    var params = populateobjLead(objLead);
    $.post(url, params)
    .done(function (data) {
        if (data === "1") {
            dhtmlx.message({
                text: locale.main_page.msgcommentsaved,
                expire: 500,
                type: "successmessage"
            });
            dhtmlx.alert(locale.main_page.msgcommentsaved);
        }
        else {
            dhtmlx.message({
                text: locale.main_page.msgcommentsaved,
                expire: -1,
                type: "errormessage"
            });
            dhtmlx.alert(locale.main_page.msgcommentnotsaved + "\n" + data);
        };
    }).fail(function (resp) {
        dhtmlx.message({
            text: "Error in CompanyBackOfficeHistory",
            expire: -1,
            type: "errormessage"
        });
    });
}

function SetBoInfoToTable(objHBCy) {
    var BO_adres = "";
    
    if ($.isEmptyObject(objHBCy))
    {
        BO_adres = locale.main_page.noBOAddressMsg;
    } else {
        BO_adres = objHBCy.HBC_Street + " " + objHBCy.HBC_HouseNr + " " + objHBCy.HBC_HouseNr + " " + objHBCy.HBC_BusNr + "<br>" + objHBCy.HBC_Postcode + " " + objHBCy.HBC_gemeente
    }

    var t = "<table cellpadding=1 width=98%  class=BOInfo border=0>";
    t += "<tr><td colspan=8 class=BOCyName>" + objHBCy.HBC_FirmName + "&nbsp;&nbsp;[BO ID:<b>" + objHBCy.HBC_ID + "</b>]</td></tr>";
    t += "<tr><td class=CwCylblInfo>" + locale.main_page.lblNoEntreprise + "</td><td><b>" + objHBCy.HBC_Firmvat + "</b></td>";
    t += "<td class=CwCylblInfo>" + locale.main_page.lblBOLang + "</td><td>" + castLang(objHBCy.HBC_intLang) + "</td></tr>";

    t += "<tr><td class=CwCylblInfo>" + locale.main_page.lblAdress + "</td><td colspan=5>" + BO_adres + "</td></tr>";
    t += "<tr><td class=CwCylblInfo>" + locale.main_page.lblContact + "</td><td colspan=3>" + objHBCy.HBC_ContactName + "</td>";
    t += "<td class=CwCylblInfo>" + locale.main_page.lblEmail + "</td><td>" + objHBCy.HBC_contactemail + "</td></tr>";

    t += "<tr><td class=CwCylblInfo>" + locale.main_page.lblTel + "</td><td>" + objHBCy.HBC_tel + "</td>";
    t += "<td class=CwCylblInfo>" + locale.main_page.lblGsm + "</td><td>" + objHBCy.HBC_gsm + "</td>";
    t += "<td class=CwCylblInfo>" + locale.main_page.lblFax + "</td><td>" + objHBCy.HBC_fax + "</td></tr>";

    t += "<tr><td class=CwCylblInfo>" + locale.main_page.lblBOConcurrent + "</td><td colspan=3>" + objHBCy.HBcon_Name + "</td>";
    t += "<td class=CwCylblInfo>" + locale.main_page.lblBOExpPayment + "</td><td>" + objHBCy.HBC_Betalingservaring + "</td>";
    t += "<td class=CwCylblInfo>" + locale.main_page.lblBODataMarketing + "</td><td>" + objHBCy.HBC_DatamarketID + "</td></tr>";

    t += "<tr><td colspan=8 class=BOCyName>" + locale.main_page.lblBOHistory + "</td></tr>";

    t += "</table>";

    return t;
}

function CancelTrial(HBU_ID) {
    JSONCompanyBackOfficeUserCancelTrialPromis(HBU_ID)
    .done(function (data) {
        if (data === 1) {
            dhtmlx.message({
                text: locale.main_page.acclevellocked,
                expire: 500,
                type: "successmessage"
            });
            dhtmlx.alert(imgalertsaveok3 + locale.main_page.acclevellocked);
            $("#" + HBU_ID).css('text-decoration', "line-through");
            $("#" + HBU_ID).css('color', "#fe0000");
        }
        else {
            dhtmlx.message({
                text: locale.main_page.lblActionCancelled,
                expire: -1,
                type: "errormessage"
            });
            dhtmlx.alert(imgalertsavenok3 + locale.main_page.lblActionCancelled);
        }
    });
}

function castLang(ID) {
    switch (ID) {
        case "1":
            return "FR";
        default:
            return "NL";
    }
}