<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/dal/clsComments/clsComments.asp" -->
<!-- #include virtual ="/classes/obj/objLead.asp" -->
<!-- #include virtual ="/classes/obj/objBoUser.asp" -->
<!-- #include virtual ="/classes/obj/objBoCompany.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->

<% 


CONST C_EVT_SEND_TRIAL=24
CONST C_EVT_SEND_TRIAL_COMMENT="SYSTEM: TRIAL SENT"

CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"
CONST C_DEFAULT_EXT = 312

CONST C_INITIAL_LEAD_EVENT = 7 'CALL

Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objBackOffice
    Set objBackOffice = new sysBackOffice
    

    select case request.querystring("q")
        case 1 'select query to get back office info based on vat number
            objBackOffice.JSONGetCompanyBackOfficeInfo()
        
        case 2 ' get back office notes(special treatment because not parsed in JSON with fancy characters)
            objBackOffice.ASPGetCompanyBackOfficeHistory()
        
        case 3 ' save back office comment to database
            Dim objLead  : set objLead = new clsObjLead
                objLead.populateLeadObject()
                response.write objBackOffice.ASPSaveCompanyBackOfficeHistory(objLead)
         
        case 4
            objBackOffice.JSONGetCompanyBackOfficebyPostCode()       
        case 5
            objBackOffice.JSONGetCompanyBackOfficeUserInfo()
        case 6
            objBackOffice.JSONGetCompanyBackOfficeUserBillingInfo()
        case 7    
            objBackOffice.JSONGetCompanyBackOfficeUserLoginInfo()
        case 8
            objBackOffice.JSONGetCompanyBackOfficeUserHitsInfo()
        case 9
            objBackOffice.JSONGetCompanyBackOfficeUserLogDetails()
        case 10
            objBackOffice.JSONGetCompanyBackOfficeUserLogHitPages()
        case 11
            objBackOffice.JSONGetCompanyBackOfficeNewTrialLoginPassword()
        case 12
            objBackOffice.AJAXSetCompanyBackOfficeAfterTrialSend()
    
        case 13
            objBackOffice.JSONGetCompanyBackOfficeReTrialLoginPassword()
        case 14
            objBackOffice.JSONCompanyBackOfficeUserCancelTrial()
        case 15
            objBackOffice.JSONCompanyBackOfficeGetTrials()
        case 16
            objBackOffice.JSONGetComboTrialCompanies()
        case 17
            objBackOffice.JSONGetComboTrialContacts()
        case 18
            objBackOffice.JSONGetCompanyBackOfficeInfoByVat()
        case 19 
            objBackOffice.AJAXSetCompanyBackOfficeAfterReTrialSend()
        case 30
            objBackOffice.JSONGetCompanyBackOfficeMobileInfo()
        case 70
            objBackOffice.JSONGetCompanyInfoFromCompanyweb()
        case 80
            objBackOffice.JSONGetCompanyVatFromBackOfficeId()
        case 95
            objBackOffice.JSONGetExpiredTrials()
    case else 'default tbd
            
    end select
    
    set objBackOffice = nothing

class sysBackOffice

    public sub Class_Initialize()
    
    end sub

'---CLASS PROPERTIES---     

    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property

    
    property get usr_id()
        usr_id=""
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    'for testing purpose only
    if len(trim(usr_id))= 0 then usr_id = 1
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get CompanyVat()
        CompanyVat=trim(request.querystring("vat"))
        if len(CompanyVat) = 0 then CompanyVat = 0
    end property
    

    property get postCode()
        postCode=trim(request.querystring("postCode"))
        if len(postCode) = 0 then postCode = "0"
    end property

    property get HBC_ID()
        HBC_ID=trim(request.querystring("HBC_ID"))
        if len(HBC_ID) = 0 then HBC_ID = -1
    end property

    property get selectedDate()
        selectedDate=trim(request.querystring("selectedDate"))
    end property
    property GET HBU_ID()
        HBU_ID=trim(request.QueryString("HBU_ID"))
        if len(HBU_ID) = 0 then HBU_ID = -1
    end property


    '<start region PUBLIC METHODS & FUNCTIONS>
    public function JSONGetExpiredTrials()
        Response.ContentType = "application/json" : response.Write pJSONGetExpiredTrials()
    end function
    public function JSONGetCompanyVatFromBackOfficeId()
        Response.ContentType = "application/json" : response.Write pJSONGetCompanyVatFromBackOfficeId()
    end function
    public function JSONGetCompanyInfoFromCompanyweb()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyInfoFromCompanyweb()
    end function
    public function JSONGetCompanyBackOfficeInfo() 
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeInfo()
    end function
    public function JSONGetCompanyBackOfficebyPostCode() 
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeInfoByPostCode()
    end function
    public function JSONGetCompanyBackOfficeMobileInfo()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeMobileInfo()
    end function
    public function JSONGetCompanyBackOfficeUserInfo()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeUserInfo()        
    end function

    public function JSONGetCompanyBackOfficeUserBillingInfo()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeUserBillingInfo()        
    end function

    public function JSONGetCompanyBackOfficeUserLoginInfo()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeUserLoginInfo()        
    end function

    public function JSONGetCompanyBackOfficeUserHitsInfo()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeUserHitsInfo()        
    end function
        
    public function JSONCompanyBackOfficeUserCancelTrial()
        Response.ContentType = "application/json" : response.write pJSONCompanyBackOfficeUserCancelTrial()
    end function

    public function JSONGetCompanyBackOfficeNewTrialLoginPassword()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeNewTrialLoginPassword()
    end function
    
    public function JSONGetCompanyBackOfficeReTrialLoginPassword()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeReTrialLoginPassword()
    end function

    public function JSONGetCompanyBackOfficeUserLogDetails()
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeUserLogDetails()        
    end function
    
    public function JSONGetCompanyBackOfficeUserLogHitPages()
        response.write pJSONGetCompanyBackOfficeUserLogHitPages() 
    end function

    public function JSONCompanyBackOfficeGetTrials()
        Response.ContentType = "application/json" : response.write  pJSONCompanyBackOfficeGetTrials()
    end function

    public function JSONGetComboTrialCompanies()
        Response.ContentType = "application/json" : response.write  pJSONGetComboTrialCompanies()
    end function

    public function JSONGetComboTrialContacts()
        Response.ContentType = "application/json" : response.write  pJSONGetComboTrialContacts()
    end function

    public function ASPGetCompanyBackOfficeHistory() 
        response.write pASPGetCompanyBackOfficeHistory()
    end function

    public function ASPSaveCompanyBackOfficeHistory(objLead)
        ASPSaveCompanyBackOfficeHistory = pASPSaveCompanyBackOfficeHistory(objLead)        
    end function

    public function AJAXSetCompanyBackOfficeAfterTrialSend()
    response.write  pAJAXSetCompanyBackOfficeAfterTrialSend()
    end function
    public function AJAXSetCompanyBackOfficeAfterReTrialSend()
        response.pAJAXSetCompanyBackOfficeAfterTrialSend()
    end function

    public function JSONGetCompanyBackOfficeInfoByVat()
        Response.ContentType = "application/json" : response.write  pJSONGetCompanyBackOfficeInfoByVat()
    end function
    
    
'<end region PUBLIC METHODS & FUNCTIONS>


'<start region PRIVATE METHODS & FUNCTIONS>
Private Function IsNotNullorEmpty(input)
	dim IsNotNullorEmpty_Output
	IsNotNullorEmpty_Output = true
	'vbEmpty (uninitialized) or vbNull (no valid data)
	if VarType(input) = 0 OR VarType(input) = 1 Then
		IsNotNullorEmpty_Output = false
	End if
	If VarType(input) = 8 Then
		If input = "" Then 
			IsNotNullorEmpty_Output = False
		End If
	End If
	IsNotNullorEmpty = IsNotNullorEmpty_Output

End Function

private function pJSONGetCompanyInfoFromCompanyweb()
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    Dim pVat, pName, pStreet, pPostal, pCity, pNumber, pBus, pVorm
    
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if
    
    pVat = CompanyVat()
    
    if ISNULL(pVat)or pVat = 0 then
        exit function
    end if
    
    strSQL = "SELECT fu_Vat, fu_Name, fu_VormId, fu_StraatNaam, fu_HuisNr, fu_BusNr, fu_Postcode, fu_Gemeente from CW_Firma_Unique_" & Userlang()
    strsql = strsql & " WHERE fu_vat = " & pVat
    
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
    
    objRS.open strSql, objConn, 1, 3
    If Err.number <> 0 then
        sErrMsg = Err.Description & "Source : " & Err.Source & _
        "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeMobileInfo()" & _
        "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                response.write sErrMsg
        exit function
    end if
	
	s = RStoJSON(objRS)
	    
    pJSONGetCompanyInfoFromCompanyweb = s
    objRS.close : set objRS = nothing
    objConn.close : set objConn = nothing
    
    
end function
private function pJSONGetCompanyBackOfficeMobileInfo()
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg, sWhere
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if
        
        sWhere = " WHERE (mu_Hbc_id = " & HBC_ID & ")"
        
    'if nothing valid then return nothing
    if HBC_ID = 0 then sWhere = " WHERE 1=0"
    'SELECT TOP 1
    strSql = "SELECT [mu_id], [mu_InstallID], [mu_DeviceName],[mu_Email],[mu_Password],[mu_MobileRegistered],[mu_MobileBlocked],[mu_MobileHits],[mu_LastUpdate],[mu_LastUpdateBy],[mu_accounttype],[mu_Vervaldag] "
    strSql = strSQl &  " FROM Mob_Users "
    strSql = strSQl &  sWhere & " ORDER BY mu_hbu_id desc"

  
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeMobileInfo()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if
        
		if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if
        
        pJSONGetCompanyBackOfficeMobileInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function
private function pJSONGetCompanyBackOfficeUserInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg, sWhere
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if
        
    'strSql = "select * from hb_users INNER JOIN HB_Company ON HB_Users.HBU_HBC_ID = HB_Company.HBC_ID WHERE (HB_Company.HBC_Firmvat = " & CompanyVat & ")"
    
    'fuzzy logic this or that
    sWhere = " WHERE (u.HBU_HBC_ID = " & HBC_ID & ") or (HB_Company.HBC_Firmvat = " & CompanyVat & ")"
    'priority to ID because backoffice data is really messed up    
    if HBC_ID <> null or HBC_ID > 0 then
        sWhere = " WHERE (u.HBU_HBC_ID = " & HBC_ID & ")"
    else 
        if CompanyVat <> null or CompanyVat > 0 then
            sWhere = " WHERE (HB_Company.HBC_Firmvat = " & CompanyVat & ")"
        end if
    end if
    'if nothing valid then return nothing
    if CompanyVat = 0 and HBC_ID = 0 then sWhere = " WHERE 1=0"
    'SELECT TOP 1
    strSql = "SELECT  u.*, COALESCE(vw_countActiveFollowUps.ActiveFollowUps,0)ActiveFollowUps "
    strSql = strSQl &  " FROM HB_Users u "
    strSql = strSQl &  " INNER JOIN HB_Company ON u.HBU_HBC_ID = HB_Company.HBC_ID left outer JOIN "
    strSql = strSQl &  " vw_countActiveFollowUps ON HB_Company.HBC_ID = vw_countActiveFollowUps.FU_CompID "
    strSql = strSQl &  sWhere & " ORDER BY HBU_ID desc"
	

  
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeUserInfo()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if
        
with objRS
    s= "["
    if not .bof and not .eof then
        
        do while not .eof
            s=s & "{"
            s=s & """ID"":""" & toUnicode(.fields("HBU_ID")) & """"
            s=s & " ,""HBC_ID"":""" & toUnicode(.fields("HBU_HBC_ID")) & """"
            s=s & " ,""Origin"":""" & toUnicode(.fields("HBU_Origin")) & """"
            s=s & " ,""intLang"":""" & toUnicode(.fields("HBU_intLang")) & """"
            s=s & " ,""UserName"":""" & toUnicode(.fields("HBU_UserName")) & """"
            s=s & " ,""Password"":""" & toUnicode(.fields("HBU_Password")) & """"
            s=s & " ,""FollowupEmail"":""" & toUnicode(.fields("HBU_FollowupEmail")) & """"
            s=s & " ,""LastUpdate"":""" & toUnicode(.fields("HBU_LastUpdate")) & """"
            s=s & " ,""LastUpdateUser"":""" & toUnicode(.fields("HBU_LastUpdateUser")) & """"
            s=s & " ,""CreationDate"":""" & toUnicode(.fields("HBU_CreationDate")) & """"
            s=s & " ,""AccLvl_BLOCKED"":""" & toUnicode(.fields("HBU_AccLvl_BLOCKED")) & """"
            s=s & " ,""AccLvl_Website"":""" & toUnicode(.fields("HBU_AccLvl_Website")) & """"
            s=s & " ,""Aantal_Website"":""" & toUnicode(.fields("HBU_Aantal_Website")) & """"
            s=s & " ,""Einde_Website"":""" & toUnicode(.fields("HBU_Einde_Website")) & """"
            s=s & " ,""AccLvl_Mandaten"":""" & toUnicode(.fields("HBU_AccLvl_Mandaten")) & """"
            s=s & " ,""AccLvl_Followups"":""" & toUnicode(.fields("HBU_AccLvl_Followups")) & """"
            s=s & " ,""Aantal_Followups"":""" & toUnicode(.fields("HBU_Aantal_Followups")) & """"
            s=s & " ,""AccLvl_FirstReports"":""" & toUnicode(.fields("HBU_AccLvl_FirstReports")) & """"
            s=s & " ,""Aantal_FirstReports"":""" & toUnicode(.fields("HBU_Aantal_FirstReports")) & """"
            s=s & " ,""AccLvl_JrcaXml"":""" & toUnicode(.fields("HBU_AccLvl_JrcaXml")) & """"
            s=s & " ,""Aantal_JrcaXml"":""" & toUnicode(.fields("HBU_Aantal_JrcaXml")) & """"
            s=s & " ,""Einde_JrcaXml"":""" & toUnicode(.fields("HBU_Einde_JrcaXml")) & """"
            s=s & " ,""AccLvl_Starters"":""" & toUnicode(.fields("HBU_AccLvl_Starters")) & """"
            s=s & " ,""Type_Starters"":""" & toUnicode(.fields("HBU_Type_Starters")) & """"
            s=s & " ,""Einde_Starters"":""" & toUnicode(.fields("HBU_Einde_Starters")) & """"
            s=s & " ,""AccLvl_Failures"":""" & toUnicode(.fields("HBU_AccLvl_Failures")) & """"
            s=s & " ,""Type_Failures"":""" & toUnicode(.fields("HBU_Type_Failures")) & """"
            s=s & " ,""Einde_Failures"":""" & toUnicode(.fields("HBU_Einde_Failures")) & """"
            s=s & " ,""AccLvl_DataCheck"":""" & toUnicode(.fields("HBU_AccLvl_DataCheck")) & """"
            s=s & " ,""Einde_DataCheck"":""" & toUnicode(.fields("HBU_Einde_DataCheck")) & """"
            s=s & " ,""AccLvl_DataRequest"":""" & toUnicode(.fields("HBU_AccLvl_DataRequest")) & """"
            s=s & " ,""Einde_DataRequest"":""" & toUnicode(.fields("HBU_Einde_DataRequest")) & """"
            s=s & " ,""AccLvl_BusAna"":""" & toUnicode(.fields("HBU_AccLvl_BusAna")) & """"
            s=s & " ,""Einde_BusAna"":""" & toUnicode(.fields("HBU_Einde_BusAna")) & """"
            s=s & " ,""ZoekNaam"":""" & toUnicode(.fields("HBU_ZoekNaam")) & """"
            s=s & " ,""PopUpCount"":""" & toUnicode(.fields("HBU_PopUpCount")) & """"
            s=s & " ,""TrialOrigin"":""" & toUnicode(.fields("HBU_TrialOrigin")) & """"
            s=s & " ,""AccLvl_DmBlocked"":""" & toUnicode(.fields("HBU_AccLvl_DmBlocked")) & """"
            s=s & " ,""Aantal_BusAna"":""" & toUnicode(.fields("HBU_Aantal_BusAna")) & """"
            s=s & " ,""AccLvl_BetalingsErvaring"":""" & toUnicode(.fields("HBU_AccLvl_BetalingsErvaring")) & """"
            s=s & " ,""Aantal_BetalingsErvaring"":""" & toUnicode(.fields("HBU_Aantal_BetalingsErvaring")) & """"
            s=s & " ,""AccLvl_XMLFollowups"":""" & toUnicode(.fields("HBU_AccLvl_XMLFollowups")) & """"
            s=s & " ,""Password_EndDate"":""" & toUnicode(.fields("HBU_Password_EndDate")) & """"
            s=s & " ,""Previous_Password"":""" & toUnicode(.fields("HBU_Previous_Password")) & """"
            s=s & " ,""BetErv_Password"":""" & toUnicode(.fields("HBU_BetErv_Password")) & """"
            s=s & " ,""MobAdm_Password"":""" & toUnicode(.fields("HBU_MobAdm_Password")) & """"
            s=s & " ,""HitsPerJaar"":""" & toUnicode(.fields("HBU_HitsPerJaar")) & """"
            s=s & " ,""HBU_AccLvl_International"":""" & toUnicode(.fields("HBU_AccLvl_International")) & """"
            s=s & " ,""CustomerUID"":""" & toUnicode(.fields("HBU_CustomerUID")) & """"
            s=s & " ,""ReportLayout"":""" & toUnicode(.fields("HBU_ReportLayout")) & """"
            s=s & " ,""Mailing"":""" & toUnicode(.fields("HBU_Mailing")) & """"
            s=s & " ,""trialIP"":""" & toUnicode(.fields("hbu_trialIP")) & """"
            s=s & " ,""ActiveFollowUps"":""" & toUnicode(.fields("ActiveFollowUps")) & """"
            s=s & " },"
        .movenext
        loop
        s = left(s,len(s)-1)
        
    end if
    s = s & "]"
end with
        
        pJSONGetCompanyBackOfficeUserInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    
end function
        private function pJSONGetExpiredTrials()
        pJSONGetExpiredTrials=0
        Dim objConn, objRS, strSql, sErrMsg, s, hbcIdList
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .CursorLocation = 3
            .Open    
        end with
    
        if objConn.State = 0 then 
            set objConn = nothing
            exit function
        end if
        s=0
        
        hbcIdList = pGetCompanyHBC_ID_List()


        strSql = "select HBU_HBC_ID as ID" 
        strSql = strSql & "  from  HB_Users "
        strSql = strSql & " inner join [HB_Company] on HBU_HBC_ID = HBC_ID "
        strSql = strSql & " left join HB_Order on HBO_HBC_ID = HBC_ID and HBU_Einde_Website = HBO_tot "
        strSql = strSql & " WHERE HBC_ID in(" & hbcIdList & ") AND HBU_Aantal_Website < 100 and (HBU_UserName like 'TX%' OR HBU_UserName like 'GR%') and HBO_ID is null and "  
        strSql = strSql & " (hbu_acclvl_blocked=1 AND HBU_Aantal_Website<100 AND HBU_Einde_Website > convert(varchar, getdate()-14, 112))"
        strSql = strSql & " group by HBU_HBC_ID order by max(HBU_Einde_Website) "    
        
        set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strSql, objConn,1,3

		if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if

        pJSONGetExpiredTrials = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end function

    private function pJSONGetCompanyVatFromBackOfficeId()
     Dim objConn, oRs, strSql, s, sfield            
    strSql = "SELECT hbc_id, HBC_Firmvat as vat FROM HB_Company WHERE HBC_ID="  & hbc_id()
            
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BackOfficeConnString() , adopenstatic, adcmdreadonly, adcmdtext
        oRs.open strSql, objConn
             
				if not oRS.bof and not oRS.eof then
			s = "[" & RStoJSON(oRS) & "]"
		else
			s = "[]"
		end if
    pJSONGetCompanyVatFromBackOfficeId = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
    end function
private function pJSONGetComboTrialCompanies()       
    Dim objConn, oRs, strSql, s, sfield
    sfield= fieldName()
            
    strSql = "SELECT DISTINCT * FROM dbo.[vw_GetComboCompaniesMailTemplates] WHERE user_id="  & usr_id()
            
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        oRs.open strSql, objConn
             
        s = "{"
        with oRs
            if not .bof and not .eof then
            .moveFirst 
                s= s&  "options:["
                    do while not .eof
                        s = s & "{value:""" &toUnicode(.fields(0)) & """, text:""" & trim(toUnicode(.fields(sfield))) & """},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
            s = s & "]"    
        end if
        End With
        s = s & "}"
    pGetComboCompaniesMailTemplates = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function

private function pJSONGetComboTrialContacts()
    Dim objConn, oRs, strSql, s, sfield
    sfield= fieldName()
            
    strSql = "SELECT DISTINCT * FROM dbo.[vw_GetComboCompaniesMailTemplates] WHERE user_id="  & usr_id()
            
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
        oRs.open strSql, objConn
             
        s = "{"
        with oRs
            if not .bof and not .eof then
            .moveFirst 
                s= s& "options:["
                    do while not .eof
                        s = s & "{value:""" &toUnicode(.fields(0)) & """, text:""" & trim(toUnicode(.fields(sfield))) & """},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
            s = s & "]"    
        end if
        End With
        s = s & "}"
    pGetComboCompaniesMailTemplates = s
    oRs.close : set oRs = nothing
    objConn.close : set objConn = nothing
end function


    private function pGetCompanyHBC_ID_List()
    pGetCompanyHBC_ID_List=0
    Dim objConn, objRS, strSql, sErrMsg, s
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
    
        if objConn.State = 0 then 
            set objConn = nothing
            exit function
        end if
        s=0

        strSql = "select distinct company_hbc_id from ( " 
        strSql = strSql & " SELECT companies.company_hbc_id "
        strSql = strSql & " FROM [salestool].[dbo].[mails] "
        strSql = strSql & " inner join companies on companies.company_id = [mails].company_id and not company_hbc_id is null "
        strSql = strSql & " where user_id="& usr_id() & " and mail_template_id = 1 and mail_creationdate > getdate() - 60 "
        strSql = strSql & " union "
        strSql = strSql & " select c.company_hbc_id "
        strSql = strSql & " from salestool.dbo.comments ev "
        strSql = strSql & " inner join salestool.dbo.companies c on ev.comment_company_id = c.company_id "
        strSql = strSql & " where c.company_hbc_id is not null and ev.comment_usr_id=" & usr_id() & " and ev.comment_event_type_id = 24 "
        strSql = strSQl & " union " 
        strSql = strSql & " select c.company_hbc_id "
        strSql = strSql & " from salestool.dbo.events ev "
        strSql = strSql & " inner join salestool.dbo.leads l "
        strSql = strSql & " on ev.company_id = l.company_id "
        strSql = strSql & " inner join salestool.dbo.companies c "
        strSql = strSql & " on ev.company_id = c.company_id "
        strSql = strSql & " where ev.usr_id = " & usr_id() & " "
        strSql = strSql & " and l.source_id in (3,4) "
        strSql = strSql & " and not c.company_hbc_id is null"
        strSql = strSql & " ) as DD "
        
        set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strSql, objConn,1,3
        with objRS
            if not .bof and not .eof then
                do while not .eof
                s = s & .fields("company_hbc_id") & ","
                .movenext
                loop
        end if
        end with 
        s = left(s, len(s)-1)

        pGetCompanyHBC_ID_List = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
       
    end function

    private function pJSONCompanyBackOfficeGetTrials()
        pJSONCompanyBackOfficeGetTrials = "{""rows"":[]}"
        Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg, hbcIdList
    dim customcss
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .CursorLocation = 3
            .Open    
        end with
    
        if objConn.State = 0 then 
            set objConn = nothing
            exit function
        end if

        hbcIdList = pGetCompanyHBC_ID_List()
        
        if len(trim(hbcIdList))=0 then exit function 
           

        'strSql = "select hbu.HBU_HBC_ID, hbc.[HBC_FirmName], hbc.HBC_ContactName, hbu.HBU_ID, [dbo].[CastDate] (hbu.HBU_Einde_Website,'/')as endDate, [dbo].[CastDate] (hbu.HBU_Password_EndDate,'/')as PasswordEndDate, "
        'strSql = strSql & " l.HitNumbers, hbu.HBU_Einde_Website, hbu.HBU_Password_EndDate, " 
        'strSql = strSql & " hbu.HBU_Einde_BusAna, hbu.HBU_AccLvl_BLOCKED "
        'strSql = strSql & " from .dbo.HB_Users hbu inner join .[dbo].[HB_Company] hbc on hbu.HBU_HBC_ID = hbc.HBC_ID "
        'strSql = strSql & " left outer join [dbo].[vw_GetWeeklyLogs] l on hbu.HBU_HBC_ID = l.Cp_id "
        'strSql = strSql & " where hbu.HBU_HBC_ID in(" & hbcIdList & ")" 
        'strSql = strSql & " AND (hbu_acclvl_blocked = 0 and HBU_Einde_Website < '" &  SetDate() & "') or (hbu_acclvl_BusAna = 1 and hbu_acclvl_dmblocked = 0 and HBU_Einde_BusAna < '" &  SetDate() & "') " 
        'strSql = strSql & " order by hbu.HBU_Einde_Website desc"

    strSql = "select HBU_HBC_ID, max(HBC_FirmName) as HBC_FirmName,max(HBU_UserName) as HBU_UserName, ISNULL(max(HitNumbers),0) as HitNumbers, max(HBU_Einde_Website) as endDate, "
    strSql = strSql & " max(hbu_acclvl_blocked) as hbu_acclvl_blocked " 
    strSql = strSql & "  from  HB_Users "
    strSql = strSql & " inner join [HB_Company] on HBU_HBC_ID = HBC_ID "
    strSql = strSql & " left join HB_Order on HBO_HBC_ID = HBC_ID and HBU_Einde_Website = HBO_tot "
    strSql = strSql & " left join [dbo].[vw_GetWeeklyLogs] l on HBU_HBC_ID = Cp_id "
    strSql = strSql & " WHERE HBU_HBC_ID in(" & hbcIdList & ") and " 
    strSql = strsql & " HBU_Aantal_Website < 100 and "
    strSql = strSql & " (HBU_UserName like 'TX%' OR HBU_UserName like 'GR%') and HBO_ID is null and "
    strSql = strSql & " ((hbu_acclvl_blocked = 0 and HBU_AccLvl_Website = 1) OR (hbu_acclvl_blocked=1 AND HBU_Aantal_Website<100 AND HBU_Einde_Website > convert(varchar, getdate()-14, 112))) "
    strSql = strsql & " group by HBU_HBC_ID "
    strSql = strSql & " order by max(HBU_Einde_Website) "
    
        set objRS=Server.CreateObject("ADODB.Recordset")
        objRS.open strSql, objConn, 1, 3 

        s = "{"
        s= s & """rows"":["
        with objRS
            if not .bof and not .eof then
            Dim doctext  , doclink      
            doctext = "doc_file_desc_"  & UserLang()
            
            .moveFirst 
                 
                    do while not .eof
                        'doclink = "<img src=/graphics/common/win_16x16/" & toUnicode(.fields("doc_img")) & ">^" & toUnicode(.fields("doc_fullPath"))
                        s = s & "{""id"":""" & toUnicode(.fields("HBU_HBC_ID")) & """, ""data"": [""" & toUnicode(.fields("HBU_HBC_ID")) & """,""" & toUnicode(.fields("HBC_FirmName"))  & """, """ & toUnicode(.fields("HBU_UserName"))  & """, """ & toUnicode(.fields("HitNumbers"))  & """, """  & toUnicode(.fields("endDate"))  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
             
            end if
        end with
        s = s & "]" 
        s = s & "}"
        pJSONCompanyBackOfficeGetTrials = s

        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    
    end function


    private function pJSONCompanyBackOfficeUserCancelTrial()
        pJSONCompanyBackOfficeUserCancelTrial=0
        Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .CursorLocation = 3
            .Open    
        end with
    
        if objConn.State = 0 then 
            set objConn = nothing
            exit function
        end if
        strSql = "SELECT  u.HBU_ID, u.HBU_HBC_ID,u.HBU_AccLvl_BLOCKED, u.HBU_LastUpdateUser, u.HBU_LastUpdate, u.HBU_Einde_Website "
        strSql = strSql & " FROM HB_Users u WHERE (HBU_ID =" & HBU_ID() & ")"
   
        set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strSql, objConn,2,3
        with objRS
            if not .bof and not .eof then
                .fields("HBU_AccLvl_BLOCKED")=1
                .fields("HBU_LastUpdateUser")=usr_id()
                .fields("HBU_LastUpdate")=now()
                .update
        pJSONCompanyBackOfficeUserCancelTrial=1    
        end if
        end with 

            objRS.close : set objRS = nothing
            objConn.close : set objConn = nothing
    
    end function

private function pJSONGetCompanyBackOfficeUserLoginInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if

    strSql = "SELECT "
    strSql = strSQl &  " u.HBU_ID as ID, u.HBU_HBC_ID as HBC_ID, u.HBU_intLang as intLang, u.HBU_UserName as UserName, u.HBU_Password as Password, u.HBU_FollowupEmail as FollowupEmail, u.HBU_Einde_Website as Einde_Website, u.HBU_AccLvl_BLOCKED "
    strSql = strSQl &  " FROM HB_Users u "
    strSql = strSQl &  " WHERE (HBU_HBC_ID =" & HBC_ID & ")" 'is not null AND (HB_Company.HBC_Firmvat = " & CompanyVat & ")"
    
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeUserLoginInfo()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if
        
		if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if
        
        pJSONGetCompanyBackOfficeUserLoginInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    
end function


private function pJSONGetCompanyBackOfficeUserBillingInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg,sWhere, sOrder
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if
    
    strSql = "SELECT top 3 HBO_HBC_ID as HBC_ID, HBO_FactNr, HBO_PrintEuroDate as HBO_PrintDate, HBO_totEuroDate as HBO_tot, "
	strSql = strSql & " HBO_BetaaldOpEuroDate as HBO_BetaaldOp, HBO_prijs, HBO_Info "
	strSql = strSql & " FROM vw_GetLastBillingInfo "
    sWhere = " WHERE (HBO_HBC_ID = " & HBC_ID & ")"
    sOrder =  " order by vw_GetLastBillingInfo.HBO_PrintDate desc"

    strSql = strSql & sWhere & sOrder

    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeUserBillingInfo()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if
        
		if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if
        
        pJSONGetCompanyBackOfficeUserBillingInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    
end function




private function pJSONGetCompanyBackOfficeUserHitsInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if

    strSql = "SELECT Cp_id as ID, Cp_ID as HBC_ID, euroDate as HitDates, HitNumbers "
    strSql = strSQl &  " FROM [dbo].[vw_GetAllLogs] "
    strSql = strSQl &  " WHERE (Cp_ID =" & HBC_ID & ") order by HitDates DESC"
    
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeUserHitsInfo()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if
        
		if not objRS.bof and not objRS.eof then
			s = "[" & RStoJSON(objRS) & "]"
		else
			s = "[]"
		end if
        
        pJSONGetCompanyBackOfficeUserHitsInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    
end function




private function pJSONGetCompanyBackOfficeUserLogDetails()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if

    strSql = "SELECT     TOP (300) Cp_id, YEAR(RequestDate) * 10000 + MONTH(RequestDate) * 100 + DAY(RequestDate) AS HitDates, "
    strSql = strSQl & " COUNT(1) AS HitNumbers,"
    strSql = strSQl & " CONVERT (varchar, CONVERT (varchar, RequestDate, 105))as euroDate"
    strSql = strSQl & " FROM         dbo.Logs"
    strSql = strSQl & " GROUP BY Cp_id, YEAR(RequestDate) * 10000 + MONTH(RequestDate) * 100 + DAY(RequestDate),"
    strSql = strSQl & " CONVERT (varchar, CONVERT (varchar, RequestDate, 105)) "
    strSql = strSQl & " HAVING (Cp_ID =" & HBC_ID & ") ORDER BY HitDates DESC"
    
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeUserLogDetails()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if
        s= "{"
with objRS
    if not .bof and not .eof then
     dim i
        .movelast : .movefirst  : i = .recordcount
        s = s & """rows"":["
        do while not .eof    
           s = s & "{""id"":""" & toUnicode(.fields("HitDates")) & """, ""data"": [""" & toUnicode(.fields("euroDate"))  & """, """  & toUnicode(.fields("HitNumbers")) & """, """ & toUnicode(.fields("Cp_id")) & """]},"
        .movenext
        loop
        s = left(s, (len(s)-1))
            s = s & "]" 
    end if
end with
s = s & "}"

        
        pJSONGetCompanyBackOfficeUserLogDetails = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    
end function

private function pJSONGetCompanyBackOfficeUserLogHitPages()
    Dim objConn, objCmd, objParam, objRS, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    strSql = "select year(RequestDate)*10000 + month(RequestDate)*100 + day(RequestDate) as HitDates, "
    strSql = strSql & " CONVERT (varchar, CONVERT (varchar, RequestDate, 103))as euroDate, "
    strSql = strSql & " usercode,requestfile,count(1) as HitNumbers "
    strSql = strSql & " from logs " 
    strSql = strSql & " where cp_id = " & HBC_ID 
    strSql = strSql & " and year(RequestDate)*10000 + month(RequestDate)*100 + day(RequestDate) =" &selectedDate
    strSql = strSql & " group by year(RequestDate)*10000 + month(RequestDate)*100 + day(RequestDate),usercode,requestfile, "
    strSql = strSql & " CONVERT (varchar, CONVERT (varchar, RequestDate, 103))"
    
    on error resume next        
    set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeUserLogHitPages()" & _
            "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
            exit function
        end if

    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if
 
    s = "{"
    with objRS
    if not .bof and not .eof then
    Dim i 
        s= s & """rows"":["
        do while not .eof
    i = i +1    
           s = s & "{""id"":""" & i & """, ""data"": [""" & toUnicode(.fields("euroDate"))  & """, """  & toUnicode(.fields("usercode")) & """, """ & toUnicode(.fields("requestfile")) & """, """ & toUnicode(.fields("HitNumbers")) & """]},"
        .movenext
        loop
        s = left(s, (len(s)-1))
            s = s & "]" 
    end if
end with    
    s = s & "}"
           
        pJSONGetCompanyBackOfficeUserLogHitPages = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function



private function pJSONGetCompanyBackOfficeInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = "dbo.ST_GetBackOfficeCompanyInfo"    
    a = CompanyVat()
    b = userlang()
    c = postCode()
    d= HBC_ID()
    
    Dim jfc_s_desc, jfc_l_desc, nbc_desc, HBcon_Name
        jfc_s_desc = "jfc_s_desc_" + b
        jfc_l_desc = "jfc_l_desc_" + b
        nbc_desc = "nbc_desc_" + b
        HBcon_Name = "HBcon_Name_" + b

    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with

    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@vatcode", 8, 1, len(a), a) 
            .Parameters.Append .CreateParameter("@postcode", 8, 1, len(c), c)
            .Parameters.Append .CreateParameter("@hbc_id", 8, 1, len(d), d)  
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeInfo()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with       
                with objRS
                    if not .bof and not .eof then
                        do while not .eof
                            s=s & " ""HBC_ID"":""" & toUnicode(.fields("HBC_ID")) & """"
                            s=s & " ,""HBC_Firmvat"":""" & toUnicode(.fields("HBC_Firmvat")) & """"
                            s=s & " ,""HBC_FirmName"":""" & toUnicode(.fields("HBC_FirmName")) & """"
                            s=s & " ,""HBC_ContactName"":""" & toUnicode(.fields("HBC_ContactName")) & """"
                            s=s & " ,""HBC_Street"":""" & toUnicode(.fields("HBC_Street")) & """"
                            s=s & " ,""HBC_HouseNr"":""" & toUnicode(.fields("HBC_HouseNr")) & """"
                            s=s & " ,""HBC_BusNr"":""" & toUnicode(.fields("HBC_BusNr")) & """"                        
                            s=s & " ,""HBC_Postcode"":""" & toUnicode(.fields("HBC_Postcode")) & """"
                            s=s & " ,""HBC_gemeente"":""" & toUnicode(.fields("HBC_gemeente")) & """"
                            s=s & " ,""HBC_fax"":""" & toUnicode(.fields("HBC_fax")) & """"
                            s=s & " ,""HBC_tel"":""" & toUnicode(.fields("HBC_tel")) & """"
                            s=s & " ,""HBC_gsm"":""" & toUnicode(.fields("HBC_gsm")) & """"
                            s=s & " ,""HBC_contactemail"":""" & toUnicode(.fields("HBC_contactemail")) & """"
                            's=s & " ,""HBC_Nota"":""" & toUnicode(.fields("HBC_Nota")) & """"
                            s=s & " ,""HBcon_Name"":""" &toUnicode(.fields(HBcon_Name)) & """"
                            s=s & " ,""HBC_intLang"":""" & toUnicode(.fields("HBC_intLang")) & """"
                            s=s & " ,""HBC_Betalingservaring"":""" & toUnicode(.fields("HBC_Betalingservaring")) & """"
                            s=s & " ,""HBC_DatamarketID"":""" & toUnicode(.fields("HBC_DatamarketID")) & """"
                            s=s & " ,""jfc_s_desc"":""" &toUnicode(.fields(jfc_s_desc)) & """"
                            s=s & " ,""jfc_l_desc"":""" &toUnicode(.fields(jfc_l_desc)) & """"
                            s=s & " ,""nbc_desc"":""" &toUnicode(.fields(nbc_desc)) & """"
                        .movenext
                        loop
                    end if
                end with
            s = "{" & s & "}"
        
        pJSONGetCompanyBackOfficeInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
end function

    private function pASPGetCompanyBackOfficeHistory()
    Dim vatCode, strSql, s 
    Dim objConn, objRS 
    s = ""
    vatCode = CompanyVat()

    strSql = "SELECT HBC_Nota FROM HB_Company WHERE HBC_Firmvat=" & vatCode 
    if HBC_ID() <> 0 then strSql = "SELECT HBC_Nota FROM HB_Company WHERE HBC_ID=" & HBC_ID()


    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    if objConn.State = 1 then
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 1 'adCmdText
            .CommandText =  strSql
            on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysBackOffice.pASPGetCompanyBackOfficeHistory()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with
           
         with objRS
                    if not .bof and not .eof then
                        do while not .eof
                            s=s & server.htmlencode(toUnicode(.fields("HBC_Nota")))
                            s= replace(trim(.fields("HBC_Nota")),chr(10) & chr(13),"<br>",1,-1,vbtextcompare)
                            s= replace(s, vbcrlf, "<br>",1,-1, vbtextcompare)
                            
                        .movenext
                        loop
                    end if
         end with    
        s= replace(s, chr(10) & chr(13), "<br>")
        
        pASPGetCompanyBackOfficeHistory = s
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing

    end if


    end function

private function pJSONGetCompanyBackOfficeInfoByPostCode()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = "dbo.ST_GetBackOfficeCompanyInfo"    
    a = CompanyVat()
    b = userlang()
    c = postCode()
    
    Dim jfc_s_desc, jfc_l_desc, nbc_desc, HBcon_Name
        jfc_s_desc = "jfc_s_desc_" + b
        jfc_l_desc = "jfc_l_desc_" + b
        nbc_desc = "nbc_desc_" + b
        HBcon_Name = "HBcon_Name_" + b

    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with

    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@vatcode", 8, 1, len(a), a) 
                       .Parameters.Append .CreateParameter("@postcode", 8, 1, len(c), c) 
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeInfo()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with       
        s= "["
                with objRS
                
                    if not .bof and not .eof then
                        
                        do while not .eof
                             s=s & "{"
                            s=s & " ""HBC_ID"":""" & toUnicode(.fields("HBC_ID")) & """"
                            s=s & " ,""HBC_Firmvat"":""" & toUnicode(.fields("HBC_Firmvat")) & """"
                            s=s & " ,""HBC_FirmName"":""" & toUnicode(.fields("HBC_FirmName")) & """"
                            s=s & " ,""HBC_ContactName"":""" & toUnicode(.fields("HBC_ContactName")) & """"
                            s=s & " ,""HBC_Street"":""" & toUnicode(.fields("HBC_Street")) & """"
                            s=s & " ,""HBC_HouseNr"":""" & toUnicode(.fields("HBC_HouseNr")) & """"
                            s=s & " ,""HBC_BusNr"":""" & toUnicode(.fields("HBC_BusNr")) & """"                        
                            s=s & " ,""HBC_Postcode"":""" & toUnicode(.fields("HBC_Postcode")) & """"
                            s=s & " ,""HBC_gemeente"":""" & toUnicode(.fields("HBC_gemeente")) & """"
                            s=s & " ,""HBC_fax"":""" & toUnicode(.fields("HBC_fax")) & """"
                            s=s & " ,""HBC_tel"":""" & toUnicode(.fields("HBC_tel")) & """"
                            s=s & " ,""HBC_gsm"":""" & toUnicode(.fields("HBC_gsm")) & """"
                            s=s & " ,""HBC_contactemail"":""" & toUnicode(.fields("HBC_contactemail")) & """"
                            's=s & " ,""HBC_Nota"":""" & toUnicode(.fields("HBC_Nota")) & """"
                            s=s & " ,""HBcon_Name"":""" &toUnicode(.fields(HBcon_Name)) & """"
                            s=s & " ,""HBC_intLang"":""" & toUnicode(.fields("HBC_intLang")) & """"
                            s=s & " ,""HBC_Betalingservaring"":""" & toUnicode(.fields("HBC_Betalingservaring")) & """"
                            s=s & " ,""HBC_DatamarketID"":""" & toUnicode(.fields("HBC_DatamarketID")) & """"
                            s=s & " ,""jfc_s_desc"":""" &toUnicode(.fields(jfc_s_desc)) & """"
                            s=s & " ,""jfc_l_desc"":""" &toUnicode(.fields(jfc_l_desc)) & """"
                            s=s & " ,""nbc_desc"":""" &toUnicode(.fields(nbc_desc)) & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        
                    end if
                    
                end with
    s = s & "]"    
        
        pJSONGetCompanyBackOfficeInfoByPostCode = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
end function

private function pJSONGetCompanyBackOfficeInfoByVat()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = "dbo.ST_GetBackOfficeCompanyInfo"    
    a = CompanyVat()
    b = userlang()
    c = postCode()
    d= HBC_ID()
    
    Dim jfc_s_desc, jfc_l_desc, nbc_desc, HBcon_Name
        jfc_s_desc = "jfc_s_desc_" + b
        jfc_l_desc = "jfc_l_desc_" + b
        nbc_desc = "nbc_desc_" + b
        HBcon_Name = "HBcon_Name_" + b

    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with

    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@vatcode", 8, 1, len(a), a) 
            .Parameters.Append .CreateParameter("@postcode", 8, 1, len(c), c)
            .Parameters.Append .CreateParameter("@hbc_id", 8, 1, len(d), d)  
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeInfoByVat()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with       
         s= "["
                with objRS
                
                    if not .bof and not .eof then
                        
                        do while not .eof
                             s=s & "{"
                            s=s & " ""HBC_ID"":""" & toUnicode(.fields("HBC_ID")) & """"
                            s=s & " ,""HBC_Firmvat"":""" & toUnicode(.fields("HBC_Firmvat")) & """"
                            s=s & " ,""HBC_FirmName"":""" & toUnicode(.fields("HBC_FirmName")) & """"
                            s=s & " ,""HBC_ContactName"":""" & toUnicode(.fields("HBC_ContactName")) & """"
                            s=s & " ,""HBC_Street"":""" & toUnicode(.fields("HBC_Street")) & """"
                            s=s & " ,""HBC_HouseNr"":""" & toUnicode(.fields("HBC_HouseNr")) & """"
                            s=s & " ,""HBC_BusNr"":""" & toUnicode(.fields("HBC_BusNr")) & """"                        
                            s=s & " ,""HBC_Postcode"":""" & toUnicode(.fields("HBC_Postcode")) & """"
                            s=s & " ,""HBC_gemeente"":""" & toUnicode(.fields("HBC_gemeente")) & """"
                            s=s & " ,""HBC_fax"":""" & toUnicode(.fields("HBC_fax")) & """"
                            s=s & " ,""HBC_tel"":""" & toUnicode(.fields("HBC_tel")) & """"
                            s=s & " ,""HBC_gsm"":""" & toUnicode(.fields("HBC_gsm")) & """"
                            s=s & " ,""HBC_contactemail"":""" & toUnicode(.fields("HBC_contactemail")) & """"
                            's=s & " ,""HBC_Nota"":""" & toUnicode(.fields("HBC_Nota")) & """"
                            s=s & " ,""HBcon_Name"":""" &toUnicode(.fields(HBcon_Name)) & """"
                            s=s & " ,""HBC_intLang"":""" & toUnicode(.fields("HBC_intLang")) & """"
                            s=s & " ,""HBC_Betalingservaring"":""" & toUnicode(.fields("HBC_Betalingservaring")) & """"
                            s=s & " ,""HBC_DatamarketID"":""" & toUnicode(.fields("HBC_DatamarketID")) & """"
                            s=s & " ,""jfc_s_desc"":""" &toUnicode(.fields(jfc_s_desc)) & """"
                            s=s & " ,""jfc_l_desc"":""" &toUnicode(.fields(jfc_l_desc)) & """"
                            s=s & " ,""nbc_desc"":""" &toUnicode(.fields(nbc_desc)) & """"
                            s=s & " },"
                            .movenext
                            loop
                            s = left(s,len(s)-1)
                            
                    end if 
                end with
        s = s & "]" 
        
        pJSONGetCompanyBackOfficeInfoByVat = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
end function


private function pASPSaveCompanyBackOfficeHistory(objLead)
    'this prevents any insert which is not coming from the interface
    if objLead.trusted <> 1 then exit function
    
    Dim debugMsg, comment_ID
    Dim objConn, objRS, strSql, sErrMsg

    strSql = "SELECT TOP 1 * FROM [dbo].[comments]"    
    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        
    if objConn.State <> 1 then exit function    
  
    set objRS = Server.CreateObject("ADODB.Recordset")
        objRs.open strsql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pASPSaveCompanyBackOfficeHistory()" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            'response.write sErrMsg
            exit function
        end if
    
        with objRS
            .addNew
                .fields("comment_event_type_id")= C_INITIAL_LEAD_EVENT
                .fields("comment_usr_id")=objLead.user_id
                .fields("comment_event_id")=0
                .fields("comment_lead_id")=objLead.lead_id
                .fields("comment_company_vat")=objLead.company_vat
                .fields("comment_company_id")=objLead.company_id
                .fields("comment_date")= SetDate()
                .fields("comment_time")= SetTime()
                .fields("comment")=objLead.HBCompanyNotes
                .fields("AuditUserCreated")=objLead.user_id
            .update
             comment_ID =  toUnicode(.fields("comment_id"))        
            sErrMsg= 1
        end with
        
        objRs.Close : set objRS = nothing
        objConn.close : set objConn = nothing  

        pASPSaveCompanyBackOfficeHistory= sErrMsg

end function

    Private function pUpdateCompany_hbc_id(objBoCompany)
    pUpdateCompany_hbc_id =0
    Dim objConn, objRS, strSql, sErrMsg
    if len(trim(objBoCompany.company_id)) = 0 then exit function

    strSql = "SELECT TOP 1 * FROM [dbo].[companies] where company_id=" & objBoCompany.company_id    
    
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        
    if objConn.State <> 1 then exit function    
  
    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strSql, objConn,2,3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysBackOffice.pUpdateCompany_hbc_id(objBoCompany)" & _
            "<br />while triggering [objRs.open strsql:] " & strsql
            'response.write sErrMsg
            exit function
        end if
    
        with objRs
            if not .bof and not .eof then
                'response.write "into objrs of " & strsql : response.end
                    .fields("company_hbc_id")= objBoCompany.HBC_ID
                    .update
                
            end if
            pUpdateCompany_hbc_id =1
        end with
            
    On error resume next
    if err.number = 0 then pUpdateCompany_hbc_id=1 else pUpdateCompany_hbc_id= err.description    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing

    end function
        

'<START region PRIVATE FUNCTIONS: TRIAL PROCESSING >
private function pJSONGetCompanyBackOfficeReTrialLoginPassword()
 dim sWhere
    if  HBC_ID() > 0 then 
        sWhere = " WHERE (u.HBU_HBC_ID =" & HBC_ID() & ")"
    Else
        response.Write("STOP!")
        Response.Status = "503.2 - Concurrent request limit exceeded."
        response.End()
    end if
 
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if

    'strSql = "SELECT  u.HBU_ID, u.HBU_HBC_ID, u.HBU_intLang, u.HBU_UserName, u.HBU_Password, u.HBU_FollowupEmail , HBU_Einde_Website "
    'strSql = strSql & " FROM HB_Users u WHERE (HBU_ID =" & HBU_ID() & ")"
   
    strSql = "SELECT u.HBU_ID, u.HBU_HBC_ID, u.HBU_intLang, u.HBU_UserName, u.HBU_Password, u.HBU_FollowupEmail, u.HBU_Einde_Website, HB_Company.HBC_Firmvat "
    strSql = strSql & " FROM HB_Users AS u INNER JOIN  HB_Company ON u.HBU_HBC_ID = HB_Company.HBC_ID"

    strSql = strSql & sWhere
    
    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strSql, objConn,2,3
    with objRS
    
     s= "["
        if not .bof and not .eof then
         dim i 
            
            do while not .eof
                s= """login"":""" & toUnicode(.fields("HBU_UserName")) & """"
                s=s & " ,""password"":""" & toUnicode(.fields("HBU_Password")) & """"
                s=s & " ,""End_Website"":""" & toUnicode(.fields("HBU_Einde_Website")) & """"
            .movenext
            loop
        end if
    end with 
    s = "{" & s & "}"
        pJSONGetCompanyBackOfficeReTrialLoginPassword = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

end function

private function pJSONGetCompanyBackOfficeNewTrialLoginPassword()
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
    
    if objConn.State = 0 then 
        set objConn = nothing
        exit function
    end if
    strsql = "dbo.[spGetNewTXLoginAndPwd]"
    set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = false
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then 
                sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysBackOffice.pJSONGetCompanyBackOfficeNewTrialLoginPassword()" & _
                    "<br />while triggering [set objRS = .Execute]. -> :"& strsql
                    response.write sErrMsg
                    exit function
                    set objRs= nothing
            end if
    with objRS
    
        if not .bof and not .eof then
         dim i 
            
            do while not .eof
                s= """login"":""" & toUnicode(.fields("NewLogin")) & """"
                s=s & " ,""password"":""" & toUnicode(.fields("NewPassword")) & """"
                s=s & " ,""End_Website"":""" & toUnicode(.fields("End_Website")) & """"
            .movenext
            loop            
        end if
    end with 
    s = "{" & s & "}"

        pJSONGetCompanyBackOfficeNewTrialLoginPassword = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

end function


Private function pAJAXSetCompanyBackOfficeAfterTrialSend()
    Dim objBoCompany  : set objBoCompany = new clsObjBoCompany
        objBoCompany.populateBoCompanyObject()
            'objBoCompany.responseWriteBoCompanyObject() : response.end  'uncomment for debugging; will print objBoUser to browser
    Dim objBoUser  : set objBoUser = new clsObjBoUser
        objBoUser.populateBoUserObject()
            'objBoUser.responseWriteBoUserObject() : response.End  'uncomment for debugging; will print objBoUser to browser

    Dim objConn, retval 
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .mode=3 '3:readWrite 1:'adModeRead 2:adModeWrite
            .CursorLocation = 3
            .Open
            .begintrans    
        end with
    
    retval=0
        retval=pSetBoCompanyAfterTrialSend(objConn,objBoCompany)
        if retval=1 then 
            objBoUser.HBU_HBC_ID = objBoCompany.HBC_ID
            'response.write "objBoUser.HBU_HBC_ID:" & objBoUser.HBU_HBC_ID & " - objBoCompany.HBC_ID:"  & objBoCompany.HBC_ID : response.end
            retval=pSetBoContactAfterTrialSend(objConn,objBoUser)
        end if

        if retval=1 then
            retval = pUpdateCompany_hbc_id(objBoCompany)
        end if
   
        if retVal=1 then
            objConn.commitTrans
     
            Dim oComment : set oComment = new sysComments     
            Dim objComment : set objComment = new clsObjComment
      

                objComment.comment_event_type_id = C_EVT_SEND_TRIAL
                objComment.comment_usr_id = usr_id() 
                objComment.comment_event_id = objBoCompany.event_id  
                objComment.comment_company_vat = objBoCompany.HBC_FirmVat
                objComment.comment_company_id= objBoCompany.company_id
                objComment.comment_date = SetDate() 
                objComment.comment_time = SetTime() 
                objComment.comment = C_EVT_SEND_TRIAL_COMMENT 
                objComment.AuditUserUpdated = request("AuditUserUpdated")  
                objComment.trusted = 1
                
                oComment.AddComment(objComment)
                retval = 1            
        else
            objConn.rollbackTrans
        end if


    set objConn = nothing
    pAJAXSetCompanyBackOfficeAfterTrialSend=retVal
    'response.write "pAJAXSetCompanyBackOfficeAfterTrialSend=retVal" & retval : response.end
    
end function


private function pGetBoCompanyAddressAfterTrialSend(objBoCompany)
    Dim objRS, strSql, sErrMsg, blnAddNew

        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .mode=3 '3:readWrite 1:'adModeRead 2:adModeWrite
            .CursorLocation = 3
            .Open    
        end with
     
       if objConn.State <> 1 then exit function

    strSql = "SELECT [company_address_street],[company_address_number],[company_address_boxnumber] ,[company_address_floor],[company_address_postcode],[company_address_localite] "
    strSql = strSql & " FROM vw_GetCompany_FR WHERE [company_vat]=" &  objBoCompany.HBC_Firmvat
   
    'response.write "pGetBoCompanyAddressAfterTrialSend(objConn,objBoCompany)<br>" & strSql : response.end

    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strSql, objConn,2,3
    with objRs
        if not .bof and not .eof then
            objBoCompany.HBC_Street= toUnicode(.fields("company_address_street"))
            objBoCompany.HBC_HouseNr=toUnicode(.fields("company_address_number"))
            objBoCompany.HBC_BusNr=toUnicode(.fields("company_address_boxnumber"))
            objBoCompany.HBC_Postcode=toUnicode(.fields("company_address_postcode"))
            objBoCompany.HBC_gemeente=toUnicode(.fields("company_address_localite"))
        end if
    end with
            
    On error resume next
    if err.number = 0 then pGetBoCompanyAddressAfterTrialSend=1 else pGetBoCompanyAddressAfterTrialSend= err.description    
    objRs.Close : set objRS = nothing
    objConn.close : set objConn = nothing
    
end function

Private Function pSetBoCompanyAfterTrialSend(objConn,objBoCompany)
    pSetBoCompanyAfterTrialSend=0
    Dim objRS, strSql, sErrMsg, blnAddNew, sqlAddr

    if not objConn is nothing then
    else 
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .mode=3 '3:readWrite 1:'adModeRead 2:adModeWrite
            .CursorLocation = 3
            .Open    
        end with
    end if     
       if objConn.State <> 1 then exit function
    if len(trim(objBoCompany.HBC_ID))= 0 then objBoCompany.HBC_ID=0
    strSql = "SELECT HBC_ID, HBC_FirmVat,HBC_FirmName,HBC_ContactName,HBC_ContactEmail,HBC_Tel,HBC_origin,HBC_intLang,HBC_ExtCode,HBC_nota,HBC_ConcurrentID,HBC_DatamarketID "
    strSql = strSql & " ,HBC_Street, HBC_HouseNr, HBC_BusNr, HBC_postCode, HBC_gemeente, HBC_CreationDate, HBC_LastUpdate, HBC_LastUpdateUser "
    strSql = strSql & " FROM  HB_Company "
    strSql = strSql & " WHERE [HBC_ID]=" & objBoCompany.HBC_ID
    
    'response.write "pSetBoCompanyAfterTrialSend(objConn,objBoCompany)<br>" & strSql : response.end

    'get address info from SalesTool database(lead to event promotion tries to build an address and saves it into SalesTool App)
    pGetBoCompanyAddressAfterTrialSend objBoCompany

    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn,2,3
    with objRs
        if .bof and .eof then
			blnAddNew=true
		end if

    'response.write "pSetBoCompanyAfterTrialSend(objConn,objBoCompany) > blnAddNew:"  & blnAddNew : response.end
	
        if blnAddNew = true then 
			.addNew
            
			.fields("HBC_FirmVat")= objBoCompany.HBC_FirmVat
            .fields("HBC_FirmName")= objBoCompany.HBC_FirmName
            .fields("HBC_ContactName")= objBoCompany.HBC_ContactName
            .fields("HBC_ContactEmail")= objBoCompany.HBC_ContactEmail
            .fields("HBC_Tel")= objBoCompany.HBC_Tel
            .fields("HBC_origin")= objBoCompany.HBC_origin
            .fields("HBC_intLang")= objBoCompany.HBC_intLang
            .fields("HBC_ExtCode")= objBoCompany.HBC_ExtCode            
            .fields("HBC_ConcurrentID")= objBoCompany.HBC_ConcurrentID      '0
            .fields("HBC_DatamarketID")= objBoCompany.HBC_DatamarketID      '0
            .fields("HBC_Street")=objBoCompany.HBC_Street
            .fields("HBC_HouseNr")=objBoCompany.HBC_HouseNr
            .fields("HBC_BusNr")=objBoCompany.HBC_BusNr
            .fields("HBC_postCode")=objBoCompany.HBC_Postcode
            .fields("HBC_gemeente")=objBoCompany.HBC_gemeente
            
            .fields("HBC_nota")= objBoCompany.HBC_nota 
            .fields("HBC_CreationDate") = now()
                               
			.update
		End if
            objBoCompany.HBC_ID = toUnicode(.fields("HBC_ID"))   
            pSetBoCompanyAfterTrialSend=1
		
    end with

    if pSetBoCompanyAfterTrialSend <> 1 then pSetBoCompanyAfterTrialSend= err.description    
    objRs.Close : set objRS = nothing
    
    end function

Private Function pSetBoContactAfterTrialSend(objConn, objBoUser)

    pSetBoContactAfterTrialSend=0
    Dim objRS, strSql, sErrMsg, blnAddNew

    if not objConn is nothing then
    else
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = BackOfficeConnString()
            .mode=3 '3:readWrite 1:'adModeRead 2:adModeWrite
            .CursorLocation = 3
            .Open    
        end with
    end if

    if objConn.State <> 1 then exit function
    if len(trim(objBoUser.HBU_ID))= 0 then objBoUser.HBU_ID=0
    strSql = "SELECT HBU_HBC_ID,HBU_Origin,HBU_TrialOrigin,HBU_UserName,HBU_Zoeknaam,HBU_Password,HBU_LastUpdateUser,HBU_AccLvl_BLOCKED,HBU_AccLvl_DmBLOCKED,HBU_AccLvl_Website,HBU_Aantal_Website,HBU_Einde_Website,HBU_AccLvl_Followups,HBU_Aantal_Followups,HBU_AccLvl_FirstReports,HBU_Aantal_FirstReports,HBU_intlang,HBU_AccLvl_Mandaten, HBU_FollowupEmail "
    strSql = strSql & " ,HBU_LastUpdate,HBU_LastUpdateUser  FROM  HB_Users "
    strSql = strSql & " WHERE [HBU_HBC_ID]=" & objBoUser.HBU_HBC_ID
    
    'response.write "pSetBoContactAfterTrialSend(objConn, objBoUser)<br>" & strsql : response.end
    
    set objRS = Server.CreateObject("ADODB.Recordset")
    objRs.open strsql, objConn,2,3
    with objRs
    'if .bof and .eof then blnAddNew=true
    '    if blnAddNew = false Then
    '        dim newEndDate
    '        dim toUseDate
    '        dim toUseDateMonth
    '        dim toUseDateDay
    '        
    '        newEndDate = DateAdd("d", 7, Now())
    '        
    '        toUseDateYear = DatePart("yyyy", newEndDate)
    '        
    '        toUseDateMonth = DatePart("m", newEndDate)
    '        toUseDateDay = DatePart("d", newEndDate)
    '        
    '        if (len(cstr(toUseDateMonth)) = 1) Then
    '            toUseDateMonth = "0" & toUseDateMonth
    '        End if
    '        if (len(cstr(toUseDateDay)) = 1) Then
    '            toUseDateDay = "0" & toUseDateDay
    '        End if			
    '        
    '        objBoUser.HBU_AccLvl_BLOCKED =0            '0
    '        objBoUser.HBU_AccLvl_DmBLOCKED =0        '0
    '        objBoUser.HBU_AccLvl_Website     =1        '1
    '        objBoUser.HBU_Aantal_Website    =25         '25
    '        objBoUser.HBU_Einde_Website  = toUseDateYear & toUseDateMonth & toUseDateDay 'End WebSite
    '        objBoUser.HBU_AccLvl_Followups  =0       '0
    '        objBoUser.HBU_Aantal_Followups =0        '0
    '        objBoUser.HBU_AccLvl_FirstReports=0   '0
    '        objBoUser.HBU_Aantal_FirstReports =0  '0
    '        objBoUser.HBU_AccLvl_Mandaten  =1         '1
    '        
    '    End if
    'response.write "boUser: <br/>" & objBoUser.responseWriteBoUserObject() : response.end
        .addNew
            .fields("HBU_HBC_ID")= objBoUser.HBU_HBC_ID
            .fields("HBU_Origin")= objBoUser.HBU_Origin                             '14
            .fields("HBU_TrialOrigin")= objBoUser.HBU_TrialOrigin                   'SALESTOOL'
            .fields("HBU_UserName")= objBoUser.HBU_UserName                         'HBC_ExtCode
            .fields("HBU_Zoeknaam")= objBoUser.HBU_Zoeknaam                         'HBC_ExtCode
            .fields("HBU_Password")= objBoUser.HBU_Password                         'new Pwd
            .fields("HBU_LastUpdateUser")= objBoUser.HBU_LastUpdateUser             'WebTry'
            .fields("HBU_AccLvl_BLOCKED")= objBoUser.HBU_AccLvl_BLOCKED             '0
            .fields("HBU_AccLvl_DmBLOCKED")= objBoUser.HBU_AccLvl_DmBLOCKED         '0
            .fields("HBU_AccLvl_Website")= objBoUser.HBU_AccLvl_Website             '1
            .fields("HBU_Aantal_Website")= objBoUser.HBU_Aantal_Website             '25
            .fields("HBU_Einde_Website")= objBoUser.HBU_Einde_Website               'End WebSite
            .fields("HBU_AccLvl_Followups")= objBoUser.HBU_AccLvl_Followups         '0
            .fields("HBU_Aantal_Followups")= objBoUser.HBU_Aantal_Followups         '0
            .fields("HBU_AccLvl_FirstReports")= objBoUser.HBU_AccLvl_FirstReports   '0
            .fields("HBU_Aantal_FirstReports")= objBoUser.HBU_Aantal_FirstReports   '0
            .fields("HBU_intlang")= objBoUser.HBU_intlang                           'customerlang 
            .fields("HBU_AccLvl_Mandaten")= objBoUser.HBU_AccLvl_Mandaten           '1
            .fields("HBU_FollowupEmail")= objBoUser.HBU_FollowupEmail    
            .fields("HBU_LastUpdate")= now()
            .fields("HBU_LastUpdateUser")= usr_id()
            .update  
        pSetBoContactAfterTrialSend=1
    'response.write " pSetBoContactAfterTrialSend result : " & pSetBoContactAfterTrialSend :response.end
    end with
    'On error resume next
    if pSetBoContactAfterTrialSend <> 1 then pSetBoContactAfterTrialSend= err.description    
    objRs.Close : set objRS = nothing
    'objConn.close : set objConn = nothing
    
end function

'<END region PRIVATE FUNCTIONS:  TRIAL PROCESSING >


    private function formatTrialEndDate(sdate)
    dim dd, mm, yy
        yy = left(sdate, 4)
        dd = right(sdate, 2)
        mm = mid(sdate,5,2)
    formatTrialEndDate = dd & "/" & mm & "/" & yy
    end function

    public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    public function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function



'<end region PRIVATE METHODS & FUNCTIONS>
public sub Class_Terminate()
    'set objLead = nothing
    'set objBackOffice = nothing
    'Set objBoUser = nothing
    'Set objBoUserBills = nothing
    'set objBoCompany= nothing
end sub


end class
%>
