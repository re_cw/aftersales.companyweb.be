﻿<!-- #include virtual ="/classes/dal/connector.inc" -->
<%
Response.CodePage = 65001    
Response.CharSet = "utf-8"
 Dim myItems
    myItems = ("[ {type: ""buttonSelect"", id: ""new"", text: ""Nouveau"", img: ""new.gif"", img_disabled: ""new_dis.gif""}]")

Dim objsysMenu
    set objsysMenu = new sysMenu
    
    select case request.QueryString("a")         
    
            case "DYNXML_GetToolBarParentMenu"
                objsysMenu.DYNXML_GetToolBarParentMenu()
            case "DYNXML_GetToolBarChildMenu"
                objsysMenu.DYNXML_GetToolBarChildMenu(request.QueryString("id"))
			case "DYNXML_GetLowUsageChildMenu"
				objsysMenu.DYNXML_GetLowUsageChildMenu()
            case "DYNXML_GetSideBarMenu"
                objsysMenu.DYNXML_GetSideBarMenu(request.QueryString("id"))
            case "JSON_GetSideBarMenu"
                objsysMenu.JSON_GetSideBarMenu(request.QueryString("id"))
    
            case "XML_GetToolBarParentMenu" 'read
                objsysMenu.XML_GetToolBarParentMenu()    
            case "myItems"
        objsysMenu.ITMS_GetToolBarParentMenu()
            'response.write myItems        
    
    
    
    
    case else 'default should be read
    end select

   					
    
set objsysMenu = nothing


CONST C_GUEST_USER = 1
    CONST C_LOWEST_HR_FUNCTION_ID = 5
CONST C_DEFAULT_LANG = "FR"

class sysMenu
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property

    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property
    property get hr_function_id()
        hr_function_id = request.Cookies("TELESALESTOOLV2")("hrfunctionid")
        if len(trim(hr_function_id)) = 0 then hr_function_id = C_LOWEST_HR_FUNCTION_ID    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
	public function DYNXML_GetLowUsageChildMenu
		
	end function
    'this one is used for the dynamic toolbar generation
    public function DYNXML_GetToolBarParentMenu()
        DYNXML_GetToolBarParentMenu = pDYNXML_GetToolBarMenu(0) 
        Response.ContentType = "application/xml"
        Response.write DYNXML_GetToolBarParentMenu
    end function
    public function DYNXML_GetToolBarChildMenu(id)
    DYNXML_GetToolBarChildMenu = pDYNXML_GetToolBarMenu(id) : Response.ContentType = "application/xml": response.write DYNXML_GetToolBarChildMenu
    end function


    
    public function ITMS_GetToolBarParentMenu()
        response.write pITMS_GetToolBarParentMenu()
    end function
    
    public function XML_GetToolBarParentMenu()
        XML_GetToolBarParentMenu = pXML_GetToolBarParentMenu() : Response.ContentType = "application/xml": response.write XML_GetToolBarParentMenu
    end function   

    public function DYNXML_GetSideBarMenu(toolbarId)
        DYNXML_GetSideBarMenu = pDYNXML_GetSideBarMenu(toolbarId): Response.ContentType = "application/xml": response.write DYNXML_GetSideBarMenu
    end function

    public function JSON_GetSideBarMenu(toolbarId)
        Response.ContentType = "application/json" : response.write pJSON_GetSideBarMenu(toolbarId)
    end function

'<START REGION GLOBAL BUSINESS LOGIC>===========================================================



'<START REGION XML BUSINESS LOGIC>===========================================================



 '***** this one is used for the dynamic toolbar generation
 private function pDYNXML_GetToolBarMenu(toolbarId)
    Dim objConn, oRs, strSql,  s,d
    if toolbarId = 0 then
        strSql = "SELECT * FROM [dbo].[App_Sys_Menus]  WHERE (sys_menu_enabled=1 AND [sys_menu_parent_id] =0 AND sys_menu_role_level >=" & hr_function_id() & ") ORDER BY sys_menu_order"        
    else
        strSql = "SELECT * FROM [dbo].[App_Sys_Menus]  WHERE ([sys_menu_parent_id] =" & toolbarId & " AND sys_menu_enabled=1 AND sys_menu_role_level >=" & hr_function_id() & ") ORDER BY sys_menu_order" 
    end if
    

    Dim id, ttype, img, title, enabled, hidden, action, icon, pattern, sys_menu_parent_id 
        ttype= "sys_menu_type"
        id  = "sys_menu_id"
        text = "sys_menu_name_" &  Userlang
        title = "sys_menu_alt_" &  Userlang
        img = "sys_menu_img"
        pattern = "sys_menu_pattern"
        url = "sys_menu_url"

        'strsql = "select * from dbo.vw_app_sys_menus_XML_GetSysRootMenus"
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                with oRs
                    if not .bof and not .eof then
                     s= "<?xml version=""1.0"" encoding=""utf-8"" ?><toolbar>"
                        do while not .eof 
                        sys_menu_parent_id = .fields("sys_menu_parent_id")             
                            s=s & "<item type=""" & .fields(ttype) & """"
                            s=s & " id=""" & .fields(id) & """"
                            s=s & " text=""" & .fields(text) & """"
                            s=s & " title=""" & .fields(title) & """"
                            s=s & " img=""" & .fields(img) & """"
                            s=s & " img_disabled=""" & .fields(img) & "_dis.gif"""
                            s=s & ">" 
                            s=s & " <userdata name=""pattern"">" & .fields(pattern) & "</userdata>"
                            s=s & " <userdata name=""url"">" & .fields(url) & "</userdata>"
                            s=s & pDYNXML_GetToolBarChildMenu(.fields(id))
                            s=s & " </item>"
                        .movenext
                        loop   
                    end if
                End With
            s = s & "</toolbar>"
            
    'response.write server.htmlencode (s)
    'response.end
    response.write s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function  

private function pDYNXML_GetToolBarChildMenu(parentID)
    Dim objConn, oRs, strSql,  s,d
    if parentID <> 0 then
        strSql = "SELECT * FROM [dbo].[App_Sys_Menus]  WHERE ([sys_menu_parent_id] =" & parentID & " AND sys_menu_enabled=1 AND sys_menu_role_level >=" & hr_function_id() & ") ORDER BY sys_menu_order" 
    end if
    

    Dim id, ttype, img, title, enabled, hidden, action, icon, pattern 
        ttype= "sys_menu_type"
        id  = "sys_menu_id"
        text = "sys_menu_name_" &  Userlang
        title = "sys_menu_alt_" &  Userlang
        img = "sys_menu_img"
        pattern = "sys_menu_pattern"
        url = "sys_menu_url"

        'strsql = "select * from dbo.vw_app_sys_menus_XML_GetSysRootMenus"
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString , adopenstatic, adcmdreadonly, adcmdtext
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                with oRs
                    if not .bof and not .eof then
                     
                        do while not .eof                
                            s=s & "<item type=""" & .fields(ttype) & """"
                            s=s & " id=""" & .fields(id) & """"
                            s=s & " text=""" & .fields(text) & """"
                            s=s & " title=""" & .fields(title) & """"
                            s=s & " img=""" & .fields(img) & """"
                            s=s & " img_disabled=""" & .fields(img) & "_dis.gif"""
                            s=s & " >" 
                            s=s & " <userdata name=""pattern"">" & .fields(pattern) & "</userdata>"
                            s=s & " <userdata name=""url"">" & .fields(url) & "</userdata>"
                            s=s & " </item>"
                        .movenext
                        loop   
                    end if
                End With
            
            pDYNXML_GetToolBarChildMenu = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function



    private function pJSON_GetSideBarMenu(toolbarId)
    Dim objConn, oRs, strSql,  s,d
        strSql = "SELECT * FROM [dbo].[App_Sys_Menus]  WHERE ([sys_menu_parent_id] =" & toolbarId & " AND sys_menu_role_level >=" & usr_role_id & ")"
    Dim id, ttype, img, title, enabled, hidden, action, icon, pattern
        ttype= "sys_menu_type"
        id  = "sys_menu_id"
        text = "sys_menu_name_" &  Userlang
        title = "sys_menu_alt_" &  Userlang
        img = "sys_menu_img"
        pattern = "sys_menu_pattern"
        url = "sys_menu_url"
        'strsql = "select * from dbo.vw_app_sys_menus_XML_GetSysRootMenus"
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open sConnStrDev , adopenstatic, adcmdreadonly, adcmdtext
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                with oRs
                    if not .bof and not .eof then
                    s= "{items :["
                        do while not .eof                
                            's=s & "{item" 'type=""" & .fields(ttype) & """"
                            s=s & "{ id:""" & .fields(id) & """"
                            s=s & " ,text:""" & .fields(text) & """"
                            s=s & " ,title:""" & .fields(title) & """"
                            s=s & " ,icon:""" & .fields(img) & """"
                            s=s & " ,pattern:""" & .fields(pattern) & """"
                            s=s & " ,url:""" & .fields(url) & """"
                            s=s & " },"
                        .movenext
                        loop   
                    end if
                End With
                s = left(s,len(s)-1)
                s = s & "]}"
            pJSON_GetSideBarMenu = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function 



private function pDYNXML_GetSideBarMenu(toolbarId)
    Dim objConn, oRs, strSql,  s,d
        strSql = "SELECT * FROM [dbo].[App_Sys_Menus]  WHERE ([sys_menu_parent_id] =" & toolbarId & " AND sys_menu_role_level >=" & usr_role_id & ")"
    Dim id, ttype, img, title, enabled, hidden, action, icon
        ttype= "sys_menu_type"
        id  = "sys_menu_id"
        text = "sys_menu_name_" &  Userlang
        title = "sys_menu_alt_" &  Userlang
        img = "sys_menu_img"
        pattern = "sys_menu_pattern"
        url = "sys_menu_url"
        'strsql = "select * from dbo.vw_app_sys_menus_XML_GetSysRootMenus"
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open sConnStrDev , adopenstatic, adcmdreadonly, adcmdtext
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                with oRs
                    if not .bof and not .eof then
                    s= "<?xml version=""1.0"" encoding=""utf-8"" ?><toolbar>"
                        do while not .eof                
                            s=s & "<item" 'type=""" & .fields(ttype) & """"
                            s=s & " id=""" & .fields(id) & """"
                            s=s & " text=""" & .fields(text) & """"
                            s=s & " title=""" & .fields(title) & """"
                            s=s & " icon=""" & .fields(img) & """"
                            s=s & " pattern=""" & .fields(pattern) & """"
                            s=s & " url=""" & .fields(url) & """"
                            s=s & " />"
                        .movenext
                        loop   
                    end if
                End With
            s = s & "</toolbar>"
            pDYNXML_GetSideBarMenu = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function 



 private function pXML_GetToolBarParentMenu()
  ' working in the dhtmlX framework with myObj.loadStruct (xml)     
    Dim objConn, oRs, strSql, EventTypeName, s,d, sys_menu_img, pattern
       
        sys_menu_name = "sys_menu_name_" &  Userlang
        sys_menu_alt = "sys_menu_alt_" &  Userlang
        sys_menu_img = "sys_menu_img"
        pattern = "sys_menu_pattern"
        url = "sys_menu_url"
        strsql = "select * from dbo.vw_app_sys_menus_XML_GetSysRootMenus"
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open sConnStrDev , adopenstatic, adcmdreadonly, adcmdtext
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                                    with oRs
                    if not .bof and not .eof then
                    s = .fields(0)                    
                    end if
                End With
     Response.ContentType = "application/xml"       
     
            pXML_GetToolBarParentMenu = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

    end function


    private function pITMS_GetToolBarParentMenu()
    Dim objConn, oRs, strSql,  s,d
        strSql = "SELECT * FROM [dbo].[App_Sys_Menus]  WHERE (sys_menu_id > 0 AND sys_menu_role_level >=" & usr_role_id & ")"
    Dim id, ttype, img, title, enabled, hidden, action, pattern
        ttype= "sys_menu_type"
        id  = "sys_menu_id"
        text = "sys_menu_name_" &  Userlang
        title = "sys_menu_alt_" &  Userlang
        img = "sys_menu_img"
        pattern = "sys_menu_pattern"
    url = "sys_menu_url"
        'strsql = "select * from dbo.vw_app_sys_menus_XML_GetSysRootMenus"
            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open sConnStrDev , adopenstatic, adcmdreadonly, adcmdtext
                oRs.cursorlocation = 3
                oRs.open strSql, objConn
                with oRs
                    if not .bof and not .eof then
                    s= "["
                        do while not .eof                
                        s=s & "{type: """ & .fields(ttype) & """, id: """ & .fields(id) & """, text: """ & .fields(text) & """, title: """ & .fields(title) & """, img: """ & .fields(img) & """, img_disabled: """ & .fields(img) & "_dis.gif"", pattern: """ & .fields(pattern) & """, url: """ & .fields(url) & """},"
                        .movenext
                        loop   
                    end if
                End With
                s = left(s,len(s)-1)
                s = s & "]"
 
            pITMS_GetToolBarParentMenu = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing

end function
'<END REGION XML BUSINESS LOGIC>=============================================================



end class

%>
