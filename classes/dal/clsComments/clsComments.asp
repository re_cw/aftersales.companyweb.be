﻿<!-- #include virtual ="/classes/obj/objComment.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

Dim objComment, objsysComment
   
    set objsysComment = new sysComments
    set objComment = new clsObjComment

        select case request.QueryString("a")         
            case "AddComment" 'add
                objComment.populateCommentObject
                objsysComment.AddComment
            case "readComment"
                response.write request("comment_id")
                response.write  request("comment_event_type_id") 
                response.write  request("comment_usr_id") 
                response.write  request("comment_event_id") 
                response.write  request("comment_lead_id") 
                response.write  request("comment_company_vat") 
				response.write  request("comment_company_id")
                response.write  request("comment_date") 
                response.write  request("comment_time") 
                response.write  request("comment") 
                response.write  request("AuditUserUpdated")  
                response.write  request("trusted")
            case else 'default should be read
        end select




class sysComments

    Dim m_lang_radical
    Dim m_usr_id
    Dim m_usr_role_id
    Dim m_userLang

    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    
    
Public function AddComment(objComment)
    AddComment = pAddComment(objComment)
end function

 '<START REGION PRIVATE BUSINESS LOGIC>----------------------------------------------------------


Private function pAddComment(objComment)
    Dim objConn, objRS, strSql, sErrMsg, objCmd
        blnNew=false
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then set objConn = nothing : exit function
        
        strSql="dbo.Comments_ADD"                    
            
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@comment_event_type_id", 5, 1, len(objComment.comment_event_type_id), objComment.comment_event_type_id)
                .Parameters.Append .CreateParameter("@comment_usr_id", 5, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@comment_event_id", 8, 1, len(objComment.comment_event_id), objComment.comment_event_id)
                .Parameters.Append .CreateParameter("@comment_lead_id", 8, 1, 1, 0)
                .Parameters.Append .CreateParameter("@comment_company_vat", 8, 1, len(objComment.comment_company_vat), objComment.comment_company_vat)
				.Parameters.Append .CreateParameter("@comment_company_id", 8, 1, len(objComment.comment_company_id), objComment.comment_company_id)
                .Parameters.Append .CreateParameter("@comment_date", 5, 1, len(objComment.comment_date), objComment.comment_date)
                .Parameters.Append .CreateParameter("@comment_time", 8, 1, len(objComment.comment_time), objComment.comment_time)
                .Parameters.Append .CreateParameter("@comment", 8, 1, len(objComment.comment), objComment.comment)

                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(objComment.comment_usr_id), objComment.comment_usr_id)
                .Parameters.Append .CreateParameter("@comment_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
            pAddComment=sErrMsg
            set objRs= nothing
            
            comment_ID=objCmd(10)
 
    end function


    private function formatTimeStamp(sdate, stime)
    dim dd, mm, yy
        yy = left(sdate, 4)
        dd = right(sdate, 2)
        mm = mid(sdate,5,2)
    formatTimeStamp = dd & "-" & mm & "-" & yy & " " & stime
    end function

    public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    public function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function

'<end region PRIVATE METHODS & FUNCTIONS>
    public sub Class_Terminate()
        'set objsysComment = nothing
        'set objComment = nothing 
    end sub

'<END REGION PRIVATE BUSINESS LOGIC>----------------------------------------------------------
end class



%>