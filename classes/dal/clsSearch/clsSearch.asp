﻿
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/dal/clsComments/clsComments.asp" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_GUEST_USER = 1
CONST C_DEFAULT_LANG = "FR"

Dim objSearch
Set objSearch = new sysSearch   

    select case request.querystring("q")
        case "GetTsCompanyByEventID" 
            objSearch.GetTsCompanyByEventID()
        case "GetTsCompanyByCompanyID"
            objSearch.GetTsCompanyByCompanyID() 
        case "GetTsCompanyByBackOfficeID"
            objSearch.GetTsCompanyByBackOfficeID() 
        case "GetTsCompanyByCompanyVat"
            objSearch.GetTsCompanyByCompanyVat()
        case "GetTsCompanyByTTID"
            objSearch.GetTsCompanyByTTID()
        case "GetTsCompanyByCompanyName"
            objSearch.GetTsCompanyByCompanyName()
        case "GetTsCompanyHistoryByEventID"   
            objSearch.GetTsCompanyHistoryByEventID()
        case "GetTsCompanyByContact"
            objSearch.GetTsCompanyByContact()
        case "GetTsCompanyByString"
            objSearch.JSONGetTsCompanyByString()
        case "GetCompanyBackOfficeInfo"
            objSearch.JSONGetCompanyBackOfficeInfo()
        case "GetCompanyBackOfficeInfoByString"
            objSearch.JSONGetCompanyBackOfficeInfoByString()
        case "UpdateTsEventUserID"
            objSearch.UpdateTsEventUserID()
		case "UpdateTsCompanyIsBlacklisted"
			objSearch.UpdateTsCompanyIsBlacklisted()
        case else
    end select


class sysSearch

    Dim m_company_id_list

    '*** connection strings ***
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
    
    Property get firstname()
        firstname="" : firstname=request.Cookies("TELESALESTOOLV2")("firstname")
    end property

    Property get lastname()
        lastname="" : lastname=request.Cookies("TELESALESTOOLV2")("lastname")
    end property

    property get usr_id()
        usr_id="" : usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property

    property get event_id()
        event_id=trim(request.querystring("event_id")) : if len(event_id) = 0 then event_id = 0
    end property

    property get company_id()
        company_id=trim(request.querystring("company_id")) : if len(company_id) = 0 then company_id = 0
    end property

    property get company_id_list() : company_id_list = m_company_id_list : end property 
    property let company_id_list(varval) : m_company_id_list = varval : end property


    property get company_hbc_id()
        company_hbc_id=trim(request.querystring("company_hbc_id")) : if len(company_hbc_id) = 0 then company_hbc_id = 0
    end property

    property get company_vat()
        company_vat=trim(request.querystring("company_vat")) : if len(company_vat) = 0 then company_vat = -1
    end property
    property get tt_id()
        tt_id=trim(request.querystring("tt_id")) : if len(tt_id) = 0 then tt_id = 0
    end property

    property get company_name()
        company_name=Urldecode(trim(request.querystring("company_name"))) : if len(company_name) = 0 then company_name = -1
    end property
    property get cbxstrOpt()
        cbxstrOpt=trim(request.querystring("cbxstrOpt")) : if len(cbxstrOpt) = 0 then company_name = 0
    end property
    
    property get fieldname()
        fieldname=UrlDecode(trim(request.querystring("fieldname"))) : if len(fieldname) = 0 then fieldname = -1
    end property
    property get fieldvalue()
        fieldvalue=UrlDecode(trim(request.querystring("fieldvalue"))) : if len(fieldvalue) = 0 then fieldvalue = -1
    end property

    property get postCode()
        postCode=trim(request.querystring("postCode")) : if len(postCode) = 0 then postCode = "0"
    end property

    public function UpdateTsEventUserID()
        Response.ContentType = "application/json" : response.write pUpdateTsEventUserID()
    end function
	
	public function UpdateTsCompanyIsBlacklisted()
        Response.ContentType = "application/json" : response.write pUpdateTsCompanyIsBlacklisted()
    end function

    public function GetTsCompanyByEventID()
        Response.ContentType = "application/json" : response.write pJSONGetTsCompanyByID(1)
    end function
    public function GetTsCompanyByCompanyID()
        Response.ContentType = "application/json" : response.write pJSONGetTsCompanyByID(2)
    end function
    public function GetTsCompanyByBackOfficeID()
        Response.ContentType = "application/json" : response.write pJSONGetTsCompanyByID(3)
    end function
    public function GetTsCompanyByCompanyVat()
        Response.ContentType = "application/json" : response.write pJSONGetTsCompanyByID(4)
    end function
    public function GetTsCompanyByTTID()
        Response.ContentType = "application/json" : response.write pJSONGetTsCompanyByID(5)
    end function
    public function GetTsCompanyHistoryByEventID()
            Response.ContentType = "application/json" : response.write pJSONGetTsCompanyHistoryByEventID()
    end function
    public function GetTsCompanyByContact()
        dim ids
        idList = pJSONGetTsCompanyByContact()

        Response.ContentType = "application/json" : response.write pJSONGridGetTsCompanyByCompanyID(idList)
    end function

    public function GetTsCompanyByCompanyName()
        Response.ContentType = "application/json" : response.write pJSONGridGetTsCompanyByCompanyName()
    end function
    public function JSONGetTsCompanyByString()
        Response.ContentType = "application/json" : response.write pJSONGetTsCompanyByString()
    end function
    
    
    public function JSONGetCompanyBackOfficeInfo() 
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeInfo()
    end function
    
    public function JSONGetCompanyBackOfficeInfoByString() 
        Response.ContentType = "application/json" : response.write pJSONGetCompanyBackOfficeInfoByString()
    end function
    public function GetTsCompanyByPhone()

    end function



'<START REGION>PRIVATE FUNCTIONS AND METHODS
function URLDecode(sText)
    sDecoded = sText
    
    Set oRegExpr = Server.CreateObject("VBScript.RegExp")
    oRegExpr.Pattern = "%[0-9,A-F]{2}"
    oRegExpr.Global = True
    
    Set oMatchCollection = oRegExpr.Execute(sText)

    For Each oMatch In oMatchCollection
        sDecoded = Replace(sDecoded,oMatch.value,Chr(CInt("&H" & Right(oMatch.Value,2))))
    Next
    URLDecode = sDecoded
End function
Private Function IsNotNullorEmpty(input)
    dim IsNotNullorEmpty_Output
    IsNotNullorEmpty_Output = true
    'vbEmpty (uninitialized) or vbNull (no valid data)
    if VarType(input) = 0 OR VarType(input) = 1 Then
        IsNotNullorEmpty_Output = false
    End if
    If VarType(input) = 8 Then
        If input = "" Then 
            IsNotNullorEmpty_Output = False
        End If
    End If
    IsNotNullorEmpty = IsNotNullorEmpty_Output

End Function

    Private function pUpdateTsEventUserID()
        pUpdateTsEventUserID = 0
        
            Dim oComment : set oComment = new sysComments     
            Dim objComment : set objComment = new clsObjComment

        Dim objConn, objRS, strSql, sWhere, sText, sOwned
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if
        select case Userlang()
            case "FR"
                sOwned = "appartient maintenant à "
            case "NL"
                sOwned = "is nu eigendom van "
            case else
                sOwned = "is now owned by "
        end select
    
        strSql= "SELECT TOP 1 * FROM dbo.events WHERE id=" & event_id()
        sText= "EVENT ID [ " & event_id() & " ] " & sOwned & ": " & firstname() & " " & lastname() & " (user id:" & usr_id() & ")"
        
        'on error resume next        
        objConn.beginTrans
    
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pUpdateTsEventUserID()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

        with objRS
            if not .bof and not .eof then
                .fields("usr_id") = usr_id()
                .fields("text") = sText
                .fields("readonly") = NULL
                .fields("visible") = 1
                .fields("event_type_id") = 15
				.fields("start_date") = SetDateTimeWithMinutes(0)
				.fields("end_date") = SetDateTimeWithMinutes(5)

                objComment.comment_event_type_id = .fields("event_type_id")
                objComment.comment_usr_id = usr_id() 
                objComment.comment_event_id = 15
                'objComment.comment_company_vat = .fields("company_vat")
                objComment.comment_date = SetDate() 
                objComment.comment_time = SetTime() 
                objComment.comment = sText 
                objComment.AuditUserUpdated = usr_id()  
                objComment.trusted = 1

            .update
            objConn.CommitTrans
    
            pUpdateTsEventUserID = 1
            end if
        end with

        if Err.number <> 0 then
    
            objConn.RollbackTrans
            pUpdateTsEventUserID = 0
        else    
            oComment.AddComment(objComment)
    pUpdateTsEventUserID = 1
        end if    

        'objRS.close : set objRS = nothing
        'objConn.close : set objConn = nothing
    end function
	
	Private function pUpdateTsCompanyIsBlacklisted()
		'Check if user has rights
		if usr_role_id() = "2" OR usr_role_id() = "1"  then
			pUpdateTsCompanyIsBlacklisted = 0
			
			Dim oComment : set oComment = new sysComments     
			Dim objComment : set objComment = new clsObjComment

			Dim objConn, objRS, strSql, sWhere, sText, sBlacklisted, comment_ID
			set objConn=Server.CreateObject("ADODB.Connection")
			with objConn
				.ConnectionString = SalesToolConnString()
				.CursorLocation = 3
				.Open    
			end with
			if objConn.State = 0 then set objConn = nothing : exit function : end if
			select case Userlang()
				case "FR"
					sBlacklisted = "a été mis sur la liste noire par "
				case "NL"
					sBlacklisted = "is toegevoegd aan de blacklist door "
				case else
					sBlacklisted = "has been blacklisted by "
			end select
		
			strSql = "select top 1 c.* 	from events e join companies c on c.company_id = e.company_id where e.id = " & event_id()
			
			sText= "EVENT ID [ " & event_id() & " ] " & sBlacklisted & ": " & firstname() & " " & lastname() & " (user id:" & usr_id() & ")"
			
			'on error resume next        
			objConn.beginTrans
		
			set objRS=Server.CreateObject("ADODB.Recordset") 
			objRS.open strSql, objConn, 1, 3
			If Err.number <> 0 then
				sErrMsg = Err.Description & "Source : " & Err.Source & _
				"<br />Error in Class: sysSearch.pUpdateTsCompanyIsBlacklisted()" & _
				"<br />while triggering [set objRS = .Execute]. -> :" & strsql
			end if

			with objRS
				if not .bof and not .eof then
					.fields("company_state")= 1
					.fields("AuditDateUpdated")=now()
					.fields("AuditUserUpdated")=usr_id()
					
					.update
					objConn.commitTrans
				end if
			end with

			if Err.number <> 0 then
				objConn.RollbackTrans
				pUpdateTsCompanyIsBlacklisted = 0
			else    
				oComment.AddComment(objComment)
				pUpdateTsCompanyIsBlacklisted = 1
			end if    
			
			objConn.beginTrans
		
			strSql = "SELECT TOP 1 * FROM [dbo].[comments]" 
		
			set objRS=Server.CreateObject("ADODB.Recordset") 
			objRS.open strSql, objConn, 1, 3
			If Err.number <> 0 then
				sErrMsg = Err.Description & "Source : " & Err.Source & _
				"<br />Error in Class: sysSearch.pUpdateTsCompanyIsBlacklisted()" & _
				"<br />while triggering [set objRS = .Execute]. -> :" & strsql
			end if

			with objRS
				.addNew
					.fields("comment_event_type_id")= 21
					.fields("comment_usr_id")=usr_id()
					.fields("comment_event_id")=event_id()
					.fields("comment_lead_id")=0
					.fields("comment_company_vat")= company_vat()
					.fields("comment_company_id")= company_id()
					.fields("comment_date")= SetDate()
					.fields("comment_time")= SetTime()
					.fields("comment")=sText
					.fields("AuditUserCreated")=usr_id()
				.update
				 comment_ID =  toUnicode(.fields("comment_id"))
			end with
			
			objConn.commitTrans
			
			
		end if
        objRs.Close : set objRS = nothing
        objConn.close : set objConn = nothing  
    end function

    Private function pJSONGetTsCompanyByID(sourceID)
        
        Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, vwCompany, event_type_name
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if

        vwCompany = "dbo.vw_GetCompany_" & Userlang()
        event_type_name = "event_type_name_" & userlang()
        strSql = "SELECT dbo.vw_Users.*, dbo.event_types.*, dbo.events.*," & vwCompany & ".* " 
		strSql = strSql & " ,CONVERT(VARCHAR(10), events.start_date, 112) AS us_start_date "
		strSql = strSql & " ,CONVERT(VARCHAR(10), events.end_date, 112) AS us_end_date "
		strSql = strSql & " FROM dbo.events RIGHT OUTER JOIN " & vwCompany & " ON dbo.events.company_id = " & vwCompany & ".company_id LEFT OUTER JOIN event_types ON events.event_type_id = event_types.event_type_id  LEFT OUTER JOIN vw_Users ON events.usr_id = vw_Users.usr_id " 
    
        select case sourceID
			case 1
				sWhere = " where id=" & event_id()
			case 2
				sWhere = " where " & vwCompany & ".company_id=" & company_id()
			case 3
				sWhere = " where company_hbc_id=" & company_hbc_id()
			case 4
				sWhere = " where " & vwCompany & ". company_vat=" & company_vat()
			case 5
				sWhere = " where tt_id=" & tt_id()
			case 6
				sWhere = " where " & vwCompany & ".company_id in (" & company_id_list() & ")"
        end select

        strSql = strSql & sWhere & " ORDER BY start_date DESC"
               
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGetTsCompanyByID()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

		s= "["
		
        with objRS
        if not .bof and not .eof then
			Dim nbc_desc, jfc_s_desc,  s
			nbc_desc = "nbc_desc_" & userlang()
			jfc_s_desc = "jfc_s_desc_" & userlang()
        
			do while not .eof
				s=s & "{"
				s=s & " ""id"":""" & toUnicode(trim(toUnicode(.fields("id")))) & """"
				s=s & " ,""company_id"":""" & toUnicode(.fields("company_id")) & """"
				s=s & " ,""company_address_id"":""" & toUnicode(.fields("company_address_id")) & """"
				s=s & " ,""company_hbc_id"":""" & toUnicode(.fields("company_hbc_id")) & """"
				s=s & " ,""event_type_id"":""" & toUnicode(trim(.fields("event_type_id"))) & """"
				s=s & " ,""usr_id"":""" & toUnicode(trim(.fields("usr_id"))) & """"
				s=s & " ,""tt_id"":""" & toUnicode(trim(.fields("tt_id"))) & """"
				s=s & " ,""start_date"":""" & toUnicode(trim(.fields("start_date"))) & """"
				s=s & " ,""end_date"":""" & toUnicode(trim(.fields("end_date"))) & """"
				s=s & " ,""text"":""" & toUnicode(trim(toUnicode(.fields("text")))) & """"
				s=s & " ,""event_type_name"":""" & toUnicode(trim(.fields(event_type_name))) & """"
				s=s & " ,""event_type_textColor"":""" & toUnicode(trim(.fields("event_type_textColor"))) & """"
				s=s & " ,""event_type_color"":""" & toUnicode(trim(.fields("event_type_color"))) & """"
				s=s & " ,""event_type_icon"":""" & toUnicode(trim(.fields("event_type_icon"))) & """"
				s=s & " ,""company_vat"":""" & toUnicode(trim(.fields("company_vat"))) & """"
				s=s & " ,""company_name"":""" & toUnicode(trim(.fields("company_name"))) & """"
				s=s & " ,""lang_radical"":""" & toUnicode(trim(.fields("lang_radical"))) & """"
				s=s & " ,""lang_name"":""" & toUnicode(trim(.fields("lang_name"))) & """"
				s=s & " ,""jfc_s_desc"":""" & toUnicode(trim(.fields(jfc_s_desc))) & """"
				s=s & " ,""company_address_street"":""" & toUnicode(trim(.fields("company_address_street"))) & """"
				s=s & " ,""company_address_number"":""" & toUnicode(trim(.fields("company_address_number"))) & """"
				s=s & " ,""company_address_boxnumber"":""" & toUnicode(trim(.fields("company_address_boxnumber"))) & """"
				s=s & " ,""company_address_floor"":""" & toUnicode(trim(.fields("company_address_floor"))) & """"
				s=s & " ,""company_address_postcode"":""" & toUnicode(trim(.fields("company_address_postcode"))) & """"
				s=s & " ,""company_address_localite"":""" & toUnicode(trim(.fields("company_address_localite"))) & """"
				s=s & " ,""nbc_desc"":""" & toUnicode(trim(.fields(nbc_desc))) & """"
				s=s & " ,""company_nace"":""" & toUnicode(trim(.fields("company_nace"))) & """"
				s=s & " ,""company_tel"":""" & toUnicode(trim(.fields("company_tel"))) & """"
				s=s & " ,""company_mobile"":""" & toUnicode(trim(.fields("company_mobile"))) & """"
				s=s & " ,""company_mail"":""" & toUnicode(trim(.fields("company_mail"))) & """"
				s=s & " ,""firstname"":""" & toUnicode(trim(toUnicode(.fields("firstname")))) & """"
				s=s & " ,""lastname"":""" & toUnicode(trim(toUnicode(.fields("lastname")))) & """"
				s=s & " ,""extension"":""" & toUnicode(trim(toUnicode(.fields("extension")))) & """"
				s=s & " ,""us_start_date"":""" & toUnicode(trim(.fields("us_start_date"))) & """"
				s=s & " ,""us_end_date"":""" & toUnicode(trim(.fields("us_end_date"))) & """"
				s=s & " ,""errmsg"":""" & toUnicode(sErrMsg) & """"
				s=s & " ,""strsql"":""" & toUnicode(strSql) & """"
				s=s & " },"
				.movenext
			loop
            s = left(s,len(s)-1)
		end if
	end with
		
	s = s & "]"
        
    pJSONGetTsCompanyByID = s
    objRS.close : set objRS = nothing
    objConn.close : set objConn = nothing

    end function


    private function pJSONGetTsCompanyHistoryByEventID()
        Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, event_type_name, strComment
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if
        event_type_name = "event_type_name_" & userlang()
        strSql = "select comment_id, comment_date,comment_time, event_type_name_" & userlang() & " as event_type_name "
        strSql = strSql & ", ISNULL(REPLACE(REPLACE([comment], CHAR(10), ''), CHAR(13), '<br>'), '') as comment "
        strSql = strSql & " from [dbo].[vw_GetComments] where [comment_event_id]=" & event_id() & " order by auditdatecreated desc"

         on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGetTsCompanyByID()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if
		
		if not objRs.bof and not objRs.eof then
			s = "[" & RStoJSON(objRs) & "]"
		else
			s = "[]"
		end if

        pJSONGetTsCompanyHistoryByEventID = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end function


    private function pJSONGetTsCompanyByContact()
    if fieldName() = -1 then exit function
    if fieldvalue() = -1 then exit function    
        Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, event_type_name, strComment
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if
        strSql = "SELECT distinct company_id from vw_GetCompanyByContactInfo "
        sWhere = " WHERE 1=1 "
        Dim strCriterion
        strCriterion = fieldvalue()
        strCriterion = replace(strCriterion, "'", "''",1,-1, vbtextcompare)
    
        Dim strOpt
        select case cbxstrOpt()
            case 0
                strOpt = " like '" & strCriterion & "'"
            case 1
                strOpt = " like '" & strCriterion & "%'"
				'HAX to search for x% and not %x%
            case 2
                strOpt = " like '" & strCriterion & "%'"
            case 3
                strOpt = " like '%" & strCriterion & "'"
            case else
                strOpt = " like '" & strCriterion & "'"
        end select

        Dim sAnd
        select case fieldName()
            case "@contact"
                sAnd = " AND( contact_firstname" & strOpt & " OR contact_lastname " & strOpt & ")"
            case "@phone"
                sAnd = " AND( company_tel" & strOpt & " OR company_mobile " & strOpt & " OR contact_tel " & strOpt & " OR contact_mobile " & strOpt & ")"
			case "@contactFirstName"
				sAnd = " AND (contact_firstname" & strOpt & ")"
			case "@contactLastName"
				sAnd = " AND (contact_lastname" & strOpt & ")"
        end select
    
        strSql = strSql & sWhere & sAnd
        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGetTsCompanyByContact()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if
        s=0
        with objRS
            if not .bof and not .eof then       
            .moveFirst 
                    do while not .eof
                        s = s   & toUnicode(.fields("company_id")) & ","
                    .movenext
                loop       
            s = left(s, (len(s)-1))
            
        end if
        end with

        pJSONGetTsCompanyByContact = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end function



    private function pJSONGridGetTsCompanyByCompanyID(idList)
        'if company_name()=-1 then exit function
    
        Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, vwCompany, event_type_name
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        
        if objConn.State = 0 then set objConn = nothing : exit function : end if
        vwCompany = "dbo.vw_GetCompany_" & Userlang()
        strSql = "SELECT " & vwCompany & ".* , dbo.events.id as event_id  FROM " & vwCompany & " LEFT OUTER JOIN dbo.events on " & vwCompany & ".company_id = dbo.events.company_id"

            sWhere = " where " & vwCompany & ".company_id in (" & idList & ")"
    
        strSql = strSql & sWhere
  
        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGridGetTsCompanyByCompanyID()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

        s= "{""rows"":["
        with objRS
            if not .bof and not .eof then       
            .moveFirst 
                 
                    do while not .eof
                        s = s & "{""id"": """ & toUnicode(.fields("company_id")) & """, "
                        s = s & """data"": [ "
                        s = s & """" & toUnicode(.fields("company_id")) & ""","
                        s = s & """" & toUnicode(.fields("company_name"))  & """, "
                        s = s & """" & toUnicode(.fields("company_vat"))  & """, "
                        s = s & """" & toUnicode(.fields("company_tel"))  & """, "
                        s = s & """" & toUnicode(.fields("event_id"))  & """, "
                        s = s & """" & toUnicode(.fields("company_hbc_id"))  & """, "
                        s = s & """" & toUnicode(.fields("company_address_id"))  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
              
            end if
        end with
        s = s & "]}"

        pJSONGridGetTsCompanyByCompanyID = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end function



    private function pJSONGridGetTsCompanyByCompanyName()
        if company_name()=-1 then exit function
    
        Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, vwCompany, event_type_name
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if
        vwCompany = "dbo.vw_GetCompany_" & Userlang()
        strSql = "SELECT " & vwCompany & ".* , dbo.events.id as event_id  FROM " & vwCompany & " LEFT OUTER JOIN dbo.events on " & vwCompany & ".company_id = dbo.events.company_id"
    
        Dim strCriterion
        strCriterion = company_name() 
        strCriterion = replace(strCriterion, "'", "''",1,-1, vbtextcompare)
        
    select case cbxstrOpt()
        case 0
            sWhere = " where company_name like '" & strCriterion & "'"
        case 1
            sWhere = " where company_name like '" & strCriterion & "%'"
        case 2
            sWhere = " where company_name like '%" & strCriterion & "%'"
        case 3
            sWhere = " where company_name like '%" & strCriterion & "'"
        case else
            sWhere = " where company_name like '" & strCriterion & "'"
        end select
    
        strSql = strSql & sWhere
    
        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGridGetTsCompanyByCompanyName()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if
        s= "{""rows"":["
        with objRS
            if not .bof and not .eof then       
            .moveFirst 
                    do while not .eof
                        s = s & "{""id"":""" & toUnicode(.fields("company_id")) & """, ""data"": [""" & toUnicode(.fields("company_id")) & """,""" & toUnicode(.fields("company_name"))  & """, """ & toUnicode(.fields("company_vat"))  & """, """ & toUnicode(.fields("company_tel"))  & """, """ & toUnicode(.fields("event_id"))  & """, """ & toUnicode(.fields("company_hbc_id"))  & """, """  & toUnicode(.fields("company_address_id"))  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))  
            end if
        end with
        s = s & "]}"
        pJSONGridGetTsCompanyByCompanyName = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end function


private function pJSONGetTsCompanyByString()
    Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, vwCompany, event_type_name
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if
        vwCompany = "dbo.vw_GetCompany_" & Userlang()
        strSql = "SELECT " & vwCompany & ".* , dbo.events.id as event_id  FROM " & vwCompany & " LEFT OUTER JOIN dbo.events on " & vwCompany & ".company_id = dbo.events.company_id"
        
        Dim strCriterion
        strCriterion = fieldvalue() 
        strCriterion = replace(strCriterion, "'", "''",1,-1, vbtextcompare)
        
    select case cbxstrOpt()
        case 0
            sWhere = " where " & fieldName() & " like '" & strCriterion & "'"
        case 1
            sWhere = " where " & fieldName() & " like '" & strCriterion & "%'"
        case 2
            sWhere = " where " & fieldName() & " like '%" & strCriterion & "%'"
        case 3
            sWhere = " where " & fieldName() & " like '%" & strCriterion & "'"
        case else
            sWhere = " where " & fieldName() & " like '" & strCriterion & "'"
        end select
    
        strSql = strSql & sWhere 

        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGridGetTsCompanyByCompanyName()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

        pJSONGetTsCompanyByString = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing

end function

'BACK OFFICE DATA LAYER
private function pJSONGetCompanyBackOfficeInfo()
   
    Dim objConn, objCmd, objParam, objRS, intParamIndex, strSql, sErrMsg

    strSql = "dbo.ST_GetBackOfficeCompanyInfo"    
    a = company_vat()
    b = userlang()
    c = postCode()
    d= company_hbc_id()
    
    Dim jfc_s_desc, jfc_l_desc, nbc_desc, HBcon_Name
        jfc_s_desc = "jfc_s_desc_" + b
        jfc_l_desc = "jfc_l_desc_" + b
        nbc_desc = "nbc_desc_" + b
        HBcon_Name = "HBcon_Name_" + b

    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with

    'parameters types: '5: adDouble - '8: adBSTR
    if objConn.State = 1 then    
        set objCmd = Server.CreateObject("ADODB.Command")
        with objCmd
            .ActiveConnection = objConn
            .CommandType = 4 'adCmdStoredProc
            .CommandText =  strSql
            .NamedParameters = True
            .Parameters.Append .CreateParameter("@vatcode", 8, 1, len(a), a) 
            .Parameters.Append .CreateParameter("@postcode", 8, 1, len(c), c)
            .Parameters.Append .CreateParameter("@hbc_id", 8, 1, len(d), d)  
        on error resume next
            set objRS = Server.CreateObject("ADODB.Recordset")
            set objRS = .Execute()
    
                If Err.number <> 0 then
                    sErrMsg = Err.Description & "Source : " & Err.Source & _
                    "<br />Error in Class: sysSearch.pJSONGetCompanyBackOfficeInfo()" & _
                    "<br />while triggering [set objRS = .Execute]."
                    response.write sErrMsg
                    exit function
                end if
         end with       
                            s= "["
                with objRS
                    if not .bof and not .eof then
                        do while not .eof
                             s=s & "{"
                            s=s & " ""HBC_ID"":""" & toUnicode(.fields("HBC_ID")) & """"
                            s=s & " ,""HBC_Firmvat"":""" & toUnicode(.fields("HBC_Firmvat")) & """"
                            s=s & " ,""HBC_FirmName"":""" & toUnicode(.fields("HBC_FirmName")) & """"
                            s=s & " ,""HBC_ContactName"":""" & toUnicode(.fields("HBC_ContactName")) & """"
                            s=s & " ,""HBC_Street"":""" & toUnicode(.fields("HBC_Street")) & """"
                            s=s & " ,""HBC_HouseNr"":""" & toUnicode(.fields("HBC_HouseNr")) & """"
                            s=s & " ,""HBC_BusNr"":""" & toUnicode(.fields("HBC_BusNr")) & """"                        
                            s=s & " ,""HBC_Postcode"":""" & toUnicode(.fields("HBC_Postcode")) & """"
                            s=s & " ,""HBC_gemeente"":""" & toUnicode(.fields("HBC_gemeente")) & """"
                            s=s & " ,""HBC_fax"":""" & toUnicode(.fields("HBC_fax")) & """"
                            s=s & " ,""HBC_tel"":""" & toUnicode(.fields("HBC_tel")) & """"
                            s=s & " ,""HBC_gsm"":""" & toUnicode(.fields("HBC_gsm")) & """"
                            s=s & " ,""HBC_contactemail"":""" & toUnicode(.fields("HBC_contactemail")) & """"
                            's=s & " ,""HBC_Nota"":""" & .fields("HBC_Nota") & """"
                            s=s & " ,""HBcon_Name"":""" & toUnicode(.fields(HBcon_Name)) & """"
                            s=s & " ,""HBC_intLang"":""" & toUnicode(.fields("HBC_intLang")) & """"
                            s=s & " ,""HBC_Betalingservaring"":""" & toUnicode(.fields("HBC_Betalingservaring")) & """"
                            s=s & " ,""HBC_DatamarketID"":""" & toUnicode(.fields("HBC_DatamarketID")) & """"
                            s=s & " ,""jfc_s_desc"":""" & toUnicode(.fields(jfc_s_desc)) & """"
                            s=s & " ,""jfc_l_desc"":""" & toUnicode(.fields(jfc_l_desc)) & """"
                            s=s & " ,""nbc_desc"":""" & toUnicode(.fields(nbc_desc)) & """"
                            s=s & " },"
                        .movenext
                        loop
                        s = left(s,len(s)-1)
                        
                    end if
                end with
        s = s & "]"   
        pJSONGetCompanyBackOfficeInfo = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
    end if
end function

private function pJSONGetCompanyBackOfficeInfoByString()
    if fieldName() = -1 then exit function
    if fieldvalue() = -1 then exit function
    
    Dim objConn, objRS, strSql, sWhere, sOrder, sErrMsg, vwCompany, event_type_name
    set objConn=Server.CreateObject("ADODB.Connection")
    with objConn
        .ConnectionString = BackOfficeConnString()
        .CursorLocation = 3
        .Open    
    end with
        if objConn.State = 0 then set objConn = nothing : exit function : end if

        strSql = "SELECT * FROM [dbo].[vw_GetBackOfficeCompanyInfo] "
        Dim strCriterion
        strCriterion = fieldvalue() 
        strCriterion = replace(strCriterion, "'", "''",1,-1, vbtextcompare)
    
    Dim strOpt    
    select case cbxstrOpt()
        case 0
            strOpt = " like '" & strCriterion & "'"
            sWhere = " where " & fieldName() & " like '" & strCriterion & "'"
        case 1
            strOpt = " like '" & strCriterion & "%'"
            sWhere = " where " & fieldName() & " like '" & strCriterion & "%'"
        case 2
            strOpt = " like '%" & strCriterion & "%'"
            sWhere = " where " & fieldName() & " like '%" & strCriterion & "%'"
        case 3
            strOpt = " like '%" & strCriterion & "'"
            sWhere = " where " & fieldName() & " like '%" & strCriterion & "'"
        case else
            strOpt = " like '" & strCriterion & "'"
            sWhere = " where " & fieldName() & " like '" & strCriterion & "'"
    end select
    
            Dim sAnd
        select case fieldName()
            case "@phone"
                sWhere = " WHERE( HBC_tel " & strOpt & " OR HBC_gsm " & strOpt & " OR HBC_fax " & strOpt & ")"
        end select

        strSql = strSql & sWhere  

      
        on error resume next        
        set objRS=Server.CreateObject("ADODB.Recordset") 
        objRS.open strSql, objConn, 1, 3
        If Err.number <> 0 then
            sErrMsg = Err.Description & "Source : " & Err.Source & _
            "<br />Error in Class: sysSearch.pJSONGridGetTsCompanyByCompanyName()" & _
            "<br />while triggering [set objRS = .Execute]. -> :" & strsql
        end if

        s= "{""rows"":["
        with objRS
            if not .bof and not .eof then  
            dim fulladdress
            
            .moveFirst 
                    do while not .eof
                        fulladdress = toUnicode(.fields("HBC_Street")) & " " & toUnicode(.fields("HBC_HouseNr")) & " " & .toUnicode(fields("HBC_BusNr")) & " " & toUnicode(.fields("HBC_Postcode")) & " " & toUnicode(.fields("HBC_gemeente"))     
                        fulladdress=trim(fulladdress)
                        s = s & "{""id"":""" & toUnicode(.fields("HBC_ID")) & """, ""data"": [""" & toUnicode(.fields("HBC_ID")) & """,""" & toUnicode(trim(.fields("HBC_FirmName")))  & """, """ & toUnicode(trim(.fields("HBC_Firmvat")))  & """, """ & toUnicode(trim(.fields("HBC_ContactName")))  & """, """ & toUnicode(trim(.fields("HBC_tel")))  & """, """ & toUnicode(trim(.fields("HBC_gsm")))  & """, """  & fulladdress  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))  
            end if
        end with
        s = s & "]}"
        pJSONGetCompanyBackOfficeInfoByString = s
        objRS.close : set objRS = nothing
        objConn.close : set objConn = nothing
end function


    private function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    private function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function
	
	private function SetDateTimeWithMinutes(minutes)
        Dim yy, mm, dd, hh, minmin, addHour
		addHour = 0
		
		yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
		
		minmin = minute(now()) + minutes
		if minmin > 60 then 
			minmin = minmin - 60
			addHour = 1
		end if
		
        if minmin < 10 then minmin = "0" & minmin
        
		hh = hour(now()) + addHour
        if hh < 10 then hh = "0" & hh
        
		SetDateTimeWithMinutes = yy & "/" & mm  & "/"& dd & " " & hh & ":" & minmin & ":00"
	end function
		

    public sub Class_Terminate()
        set objSearch = nothing
    end sub
end class
%>