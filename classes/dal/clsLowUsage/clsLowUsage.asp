<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>

<%


Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<!-- #include virtual ="/classes/utilities/json.asp" -->
<%
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"
CONST C_LOWUSAGE_TEXT = "Added from Low Usage List"
CONST C_LOWUSAGE_EVENT = 30
CONST C_LOWUSAGE_CONVERSION_MSG = "SYSTEM: LowUsage Entry converted to Event by application"

Dim objLowUsage

set objLowUsage = new sysLowUsage

	select case request.Querystring("a")
		case "JSON_GetLowUsage"
			objLowUsage.JSON_GetLowUsage
		case "JSON_GetLowUsageDetails"
			objLowUsage.JSON_GetLowUsageDetails
		case "JSON_GetLowUsageHistory"
			objLowUsage.JSON_GetLowUsageHistory
		case "JSON_GetSalesToolInfo"
			objLowUsage.JSON_GetSalesToolInfo
		case "JSON_GetManueel"
			objLowUsage.JSON_GetManueel
		case "POST_PromoteToEvent"
			objLowUsage.POST_PromoteToEvent
		case "POST_HideEntry"
			objLowUsage.POST_HideEntry
	end select
	
	set objLowUsage = nothing
	
class sysLowUsage
    property get BackOfficeConnString()
        BackOfficeConnString = sConnBoString
    end property
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property
    property get company_vat()
		company_vat = request("company_vat")
		if len(trim(company_vat)) = 0 then company_vat = -1
	end property
	    property get company_id()
		company_id = request("company_id")
		if len(trim(company_id)) = 0 then company_id = -1
	end property
	property get company_hbc_id()
		company_hbc_id = request("company_hbc_id")
		if len(trim(company_hbc_id)) = 0 then company_hbc_id = -1
	end property
		    property get company_name()
		company_name = request("company_name")
		if len(trim(company_name)) = 0 then company_name = -1
	end property
    property get BoConnString()
        BoConnString = sConnBoString
    end property
	property get hbc_id()
		hbc_id = request("hbc_id")
		if len(trim(hbc_id)) = 0 then hbc_id = 0
	end property
	property get user_id()
		user_id = request.Cookies("TELESALESTOOLV2")("usrId")    
        if len(trim(user_id)) = 0 then user_id = -1
	end property
	public function JSON_GetLowUsageDetails()
		JSON_GetLowUsageDetails=pJSON_GetLowUsageDetails(): Response.ContentType = "application/json": response.write JSON_GetLowUsageDetails
	end function
	public function POST_HideEntry()
		POST_HideEntry=pPOST_HideEntry() : Response.ContentType = "text/html" : response.write POST_HideEntry
	end function
	public function JSON_GetLowUsage()
		JSON_GetLowUsage=pJSON_GetLowUsage(): Response.ContentType = "application/json": response.write JSON_GetLowUsage
	end function
		public function JSON_GetInhoudingen()
		JSON_GetInhoudingen=pJSON_GetInhoudingen(): Response.ContentType = "application/json": response.write JSON_GetInhoudingen
	end function
		public function JSON_GetManueel()
		JSON_GetManueel=pJSON_GetManueel(): Response.ContentType = "application/json": response.write JSON_GetManueel
	end function
		public function JSON_GetLowUsageHistory()
		JSON_GetLowUsageHistory=pJSON_GetLowUsageHistory(): Response.ContentType = "application/json": response.write JSON_GetLowUsageHistory
	end function
	public function JSON_GetSalesToolInfo()
		JSON_GetSalesToolInfo = pJSON_GetSalesToolInfo : Response.ContentType = "application/json" : response.write JSON_GetSalesToolInfo
	end function
	public function POST_PromoteToEvent()
		POST_PromoteToEvent = pPOST_PromoteToEvent() : Response.ContentType ="text/html" : response.write POST_PromoteToEvent
	end function
	public function SetDate()
        dim yy, mm, dd    
        yy = year(now()) 
        mm = month(now()) 
            if mm < 10 then mm = "0" & mm
        dd = day(now())
            if dd < 10 then dd = "0" & dd
        SetDate = yy & mm & dd
    end function

    public function SetTime()
        Dim hh, mm
        hh = hour(now())
        if hh < 10 then hh = "0" & hh
        mm = minute(now())
        if mm < 10 then mm = "0" & mm
        SetTime = hh & ":" & mm
    end function
	private function pPOST_PromoteToEvent()
	    Dim objConn, strSql, sErrMsg, objCmd
		pPOST_PromoteToEvent = 0
        set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
			pPOST_PromoteToEvent = -1
			set objConn = nothing
            exit function
        end if
		if company_id() = -1 then
			pPOST_PromoteToEvent = -2
			set objConn = nothing
            exit function
		end if

    on error resume next
    objConn.BeginTrans
	
		dim objRsE, strSqlE
        set objRSE = Server.CreateObject("ADODB.Recordset")
        strSqlE = "SELECT id FROM dbo.events WHERE usr_id=" & user_id() & " AND company_id=" & company_id()
        objRsE.open strSqlE, objConn, 0, 1
        if objRsE.bof and objRsE.eof then strSql = "dbo.Events_ADD" else strSql = "dbo.Events_UPDATE"
        objRsE.close
        set objRsE = nothing
		
		Dim evtStartDate, evtEndDate, eventID
        evtStartDate =  SetBestTimeFrame()
        evtEndDate =  GetNextTimeFrame(evtStartDate, 5)
		
		        'parameters types: '5: adDouble - '8: adBSTR -'7: adDate 
        if objConn.State = 1 then
            Dim strDefaultText
            strDefaultText= company_name() & " : " & C_LOWUSAGE_TEXT
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@event_type_id", 5, 1, len(C_LOWUSAGE_EVENT), C_LOWUSAGE_EVENT)
                .Parameters.Append .CreateParameter("@company_id", 5, 1, len(company_id()), company_id())
                .Parameters.Append .CreateParameter("@text", 8, 1, len(strDefaultText), strDefaultText)
                .Parameters.Append .CreateParameter("@start_date", 7, 1, len(evtStartDate), evtStartDate)
                .Parameters.Append .CreateParameter("@end_date", 7, 1, len(evtEndDate), evtEndDate)
                .Parameters.Append .CreateParameter("@usr_id", 5, 1, len(user_id()), user_id())
                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(user_id()), user_id())
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(user_id()), user_id())
                .Parameters.Append .CreateParameter("@id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"        
            'objRs.Close
            set objRs= nothing
            eventID=objCmd(8)
        end if
		
		    if objConn.State = 1 then
            strSql="dbo.Comments_ADD"
            Dim comment, comment_date, comment_time, comment_ID
            comment = C_LOWUSAGE_CONVERSION_MSG
            comment_date = SetDate()
            comment_time = SetTime()
            set objCmd = Server.CreateObject("ADODB.Command")
            with objCmd
                .ActiveConnection = objConn
                .CommandType = 4 'adCmdStoredProc
                .CommandText =  strSql
                .NamedParameters = True
                .Parameters.Append .CreateParameter("@comment_event_type_id", 5, 1, len(C_LOWUSAGE_EVENT), C_LOWUSAGE_EVENT)
                .Parameters.Append .CreateParameter("@comment_usr_id", 5, 1, len(user_id()), user_id())
                .Parameters.Append .CreateParameter("@comment_event_id", 8, 1, len(eventID), eventID)
                .Parameters.Append .CreateParameter("@comment_lead_id", 8, 1, len("-1"), -1)
                .Parameters.Append .CreateParameter("@comment_company_vat", 8, 1, len(company_vat()), company_vat())
				.Parameters.Append .CreateParameter("@comment_company_id", 8, 1, len(company_id()), company_id())
                .Parameters.Append .CreateParameter("@comment_date", 5, 1, len(comment_date), comment_date)
                .Parameters.Append .CreateParameter("@comment_time", 8, 1, len(comment_time), comment_time)
                .Parameters.Append .CreateParameter("@comment", 8, 1, len(comment), comment)

                .Parameters.Append .CreateParameter("@AuditUserCreated", 8, 1, len(user_id()), user_id())
                .Parameters.Append .CreateParameter("@AuditUserUpdated", 8, 1, len(user_id()), user_id())
                .Parameters.Append .CreateParameter("@comment_id", 5, 2)
                .Parameters.Append .CreateParameter("@sqlErrMsg", 8, 2,1000)
                set objRS = Server.CreateObject("ADODB.Recordset")
                objRS.CursorType = 0
                objRS.lockType = 1
                set objRS = .Execute()
            end with
            if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
            'objRs.Close
            set objRs= nothing
            comment_ID=objCmd(10)
        end if  

        if err.number <> 0 then sErrMsg= sErrMsg & strsql &" : " &  err.description & "<br>"
        
        if Err.number = 0 then
            objConn.CommitTrans
            pPOST_PromoteToEvent = 1
    
        else
            objConn.RollbackTrans
            pPOST_PromoteToEvent = pPOST_PromoteToEvent & sErrMsg
        end if

    objConn.Close
    Set objConn = nothing
    
	end function
	private function pPOST_HideEntry()
	dim strSQL, conn, comm, rs
	
	strSQL = " UPDATE TST_retentieLijst "
	strSQL = strSQL & " SET tr_status='AFTERSALES',tr_update = year(getdate()) * 10000 + month(getdate()) * 100 + day(getdate())"
	strSQL = strSQL & " WHERE tr_HBC_ID = " & company_hbc_id()

	set conn = Server.CreateObject("ADODB.Connection")
	Set comm = CreateObject("ADODB.Command" )
	conn.open BackOfficeConnString()
	set comm.activeconnection = conn
	comm.commandtext = strSql
	
	comm.execute
	conn.close
	
	pPOST_HideEntry = pPOST_HideEntry & "1"
			
	end function
	private function pJSON_GetLowUsage()
	    Dim objConn, oRs, strSql
	
    strSql = " select distinct tr_HBC_ID as id, tr_HBU_Einde_Website as einde,  tr_HBC_Firmvat as vat, tr_HBC_FirmName as name,  tr_HBC_Nota as note "
	strSql = strSql & " from TST_retentieLijst where tr_status = 'NEW' "
	strSql = strSql & " order by tr_HBU_Einde_Website,tr_HBC_ID"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        
        oRs.open strSql, objConn
		s = RStoDhtmlXJson(oRs, "id")
		
		pJSON_GetLowUsage = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function
	private function pJSON_GetLowUsageDetails()
	    Dim objConn, oRs, strSql
	
    strSql = " select distinct tr_HBC_ID as id, tr_HBU_Einde_Website as einde,  tr_HBC_Firmvat as vat, tr_HBC_FirmName as name"
	strSql = strSql & " from TST_retentieLijst where tr_status = 'NEW' AND tr_HBC_ID = " & hbc_id()
	strSql = strSql & " order by tr_HBU_Einde_Website,tr_HBC_ID"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        
        oRs.open strSql, objConn
		s = RStoJSon(oRs)
		
		pJSON_GetLowUsageDetails = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function
		private function pJSON_GetLowUsageHistory()
	    Dim objConn, oRs, strSql
	
    strSql = " select tr_HBC_ID as id, tr_HBC_Nota as note"
	strSql = strSql & " from TST_retentieLijst where tr_status = 'NEW' AND tr_HBC_ID = " & hbc_id()
	strSql = strSql & " order by tr_HBU_Einde_Website,tr_HBC_ID"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        
        oRs.open strSql, objConn
		s = RStoJSon(oRs)
		
		pJSON_GetLowUsageHistory = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function

	private function pJSON_GetInhoudingen()
	    Dim objConn, oRs, strSql
	
    strSql = "SELECT qs_id, qs_datum, qs_vat, qs_" & Userlang() & "Name as qs_Name, qs_"&Userlang()& "vorm as qs_vorm, qs_" & Userlang() & "Gemeente as qs_gemeente, qs_postcode, qs_more from ST_QuickSamples where qs_type = 'INHOUDING' ORDER BY qs_datum desc"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pGetCompanyHistory = strSql
        oRs.open strSql, objConn
		
		s= "{""rows"":["
		with oRs
            if not .bof and not .eof then
                .moveFirst 
                do while not .eof
					s = s & "{"
					s = s & """id"": " & .fields("qs_id") & ", "
					s = s & """data"": ["
					s = s & """" & .fields("qs_datum") & """," 
					s = s & """" & .fields("qs_vat") & ""","
					s = s & """" & .fields("qs_Name") & ""","
					s = s & """" & .fields("qs_Vorm") & ""","
					s = s & """" & .fields("qs_Gemeente") & ""","
					s = s & """" & .fields("qs_PostCode") & ""","
					s = s & """" & .fields("qs_more") & """]},"
                    .movenext
                loop       
				s = left(s, (len(s)-1))  
			end if
		End With
		s = s & "]}"  
		pJSON_GetInhoudingen = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function
	
	private function pJSON_GetManueel()
	    Dim objConn, oRs, strSql
	
    strSql = "SELECT qs_id, qs_datum, qs_vat, qs_" & Userlang() & "Name as qs_Name, qs_"&Userlang()& "vorm as qs_vorm, qs_" & Userlang() & "Gemeente as qs_gemeente, qs_postcode, qs_more from ST_QuickSamples where qs_type = 'HANDMATIG' ORDER BY qs_datum desc"
		
    set objConn=Server.CreateObject("ADODB.Connection")
    set oRs=Server.CreateObject("ADODB.Recordset") 
        objConn.open BoConnString() , adopenstatic, adcmdreadonly, adcmdtext
        pGetCompanyHistory = strSql
        oRs.open strSql, objConn
		
		s= "{""rows"":["
		with oRs
            if not .bof and not .eof then
                .moveFirst 
                do while not .eof
					s = s & "{"
					s = s & """id"": " & .fields("qs_id") & ", "
					s = s & """data"": ["
					s = s & """" & .fields("qs_datum") & """," 
					s = s & """" & .fields("qs_vat") & ""","
					s = s & """" & .fields("qs_Name") & ""","
					s = s & """" & .fields("qs_Vorm") & ""","
					s = s & """" & .fields("qs_Gemeente") & ""","
					s = s & """" & .fields("qs_PostCode") & ""","
					s = s & """" & .fields("qs_more") & """]},"
                    .movenext
                loop       
				s = left(s, (len(s)-1))  
			end if
		End With
		s = s & "]}"  
		pJSON_GetManueel = s
		
        oRs.close : set oRs = nothing
        objConn.close : set objConn = nothing   
	end function
	
	    private function SetBestTimeFrame()
        Dim oConn, objRS, strSql, blnNew, tmpDate
        
        set oConn=Server.CreateObject("ADODB.Connection")
        with oConn
            .ConnectionString = SalesToolConnString()
            .CursorLocation = 3
            .Open    
        end with
        if oConn.State = 0 then exit function
        set objRS = Server.CreateObject("ADODB.Recordset")
        Dim eventStartDate
        eventStartDate = roundtime(now(), 5)
        
        strSql = "SELECT events.*, event_types.event_type_duration_min FROM event_types INNER JOIN events ON event_types.event_type_id = events.event_type_id "
        strSql = strSql & " WHERE usr_id=" & user_id() & " AND (start_date >='" & eventStartDate  & "' OR end_date >='" & eventStartDate & "') ORDER BY start_date"
  
        'response.write strsqL

        objRS.open strSql, oConn, 0, 1
        with objRs
            if not .bof and not .eof then
                do while not .eof        
                    if cdate(.fields("start_date")) <= cdate(eventStartDate) then
                        eventStartDate = .fields("end_date")                
                        tmpDate = eventStartDate
                    else
                        tmpDate =eventStartDate
                        exit do
                    end if
                .movenext
                loop
            else
                tmpDate =eventStartDate
            end if
        end with
    
        SetBestTimeFrame=cdate(roundtime(tmpDate,5))
        objRS.close
        set objRS = nothing
        oConn.Close
        Set oConn = nothing
    
    end function
	    private function roundtime(vtime, NbMinutes) 
    'nbMinutes round to x minutes, ex: NbMinutes= 5 -> round to next 5minutes
        v_month = month(vtime)
        v_day = day(vtime)
        v_year = year(vtime)
        v_hour = hour(vtime)
        v_minute = round(minute(vtime)/NbMinutes) * NbMinutes
        if v_minute >= 60 then
            v_minute = 0
			v_hour = v_hour + 1
		end if
		
        if v_hour > 17 then  '18 (old value:24) because we must fix these values according to opening hours in agenda : 08:00 & 18:00
			v_hour = 9       '8' (old value:0)   
			v_day = v_day + 1        
        end if
        roundtime = GetWeekDay(v_year & "-" & v_month & "-" & v_day) & " " & v_hour & ":" & v_minute & ":00"
        'roundtime = cdate(v_year & "-" & v_month & "-" & v_day & " " & v_hour & ":" & v_minute & ":00")
    end function
	    private function GetWeekDay(theDate)
        do while not Weekday(theDate,vbMonday) <= 5
            theDate= DateAdd("d",1,theDate)        
        loop
        GetWeekDay = year(theDate) & "-" & month(theDate) & "-" & day(theDate)
    end function
	    private function GetNextTimeFrame(vTime, nbMinutes)   
        'ex: NbMinutes= 5 -> get next +5mins timeframe
        GetNextTimeFrame = dateadd("n",nbMinutes ,vTime)
    end function
end class
%>
