﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
Session.CodePage = 65001
Response.charset ="UTF-8"
Session.LCID     = 1033 'en-US
%>
<!-- #include virtual ="/classes/dal/connector.inc" -->
<% 
Response.Buffer = True
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

CONST C_DEFAULT_LANG= "FR"
CONST C_BASIC_HR_FUNCTION= 4

    CONST C_EQUAL = "="
    CONST C_LIKE = "like"
    CONST C_IN = "in"

Dim objMailTemplate, objMail
   
    set objMailTemplate = new sysMailTemplate

    select case request.QueryString("a")         
        case "GetComboMailTemplatesWithImg"
            objMailTemplate.GetComboMailTemplatesWithImg(false)
        case "GetComboGenericMailTemplatesWithImg"
            objMailTemplate.GetComboMailTemplatesWithImg(true)
        
        case "GetMails"
            objMailTemplate.GetMails
        case "GetEMail"
            objMailTemplate.GetEMail
        case else 'default should be read
    end select
    

class sysMailTemplate

    Dim m_lang_radical, m_usr_id, m_usr_role_id, m_userLang   

    property get DevBackOfficeConnString()
        DevBackOfficeConnString = sConnStrBackOfficeDev
    end property
    
    property get DevConnString()
        DevConnString = sConnStrDev
    end property
    
    property get SalesToolConnString()
        SalesToolConnString = sConnString
    end property
    
    property get usr_id()
        usr_id=request.Cookies("TELESALESTOOLV2")("usrId")
    end property
    
    property get usr_role_id()
        usr_role_id = request.Cookies("TELESALESTOOLV2")("usrRoleId")
        if len(trim(usr_role_id)) = 0 then usr_role_id = C_GUEST_USER    
    end property

    property get Userlang()
        Userlang = request.Cookies("TELESALESTOOLV2")("lang_radical")    
        if len(trim(Userlang)) = 0 then Userlang = C_DEFAULT_LANG
    end property



    property get hr_function_id()
        hr_function_id =Request.Cookies ("TELESALESTOOLV2")("hrfunctionid")
        if len(trim(hr_function_id)) = 0 then hr_function_id = C_BASIC_HR_FUNCTION    
    end property
    
    property get event_id()
        event_id=request("event_id")
    end property

    property get link_id()
        link_id=request("link_id")
    end property

    property get useIcons()
        useIcons=request("c")
        if len(trim(useIcons)) = 0 then useIcons = 0
    end property

    public function GetComboMailTemplatesWithImg(IsGeneric)
        GetComboMailTemplatesWithImg = pGetComboMailTemplatesWithImgg(IsGeneric) :  response.write GetComboMailTemplatesWithImg
    end function

    public function GetMails() 
        response.write pGetMails()
    end function

    public function GetEMail() 
        response.write pGetEMail()
    end function

'<START REGION JSON BUSINESS LOGIC>===========================================================

'get list of available mails templates(json format)
    private function pGetComboMailTemplatesWithImgg(blnGeneric)       
        Dim objConn, oRs, strSql, mailTemplateName, s, mainSql, sAndGeneric, orderBy
            mailTemplateName = "mail_template_name_" & UserLang() 
            
            sAndGeneric = ""
            mainSql = "SELECT * FROM dbo.[vw_GetMail_Templates] WHERE mail_hrfunction_id >=" & hr_function_id() 
            If blnGeneric then sAndGeneric = " AND mail_template_generic=1 "
            orderBy = " order by mail_template_order"
    
            strSql = mainSql & sAndGeneric &  orderBy   


            set objConn=Server.CreateObject("ADODB.Connection")
            set oRs=Server.CreateObject("ADODB.Recordset") 
                objConn.open SalesToolConnString() , adopenstatic, adcmdreadonly, adcmdtext
                oRs.open strSql, objConn
             
                with oRs
                    if not .bof and not .eof then
                    .moveFirst 
                        s= "{options:["
                                s = s & "{value:""-1"", img: ""cancel.png"", text:""select...""},"
                            do while not .eof
                                s = s & "{value:""" & .fields(0) & """, img: """ & .fields("mail_template_icon") & """, text:""" & .fields(mailTemplateName) & """},"
                            .movenext
                        loop       
                    s = left(s, (len(s)-1))
                    s = s & "]}"    
                end if
                End With
            pGetComboMailTemplatesWithImgg = s
            oRs.close : set oRs = nothing
            objConn.close : set objConn = nothing
    end function


'get selected sent email (html generated mail.body)
private function pGetEMail()
    Dim objConn, oRs, strSql, sqlAnd, s
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3             '3 adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if 
           
    strSql = "SELECT * FROM dbo.[vw_GetEMail] Where 1=1 "
    sqlAnd = " and mail_id=" & request("mail_id")        

    strSql = strSql & sqlAnd 
    pGetEMail = ""       
    set oRs=Server.CreateObject("ADODB.Recordset")
        oRs.open strSql, objConn, 1, 3 

        with oRs
            if not .bof and not .eof then
                pGetEMail = .fields("body") 
            end if
        End With
        
        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

end function

'get list of sent mails based on criteria
private function pGetMails()
    Dim objConn, oRs, strSql, sqlAnd, s
    set objConn=Server.CreateObject("ADODB.Connection")
        with objConn
            .ConnectionString = SalesToolConnString()
            .CommandTimeOut = 15
            .mode=3 'adModeReadWrite
            .CursorLocation = 3 '2 adUseServer 3 adUseClient
            .Open    
        end with

        if objConn.State = 0 then 
        set objConn = nothing
            exit function
        end if 
           
    strSql = "SELECT * FROM dbo.[vw_GetEMail] Where 1=1 "
    sqlAnd = pBuildCriterionString()        

    strSql = strSql & sqlAnd 
           
    set oRs=Server.CreateObject("ADODB.Recordset")
        oRs.open strSql, objConn, 1, 3 

        with oRs
            if not .bof and not .eof then
            Dim contactName
            .moveFirst 
                contactName = .fields("contact_firstname") & " " & .fields("contact_lastname")
                s= "{rows:["
                    do while not .eof
                        's = s & "{key:" & .fields(0) & ",label:'" & .fields(EventTypeName) & "'},"
                        s = s & "{id:""" & .fields("mail_id") & """, data: [""" & .fields("company_name")  & """, """ & contactName  & """, """ & .fields("subject")  & """, """ & .fields("mail_creationdate")  & """, """ & .fields("mailTo")  & """, """ & .fields("mailCC")  & """, """ & .fields("mailBCC")  & """, """ & .fields("Attachment")  & """]},"
                    .movenext
                loop       
            s = left(s, (len(s)-1))
            s = s & "]}"    
        end if
        End With
        pGetMails = s

        oRs.Close : set oRs = nothing
        objConn.close : set objConn = nothing

end function

    Private function pBuildCriterionString()
    dim s
        s = s & castObjProperty("Subject", C_LIKE, request("Subject"))
        s = s & castObjProperty("Body", C_LIKE, request("Body"))
        s = s & castObjProperty("order_list_token_id", C_LIKE, request("order_list_token_id"))
        s = s & castObjProperty("event_id", C_EQUAL, request("event_id"))
        s = s & castObjProperty("company_id", C_EQUAL, request("company_id"))
        s = s & castObjProperty("company_vat", C_LIKE, request("company_vat"))
        s = s & castObjProperty("contact_id", C_EQUAL, request("contact_id"))        
        s = s & castObjProperty("user_id", C_EQUAL, request("user_id"))
        s = s & castObjProperty("mail_template_id", C_EQUAL, request("mail_template_id"))
    pBuildCriterionString = s
            
    end function

    private function castObjProperty(fieldName, operator, value)
        Dim s : s= ""
        Dim sQ : sQ = ""
        if operator = C_LIKE then sQ="'" else sQ=""
        if len(trim(value)) > 0 then s = " and " & fieldname & " " & operator & " " & sQ & value & sQ
        castObjProperty = s
    end function

                      
       
'<END REGION JSON BUSINESS LOGIC>=============================================================

    public sub Class_Terminate()
        set objMailTemplate = nothing   
    end sub


end class

%>