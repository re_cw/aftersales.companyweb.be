﻿
function AjaxMailTemplate_GetComboMailTemplatesWithImgPromise() {
	var url = "/classes/dal/clsMailTemplates/clsMailTemplate.asp";
	
	return $.get(url, { a: "GetComboMailTemplatesWithImg" })
    .fail(function (resp) {
        dhtmlx.message({
            text: "Error getting Combo Mail Templates with Images",
            expire: -1,
            type: "errormessage"
        });
    });
}
