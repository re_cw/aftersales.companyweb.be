<%

Function CleanPersoonName(PersoonNaam)

  CleanPersoonName = PersoonNaam

  CleanPersoonName = Trim(CleanPersoonName)
  CleanPersoonName = UCase(CleanPersoonName)
  CleanPersoonName = Replace(CleanPersoonName, Chr(150), "")
  CleanPersoonName = Replace(CleanPersoonName, ",", "")
  CleanPersoonName = Replace(CleanPersoonName, Chr(146), "")
  CleanPersoonName = Replace(CleanPersoonName, """", "")
  CleanPersoonName = Replace(CleanPersoonName, " ", "")
  CleanPersoonName = Replace(CleanPersoonName, "'", "")
  CleanPersoonName = Replace(CleanPersoonName, "-", "")
  CleanPersoonName = Replace(CleanPersoonName, "&", "")
  CleanPersoonName = Replace(CleanPersoonName, "_", "")
  CleanPersoonName = Replace(CleanPersoonName, "�", "E")
  CleanPersoonName = Replace(CleanPersoonName, "�", "E")
  CleanPersoonName = Replace(CleanPersoonName, "�", "A")
  CleanPersoonName = Replace(CleanPersoonName, "�", "A")
  CleanPersoonName = Replace(CleanPersoonName, "�", "U")
  CleanPersoonName = Replace(CleanPersoonName, "�", "U")
  CleanPersoonName = Replace(CleanPersoonName, "�", "E")
  CleanPersoonName = Replace(CleanPersoonName, "�", "A")
  CleanPersoonName = Replace(CleanPersoonName, "�", "A")
  CleanPersoonName = Replace(CleanPersoonName, "�", "E")
  CleanPersoonName = Replace(CleanPersoonName, "�", "O")

End Function


Function CleanNameHans(PersoonNaam)

  PersoonNaam = Trim(PersoonNaam)
  CleanNameHans = ""
    For i = 1 To Len(PersoonNaam)



        xChar = Mid(PersoonNaam, i, 1)

        Select Case Asc(xChar)
        case Asc(" "),Asc("'"),Asc("-"),Asc("�"),Asc("�"),Asc("�"),Asc("�")
        	CleanNameHans = CleanNameHans & xChar

		case else
		  if Asc(xChar) >= Asc("a")  and Asc(xChar) <= Asc("z") then
        	CleanNameHans = CleanNameHans & xChar
		  else
		     if Asc(xChar) >= Asc("A")  and Asc(xChar) <= Asc("Z") then
        		CleanNameHans = CleanNameHans & xChar
			else

			end if
		  end if

	    end select

    Next



End Function


Function SameName(kboPersoonNaam, kboPersoonVoorNaam , xbrlPersoonNaam , xbrlPersoonVoorNaam)

SameName = False

kboPersoonNaam = CleanPersoonName(kboPersoonNaam)
xbrlPersoonNaam = CleanPersoonName(xbrlPersoonNaam)
kboPersoonVoorNaam = CleanPersoonName(kboPersoonVoorNaam)
xbrlPersoonVoorNaam = CleanPersoonName(xbrlPersoonVoorNaam)


If kboPersoonNaam = xbrlPersoonNaam And kboPersoonVoorNaam = xbrlPersoonVoorNaam Then
SameName = True
Exit Function
End If
If kboPersoonNaam = xbrlPersoonVoorNaam And kboPersoonVoorNaam = xbrlPersoonNaam Then
SameName = True
Exit Function
End If
If kboPersoonNaam = xbrlPersoonVoorNaam And Left(kboPersoonVoorNaam, Len(xbrlPersoonNaam)) = xbrlPersoonNaam Then
SameName = True
Exit Function
End If
If kboPersoonNaam = xbrlPersoonVoorNaam And Left(xbrlPersoonNaam, Len(kboPersoonVoorNaam)) = kboPersoonVoorNaam Then
SameName = True
Exit Function
End If
If kboPersoonNaam = xbrlPersoonVoorNaam And Left(kboPersoonVoorNaam, 1) = Left(xbrlPersoonNaam, 1) Then
SameName = True
Exit Function
End If

End Function
Public Function InitialCapitals(x)

  InitialCapitals = ""

  x = LCase(x)
  PrevChar = " "
  TwoAgo = ""
  If Trim(x & "") <> "" Then
    For i = 1 To Len(x)
        xChar = Mid(x, i, 1)
        If PrevChar = " " Or PrevChar = "-" Or PrevChar = "." Then
            InitialCapitals = InitialCapitals & UCase(Mid(x, i, 1))
        Else
            If xChar = "i" And (PrevChar = "i" Or PrevChar = "x" Or (PrevChar = "v" And InStr(1, x, "vii") <> 0)) And (TwoAgo = " " Or TwoAgo = "-" Or TwoAgo = "." Or TwoAgo = "i" Or TwoAgo = "x" Or TwoAgo = "v") Then
                InitialCapitals = InitialCapitals & UCase(Mid(x, i, 1))
            Else
                InitialCapitals = InitialCapitals & Mid(x, i, 1)
            End If
        End If
        TwoAgo = PrevChar
        PrevChar = xChar

    Next
  End If

End Function

Function TransJurVorm(pInput,pIntLang,ShortName)
TransJurVorm = "N.A."
if pInput <> "" then
if IsNumeriek(pInput) then
	Set ObjRSJurVorm = server.CreateObject ("adodb.recordset")
	if pIntLang = 1 then
		strSQL = "select jfc_l_desc_fr as omschr,jfc_s_desc_fr as short from KBO_JuridicalFormCodes where jfc_code = " & pInput
	else
		strSQL = "select jfc_l_desc_nl as omschr,jfc_s_desc_nl as short from KBO_JuridicalFormCodes where jfc_code = " & pInput
	end if
	ObjRSJurVorm.Open strSQL,conn,3,2
	If not ObjRSJurVorm.eof Then
		if ShortName then
		TransJurVorm = "(" & ObjRSJurVorm("short") & ")"
		else
		TransJurVorm = ObjRSJurVorm("omschr") & " (" & ObjRSJurVorm("short") & ")"
		end if
	End If
	ObjRSJurVorm.close
end if
end if

End Function



Private Function GetFNaam(vat)

   Set ObjRS_Firma = server.CreateObject("adodb.recordset")


  FndNametype = ""
  FndNamelanguage = ""
  FndName = ""
  FndStraatType = ""
  FndStraatlanguage = ""
  FndStraat = ""


   FndBnr = ""
  FndHnr = ""
  FndPostCode = ""
  FndGemeente = ""

    FndMaatschNaam = ""
  FndShortName = ""
  FndCommName = ""


  ZoekLanguage = "2"

 strSQL = "select * from cw_SearchNaamAdres where sna_f_vat = " & vat

ObjRS_Firma.open strSQL, conn

If ObjRS_Firma.EOF Then

Else

    FndMode = TransJurVorm(ObjRS_Firma("Sna_F_CurrentJurVorm") & "", ZoekLanguage,true)

    Do While Not ObjRS_Firma.EOF
                FndNametype = ObjRS_Firma("Sna_fn_type") & ""
                FndNamelanguage = ObjRS_Firma("Sna_fn_language") & ""
                FndName = ObjRS_Firma("Sna_fn_name") & ""
                'FA_StraatNaam, FA_PostCode, FA_Gemeente
                FndStraatType = ObjRS_Firma("Sna_fa_type") & ""
                FndStraatlanguage = ObjRS_Firma("Sna_fa_language") & ""
                FndStraat = ObjRS_Firma("Sna_FA_StraatNaam") & ""
                FndHnr = ObjRS_Firma("Sna_FA_HuisNr") & ""
                FndBnr = ObjRS_Firma("Sna_FA_BusNr") & ""
                FndPostCode = ObjRS_Firma("Sna_FA_PostCode") & ""
                FndGemeente = ObjRS_Firma("Sna_FA_Gemeente") & ""


                If FndHnr <> "" Then FndStraat = FndStraat & " " & FndHnr
                If FndBnr <> "" Then FndStraat = FndStraat & " " & FndBnr

                Select Case FndNametype
                Case "1"
                    If FndNamelanguage = ZoekLanguage Then
                        FndMaatschNaam = FndName
                    Else
                        If FndMaatschNaam = "" Then FndMaatschNaam = FndName
                    End If
                Case "2"
                    If FndNamelanguage = ZoekLanguage Then
                        FndShortName = FndName
                    Else
                        If FndShortName = "" Then FndShortName = FndName
                    End If
                Case "3"
                    If FndNamelanguage = ZoekLanguage Then
                        FndCommName = FndName
                    Else
                        If FndCommName = "" Then FndCommName = FndName
                    End If
                End Select


    ObjRS_Firma.MoveNext
Loop
End If
ObjRS_Firma.Close
Set ObjRS_Firma = Nothing

    If FndMode <> "" Then
        GetFNaam = FndMaatschNaam & " " & FndMode
    Else
        GetFNaam = FndMaatschNaam
    End If


End Function

Function StoreTo_Cw_MandatesAdres(Connect , FM_id , Fm_linkadres, Fm_PersoonNaam , Fm_PersoonvoorNaam , Street , Number , CountryCode, Postalcode , CityNL , OtherPostalCode , OtherCity , toDate )


    MA_LandCode = "150"
    MA_language = 1

    If CountryCode <> "BE" Then
        If CountryCode = "NL" Then MA_language = 2
        totcount = Connect.Execute("select isnull(cc_code,0) from KBO_CountryCodes where cc_iso = '" & CountryCode & "'")
        If IsNumeriek(totcount(0)) Then
            MA_LandCode = totcount(0)
        End If
    End If
    If Fm_PersoonNaam <> "" Then
        Fm_PersoonNaam = "'" & Replace(Fm_PersoonNaam, "'", "''") & "'"
    End If
    If Fm_PersoonvoorNaam <> "" Then
        Fm_PersoonvoorNaam = "'" & Replace(Fm_PersoonvoorNaam, "'", "''") & "'"
    Else
        Fm_PersoonvoorNaam = "Null"
    End If
    If Street <> "" Then
        Street = "'" & Replace(InitialCapitals(Street), "'", "''") & "'"
    End If
    If Number <> "" Then
        Number = "'" & Replace(Number, "'", "''") & "'"
    Else
        Number = "Null"
    End If
    If CityNL <> "" Then
        CityNL = "'" & Replace(CityNL, "'", "''") & "'"
    Else
        CityNL = "Null"
    End If
    If Postalcode = 0 Then
        MA_PostCode = "'" & Left(CountryCode, 2) & " " & Trim(OtherPostalCode) & "'"
        If OtherCity <> "" Then
           CityNL = "'" & Replace(OtherCity, "'", "''") & "'"
        End If
        If Len(MA_PostCode) > 12 Then
            MA_PostCode = "Null"
            CityNL = "'" & Left(CountryCode, 2) & " " & Trim(OtherPostalCode) & " " & Replace(OtherCity, "'", "''") & "'"
        End If
    Else
        MA_PostCode = Postalcode
        If (Postalcode >= 1300 And Postalcode < 1500) Or (Postalcode >= 4000 And Postalcode < 8000) Then
            MA_language = 1
        Else
            MA_language = 2
        End If
    End If
    If IsNumeriek(Fm_linkadres) And Fm_linkadres <> "0" Then

       'MA_ID,MA_PersoonNaam,MA_PersoonVoorNaam,MA_language,MA_StraatCode,
       sqlstr = "update CW_MandaatAdres set "
       sqlstr = sqlstr & " Ma_PersoonNaam = " & Fm_PersoonNaam & ","
       sqlstr = sqlstr & " MA_PersoonVoorNaam = " & Fm_PersoonvoorNaam & ","
       sqlstr = sqlstr & " MA_language = " & MA_language & ","
       sqlstr = sqlstr & " MA_StraatCode = 0,"
       'MA_StraatNaam,MA_HuisNr,MA_BusNr,
       sqlstr = sqlstr & " MA_StraatNaam = " & Street & ","
       sqlstr = sqlstr & " MA_HuisNr = " & Number & ","
      'MA_PostCode,MA_NisCode,MA_Gemeente,MA_LandCode,MA_Origin,MA_UpdateDatum
       sqlstr = sqlstr & " MA_PostCode = " & MA_PostCode & ","
       sqlstr = sqlstr & " MA_Gemeente = " & CityNL & ","
       sqlstr = sqlstr & " MA_LandCode = " & MA_LandCode & ","
       sqlstr = sqlstr & " MA_Origin = 'XBL',"
       sqlstr = sqlstr & " MA_UpdateDatum = GetDate()"
       sqlstr = sqlstr & " from CW_MandaatAdres"
       sqlstr = sqlstr & " where MA_ID = " & Fm_linkadres

       Call Connect.Execute(sqlstr)


    Else
       totcount = Connect.Execute("select isnull(min(MA_ID),0) from CW_MandaatAdres where MA_PersoonNaam=" & Fm_PersoonNaam & " and MA_PersoonVoorNaam=" & Fm_PersoonvoorNaam & " and MA_StraatNaam = " & Street)

       If totcount(0) = 0 Then
           ' insert new row
           sqlstr = "insert into CW_MandaatAdres(MA_PersoonNaam,MA_PersoonVoorNaam,MA_language,MA_StraatCode,MA_StraatNaam,MA_HuisNr,MA_PostCode,MA_Gemeente,MA_LandCode,MA_Origin,MA_UpdateDatum)"
           sqlstr = sqlstr & " select " & Fm_PersoonNaam & "," & Fm_PersoonvoorNaam & "," & MA_language & ",0," & Street & "," & Number & "," & MA_PostCode & "," & CityNL & "," & MA_LandCode & ",'XBL',Getdate()"
           Call Connect.Execute(sqlstr)

           Connect.Execute ("update cw_FirmaMandaten set Fm_linkadres = (select max(MA_ID) from CW_MandaatAdres) where fm_id = " & FM_id)

       Else
            ' link row
            Connect.Execute ("update cw_FirmaMandaten set Fm_linkadres = " & totcount(0) & " where fm_id = " & FM_id)
       End If

       If toDate <> 0 Then
           Connect.Execute ("update cw_FirmaMandaten set Fm_einde = " & toDate & " where fm_id = " & FM_id & " and Fm_einde = 0")
       End If

    End If


End Function

Private Function HTMLEncode(ByVal Text, Byval KeepHTMLFormatting)
	Dim strOutput
	Dim lngCodePage

	'Response.CodePage = 65001	'UTF-8

	strOutput = Server.HTMLEncode(Text)
	If KeepHTMLFormatting Then
		strOutput = Replace(strOutput, "&lt;b&gt;","<b>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/b&gt;","</b>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;i&gt;","<i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/i&gt;","</i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;u&gt;","<i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/u&gt;","</i>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;strong&gt;","<strong>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/strong&gt;","</strong>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;em&gt;","<em>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/em&gt;","</em>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;br&gt;","<br>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;br /&gt;","<br />", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;br/&gt;","<br/>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;ul&gt;","<ul>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/ul&gt;","</ul>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;li&gt;","<li>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/li&gt;","</li>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;p&gt;","<p>", 1, -1, 1)
		strOutput = Replace(strOutput, "&lt;/p&gt;","</p>", 1, -1, 1)
		strOutput = Replace(strOutput, "&amp;nbsp;","&nbsp;", 1, -1, 1)
	End If

'	'Change CodePage back to what it was
'	Response.CodePage = lngCodePage

	HTMLEncode = strOutput

End Function

dim msgMandaat(1)
dim msgNaam(1)
dim msgVertegenwoordigd(1)
dim msgVan(1)
dim msgTot(1)
dim msgHuidig(1)
dim msgVorig(1)
dim msgDocLink(1)
dim msgAccountant(1)
dim msgRevisor(1)
dim msgExtrn(1)
dim msgBronInfo(1)
dim msgZaakvoerder(1)
dim msgHerbenoem(1)
dim msgAdd(1)

dim msgFirmaVat(1)
dim msgFirmaNaam(1)
dim msgPersoonNaam(1)
dim msgPersoonVoorNaam(1)
dim msgVertPersoonNaam(1)
dim msgVertPersoonVoorNaam(1)
dim msgStraat(1)
dim msgStad(1)

dim msgVatLoaded(1)
dim msgKboRow(1)
dim msgNbbRow(1)
dim msgRowCopy(1)
dim msgRowDel(1)
dim msgRowHerbenoem(1)
dim msgUpdated(1)
dim msgType(1)
dim msgStatus(1)
dim msgFunction(1)
dim msgRowNew(1)
dim msgUpdateLine(1)
dim msgNewLine(1)
dim msgRowUpdate(1)
dim msgAddAdres(1)
dim msgNewFirm(1)
dim msgAddNewAdres(1)
dim msgLand(1)
dim msgaddAV(1)

msgaddAV(0) = "TOT AlgVerg 20"
msgaddAV(1) = "Jusqu' AV 20"

msgNewFirm(0) = "Nieuwe Firma"
msgNewFirm(1) = "New Firm"

msgAddAdres(0) = "Adres Zoeken"
msgAddAdres(1) = "Search adresse"
msgAddNewAdres(0) = "Adres Toevoegen"
msgAddNewAdres(1) = "Add Adresse"

msgUpdateLine(0) = "Lijn Aanpassen"
msgUpdateLine(1) = "Update Line"
msgNewLine(0) = "Nieuwe Persoon"
msgNewLine(1) = "New Person"


msgType(0) = "Type"
msgType(1) = "Type"
msgStatus(0) = "Status"
msgStatus(1) = "Status"
msgFunction(0) = "Functie"
msgFunction(1) = "Function"


msgVatLoaded(0) = "Firma opgeladen ..."
msgVatLoaded(1) = "Entreprise charg� ..."
msgKboRow(0) =  "Lijn uit KBO toegevoegd ... "
msgKboRow(1) =  "Ligne BCE Inserted ... "
msgNbbRow(0) =  "Lijn uit NBB toegevoegd ... "
msgNbbRow(1) =  "Ligne BNB Inserted ... "
msgRowCopy(0) =  "Lijn Copy Succesvol ..."
msgRowCopy(1) =  "Ligne copied ..."
msgRowDel(0) =  "Lijn Delete Succesvol ..."
msgRowDel(1) =  "Ligne deleted ..."
msgRowHerbenoem(0) =  "Lijn Herbenoem Succesvol ..."
msgRowHerbenoem(1) =  "Renouvelment finished ..."
msgRowNew(0) =  "Nieuwe Lijn toegevoegd ..."
msgRowNew(1) =  "New line inserted ..."
msgRowUpdate(0) =  "Lijn Aangepast ..."
msgRowUpdate(1) =  "Ligne modifie ..."


msgFirmaVat(0) = "Btwnr"
msgFirmaVat(1) = "no TVA"
msgFirmaNaam(0) = "Firma"
msgFirmaNaam(1) = "Entreprise"
msgPersoonNaam(0) = "Naam"
msgPersoonNaam(1) = "Nom"
msgPersoonVoorNaam(0) = "Voornaam"
msgPersoonVoorNaam(1) = "Prenom"
msgVertPersoonNaam(0) = "Vv fam.naam:"
msgVertPersoonNaam(1) = "Repr.par Nom:"
msgVertPersoonVoorNaam(0) = "Vv voornaam:"
msgVertPersoonVoorNaam(1) = "Repr.par Prenom:"
msgStraat(0) = "Straat"
msgStraat(1) = "Rue"
msgStad(0) = "Gemeente"
msgStad(1) = "Ville"

msgHerbenoem(0) = "Herbenoem"
msgHerbenoem(1) = "Renouv�"
msgLand(0) = "Land"
msgLand(1) = "Country"
msgAdd(0) = "Toevoegen"
msgAdd(1) = "Ajoute"

msgUpdated(0) = "Updated"
msgUpdated(1) = "Updated"

msgNaam(0) = "Naam"
msgNaam(1) = "Nom"

msgMandaat(0) = "Mandaat: "
msgMandaat(1) = "Mandat: "
msgVertegenwoordigd(0) = "Vertegenwoordigd door "
msgVertegenwoordigd(1) = "Repr. par "
msgVan(0) = "Van"
msgVan(1) = "de"
msgTot(0) = "Tot"
msgTot(1) = "jusqu'au"
msgHuidig(0) = "Huidige Bedrijfsleiding"
msgHuidig(1) = "Gestion actuelle de l'entreprise "
msgVorig(0) = "Vorige Bedrijfsleiding"
msgVorig(1) = "Gestion pr�c�dente de l'entreprise "
msgExtrn(0) = "(*) Externe Accountant - Boekhouder - Revisor (o.b.v. laatst neergelegde jaarrekening)"
msgExtrn(1) = "(*) Expert-comptable - Comptable - Reviseur Externe (selon la derni�re bilan)"
msgDocLink(0) = "Beeld Akte"
msgDocLink(1) = "Acte"
msgBronInfo(0) = "(*) Bron: NBB (vkt1.2)"
msgBronInfo(1) = "(*) Source: BNB (vkt1.2)"
msgAccountant(0) = "Accountant - Boekhouder"
msgAccountant(1) = "Comptable - Expert-comptable"
msgRevisor(0) = "Revisor"
msgRevisor(1) = "Reviseur"
msgZaakvoerder(0) = "Zaakvoerder"
msgZaakvoerder(1) = "G�rant"




Function  CompareGriffID(strPC,strSelGriffID)
CompareGriffID = false
Select case trim(cstr(strPC))
case "1000"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1008"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1009"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1010"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1019"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1020"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1030"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1040"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1041"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1042"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1047"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1048"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1049"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1050"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1060"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1070"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1080"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1081"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1082"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1083"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1090"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1100"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1110"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1120"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1130"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1140"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1150"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1160"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1170"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1180"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1190"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1200"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1210"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1300"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1301"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1310"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1315"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1320"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1325"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1330"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1331"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1332"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1340"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1341"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1342"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1348"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1350"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1357"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1360"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1367"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1370"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1380"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1390"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1400"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1401"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1402"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1404"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1410"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1420"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1421"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1428"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1430"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1435"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1440"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1450"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1457"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1460"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1461"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1470"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1471"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1472"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1473"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1474"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1476"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1480"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1490"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1495"
if trim(cstr(strSelGriffID)) = "21" then CompareGriffID = true
case "1500"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1501"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1502"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1540"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1541"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1547"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1560"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1570"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1600"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1601"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1602"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1620"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1630"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1640"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1650"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1651"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1652"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1653"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1654"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1670"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1671"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1673"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1674"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1700"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1701"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1702"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1703"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1730"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1731"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1740"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1741"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1742"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1745"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1750"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1755"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1760"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1761"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1770"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1780"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1785"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1790"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1800"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1820"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1830"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1831"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1840"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1850"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1851"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1852"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1853"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1860"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1861"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1880"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1910"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1930"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1931"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1932"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1933"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1950"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1970"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1980"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1981"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "1982"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "2000"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2018"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2020"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2030"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2040"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2050"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2060"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2070"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2100"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2110"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2140"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2150"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2160"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2170"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2180"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2200"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2220"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2221"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2222"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2223"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2230"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2235"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2240"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2242"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2243"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2250"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2260"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2270"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2275"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2280"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2288"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2290"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2300"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2310"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2320"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2321"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2322"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2323"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2328"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2330"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2340"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2350"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2360"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2370"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2380"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2381"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2382"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2387"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2390"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2400"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2430"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2431"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2440"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2450"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2460"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2470"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2480"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2490"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2491"
if trim(cstr(strSelGriffID)) = "25" then CompareGriffID = true
case "2500"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2520"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2530"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2531"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2540"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2547"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2550"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2560"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2570"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2580"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2590"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2600"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2610"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2620"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2627"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2630"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2640"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2650"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2660"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2800"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2801"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2811"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2812"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2820"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2830"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2840"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2845"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2850"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2860"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2861"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2870"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2880"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2890"
if trim(cstr(strSelGriffID)) = "17" then CompareGriffID = true
case "2900"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2910"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2920"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2930"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2940"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2950"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2960"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2970"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2980"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "2990"
if trim(cstr(strSelGriffID)) = "1" then CompareGriffID = true
case "3000"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3001"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3010"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3012"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3018"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3020"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3040"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3050"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3051"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3052"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3053"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3054"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3060"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3061"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3070"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3071"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3078"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3080"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3090"
if trim(cstr(strSelGriffID)) = "3" then CompareGriffID = true
case "3110"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3111"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3118"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3120"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3128"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3130"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3140"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3150"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3190"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3191"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3200"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3201"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3202"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3210"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3211"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3212"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3220"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3221"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3270"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3271"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3272"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3290"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3293"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3294"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3300"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3320"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3321"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3350"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3360"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3370"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3380"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3381"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3384"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3390"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3391"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3400"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3401"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3404"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3440"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3450"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3454"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3460"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3461"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3470"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3471"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3472"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3473"
if trim(cstr(strSelGriffID)) = "14" then CompareGriffID = true
case "3500"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3501"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3510"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3511"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3512"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3520"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3530"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3540"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3545"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3550"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3560"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3570"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3580"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3581"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3582"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3583"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3590"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3600"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3620"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3621"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3630"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3631"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3640"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3650"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3660"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3665"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3668"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3670"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3680"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3690"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3700"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3717"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3720"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3721"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3722"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3723"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3724"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3730"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3732"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3740"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3742"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3746"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3770"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3790"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3791"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3792"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3793"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3798"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3800"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3803"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3806"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3830"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3831"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3832"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3840"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3850"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3870"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3890"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3891"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3900"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3910"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3920"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3930"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3940"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3941"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3945"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3950"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3960"
if trim(cstr(strSelGriffID)) = "23" then CompareGriffID = true
case "3970"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3971"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3980"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "3990"
if trim(cstr(strSelGriffID)) = "10" then CompareGriffID = true
case "4000"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4020"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4030"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4031"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4032"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4040"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4041"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4042"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4050"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4051"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4052"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4053"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4090"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4100"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4101"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4102"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4120"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4121"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4122"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4130"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4140"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4141"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4160"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4161"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4162"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4163"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4170"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4171"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4180"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4181"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4190"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4210"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4217"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4218"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4219"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4250"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4252"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4253"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4254"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4257"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4260"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4261"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4263"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4280"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4287"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4300"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4317"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4340"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4342"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4347"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4350"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4351"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4357"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4360"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4367"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4400"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4420"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4430"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4431"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4432"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4450"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4451"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4452"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4453"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4458"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4460"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4470"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4480"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4500"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4520"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4530"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4537"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4540"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4550"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4557"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4560"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4570"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4577"
if trim(cstr(strSelGriffID)) = "11" then CompareGriffID = true
case "4590"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4600"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4601"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4602"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4606"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4607"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4608"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4610"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4620"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4621"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4623"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4624"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4630"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4631"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4632"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4633"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4650"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4651"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4652"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4653"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4654"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4670"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4671"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4672"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4680"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4681"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4682"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4683"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4684"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4690"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4700"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4701"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4710"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4711"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4720"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4721"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4728"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4730"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4731"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4750"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4760"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4761"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4770"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4771"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4780"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4782"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4783"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4784"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4790"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4791"
if trim(cstr(strSelGriffID)) = "8" then CompareGriffID = true
case "4800"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4801"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4802"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4820"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4821"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4830"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4831"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4834"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4837"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4840"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4841"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4845"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4850"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4851"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4852"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4860"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4861"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4870"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4877"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4880"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4890"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4900"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4910"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4920"
if trim(cstr(strSelGriffID)) = "15" then CompareGriffID = true
case "4950"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4960"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4970"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4980"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4983"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4987"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "4990"
if trim(cstr(strSelGriffID)) = "26" then CompareGriffID = true
case "5000"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5001"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5002"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5003"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5004"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5020"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5021"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5022"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5024"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5030"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5031"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5032"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5060"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5070"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5080"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5081"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5100"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5101"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5140"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5150"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5170"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5190"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5300"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5310"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5330"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5332"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5333"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5334"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5336"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5340"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5350"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5351"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5352"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5353"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5354"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5360"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5361"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5362"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5363"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5364"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5370"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5372"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5374"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5376"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5377"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5380"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5500"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5501"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5502"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5503"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5504"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5520"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5521"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5522"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5523"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5524"
if trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5530"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5537"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5540"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5541"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5542"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5543"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5544"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5550"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5555"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5560"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5561"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5562"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5563"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5564"
if trim(cstr(strSelGriffID)) = "6" or trim(cstr(strSelGriffID)) = "19" then CompareGriffID = true
case "5570"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5571"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5572"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5573"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5574"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5575"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5576"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5580"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5590"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5600"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5620"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5621"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5630"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5640"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5641"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5644"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5646"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5650"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5651"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5660"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5670"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "5680"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "6000"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6001"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6010"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6020"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6030"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6031"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6032"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6040"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6041"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6042"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6043"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6044"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6060"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6061"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6110"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6111"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6120"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6140"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6141"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6142"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6150"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6180"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6181"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6182"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6183"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6200"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6210"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6211"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6220"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6221"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6222"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6223"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6224"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6230"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6238"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6240"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6250"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6280"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6440"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6441"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6460"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6461"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6462"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6463"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6464"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6470"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6500"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6511"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6530"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6531"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6532"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6533"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6534"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6536"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6540"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6542"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6543"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6560"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6567"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6590"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6591"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6592"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6593"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6594"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6596"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "6600"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6630"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6637"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6640"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6642"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6660"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6661"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6662"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6663"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6666"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6670"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6671"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6672"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6673"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6674"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6680"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6681"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6686"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6687"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6688"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6690"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6692"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6698"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6700"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6704"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6706"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6717"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6720"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6721"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6723"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6724"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6730"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6740"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6741"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6742"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6743"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6747"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6750"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6760"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6761"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6762"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6767"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6769"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6780"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6781"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6782"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6790"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6791"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6792"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6800"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6810"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6811"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6812"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6813"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6820"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6821"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6823"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6824"
if trim(cstr(strSelGriffID)) = "31" then CompareGriffID = true
case "6830"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6831"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6832"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6833"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6834"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6836"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6838"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6840"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6850"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6851"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6852"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6853"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6856"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6860"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6870"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6880"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6887"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6890"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6900"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6920"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6921"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6922"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6924"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6927"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6929"
if trim(cstr(strSelGriffID)) = "20" then CompareGriffID = true
case "6940"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6941"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6950"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "6951"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "6952"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "6953"
if trim(cstr(strSelGriffID)) = "6" then CompareGriffID = true
case "6960"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6970"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6971"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6972"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6980"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6982"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6983"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6984"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6986"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6987"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6990"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "6997"
if trim(cstr(strSelGriffID)) = "16" then CompareGriffID = true
case "7000"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7010"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7011"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7012"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7020"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7021"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7022"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7024"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7030"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7031"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7032"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7033"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7034"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7040"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7041"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7050"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7060"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7061"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7062"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7063"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7070"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7080"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7090"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7100"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7110"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7120"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7130"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7131"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7133"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7134"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7140"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7141"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7160"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7170"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7180"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7181"
if trim(cstr(strSelGriffID)) = "4" then CompareGriffID = true
case "7190"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7191"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7300"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7301"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7320"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7321"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7322"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7330"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7331"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7332"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7333"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7334"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7340"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7350"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7370"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7380"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7382"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7387"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7390"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7500"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7501"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7502"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7503"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7504"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7506"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7520"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7521"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7522"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7530"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7531"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7532"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7533"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7534"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7536"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7538"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7540"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7542"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7543"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7548"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7600"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7601"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7602"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7603"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7604"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7608"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7610"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7611"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7618"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7620"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7621"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7622"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7623"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7624"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7640"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7641"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7642"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7643"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7700"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7711"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7712"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7730"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7740"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7742"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7743"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7750"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7760"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7780"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7781"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7782"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7783"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7784"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7800"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7801"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7802"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7803"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7804"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7810"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7811"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7812"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7822"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7823"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7830"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7850"
if trim(cstr(strSelGriffID)) = "30" then CompareGriffID = true
case "7860"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7861"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7862"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7863"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7864"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7866"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7870"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7880"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7890"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7900"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7901"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7903"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7904"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7906"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7910"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7911"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7912"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7940"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7941"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7942"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7943"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7950"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7951"
if trim(cstr(strSelGriffID)) = "18" then CompareGriffID = true
case "7970"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7971"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7972"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "7973"
if trim(cstr(strSelGriffID)) = "24" then CompareGriffID = true
case "8000"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8020"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8200"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8210"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8211"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8300"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8301"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8310"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8340"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8370"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8377"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8380"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8400"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8420"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8421"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8430"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8431"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8432"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8433"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8434"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8450"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8460"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8470"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8480"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8490"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8500"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8501"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8510"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8511"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8520"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8530"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8531"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8540"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8550"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8551"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8552"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8553"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8554"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8560"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8570"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8572"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8573"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8580"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8581"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8582"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8583"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8587"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8600"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8610"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8620"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8630"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8640"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8647"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8650"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8660"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8670"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8670"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8680"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8690"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8691"
if trim(cstr(strSelGriffID)) = "27" then CompareGriffID = true
case "8700"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8710"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8720"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8730"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8740"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8750"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8755"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8760"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8770"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8780"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8790"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8791"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8792"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8793"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8800"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8810"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8820"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8830"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8840"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8850"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8851"
if trim(cstr(strSelGriffID)) = "2" then CompareGriffID = true
case "8860"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8870"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8880"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8890"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8900"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8902"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8904"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8906"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8908"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8920"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8930"
if trim(cstr(strSelGriffID)) = "13" then CompareGriffID = true
case "8940"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8950"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8951"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8952"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8953"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8954"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8956"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8957"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8958"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8970"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8972"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8978"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "8980"
if trim(cstr(strSelGriffID)) = "12" then CompareGriffID = true
case "9000"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9030"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9031"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9032"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9040"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9041"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9042"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9050"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9051"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9052"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9060"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9070"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9080"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9090"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9100"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9111"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9112"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9120"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9130"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9140"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9150"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9160"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9170"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9180"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9185"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9190"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9200"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9220"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9230"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9240"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9250"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9255"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9260"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9270"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9280"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9290"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9300"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9308"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9310"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9320"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9340"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9400"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9401"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9402"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9403"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9404"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9406"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9420"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9450"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9451"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9470"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9472"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9473"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9500"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9506"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9520"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9521"
if trim(cstr(strSelGriffID)) = "5" then CompareGriffID = true
case "9550"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9551"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9552"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9570"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9571"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9572"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9600"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9620"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9630"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9636"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9660"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9661"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9667"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9680"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9681"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9688"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9690"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9700"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9750"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9770"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9771"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9772"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9790"
if trim(cstr(strSelGriffID)) = "22" then CompareGriffID = true
case "9800"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9810"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9820"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9830"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9831"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9840"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9850"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9860"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9870"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9880"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9881"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9890"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9900"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9910"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9920"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9921"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9930"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9931"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9932"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9940"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9950"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9960"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9961"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9968"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9970"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9971"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9980"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9981"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9982"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9988"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9990"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9991"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true
case "9992"
if trim(cstr(strSelGriffID)) = "9" then CompareGriffID = true

end select
end function




Function  GetGriffID(PC)
GetGriffID = 0
Select case trim(cstr(PC))
case "1000" GetGriffID = 3
case "1008" GetGriffID = 3
case "1009" GetGriffID = 3
case "1010" GetGriffID = 3
case "1019" GetGriffID = 3
case "1020" GetGriffID = 3
case "1030" GetGriffID = 3
case "1040" GetGriffID = 3
case "1041" GetGriffID = 3
case "1042" GetGriffID = 3
case "1047" GetGriffID = 3
case "1048" GetGriffID = 3
case "1049" GetGriffID = 3
case "1050" GetGriffID = 3
case "1060" GetGriffID = 3
case "1070" GetGriffID = 3
case "1080" GetGriffID = 3
case "1081" GetGriffID = 3
case "1082" GetGriffID = 3
case "1083" GetGriffID = 3
case "1090" GetGriffID = 3
case "1100" GetGriffID = 3
case "1110" GetGriffID = 3
case "1120" GetGriffID = 3
case "1130" GetGriffID = 3
case "1140" GetGriffID = 3
case "1150" GetGriffID = 3
case "1160" GetGriffID = 3
case "1170" GetGriffID = 3
case "1180" GetGriffID = 3
case "1190" GetGriffID = 3
case "1200" GetGriffID = 3
case "1210" GetGriffID = 3
case "1300" GetGriffID = 21
case "1301" GetGriffID = 21
case "1310" GetGriffID = 21
case "1315" GetGriffID = 21
case "1320" GetGriffID = 21
case "1325" GetGriffID = 21
case "1330" GetGriffID = 21
case "1331" GetGriffID = 21
case "1332" GetGriffID = 21
case "1340" GetGriffID = 21
case "1341" GetGriffID = 21
case "1342" GetGriffID = 21
case "1348" GetGriffID = 21
case "1350" GetGriffID = 21
case "1357" GetGriffID = 21
case "1360" GetGriffID = 21
case "1367" GetGriffID = 21
case "1370" GetGriffID = 21
case "1380" GetGriffID = 21
case "1390" GetGriffID = 21
case "1400" GetGriffID = 21
case "1401" GetGriffID = 21
case "1402" GetGriffID = 21
case "1404" GetGriffID = 21
case "1410" GetGriffID = 21
case "1420" GetGriffID = 21
case "1421" GetGriffID = 21
case "1428" GetGriffID = 21
case "1430" GetGriffID = 21
case "1435" GetGriffID = 21
case "1440" GetGriffID = 21
case "1450" GetGriffID = 21
case "1457" GetGriffID = 21
case "1460" GetGriffID = 21
case "1461" GetGriffID = 21
case "1470" GetGriffID = 21
case "1471" GetGriffID = 21
case "1472" GetGriffID = 21
case "1473" GetGriffID = 21
case "1474" GetGriffID = 21
case "1476" GetGriffID = 21
case "1480" GetGriffID = 21
case "1490" GetGriffID = 21
case "1495" GetGriffID = 21
case "1500" GetGriffID = 3
case "1501" GetGriffID = 3
case "1502" GetGriffID = 3
case "1540" GetGriffID = 3
case "1541" GetGriffID = 3
case "1547" GetGriffID = 3
case "1560" GetGriffID = 3
case "1570" GetGriffID = 3
case "1600" GetGriffID = 3
case "1601" GetGriffID = 3
case "1602" GetGriffID = 3
case "1620" GetGriffID = 3
case "1630" GetGriffID = 3
case "1640" GetGriffID = 3
case "1650" GetGriffID = 3
case "1651" GetGriffID = 3
case "1652" GetGriffID = 3
case "1653" GetGriffID = 3
case "1654" GetGriffID = 3
case "1670" GetGriffID = 3
case "1671" GetGriffID = 3
case "1673" GetGriffID = 3
case "1674" GetGriffID = 3
case "1700" GetGriffID = 3
case "1701" GetGriffID = 3
case "1702" GetGriffID = 3
case "1703" GetGriffID = 3
case "1730" GetGriffID = 3
case "1731" GetGriffID = 3
case "1740" GetGriffID = 3
case "1741" GetGriffID = 3
case "1742" GetGriffID = 3
case "1745" GetGriffID = 3
case "1750" GetGriffID = 3
case "1755" GetGriffID = 3
case "1760" GetGriffID = 3
case "1761" GetGriffID = 3
case "1770" GetGriffID = 3
case "1780" GetGriffID = 3
case "1785" GetGriffID = 3
case "1790" GetGriffID = 3
case "1800" GetGriffID = 3
case "1820" GetGriffID = 3
case "1830" GetGriffID = 3
case "1831" GetGriffID = 3
case "1840" GetGriffID = 3
case "1850" GetGriffID = 3
case "1851" GetGriffID = 3
case "1852" GetGriffID = 3
case "1853" GetGriffID = 3
case "1860" GetGriffID = 3
case "1861" GetGriffID = 3
case "1880" GetGriffID = 3
case "1910" GetGriffID = 3
case "1930" GetGriffID = 3
case "1931" GetGriffID = 3
case "1932" GetGriffID = 3
case "1933" GetGriffID = 3
case "1950" GetGriffID = 3
case "1970" GetGriffID = 3
case "1980" GetGriffID = 3
case "1981" GetGriffID = 3
case "1982" GetGriffID = 3
case "2000" GetGriffID = 1
case "2018" GetGriffID = 1
case "2020" GetGriffID = 1
case "2030" GetGriffID = 1
case "2040" GetGriffID = 1
case "2050" GetGriffID = 1
case "2060" GetGriffID = 1
case "2070" GetGriffID = 1
case "2100" GetGriffID = 1
case "2110" GetGriffID = 1
case "2140" GetGriffID = 1
case "2150" GetGriffID = 1
case "2160" GetGriffID = 1
case "2170" GetGriffID = 1
case "2180" GetGriffID = 1
case "2200" GetGriffID = 25
case "2220" GetGriffID = 17
case "2221" GetGriffID = 17
case "2222" GetGriffID = 17
case "2223" GetGriffID = 17
case "2230" GetGriffID = 25
case "2235" GetGriffID = 25
case "2240" GetGriffID = 1
case "2242" GetGriffID = 1
case "2243" GetGriffID = 1
case "2250" GetGriffID = 25
case "2260" GetGriffID = 25
case "2270" GetGriffID = 25
case "2275" GetGriffID = 25
case "2280" GetGriffID = 25
case "2288" GetGriffID = 25
case "2290" GetGriffID = 25
case "2300" GetGriffID = 25
case "2310" GetGriffID = 25
case "2320" GetGriffID = 25
case "2321" GetGriffID = 25
case "2322" GetGriffID = 25
case "2323" GetGriffID = 25
case "2328" GetGriffID = 25
case "2330" GetGriffID = 25
case "2340" GetGriffID = 25
case "2350" GetGriffID = 25
case "2360" GetGriffID = 25
case "2370" GetGriffID = 25
case "2380" GetGriffID = 25
case "2381" GetGriffID = 25
case "2382" GetGriffID = 25
case "2387" GetGriffID = 25
case "2390" GetGriffID = 1
case "2400" GetGriffID = 25
case "2430" GetGriffID = 25
case "2431" GetGriffID = 25
case "2440" GetGriffID = 25
case "2450" GetGriffID = 25
case "2460" GetGriffID = 25
case "2470" GetGriffID = 25
case "2480" GetGriffID = 25
case "2490" GetGriffID = 25
case "2491" GetGriffID = 25
case "2500" GetGriffID = 17
case "2520" GetGriffID = 1
case "2530" GetGriffID = 1
case "2531" GetGriffID = 1
case "2540" GetGriffID = 1
case "2547" GetGriffID = 1
case "2550" GetGriffID = 1
case "2560" GetGriffID = 17
case "2570" GetGriffID = 17
case "2580" GetGriffID = 17
case "2590" GetGriffID = 17
case "2600" GetGriffID = 1
case "2610" GetGriffID = 1
case "2620" GetGriffID = 1
case "2627" GetGriffID = 1
case "2630" GetGriffID = 1
case "2640" GetGriffID = 1
case "2650" GetGriffID = 1
case "2660" GetGriffID = 1
case "2800" GetGriffID = 17
case "2801" GetGriffID = 17
case "2811" GetGriffID = 17
case "2812" GetGriffID = 17
case "2820" GetGriffID = 17
case "2830" GetGriffID = 17
case "2840" GetGriffID = 1
case "2845" GetGriffID = 1
case "2850" GetGriffID = 1
case "2860" GetGriffID = 17
case "2861" GetGriffID = 17
case "2870" GetGriffID = 17
case "2880" GetGriffID = 17
case "2890" GetGriffID = 17
case "2900" GetGriffID = 1
case "2910" GetGriffID = 1
case "2920" GetGriffID = 1
case "2930" GetGriffID = 1
case "2940" GetGriffID = 1
case "2950" GetGriffID = 1
case "2960" GetGriffID = 1
case "2970" GetGriffID = 1
case "2980" GetGriffID = 1
case "2990" GetGriffID = 1
case "3000" GetGriffID = 14
case "3001" GetGriffID = 14
case "3010" GetGriffID = 14
case "3012" GetGriffID = 14
case "3018" GetGriffID = 14
case "3020" GetGriffID = 14
case "3040" GetGriffID = 14
case "3050" GetGriffID = 14
case "3051" GetGriffID = 14
case "3052" GetGriffID = 14
case "3053" GetGriffID = 14
case "3054" GetGriffID = 14
case "3060" GetGriffID = 14
case "3061" GetGriffID = 14
case "3070" GetGriffID = 14
case "3071" GetGriffID = 14
case "3078" GetGriffID = 14
case "3080" GetGriffID = 14
case "3090" GetGriffID = 3
case "3110" GetGriffID = 14
case "3111" GetGriffID = 14
case "3118" GetGriffID = 14
case "3120" GetGriffID = 14
case "3128" GetGriffID = 14
case "3130" GetGriffID = 14
case "3140" GetGriffID = 14
case "3150" GetGriffID = 14
case "3190" GetGriffID = 14
case "3191" GetGriffID = 14
case "3200" GetGriffID = 14
case "3201" GetGriffID = 14
case "3202" GetGriffID = 14
case "3210" GetGriffID = 14
case "3211" GetGriffID = 14
case "3212" GetGriffID = 14
case "3220" GetGriffID = 14
case "3221" GetGriffID = 14
case "3270" GetGriffID = 14
case "3271" GetGriffID = 14
case "3272" GetGriffID = 14
case "3290" GetGriffID = 14
case "3293" GetGriffID = 14
case "3294" GetGriffID = 14
case "3300" GetGriffID = 14
case "3320" GetGriffID = 14
case "3321" GetGriffID = 14
case "3350" GetGriffID = 14
case "3360" GetGriffID = 14
case "3370" GetGriffID = 14
case "3380" GetGriffID = 14
case "3381" GetGriffID = 14
case "3384" GetGriffID = 14
case "3390" GetGriffID = 14
case "3391" GetGriffID = 14
case "3400" GetGriffID = 14
case "3401" GetGriffID = 14
case "3404" GetGriffID = 14
case "3440" GetGriffID = 14
case "3450" GetGriffID = 14
case "3454" GetGriffID = 14
case "3460" GetGriffID = 14
case "3461" GetGriffID = 14
case "3470" GetGriffID = 14
case "3471" GetGriffID = 14
case "3472" GetGriffID = 14
case "3473" GetGriffID = 14
case "3500" GetGriffID = 10
case "3501" GetGriffID = 10
case "3510" GetGriffID = 10
case "3511" GetGriffID = 10
case "3512" GetGriffID = 10
case "3520" GetGriffID = 10
case "3530" GetGriffID = 10
case "3540" GetGriffID = 10
case "3545" GetGriffID = 10
case "3550" GetGriffID = 10
case "3560" GetGriffID = 10
case "3570" GetGriffID = 23
case "3580" GetGriffID = 10
case "3581" GetGriffID = 10
case "3582" GetGriffID = 10
case "3583" GetGriffID = 10
case "3590" GetGriffID = 10
case "3600" GetGriffID = 23
case "3620" GetGriffID = 23
case "3621" GetGriffID = 23
case "3630" GetGriffID = 23
case "3631" GetGriffID = 23
case "3640" GetGriffID = 23
case "3650" GetGriffID = 23
case "3660" GetGriffID = 23
case "3665" GetGriffID = 23
case "3668" GetGriffID = 23
case "3670" GetGriffID = 23
case "3680" GetGriffID = 23
case "3690" GetGriffID = 23
case "3700" GetGriffID = 23
case "3717" GetGriffID = 23
case "3720" GetGriffID = 23
case "3721" GetGriffID = 23
case "3722" GetGriffID = 23
case "3723" GetGriffID = 23
case "3724" GetGriffID = 23
case "3730" GetGriffID = 23
case "3732" GetGriffID = 23
case "3740" GetGriffID = 23
case "3742" GetGriffID = 23
case "3746" GetGriffID = 23
case "3770" GetGriffID = 23
case "3790" GetGriffID = 23
case "3791" GetGriffID = 23
case "3792" GetGriffID = 23
case "3793" GetGriffID = 23
case "3798" GetGriffID = 23
case "3800" GetGriffID = 10
case "3803" GetGriffID = 10
case "3806" GetGriffID = 10
case "3830" GetGriffID = 23
case "3831" GetGriffID = 23
case "3832" GetGriffID = 23
case "3840" GetGriffID = 23
case "3850" GetGriffID = 10
case "3870" GetGriffID = 23
case "3890" GetGriffID = 10
case "3891" GetGriffID = 10
case "3900" GetGriffID = 10
case "3910" GetGriffID = 10
case "3920" GetGriffID = 10
case "3930" GetGriffID = 10
case "3940" GetGriffID = 10
case "3941" GetGriffID = 10
case "3945" GetGriffID = 10
case "3950" GetGriffID = 23
case "3960" GetGriffID = 23
case "3970" GetGriffID = 10
case "3971" GetGriffID = 10
case "3980" GetGriffID = 10
case "3990" GetGriffID = 10
case "4000" GetGriffID = 15
case "4020" GetGriffID = 15
case "4030" GetGriffID = 15
case "4031" GetGriffID = 15
case "4032" GetGriffID = 15
case "4040" GetGriffID = 15
case "4041" GetGriffID = 15
case "4042" GetGriffID = 15
case "4050" GetGriffID = 15
case "4051" GetGriffID = 15
case "4052" GetGriffID = 15
case "4053" GetGriffID = 15
case "4090" GetGriffID = 15
case "4100" GetGriffID = 15
case "4101" GetGriffID = 15
case "4102" GetGriffID = 15
case "4120" GetGriffID = 11
case "4121" GetGriffID = 11
case "4122" GetGriffID = 11
case "4130" GetGriffID = 15
case "4140" GetGriffID = 15
case "4141" GetGriffID = 15
case "4160" GetGriffID = 11
case "4161" GetGriffID = 11
case "4162" GetGriffID = 11
case "4163" GetGriffID = 11
case "4170" GetGriffID = 11
case "4171" GetGriffID = 11
case "4180" GetGriffID = 11
case "4181" GetGriffID = 11
case "4190" GetGriffID = 11
case "4210" GetGriffID = 11
case "4217" GetGriffID = 11
case "4218" GetGriffID = 11
case "4219" GetGriffID = 11
case "4250" GetGriffID = 15
case "4252" GetGriffID = 15
case "4253" GetGriffID = 15
case "4254" GetGriffID = 15
case "4257" GetGriffID = 15
case "4260" GetGriffID = 11
case "4261" GetGriffID = 11
case "4263" GetGriffID = 11
case "4280" GetGriffID = 11
case "4287" GetGriffID = 11
case "4300" GetGriffID = 15
case "4317" GetGriffID = 15
case "4340" GetGriffID = 15
case "4342" GetGriffID = 15
case "4347" GetGriffID = 15
case "4350" GetGriffID = 15
case "4351" GetGriffID = 15
case "4357" GetGriffID = 15
case "4360" GetGriffID = 15
case "4367" GetGriffID = 15
case "4400" GetGriffID = 15
case "4420" GetGriffID = 15
case "4430" GetGriffID = 15
case "4431" GetGriffID = 15
case "4432" GetGriffID = 15
case "4450" GetGriffID = 15
case "4451" GetGriffID = 15
case "4452" GetGriffID = 15
case "4453" GetGriffID = 15
case "4458" GetGriffID = 15
case "4460" GetGriffID = 15
case "4470" GetGriffID = 15
case "4480" GetGriffID = 15
case "4500" GetGriffID = 11
case "4520" GetGriffID = 11
case "4530" GetGriffID = 11
case "4537" GetGriffID = 11
case "4540" GetGriffID = 11
case "4550" GetGriffID = 11
case "4557" GetGriffID = 11
case "4560" GetGriffID = 11
case "4570" GetGriffID = 11
case "4577" GetGriffID = 11
case "4590" GetGriffID = 15
case "4600" GetGriffID = 15
case "4601" GetGriffID = 15
case "4602" GetGriffID = 15
case "4606" GetGriffID = 15
case "4607" GetGriffID = 15
case "4608" GetGriffID = 15
case "4610" GetGriffID = 15
case "4620" GetGriffID = 15
case "4621" GetGriffID = 15
case "4623" GetGriffID = 15
case "4624" GetGriffID = 15
case "4630" GetGriffID = 15
case "4631" GetGriffID = 15
case "4632" GetGriffID = 15
case "4633" GetGriffID = 15
case "4650" GetGriffID = 26
case "4651" GetGriffID = 26
case "4652" GetGriffID = 26
case "4653" GetGriffID = 26
case "4654" GetGriffID = 26
case "4670" GetGriffID = 15
case "4671" GetGriffID = 15
case "4672" GetGriffID = 15
case "4680" GetGriffID = 15
case "4681" GetGriffID = 15
case "4682" GetGriffID = 15
case "4683" GetGriffID = 15
case "4684" GetGriffID = 15
case "4690" GetGriffID = 26
case "4700" GetGriffID = 8
case "4701" GetGriffID = 8
case "4710" GetGriffID = 8
case "4711" GetGriffID = 8
case "4720" GetGriffID = 8
case "4721" GetGriffID = 8
case "4728" GetGriffID = 8
case "4730" GetGriffID = 8
case "4731" GetGriffID = 8
case "4750" GetGriffID = 8
case "4760" GetGriffID = 8
case "4761" GetGriffID = 8
case "4770" GetGriffID = 8
case "4771" GetGriffID = 8
case "4780" GetGriffID = 8
case "4782" GetGriffID = 8
case "4783" GetGriffID = 8
case "4784" GetGriffID = 8
case "4790" GetGriffID = 8
case "4791" GetGriffID = 8
case "4800" GetGriffID = 26
case "4801" GetGriffID = 26
case "4802" GetGriffID = 26
case "4820" GetGriffID = 26
case "4821" GetGriffID = 26
case "4830" GetGriffID = 26
case "4831" GetGriffID = 26
case "4834" GetGriffID = 26
case "4837" GetGriffID = 26
case "4840" GetGriffID = 26
case "4841" GetGriffID = 26
case "4845" GetGriffID = 26
case "4850" GetGriffID = 26
case "4851" GetGriffID = 26
case "4852" GetGriffID = 26
case "4860" GetGriffID = 26
case "4861" GetGriffID = 26
case "4870" GetGriffID = 15
case "4877" GetGriffID = 26
case "4880" GetGriffID = 26
case "4890" GetGriffID = 26
case "4900" GetGriffID = 26
case "4910" GetGriffID = 26
case "4920" GetGriffID = 15
case "4950" GetGriffID = 26
case "4960" GetGriffID = 26
case "4970" GetGriffID = 26
case "4980" GetGriffID = 26
case "4983" GetGriffID = 26
case "4987" GetGriffID = 26
case "4990" GetGriffID = 26
case "5000" GetGriffID = 19
case "5001" GetGriffID = 19
case "5002" GetGriffID = 19
case "5003" GetGriffID = 19
case "5004" GetGriffID = 19
case "5020" GetGriffID = 19
case "5021" GetGriffID = 19
case "5022" GetGriffID = 19
case "5024" GetGriffID = 19
case "5030" GetGriffID = 19
case "5031" GetGriffID = 19
case "5032" GetGriffID = 19
case "5060" GetGriffID = 19
case "5070" GetGriffID = 19
case "5080" GetGriffID = 19
case "5081" GetGriffID = 19
case "5100" GetGriffID = 19
case "5101" GetGriffID = 19
case "5140" GetGriffID = 19
case "5150" GetGriffID = 19
case "5170" GetGriffID = 19
case "5190" GetGriffID = 19
case "5300" GetGriffID = 19
case "5310" GetGriffID = 19
case "5330" GetGriffID = 19
case "5332" GetGriffID = 19
case "5333" GetGriffID = 19
case "5334" GetGriffID = 19
case "5336" GetGriffID = 19
case "5340" GetGriffID = 19
case "5350" GetGriffID = 19
case "5351" GetGriffID = 19
case "5352" GetGriffID = 19
case "5353" GetGriffID = 19
case "5354" GetGriffID = 19
case "5360" GetGriffID = 19
case "5361" GetGriffID = 19
case "5362" GetGriffID = 19
case "5363" GetGriffID = 19
case "5364" GetGriffID = 19
case "5370" GetGriffID = 19
case "5372" GetGriffID = 19
case "5374" GetGriffID = 19
case "5376" GetGriffID = 19
case "5377" GetGriffID = 19
case "5380" GetGriffID = 19
case "5500" GetGriffID = 19
case "5501" GetGriffID = 19
case "5502" GetGriffID = 19
case "5503" GetGriffID = 19
case "5504" GetGriffID = 19
case "5520" GetGriffID = 19
case "5521" GetGriffID = 19
case "5522" GetGriffID = 19
case "5523" GetGriffID = 19
case "5524" GetGriffID = 19
case "5530" GetGriffID = 19
case "5537" GetGriffID = 19
case "5540" GetGriffID = 19
case "5541" GetGriffID = 19
case "5542" GetGriffID = 19
case "5543" GetGriffID = 19
case "5544" GetGriffID = 19
case "5550" GetGriffID = 19
case "5555" GetGriffID = 19
case "5560" GetGriffID = 19
case "5561" GetGriffID = 19
case "5562" GetGriffID = 19
case "5563" GetGriffID = 6
case "5564" GetGriffID = 6
case "5570" GetGriffID = 6
case "5571" GetGriffID = 6
case "5572" GetGriffID = 6
case "5573" GetGriffID = 6
case "5574" GetGriffID = 6
case "5575" GetGriffID = 6
case "5576" GetGriffID = 6
case "5580" GetGriffID = 6
case "5590" GetGriffID = 6
case "5600" GetGriffID = 6
case "5620" GetGriffID = 6
case "5621" GetGriffID = 6
case "5630" GetGriffID = 6
case "5640" GetGriffID = 6
case "5641" GetGriffID = 6
case "5644" GetGriffID = 6
case "5646" GetGriffID = 6
case "5650" GetGriffID = 6
case "5651" GetGriffID = 6
case "5660" GetGriffID = 6
case "5670" GetGriffID = 6
case "5680" GetGriffID = 6
case "6000" GetGriffID = 4
case "6001" GetGriffID = 4
case "6010" GetGriffID = 4
case "6020" GetGriffID = 4
case "6030" GetGriffID = 4
case "6031" GetGriffID = 4
case "6032" GetGriffID = 4
case "6040" GetGriffID = 4
case "6041" GetGriffID = 4
case "6042" GetGriffID = 4
case "6043" GetGriffID = 4
case "6044" GetGriffID = 4
case "6060" GetGriffID = 4
case "6061" GetGriffID = 4
case "6110" GetGriffID = 4
case "6111" GetGriffID = 4
case "6120" GetGriffID = 4
case "6140" GetGriffID = 4
case "6141" GetGriffID = 4
case "6142" GetGriffID = 4
case "6150" GetGriffID = 4
case "6180" GetGriffID = 4
case "6181" GetGriffID = 4
case "6182" GetGriffID = 4
case "6183" GetGriffID = 4
case "6200" GetGriffID = 4
case "6210" GetGriffID = 4
case "6211" GetGriffID = 4
case "6220" GetGriffID = 4
case "6221" GetGriffID = 4
case "6222" GetGriffID = 4
case "6223" GetGriffID = 4
case "6224" GetGriffID = 4
case "6230" GetGriffID = 4
case "6238" GetGriffID = 4
case "6240" GetGriffID = 4
case "6250" GetGriffID = 4
case "6280" GetGriffID = 4
case "6440" GetGriffID = 4
case "6441" GetGriffID = 4
case "6460" GetGriffID = 4
case "6461" GetGriffID = 4
case "6462" GetGriffID = 4
case "6463" GetGriffID = 4
case "6464" GetGriffID = 4
case "6470" GetGriffID = 4
case "6500" GetGriffID = 4
case "6511" GetGriffID = 4
case "6530" GetGriffID = 4
case "6531" GetGriffID = 4
case "6532" GetGriffID = 4
case "6533" GetGriffID = 4
case "6534" GetGriffID = 4
case "6536" GetGriffID = 4
case "6540" GetGriffID = 4
case "6542" GetGriffID = 4
case "6543" GetGriffID = 4
case "6560" GetGriffID = 4
case "6567" GetGriffID = 4
case "6590" GetGriffID = 4
case "6591" GetGriffID = 4
case "6592" GetGriffID = 4
case "6593" GetGriffID = 4
case "6594" GetGriffID = 4
case "6596" GetGriffID = 4
case "6600" GetGriffID = 20
case "6630" GetGriffID = 31
case "6637" GetGriffID = 20
case "6640" GetGriffID = 20
case "6642" GetGriffID = 20
case "6660" GetGriffID = 16
case "6661" GetGriffID = 16
case "6662" GetGriffID = 16
case "6663" GetGriffID = 16
case "6666" GetGriffID = 16
case "6670" GetGriffID = 16
case "6671" GetGriffID = 16
case "6672" GetGriffID = 16
case "6673" GetGriffID = 16
case "6674" GetGriffID = 16
case "6680" GetGriffID = 20
case "6681" GetGriffID = 20
case "6686" GetGriffID = 20
case "6687" GetGriffID = 20
case "6688" GetGriffID = 20
case "6690" GetGriffID = 16
case "6692" GetGriffID = 16
case "6698" GetGriffID = 16
case "6700" GetGriffID = 31
case "6704" GetGriffID = 31
case "6706" GetGriffID = 31
case "6717" GetGriffID = 31
case "6720" GetGriffID = 31
case "6721" GetGriffID = 31
case "6723" GetGriffID = 31
case "6724" GetGriffID = 31
case "6730" GetGriffID = 31
case "6740" GetGriffID = 31
case "6741" GetGriffID = 31
case "6742" GetGriffID = 31
case "6743" GetGriffID = 31
case "6747" GetGriffID = 31
case "6750" GetGriffID = 31
case "6760" GetGriffID = 31
case "6761" GetGriffID = 31
case "6762" GetGriffID = 31
case "6767" GetGriffID = 31
case "6769" GetGriffID = 31
case "6780" GetGriffID = 31
case "6781" GetGriffID = 31
case "6782" GetGriffID = 31
case "6790" GetGriffID = 31
case "6791" GetGriffID = 31
case "6792" GetGriffID = 31
case "6800" GetGriffID = 20
case "6810" GetGriffID = 31
case "6811" GetGriffID = 31
case "6812" GetGriffID = 31
case "6813" GetGriffID = 31
case "6820" GetGriffID = 31
case "6821" GetGriffID = 31
case "6823" GetGriffID = 31
case "6824" GetGriffID = 31
case "6830" GetGriffID = 20
case "6831" GetGriffID = 20
case "6832" GetGriffID = 20
case "6833" GetGriffID = 20
case "6834" GetGriffID = 20
case "6836" GetGriffID = 20
case "6838" GetGriffID = 20
case "6840" GetGriffID = 20
case "6850" GetGriffID = 20
case "6851" GetGriffID = 20
case "6852" GetGriffID = 20
case "6853" GetGriffID = 20
case "6856" GetGriffID = 20
case "6860" GetGriffID = 20
case "6870" GetGriffID = 20
case "6880" GetGriffID = 20
case "6887" GetGriffID = 20
case "6890" GetGriffID = 20
case "6900" GetGriffID = 16
case "6920" GetGriffID = 20
case "6921" GetGriffID = 20
case "6922" GetGriffID = 20
case "6924" GetGriffID = 20
case "6927" GetGriffID = 20
case "6929" GetGriffID = 20
case "6940" GetGriffID = 16
case "6941" GetGriffID = 16
case "6950" GetGriffID = 6
case "6951" GetGriffID = 6
case "6952" GetGriffID = 6
case "6953" GetGriffID = 6
case "6960" GetGriffID = 16
case "6970" GetGriffID = 16
case "6971" GetGriffID = 16
case "6972" GetGriffID = 16
case "6980" GetGriffID = 16
case "6982" GetGriffID = 16
case "6983" GetGriffID = 16
case "6984" GetGriffID = 16
case "6986" GetGriffID = 16
case "6987" GetGriffID = 16
case "6990" GetGriffID = 16
case "6997" GetGriffID = 16
case "7000" GetGriffID = 18
case "7010" GetGriffID = 18
case "7011" GetGriffID = 18
case "7012" GetGriffID = 18
case "7020" GetGriffID = 18
case "7021" GetGriffID = 18
case "7022" GetGriffID = 18
case "7024" GetGriffID = 18
case "7030" GetGriffID = 18
case "7031" GetGriffID = 18
case "7032" GetGriffID = 18
case "7033" GetGriffID = 18
case "7034" GetGriffID = 18
case "7040" GetGriffID = 18
case "7041" GetGriffID = 18
case "7050" GetGriffID = 18
case "7060" GetGriffID = 18
case "7061" GetGriffID = 18
case "7062" GetGriffID = 18
case "7063" GetGriffID = 18
case "7070" GetGriffID = 18
case "7080" GetGriffID = 18
case "7090" GetGriffID = 18
case "7100" GetGriffID = 18
case "7110" GetGriffID = 18
case "7120" GetGriffID = 4
case "7130" GetGriffID = 4
case "7131" GetGriffID = 4
case "7133" GetGriffID = 4
case "7134" GetGriffID = 4
case "7140" GetGriffID = 4
case "7141" GetGriffID = 4
case "7160" GetGriffID = 4
case "7170" GetGriffID = 4
case "7180" GetGriffID = 4
case "7181" GetGriffID = 4
case "7190" GetGriffID = 18
case "7191" GetGriffID = 18
case "7300" GetGriffID = 18
case "7301" GetGriffID = 18
case "7320" GetGriffID = 24
case "7321" GetGriffID = 24
case "7322" GetGriffID = 24
case "7330" GetGriffID = 18
case "7331" GetGriffID = 18
case "7332" GetGriffID = 18
case "7333" GetGriffID = 18
case "7334" GetGriffID = 18
case "7340" GetGriffID = 18
case "7350" GetGriffID = 18
case "7370" GetGriffID = 18
case "7380" GetGriffID = 18
case "7382" GetGriffID = 18
case "7387" GetGriffID = 18
case "7390" GetGriffID = 18
case "7500" GetGriffID = 24
case "7501" GetGriffID = 24
case "7502" GetGriffID = 24
case "7503" GetGriffID = 24
case "7504" GetGriffID = 24
case "7506" GetGriffID = 24
case "7520" GetGriffID = 24
case "7521" GetGriffID = 24
case "7522" GetGriffID = 24
case "7530" GetGriffID = 24
case "7531" GetGriffID = 24
case "7532" GetGriffID = 24
case "7533" GetGriffID = 24
case "7534" GetGriffID = 24
case "7536" GetGriffID = 24
case "7538" GetGriffID = 24
case "7540" GetGriffID = 24
case "7542" GetGriffID = 24
case "7543" GetGriffID = 24
case "7548" GetGriffID = 24
case "7600" GetGriffID = 24
case "7601" GetGriffID = 24
case "7602" GetGriffID = 24
case "7603" GetGriffID = 24
case "7604" GetGriffID = 24
case "7608" GetGriffID = 24
case "7610" GetGriffID = 24
case "7611" GetGriffID = 24
case "7618" GetGriffID = 24
case "7620" GetGriffID = 24
case "7621" GetGriffID = 24
case "7622" GetGriffID = 24
case "7623" GetGriffID = 24
case "7624" GetGriffID = 24
case "7640" GetGriffID = 24
case "7641" GetGriffID = 24
case "7642" GetGriffID = 24
case "7643" GetGriffID = 24
case "7700" GetGriffID = 24
case "7711" GetGriffID = 24
case "7712" GetGriffID = 24
case "7730" GetGriffID = 24
case "7740" GetGriffID = 24
case "7742" GetGriffID = 24
case "7743" GetGriffID = 24
case "7750" GetGriffID = 24
case "7760" GetGriffID = 24
case "7780" GetGriffID = 24
case "7781" GetGriffID = 24
case "7782" GetGriffID = 24
case "7783" GetGriffID = 24
case "7784" GetGriffID = 24
case "7800" GetGriffID = 24
case "7801" GetGriffID = 24
case "7802" GetGriffID = 24
case "7803" GetGriffID = 24
case "7804" GetGriffID = 24
case "7810" GetGriffID = 24
case "7811" GetGriffID = 24
case "7812" GetGriffID = 24
case "7822" GetGriffID = 24
case "7823" GetGriffID = 24
case "7830" GetGriffID = 18
case "7850" GetGriffID = 30
case "7860" GetGriffID = 24
case "7861" GetGriffID = 24
case "7862" GetGriffID = 24
case "7863" GetGriffID = 24
case "7864" GetGriffID = 24
case "7866" GetGriffID = 24
case "7870" GetGriffID = 18
case "7880" GetGriffID = 24
case "7890" GetGriffID = 24
case "7900" GetGriffID = 24
case "7901" GetGriffID = 24
case "7903" GetGriffID = 24
case "7904" GetGriffID = 24
case "7906" GetGriffID = 24
case "7910" GetGriffID = 24
case "7911" GetGriffID = 24
case "7912" GetGriffID = 24
case "7940" GetGriffID = 18
case "7941" GetGriffID = 18
case "7942" GetGriffID = 18
case "7943" GetGriffID = 18
case "7950" GetGriffID = 18
case "7951" GetGriffID = 18
case "7970" GetGriffID = 24
case "7971" GetGriffID = 24
case "7972" GetGriffID = 24
case "7973" GetGriffID = 24
case "8000" GetGriffID = 2
case "8020" GetGriffID = 2
case "8200" GetGriffID = 2
case "8210" GetGriffID = 2
case "8211" GetGriffID = 2
case "8300" GetGriffID = 2
case "8301" GetGriffID = 2
case "8310" GetGriffID = 2
case "8340" GetGriffID = 2
case "8370" GetGriffID = 2
case "8377" GetGriffID = 2
case "8380" GetGriffID = 2
case "8400" GetGriffID = 2
case "8420" GetGriffID = 2
case "8421" GetGriffID = 2
case "8430" GetGriffID = 2
case "8431" GetGriffID = 2
case "8432" GetGriffID = 2
case "8433" GetGriffID = 2
case "8434" GetGriffID = 2
case "8450" GetGriffID = 2
case "8460" GetGriffID = 2
case "8470" GetGriffID = 2
case "8480" GetGriffID = 2
case "8490" GetGriffID = 2
case "8500" GetGriffID = 13
case "8501" GetGriffID = 13
case "8510" GetGriffID = 13
case "8511" GetGriffID = 13
case "8520" GetGriffID = 13
case "8530" GetGriffID = 13
case "8531" GetGriffID = 13
case "8540" GetGriffID = 13
case "8550" GetGriffID = 13
case "8551" GetGriffID = 13
case "8552" GetGriffID = 13
case "8553" GetGriffID = 13
case "8554" GetGriffID = 13
case "8560" GetGriffID = 13
case "8570" GetGriffID = 13
case "8572" GetGriffID = 13
case "8573" GetGriffID = 13
case "8580" GetGriffID = 13
case "8581" GetGriffID = 13
case "8582" GetGriffID = 13
case "8583" GetGriffID = 13
case "8587" GetGriffID = 13
case "8600" GetGriffID = 27
case "8610" GetGriffID = 27
case "8620" GetGriffID = 27
case "8630" GetGriffID = 27
case "8640" GetGriffID = 12
case "8647" GetGriffID = 27
case "8650" GetGriffID = 27
case "8660" GetGriffID = 27
case "8670" GetGriffID = 2
case "8670" GetGriffID = 27
case "8680" GetGriffID = 27
case "8690" GetGriffID = 27
case "8691" GetGriffID = 27
case "8700" GetGriffID = 2
case "8710" GetGriffID = 13
case "8720" GetGriffID = 13
case "8730" GetGriffID = 2
case "8740" GetGriffID = 2
case "8750" GetGriffID = 2
case "8755" GetGriffID = 2
case "8760" GetGriffID = 13
case "8770" GetGriffID = 13
case "8780" GetGriffID = 13
case "8790" GetGriffID = 13
case "8791" GetGriffID = 13
case "8792" GetGriffID = 13
case "8793" GetGriffID = 13
case "8800" GetGriffID = 13
case "8810" GetGriffID = 2
case "8820" GetGriffID = 2
case "8830" GetGriffID = 13
case "8840" GetGriffID = 12
case "8850" GetGriffID = 2
case "8851" GetGriffID = 2
case "8860" GetGriffID = 13
case "8870" GetGriffID = 13
case "8880" GetGriffID = 13
case "8890" GetGriffID = 12
case "8900" GetGriffID = 12
case "8902" GetGriffID = 12
case "8904" GetGriffID = 12
case "8906" GetGriffID = 12
case "8908" GetGriffID = 12
case "8920" GetGriffID = 12
case "8930" GetGriffID = 13
case "8940" GetGriffID = 12
case "8950" GetGriffID = 12
case "8951" GetGriffID = 12
case "8952" GetGriffID = 12
case "8953" GetGriffID = 12
case "8954" GetGriffID = 12
case "8956" GetGriffID = 12
case "8957" GetGriffID = 12
case "8958" GetGriffID = 12
case "8970" GetGriffID = 12
case "8972" GetGriffID = 12
case "8978" GetGriffID = 12
case "8980" GetGriffID = 12
case "9000" GetGriffID = 9
case "9030" GetGriffID = 9
case "9031" GetGriffID = 9
case "9032" GetGriffID = 9
case "9040" GetGriffID = 9
case "9041" GetGriffID = 9
case "9042" GetGriffID = 9
case "9050" GetGriffID = 9
case "9051" GetGriffID = 9
case "9052" GetGriffID = 9
case "9060" GetGriffID = 9
case "9070" GetGriffID = 9
case "9080" GetGriffID = 9
case "9090" GetGriffID = 9
case "9100" GetGriffID = 5
case "9111" GetGriffID = 5
case "9112" GetGriffID = 5
case "9120" GetGriffID = 5
case "9130" GetGriffID = 5
case "9140" GetGriffID = 5
case "9150" GetGriffID = 5
case "9160" GetGriffID = 5
case "9170" GetGriffID = 5
case "9180" GetGriffID = 9
case "9185" GetGriffID = 9
case "9190" GetGriffID = 5
case "9200" GetGriffID = 5
case "9220" GetGriffID = 5
case "9230" GetGriffID = 5
case "9240" GetGriffID = 5
case "9250" GetGriffID = 5
case "9255" GetGriffID = 5
case "9260" GetGriffID = 5
case "9270" GetGriffID = 5
case "9280" GetGriffID = 5
case "9290" GetGriffID = 5
case "9300" GetGriffID = 5
case "9308" GetGriffID = 5
case "9310" GetGriffID = 5
case "9320" GetGriffID = 5
case "9340" GetGriffID = 5
case "9400" GetGriffID = 5
case "9401" GetGriffID = 5
case "9402" GetGriffID = 5
case "9403" GetGriffID = 5
case "9404" GetGriffID = 5
case "9406" GetGriffID = 5
case "9420" GetGriffID = 5
case "9450" GetGriffID = 5
case "9451" GetGriffID = 5
case "9470" GetGriffID = 5
case "9472" GetGriffID = 5
case "9473" GetGriffID = 5
case "9500" GetGriffID = 22
case "9506" GetGriffID = 22
case "9520" GetGriffID = 5
case "9521" GetGriffID = 5
case "9550" GetGriffID = 22
case "9551" GetGriffID = 22
case "9552" GetGriffID = 22
case "9570" GetGriffID = 22
case "9571" GetGriffID = 22
case "9572" GetGriffID = 22
case "9600" GetGriffID = 22
case "9620" GetGriffID = 22
case "9630" GetGriffID = 22
case "9636" GetGriffID = 22
case "9660" GetGriffID = 22
case "9661" GetGriffID = 22
case "9667" GetGriffID = 22
case "9680" GetGriffID = 22
case "9681" GetGriffID = 22
case "9688" GetGriffID = 22
case "9690" GetGriffID = 22
case "9700" GetGriffID = 22
case "9750" GetGriffID = 22
case "9770" GetGriffID = 22
case "9771" GetGriffID = 22
case "9772" GetGriffID = 22
case "9790" GetGriffID = 22
case "9800" GetGriffID = 9
case "9810" GetGriffID = 9
case "9820" GetGriffID = 9
case "9830" GetGriffID = 9
case "9831" GetGriffID = 9
case "9840" GetGriffID = 9
case "9850" GetGriffID = 9
case "9860" GetGriffID = 9
case "9870" GetGriffID = 9
case "9880" GetGriffID = 9
case "9881" GetGriffID = 9
case "9890" GetGriffID = 9
case "9900" GetGriffID = 9
case "9910" GetGriffID = 9
case "9920" GetGriffID = 9
case "9921" GetGriffID = 9
case "9930" GetGriffID = 9
case "9931" GetGriffID = 9
case "9932" GetGriffID = 9
case "9940" GetGriffID = 9
case "9950" GetGriffID = 9
case "9960" GetGriffID = 9
case "9961" GetGriffID = 9
case "9968" GetGriffID = 9
case "9970" GetGriffID = 9
case "9971" GetGriffID = 9
case "9980" GetGriffID = 9
case "9981" GetGriffID = 9
case "9982" GetGriffID = 9
case "9988" GetGriffID = 9
case "9990" GetGriffID = 9
case "9991" GetGriffID = 9
case "9992" GetGriffID = 9

end select
end function

Function TryToIncrement(xStr)
    if len(xStr) > 1 then
        ToPos = 1
        FromPos = 1

        For x = Len(xStr) to 1 step -1
            charStr = mid(xStr,x,1)
            Select Case "" & charStr
                Case "9"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "8"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "7"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "6"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "5"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "4"
                    if ToPos = 1 then
                       ToPos = x
                    else
                       FromPos = x
                    end if
                Case "3"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "2"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "1"
                    if ToPos = 1 then
                       ToPos =  x
                    else
                       FromPos = x
                    end if
                Case "0"
                    if ToPos = 1 then
                       ToPos = x
                    else
                       FromPos = x
                    end if
                Case else
                    if ToPos <> 1 then
                        FromPos = x+1
                        exit for
                    end if
                end Select
        Next

        Nbr = mid(xstr,FromPos,(ToPos-FromPos+1))
        TryToIncrement = left(xstr,FromPos-1) & Nbr + 1 & Right(xstr,len(xstr) - topos)
    else
        TryToIncrement = xstr + 1
    end if

End Function

Function OnlyNumbers(txt )
Dim x
    For x = 1 To Len(txt)
    If IsNumeric(Mid(txt, x, 1)) Then OnlyNumbers = OnlyNumbers & Mid(txt, x, 1)
    Next

End Function

Private Function iif(Condition, TrueString, FalseString)
	if Condition then
		iif = TrueString
	else
		iif = FalseString
	end if
end function


    function JurSitTrans(intID,intLang)
        dim numID
        JurSitTrans = ""
        numID = 0
        if isnumeric(intID) then numID = Cint(intID)
        if intLang = 1 then
            select case numID
                case 0 : JurSitTrans = "Situation Normale"
                case 6 : JurSitTrans = "Arr�t�e pour cause de remplacement du num�ro"
                case 10 : JurSitTrans = "Dissolution de plein droit par arriv�e � terme"
                case 11 : JurSitTrans = "Cessation des activit�s en Belgique"
                case 12 : JurSitTrans = "Dissolution anticip�e - Liquidation"
                case 13 : JurSitTrans = "Dissolution judiciaire et nullit�"
                case 14 : JurSitTrans = "Cl�ture de liquidation"
                case 15 : JurSitTrans = "Cessation"
                case 16 : JurSitTrans = "Cessation d'activit� en personne physique"
                case 17 : JurSitTrans = "Transfert d'une entreprise de personne physique"
                case 20 : JurSitTrans = "R�union des parts en une seule main"
                case 21 : JurSitTrans = "Fusion par absorption"
                case 22 : JurSitTrans = "Fusion par constitution d'une nouvelle soci�t�"
                case 23 : JurSitTrans = "Scission"
                case 3 : JurSitTrans = "D�c�s"
                case 4 : JurSitTrans = "D�mission"
                case 5 : JurSitTrans = "Fin de mission"
                case 24 : JurSitTrans = "Scission par absorption"
                case 25 : JurSitTrans = "Scission par constitution de nouvelles soci�t�s"
                case 26 : JurSitTrans = "Scission mixte"
                case 30 : JurSitTrans = "Concordat avant faillite"
                case 31 : JurSitTrans = "Concordat apr�s faillite"
                case 40 : JurSitTrans = "Sursis provisoire"
                case 41 : JurSitTrans = "Sursis d�finitif"
                case 42 : JurSitTrans = "R�vocation du sursis"
                case 43 : JurSitTrans = "Fin du sursis"
                case 50 : JurSitTrans = "Faillite"
                case 51 : JurSitTrans = "Cl�ture de faillite en cas d'excusabilit�"
                case 52 : JurSitTrans = "Cl�ture de faillite sans excusabilit� du faillite"
                case 53 : JurSitTrans = "Jugement cl�ture de la faillite"
                case 90 : JurSitTrans = "Nouveaux statuts"
                case 91 : JurSitTrans = "Sursis de paiement"
                case 100 : JurSitTrans = "Identification de l'entreprise"
                case 999 : JurSitTrans = "Dossier annul�"
                case else : JurSitTrans = "Dossier inactif"
            end select
        else
            select case numID
                case 0 : JurSitTrans = "Normale Toestand"
                case 6 : JurSitTrans = "Stopzetting wegens vervanging"
                case 10 : JurSitTrans = "Ontbinding rechtswege door termijn verloop"
                case 11 : JurSitTrans = "Activiteitstopzetting in Belg�e"
                case 12 : JurSitTrans = "Vervroegde ontbinding - Vereffening"
                case 13 : JurSitTrans = "Juridische ontbinding of nietigheid"
                case 14 : JurSitTrans = "Sluiting van vereffening"
                case 15 : JurSitTrans = "Stopzetting"
                case 16 : JurSitTrans = "Stopzetting activiteit natuurlijke persoon"
                case 17 : JurSitTrans = "Overdracht van een onderneming natuurlijke persoon"
                case 20 : JurSitTrans = "Verzameling aandelen in hoofde van ��n persoon"
                case 21 : JurSitTrans = "Fusie door overneming"
                case 22 : JurSitTrans = "Fusie door oprichting van een nieuwe vennootschap"
                case 23 : JurSitTrans = "Splitsing"
                case 3 : JurSitTrans = "Overleden"
                case 4 : JurSitTrans = "Ontslag"
                case 5 : JurSitTrans = "Einde opdracht"
                case 24 : JurSitTrans = "Splitsing door opslorping"
                case 25 : JurSitTrans = "Splitsing door oprichting van nieuwe vennootschappen"
                case 26 : JurSitTrans = "Gemengde splitsing"
                case 30 : JurSitTrans = "Konkordaat v��r faling"
                case 31 : JurSitTrans = "Konkordaat na faling"
                case 40 : JurSitTrans = "Voorlopige opschorting van betaling"
                case 41 : JurSitTrans = "Definitieve opschorting van betaling"
                case 42 : JurSitTrans = "Herroeping van de opschorting"
                case 43 : JurSitTrans = "Einde van de opschorting"
                case 50 : JurSitTrans = "Faillissement"
                case 51 : JurSitTrans = "Sluiting van het faillissement in geval verschoonbaarheid"
                case 52 : JurSitTrans = "Sluiting van het failliss. in geval niet verschoonbaarheid"
                case 53 : JurSitTrans = "Vonnis sluiting van het faillissement"
                case 90 : JurSitTrans = "Nieuwe statuten"
                case 91 : JurSitTrans = "Uitstel van betaling"
                case 100 : JurSitTrans = "Bekendmaking van de onderneming"
                case 999 : JurSitTrans = "Geannuleerd dossier"
                case else : JurSitTrans = "Activiteit stopgezet"
            end select
        end if
    end function



    function getOtype(vtype)
        rval=""
        select case ucase(vtype)
            case "AMBTSHALVE"
                rval = vAmbtshalve(intlang)
            case "BEKENTENIS"
                rval = vBekentenis(intlang)
            case "DAGVAARDING"
                rval = vDagvaarding(intlang)
            case "ONBEKEND"
                rval = vVerklaring(intlang)
            case "VERZOEKSCHRIFT"
                rval = vVerzoekschrift(intlang)
        end select
        getotype=rval
    end function

    function getStype(vtype)
        rval=""
        select case ucase(vtype)
            case "GEBREK ACTIEF"
                rval = vGebrekActief(intlang)
            case "GEBREK PASSIEF"
                rval = vGebrekPassief(intlang)
            case "ONTOEREIKEND ACTIEF"
                rval = vOntoereikendActief(intlang)
            case "ONBEKEND"
                rval = vVerklaring(intlang)
            case "VEREFFENING"
                rval = vVereffening(intlang)
        end select
        getStype=rval
    end function

    function getSResult(vResult)
        rval=""
        select case ucase(vResult)
            case "ONVERSCHOONBAAR"
                rval = vOnverschoonbaar(intlang)
            case "VERSCHOONBAAR"
                rval = vVerschoonbaar(intlang)
        end select
        getSResult=rval
    end function


function date_as_ddmmmyyyy(xd)
    strd = year(xd) * 10000 + month(xd) * 100 + day(xd)
    date_as_ddmmmyyyy = cint(right(strd,2)) & "-"  & maandnaam(mid(strd,5,2)) & "-" & left(strd,4)

end function

function strDate_as_ddmmmyyyy(strd)
    strDate_as_ddmmmyyyy = strd
    if len(strd) = 8 then
    strDate_as_ddmmmyyyy = cint(right(strd,2)) & "-"  & maandnaam(mid(strd,5,2)) & "-" & left(strd,4)
    end if
end function


function maandnaam(x)
 select case x
 case "01" : maandnaam = "Jan"
 case "02" : maandnaam = "Feb"
 case "03" : maandnaam = "Maa"
 case "04" : maandnaam = "Apr"
 case "05" : maandnaam = "Mei"
 case "06" : maandnaam = "Jun"
 case "07" : maandnaam = "Jul"
 case "08" : maandnaam = "Aug"
 case "09" : maandnaam = "Sep"
 case "10" : maandnaam = "Okt"
 case "11" : maandnaam = "Nov"
 case "12" : maandnaam = "Dec"
 end select
end function

function weekdagnaam(strd)

 dteX = cdate(left(strd,4) & "-" & mid(strd,5,2) & "-" & right(strd,2))

 select case weekday(dteX,2)
 case 1 : weekdagnaam = "Maandag"
 case 2 : weekdagnaam = "Dinsdag"
 case 3 : weekdagnaam = "Woensdag"
 case 4 : weekdagnaam = "Donderdag"
 case 5 : weekdagnaam = "Vrijdag"
 case 6 : weekdagnaam = "Zaterdag"
 case 7 : weekdagnaam = "Zondag"
 end select

end function


function strDate_as_wd_ddmmmyyyy(strd)
    strDate_as_wd_ddmmmyyyy = strd
    if len(strd) = 8 then
    strDate_as_wd_ddmmmyyyy = weekdagnaam(strd) & ", " & cint(right(strd,2)) & "-"  & maandnaam(mid(strd,5,2)) & "-" & left(strd,4)
    end if
end function

function getGriffie(vGRid)
    getGriffie = ""
    select case vGRid & ""
        case "28"
             getGriffie = "Aalst (Dendermonde)"
        case "1"
             getGriffie = "Antwerpen"
        case "31"
             getGriffie = "Arlon"
        case "2"
             getGriffie = "Brugge"
        case "3"
             getGriffie = "Brussel"
        case "4"
             getGriffie = "Charleroi"
        case "5"
             getGriffie = "Dendermonde"
        case "6"
             getGriffie = "Dinant"
        case "8"
             getGriffie = "Eupen"
        case "9"
             getGriffie = "Gent"
        case "10"
             getGriffie = "Hasselt"
        case "11"
             getGriffie = "Huy"
        case "12"
             getGriffie = "Ieper"
        case "13"
             getGriffie = "Kortrijk"
        case "30"
             getGriffie = "La Louviere"
        case "14"
             getGriffie = "Leuven"
        case "15"
             getGriffie = "Li�ge"
        case "16"
             getGriffie = "Marche-en-Famenne"
        case "17"
             getGriffie = "Mechelen"
        case "18"
             getGriffie = "Mons"
        case "19"
             getGriffie = "Namur"
        case "20"
             getGriffie = "Neufch�teau"
        case "21"
             getGriffie = "Nivelles"
        case "22"
             getGriffie = "Oudenaarde"
        case "23"
             getGriffie = "Tongeren"
        case "24"
             getGriffie = "Tournai"
        case "25"
             getGriffie = "Turnhout"
        case "26"
             getGriffie = "Verviers"
        case "27"
             getGriffie = "Veurne"
    end select
end function

function getgoType(vType)
    select case vType & ""
        case "AANSTELLING_VEREFFENAAR"
            getgoType = "Aanstelling vereffenaar"
        case "VERVANGING_VEREFFENAAR"
            getgoType = "Vervanging vereffenaar"
        case "VERLENGEN_MANDAAT"
            getgoType = "Verlengen mandaat"
        case "ONTBINDING_SLUITINGONTB"
            getgoType = "Ontbinding + onmiddelijke sluiting vereffening"
        case "SLUITINGONTB"
            getgoType = "Sluiting vereffening"
        case "SLUITINGFAIL"
            getgoType = "Ontbinding + sluiting faillissement"
        case "INTREKKINGONTB"
            getgoType = "Intrekking ontbinding"
    end select
end function

%>