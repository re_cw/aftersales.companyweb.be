<%

vJRCAPDFS = "Y:\XBRL"
vJRCATXT = "Y:\JRCATXT"
vConnStr = "Provider=sqloledb;Data Source=phoenix,1433;Network Library=DBMSSOCN;Initial Catalog=odmdbase_sub2;User ID=odm;Password=Py36qtUF;"
zipFileDir = "Y:\ZipUpdates"
zipFileURL = "http://odm.outcome.be/zips/"

vJRCAPDFS = "X:\XBRL"
vJRCATXT = "X:\JRCATXT"
vConnStr = "Provider=sqloledb;Data Source=192.168.2.2,1433;Network Library=DBMSSOCN;Initial Catalog=odmdbase;User ID=odmuser;Password=odmuser;"
zipFileDir = "X:\ZipUpdates"
zipFileURL = "http://odm.outcome.be/zips/"

config_searchConnStr = "Provider=sqloledb;Data Source=192.168.2.2,1433;Network Library=DBMSSOCN;Initial Catalog=odmsearch;User ID=odmuser;Password=odmuser;"

JRCALocation = "http://odm.outcome.be/jrca.asp?lang=[lang]&login=[login]&vat=[vat]&key=[key]"
JRCALocation_ = "http://odm.outcome.be/jrca_[lang].asp?login=[login]&vat=[vat]&key=[key]"
HRLocation = "http://odm.outcome.be/rapport_[lang].asp?login=[login]&vat=[vat]&key=[key]"
RapportLocation = "http://odm.outcome.be/rappLoad.asp?lang=[lang]&login=[login]&vat=[vat]&key=[key]"
PageDetailLocation = "Page_CompanyDetail.asp?login=[login]&vat=[vat]&key=[key]"
RALocation = "http://www.companyweb.be/RegisteredContractors.asp?login=[login]&vat=[vat]&lang=[lang]&key=[key]"
RFTLocation = "http://www.companyweb.be/rft.asp?login=[login]&date=[date]&nr=[nr]&lang=[lang]&key=[key]"
PdfLocation = "http://www.companyweb.be/pdf.asp?login=[login]&pdf=[pdf]&key=[key]"
PTCLocation = "http://www.companyweb.be/Participations.asp?login=[login]&vat=[vat]&lang=[lang]&key=[key]"
ContactLocation = "http://www.companyweb.be/ProContact.asp?login=[login]&vat=[vat]&lang=[lang]&key=[key]"
FaResultLocation = "Page_fa_result.asp?login=[login]&sid=[sid]&key=[key]"
CompResultLocation = "Page_Comp_result.asp?login=[login]&sid=[sid]&key=[key]"
HeadOfLocation = "http://www.companyweb.be/headofcompany.asp?login=[login]&vat=[vat]&lang=[lang]&key=[key]"
WarningLocation = "http://www.companyweb.be/warnings.asp?login=[login]&vat=[vat]&lang=[lang]&key=[key]"
CuratorLocation = "http://www.companyweb.be/curator.asp?login=[login]&date=[date]&vat=[vat]&lang=[lang]&key=[key]"

Function OnlyNumbers(txt ) 
Dim x 
    For x = 1 To Len(txt)
    If instr(1,"0123456789",Mid(txt, x, 1)) <> 0 Then OnlyNumbers = OnlyNumbers & Mid(txt, x, 1)
    Next

End Function

Function IsNumeriek(intval) 
IsNumeriek = false
if intval <> "" then
if onlynumbers(intval) = trim(intval) then IsNumeriek = true
end if
end function

Function CleanUpName(x) 
Dim xChar 
Dim i 
    
  CleanUpName = ""
  
  x = UCase(x)
  
  If Trim(x & "") <> "" Then
    For i = 1 To Len(x)
        xChar = Mid(x, i, 1)
        If (Asc(xChar) >= Asc("A") And Asc(xChar) <= Asc("Z")) or (Asc(xChar) >= Asc("0") And Asc(xChar) <= Asc("9")) Then
            CleanUpName = CleanUpName & Mid(x, i, 1)
        End If
    Next
  End If

End Function


Function IsBTWnr(pInput)


IsBTWnr = False
    
    xStr = OnlyNumbers(pInput & "")
    If Len(xStr) = 9 Then
        modlo = CDbl(Left(xStr, 7)) Mod 97
        diff = 97 - CLng(Right(xStr, 2))
        If modlo = diff Then
             IsBTWnr = True
        End If
    End If
    

End Function

Function BuildMD5(pInput)

	tt = conn.execute("select dbo.fn_md5(upper('" & pInput & "'))")	    
	BuildMD5 = tt(0)

End Function

Function BuildCuratorPath(xVat , xDate , xLang ,xlogin,xpswd)

BuildCuratorPath = ""

If IsNumeric(xVat) Then
	
    BuildCuratorPath = CuratorLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildCuratorPath = Replace(BuildCuratorPath, "[login]", ucase(xlogin))
    BuildCuratorPath = Replace(BuildCuratorPath, "[date]", xDate)
    BuildCuratorPath = Replace(BuildCuratorPath, "[vat]", xVat)
    BuildCuratorPath = Replace(BuildCuratorPath, "[lang]", xLang)
    BuildCuratorPath = Replace(BuildCuratorPath, "[key]", BuildMD5(xKey))
End If

End Function

Function BuildPDFPath(x,xlogin,xpswd) 

BuildPDFPath = ""

If Len(Trim(x & "")) > 23 Then
	
	xKey = "[login]|[password]|OC|[pdf]"
	
    BuildPDFPath = PdfLocation
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[pdf]", Right(x, 23))
    BuildPDFPath = Replace(BuildPDFPath, "[login]",ucase(xlogin))
    BuildPDFPath = Replace(BuildPDFPath, "[pdf]", Right(x, 23))
    BuildPDFPath = Replace(BuildPDFPath, "[key]", BuildMD5(xKey))
 End If

End Function

Function BuildFaResultPath(x,xlogin,xpswd) 

BuildFaResultPath = ""

If isnumeric(x) Then
	
	xKey = "[login]|[password]|OC|[sid]"
	
    BuildFaResultPath = FaResultLocation
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[sid]",x)
    BuildFaResultPath = Replace(BuildFaResultPath, "[login]",ucase(xlogin))
    BuildFaResultPath = Replace(BuildFaResultPath, "[sid]", x)
    BuildFaResultPath = Replace(BuildFaResultPath, "[key]", BuildMD5(xKey))
 End If

End Function

Function BuildCompResultPath(x,xlogin,xpswd) 

BuildCompResultPath = ""

If isnumeric(x) Then
	
	xKey = "[login]|[password]|OC|[sid]"
	
    BuildCompResultPath = CompResultLocation
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[sid]",x)
    BuildCompResultPath = Replace(BuildCompResultPath, "[login]",ucase(xlogin))
    BuildCompResultPath = Replace(BuildCompResultPath, "[sid]", x)
    BuildCompResultPath = Replace(BuildCompResultPath, "[key]", BuildMD5(xKey))
 End If

End Function

Function BuildRFTPath(xVat , xDate , xNr , xLang ,xlogin,xpswd)

BuildRFTPath = ""

If IsNumeric(xNr) Then
	
    BuildRFTPath = RFTLocation
    
    xKey = "[login]|[password]|OC|[nr]"
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[nr]", xNr)
    BuildRFTPath = Replace(BuildRFTPath, "[login]", ucase(xlogin))
    BuildRFTPath = Replace(BuildRFTPath, "[date]", xDate)
    BuildRFTPath = Replace(BuildRFTPath, "[nr]", xNr)
    BuildRFTPath = Replace(BuildRFTPath, "[lang]", xLang)
    BuildRFTPath = Replace(BuildRFTPath, "[key]", BuildMD5(xKey))
End If

End Function

Function BuildJrCaPath(xVat,xlogin,xpswd,xlang) 

BuildJrCaPath = ""

If IsNumeric(xVat) And Len(xVat) = 9 Then
	
    BuildJrCaPath = JRCALocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]",ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildJrCaPath = Replace(BuildJrCaPath, "[login]", ucase(xlogin))
    BuildJrCaPath = Replace(BuildJrCaPath, "[lang]", ucase(xlang))
    BuildJrCaPath = Replace(BuildJrCaPath, "[vat]", xVat)
    BuildJrCaPath = Replace(BuildJrCaPath, "[key]", BuildMD5(xKey))
End If

End Function


Function BuildHRPath(xVat,xlogin,xpswd,xlang) 

BuildHRPath = ""

If IsNumeric(xVat) And Len(xVat) = 9 Then
	
    BuildHRPath = HRLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]",ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildHRPath = Replace(BuildHRPath, "[login]", ucase(xlogin))
    BuildHRPath = Replace(BuildHRPath, "[lang]", ucase(xlang))
    BuildHRPath = Replace(BuildHRPath, "[vat]", xVat)
    BuildHRPath = Replace(BuildHRPath, "[key]", BuildMD5(xKey))
End If

End Function

Function BuildRapportPath(xVat,xlogin,xpswd,xlang) 

BuildRapportPath = ""

If IsNumeric(xVat) And Len(xVat) = 9 Then
	
    BuildRapportPath = RapportLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]",ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildRapportPath = Replace(BuildRapportPath, "[login]", ucase(xlogin))
    BuildRapportPath = Replace(BuildRapportPath, "[lang]", ucase(xlang))
    BuildRapportPath = Replace(BuildRapportPath, "[vat]", xVat)
    BuildRapportPath = Replace(BuildRapportPath, "[key]", BuildMD5(xKey))
End If

End Function



Function BuildFirmDetailPath(xVat,xlogin,xpswd) 

BuildFirmDetailPath = ""

If IsNumeric(xVat) And Len(xVat) = 9 Then
	
    BuildFirmDetailPath = PageDetailLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]",ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildFirmDetailPath = Replace(BuildFirmDetailPath, "[login]", ucase(xlogin))
    BuildFirmDetailPath = Replace(BuildFirmDetailPath, "[vat]", xVat)
    BuildFirmDetailPath = Replace(BuildFirmDetailPath, "[key]", BuildMD5(xKey))
End If

End Function


Function BuildRCPath(xVat, xLang,xlogin,xpswd)

BuildRCPath = ""

If RALocation <> "" Then
	
	BuildRCPath = RALocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildRCPath = Replace(BuildRCPath, "[login]", ucase(xlogin))
    BuildRCPath = Replace(BuildRCPath, "[vat]", xVat)
    BuildRCPath = Replace(BuildRCPath, "[lang]", xLang)
    BuildRCPath = Replace(BuildRCPath, "[key]", BuildMD5(xKey))

End If

End Function


Function BuildPTCPath(xVat, xLang,xlogin,xpswd)

BuildPTCPath = ""

If PTCLocation <> "" Then
	
	BuildPTCPath = PTCLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildPTCPath = Replace(BuildPTCPath, "[login]", ucase(xlogin))
    BuildPTCPath = Replace(BuildPTCPath, "[vat]", xVat)
    BuildPTCPath = Replace(BuildPTCPath, "[lang]", xLang)
    BuildPTCPath = Replace(BuildPTCPath, "[key]", BuildMD5(xKey))

End If

End Function



Function BuildContactPath(xVat, xLang,xlogin,xpswd)

BuildContactPath = ""

If ContactLocation <> "" Then
	
	BuildContactPath = ContactLocation
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildContactPath = Replace(BuildContactPath, "[login]", ucase(xlogin))
    BuildContactPath = Replace(BuildContactPath, "[vat]", xVat)
    BuildContactPath = Replace(BuildContactPath, "[lang]", xLang)
    BuildContactPath = Replace(BuildContactPath, "[key]", BuildMD5(xKey))

End If

End Function



Function BuildHeadOfPath(xVat,xlogin,xpswd,xlang) 

BuildHeadOfPath = ""

If HeadOfLocation <> "" Then
	
    BuildHeadOfPath = HeadOfLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]",ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildHeadOfPath = Replace(BuildHeadOfPath, "[login]", ucase(xlogin))
    BuildHeadOfPath = Replace(BuildHeadOfPath, "[lang]", ucase(xlang))
    BuildHeadOfPath = Replace(BuildHeadOfPath, "[vat]", xVat)
    BuildHeadOfPath = Replace(BuildHeadOfPath, "[key]", BuildMD5(xKey))
End If

End Function


Function BuildWarningPath(xVat,xlogin,xpswd,xLang)

BuildWarningPath = ""

If WarningLocation <> "" Then
	
	BuildWarningPath = WarningLocation
    
    xKey = "[login]|[password]|OC|[vat]"
    xKey = Replace(xKey, "[login]", ucase(xlogin))
    xKey = Replace(xKey, "[password]", ucase(xpswd))
    xKey = Replace(xKey, "[vat]", xVat)
    BuildWarningPath = Replace(BuildWarningPath, "[login]", ucase(xlogin))
    BuildWarningPath = Replace(BuildWarningPath, "[vat]", xVat)
    BuildWarningPath = Replace(BuildWarningPath, "[lang]", xLang)
    BuildWarningPath = Replace(BuildWarningPath, "[key]", BuildMD5(xKey))

End If

End Function



Function NumberFrmt(txt) 
Dim x 
Dim x2 
Dim tmp 
Dim Neg 

    NumberFrmt = ""

If IsNumeric(txt) then
    if txt < 0 then
    	Neg = "- " 
    else
    	Neg = "" 
    end if
    tmp = int(txt)
    tmp = txt - tmp
    txt = int(txt)
    x2 = 0
    For x = Len(txt) To 1 step - 1
    If IsNumeric(Mid(txt, x, 1)) Then 
    if x2 mod 3 = 0 and x2<>0 then
    NumberFrmt =  Mid(txt, x, 1) & "." & NumberFrmt
    else
    NumberFrmt =  Mid(txt, x, 1) & NumberFrmt
    end if
    x2 = x2 + 1
    end if
    Next
    
    if tmp <> 0 then NumberFrmt = NumberFrmt & "," & Mid(tmp, 3, 1)
    NumberFrmt = Neg &  NumberFrmt 
end if

End Function


%>