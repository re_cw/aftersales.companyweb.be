<%
    intLang = 0
    admin = false
    supervisor = false
    GoodLog = false

    if request.QueryString("lng") <> "" then
        Response.Cookies("intLang") = request.QueryString("lng")
    end if

    if Request.QueryString("lang") = 1 then Response.Cookies("intLang") = "1"

    if Request.Cookies("intLang") = "" then
	    GoodLog = false
    else
	    intLang = clng(Request.Cookies("intLang"))
    end if

    'ADMIN
    if Ucase(Request.Cookies("EditMandatesUser")) = "HANS" and Ucase(Request.Cookies("EditMandatesPwd")) = "TEST" then
        GoodLog = true
        FM_Online = 10
        admin=true
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "PATRICK" and Ucase(Request.Cookies("EditMandatesPwd")) = "ADMIN" then
        GoodLog = true
        FM_Online = 12
        admin=true
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "MELISSA" and Ucase(Request.Cookies("EditMandatesPwd")) = "MELISSA" then
        GoodLog = true
        FM_Online = 13
        admin=true
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "EVA" and Ucase(Request.Cookies("EditMandatesPwd")) = "TEST" then
        GoodLog = true
        FM_Online = 14
        admin=true
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SYLVIE" and Ucase(Request.Cookies("EditMandatesPwd")) = "SYLVIE" then
        GoodLog = true
        FM_Online = 15
        admin=true
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "BENTE" and Ucase(Request.Cookies("EditMandatesPwd")) = "BENTE" then
        GoodLog = true
        FM_Online = 16
        admin=true
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "EVELIEN" and Ucase(Request.Cookies("EditMandatesPwd")) = "EVELIEN" then
        GoodLog = true
        FM_Online = 17
        admin=true
    end if


    'PAN
    if Ucase(Request.Cookies("EditMandatesUser")) = "PAN01" and Ucase(Request.Cookies("EditMandatesPwd")) = "Z7X6G6" then
        GoodLog = true
        supervisor = true
        FM_Online = 101
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "PAN02" and Ucase(Request.Cookies("EditMandatesPwd")) = "K8E3Y2" then
        GoodLog = true
        FM_Online = 102
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "PAN03" and Ucase(Request.Cookies("EditMandatesPwd")) = "Y2S7E4" then
        GoodLog = true
        FM_Online = 103
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "PAN04" and Ucase(Request.Cookies("EditMandatesPwd")) = "B7P5X3" then
        GoodLog = true
        FM_Online = 104
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "PAN05" and Ucase(Request.Cookies("EditMandatesPwd")) = "K2F6D7" then
        GoodLog = true
        FM_Online = 105
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "PAN06" and Ucase(Request.Cookies("EditMandatesPwd")) = "W7W6W2" then
        GoodLog = true
        FM_Online = 106
    end if



    'TUNESIE
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN01" and Ucase(Request.Cookies("EditMandatesPwd")) = "Z7A6G6" then
        GoodLog = true
        supervisor = true
        FM_Online = 101
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN02" and Ucase(Request.Cookies("EditMandatesPwd")) = "K8E3C2" then
        GoodLog = true
        FM_Online = 102
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN03" and Ucase(Request.Cookies("EditMandatesPwd")) = "Z2S7E4" then
        GoodLog = true
        FM_Online = 103
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN04" and Ucase(Request.Cookies("EditMandatesPwd")) = "B7P5K3" then
        GoodLog = true
        FM_Online = 104
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN05" and Ucase(Request.Cookies("EditMandatesPwd")) = "K2F6F7" then
        GoodLog = true
        FM_Online = 105
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN06" and Ucase(Request.Cookies("EditMandatesPwd")) = "W7F6X2" then
        GoodLog = true
        FM_Online = 106
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN07" and Ucase(Request.Cookies("EditMandatesPwd")) = "C2N6U3" then
        GoodLog = true
        FM_Online = 128
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN08" and Ucase(Request.Cookies("EditMandatesPwd")) = "N8K7G3" then
        GoodLog = true
        FM_Online = 129
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN09" and Ucase(Request.Cookies("EditMandatesPwd")) = "U2F8E5" then
        GoodLog = true
        FM_Online = 130
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN10" and Ucase(Request.Cookies("EditMandatesPwd")) = "E5H5Z5" then
        GoodLog = true
        FM_Online = 131
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN11" and Ucase(Request.Cookies("EditMandatesPwd")) = "V5N2P7" then
        GoodLog = true
        FM_Online = 132
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN12" and Ucase(Request.Cookies("EditMandatesPwd")) = "Z3A3N4" then
        GoodLog = true
        FM_Online = 133
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN13" and Ucase(Request.Cookies("EditMandatesPwd")) = "D5M4Y4" then
        GoodLog = true
        FM_Online = 153
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN14" and Ucase(Request.Cookies("EditMandatesPwd")) = "X8N4R5" then
        GoodLog = true
        FM_Online = 154
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN15" and Ucase(Request.Cookies("EditMandatesPwd")) = "G8J2U2" then
        GoodLog = true
        FM_Online = 155
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN16" and Ucase(Request.Cookies("EditMandatesPwd")) = "M4Y2P5 " then
        GoodLog = true
        FM_Online = 156
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN17" and Ucase(Request.Cookies("EditMandatesPwd")) = "C7W7S9" then
        GoodLog = true
        FM_Online = 157
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN18" and Ucase(Request.Cookies("EditMandatesPwd")) = "U3E2P1" then
        GoodLog = true
        FM_Online = 158
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN19" and Ucase(Request.Cookies("EditMandatesPwd")) = "W4M8K7" then
        GoodLog = true
        FM_Online = 159
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "TUN20" and Ucase(Request.Cookies("EditMandatesPwd")) = "B8N8T6" then
        GoodLog = true
        FM_Online = 160
    end if

    'SURINAME
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR01" and Ucase(Request.Cookies("EditMandatesPwd")) = "0R7F2J2" then
        GoodLog = true
        supervisor = true
        FM_Online = 114
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR02" and Ucase(Request.Cookies("EditMandatesPwd")) = "0W5D3Y3" then
        GoodLog = true
        FM_Online = 115
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR03" and Ucase(Request.Cookies("EditMandatesPwd")) = "0R7E3C3" then
        GoodLog = true
        FM_Online = 116
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR04" and Ucase(Request.Cookies("EditMandatesPwd")) = "0F5C7T4" then
        GoodLog = true
        FM_Online = 117
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR05" and Ucase(Request.Cookies("EditMandatesPwd")) = "0K4A8H4" then
        GoodLog = true
        FM_Online = 118
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR06" and Ucase(Request.Cookies("EditMandatesPwd")) = "0Z8A5M5" then
        GoodLog = true
        FM_Online = 119
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR07" and Ucase(Request.Cookies("EditMandatesPwd")) = "0Y6U3K3" then
        GoodLog = true
        FM_Online = 120
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR08" and Ucase(Request.Cookies("EditMandatesPwd")) = "0S2D3B2" then
        GoodLog = true
        FM_Online = 139
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR09" and Ucase(Request.Cookies("EditMandatesPwd")) = "0R4R4V5" then
        GoodLog = true
        FM_Online = 140
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR10" and Ucase(Request.Cookies("EditMandatesPwd")) = "0U4Y5B5" then
        GoodLog = true
        FM_Online = 141
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR11" and Ucase(Request.Cookies("EditMandatesPwd")) = "0A5S7E7" then
        GoodLog = true
        FM_Online = 142
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR12" and Ucase(Request.Cookies("EditMandatesPwd")) = "0T2D5D5" then
        GoodLog = true
        FM_Online = 143
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR13" and Ucase(Request.Cookies("EditMandatesPwd")) = "0S3J5M3" then
        GoodLog = true
        FM_Online = 144
    end if
    if Ucase(Request.Cookies("EditMandatesUser")) = "SUR14" and Ucase(Request.Cookies("EditMandatesPwd")) = "0B8N8T3" then
        GoodLog = true
        FM_Online = 145
    end if

    if left(Request.Cookies("EditMandatesUser"),3) = "SUR" then
        Response.write("Password Expired")
        Response.end
    end if

    if not GoodLog then
        Response.Redirect("/logon.asp")
    end if



%>