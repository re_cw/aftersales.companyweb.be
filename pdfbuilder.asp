﻿<%

public function CreateGUID()
  Randomize Timer
  Dim tmpTemp1,tmpTemp2,tmpTemp3
  tmpTemp1 = Right(String(15,48) & CStr(CLng(DateDiff("s","1/1/2000",Date()))), 15)
  tmpTemp2 = Right(String(5,48) & CStr(CLng(DateDiff("s","12:00:00 AM",Time()))), 5)
  tmpTemp3 = Right(String(5,48) & CStr(Int(Rnd(1) * 100000)),5)
  CreateGUID = tmpTemp1 & tmpTemp2 & tmpTemp3
End Function

Dim pdf
Set pdf = Server.CreateObject("Persits.Pdf")

Dim pdfKey
    pdfKey = "arnem"

Dim lang
    lang= request.Cookies ("TELESALESTOOLV2")("lang_radical")
Dim ext
    ext = ".pdf"
Dim userFirstName
    userFirstName = request.Cookies ("TELESALESTOOLV2")("firstname")

Dim pdfName
    pdfName = "bestelbon_int_" & lang & ext

Dim pdfUrl
    pdfUrl =server.mappath("\xlib\xtemplates\" + pdfName)

Dim pdfDoc, pdfField

    set pdfDoc = pdf.openDocument(pdfUrl,pdfKey)

    Set pdfField = pdfDoc.Form.FindField("datum")
            pdfField.SetFieldValue now(), pdfDoc.Fonts("Helvetica")
    Set pdfField = pdfDoc.Form.FindField("contact")
            pdfField.SetFieldValue userFirstName, pdfDoc.Fonts("Helvetica")

response.write "Server.MapPath pdfUrl:" & pdfUrl & "<br>"

Dim objFSO
    set objFSO = Server.CreateObject("scripting.Filesystemobject")
Dim outputFolder
    'outputFolder = "\xtmpTO\"
    outputFolder = "Y:\Companyweb\BestelBons\TST2\"

Dim outGuid
    outGuid = CreateGUID()

Dim outFileName

select case lang
    case "FR"
        outFileName = "commande_" & outGuid & ext
    case "NL"
        outFileName = "bestelbon_" & outGuid & ext
    case else
end select

Dim outputFile
    outputFile = outputFolder & outFileName
    If objFSO.FileExists(outputFile)then objFSO.DeleteFile(outputFile)

    response.write "Server.MapPath outputFile:" & outputFile & "<br>"

    pdfDoc.Save outputFile, true

    set objFSO = nothing
    set pdfDoc = nothing
    set pdf = nothing
%>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8" />
    <style type="text/css">
        body {
            font: 12px/1.5 Verdana, Arial, Helvetica, sans-serif;
            background: #e6e6e6;
            color: #222;
        }

        a {
            font: 12px/1.5 Verdana, Arial, Helvetica, sans-serif;
            color: #222;
            text-decoration: none;
        }

        .biglink {
            font: 14px/1.5 Verdana, Arial, Helvetica, sans-serif;
            color: #eb3c34;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <%

       response.write "outputFile:" & outputFile & vbcrlf
    %>
</body>
</html>